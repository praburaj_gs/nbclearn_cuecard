﻿package {

    public interface IExtensions {

        function getWidth():int;
        function getHeight():int;
        function init(_arg1):void;

    }
}//package 
