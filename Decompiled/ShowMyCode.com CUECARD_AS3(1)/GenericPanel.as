﻿package {
    import flash.events.*;
    import flash.display.*;
    import flash.net.*;
    import flash.text.*;

    public class GenericPanel extends MovieClip {

        private var title_txt:TextField;
        private var file_txt:TextField;
        private var fileDirBackground:MovieClip;
        private var button_1:MovieClip;
        private var button_2:MovieClip;
        private var buttonTxtFmt:TextFormat;
        private var mainTextFmt:TextFormat;
        private var fileTextFmt:TextFormat;
        private var _butonCallBack:Function;
        private var buttonColor:uint;
        private var _cancelFunction:Function;
        private var imageLoader:Loader;
        private var image:Sprite;
        private var logoNotLoaded:Boolean = true;
        private var dislayImage:Boolean = false;
        private var onlyOneButton:Boolean = false;

        public function GenericPanel(){
            this.title_txt = new TextField();
            this.title_txt.multiline = true;
            this.title_txt.wordWrap = true;
            this.title_txt.width = 322;
            this.title_txt.autoSize = "center";
            this.title_txt.textColor = 0xFFFFFF;
            this.title_txt.antiAliasType = "normal";
            this.mainTextFmt = new TextFormat();
            this.mainTextFmt.color = 0xFFFFFF;
            this.mainTextFmt.bold = false;
            this.mainTextFmt.size = 14;
            this.mainTextFmt.font = "Arial";
            this.mainTextFmt.align = "center";
            this.file_txt = new TextField();
            this.file_txt.multiline = true;
            this.file_txt.wordWrap = true;
            this.file_txt.width = 270;
            this.file_txt.autoSize = "center";
            this.file_txt.textColor = 0;
            this.file_txt.antiAliasType = "normal";
            this.fileTextFmt = new TextFormat();
            this.fileTextFmt.color = 0;
            this.fileTextFmt.bold = false;
            this.fileTextFmt.size = 14;
            this.fileTextFmt.font = "Arial";
            this.fileTextFmt.align = "center";
            this.button_1 = new MainMenuButton();
            this.button_2 = new MainMenuButton();
            this.buttonTxtFmt = new TextFormat();
            this.buttonTxtFmt.bold = true;
            this.buttonTxtFmt.align = "center";
            this.button_1.button_txt.setTextFormat(this.buttonTxtFmt);
            this.button_2.button_txt.setTextFormat(this.buttonTxtFmt);
            this.button_1.bg.width = 120;
            this.button_2.bg.width = 120;
            this.button_1.button_txt.x = -120;
            this.button_2.button_txt.x = -120;
            this.button_1.button_txt.width = 120;
            this.button_2.button_txt.width = 120;
            addChild(this.button_1);
            addChild(this.button_2);
            this.button_1.addEventListener(MouseEvent.CLICK, this.buttonPressed);
            this.button_1.addEventListener(MouseEvent.ROLL_OVER, this.btnRollOver);
            this.button_1.addEventListener(MouseEvent.ROLL_OUT, this.btnRollOut);
            this.button_2.addEventListener(MouseEvent.CLICK, this.cancelPressed);
            this.button_2.addEventListener(MouseEvent.ROLL_OVER, this.btnRollOver);
            this.button_2.addEventListener(MouseEvent.ROLL_OUT, this.btnRollOut);
            this.button_2.buttonMode = true;
            this.button_2.useHandCursor = true;
            this.button_1.buttonMode = true;
            this.button_1.useHandCursor = true;
            this.fileDirBackground = new MainMenuButton();
            this.fileDirBackground.button_txt.text = "";
            Colors.changeColor(this.fileDirBackground.bg.grfx, 0xFFFFFF);
            addChild(this.fileDirBackground);
            addChild(this.title_txt);
            addChild(this.file_txt);
        }
        public function displayNBCLogo(){
            var _local1:URLRequest;
            if (this.logoNotLoaded){
                this.logoNotLoaded = false;
                this.image = new Sprite();
                this.imageLoader = new Loader();
                _local1 = new URLRequest("introScreen.png");
                this.imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, this.logoLoaded);
                this.imageLoader.load(_local1);
                addChild(this.image);
                this.image.addChild(this.imageLoader);
            } else {
                this.dislayImage = true;
                if (!this.image){
                    addChild(this.image);
                } else {
                    this.image.visible = true;
                };
                this.arrangeContent();
            };
        }
        private function logoLoaded(_arg1:Event=null){
            this.dislayImage = true;
            this.arrangeContent();
        }
        public function setText(_arg1:String){
            if (((!((_arg1 == ""))) || (!((_arg1 == " "))))){
                this.dislayImage = false;
                if (this.image){
                    this.image.visible = false;
                };
            };
            this.title_txt.text = _arg1;
            if (this.title_txt.width > 322){
                this.title_txt.width = 270;
            };
            this.title_txt.setTextFormat(this.mainTextFmt);
        }
        public function setButtonAtributes(_arg1:String, _arg2:String, _arg3:uint, _arg4:Function=null){
            this.button_1.button_txt.text = _arg1;
            this.button_1.button_txt.setTextFormat(this.buttonTxtFmt);
            if ((((_arg2 == "")) || ((_arg2 == " ")))){
                this.onlyOneButton = true;
                this.button_2.visible = false;
            } else {
                this.button_2.visible = true;
                this.button_2.button_txt.text = _arg2;
                this.button_2.button_txt.setTextFormat(this.buttonTxtFmt);
                this.onlyOneButton = false;
            };
            this.buttonColor = _arg3;
            Colors.changeColor(this.button_1.bg.grfx, _arg3);
            Colors.changeColor(this.button_2.bg.grfx, _arg3);
            if (_arg4 != null){
                this.butonCallBack = _arg4;
            };
        }
        public function removeFileDirectory(){
            this.fileDirBackground.visible = false;
            this.file_txt.visible = false;
            this.arrangeContent();
        }
        public function setFileDirectory(_arg1:String){
            this.fileDirBackground.visible = true;
            this.file_txt.visible = true;
            this.file_txt.text = _arg1;
            this.file_txt.setTextFormat(this.fileTextFmt);
            this.fileDirBackground.width = (this.file_txt.width + 10);
            this.fileDirBackground.height = (this.file_txt.height + 15);
            this.arrangeContent();
        }
        public function getFileDirectory():String{
            return (String(this.file_txt.text));
        }
        public function set butonCallBack(_arg1:Function){
            this._butonCallBack = _arg1;
        }
        public function get butonCallBack():Function{
            return (this._butonCallBack);
        }
        public function set cancelFunction(_arg1:Function){
            this._cancelFunction = _arg1;
        }
        private function btnRollOver(_arg1:MouseEvent){
            var _local2:MovieClip = (_arg1.currentTarget as MovieClip);
            Colors.changeColor(_local2.bg.grfx, 0x333333);
        }
        private function btnRollOut(_arg1:MouseEvent){
            var _local2:MovieClip = (_arg1.currentTarget as MovieClip);
            Colors.changeColor(_local2.bg.grfx, this.buttonColor);
        }
        private function buttonPressed(_arg1:MouseEvent){
            if (this._butonCallBack != null){
                this._butonCallBack();
            };
        }
        private function cancelPressed(_arg1:MouseEvent){
            trace("CANCEL");
            if (this._cancelFunction != null){
                this._cancelFunction();
            };
        }
        public function arrangeContent():void{
            this.parent["nbc_logo"].x = this.parent["bg"].width;
            this.parent["nbc_logo"].y = this.parent["bg"].height;
            this.title_txt.x = ((this.parent["bg"].width / 2) - (this.title_txt.width / 2));
            this.title_txt.y = ((this.parent["bg"].height / 2) - (this.title_txt.height / 2));
            if (this.file_txt.visible){
                this.title_txt.y = (this.title_txt.y - 40);
                this.file_txt.x = ((this.parent["bg"].width / 2) - (this.file_txt.width / 2));
                this.file_txt.y = ((this.title_txt.y + this.title_txt.height) + 15);
                this.fileDirBackground.x = ((this.file_txt.x + this.file_txt.width) + 5);
                this.fileDirBackground.y = ((this.file_txt.y + (this.file_txt.height / 2)) - 2.5);
                this.button_1.y = ((this.fileDirBackground.y + (this.fileDirBackground.height / 2)) + 22);
            } else {
                this.button_1.y = ((this.title_txt.y + this.title_txt.height) + 15);
            };
            this.button_2.y = this.button_1.y;
            if (this.onlyOneButton == false){
                this.button_1.x = ((this.title_txt.x + (this.title_txt.width / 2)) - 5);
                this.button_2.x = ((this.button_1.x + this.button_2.width) + 10);
            } else {
                this.button_1.x = ((this.title_txt.x + (this.title_txt.width / 2)) + (this.button_1.width / 2));
            };
            if (this.dislayImage){
                this.button_1.visible = false;
                this.button_2.visible = false;
                this.image.x = ((this.parent["bg"].width / 2) - (this.image.width / 2));
                this.image.y = ((this.parent["bg"].height / 2) - (this.image.height / 2));
            } else {
                this.button_1.visible = true;
                this.button_2.visible = true;
            };
        }

    }
}//package 
