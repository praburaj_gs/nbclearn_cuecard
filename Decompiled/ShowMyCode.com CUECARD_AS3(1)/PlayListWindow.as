﻿package {
    import flash.events.*;
    import flash.display.*;
    import fl.containers.*;

    public class PlayListWindow extends MovieClip {

        public var pane_mc:ScrollPane;
        private var player;
        private var playList_mc:MovieClip;
        private var AODlibrary:Object;
        public var cueCard:AS3CueCard;
        public var active:Boolean = false;
        public var open:Boolean = false;
        private var thumbnail_array:Array;
        public var nbc_logo:Sprite;
        private var pen_mc:Shape;
        private var lines_array:Array;
        private var pane:ScrollPane;

        public function PlayListWindow(_arg1, _arg2:AS3CueCard){
            this.cueCard = _arg2;
            this.playList_mc = this;
            this.player = _arg1;
            this.thumbnail_array = new Array();
            this.lines_array = new Array();
            this.pane = this.pane_mc;
            this.__setProp_pane_mc_PLAYLISTINNERCONTENT_pane_0();
        }
        public function updatePlayList():void{
            this.getPlayList();
        }
        private function arangePlayList():void{
        }
        public function setAODLib(_arg1:Object){
            this.AODlibrary = _arg1;
            this.getPlayList();
        }
        public function getPlayList(){
            this.buildPlayList(this.AODlibrary.getAllCueCards());
        }
        function deleteThumb(_arg1:PlayListThumbnail){
            var _local3:int;
            var _local4:PlayListThumbnail;
            var _local5:MovieClip;
            var _local2:String = this.deleteCueCard(_arg1.data.Asset_Id);
            if (_local2 == "SUCCESS"){
                _local3 = 0;
                while (_local3 < this.thumbnail_array.length) {
                    _local4 = this.thumbnail_array[_local3];
                    if (_local4 == _arg1){
                        _arg1.remove();
                        this.thumbnail_array.splice(_local3, 1);
                        _local5 = (this.pane.content as MovieClip);
                        _local5.removeChild(_arg1);
                        break;
                    };
                    _local3++;
                };
                this.arrangeContent();
            };
        }
        private function deleteCueCard(_arg1:String){
            var _local2:String = this.AODlibrary.deleteCueCardFromCatalogue(_arg1);
            return (_local2);
        }
        public function playVideo(_arg1){
            this.cueCard.loadCueCard(_arg1);
        }
        private function buildPlayList(_arg1){
            var _local3:Object;
            var _local2:int;
            while (_local2 < _arg1.length) {
                _local3 = _arg1[_local2];
                this.addThumbnail(_local3);
                _local2++;
            };
        }
        public function newCardAdded(_arg1:Object){
            trace("-------NEW CARD ADDED-------");
            trace(("event.getCueCard().Asset_Id = " + _arg1.getCueCard().Asset_Id));
        }
        public function addThumbnail(_arg1:Object){
            var _local2:PlayListThumbnail = new PlayListThumbnail(_arg1, this);
            var _local3:MovieClip = (this.pane.content as MovieClip);
            (_local3.addChild(_local2) as MovieClip);
            _local2.loadImage(_arg1.ThumbnailUrl);
            this.thumbnail_array.push(_local2);
        }
        private function createSeporator(_arg1:Number, _arg2:Number):Shape{
            var _local3:Shape = new DottedLine(_arg1);
            var _local4:MovieClip = (this.pane.content as MovieClip);
            (_local4.addChild(_local3) as MovieClip);
            _local3.y = _arg2;
            _local3.x = 15;
            this.lines_array.push(_local3);
            return (_local3);
        }
        private function clearSeporators(){
            var _local2:MovieClip;
            var _local1:int;
            while (_local1 < this.lines_array.length) {
                _local2 = (this.pane.content as MovieClip);
                _local2.removeChild(this.lines_array[_local1]);
                delete this.lines_array[_local1];
                _local1++;
            };
            this.lines_array = new Array();
        }
        public function arrangeContent():void{
            var _local2:PlayListThumbnail;
            var _local3:PlayListThumbnail;
            var _local4:Number;
            var _local5:Number;
            if (!this.active){
                return;
            };
            if (this.lines_array.length > 0){
                this.clearSeporators();
            };
            this.pane.setSize((this.parent["bg"].width - 5), (this.parent["bg"].height - 32));
            var _local1:int;
            while (_local1 < this.thumbnail_array.length) {
                _local2 = this.thumbnail_array[_local1];
                _local3 = this.thumbnail_array[(_local1 - 1)];
                if (_local3){
                    _local4 = (_local3.x + _local3.getWidth());
                    _local5 = (Number(_local4) + Number(_local2.getWidth()));
                    if (_local5 > this.parent["bg"].width){
                        _local2.y = ((Number(_local3.y) + Number(_local3.getHeight())) + 15);
                        this.createSeporator((this.parent["bg"].width - 50), (_local2.y - 7));
                        _local2.x = 15;
                    } else {
                        _local2.y = _local3.y;
                        _local2.x = _local4;
                    };
                } else {
                    _local2.y = 10;
                    _local2.x = 15;
                    this.createSeporator((this.parent["bg"].width - 50), (_local2.y - 7));
                };
                _local1++;
            };
            this.parent["nbc_logo"].x = this.parent["bg"].width;
            this.parent["nbc_logo"].y = this.parent["bg"].height;
            this.pane.update();
        }
        public function setText(_arg1:String){
        }
        public function removeFileDirectory(){
        }
        public function setFileDirectory(){
        }
        function __setProp_pane_mc_PLAYLISTINNERCONTENT_pane_0(){
            try {
                this.pane_mc["componentInspectorSetting"] = true;
            } catch(e:Error) {
            };
            this.pane_mc.enabled = true;
            this.pane_mc.horizontalLineScrollSize = 4;
            this.pane_mc.horizontalPageScrollSize = 0;
            this.pane_mc.horizontalScrollPolicy = "auto";
            this.pane_mc.scrollDrag = false;
            this.pane_mc.source = "EMPTY";
            this.pane_mc.verticalLineScrollSize = 4;
            this.pane_mc.verticalPageScrollSize = 0;
            this.pane_mc.verticalScrollPolicy = "auto";
            this.pane_mc.visible = true;
            try {
                this.pane_mc["componentInspectorSetting"] = false;
            } catch(e:Error) {
            };
        }

    }
}//package 
