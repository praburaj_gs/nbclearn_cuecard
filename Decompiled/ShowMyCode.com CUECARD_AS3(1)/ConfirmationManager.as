﻿package {
    import flash.events.*;
    import flash.display.*;
    import flash.text.*;

    public class ConfirmationManager extends MovieClip {

        public var main_mc:MovieClip;
        private var callBack:Function;
        private var label:String;
        private var discription:String;
        private var title_txt:TextField;
        public var button_1:MovieClip;
        private var button_2:MovieClip;
        private var buttonTxtFmt:TextFormat;
        private var mainTextFmt:TextFormat;
        private var fileTextFmt:TextFormat;
        private var _butonCallBack:Function;
        private var buttonColor:uint;
        private var _cancelFunction:Function;
        private var _cancelLabel:String;
        private var referenceObject:Object;
        private var pen_mc:Sprite;
        private var cueCard:AS3CueCard;
        private var allowPress:Boolean = false;

        public function ConfirmationManager(_arg1, _arg2:Object=null){
            this.referenceObject = _arg2;
            this.cueCard = _arg1;
            this.pen_mc = new Sprite();
            addChild(this.pen_mc);
            this.main_mc.setChildIndex(this.main_mc.bg, (this.main_mc.numChildren - 1));
            this.setChildIndex(this.main_mc, (this.numChildren - 1));
            this.visible = false;
            this.x = -75;
            this.y = 100;
            this.title_txt = new TextField();
            this.title_txt.multiline = true;
            this.title_txt.wordWrap = true;
            this.title_txt.width = 322;
            this.title_txt.autoSize = "center";
            this.title_txt.textColor = 0xFFFFFF;
            this.title_txt.antiAliasType = "normal";
            this.mainTextFmt = new TextFormat();
            this.mainTextFmt.color = 0xFFFFFF;
            this.mainTextFmt.bold = false;
            this.mainTextFmt.size = 14;
            this.mainTextFmt.font = "Arial";
            this.mainTextFmt.align = "center";
            this.button_1 = new MainMenuButton();
            this.button_2 = new MainMenuButton();
            this.buttonTxtFmt = new TextFormat();
            this.buttonTxtFmt.bold = true;
            this.buttonTxtFmt.align = "center";
            this.button_1.button_txt.setTextFormat(this.buttonTxtFmt);
            this.button_2.button_txt.setTextFormat(this.buttonTxtFmt);
            this.button_1.bg.width = 80;
            this.button_2.bg.width = 80;
            this.button_1.button_txt.x = -80;
            this.button_2.button_txt.x = -80;
            this.button_1.button_txt.width = 80;
            this.button_2.button_txt.width = 80;
            this.main_mc.addChild(this.title_txt);
            this.main_mc.addChild(this.button_1);
            this.main_mc.addChild(this.button_2);
            this.initEvents();
        }
        public function confirm(_arg1:String, _arg2:String, _arg3:Function, _arg4:Boolean=false){
            this.allowPress = false;
            this.discription = _arg1;
            this.label = _arg2;
            this.title_txt.text = this.discription;
            if (this.title_txt.width > 300){
                this.title_txt.width = 300;
            };
            this.title_txt.setTextFormat(this.mainTextFmt);
            this.title_txt.x = ((300 / 2) - (this.title_txt.width / 2));
            this.title_txt.y = ((90 / 4) - (this.title_txt.height / 2));
            this.button_1.y = ((this.title_txt.y + this.title_txt.height) + 15);
            this.button_2.y = this.button_1.y;
            if (_arg4 == true){
                this.button_2.visible = false;
                this.button_1.x = ((this.title_txt.x + (this.title_txt.width / 2)) + (this.button_1.width / 2));
            } else {
                this.button_2.visible = true;
                this.button_1.x = ((this.title_txt.x + (this.title_txt.width / 2)) - 5);
                this.button_2.x = ((this.button_1.x + this.button_2.width) + 10);
            };
            this.button_2.x = ((this.button_1.x + this.button_2.width) + 10);
            this.setButtonAtributes(_arg2, this._cancelLabel, 0x81002C, _arg3);
            this.main_mc.bg.height = (this.button_1.y + 20);
            this.visible = true;
            if (this.referenceObject){
                this.x = -229;
                this.y = -1;
                this.drawCoverRect((this.referenceObject.width + 20), (this.referenceObject.height + 32));
                this.main_mc.x = ((this.referenceObject.width / 2) - (this.main_mc.width / 2));
                this.main_mc.y = ((this.referenceObject.height / 2) - (this.main_mc.height / 2));
            };
            this.cueCard.addItemToTabOrder(this.title_txt, "");
            this.cueCard.addItemToTabOrder(this.button_1, _arg2);
            this.cueCard.addItemToTabOrder(this.button_2, this._cancelLabel);
            if (this.cueCard.fmanager.getFocus()){
                this.cueCard.fmanager.setFocus(this.title_txt);
            };
            this.allowPress = true;
        }
        private function drawCoverRect(_arg1:Number, _arg2:Number){
            this.pen_mc.graphics.clear();
            this.pen_mc.graphics.moveTo(0, 0);
            this.pen_mc.graphics.beginFill(0, 0.5);
            this.pen_mc.graphics.drawRect(0, 0, (_arg1 + 6), (_arg2 + 30));
            this.pen_mc.graphics.endFill();
        }
        public function setButtonAtributes(_arg1:String, _arg2:String, _arg3:uint, _arg4:Function=null){
            this.button_1.button_txt.text = _arg1;
            this.button_2.button_txt.text = _arg2;
            this.button_1.button_txt.setTextFormat(this.buttonTxtFmt);
            this.button_2.button_txt.setTextFormat(this.buttonTxtFmt);
            this.buttonColor = _arg3;
            Colors.changeColor(this.button_1.bg.grfx, _arg3);
            Colors.changeColor(this.button_2.bg.grfx, _arg3);
            if (_arg4 != null){
                this._butonCallBack = _arg4;
            };
        }
        public function possition(_arg1:Number, _arg2:Number){
            if (_arg1){
                this.x = _arg1;
            };
            if (_arg2){
                this.y = _arg2;
            };
        }
        private function initEvents(){
            this.button_1.addEventListener(MouseEvent.CLICK, function (_arg1:MouseEvent){
                buttonPressed();
            });
            this.button_1.addEventListener(KeyboardEvent.KEY_UP, function (_arg1:KeyboardEvent){
                if ((((_arg1.keyCode == 13)) || ((_arg1.keyCode == 32)))){
                    trace("!~ KEYBOARD CONFIRMATION BUTTON PRESSED");
                    buttonPressed();
                };
            });
            this.button_1.addEventListener(MouseEvent.ROLL_OVER, this.btnRollOver);
            this.button_1.addEventListener(MouseEvent.ROLL_OUT, this.btnRollOut);
            this.button_2.addEventListener(MouseEvent.CLICK, this.cancelPressed);
            this.button_2.addEventListener(MouseEvent.ROLL_OVER, this.btnRollOver);
            this.button_2.addEventListener(MouseEvent.ROLL_OUT, this.btnRollOut);
            this.button_2.buttonMode = true;
            this.button_2.useHandCursor = true;
            this.button_1.buttonMode = true;
            this.button_1.useHandCursor = true;
        }
        public function get butonCallBack():Function{
            return (this._butonCallBack);
        }
        public function setCancelFunction(_arg1:String, _arg2:Function):void{
            this._cancelFunction = _arg2;
            this._cancelLabel = _arg1;
        }
        private function btnRollOver(_arg1:MouseEvent){
            var _local2:MovieClip = (_arg1.currentTarget as MovieClip);
            Colors.changeColor(_local2.bg.grfx, 0x333333);
        }
        private function btnRollOut(_arg1:MouseEvent){
            var _local2:MovieClip = (_arg1.currentTarget as MovieClip);
            Colors.changeColor(_local2.bg.grfx, this.buttonColor);
        }
        private function buttonPressed(){
            if (this.allowPress == true){
                if (this._butonCallBack != null){
                    this._butonCallBack();
                } else {
                    this.cueCard.setMainFocus(this.button_1);
                };
                this.visible = false;
                this.pen_mc.graphics.clear();
            };
        }
        private function cancelPressed(_arg1:MouseEvent){
            this.visible = false;
            this.pen_mc.graphics.clear();
            if (this._cancelFunction != null){
                this._cancelFunction();
            } else {
                this.cueCard.setMainFocus(this.button_2);
            };
        }

    }
}//package 
