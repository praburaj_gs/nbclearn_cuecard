﻿package {
    import flash.events.*;
    import flash.display.*;
    import flash.net.*;

    public class MezzThumbnail extends Sprite {

        private var clipID:String;
        private var imageURL:String;
        private var imageLoader:Loader;
        public var playButton:MezzPlayButton;
        private var clickHandler:Function;
        private var request:URLRequest;
        private var referenceClip:MovieClip;
        private var mainDragBox:MovieClip;

        public function MezzThumbnail(_arg1:MovieClip, _arg2:MovieClip){
            var _referenceClip:* = _arg1;
            var _mainDragBox:* = _arg2;
            super();
            this.mainDragBox = _mainDragBox;
            this.referenceClip = _referenceClip;
            this.request = new URLRequest();
            this.imageLoader = new Loader();
            this.addChild(this.imageLoader);
            this.playButton = new MezzPlayButton();
            this.playButton.buttonMode = true;
            this.playButton.useHandCursor = true;
            this.addChild(this.playButton);
            this.playButton.addEventListener(MouseEvent.MOUSE_OVER, function (){
                playButton.gotoAndStop(2);
            });
            this.playButton.addEventListener(MouseEvent.MOUSE_OUT, function (){
                playButton.gotoAndStop(1);
            });
            this.playButton.addEventListener(MouseEvent.CLICK, this.buttonClicked);
        }
        public function loadImage(_arg1:String, _arg2:String, _arg3:Function){
            this.visible = false;
            this.clickHandler = _arg3;
            this.clipID = _arg1;
            this.imageURL = _arg2;
            this.request.url = _arg2;
            this.imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, this.failedToLoadImage);
            this.imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, this.imageLoaded);
            this.imageLoader.load(this.request);
        }
        public function arrangeContent(){
            if (this.visible){
                this.mezzArrange();
            };
        }
        public function hidePlayButton(){
            trace("Hide Play Button");
            this.playButton.visible = false;
        }
        public function showPlayButton(){
            trace("Show Play Button");
            this.playButton.visible = true;
        }
        private function imageLoaded(_arg1:Event=null){
            trace("~Mezz Loaded");
            this.mezzArrange();
            this.visible = true;
            this.mainDragBox.visible = false;
        }
        private function mezzArrange(_arg1:Event=null){
            this.imageLoader.width = this.referenceClip.width;
            this.imageLoader.height = this.referenceClip.height;
            this.playButton.x = ((this.imageLoader.width / 2) - (this.playButton.width / 2));
            this.playButton.y = ((this.imageLoader.height / 2) - (this.playButton.height / 2));
        }
        private function failedToLoadImage(_arg1:Event){
            trace("! IMAGE LOAD FAILED");
            trace(("!request.url: " + this.request.url.toString()));
        }
        private function buttonClicked(_arg1:MouseEvent){
            this.visible = false;
            this.clickHandler(this.clipID);
        }

    }
}//package 
