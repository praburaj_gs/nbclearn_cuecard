﻿package {
    import flash.geom.*;
    import fl.motion.*;

    public class Colors extends Color {

        public static function changeColor(_arg1, _arg2:uint):void{
            var _local3:ColorTransform = new ColorTransform();
            _local3.color = _arg2;
            var _local4:Transform = new Transform(_arg1);
            _local4.colorTransform = _local3;
        }
        public static function changeTint(_arg1, _arg2:uint, _arg3:Number){
            var _local4:Color = new Color();
            _local4.setTint(_arg2, _arg3);
            _arg1.transform.colorTransform = _local4;
        }

    }
}//package 
