﻿package {
    import flash.display.*;
    import flash.text.*;

    public dynamic class DownloadThumb extends MovieClip {

        public var date_txt:TextField;
        public var time_txt:TextField;
        public var fileSize_txt:TextField;
        public var title_txt:TextField;
        public var progress_mc:MovieClip;
        public var close_btn:CLOSE_BTN;

    }
}//package 
