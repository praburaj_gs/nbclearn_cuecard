﻿package fl.controls {
    import flash.events.*;

    public class RadioButtonGroup extends EventDispatcher {

        private static var groups:Object;
        private static var groupCount:uint = 0;

        protected var radioButtons:Array;
        protected var _name:String;
        protected var _selection:RadioButton;

        public function RadioButtonGroup(_arg1:String){
            _name = _arg1;
            radioButtons = [];
            registerGroup(this);
        }
        public static function getGroup(_arg1:String):RadioButtonGroup{
            if (groups == null){
                groups = {};
            };
            var _local2:RadioButtonGroup = (groups[_arg1] as RadioButtonGroup);
            if (_local2 == null){
                _local2 = new RadioButtonGroup(_arg1);
                if ((++groupCount % 20) == 0){
                    cleanUpGroups();
                };
            };
            return (_local2);
        }
        private static function registerGroup(_arg1:RadioButtonGroup):void{
            if (groups == null){
                groups = {};
            };
            groups[_arg1.name] = _arg1;
        }
        private static function cleanUpGroups():void{
            var _local1:String;
            var _local2:RadioButtonGroup;
            for (_local1 in groups) {
                _local2 = (groups[_local1] as RadioButtonGroup);
                if (_local2.radioButtons.length == 0){
                    delete groups[_local1];
                };
            };
        }

        public function getRadioButtonIndex(_arg1:RadioButton):int{
            var _local3:RadioButton;
            var _local2:int;
            while (_local2 < radioButtons.length) {
                _local3 = (radioButtons[_local2] as RadioButton);
                if (_local3 == _arg1){
                    return (_local2);
                };
                _local2++;
            };
            return (-1);
        }
        public function addRadioButton(_arg1:RadioButton):void{
            if (_arg1.groupName != name){
                _arg1.groupName = name;
                return;
            };
            radioButtons.push(_arg1);
            if (_arg1.selected){
                selection = _arg1;
            };
        }
        public function get selectedData():Object{
            var _local1:RadioButton = _selection;
            return (((_local1)==null) ? null : _local1.value);
        }
        public function removeRadioButton(_arg1:RadioButton):void{
            var _local2:int = getRadioButtonIndex(_arg1);
            if (_local2 != -1){
                radioButtons.splice(_local2, 1);
            };
            if (_selection == _arg1){
                _selection = null;
            };
        }
        public function get name():String{
            return (_name);
        }
        public function get selection():RadioButton{
            return (_selection);
        }
        public function get numRadioButtons():int{
            return (radioButtons.length);
        }
        public function set selectedData(_arg1:Object):void{
            var _local3:RadioButton;
            var _local2:int;
            while (_local2 < radioButtons.length) {
                _local3 = (radioButtons[_local2] as RadioButton);
                if (_local3.value == _arg1){
                    selection = _local3;
                    return;
                };
                _local2++;
            };
        }
        public function set selection(_arg1:RadioButton):void{
            if ((((((_selection == _arg1)) || ((_arg1 == null)))) || ((getRadioButtonIndex(_arg1) == -1)))){
                return;
            };
            _selection = _arg1;
            dispatchEvent(new Event(Event.CHANGE, true));
        }
        public function getRadioButtonAt(_arg1:int):RadioButton{
            return (RadioButton(radioButtons[_arg1]));
        }

    }
}//package fl.controls 
