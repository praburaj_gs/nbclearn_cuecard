﻿package fl.controls {
    import flash.events.*;
    import flash.display.*;
    import fl.managers.*;
    import flash.ui.*;

    public class RadioButton extends LabelButton implements IFocusManagerGroup {

        private static var defaultStyles:Object = {
            icon:null,
            upIcon:"RadioButton_upIcon",
            downIcon:"RadioButton_downIcon",
            overIcon:"RadioButton_overIcon",
            disabledIcon:"RadioButton_disabledIcon",
            selectedDisabledIcon:"RadioButton_selectedDisabledIcon",
            selectedUpIcon:"RadioButton_selectedUpIcon",
            selectedDownIcon:"RadioButton_selectedDownIcon",
            selectedOverIcon:"RadioButton_selectedOverIcon",
            focusRectSkin:null,
            focusRectPadding:null,
            textFormat:null,
            disabledTextFormat:null,
            embedFonts:null,
            textPadding:5
        };
        public static var createAccessibilityImplementation:Function;

        protected var _group:RadioButtonGroup;
        protected var defaultGroupName:String = "RadioButtonGroup";
        protected var _value:Object;

        public function RadioButton(){
            mode = "border";
            groupName = defaultGroupName;
        }
        public static function getStyleDefinition():Object{
            return (defaultStyles);
        }

        override protected function draw():void{
            super.draw();
        }
        override public function set selected(_arg1:Boolean):void{
            if ((((_arg1 == false)) || (selected))){
                return;
            };
            if (_group != null){
                _group.selection = this;
            } else {
                super.selected = _arg1;
            };
        }
        override protected function drawLayout():void{
            super.drawLayout();
            var _local1:Number = Number(getStyleValue("textPadding"));
            switch (_labelPlacement){
                case ButtonLabelPlacement.RIGHT:
                    icon.x = _local1;
                    textField.x = (icon.x + (icon.width + _local1));
                    background.width = ((textField.x + textField.width) + _local1);
                    background.height = (Math.max(textField.height, icon.height) + (_local1 * 2));
                    break;
                case ButtonLabelPlacement.LEFT:
                    icon.x = ((width - icon.width) - _local1);
                    textField.x = (((width - icon.width) - (_local1 * 2)) - textField.width);
                    background.width = ((textField.width + icon.width) + (_local1 * 3));
                    background.height = (Math.max(textField.height, icon.height) + (_local1 * 2));
                    break;
                case ButtonLabelPlacement.TOP:
                case ButtonLabelPlacement.BOTTOM:
                    background.width = (Math.max(textField.width, icon.width) + (_local1 * 2));
                    background.height = ((textField.height + icon.height) + (_local1 * 3));
                    break;
            };
            background.x = Math.min((icon.x - _local1), (textField.x - _local1));
            background.y = Math.min((icon.y - _local1), (textField.y - _local1));
        }
        protected function handleClick(_arg1:MouseEvent):void{
            if (_group == null){
                return;
            };
            _group.dispatchEvent(new MouseEvent(MouseEvent.CLICK, true));
        }
        protected function handleChange(_arg1:Event):void{
            super.selected = (_group.selection == this);
            dispatchEvent(new Event(Event.CHANGE, true));
        }
        public function set groupName(_arg1:String):void{
            if (_group != null){
                _group.removeRadioButton(this);
                _group.removeEventListener(Event.CHANGE, handleChange);
            };
            _group = ((_arg1)==null) ? null : RadioButtonGroup.getGroup(_arg1);
            if (_group != null){
                _group.addRadioButton(this);
                _group.addEventListener(Event.CHANGE, handleChange, false, 0, true);
            };
        }
        override protected function configUI():void{
            super.configUI();
            super.toggle = true;
            var _local1:Shape = new Shape();
            var _local2:Graphics = _local1.graphics;
            _local2.beginFill(0, 0);
            _local2.drawRect(0, 0, 100, 100);
            _local2.endFill();
            background = (_local1 as DisplayObject);
            addChildAt(background, 0);
            addEventListener(MouseEvent.CLICK, handleClick, false, 0, true);
        }
        override protected function keyUpHandler(_arg1:KeyboardEvent):void{
            super.keyUpHandler(_arg1);
            if ((((_arg1.keyCode == Keyboard.SPACE)) && (!(_toggle)))){
                _toggle = true;
            };
        }
        override protected function drawBackground():void{
        }
        override public function get selected():Boolean{
            return (super.selected);
        }
        override protected function initializeAccessibility():void{
            if (RadioButton.createAccessibilityImplementation != null){
                RadioButton.createAccessibilityImplementation(this);
            };
        }
        private function setThis():void{
            var _local1:RadioButtonGroup = _group;
            if (_local1 != null){
                if (_local1.selection != this){
                    _local1.selection = this;
                };
            } else {
                super.selected = true;
            };
        }
        public function set value(_arg1:Object):void{
            _value = _arg1;
        }
        override public function set autoRepeat(_arg1:Boolean):void{
        }
        override public function set toggle(_arg1:Boolean):void{
            throw (new Error("Warning: You cannot change a RadioButtons toggle."));
        }
        override public function get autoRepeat():Boolean{
            return (false);
        }
        override protected function keyDownHandler(_arg1:KeyboardEvent):void{
            switch (_arg1.keyCode){
                case Keyboard.DOWN:
                    setNext(!(_arg1.ctrlKey));
                    _arg1.stopPropagation();
                    break;
                case Keyboard.UP:
                    setPrev(!(_arg1.ctrlKey));
                    _arg1.stopPropagation();
                    break;
                case Keyboard.LEFT:
                    setPrev(!(_arg1.ctrlKey));
                    _arg1.stopPropagation();
                    break;
                case Keyboard.RIGHT:
                    setNext(!(_arg1.ctrlKey));
                    _arg1.stopPropagation();
                    break;
                case Keyboard.SPACE:
                    setThis();
                    _toggle = false;
                default:
                    super.keyDownHandler(_arg1);
            };
        }
        override public function drawFocus(_arg1:Boolean):void{
            var _local2:Number;
            super.drawFocus(_arg1);
            if (_arg1){
                _local2 = Number(getStyleValue("focusRectPadding"));
                uiFocusRect.x = (background.x - _local2);
                uiFocusRect.y = (background.y - _local2);
                uiFocusRect.width = (background.width + (_local2 * 2));
                uiFocusRect.height = (background.height + (_local2 * 2));
            };
        }
        private function setPrev(_arg1:Boolean=true):void{
            var _local6:*;
            var _local2:RadioButtonGroup = _group;
            if (_local2 == null){
                return;
            };
            var _local3:IFocusManager = focusManager;
            if (_local3){
                _local3.showFocusIndicator = true;
            };
            var _local4:int = _local2.getRadioButtonIndex(this);
            var _local5:int = _local4;
            if (_local4 != -1){
                do  {
                    --_local5;
                    _local5 = ((_local5)==-1) ? (_local2.numRadioButtons - 1) : _local5;
                    _local6 = _local2.getRadioButtonAt(_local5);
                    if (((_local6) && (_local6.enabled))){
                        if (_arg1){
                            _local2.selection = _local6;
                        };
                        _local6.setFocus();
                        return;
                    };
                    if (((_arg1) && (!((_local2.getRadioButtonAt(_local5) == _local2.selection))))){
                        _local2.selection = this;
                    };
                    this.drawFocus(true);
                } while (_local5 != _local4);
            };
        }
        override public function get toggle():Boolean{
            return (true);
        }
        public function get value():Object{
            return (_value);
        }
        public function get groupName():String{
            return (((_group)==null) ? null : _group.name);
        }
        private function setNext(_arg1:Boolean=true):void{
            var _local7:*;
            var _local2:RadioButtonGroup = _group;
            if (_local2 == null){
                return;
            };
            var _local3:IFocusManager = focusManager;
            if (_local3){
                _local3.showFocusIndicator = true;
            };
            var _local4:int = _local2.getRadioButtonIndex(this);
            var _local5:Number = _local2.numRadioButtons;
            var _local6:int = _local4;
            if (_local4 != -1){
                do  {
                    _local6++;
                    _local6 = ((_local6)>(_local2.numRadioButtons - 1)) ? 0 : _local6;
                    _local7 = _local2.getRadioButtonAt(_local6);
                    if (((_local7) && (_local7.enabled))){
                        if (_arg1){
                            _local2.selection = _local7;
                        };
                        _local7.setFocus();
                        return;
                    };
                    if (((_arg1) && (!((_local2.getRadioButtonAt(_local6) == _local2.selection))))){
                        _local2.selection = this;
                    };
                    this.drawFocus(true);
                } while (_local6 != _local4);
            };
        }
        public function set group(_arg1:RadioButtonGroup):void{
            groupName = _arg1.name;
        }
        public function get group():RadioButtonGroup{
            return (_group);
        }

    }
}//package fl.controls 
