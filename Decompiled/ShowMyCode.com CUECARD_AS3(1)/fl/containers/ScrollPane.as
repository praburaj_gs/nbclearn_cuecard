﻿package fl.containers {
    import flash.events.*;
    import flash.display.*;
    import fl.controls.*;
    import fl.core.*;
    import flash.net.*;
    import flash.system.*;
    import fl.managers.*;
    import fl.events.*;
    import flash.ui.*;

    public class ScrollPane extends BaseScrollPane implements IFocusManagerComponent {

        private static var defaultStyles:Object = {
            upSkin:"ScrollPane_upSkin",
            disabledSkin:"ScrollPane_disabledSkin",
            focusRectSkin:null,
            focusRectPadding:null,
            contentPadding:0
        };

        protected var scrollDragHPos:Number;
        protected var loader:Loader;
        protected var xOffset:Number;
        protected var _source:Object = "";
        protected var scrollDragVPos:Number;
        protected var _scrollDrag:Boolean = false;
        protected var currentContent:Object;
        protected var contentClip:Sprite;
        protected var yOffset:Number;

        public static function getStyleDefinition():Object{
            return (mergeStyles(defaultStyles, BaseScrollPane.getStyleDefinition()));
        }

        protected function clearContent():void{
            if (contentClip.numChildren == 0){
                return;
            };
            contentClip.removeChildAt(0);
            currentContent = null;
            if (loader != null){
                try {
                    loader.close();
                } catch(e) {
                };
                try {
                    loader.unload();
                } catch(e) {
                };
                loader = null;
            };
        }
        protected function passEvent(_arg1:Event):void{
            dispatchEvent(_arg1);
        }
        protected function calculateAvailableHeight():Number{
            var _local1:Number = Number(getStyleValue("contentPadding"));
            return (((height - (_local1 * 2)) - (((((_horizontalScrollPolicy == ScrollPolicy.ON)) || ((((_horizontalScrollPolicy == ScrollPolicy.AUTO)) && ((_maxHorizontalScrollPosition > 0)))))) ? 15 : 0)));
        }
        override protected function drawLayout():void{
            super.drawLayout();
            contentScrollRect = contentClip.scrollRect;
            contentScrollRect.width = availableWidth;
            contentScrollRect.height = availableHeight;
            contentClip.cacheAsBitmap = useBitmapScrolling;
            contentClip.scrollRect = contentScrollRect;
            contentClip.x = (contentClip.y = contentPadding);
        }
        public function get bytesTotal():Number{
            return ((((((loader == null)) || ((loader.contentLoaderInfo == null)))) ? 0 : loader.contentLoaderInfo.bytesTotal));
        }
        public function get source():Object{
            return (_source);
        }
        override protected function handleScroll(_arg1:ScrollEvent):void{
            passEvent(_arg1);
            super.handleScroll(_arg1);
        }
        protected function onContentLoad(_arg1:Event):void{
            update();
            var _local2:* = calculateAvailableHeight();
            calculateAvailableSize();
            horizontalScrollBar.setScrollProperties(availableWidth, 0, ((useFixedHorizontalScrolling) ? _maxHorizontalScrollPosition : (contentWidth - availableWidth)), availableWidth);
            verticalScrollBar.setScrollProperties(_local2, 0, (contentHeight - _local2), _local2);
            passEvent(_arg1);
        }
        public function get scrollDrag():Boolean{
            return (_scrollDrag);
        }
        protected function setScrollDrag():void{
            if (_scrollDrag){
                contentClip.addEventListener(MouseEvent.MOUSE_DOWN, doStartDrag, false, 0, true);
                stage.addEventListener(MouseEvent.MOUSE_UP, endDrag, false, 0, true);
            } else {
                contentClip.removeEventListener(MouseEvent.MOUSE_DOWN, doStartDrag);
                stage.removeEventListener(MouseEvent.MOUSE_UP, endDrag);
                removeEventListener(MouseEvent.MOUSE_MOVE, doDrag);
            };
            contentClip.buttonMode = _scrollDrag;
        }
        public function get percentLoaded():Number{
            if (loader != null){
                return (Math.round(((bytesLoaded / bytesTotal) * 100)));
            };
            return (0);
        }
        override protected function setVerticalScrollPosition(_arg1:Number, _arg2:Boolean=false):void{
            var _local3:* = contentClip.scrollRect;
            _local3.y = _arg1;
            contentClip.scrollRect = _local3;
        }
        protected function endDrag(_arg1:MouseEvent):void{
            stage.removeEventListener(MouseEvent.MOUSE_MOVE, doDrag);
        }
        override protected function drawBackground():void{
            var _local1:DisplayObject = background;
            background = getDisplayObjectInstance(getStyleValue(((enabled) ? "upSkin" : "disabledSkin")));
            background.width = width;
            background.height = height;
            addChildAt(background, 0);
            if (((!((_local1 == null))) && (!((_local1 == background))))){
                removeChild(_local1);
            };
        }
        public function set source(_arg1:Object):void{
            var _local2:*;
            clearContent();
            if (isLivePreview){
                return;
            };
            _source = _arg1;
            if ((((_source == "")) || ((_source == null)))){
                return;
            };
            currentContent = getDisplayObjectInstance(_arg1);
            if (currentContent != null){
                _local2 = contentClip.addChild((currentContent as DisplayObject));
                dispatchEvent(new Event(Event.INIT));
                update();
            } else {
                load(new URLRequest(_source.toString()));
            };
        }
        public function set scrollDrag(_arg1:Boolean):void{
            _scrollDrag = _arg1;
            invalidate(InvalidationType.STATE);
        }
        protected function initLoader():void{
            loader = new Loader();
            loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleError, false, 0, true);
            loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleError, false, 0, true);
            loader.contentLoaderInfo.addEventListener(Event.OPEN, passEvent, false, 0, true);
            loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, passEvent, false, 0, true);
            loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onContentLoad, false, 0, true);
            loader.contentLoaderInfo.addEventListener(Event.INIT, passEvent, false, 0, true);
            loader.contentLoaderInfo.addEventListener(HTTPStatusEvent.HTTP_STATUS, passEvent, false, 0, true);
            contentClip.addChild(loader);
        }
        override protected function draw():void{
            if (isInvalid(InvalidationType.STYLES)){
                drawBackground();
            };
            if (isInvalid(InvalidationType.STATE)){
                setScrollDrag();
            };
            super.draw();
        }
        protected function clearLoadEvents():void{
            loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handleError);
            loader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handleError);
            loader.contentLoaderInfo.removeEventListener(Event.OPEN, passEvent);
            loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, passEvent);
            loader.contentLoaderInfo.removeEventListener(HTTPStatusEvent.HTTP_STATUS, passEvent);
            loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onContentLoad);
        }
        protected function handleError(_arg1:Event):void{
            passEvent(_arg1);
            clearLoadEvents();
            loader.contentLoaderInfo.removeEventListener(Event.INIT, handleInit);
        }
        public function get bytesLoaded():Number{
            return ((((((loader == null)) || ((loader.contentLoaderInfo == null)))) ? 0 : loader.contentLoaderInfo.bytesLoaded));
        }
        override protected function setHorizontalScrollPosition(_arg1:Number, _arg2:Boolean=false):void{
            var _local3:* = contentClip.scrollRect;
            _local3.x = _arg1;
            contentClip.scrollRect = _local3;
        }
        override protected function configUI():void{
            super.configUI();
            contentClip = new Sprite();
            addChild(contentClip);
            contentClip.scrollRect = contentScrollRect;
            _horizontalScrollPolicy = ScrollPolicy.AUTO;
            _verticalScrollPolicy = ScrollPolicy.AUTO;
        }
        protected function handleInit(_arg1:Event):void{
            loader.contentLoaderInfo.removeEventListener(Event.INIT, handleInit);
            passEvent(_arg1);
            invalidate(InvalidationType.SIZE);
        }
        public function update():void{
            var _local1:DisplayObject = contentClip.getChildAt(0);
            setContentSize(_local1.width, _local1.height);
        }
        public function refreshPane():void{
            if ((_source is URLRequest)){
                _source = _source.url;
            };
            source = _source;
        }
        public function load(_arg1:URLRequest, _arg2:LoaderContext=null):void{
            if (_arg2 == null){
                _arg2 = new LoaderContext(false, ApplicationDomain.currentDomain);
            };
            clearContent();
            initLoader();
            currentContent = (_source = _arg1);
            loader.load(_arg1, _arg2);
        }
        protected function doStartDrag(_arg1:MouseEvent):void{
            if (!enabled){
                return;
            };
            xOffset = mouseX;
            yOffset = mouseY;
            scrollDragHPos = horizontalScrollPosition;
            scrollDragVPos = verticalScrollPosition;
            stage.addEventListener(MouseEvent.MOUSE_MOVE, doDrag, false, 0, true);
        }
        protected function doDrag(_arg1:MouseEvent):void{
            var _local2:* = (scrollDragVPos - (mouseY - yOffset));
            _verticalScrollBar.setScrollPosition(_local2);
            setVerticalScrollPosition(_verticalScrollBar.scrollPosition, true);
            var _local3:* = (scrollDragHPos - (mouseX - xOffset));
            _horizontalScrollBar.setScrollPosition(_local3);
            setHorizontalScrollPosition(_horizontalScrollBar.scrollPosition, true);
        }
        override protected function keyDownHandler(_arg1:KeyboardEvent):void{
            var _local2:int = calculateAvailableHeight();
            switch (_arg1.keyCode){
                case Keyboard.DOWN:
                    verticalScrollPosition++;
                    break;
                case Keyboard.UP:
                    verticalScrollPosition--;
                    break;
                case Keyboard.RIGHT:
                    horizontalScrollPosition++;
                    break;
                case Keyboard.LEFT:
                    horizontalScrollPosition--;
                    break;
                case Keyboard.END:
                    verticalScrollPosition = maxVerticalScrollPosition;
                    break;
                case Keyboard.HOME:
                    verticalScrollPosition = 0;
                    break;
                case Keyboard.PAGE_UP:
                    verticalScrollPosition = (verticalScrollPosition - _local2);
                    break;
                case Keyboard.PAGE_DOWN:
                    verticalScrollPosition = (verticalScrollPosition + _local2);
                    break;
            };
        }
        public function get content():DisplayObject{
            var _local1:Object = currentContent;
            if ((_local1 is URLRequest)){
                _local1 = loader.content;
            };
            return ((_local1 as DisplayObject));
        }

    }
}//package fl.containers 
