﻿package org.osmf.metadata {

    public class CuePoint extends TimelineMarker {

        public static const EMBEDDED_CUEPOINTS_NAMESPACE:String = "http://www.osmf.org/timeline/embeddedCuePoints/1.0";
        public static const DYNAMIC_CUEPOINTS_NAMESPACE:String = "http://www.osmf.org/timeline/dynamicCuePoints/1.0";

        private var _name:String;
        private var _type:String;
        private var _parameters:Object;

        public function CuePoint(_arg1:String, _arg2:Number, _arg3:String, _arg4:Object, _arg5:Number=NaN){
            super(_arg2, _arg5);
            this._type = _arg1;
            this._name = _arg3;
            this._parameters = _arg4;
        }
        public function get type():String{
            return (this._type);
        }
        public function get name():String{
            return (this._name);
        }
        public function get parameters():Object{
            return (this._parameters);
        }

    }
}//package org.osmf.metadata 
