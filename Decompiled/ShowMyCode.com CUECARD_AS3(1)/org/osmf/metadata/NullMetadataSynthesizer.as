﻿package org.osmf.metadata {
    import __AS3__.vec.*;

    public class NullMetadataSynthesizer extends MetadataSynthesizer {

        override public function synthesize(_arg1:String, _arg2:Metadata, _arg3:Vector.<Metadata>, _arg4:String, _arg5:Metadata):Metadata{
            return (null);
        }

    }
}//package org.osmf.metadata 
