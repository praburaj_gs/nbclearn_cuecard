﻿package org.osmf.metadata {

    public final class MetadataNamespaces {

        public static const MEDIA_ELEMENT_METADATA:String = "http://www.osmf.org/mediaElement/1.0";
        public static const DERIVED_RESOURCE_METADATA:String = "http://www.osmf.org/derivedResource/1.0";
        public static const FMS_SERVER_VERSION_METADATA:String = "http://www.osmf.org/fmsServerVersion/1.0";
        public static const ELEMENT_ID:String = "http://www.osmf.org/layout/elementId/1.0";
        public static const LAYOUT_RENDERER_TYPE:String = "http://www.osmf.org/layout/renderer_type/1.0";
        public static const ABSOLUTE_LAYOUT_PARAMETERS:String = "http://www.osmf.org/layout/absolute/1.0";
        public static const RELATIVE_LAYOUT_PARAMETERS:String = "http://www.osmf.org/layout/relative/1.0";
        public static const ANCHOR_LAYOUT_PARAMETERS:String = "http://www.osmf.org/layout/anchor/1.0";
        public static const PADDING_LAYOUT_PARAMETERS:String = "http://www.osmf.org/layout/padding/1.0";
        public static const LAYOUT_ATTRIBUTES:String = "http://www.osmf.org/layout/attributes/1.0";
        public static const OVERLAY_LAYOUT_PARAMETERS:String = "http://www.osmf.org/layout/overlay/1.0";
        public static const BOX_LAYOUT_ATTRIBUTES:String = "http://www.osmf.org/layout/attributes/box/1.0";
        public static const DRM_METADATA:String = "http://www.osmf.org/drm/1.0";
        public static const MULTICAST_INFO:String = "http://www.osmf.org/multicast/info/1.0";
        public static const MULTICAST_NET_LOADER:String = "http://www.osmf.org/multicast/netloader/1.0";
        public static const DVR_METADATA:String = "http://www.osmf.org/dvr/1.0";
        public static const DRM_ADDITIONAL_HEADER_KEY:String = "DRMAdditionalHeader";
        public static const HTTP_STREAMING_METADATA:String = "http://www.osmf.org/httpstreaming/1.0";
        public static const HTTP_STREAMING_BOOTSTRAP_KEY:String = "bootstrap";
        public static const HTTP_STREAMING_STREAM_METADATA_KEY:String = "streamMetadata";
        public static const HTTP_STREAMING_XMP_METADATA_KEY:String = "xmpMetadata";
        public static const HTTP_STREAMING_SERVER_BASE_URLS_KEY:String = "serverBaseUrls";
        public static const HTTP_STREAMING_DVR_BEGIN_OFFSET_KEY:String = "beginOffset";
        public static const HTTP_STREAMING_DVR_END_OFFSET_KEY:String = "endOffset";
        public static const HTTP_STREAMING_DVR_WINDOW_DURATION_KEY:String = "windowDuration";
        public static const HTTP_STREAMING_DVR_OFFLINE_KEY:String = "dvrOffline";
        public static const HTTP_STREAMING_DVR_ID_KEY:String = "dvrId";
        public static const RESOURCE_INITIAL_INDEX:String = "resourceInitialIndex";
        public static const BEST_EFFORT_FETCH_METADATA:String = "http://www.osmf.org/bestEffortFetch/1.0";
        public static const HTTP_STREAMING_BEST_EFFORT_FETCH_MAX_FORWARD_FETCHES:String = "bestEffortFetchMaxForwardFetches";
        public static const HTTP_STREAMING_BEST_EFFORT_FETCH_MAX_BACKWARD_FETCHES:String = "bestEffortFetchMaxBackwardFetches";
        public static const HTTP_STREAMING_BEST_EFFORT_FETCH_SEGMENT_DURATION:String = "bestEffortFetchSegmentDuration";
        public static const HTTP_STREAMING_BEST_EFFORT_FETCH_FRAGMENT_DURATION:String = "bestEffortFetchFragmentDuration";

    }
}//package org.osmf.metadata 
