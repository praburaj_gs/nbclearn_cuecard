﻿package org.osmf.metadata {
    import org.osmf.events.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class MetadataWatcher {

        private var parentMetadata:Metadata;
        private var namespaceURL:String;
        private var key:String;
        private var callback:Function;
        private var currentMetadata:Metadata;
        private var watching:Boolean;

        public function MetadataWatcher(_arg1:Metadata, _arg2:String, _arg3:String, _arg4:Function){
            if ((((((_arg1 == null)) || ((_arg2 == null)))) || ((_arg4 == null)))){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            this.parentMetadata = _arg1;
            this.namespaceURL = _arg2;
            this.key = _arg3;
            this.callback = _arg4;
        }
        public function watch(_arg1:Boolean=true):void{
            if (this.watching == false){
                this.watching = true;
                this.parentMetadata.addEventListener(MetadataEvent.VALUE_ADD, this.onMetadataAdd, false, 0, true);
                this.processWatchedMetadataChange((this.parentMetadata.getValue(this.namespaceURL) as Metadata));
                if (_arg1 == true){
                    if (this.key != null){
                        this.callback(((this.currentMetadata) ? this.currentMetadata.getValue(this.key) : undefined));
                    } else {
                        this.callback(((this.currentMetadata) ? this.currentMetadata : undefined));
                    };
                };
            };
        }
        public function unwatch():void{
            if (this.watching == true){
                this.processWatchedMetadataChange(null, false);
                this.parentMetadata.removeEventListener(MetadataEvent.VALUE_ADD, this.onMetadataAdd);
                this.watching = false;
            };
        }
        private function processWatchedMetadataChange(_arg1:Metadata, _arg2:Boolean=true):void{
            var _local3:Metadata;
            if (this.currentMetadata != _arg1){
                _local3 = this.currentMetadata;
                if (this.currentMetadata){
                    this.currentMetadata.removeEventListener(MetadataEvent.VALUE_CHANGE, this.onValueChange);
                    this.currentMetadata.removeEventListener(MetadataEvent.VALUE_ADD, this.onValueAdd);
                    this.currentMetadata.removeEventListener(MetadataEvent.VALUE_REMOVE, this.onValueRemove);
                    this.parentMetadata.removeEventListener(MetadataEvent.VALUE_REMOVE, this.onMetadataRemove);
                } else {
                    this.parentMetadata.removeEventListener(MetadataEvent.VALUE_ADD, this.onMetadataAdd);
                };
                this.currentMetadata = _arg1;
                if (_arg1){
                    _arg1.addEventListener(MetadataEvent.VALUE_CHANGE, this.onValueChange, false, 0, true);
                    _arg1.addEventListener(MetadataEvent.VALUE_ADD, this.onValueAdd, false, 0, true);
                    _arg1.addEventListener(MetadataEvent.VALUE_REMOVE, this.onValueRemove, false, 0, true);
                    this.parentMetadata.addEventListener(MetadataEvent.VALUE_REMOVE, this.onMetadataRemove);
                } else {
                    this.parentMetadata.addEventListener(MetadataEvent.VALUE_ADD, this.onMetadataAdd);
                };
            };
        }
        private function onMetadataAdd(_arg1:MetadataEvent):void{
            var _local2:Metadata = (_arg1.value as Metadata);
            if (((_local2) && ((_arg1.key == this.namespaceURL)))){
                this.processWatchedMetadataChange(_local2);
                if (this.key == null){
                    this.callback(_local2);
                } else {
                    this.callback(_local2.getValue(this.key));
                };
            };
        }
        private function onMetadataRemove(_arg1:MetadataEvent):void{
            var _local2:Metadata = (_arg1.value as Metadata);
            if (((_local2) && ((_arg1.key == this.namespaceURL)))){
                this.processWatchedMetadataChange(null);
                this.callback(undefined);
            };
        }
        private function onValueChange(_arg1:MetadataEvent):void{
            if (this.key){
                if (this.key == _arg1.key){
                    this.callback(_arg1.value);
                };
            } else {
                this.callback((_arg1.target as Metadata));
            };
        }
        private function onValueAdd(_arg1:MetadataEvent):void{
            if (this.key){
                if (this.key == _arg1.key){
                    this.callback(_arg1.value);
                };
            } else {
                this.callback((_arg1.target as Metadata));
            };
        }
        private function onValueRemove(_arg1:MetadataEvent):void{
            if (this.key){
                if (this.key == _arg1.key){
                    this.callback(undefined);
                };
            } else {
                this.callback((_arg1.target as Metadata));
            };
        }

    }
}//package org.osmf.metadata 
