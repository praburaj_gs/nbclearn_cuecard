﻿package org.osmf.metadata {

    public class TimelineMarker {

        private var _time:Number;
        private var _duration:Number;

        public function TimelineMarker(_arg1:Number, _arg2:Number=NaN){
            this._time = _arg1;
            this._duration = _arg2;
        }
        public function get time():Number{
            return (this._time);
        }
        public function get duration():Number{
            return (this._duration);
        }

    }
}//package org.osmf.metadata 
