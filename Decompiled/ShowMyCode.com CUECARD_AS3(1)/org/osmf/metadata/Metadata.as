﻿package org.osmf.metadata {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.utils.*;

    public class Metadata extends EventDispatcher {

        private var data:Dictionary;

        public function getValue(_arg1:String){
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            return (((this.data)!=null) ? this.data[_arg1] : null);
        }
        public function addValue(_arg1:String, _arg2:Object):void{
            var _local4:Event;
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            if (this.data == null){
                this.data = new Dictionary();
            };
            var _local3:* = this.data[_arg1];
            this.data[_arg1] = _arg2;
            if (_local3 != _arg2){
                _local4 = (((_local3 === undefined)) ? new MetadataEvent(MetadataEvent.VALUE_ADD, false, false, _arg1, _arg2) : new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, _arg1, _arg2, _local3));
                dispatchEvent(_local4);
            };
        }
        public function removeValue(_arg1:String){
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            var _local2:* = this.data[_arg1];
            if (_local2 !== undefined){
                delete this.data[_arg1];
                dispatchEvent(new MetadataEvent(MetadataEvent.VALUE_REMOVE, false, false, _arg1, _local2));
            };
            return (_local2);
        }
        public function get keys():Vector.<String>{
            var _local2:Object;
            var _local1:Vector.<String> = new Vector.<String>();
            if (this.data != null){
                for (_local2 in this.data) {
                    _local1.push(_local2);
                };
            };
            return (_local1);
        }
        public function get synthesizer():MetadataSynthesizer{
            return (null);
        }

    }
}//package org.osmf.metadata 
