﻿package org.osmf.metadata {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.utils.*;

    public class TimelineMetadata extends Metadata {

        private static const CHECK_INTERVAL:Number = 100;
        private static const TOLERANCE:Number = 0.25;

        private var temporalKeyCollection:Vector.<Number>;
        private var temporalValueCollection:Vector.<TimelineMarker>;
        private var media:MediaElement;
        private var timeTrait:TimeTrait;
        private var seekTrait:SeekTrait;
        private var playTrait:PlayTrait;
        private var lastFiredTemporalMetadataIndex:int;
        private var intervalTimer:Timer;
        private var restartTimer:Boolean;
        private var _enabled:Boolean;
        private var durationTimers:Dictionary;

        public function TimelineMetadata(_arg1:MediaElement){
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            this.media = _arg1;
            this._enabled = true;
            this.intervalTimer = new Timer(CHECK_INTERVAL);
            this.intervalTimer.addEventListener(TimerEvent.TIMER, this.onIntervalTimer);
            this.timeTrait = (_arg1.getTrait(MediaTraitType.TIME) as TimeTrait);
            this.seekTrait = (_arg1.getTrait(MediaTraitType.SEEK) as SeekTrait);
            this.setupTraitEventListener(MediaTraitType.SEEK);
            this.playTrait = (_arg1.getTrait(MediaTraitType.PLAY) as PlayTrait);
            this.setupTraitEventListener(MediaTraitType.PLAY);
            _arg1.addEventListener(MediaElementEvent.TRAIT_ADD, this.onTraitAdd);
            _arg1.addEventListener(MediaElementEvent.TRAIT_REMOVE, this.onTraitRemove);
        }
        public function get numMarkers():int{
            return (((this.temporalValueCollection) ? this.temporalValueCollection.length : 0));
        }
        public function getMarkerAt(_arg1:int):TimelineMarker{
            if ((((((_arg1 >= 0)) && (!((this.temporalValueCollection == null))))) && ((_arg1 < this.temporalValueCollection.length)))){
                return (this.temporalValueCollection[_arg1]);
            };
            return (null);
        }
        public function addMarker(_arg1:TimelineMarker):void{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            this.addValue(("" + _arg1.time), _arg1);
        }
        public function removeMarker(_arg1:TimelineMarker):TimelineMarker{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            return (this.removeValue(("" + _arg1.time)));
        }
        override public function addValue(_arg1:String, _arg2:Object):void{
            var _local5:int;
            var _local3:Number = new Number(_arg1);
            var _local4:TimelineMarker = (_arg2 as TimelineMarker);
            if ((((((((_arg1 == null)) || (isNaN(_local3)))) || ((_local3 < 0)))) || ((_local4 == null)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            if (this.temporalValueCollection == null){
                this.temporalKeyCollection = new Vector.<Number>();
                this.temporalKeyCollection.push(_local3);
                this.temporalValueCollection = new Vector.<TimelineMarker>();
                this.temporalValueCollection.push(_arg2);
            } else {
                _local5 = this.findTemporalMetadata(0, (this.temporalValueCollection.length - 1), _local3);
                if (_local5 < 0){
                    _local5 = (_local5 * -1);
                    this.temporalKeyCollection.splice(_local5, 0, _local3);
                    this.temporalValueCollection.splice(_local5, 0, _local4);
                } else {
                    if ((((_local5 == 0)) && (!((_local3 == this.temporalKeyCollection[0]))))){
                        this.temporalKeyCollection.splice(_local5, 0, _local3);
                        this.temporalValueCollection.splice(_local5, 0, _local4);
                    } else {
                        this.temporalKeyCollection[_local5] = _local3;
                        this.temporalValueCollection[_local5] = _local4;
                    };
                };
            };
            this.enabled = true;
            dispatchEvent(new MetadataEvent(MetadataEvent.VALUE_ADD, false, false, _arg1, _local4));
            dispatchEvent(new TimelineMetadataEvent(TimelineMetadataEvent.MARKER_ADD, false, false, _local4));
        }
        override public function removeValue(_arg1:String){
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            var _local2:Number = new Number(_arg1);
            var _local3:* = null;
            var _local4:int = ((this.temporalValueCollection) ? this.findTemporalMetadata(0, (this.temporalValueCollection.length - 1), _local2) : -1);
            if (_local4 >= 0){
                this.temporalKeyCollection.splice(_local4, 1);
                _local3 = this.temporalValueCollection.splice(_local4, 1)[0];
                if (this.temporalValueCollection.length == 0){
                    this.reset(false);
                    this.temporalValueCollection = null;
                    this.temporalKeyCollection = null;
                };
                dispatchEvent(new MetadataEvent(MetadataEvent.VALUE_REMOVE, false, false, _arg1, _local3));
                dispatchEvent(new TimelineMetadataEvent(TimelineMetadataEvent.MARKER_REMOVE, false, false, (_local3 as TimelineMarker)));
            };
            return (_local3);
        }
        override public function getValue(_arg1:String){
            var _local3:int;
            var _local4:Number;
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            var _local2:Number = new Number(_arg1);
            if (!isNaN(_local2)){
                _local3 = 0;
                while (_local3 < this.temporalKeyCollection.length) {
                    _local4 = this.temporalKeyCollection[_local3];
                    if (_local4 == _local2){
                        return (this.temporalValueCollection[_local3]);
                    };
                    _local3++;
                };
            };
            return (null);
        }
        public function get enabled():Boolean{
            return (this._enabled);
        }
        public function set enabled(_arg1:Boolean):void{
            this._enabled = _arg1;
            this.reset(_arg1);
        }
        private function startTimer(_arg1:Boolean=true):void{
            if (!_arg1){
                this.intervalTimer.stop();
            } else {
                if (((((((((((!((this.timeTrait == null))) && (!((this.temporalValueCollection == null))))) && ((this.temporalValueCollection.length > 0)))) && (this.restartTimer))) && (this.enabled))) && (!(this.intervalTimer.running)))){
                    if (((!((this.playTrait == null))) && ((this.playTrait.playState == PlayState.PLAYING)))){
                        this.intervalTimer.start();
                    };
                };
            };
        }
        private function reset(_arg1:Boolean):void{
            this.lastFiredTemporalMetadataIndex = -1;
            this.restartTimer = true;
            this.intervalTimer.reset();
            this.intervalTimer.delay = CHECK_INTERVAL;
            if (_arg1){
                this.startTimer();
            };
        }
        private function checkForTemporalMetadata():void{
            var _local1:Number = this.timeTrait.currentTime;
            var _local2:int = this.findTemporalMetadata((this.lastFiredTemporalMetadataIndex + 1), (this.temporalValueCollection.length - 1), _local1);
            if (_local2 <= 0){
                _local2 = (_local2 * -1);
                _local2 = ((_local2)>0) ? (_local2 - 1) : 0;
            };
            if (((!(this.checkTemporalMetadata(_local2, _local1))) && (((_local2 + 1) < this.temporalValueCollection.length)))){
                this.checkTemporalMetadata((_local2 + 1), _local1);
            };
        }
        private function setupTraitEventListener(_arg1:String, _arg2:Boolean=true):void{
            var _local3:PlayEvent;
            if (_arg2){
                if ((((_arg1 == MediaTraitType.SEEK)) && (!((this.seekTrait == null))))){
                    this.seekTrait.addEventListener(SeekEvent.SEEKING_CHANGE, this.onSeekingChange);
                } else {
                    if ((((_arg1 == MediaTraitType.PLAY)) && (!((this.playTrait == null))))){
                        this.playTrait.addEventListener(PlayEvent.PLAY_STATE_CHANGE, this.onPlayStateChange);
                        if (this.playTrait.playState == PlayState.PLAYING){
                            _local3 = new PlayEvent(PlayEvent.PLAY_STATE_CHANGE, false, false, PlayState.PLAYING);
                            this.onPlayStateChange(_local3);
                        };
                    };
                };
            } else {
                if ((((_arg1 == MediaTraitType.SEEK)) && (!((this.seekTrait == null))))){
                    this.seekTrait.removeEventListener(SeekEvent.SEEKING_CHANGE, this.onSeekingChange);
                } else {
                    if ((((_arg1 == MediaTraitType.PLAY)) && (!((this.playTrait == null))))){
                        this.playTrait.removeEventListener(PlayEvent.PLAY_STATE_CHANGE, this.onPlayStateChange);
                    };
                };
            };
        }
        private function onSeekingChange(_arg1:SeekEvent):void{
            if (_arg1.seeking){
                this.reset(true);
            };
        }
        private function onPlayStateChange(_arg1:PlayEvent):void{
            var _local2:Timer;
            if (_arg1.playState == PlayState.PLAYING){
                if (this.durationTimers != null){
                    for each (_local2 in this.durationTimers) {
                        _local2.start();
                    };
                };
                this.startTimer();
            } else {
                if (this.durationTimers != null){
                    for each (_local2 in this.durationTimers) {
                        _local2.stop();
                    };
                };
                this.startTimer(false);
            };
        }
        private function findTemporalMetadata(_arg1:int, _arg2:int, _arg3:Number):int{
            var _local4:int;
            if (_arg1 <= _arg2){
                _local4 = ((_arg1 + _arg2) / 2);
                if (_arg3 == this.temporalKeyCollection[_local4]){
                    return (_local4);
                };
                if (_arg3 < this.temporalKeyCollection[_local4]){
                    return (this.findTemporalMetadata(_arg1, (_local4 - 1), _arg3));
                };
                return (this.findTemporalMetadata((_local4 + 1), _arg2, _arg3));
            };
            return (-(_arg1));
        }
        private function dispatchTemporalEvents(_arg1:int):void{
            var marker:* = null;
            var timer:* = null;
            var endTime:* = NaN;
            var onDurationTimer:* = null;
            var index:* = _arg1;
            marker = this.temporalValueCollection[index];
            dispatchEvent(new TimelineMetadataEvent(TimelineMetadataEvent.MARKER_TIME_REACHED, false, false, marker));
            if (marker.duration > 0){
                onDurationTimer = function (_arg1:TimerEvent):void{
                    if (((timeTrait) && ((timeTrait.currentTime >= endTime)))){
                        timer.removeEventListener(TimerEvent.TIMER, onDurationTimer);
                        delete durationTimers[marker];
                        dispatchEvent(new TimelineMetadataEvent(TimelineMetadataEvent.MARKER_DURATION_REACHED, false, false, marker));
                    };
                };
                timer = new Timer(CHECK_INTERVAL);
                endTime = (marker.time + marker.duration);
                if (this.durationTimers == null){
                    this.durationTimers = new Dictionary();
                };
                this.durationTimers[marker] = timer;
                timer.addEventListener(TimerEvent.TIMER, onDurationTimer);
                timer.start();
            };
        }
        private function checkTemporalMetadata(_arg1:int, _arg2:Number):Boolean{
            var _local4:Number;
            var _local5:Number;
            var _local6:Number;
            if (((!(this.temporalValueCollection)) || (!(this.temporalValueCollection.length)))){
                return (false);
            };
            var _local3:Boolean;
            if ((((((this.temporalValueCollection[_arg1].time >= (_arg2 - TOLERANCE))) && ((this.temporalValueCollection[_arg1].time <= (_arg2 + TOLERANCE))))) && (!((_arg1 == this.lastFiredTemporalMetadataIndex))))){
                this.lastFiredTemporalMetadataIndex = _arg1;
                this.dispatchTemporalEvents(_arg1);
                _local4 = this.temporalKeyCollection[_arg1];
                _local5 = this.calcNextTime(_arg1);
                _local6 = (((_local5 - _local4) * 1000) / 4);
                _local6 = ((_local6)>CHECK_INTERVAL) ? _local6 : CHECK_INTERVAL;
                if (_local4 == _local5){
                    this.startTimer(false);
                    this.restartTimer = false;
                } else {
                    if (_local6 != this.intervalTimer.delay){
                        this.intervalTimer.reset();
                        this.intervalTimer.delay = _local6;
                        this.startTimer();
                    };
                };
                _local3 = true;
            } else {
                if (((!((this.intervalTimer.delay == CHECK_INTERVAL))) && (((_arg2 + (this.intervalTimer.delay / 1000)) > this.calcNextTime(_arg1))))){
                    this.intervalTimer.reset();
                    this.intervalTimer.delay = CHECK_INTERVAL;
                    this.startTimer();
                };
            };
            return (_local3);
        }
        private function calcNextTime(_arg1:int):Number{
            return (this.temporalValueCollection[((((_arg1 + 1) < this.temporalKeyCollection.length)) ? (_arg1 + 1) : (this.temporalKeyCollection.length - 1))].time);
        }
        private function onIntervalTimer(_arg1:TimerEvent):void{
            this.checkForTemporalMetadata();
        }
        private function onTraitAdd(_arg1:MediaElementEvent):void{
            switch (_arg1.traitType){
                case MediaTraitType.TIME:
                    this.timeTrait = (this.media.getTrait(MediaTraitType.TIME) as TimeTrait);
                    this.startTimer();
                    break;
                case MediaTraitType.SEEK:
                    this.seekTrait = (this.media.getTrait(MediaTraitType.SEEK) as SeekTrait);
                    break;
                case MediaTraitType.PLAY:
                    this.playTrait = (this.media.getTrait(MediaTraitType.PLAY) as PlayTrait);
                    break;
            };
            this.setupTraitEventListener(_arg1.traitType);
        }
        private function onTraitRemove(_arg1:MediaElementEvent):void{
            this.setupTraitEventListener(_arg1.traitType, false);
            switch (_arg1.traitType){
                case MediaTraitType.TIME:
                    this.timeTrait = null;
                    if (this.media.hasOwnProperty("numChildren") == false){
                        this.startTimer(false);
                    };
                    break;
                case MediaTraitType.SEEK:
                    this.seekTrait = null;
                    break;
                case MediaTraitType.PLAY:
                    this.playTrait = null;
                    break;
            };
        }

    }
}//package org.osmf.metadata 
