﻿package org.osmf.metadata {
    import __AS3__.vec.*;
    import org.osmf.elements.compositeClasses.*;

    public class MetadataSynthesizer {

        public function synthesize(_arg1:String, _arg2:Metadata, _arg3:Vector.<Metadata>, _arg4:String, _arg5:Metadata):Metadata{
            var _local6:Metadata;
            if ((((_arg4 == CompositionMode.SERIAL)) && (_arg5))){
                _local6 = (_arg5.getValue(_arg1) as Metadata);
            } else {
                _local6 = ((_arg3.length)>=1) ? _arg3[0] : null;
            };
            return (_local6);
        }

    }
}//package org.osmf.metadata 
