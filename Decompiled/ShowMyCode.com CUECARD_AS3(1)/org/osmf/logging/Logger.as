﻿package org.osmf.logging {

    public class Logger {

        private var _category:String;

        public function Logger(_arg1:String){
            this._category = _arg1;
        }
        public function get category():String{
            return (this._category);
        }
        public function debug(_arg1:String, ... _args):void{
        }
        public function info(_arg1:String, ... _args):void{
        }
        public function warn(_arg1:String, ... _args):void{
        }
        public function error(_arg1:String, ... _args):void{
        }
        public function fatal(_arg1:String, ... _args):void{
        }

    }
}//package org.osmf.logging 
