﻿package org.osmf.logging {

    public class Log {

        private static var _loggerFactory:LoggerFactory;

        public static function get loggerFactory():LoggerFactory{
            return (_loggerFactory);
        }
        public static function set loggerFactory(_arg1:LoggerFactory):void{
            _loggerFactory = _arg1;
        }
        public static function getLogger(_arg1:String):Logger{
            return (((_loggerFactory)==null) ? null : _loggerFactory.getLogger(_arg1));
        }

    }
}//package org.osmf.logging 
