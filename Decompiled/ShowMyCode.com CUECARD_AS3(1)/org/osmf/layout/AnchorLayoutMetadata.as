﻿package org.osmf.layout {
    import org.osmf.metadata.*;
    import org.osmf.events.*;

    class AnchorLayoutMetadata extends NonSynthesizingMetadata {

        public static const LEFT:String = "left";
        public static const TOP:String = "top";
        public static const RIGHT:String = "right";
        public static const BOTTOM:String = "bottom";

        private var _left:Number;
        private var _top:Number;
        private var _right:Number;
        private var _bottom:Number;

        override public function getValue(_arg1:String){
            if (_arg1 == null){
                return (undefined);
            };
            if (_arg1 == LEFT){
                return (this.left);
            };
            if (_arg1 == TOP){
                return (this.top);
            };
            if (_arg1 == RIGHT){
                return (this.right);
            };
            if (_arg1 == BOTTOM){
                return (this.bottom);
            };
            return (undefined);
        }
        public function get left():Number{
            return (this._left);
        }
        public function set left(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._left != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, LEFT, _arg1, this._left);
                this._left = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get top():Number{
            return (this._top);
        }
        public function set top(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._top != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, TOP, _arg1, this._top);
                this._top = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get right():Number{
            return (this._right);
        }
        public function set right(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._right != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, RIGHT, _arg1, this._right);
                this._right = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get bottom():Number{
            return (this._bottom);
        }
        public function set bottom(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._bottom != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, BOTTOM, _arg1, this._bottom);
                this._bottom = _arg1;
                dispatchEvent(_local2);
            };
        }

    }
}//package org.osmf.layout 
