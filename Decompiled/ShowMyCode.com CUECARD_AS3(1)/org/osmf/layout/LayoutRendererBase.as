﻿package org.osmf.layout {
    import flash.events.*;
    import flash.display.*;
    import __AS3__.vec.*;
    import org.osmf.metadata.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import flash.geom.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class LayoutRendererBase extends EventDispatcher {

        private static var dispatcher:DisplayObject = new Sprite();
        private static var cleaningRenderers:Boolean;
        private static var dirtyRenderers:Vector.<LayoutRendererBase> = new Vector.<LayoutRendererBase>();
;

        private var _parent:LayoutRendererBase;
        private var _container:ILayoutTarget;
        private var layoutMetadata:LayoutMetadata;
        private var layoutTargets:Vector.<ILayoutTarget>;
        private var stagedDisplayObjects:Dictionary;
        private var _measuredWidth:Number;
        private var _measuredHeight:Number;
        private var dirty:Boolean;
        private var cleaning:Boolean;
        private var metaDataWatchers:Dictionary;

        public function LayoutRendererBase(){
            this.layoutTargets = new Vector.<ILayoutTarget>();
            this.stagedDisplayObjects = new Dictionary(true);
            this.metaDataWatchers = new Dictionary();
            super();
        }
        private static function flagDirty(_arg1:LayoutRendererBase):void{
            if ((((_arg1 == null)) || (!((dirtyRenderers.indexOf(_arg1) == -1))))){
                return;
            };
            dirtyRenderers.push(_arg1);
            if (cleaningRenderers == false){
                dispatcher.addEventListener(Event.EXIT_FRAME, onExitFrame);
            };
        }
        private static function flagClean(_arg1:LayoutRendererBase):void{
            var _local2:Number = dirtyRenderers.indexOf(_arg1);
            if (_local2 != -1){
                dirtyRenderers.splice(_local2, 1);
            };
        }
        private static function onExitFrame(_arg1:Event):void{
            var _local2:LayoutRendererBase;
            dispatcher.removeEventListener(Event.EXIT_FRAME, onExitFrame);
            cleaningRenderers = true;
            while (dirtyRenderers.length != 0) {
                _local2 = dirtyRenderers.shift();
                if (_local2.parent == null){
                    _local2.validateNow();
                } else {
                    _local2.dirty = false;
                };
            };
            cleaningRenderers = false;
        }

        final public function get parent():LayoutRendererBase{
            return (this._parent);
        }
        final function setParent(_arg1:LayoutRendererBase):void{
            this._parent = _arg1;
            this.processParentChange(this._parent);
        }
        final public function get container():ILayoutTarget{
            return (this._container);
        }
        final public function set container(_arg1:ILayoutTarget):void{
            var _local2:ILayoutTarget;
            if (_arg1 != this._container){
                _local2 = this._container;
                if (_local2 != null){
                    this.reset();
                    _local2.dispatchEvent(new LayoutTargetEvent(LayoutTargetEvent.UNSET_AS_LAYOUT_RENDERER_CONTAINER, false, false, this));
                    _local2.removeEventListener(DisplayObjectEvent.MEDIA_SIZE_CHANGE, this.invalidatingEventHandler);
                };
                this._container = _arg1;
                if (this._container){
                    this.layoutMetadata = this._container.layoutMetadata;
                    this._container.addEventListener(DisplayObjectEvent.MEDIA_SIZE_CHANGE, this.invalidatingEventHandler, false, 0, true);
                    this._container.dispatchEvent(new LayoutTargetEvent(LayoutTargetEvent.SET_AS_LAYOUT_RENDERER_CONTAINER, false, false, this));
                    this.invalidate();
                };
                this.processContainerChange(_local2, _arg1);
            };
        }
        final public function addTarget(_arg1:ILayoutTarget):ILayoutTarget{
            var _local4:String;
            var _local5:MetadataWatcher;
            if (_arg1 == null){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            if (this.layoutTargets.indexOf(_arg1) != -1){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            _arg1.dispatchEvent(new LayoutTargetEvent(LayoutTargetEvent.ADD_TO_LAYOUT_RENDERER, false, false, this));
            var _local2:int = Math.abs(BinarySearch.search(this.layoutTargets, this.compareTargets, _arg1));
            this.layoutTargets.splice(_local2, 0, _arg1);
            var _local3:Array = (this.metaDataWatchers[_arg1] = new Array());
            for each (_local4 in this.usedMetadatas) {
                _local5 = new MetadataWatcher(_arg1.layoutMetadata, _local4, null, this.targetMetadataChangeCallback);
                _local5.watch();
                _local3.push(_local5);
            };
            _arg1.addEventListener(DisplayObjectEvent.DISPLAY_OBJECT_CHANGE, this.invalidatingEventHandler);
            _arg1.addEventListener(DisplayObjectEvent.MEDIA_SIZE_CHANGE, this.invalidatingEventHandler);
            _arg1.addEventListener(LayoutTargetEvent.ADD_TO_LAYOUT_RENDERER, this.onTargetAddedToRenderer);
            _arg1.addEventListener(LayoutTargetEvent.SET_AS_LAYOUT_RENDERER_CONTAINER, this.onTargetSetAsContainer);
            this.invalidate();
            this.processTargetAdded(_arg1);
            return (_arg1);
        }
        final public function removeTarget(_arg1:ILayoutTarget):ILayoutTarget{
            var _local2:ILayoutTarget;
            var _local4:MetadataWatcher;
            if (_arg1 == null){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            var _local3:Number = this.layoutTargets.indexOf(_arg1);
            if (_local3 != -1){
                this.removeFromStage(_arg1);
                _local2 = this.layoutTargets.splice(_local3, 1)[0];
                _arg1.removeEventListener(DisplayObjectEvent.DISPLAY_OBJECT_CHANGE, this.invalidatingEventHandler);
                _arg1.removeEventListener(DisplayObjectEvent.MEDIA_SIZE_CHANGE, this.invalidatingEventHandler);
                _arg1.removeEventListener(LayoutTargetEvent.ADD_TO_LAYOUT_RENDERER, this.onTargetAddedToRenderer);
                _arg1.removeEventListener(LayoutTargetEvent.SET_AS_LAYOUT_RENDERER_CONTAINER, this.onTargetSetAsContainer);
                for each (_local4 in this.metaDataWatchers[_arg1]) {
                    _local4.unwatch();
                };
                delete this.metaDataWatchers[_arg1];
                this.processTargetRemoved(_arg1);
                _arg1.dispatchEvent(new LayoutTargetEvent(LayoutTargetEvent.REMOVE_FROM_LAYOUT_RENDERER, false, false, this));
                this.invalidate();
            } else {
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            return (_local2);
        }
        final public function hasTarget(_arg1:ILayoutTarget):Boolean{
            return (!((this.layoutTargets.indexOf(_arg1) == -1)));
        }
        final public function get measuredWidth():Number{
            return (this._measuredWidth);
        }
        final public function get measuredHeight():Number{
            return (this._measuredHeight);
        }
        final public function invalidate():void{
            if ((((this.cleaning == false)) && ((this.dirty == false)))){
                this.dirty = true;
                if (this._parent != null){
                    this._parent.invalidate();
                } else {
                    flagDirty(this);
                };
            };
        }
        final public function validateNow():void{
            if ((((this._container == null)) || ((this.cleaning == true)))){
                return;
            };
            if (this._parent){
                this._parent.validateNow();
                return;
            };
            this.cleaning = true;
            this.measure();
            this.layout(this._measuredWidth, this._measuredHeight);
            this.cleaning = false;
        }
        function measure():void{
            var _local1:ILayoutTarget;
            var _local2:Point;
            this.prepareTargets();
            for each (_local1 in this.layoutTargets) {
                _local1.measure(true);
            };
            _local2 = this.calculateContainerSize(this.layoutTargets);
            this._measuredWidth = _local2.x;
            this._measuredHeight = _local2.y;
            this._container.measure(false);
        }
        function layout(_arg1:Number, _arg2:Number):void{
            var _local3:ILayoutTarget;
            var _local4:Rectangle;
            var _local5:DisplayObject;
            this.processUpdateMediaDisplayBegin(this.layoutTargets);
            this._container.layout(_arg1, _arg2, false);
            for each (_local3 in this.layoutTargets) {
                _local4 = this.calculateTargetBounds(_local3, _arg1, _arg2);
                _local3.layout(_local4.width, _local4.height, true);
                _local5 = _local3.displayObject;
                if (_local5){
                    _local5.x = _local4.x;
                    _local5.y = _local4.y;
                };
            };
            this.dirty = false;
            this.processUpdateMediaDisplayEnd();
        }
        protected function get usedMetadatas():Vector.<String>{
            return (new Vector.<String>());
        }
        protected function compareTargets(_arg1:ILayoutTarget, _arg2:ILayoutTarget):Number{
            return (0);
        }
        protected function processContainerChange(_arg1:ILayoutTarget, _arg2:ILayoutTarget):void{
        }
        protected function processTargetAdded(_arg1:ILayoutTarget):void{
        }
        protected function processTargetRemoved(_arg1:ILayoutTarget):void{
        }
        protected function processStagedTarget(_arg1:ILayoutTarget):void{
        }
        protected function processUnstagedTarget(_arg1:ILayoutTarget):void{
        }
        protected function processUpdateMediaDisplayBegin(_arg1:Vector.<ILayoutTarget>):void{
        }
        protected function processUpdateMediaDisplayEnd():void{
        }
        protected function updateTargetOrder(_arg1:ILayoutTarget):void{
            var _local2:int = this.layoutTargets.indexOf(_arg1);
            if (_local2 != -1){
                this.layoutTargets.splice(_local2, 1);
                _local2 = Math.abs(BinarySearch.search(this.layoutTargets, this.compareTargets, _arg1));
                this.layoutTargets.splice(_local2, 0, _arg1);
            };
        }
        protected function calculateTargetBounds(_arg1:ILayoutTarget, _arg2:Number, _arg3:Number):Rectangle{
            return (new Rectangle());
        }
        protected function calculateContainerSize(_arg1:Vector.<ILayoutTarget>):Point{
            return (new Point());
        }
        protected function processParentChange(_arg1:LayoutRendererBase):void{
        }
        private function reset():void{
            var _local1:ILayoutTarget;
            for each (_local1 in this.layoutTargets) {
                this.removeTarget(_local1);
            };
            if (this._container){
                this._container.removeEventListener(DisplayObjectEvent.MEDIA_SIZE_CHANGE, this.invalidatingEventHandler);
                this.validateNow();
            };
            this._container = null;
            this.layoutMetadata = null;
        }
        private function targetMetadataChangeCallback(_arg1:Metadata):void{
            this.invalidate();
        }
        private function invalidatingEventHandler(_arg1:Event):void{
            this.invalidate();
        }
        private function onTargetAddedToRenderer(_arg1:LayoutTargetEvent):void{
            var _local2:ILayoutTarget;
            if (_arg1.layoutRenderer != this){
                _local2 = (_arg1.target as ILayoutTarget);
                if (this.hasTarget(_local2)){
                    this.removeTarget(_local2);
                };
            };
        }
        private function onTargetSetAsContainer(_arg1:LayoutTargetEvent):void{
            var _local2:ILayoutTarget;
            if (_arg1.layoutRenderer != this){
                _local2 = (_arg1.target as ILayoutTarget);
                if (this.container == _local2){
                    this.container = null;
                };
            };
        }
        private function prepareTargets():void{
            var _local2:ILayoutTarget;
            var _local3:DisplayObject;
            var _local1:int;
            for each (_local2 in this.layoutTargets) {
                _local3 = _local2.displayObject;
                if (_local3){
                    this.addToStage(_local2, _local2.displayObject, _local1);
                    _local1++;
                } else {
                    this.removeFromStage(_local2);
                };
            };
        }
        private function addToStage(_arg1:ILayoutTarget, _arg2:DisplayObject, _arg3:Number):void{
            var _local4:DisplayObject = this.stagedDisplayObjects[_arg1];
            if (_local4 == _arg2){
                this._container.dispatchEvent(new LayoutTargetEvent(LayoutTargetEvent.SET_CHILD_INDEX, false, false, this, _arg1, _local4, _arg3));
            } else {
                if (_local4 != null){
                    this._container.dispatchEvent(new LayoutTargetEvent(LayoutTargetEvent.REMOVE_CHILD, false, false, this, _arg1, _local4));
                };
                this.stagedDisplayObjects[_arg1] = _arg2;
                this._container.dispatchEvent(new LayoutTargetEvent(LayoutTargetEvent.ADD_CHILD_AT, false, false, this, _arg1, _arg2, _arg3));
                if (_local4 == null){
                    this.processStagedTarget(_arg1);
                };
            };
        }
        private function removeFromStage(_arg1:ILayoutTarget):void{
            var _local2:DisplayObject = this.stagedDisplayObjects[_arg1];
            if (_local2 != null){
                delete this.stagedDisplayObjects[_arg1];
                this._container.dispatchEvent(new LayoutTargetEvent(LayoutTargetEvent.REMOVE_CHILD, false, false, this, _arg1, _local2));
            };
        }

    }
}//package org.osmf.layout 
