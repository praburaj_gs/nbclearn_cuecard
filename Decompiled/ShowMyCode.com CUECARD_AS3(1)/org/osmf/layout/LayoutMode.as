﻿package org.osmf.layout {

    public final class LayoutMode {

        public static const NONE:String = "none";
        public static const VERTICAL:String = "vertical";
        public static const HORIZONTAL:String = "horizontal";
        public static const OVERLAY:String = "overlay";

    }
}//package org.osmf.layout 
