﻿package org.osmf.layout {
    import flash.events.*;
    import flash.display.*;

    public interface ILayoutTarget extends IEventDispatcher {

        function get displayObject():DisplayObject;
        function get layoutMetadata():LayoutMetadata;
        function get measuredWidth():Number;
        function get measuredHeight():Number;
        function measure(_arg1:Boolean=true):void;
        function layout(_arg1:Number, _arg2:Number, _arg3:Boolean=true):void;

    }
}//package org.osmf.layout 
