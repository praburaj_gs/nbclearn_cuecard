﻿package org.osmf.layout {
    import org.osmf.metadata.*;
    import org.osmf.events.*;

    class BoxAttributesMetadata extends NonSynthesizingMetadata {

        public static const RELATIVE_SUM:String = "relativeSum";
        public static const ABSOLUTE_SUM:String = "absoluteSum";

        private var _relativeSum:Number;
        private var _absoluteSum:Number;

        public function BoxAttributesMetadata(){
            this._relativeSum = 0;
            this._absoluteSum = 0;
        }
        override public function getValue(_arg1:String){
            if (_arg1 == null){
                return (undefined);
            };
            if (_arg1 == RELATIVE_SUM){
                return (this.relativeSum);
            };
            if (_arg1 == ABSOLUTE_SUM){
                return (this.absoluteSum);
            };
            return (undefined);
        }
        public function get relativeSum():Number{
            return (this._relativeSum);
        }
        public function set relativeSum(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._relativeSum != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, RELATIVE_SUM, _arg1, this._relativeSum);
                this._relativeSum = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get absoluteSum():Number{
            return (this._absoluteSum);
        }
        public function set absoluteSum(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._absoluteSum != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, ABSOLUTE_SUM, _arg1, this._absoluteSum);
                this._absoluteSum = _arg1;
                dispatchEvent(_local2);
            };
        }

    }
}//package org.osmf.layout 
