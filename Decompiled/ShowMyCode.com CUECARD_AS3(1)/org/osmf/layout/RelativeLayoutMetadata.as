﻿package org.osmf.layout {
    import org.osmf.metadata.*;
    import org.osmf.events.*;

    class RelativeLayoutMetadata extends NonSynthesizingMetadata {

        public static const X:String = "x";
        public static const Y:String = "y";
        public static const WIDTH:String = "width";
        public static const HEIGHT:String = "height";

        private var _x:Number;
        private var _y:Number;
        private var _width:Number;
        private var _height:Number;

        override public function getValue(_arg1:String){
            if (_arg1 == null){
                return (undefined);
            };
            if (_arg1 == X){
                return (this.x);
            };
            if (_arg1 == Y){
                return (this.y);
            };
            if (_arg1 == WIDTH){
                return (this.width);
            };
            if (_arg1 == HEIGHT){
                return (this.height);
            };
            return (undefined);
        }
        public function get x():Number{
            return (this._x);
        }
        public function set x(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._x != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, X, _arg1, this._x);
                this._x = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get y():Number{
            return (this._y);
        }
        public function set y(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._y != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, Y, _arg1, this._y);
                this._y = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get width():Number{
            return (this._width);
        }
        public function set width(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._width != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, WIDTH, _arg1, this._width);
                this._width = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get height():Number{
            return (this._height);
        }
        public function set height(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._height != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, HEIGHT, _arg1, this._height);
                this._height = _arg1;
                dispatchEvent(_local2);
            };
        }

    }
}//package org.osmf.layout 
