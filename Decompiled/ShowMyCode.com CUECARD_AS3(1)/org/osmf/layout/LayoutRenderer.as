﻿package org.osmf.layout {
    import __AS3__.vec.*;
    import org.osmf.metadata.*;
    import flash.utils.*;
    import flash.geom.*;

    public class LayoutRenderer extends LayoutRendererBase {

        private static const USED_METADATAS_COUNT:int = 6;
        private static const USED_METADATAS:Vector.<String> = new Vector.<String>(USED_METADATAS_COUNT, true);
;
        private static const X:int = 1;
        private static const Y:int = 2;
        private static const WIDTH:int = 4;
        private static const HEIGHT:int = 8;
        private static const POSITION:int = 3;
        private static const DIMENSIONS:int = 12;
        private static const ALL:int = 15;

        private var layoutMode:String = "none";
        private var lastCalculatedBounds:Rectangle;
        private var targetMetadataWatchers:Dictionary;
        private var containerAbsoluteWatcher:MetadataWatcher;
        private var containerAttributesWatcher:MetadataWatcher;

        public function LayoutRenderer(){
            this.targetMetadataWatchers = new Dictionary();
            super();
        }
        override protected function get usedMetadatas():Vector.<String>{
            return (USED_METADATAS);
        }
        override protected function processContainerChange(_arg1:ILayoutTarget, _arg2:ILayoutTarget):void{
            var oldContainer:* = _arg1;
            var newContainer:* = _arg2;
            if (oldContainer){
                this.containerAbsoluteWatcher.unwatch();
                this.containerAttributesWatcher.unwatch();
            };
            if (newContainer){
                this.containerAbsoluteWatcher = new MetadataWatcher(newContainer.layoutMetadata, MetadataNamespaces.ABSOLUTE_LAYOUT_PARAMETERS, null, function (... _args):void{
                    invalidate();
                });
                this.containerAbsoluteWatcher.watch();
                this.containerAttributesWatcher = new MetadataWatcher(newContainer.layoutMetadata, MetadataNamespaces.LAYOUT_ATTRIBUTES, null, function (_arg1:LayoutAttributesMetadata):void{
                    layoutMode = ((_arg1) ? _arg1.layoutMode : LayoutMode.NONE);
                    invalidate();
                });
                this.containerAttributesWatcher.watch();
            };
            invalidate();
        }
        override protected function processUpdateMediaDisplayBegin(_arg1:Vector.<ILayoutTarget>):void{
            this.lastCalculatedBounds = null;
        }
        override protected function processUpdateMediaDisplayEnd():void{
            this.lastCalculatedBounds = null;
        }
        override protected function processTargetAdded(_arg1:ILayoutTarget):void{
            var target:* = _arg1;
            var attributes:* = (target.layoutMetadata.getValue(MetadataNamespaces.LAYOUT_ATTRIBUTES) as LayoutAttributesMetadata);
            var relative:* = (target.layoutMetadata.getValue(MetadataNamespaces.RELATIVE_LAYOUT_PARAMETERS) as RelativeLayoutMetadata);
            if ((((((((((((this.layoutMode == LayoutMode.NONE)) || ((this.layoutMode == LayoutMode.OVERLAY)))) && ((relative == null)))) && ((attributes == null)))) && ((target.layoutMetadata.getValue(MetadataNamespaces.ABSOLUTE_LAYOUT_PARAMETERS) == null)))) && ((target.layoutMetadata.getValue(MetadataNamespaces.ANCHOR_LAYOUT_PARAMETERS) == null)))){
                relative = new RelativeLayoutMetadata();
                relative.width = 100;
                relative.height = 100;
                target.layoutMetadata.addValue(MetadataNamespaces.RELATIVE_LAYOUT_PARAMETERS, relative);
                attributes = new LayoutAttributesMetadata();
                attributes.scaleMode = ((attributes.scaleMode) || (ScaleMode.LETTERBOX));
                attributes.verticalAlign = ((attributes.verticalAlign) || (VerticalAlign.MIDDLE));
                attributes.horizontalAlign = ((attributes.horizontalAlign) || (HorizontalAlign.CENTER));
                target.layoutMetadata.addValue(MetadataNamespaces.LAYOUT_ATTRIBUTES, attributes);
            };
            var watcher:* = new MetadataWatcher(target.layoutMetadata, MetadataNamespaces.OVERLAY_LAYOUT_PARAMETERS, OverlayLayoutMetadata.INDEX, function (... _args):void{
                updateTargetOrder(target);
            });
            watcher.watch();
            this.targetMetadataWatchers[target] = watcher;
        }
        override protected function processTargetRemoved(_arg1:ILayoutTarget):void{
            var _local2:MetadataWatcher = this.targetMetadataWatchers[_arg1];
            delete this.targetMetadataWatchers[_arg1];
            _local2.unwatch();
            _local2 = null;
        }
        override protected function compareTargets(_arg1:ILayoutTarget, _arg2:ILayoutTarget):Number{
            var _local3:OverlayLayoutMetadata = (_arg1.layoutMetadata.getValue(MetadataNamespaces.OVERLAY_LAYOUT_PARAMETERS) as OverlayLayoutMetadata);
            var _local4:OverlayLayoutMetadata = (_arg2.layoutMetadata.getValue(MetadataNamespaces.OVERLAY_LAYOUT_PARAMETERS) as OverlayLayoutMetadata);
            var _local5:Number = ((_local3) ? _local3.index : NaN);
            var _local6:Number = ((_local4) ? _local4.index : NaN);
            if (((isNaN(_local5)) && (isNaN(_local6)))){
                return (1);
            };
            _local5 = ((_local5) || (0));
            _local6 = ((_local6) || (0));
            return ((((_local5 < _local6)) ? -1 : (((_local5 > _local6)) ? 1 : 0)));
        }
        override protected function calculateTargetBounds(_arg1:ILayoutTarget, _arg2:Number, _arg3:Number):Rectangle{
            var _local7:Number;
            var _local9:Number;
            var _local13:BoxAttributesMetadata;
            var _local14:RelativeLayoutMetadata;
            var _local15:AnchorLayoutMetadata;
            var _local16:Point;
            var _local4:LayoutAttributesMetadata = (((_arg1.layoutMetadata.getValue(MetadataNamespaces.LAYOUT_ATTRIBUTES) as LayoutAttributesMetadata)) || (new LayoutAttributesMetadata()));
            if (_local4.includeInLayout == false){
                return (new Rectangle());
            };
            var _local5:Rectangle = new Rectangle(0, 0, _arg1.measuredWidth, _arg1.measuredHeight);
            var _local6:AbsoluteLayoutMetadata = (_arg1.layoutMetadata.getValue(MetadataNamespaces.ABSOLUTE_LAYOUT_PARAMETERS) as AbsoluteLayoutMetadata);
            var _local8:Number = 0;
            var _local10:Number = 0;
            var _local11:int = ALL;
            if (_local6){
                if (!isNaN(_local6.x)){
                    _local5.x = _local6.x;
                    _local11 = (_local11 ^ X);
                };
                if (!isNaN(_local6.y)){
                    _local5.y = _local6.y;
                    _local11 = (_local11 ^ Y);
                };
                if (!isNaN(_local6.width)){
                    _local5.width = _local6.width;
                    _local11 = (_local11 ^ WIDTH);
                };
                if (!isNaN(_local6.height)){
                    _local5.height = _local6.height;
                    _local11 = (_local11 ^ HEIGHT);
                };
            };
            if (_local11 != 0){
                _local14 = (_arg1.layoutMetadata.getValue(MetadataNamespaces.RELATIVE_LAYOUT_PARAMETERS) as RelativeLayoutMetadata);
                if (_local14){
                    if ((((_local11 & X)) && (!(isNaN(_local14.x))))){
                        _local5.x = ((((_arg2 * _local14.x) / 100)) || (0));
                        _local11 = (_local11 ^ X);
                    };
                    if ((((_local11 & WIDTH)) && (!(isNaN(_local14.width))))){
                        if (this.layoutMode == LayoutMode.HORIZONTAL){
                            _local13 = (((container.layoutMetadata.getValue(MetadataNamespaces.BOX_LAYOUT_ATTRIBUTES) as BoxAttributesMetadata)) || (new BoxAttributesMetadata()));
                            _local5.width = ((Math.max(0, (_arg2 - _local13.absoluteSum)) * _local14.width) / _local13.relativeSum);
                        } else {
                            _local5.width = ((_arg2 * _local14.width) / 100);
                        };
                        _local11 = (_local11 ^ WIDTH);
                    };
                    if ((((_local11 & Y)) && (!(isNaN(_local14.y))))){
                        _local5.y = ((((_arg3 * _local14.y) / 100)) || (0));
                        _local11 = (_local11 ^ Y);
                    };
                    if ((((_local11 & HEIGHT)) && (!(isNaN(_local14.height))))){
                        if (this.layoutMode == LayoutMode.VERTICAL){
                            _local13 = (((container.layoutMetadata.getValue(MetadataNamespaces.BOX_LAYOUT_ATTRIBUTES) as BoxAttributesMetadata)) || (new BoxAttributesMetadata()));
                            _local5.height = ((Math.max(0, (_arg3 - _local13.absoluteSum)) * _local14.height) / _local13.relativeSum);
                        } else {
                            _local5.height = ((_arg3 * _local14.height) / 100);
                        };
                        _local11 = (_local11 ^ HEIGHT);
                    };
                };
            };
            if (_local4.scaleMode){
                if ((((_local11 & WIDTH)) || ((_local11 & HEIGHT)))){
                    if ((((_local11 & WIDTH)) && (!(isNaN(_arg1.measuredWidth))))){
                        _local5.width = _arg1.measuredWidth;
                        _local11 = (_local11 ^ WIDTH);
                    };
                    if ((((_local11 & HEIGHT)) && (!(isNaN(_arg1.measuredHeight))))){
                        _local5.height = _arg1.measuredHeight;
                        _local11 = (_local11 ^ HEIGHT);
                    };
                };
            };
            if (_local11 != 0){
                _local15 = (_arg1.layoutMetadata.getValue(MetadataNamespaces.ANCHOR_LAYOUT_PARAMETERS) as AnchorLayoutMetadata);
                if (_local15){
                    if ((((_local11 & X)) && (!(isNaN(_local15.left))))){
                        _local5.x = _local15.left;
                        _local11 = (_local11 ^ X);
                    };
                    if ((((_local11 & Y)) && (!(isNaN(_local15.top))))){
                        _local5.y = _local15.top;
                        _local11 = (_local11 ^ Y);
                    };
                    if (((!(isNaN(_local15.right))) && (_arg2))){
                        if ((((_local11 & X)) && (!((_local11 & WIDTH))))){
                            _local5.x = Math.max(0, ((_arg2 - _local5.width) - _local15.right));
                            _local11 = (_local11 ^ X);
                        } else {
                            if ((((_local11 & WIDTH)) && (!((_local11 & X))))){
                                _local5.width = Math.max(0, ((_arg2 - _local15.right) - _local5.x));
                                _local11 = (_local11 ^ WIDTH);
                            } else {
                                _local5.x = Math.max(0, ((_arg2 - _arg1.measuredWidth) - _local15.right));
                                _local11 = (_local11 ^ X);
                            };
                        };
                        _local8 = (_local8 + _local15.right);
                    };
                    if (((!(isNaN(_local15.bottom))) && (_arg3))){
                        if ((((_local11 & Y)) && (!((_local11 & HEIGHT))))){
                            _local5.y = Math.max(0, ((_arg3 - _local5.height) - _local15.bottom));
                            _local11 = (_local11 ^ Y);
                        } else {
                            if ((((_local11 & HEIGHT)) && (!((_local11 & Y))))){
                                _local5.height = Math.max(0, ((_arg3 - _local15.bottom) - _local5.y));
                                _local11 = (_local11 ^ HEIGHT);
                            } else {
                                _local5.y = Math.max(0, ((_arg3 - _arg1.measuredHeight) - _local15.bottom));
                                _local11 = (_local11 ^ Y);
                            };
                        };
                        _local10 = (_local10 + _local15.bottom);
                    };
                };
            };
            var _local12:PaddingLayoutMetadata = (_arg1.layoutMetadata.getValue(MetadataNamespaces.PADDING_LAYOUT_PARAMETERS) as PaddingLayoutMetadata);
            if (_local12){
                if (!isNaN(_local12.left)){
                    _local5.x = (_local5.x + _local12.left);
                };
                if (!isNaN(_local12.top)){
                    _local5.y = (_local5.y + _local12.top);
                };
                if (((!(isNaN(_local12.right))) && (!((_local11 & WIDTH))))){
                    _local5.width = (_local5.width - (_local12.right + ((_local12.left) || (0))));
                };
                if (((!(isNaN(_local12.bottom))) && (!((_local11 & HEIGHT))))){
                    _local5.height = (_local5.height - (_local12.bottom + ((_local12.top) || (0))));
                };
            };
            if (_local4.scaleMode){
                if (((((((!(isNaN(_arg1.measuredWidth))) && (!((_local11 & WIDTH))))) && (!(isNaN(_arg1.measuredHeight))))) && (!((_local11 & HEIGHT))))){
                    _local16 = ScaleModeUtils.getScaledSize(_local4.scaleMode, _local5.width, _local5.height, _arg1.measuredWidth, _arg1.measuredHeight);
                    _local7 = (_local5.width - _local16.x);
                    _local9 = (_local5.height - _local16.y);
                    _local5.width = _local16.x;
                    _local5.height = _local16.y;
                };
            };
            if (this.layoutMode != LayoutMode.HORIZONTAL){
                _local7 = ((_local7) || ((((_arg2 - ((_local5.x) || (0))) - ((_local5.width) || (0))) - _local8)));
            };
            if (this.layoutMode != LayoutMode.VERTICAL){
                _local9 = ((_local9) || ((((_arg3 - ((_local5.y) || (0))) - ((_local5.height) || (0))) - _local10)));
            };
            if (_local9){
                switch (_local4.verticalAlign){
                    case null:
                    case VerticalAlign.TOP:
                        break;
                    case VerticalAlign.MIDDLE:
                        _local5.y = (_local5.y + (_local9 / 2));
                        break;
                    case VerticalAlign.BOTTOM:
                        _local5.y = (_local5.y + _local9);
                        break;
                };
            };
            if (_local7){
                switch (_local4.horizontalAlign){
                    case null:
                    case HorizontalAlign.LEFT:
                        break;
                    case HorizontalAlign.CENTER:
                        _local5.x = (_local5.x + (_local7 / 2));
                        break;
                    case HorizontalAlign.RIGHT:
                        _local5.x = (_local5.x + _local7);
                        break;
                };
            };
            if (_local4.snapToPixel){
                _local5.x = Math.round(_local5.x);
                _local5.y = Math.round(_local5.y);
                _local5.width = Math.round(_local5.width);
                _local5.height = Math.round(_local5.height);
            };
            if ((((this.layoutMode == LayoutMode.HORIZONTAL)) || ((this.layoutMode == LayoutMode.VERTICAL)))){
                if (this.lastCalculatedBounds != null){
                    if (this.layoutMode == LayoutMode.HORIZONTAL){
                        _local5.x = (this.lastCalculatedBounds.x + this.lastCalculatedBounds.width);
                    } else {
                        _local5.y = (this.lastCalculatedBounds.y + this.lastCalculatedBounds.height);
                    };
                };
                this.lastCalculatedBounds = _local5;
            };
            return (_local5);
        }
        override protected function calculateContainerSize(_arg1:Vector.<ILayoutTarget>):Point{
            var _local4:BoxAttributesMetadata;
            var _local5:Rectangle;
            var _local6:Rectangle;
            var _local7:Rectangle;
            var _local8:ILayoutTarget;
            var _local2:Point = new Point(NaN, NaN);
            var _local3:AbsoluteLayoutMetadata = (container.layoutMetadata.getValue(MetadataNamespaces.ABSOLUTE_LAYOUT_PARAMETERS) as AbsoluteLayoutMetadata);
            if (_local3){
                _local2.x = _local3.width;
                _local2.y = _local3.height;
            };
            if (((!((this.layoutMode == LayoutMode.NONE))) && (!((this.layoutMode == LayoutMode.OVERLAY))))){
                _local4 = new BoxAttributesMetadata();
                container.layoutMetadata.addValue(MetadataNamespaces.BOX_LAYOUT_ATTRIBUTES, _local4);
            };
            if (((((isNaN(_local2.x)) || (isNaN(_local2.y)))) || (((!((this.layoutMode == LayoutMode.NONE))) && (!((this.layoutMode == LayoutMode.OVERLAY))))))){
                _local5 = new Rectangle();
                for each (_local8 in _arg1) {
                    if (_local8.layoutMetadata.includeInLayout){
                        _local6 = this.calculateTargetBounds(_local8, _local2.x, _local2.y);
                        this.calculateTargetBounds(_local8, _local2.x, _local2.y).x = ((_local6.x) || (0));
                        _local6.y = ((_local6.y) || (0));
                        _local6.width = ((_local6.width) || (((_local8.measuredWidth) || (0))));
                        _local6.height = ((_local6.height) || (((_local8.measuredHeight) || (0))));
                        if (this.layoutMode == LayoutMode.HORIZONTAL){
                            if (!isNaN(_local8.layoutMetadata.percentWidth)){
                                _local4.relativeSum = (_local4.relativeSum + _local8.layoutMetadata.percentWidth);
                            } else {
                                _local4.absoluteSum = (_local4.absoluteSum + _local6.width);
                            };
                            if (_local7){
                                _local6.x = (_local7.x + _local7.width);
                            };
                            _local7 = _local6;
                        } else {
                            if (this.layoutMode == LayoutMode.VERTICAL){
                                if (!isNaN(_local8.layoutMetadata.percentHeight)){
                                    _local4.relativeSum = (_local4.relativeSum + _local8.layoutMetadata.percentHeight);
                                } else {
                                    _local4.absoluteSum = (_local4.absoluteSum + _local6.height);
                                };
                                if (_local7){
                                    _local6.y = (_local7.y + _local7.height);
                                };
                                _local7 = _local6;
                            };
                        };
                        _local5 = _local5.union(_local6);
                    };
                };
                _local2.x = ((_local2.x) || ((((((_local3 == null)) || (isNaN(_local3.width)))) ? _local5.width : _local3.width)));
                _local2.y = ((_local2.y) || ((((((_local3 == null)) || (isNaN(_local3.height)))) ? _local5.height : _local3.height)));
            };
            return (_local2);
        }

        USED_METADATAS[0] = MetadataNamespaces.ABSOLUTE_LAYOUT_PARAMETERS;
        USED_METADATAS[1] = MetadataNamespaces.RELATIVE_LAYOUT_PARAMETERS;
        USED_METADATAS[2] = MetadataNamespaces.ANCHOR_LAYOUT_PARAMETERS;
        USED_METADATAS[3] = MetadataNamespaces.PADDING_LAYOUT_PARAMETERS;
        USED_METADATAS[4] = MetadataNamespaces.LAYOUT_ATTRIBUTES;
        USED_METADATAS[5] = MetadataNamespaces.OVERLAY_LAYOUT_PARAMETERS;
    }
}//package org.osmf.layout 
