﻿package org.osmf.layout {
    import org.osmf.layout.*;

    class LayoutTargetRenderers {

        public var containerRenderer:LayoutRendererBase;
        public var parentRenderer:LayoutRendererBase;

        public function LayoutTargetRenderers(_arg1:ILayoutTarget){
            _arg1.addEventListener(LayoutTargetEvent.ADD_TO_LAYOUT_RENDERER, this.onAddedToLayoutRenderer);
            _arg1.addEventListener(LayoutTargetEvent.REMOVE_FROM_LAYOUT_RENDERER, this.onRemovedFromLayoutRenderer);
            _arg1.addEventListener(LayoutTargetEvent.SET_AS_LAYOUT_RENDERER_CONTAINER, this.onSetAsLayoutRendererContainer);
            _arg1.addEventListener(LayoutTargetEvent.UNSET_AS_LAYOUT_RENDERER_CONTAINER, this.onUnsetAsLayoutRendererContainer);
        }
        private function onSetAsLayoutRendererContainer(_arg1:LayoutTargetEvent):void{
            if (this.containerRenderer != _arg1.layoutRenderer){
                this.containerRenderer = _arg1.layoutRenderer;
                this.containerRenderer.setParent(this.parentRenderer);
            };
        }
        private function onUnsetAsLayoutRendererContainer(_arg1:LayoutTargetEvent):void{
            if (((!((this.containerRenderer == null))) && ((this.containerRenderer == _arg1.layoutRenderer)))){
                this.containerRenderer.setParent(null);
                this.containerRenderer = null;
            };
        }
        private function onAddedToLayoutRenderer(_arg1:LayoutTargetEvent):void{
            if (this.parentRenderer != _arg1.layoutRenderer){
                this.parentRenderer = _arg1.layoutRenderer;
                if (this.containerRenderer){
                    this.containerRenderer.setParent(this.parentRenderer);
                };
            };
        }
        private function onRemovedFromLayoutRenderer(_arg1:LayoutTargetEvent):void{
            if (this.parentRenderer == _arg1.layoutRenderer){
                this.parentRenderer = null;
                if (this.containerRenderer){
                    this.containerRenderer.setParent(null);
                };
            };
        }

    }
}//package org.osmf.layout 
