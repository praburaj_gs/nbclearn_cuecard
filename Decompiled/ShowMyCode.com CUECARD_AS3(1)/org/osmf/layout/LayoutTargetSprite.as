﻿package org.osmf.layout {
    import flash.display.*;
    import org.osmf.events.*;

    public class LayoutTargetSprite extends Sprite implements ILayoutTarget {

        private var _layoutMetadata:LayoutMetadata;
        private var _measuredWidth:Number = NaN;
        private var _measuredHeight:Number = NaN;
        private var renderers:LayoutTargetRenderers;

        public function LayoutTargetSprite(_arg1:LayoutMetadata=null){
            this._layoutMetadata = ((_arg1) || (new LayoutMetadata()));
            this.renderers = new LayoutTargetRenderers(this);
            addEventListener(LayoutTargetEvent.ADD_CHILD_AT, this.onAddChildAt);
            addEventListener(LayoutTargetEvent.SET_CHILD_INDEX, this.onSetChildIndex);
            addEventListener(LayoutTargetEvent.REMOVE_CHILD, this.onRemoveChild);
            mouseEnabled = true;
            mouseChildren = true;
            super();
        }
        public function get displayObject():DisplayObject{
            return (this);
        }
        public function get layoutMetadata():LayoutMetadata{
            return (this._layoutMetadata);
        }
        public function get measuredWidth():Number{
            return (this._measuredWidth);
        }
        public function get measuredHeight():Number{
            return (this._measuredHeight);
        }
        public function measure(_arg1:Boolean=true):void{
            var _local2:Number;
            var _local3:Number;
            var _local4:DisplayObjectEvent;
            if (((_arg1) && (this.renderers.containerRenderer))){
                this.renderers.containerRenderer.measure();
            };
            if (this.renderers.containerRenderer){
                _local2 = this.renderers.containerRenderer.measuredWidth;
                _local3 = this.renderers.containerRenderer.measuredHeight;
            } else {
                _local2 = (super.width / scaleX);
                _local3 = (super.height / scaleY);
            };
            if (((!((_local2 == this._measuredWidth))) || (!((_local3 == this._measuredHeight))))){
                _local4 = new DisplayObjectEvent(DisplayObjectEvent.MEDIA_SIZE_CHANGE, false, false, null, null, this._measuredWidth, this._measuredHeight, _local2, _local3);
                this._measuredWidth = _local2;
                this._measuredHeight = _local3;
                dispatchEvent(_local4);
            };
        }
        public function layout(_arg1:Number, _arg2:Number, _arg3:Boolean=true):void{
            if (this.renderers.containerRenderer == null){
                super.width = _arg1;
                super.height = _arg2;
            } else {
                if (_arg3){
                    this.renderers.containerRenderer.layout(_arg1, _arg2);
                };
            };
        }
        public function validateNow():void{
            if (this.renderers.containerRenderer){
                this.renderers.containerRenderer.validateNow();
            };
        }
        protected function onAddChildAt(_arg1:LayoutTargetEvent):void{
            addChildAt(_arg1.displayObject, _arg1.index);
        }
        protected function onRemoveChild(_arg1:LayoutTargetEvent):void{
            removeChild(_arg1.displayObject);
        }
        protected function onSetChildIndex(_arg1:LayoutTargetEvent):void{
            setChildIndex(_arg1.displayObject, _arg1.index);
        }
        override public function set x(_arg1:Number):void{
            var _local2:int;
            super.x = _arg1;
            if (this.numChildren){
                _local2 = 0;
                while (_local2 < this.numChildren) {
                    if (this.getChildAt(_local2)){
                        this.getChildAt(_local2).x = this.getChildAt(_local2).x;
                    };
                    _local2++;
                };
            };
        }
        override public function set y(_arg1:Number):void{
            var _local2:int;
            super.y = _arg1;
            if (this.numChildren){
                _local2 = 0;
                while (_local2 < this.numChildren) {
                    if (this.getChildAt(_local2)){
                        this.getChildAt(_local2).y = this.getChildAt(_local2).y;
                    };
                    _local2++;
                };
            };
        }
        override public function set width(_arg1:Number):void{
            this._layoutMetadata.width = _arg1;
        }
        override public function get width():Number{
            return (this._measuredWidth);
        }
        override public function set height(_arg1:Number):void{
            this._layoutMetadata.height = _arg1;
        }
        override public function get height():Number{
            return (this._measuredHeight);
        }

    }
}//package org.osmf.layout 
