﻿package org.osmf.layout {
    import flash.events.*;
    import flash.display.*;
    import org.osmf.media.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class MediaElementLayoutTarget extends EventDispatcher implements ILayoutTarget {

        private static const layoutTargets:Dictionary = new Dictionary(true);

        private var _mediaElement:MediaElement;
        private var _layoutMetadata:LayoutMetadata;
        private var displayObjectTrait:DisplayObjectTrait;
        private var _displayObject:DisplayObject;
        private var renderers:LayoutTargetRenderers;

        public function MediaElementLayoutTarget(_arg1:MediaElement, _arg2:Class){
            if (_arg2 != ConstructorLock){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.ILLEGAL_CONSTRUCTOR_INVOCATION)));
            };
            this._mediaElement = _arg1;
            this._mediaElement.addEventListener(MediaElementEvent.TRAIT_ADD, this.onMediaElementTraitsChange);
            this._mediaElement.addEventListener(MediaElementEvent.TRAIT_REMOVE, this.onMediaElementTraitsChange);
            this._mediaElement.addEventListener(MediaElementEvent.METADATA_ADD, this.onMetadataAdd);
            this._mediaElement.addEventListener(MediaElementEvent.METADATA_REMOVE, this.onMetadataRemove);
            this.renderers = new LayoutTargetRenderers(this);
            this._layoutMetadata = (this._mediaElement.getMetadata(LayoutMetadata.LAYOUT_NAMESPACE) as LayoutMetadata);
            addEventListener(LayoutTargetEvent.ADD_CHILD_AT, this.onAddChildAt);
            addEventListener(LayoutTargetEvent.SET_CHILD_INDEX, this.onSetChildIndex);
            addEventListener(LayoutTargetEvent.REMOVE_CHILD, this.onRemoveChild);
            this.onMediaElementTraitsChange();
        }
        public static function getInstance(_arg1:MediaElement):MediaElementLayoutTarget{
            var _local2:*;
            for (_local2 in layoutTargets) {
                if (_local2.mediaElement == _arg1){
                    break;
                };
                _local2 = null;
            };
            if (_local2 == null){
                _local2 = new MediaElementLayoutTarget(_arg1, ConstructorLock);
                layoutTargets[_local2] = true;
            };
            return (_local2);
        }

        public function get mediaElement():MediaElement{
            return (this._mediaElement);
        }
        public function get layoutMetadata():LayoutMetadata{
            if (this._layoutMetadata == null){
                this._layoutMetadata = new LayoutMetadata();
                this._mediaElement.addMetadata(LayoutMetadata.LAYOUT_NAMESPACE, this._layoutMetadata);
            };
            return (this._layoutMetadata);
        }
        public function get displayObject():DisplayObject{
            return (this._displayObject);
        }
        public function get measuredWidth():Number{
            return (((this.displayObjectTrait) ? this.displayObjectTrait.mediaWidth : NaN));
        }
        public function get measuredHeight():Number{
            return (((this.displayObjectTrait) ? this.displayObjectTrait.mediaHeight : NaN));
        }
        public function measure(_arg1:Boolean=true):void{
            if ((this._displayObject is ILayoutTarget)){
                ILayoutTarget(this._displayObject).measure(_arg1);
            };
        }
        public function layout(_arg1:Number, _arg2:Number, _arg3:Boolean=true):void{
            if ((this._displayObject is ILayoutTarget)){
                ILayoutTarget(this._displayObject).layout(_arg1, _arg2, _arg3);
            } else {
                if (((!((this._displayObject == null))) && ((this.renderers.containerRenderer == null)))){
                    this._displayObject.width = _arg1;
                    this._displayObject.height = _arg2;
                };
            };
        }
        private function onMediaElementTraitsChange(_arg1:MediaElementEvent=null):void{
            var _local2:DisplayObjectTrait;
            if ((((_arg1 == null)) || (((_arg1) && ((_arg1.traitType == MediaTraitType.DISPLAY_OBJECT)))))){
                _local2 = ((((_arg1) && ((_arg1.type == MediaElementEvent.TRAIT_REMOVE)))) ? null : (this._mediaElement.getTrait(MediaTraitType.DISPLAY_OBJECT) as DisplayObjectTrait));
                if (_local2 != this.displayObjectTrait){
                    if (this.displayObjectTrait){
                        this.displayObjectTrait.removeEventListener(DisplayObjectEvent.DISPLAY_OBJECT_CHANGE, this.onDisplayObjectTraitDisplayObjecChange);
                        this.displayObjectTrait.removeEventListener(DisplayObjectEvent.MEDIA_SIZE_CHANGE, this.onDisplayObjectTraitMediaSizeChange);
                    };
                    this.displayObjectTrait = _local2;
                    if (this.displayObjectTrait){
                        this.displayObjectTrait.addEventListener(DisplayObjectEvent.DISPLAY_OBJECT_CHANGE, this.onDisplayObjectTraitDisplayObjecChange);
                        this.displayObjectTrait.addEventListener(DisplayObjectEvent.MEDIA_SIZE_CHANGE, this.onDisplayObjectTraitMediaSizeChange);
                    };
                    this.updateDisplayObject(((this.displayObjectTrait) ? this.displayObjectTrait.displayObject : null));
                };
            };
        }
        private function onMetadataAdd(_arg1:MediaElementEvent):void{
            if (_arg1.namespaceURL == LayoutMetadata.LAYOUT_NAMESPACE){
                this._layoutMetadata = (_arg1.metadata as LayoutMetadata);
            };
        }
        private function onMetadataRemove(_arg1:MediaElementEvent):void{
            if (_arg1.namespaceURL == LayoutMetadata.LAYOUT_NAMESPACE){
                this._layoutMetadata = null;
            };
        }
        private function updateDisplayObject(_arg1:DisplayObject):void{
            var _local2:DisplayObject = this._displayObject;
            if (_arg1 != this.displayObject){
                this._displayObject = _arg1;
                dispatchEvent(new DisplayObjectEvent(DisplayObjectEvent.DISPLAY_OBJECT_CHANGE, false, false, _local2, _arg1));
            };
            if ((((_arg1 is ILayoutTarget)) && (this.renderers.parentRenderer))){
                ILayoutTarget(_arg1).dispatchEvent(new LayoutTargetEvent(LayoutTargetEvent.ADD_TO_LAYOUT_RENDERER, false, false, this.renderers.parentRenderer));
            };
        }
        private function onDisplayObjectTraitDisplayObjecChange(_arg1:DisplayObjectEvent):void{
            this.updateDisplayObject(_arg1.newDisplayObject);
        }
        private function onDisplayObjectTraitMediaSizeChange(_arg1:DisplayObjectEvent):void{
            dispatchEvent(_arg1.clone());
        }
        private function onAddChildAt(_arg1:LayoutTargetEvent):void{
            if ((this._displayObject is ILayoutTarget)){
                ILayoutTarget(this._displayObject).dispatchEvent(_arg1.clone());
            } else {
                if ((this._displayObject is DisplayObjectContainer)){
                    DisplayObjectContainer(this._displayObject).addChildAt(_arg1.displayObject, _arg1.index);
                };
            };
        }
        private function onRemoveChild(_arg1:LayoutTargetEvent):void{
            if ((this._displayObject is ILayoutTarget)){
                ILayoutTarget(this._displayObject).dispatchEvent(_arg1.clone());
            } else {
                if ((this._displayObject is DisplayObjectContainer)){
                    DisplayObjectContainer(this._displayObject).removeChild(_arg1.displayObject);
                };
            };
        }
        private function onSetChildIndex(_arg1:LayoutTargetEvent):void{
            if ((this._displayObject is ILayoutTarget)){
                ILayoutTarget(this._displayObject).dispatchEvent(_arg1.clone());
            } else {
                if ((this._displayObject is DisplayObjectContainer)){
                    DisplayObjectContainer(this._displayObject).setChildIndex(_arg1.displayObject, _arg1.index);
                };
            };
        }

    }
}//package org.osmf.layout 

class ConstructorLock {

    public function ConstructorLock(){
    }
}
