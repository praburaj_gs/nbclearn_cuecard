﻿package org.osmf.layout {
    import org.osmf.layout.*;
    import org.osmf.utils.*;

    class BinarySearch {

        public static function search(_arg1:Object, _arg2:Function, _arg3, _arg4:int=0, _arg5:int=-2147483648):int{
            var _local7:int;
            var _local8:*;
            if ((((_arg1 == null)) || ((_arg2 == null)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            var _local6:int = -(_arg4);
            _arg5 = (((_arg5 == int.MIN_VALUE)) ? (_arg1.length - 1) : _arg5);
            if ((((_arg1.length > 0)) && ((_arg4 <= _arg5)))){
                _local7 = ((_arg4 + _arg5) / 2);
                _local8 = _arg1[_local7];
                switch (_arg2(_arg3, _local8)){
                    case -1:
                        _local6 = search(_arg1, _arg2, _arg3, _arg4, (_local7 - 1));
                        break;
                    case 0:
                        _local6 = _local7;
                        break;
                    case 1:
                        _local6 = search(_arg1, _arg2, _arg3, (_local7 + 1), _arg5);
                        break;
                };
            };
            return (_local6);
        }

    }
}//package org.osmf.layout 
