﻿package org.osmf.layout {
    import org.osmf.metadata.*;
    import org.osmf.events.*;
    import org.osmf.layout.*;

    class LayoutAttributesMetadata extends NonSynthesizingMetadata {

        public static const SCALE_MODE:String = "scaleMode";
        public static const VERTICAL_ALIGN:String = "verticalAlign";
        public static const HORIZONTAL_ALIGN:String = "horizontalAlign";
        public static const SNAP_TO_PIXEL:String = "snapToPixel";
        public static const MODE:String = "layoutMode";
        public static const INCLUDE_IN_LAYOUT:String = "includeInLayout";

        private var _scaleMode:String;
        private var _verticalAlign:String;
        private var _horizontalAlign:String;
        private var _snapToPixel:Boolean;
        private var _layoutMode:String;
        private var _includeInLayout:Boolean;

        public function LayoutAttributesMetadata(){
            this._verticalAlign = null;
            this._horizontalAlign = null;
            this._scaleMode = null;
            this._snapToPixel = true;
            this._layoutMode = LayoutMode.NONE;
            this._includeInLayout = true;
        }
        override public function getValue(_arg1:String){
            if (_arg1 == null){
                return (undefined);
            };
            if (_arg1 == SCALE_MODE){
                return (this.scaleMode);
            };
            if (_arg1 == VERTICAL_ALIGN){
                return (this.verticalAlign);
            };
            if (_arg1 == HORIZONTAL_ALIGN){
                return (this.horizontalAlign);
            };
            if (_arg1 == SNAP_TO_PIXEL){
                return (this.snapToPixel);
            };
            if (_arg1 == INCLUDE_IN_LAYOUT){
                return (this.snapToPixel);
            };
            return (undefined);
        }
        public function get scaleMode():String{
            return (this._scaleMode);
        }
        public function set scaleMode(_arg1:String):void{
            var _local2:MetadataEvent;
            if (this._scaleMode != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, SCALE_MODE, _arg1, this._scaleMode);
                this._scaleMode = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get verticalAlign():String{
            return (this._verticalAlign);
        }
        public function set verticalAlign(_arg1:String):void{
            var _local2:MetadataEvent;
            if (this._verticalAlign != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, VERTICAL_ALIGN, _arg1, this._verticalAlign);
                this._verticalAlign = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get horizontalAlign():String{
            return (this._horizontalAlign);
        }
        public function set horizontalAlign(_arg1:String):void{
            var _local2:MetadataEvent;
            if (this._horizontalAlign != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, HORIZONTAL_ALIGN, _arg1, this._horizontalAlign);
                this._horizontalAlign = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get snapToPixel():Boolean{
            return (this._snapToPixel);
        }
        public function set snapToPixel(_arg1:Boolean):void{
            var _local2:MetadataEvent;
            if (this._snapToPixel != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, SNAP_TO_PIXEL, _arg1, this._snapToPixel);
                this._snapToPixel = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get layoutMode():String{
            return (this._layoutMode);
        }
        public function set layoutMode(_arg1:String):void{
            var _local2:MetadataEvent;
            if (this._layoutMode != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, MODE, _arg1, this._layoutMode);
                this._layoutMode = _arg1;
                dispatchEvent(_local2);
            };
        }
        public function get includeInLayout():Boolean{
            return (this._includeInLayout);
        }
        public function set includeInLayout(_arg1:Boolean):void{
            var _local2:MetadataEvent;
            if (this._includeInLayout != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, INCLUDE_IN_LAYOUT, _arg1, this._layoutMode);
                this._includeInLayout = _arg1;
                dispatchEvent(_local2);
            };
        }

    }
}//package org.osmf.layout 
