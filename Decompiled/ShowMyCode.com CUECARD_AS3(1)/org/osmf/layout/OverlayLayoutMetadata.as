﻿package org.osmf.layout {
    import org.osmf.metadata.*;
    import org.osmf.events.*;

    class OverlayLayoutMetadata extends NonSynthesizingMetadata {

        public static const INDEX:String = "index";

        private var _index:Number = NaN;

        override public function getValue(_arg1:String){
            if (_arg1 == INDEX){
                return (this.index);
            };
            return (undefined);
        }
        public function get index():Number{
            return (this._index);
        }
        public function set index(_arg1:Number):void{
            var _local2:MetadataEvent;
            if (this._index != _arg1){
                _local2 = new MetadataEvent(MetadataEvent.VALUE_CHANGE, false, false, INDEX, _arg1, this._index);
                this._index = _arg1;
                dispatchEvent(_local2);
            };
        }

    }
}//package org.osmf.layout 
