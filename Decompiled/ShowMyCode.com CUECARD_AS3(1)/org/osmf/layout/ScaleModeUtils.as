﻿package org.osmf.layout {
    import org.osmf.layout.*;
    import flash.geom.*;

    class ScaleModeUtils {

        public static function getScaledSize(_arg1:String, _arg2:Number, _arg3:Number, _arg4:Number, _arg5:Number):Point{
            var _local6:Point;
            var _local7:Number;
            var _local8:Number;
            switch (_arg1){
                case ScaleMode.ZOOM:
                case ScaleMode.LETTERBOX:
                    _local7 = (_arg2 / _arg3);
                    _local8 = (((_arg4) || (_arg2)) / ((_arg5) || (_arg3)));
                    if ((((((_arg1 == ScaleMode.ZOOM)) && ((_local8 < _local7)))) || ((((_arg1 == ScaleMode.LETTERBOX)) && ((_local8 > _local7)))))){
                        _local6 = new Point(_arg2, (_arg2 / _local8));
                    } else {
                        _local6 = new Point((_arg3 * _local8), _arg3);
                    };
                    break;
                case ScaleMode.STRETCH:
                    _local6 = new Point(_arg2, _arg3);
                    break;
                case ScaleMode.NONE:
                    _local6 = new Point(((_arg4) || (_arg2)), ((_arg5) || (_arg3)));
                    break;
            };
            return (_local6);
        }

    }
}//package org.osmf.layout 
