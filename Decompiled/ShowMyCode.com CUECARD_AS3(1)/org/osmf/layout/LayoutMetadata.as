﻿package org.osmf.layout {
    import org.osmf.metadata.*;

    public class LayoutMetadata extends Metadata {

        public static const LAYOUT_NAMESPACE:String = "http://www.osmf.org/layout/1.0";

        private const SYNTHESIZER:NullMetadataSynthesizer;

        public function LayoutMetadata(){
            this.SYNTHESIZER = new NullMetadataSynthesizer();
            super();
        }
        public function get index():Number{
            return (((this.lazyOverlay) ? this.lazyOverlay.index : NaN));
        }
        public function set index(_arg1:Number):void{
            this.eagerOverlay.index = _arg1;
        }
        public function get scaleMode():String{
            return (((this.lazyAttributes) ? this.lazyAttributes.scaleMode : null));
        }
        public function set scaleMode(_arg1:String):void{
            this.eagerAttributes.scaleMode = _arg1;
        }
        public function get horizontalAlign():String{
            return (((this.lazyAttributes) ? this.lazyAttributes.horizontalAlign : null));
        }
        public function set horizontalAlign(_arg1:String):void{
            this.eagerAttributes.horizontalAlign = _arg1;
        }
        public function get verticalAlign():String{
            return (((this.lazyAttributes) ? this.lazyAttributes.verticalAlign : null));
        }
        public function set verticalAlign(_arg1:String):void{
            this.eagerAttributes.verticalAlign = _arg1;
        }
        public function get snapToPixel():Boolean{
            return (((this.lazyAttributes) ? this.lazyAttributes.snapToPixel : true));
        }
        public function set snapToPixel(_arg1:Boolean):void{
            this.eagerAttributes.snapToPixel = _arg1;
        }
        public function get layoutMode():String{
            return (((this.lazyAttributes) ? this.lazyAttributes.layoutMode : LayoutMode.NONE));
        }
        public function set layoutMode(_arg1:String):void{
            this.eagerAttributes.layoutMode = _arg1;
        }
        public function get includeInLayout():Boolean{
            return (((this.lazyAttributes) ? this.lazyAttributes.includeInLayout : true));
        }
        public function set includeInLayout(_arg1:Boolean):void{
            this.eagerAttributes.includeInLayout = _arg1;
        }
        public function get x():Number{
            return (((this.lazyAbsolute) ? this.lazyAbsolute.x : NaN));
        }
        public function set x(_arg1:Number):void{
            this.eagerAbsolute.x = _arg1;
        }
        public function get y():Number{
            return (((this.lazyAbsolute) ? this.lazyAbsolute.y : NaN));
        }
        public function set y(_arg1:Number):void{
            this.eagerAbsolute.y = _arg1;
        }
        public function get width():Number{
            return (((this.lazyAbsolute) ? this.lazyAbsolute.width : NaN));
        }
        public function set width(_arg1:Number):void{
            this.eagerAbsolute.width = _arg1;
        }
        public function get height():Number{
            return (((this.lazyAbsolute) ? this.lazyAbsolute.height : NaN));
        }
        public function set height(_arg1:Number):void{
            this.eagerAbsolute.height = _arg1;
        }
        public function get percentX():Number{
            return (((this.lazyRelative) ? this.lazyRelative.x : NaN));
        }
        public function set percentX(_arg1:Number):void{
            this.eagerRelative.x = _arg1;
        }
        public function get percentY():Number{
            return (((this.lazyRelative) ? this.lazyRelative.y : NaN));
        }
        public function set percentY(_arg1:Number):void{
            this.eagerRelative.y = _arg1;
        }
        public function get percentWidth():Number{
            return (((this.lazyRelative) ? this.lazyRelative.width : NaN));
        }
        public function set percentWidth(_arg1:Number):void{
            this.eagerRelative.width = _arg1;
        }
        public function get percentHeight():Number{
            return (((this.lazyRelative) ? this.lazyRelative.height : NaN));
        }
        public function set percentHeight(_arg1:Number):void{
            this.eagerRelative.height = _arg1;
        }
        public function get left():Number{
            return (((this.lazyAnchor) ? this.lazyAnchor.left : NaN));
        }
        public function set left(_arg1:Number):void{
            this.eagerAnchor.left = _arg1;
        }
        public function get top():Number{
            return (((this.lazyAnchor) ? this.lazyAnchor.top : NaN));
        }
        public function set top(_arg1:Number):void{
            this.eagerAnchor.top = _arg1;
        }
        public function get right():Number{
            return (((this.lazyAnchor) ? this.lazyAnchor.right : NaN));
        }
        public function set right(_arg1:Number):void{
            this.eagerAnchor.right = _arg1;
        }
        public function get bottom():Number{
            return (((this.lazyAnchor) ? this.lazyAnchor.bottom : NaN));
        }
        public function set bottom(_arg1:Number):void{
            this.eagerAnchor.bottom = _arg1;
        }
        public function get paddingLeft():Number{
            return (((this.lazyPadding) ? this.lazyPadding.left : NaN));
        }
        public function set paddingLeft(_arg1:Number):void{
            this.eagerPadding.left = _arg1;
        }
        public function get paddingTop():Number{
            return (((this.lazyPadding) ? this.lazyPadding.top : NaN));
        }
        public function set paddingTop(_arg1:Number):void{
            this.eagerPadding.top = _arg1;
        }
        public function get paddingRight():Number{
            return (((this.lazyPadding) ? this.lazyPadding.right : NaN));
        }
        public function set paddingRight(_arg1:Number):void{
            this.eagerPadding.right = _arg1;
        }
        public function get paddingBottom():Number{
            return (((this.lazyPadding) ? this.lazyPadding.bottom : NaN));
        }
        public function set paddingBottom(_arg1:Number):void{
            this.eagerPadding.bottom = _arg1;
        }
        override public function toString():String{
            return ((((((((((((((((((((((((((((((((((((((((((((((((((((("abs [" + this.x) + ", ") + this.y) + ", ") + this.width) + ", ") + this.height) + "] ") + "rel [") + this.percentX) + ", ") + this.percentY) + ", ") + this.percentWidth) + ", ") + this.percentHeight) + "] ") + "anch (") + this.left) + ", ") + this.top) + ")(") + this.right) + ", ") + this.bottom) + ") ") + "pad [") + this.paddingLeft) + ", ") + this.paddingTop) + ", ") + this.paddingRight) + ", ") + this.paddingBottom) + "] ") + "layoutMode: ") + this.layoutMode) + " ") + "index: ") + this.index) + " ") + "scale: ") + this.scaleMode) + " ") + "valign: ") + this.verticalAlign) + " ") + "halign: ") + this.horizontalAlign) + " ") + "snap: ") + this.snapToPixel));
        }
        override public function get synthesizer():MetadataSynthesizer{
            return (this.SYNTHESIZER);
        }
        private function get lazyAttributes():LayoutAttributesMetadata{
            return ((getValue(MetadataNamespaces.LAYOUT_ATTRIBUTES) as LayoutAttributesMetadata));
        }
        private function get eagerAttributes():LayoutAttributesMetadata{
            var _local1:LayoutAttributesMetadata = this.lazyAttributes;
            if (_local1 == null){
                _local1 = new LayoutAttributesMetadata();
                addValue(MetadataNamespaces.LAYOUT_ATTRIBUTES, _local1);
            };
            return (_local1);
        }
        private function get lazyOverlay():OverlayLayoutMetadata{
            return ((getValue(MetadataNamespaces.OVERLAY_LAYOUT_PARAMETERS) as OverlayLayoutMetadata));
        }
        private function get eagerOverlay():OverlayLayoutMetadata{
            var _local1:OverlayLayoutMetadata = this.lazyOverlay;
            if (_local1 == null){
                _local1 = new OverlayLayoutMetadata();
                addValue(MetadataNamespaces.OVERLAY_LAYOUT_PARAMETERS, _local1);
            };
            return (_local1);
        }
        private function get lazyAbsolute():AbsoluteLayoutMetadata{
            return ((getValue(MetadataNamespaces.ABSOLUTE_LAYOUT_PARAMETERS) as AbsoluteLayoutMetadata));
        }
        private function get eagerAbsolute():AbsoluteLayoutMetadata{
            var _local1:AbsoluteLayoutMetadata = this.lazyAbsolute;
            if (_local1 == null){
                _local1 = new AbsoluteLayoutMetadata();
                addValue(MetadataNamespaces.ABSOLUTE_LAYOUT_PARAMETERS, _local1);
            };
            return (_local1);
        }
        private function get lazyRelative():RelativeLayoutMetadata{
            return ((getValue(MetadataNamespaces.RELATIVE_LAYOUT_PARAMETERS) as RelativeLayoutMetadata));
        }
        private function get eagerRelative():RelativeLayoutMetadata{
            var _local1:RelativeLayoutMetadata = this.lazyRelative;
            if (_local1 == null){
                _local1 = new RelativeLayoutMetadata();
                addValue(MetadataNamespaces.RELATIVE_LAYOUT_PARAMETERS, _local1);
            };
            return (_local1);
        }
        private function get lazyAnchor():AnchorLayoutMetadata{
            return ((getValue(MetadataNamespaces.ANCHOR_LAYOUT_PARAMETERS) as AnchorLayoutMetadata));
        }
        private function get eagerAnchor():AnchorLayoutMetadata{
            var _local1:AnchorLayoutMetadata = this.lazyAnchor;
            if (_local1 == null){
                _local1 = new AnchorLayoutMetadata();
                addValue(MetadataNamespaces.ANCHOR_LAYOUT_PARAMETERS, _local1);
            };
            return (_local1);
        }
        private function get lazyPadding():PaddingLayoutMetadata{
            return ((getValue(MetadataNamespaces.PADDING_LAYOUT_PARAMETERS) as PaddingLayoutMetadata));
        }
        private function get eagerPadding():PaddingLayoutMetadata{
            var _local1:PaddingLayoutMetadata = this.lazyPadding;
            if (_local1 == null){
                _local1 = new PaddingLayoutMetadata();
                addValue(MetadataNamespaces.PADDING_LAYOUT_PARAMETERS, _local1);
            };
            return (_local1);
        }

    }
}//package org.osmf.layout 
