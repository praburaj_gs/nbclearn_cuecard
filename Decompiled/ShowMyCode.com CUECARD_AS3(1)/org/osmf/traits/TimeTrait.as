﻿package org.osmf.traits {
    import org.osmf.events.*;

    public class TimeTrait extends MediaTraitBase {

        private var _duration:Number;
        private var _currentTime:Number;

        public function TimeTrait(_arg1:Number=NaN){
            super(MediaTraitType.TIME);
            this._duration = _arg1;
        }
        public function get duration():Number{
            return (this._duration);
        }
        public function get currentTime():Number{
            return (this._currentTime);
        }
        protected function durationChangeStart(_arg1:Number):void{
        }
        protected function durationChangeEnd(_arg1:Number):void{
            dispatchEvent(new TimeEvent(TimeEvent.DURATION_CHANGE, false, false, this._duration));
        }
        protected function currentTimeChangeStart(_arg1:Number):void{
        }
        protected function currentTimeChangeEnd(_arg1:Number):void{
        }
        protected function signalComplete():void{
            dispatchEvent(new TimeEvent(TimeEvent.COMPLETE));
        }
        final protected function setCurrentTime(_arg1:Number):void{
            var _local2:Number;
            if (!isNaN(_arg1)){
                if (!isNaN(this._duration)){
                    _arg1 = Math.min(_arg1, this._duration);
                } else {
                    _arg1 = 0;
                };
            };
            if (((!((this._currentTime == _arg1))) && (!(((isNaN(this._currentTime)) && (isNaN(_arg1))))))){
                this.currentTimeChangeStart(_arg1);
                _local2 = this._currentTime;
                this._currentTime = _arg1;
                this.currentTimeChangeEnd(_local2);
                if ((((this.currentTime == this.duration)) && ((this.currentTime > 0)))){
                    this.signalComplete();
                };
            };
        }
        final protected function setDuration(_arg1:Number):void{
            var _local2:Number;
            if (this._duration != _arg1){
                this.durationChangeStart(_arg1);
                _local2 = this._duration;
                this._duration = _arg1;
                this.durationChangeEnd(_local2);
                if (((((!(isNaN(this._currentTime))) && (!(isNaN(this._duration))))) && ((this._currentTime > this._duration)))){
                    this.setCurrentTime(this.duration);
                };
            };
        }

    }
}//package org.osmf.traits 
