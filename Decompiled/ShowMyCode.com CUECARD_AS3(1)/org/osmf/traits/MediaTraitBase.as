﻿package org.osmf.traits {
    import flash.events.*;

    public class MediaTraitBase extends EventDispatcher {

        private var _traitType:String;

        public function MediaTraitBase(_arg1:String){
            this._traitType = _arg1;
        }
        public function get traitType():String{
            return (this._traitType);
        }
        public function dispose():void{
        }

    }
}//package org.osmf.traits 
