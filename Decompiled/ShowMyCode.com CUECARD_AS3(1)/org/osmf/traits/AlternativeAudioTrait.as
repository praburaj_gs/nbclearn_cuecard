﻿package org.osmf.traits {
    import org.osmf.net.*;
    import org.osmf.events.*;
    import org.osmf.utils.*;

    public class AlternativeAudioTrait extends MediaTraitBase {

        protected static const INVALID_TRANSITION_INDEX:int = -2;
        protected static const DEFAULT_TRANSITION_INDEX:int = -1;

        private var _currentIndex:int = -1;
        private var _numAlternativeAudioStreams:int;
        private var _switching:Boolean;
        protected var _indexToSwitchTo:int = -2;

        public function AlternativeAudioTrait(_arg1:int){
            super(MediaTraitType.ALTERNATIVE_AUDIO);
            this._numAlternativeAudioStreams = _arg1;
            this._switching = false;
        }
        public function get numAlternativeAudioStreams():int{
            return (this._numAlternativeAudioStreams);
        }
        public function get currentIndex():int{
            return (this._currentIndex);
        }
        public function getItemForIndex(_arg1:int):StreamingItem{
            if ((((_arg1 <= INVALID_TRANSITION_INDEX)) || ((_arg1 >= this.numAlternativeAudioStreams)))){
                throw (new RangeError(OSMFStrings.getString(OSMFStrings.ALTERNATIVEAUDIO_INVALID_INDEX)));
            };
            return (null);
        }
        public function get switching():Boolean{
            return (this._switching);
        }
        public function switchTo(_arg1:int):void{
            if (_arg1 != this._indexToSwitchTo){
                if ((((_arg1 < INVALID_TRANSITION_INDEX)) || ((_arg1 >= this.numAlternativeAudioStreams)))){
                    throw (new RangeError(OSMFStrings.getString(OSMFStrings.ALTERNATIVEAUDIO_INVALID_INDEX)));
                };
                this.setSwitching(true, _arg1);
            };
        }
        final protected function setCurrentIndex(_arg1:int):void{
            this._currentIndex = _arg1;
        }
        final protected function setSwitching(_arg1:Boolean, _arg2:int):void{
            if (((!((_arg1 == this._switching))) || (!((_arg2 == this._indexToSwitchTo))))){
                this.beginSwitching(_arg1, _arg2);
                this._switching = _arg1;
                if (this._switching == false){
                    this.setCurrentIndex(_arg2);
                };
                this.endSwitching(_arg2);
            };
        }
        protected function beginSwitching(_arg1:Boolean, _arg2:int):void{
            if (_arg1){
                this._indexToSwitchTo = _arg2;
            };
        }
        protected function endSwitching(_arg1:int):void{
            if (!this._switching){
                this._indexToSwitchTo = INVALID_TRANSITION_INDEX;
            };
            dispatchEvent(new AlternativeAudioEvent(AlternativeAudioEvent.AUDIO_SWITCHING_CHANGE, false, false, this.switching));
        }

    }
}//package org.osmf.traits 
