﻿package org.osmf.traits {
    import flash.events.*;
    import org.osmf.media.*;
    import org.osmf.events.*;
    import flash.utils.*;

    public class TraitEventDispatcher extends EventDispatcher {

        private static var eventMaps:Dictionary;

        private var _mediaElement:MediaElement;

        public function TraitEventDispatcher(){
            if (eventMaps == null){
                eventMaps = new Dictionary();
                eventMaps[TimeEvent.DURATION_CHANGE] = MediaTraitType.TIME;
                eventMaps[TimeEvent.COMPLETE] = MediaTraitType.TIME;
                eventMaps[PlayEvent.PLAY_STATE_CHANGE] = MediaTraitType.PLAY;
                eventMaps[PlayEvent.CAN_PAUSE_CHANGE] = MediaTraitType.PLAY;
                eventMaps[AudioEvent.VOLUME_CHANGE] = MediaTraitType.AUDIO;
                eventMaps[AudioEvent.MUTED_CHANGE] = MediaTraitType.AUDIO;
                eventMaps[AudioEvent.PAN_CHANGE] = MediaTraitType.AUDIO;
                eventMaps[SeekEvent.SEEKING_CHANGE] = MediaTraitType.SEEK;
                eventMaps[DynamicStreamEvent.SWITCHING_CHANGE] = MediaTraitType.DYNAMIC_STREAM;
                eventMaps[DynamicStreamEvent.AUTO_SWITCH_CHANGE] = MediaTraitType.DYNAMIC_STREAM;
                eventMaps[DynamicStreamEvent.NUM_DYNAMIC_STREAMS_CHANGE] = MediaTraitType.DYNAMIC_STREAM;
                eventMaps[AlternativeAudioEvent.AUDIO_SWITCHING_CHANGE] = MediaTraitType.ALTERNATIVE_AUDIO;
                eventMaps[AlternativeAudioEvent.NUM_ALTERNATIVE_AUDIO_STREAMS_CHANGE] = MediaTraitType.ALTERNATIVE_AUDIO;
                eventMaps[DisplayObjectEvent.DISPLAY_OBJECT_CHANGE] = MediaTraitType.DISPLAY_OBJECT;
                eventMaps[DisplayObjectEvent.MEDIA_SIZE_CHANGE] = MediaTraitType.DISPLAY_OBJECT;
                eventMaps[LoadEvent.LOAD_STATE_CHANGE] = MediaTraitType.LOAD;
                eventMaps[LoadEvent.BYTES_LOADED_CHANGE] = MediaTraitType.LOAD;
                eventMaps[LoadEvent.BYTES_TOTAL_CHANGE] = MediaTraitType.LOAD;
                eventMaps[BufferEvent.BUFFERING_CHANGE] = MediaTraitType.BUFFER;
                eventMaps[BufferEvent.BUFFER_TIME_CHANGE] = MediaTraitType.BUFFER;
                eventMaps[DRMEvent.DRM_STATE_CHANGE] = MediaTraitType.DRM;
                eventMaps[DVREvent.IS_RECORDING_CHANGE] = MediaTraitType.DVR;
            };
        }
        public function get media():MediaElement{
            return (this._mediaElement);
        }
        public function set media(_arg1:MediaElement):void{
            var _local2:String;
            if (_arg1 != this._mediaElement){
                if (this._mediaElement != null){
                    this._mediaElement.removeEventListener(MediaElementEvent.TRAIT_ADD, this.onTraitAdd);
                    this._mediaElement.removeEventListener(MediaElementEvent.TRAIT_REMOVE, this.onTraitRemove);
                    for each (_local2 in this._mediaElement.traitTypes) {
                        this.onTraitChanged(_local2, false);
                    };
                };
                this._mediaElement = _arg1;
                if (this._mediaElement != null){
                    this._mediaElement.addEventListener(MediaElementEvent.TRAIT_ADD, this.onTraitAdd);
                    this._mediaElement.addEventListener(MediaElementEvent.TRAIT_REMOVE, this.onTraitRemove);
                    for each (_local2 in this._mediaElement.traitTypes) {
                        this.onTraitChanged(_local2, true);
                    };
                };
            };
        }
        override public function addEventListener(_arg1:String, _arg2:Function, _arg3:Boolean=false, _arg4:int=0, _arg5:Boolean=false):void{
            var _local6:Boolean = hasEventListener(_arg1);
            super.addEventListener(_arg1, _arg2, _arg3, _arg4, _arg5);
            if (((((this._mediaElement) && (!(_local6)))) && (!((eventMaps[_arg1] == undefined))))){
                this.changeListeners(true, eventMaps[_arg1], _arg1);
            };
        }
        override public function removeEventListener(_arg1:String, _arg2:Function, _arg3:Boolean=false):void{
            super.removeEventListener(_arg1, _arg2, _arg3);
            if (((((this._mediaElement) && (!(hasEventListener(_arg1))))) && (!((eventMaps[_arg1] == undefined))))){
                this.changeListeners(false, eventMaps[_arg1], _arg1);
            };
        }
        private function onTraitAdd(_arg1:MediaElementEvent):void{
            this.onTraitChanged(_arg1.traitType, true);
        }
        private function onTraitRemove(_arg1:MediaElementEvent):void{
            this.onTraitChanged(_arg1.traitType, false);
        }
        private function onTraitChanged(_arg1:String, _arg2:Boolean):void{
            switch (_arg1){
                case MediaTraitType.TIME:
                    this.changeListeners(_arg2, _arg1, TimeEvent.DURATION_CHANGE);
                    this.changeListeners(_arg2, _arg1, TimeEvent.COMPLETE);
                    break;
                case MediaTraitType.PLAY:
                    this.changeListeners(_arg2, _arg1, PlayEvent.PLAY_STATE_CHANGE);
                    this.changeListeners(_arg2, _arg1, PlayEvent.CAN_PAUSE_CHANGE);
                    break;
                case MediaTraitType.AUDIO:
                    this.changeListeners(_arg2, _arg1, AudioEvent.VOLUME_CHANGE);
                    this.changeListeners(_arg2, _arg1, AudioEvent.MUTED_CHANGE);
                    this.changeListeners(_arg2, _arg1, AudioEvent.PAN_CHANGE);
                    break;
                case MediaTraitType.SEEK:
                    this.changeListeners(_arg2, _arg1, SeekEvent.SEEKING_CHANGE);
                    break;
                case MediaTraitType.DYNAMIC_STREAM:
                    this.changeListeners(_arg2, _arg1, DynamicStreamEvent.SWITCHING_CHANGE);
                    this.changeListeners(_arg2, _arg1, DynamicStreamEvent.AUTO_SWITCH_CHANGE);
                    this.changeListeners(_arg2, _arg1, DynamicStreamEvent.NUM_DYNAMIC_STREAMS_CHANGE);
                    break;
                case MediaTraitType.ALTERNATIVE_AUDIO:
                    this.changeListeners(_arg2, _arg1, AlternativeAudioEvent.AUDIO_SWITCHING_CHANGE);
                    this.changeListeners(_arg2, _arg1, AlternativeAudioEvent.NUM_ALTERNATIVE_AUDIO_STREAMS_CHANGE);
                    break;
                case MediaTraitType.DISPLAY_OBJECT:
                    this.changeListeners(_arg2, _arg1, DisplayObjectEvent.DISPLAY_OBJECT_CHANGE);
                    this.changeListeners(_arg2, _arg1, DisplayObjectEvent.MEDIA_SIZE_CHANGE);
                    break;
                case MediaTraitType.LOAD:
                    this.changeListeners(_arg2, _arg1, LoadEvent.LOAD_STATE_CHANGE);
                    this.changeListeners(_arg2, _arg1, LoadEvent.BYTES_TOTAL_CHANGE);
                    this.changeListeners(_arg2, _arg1, LoadEvent.BYTES_LOADED_CHANGE);
                    break;
                case MediaTraitType.BUFFER:
                    this.changeListeners(_arg2, _arg1, BufferEvent.BUFFERING_CHANGE);
                    this.changeListeners(_arg2, _arg1, BufferEvent.BUFFER_TIME_CHANGE);
                    break;
                case MediaTraitType.DRM:
                    this.changeListeners(_arg2, _arg1, DRMEvent.DRM_STATE_CHANGE);
                    break;
                case MediaTraitType.DVR:
                    this.changeListeners(_arg2, _arg1, DVREvent.IS_RECORDING_CHANGE);
                    break;
            };
        }
        private function changeListeners(_arg1:Boolean, _arg2:String, _arg3:String):void{
            if (this._mediaElement.getTrait(_arg2) != null){
                if (((_arg1) && (hasEventListener(_arg3)))){
                    this._mediaElement.getTrait(_arg2).addEventListener(_arg3, this.redispatchEvent);
                } else {
                    this._mediaElement.getTrait(_arg2).removeEventListener(_arg3, this.redispatchEvent);
                };
            };
        }
        private function redispatchEvent(_arg1:Event):void{
            dispatchEvent(_arg1.clone());
        }

    }
}//package org.osmf.traits 
