﻿package org.osmf.traits {
    import org.osmf.events.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class PlayTrait extends MediaTraitBase {

        private var _playState:String;
        private var _canPause:Boolean;

        public function PlayTrait(){
            super(MediaTraitType.PLAY);
            this._canPause = true;
            this._playState = PlayState.STOPPED;
        }
        final public function play():void{
            this.attemptPlayStateChange(PlayState.PLAYING);
        }
        public function get canPause():Boolean{
            return (this._canPause);
        }
        final public function pause():void{
            if (this.canPause){
                this.attemptPlayStateChange(PlayState.PAUSED);
            } else {
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.PAUSE_NOT_SUPPORTED)));
            };
        }
        final public function stop():void{
            this.attemptPlayStateChange(PlayState.STOPPED);
        }
        public function get playState():String{
            return (this._playState);
        }
        final protected function setCanPause(_arg1:Boolean):void{
            if (_arg1 != this._canPause){
                this._canPause = _arg1;
                dispatchEvent(new PlayEvent(PlayEvent.CAN_PAUSE_CHANGE, false, false, this.playState, this._canPause));
            };
        }
        protected function playStateChangeStart(_arg1:String):void{
        }
        protected function playStateChangeEnd():void{
            dispatchEvent(new PlayEvent(PlayEvent.PLAY_STATE_CHANGE, false, false, this.playState));
        }
        private function attemptPlayStateChange(_arg1:String):void{
            if (this._playState != _arg1){
                this.playStateChangeStart(_arg1);
                this._playState = _arg1;
                this.playStateChangeEnd();
            };
        }

    }
}//package org.osmf.traits 
