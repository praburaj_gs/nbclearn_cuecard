﻿package org.osmf.traits {
    import org.osmf.media.*;
    import org.osmf.events.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class LoadTrait extends MediaTraitBase {

        private var loader:LoaderBase;
        private var _resource:MediaResourceBase;
        private var _loadState:String;
        private var _bytesLoaded:Number;
        private var _bytesTotal:Number;

        public function LoadTrait(_arg1:LoaderBase, _arg2:MediaResourceBase){
            super(MediaTraitType.LOAD);
            this.loader = _arg1;
            this._resource = _arg2;
            this._loadState = LoadState.UNINITIALIZED;
            if (_arg1 != null){
                _arg1.addEventListener(LoaderEvent.LOAD_STATE_CHANGE, this.onLoadStateChange, false, int.MAX_VALUE, true);
            };
        }
        public function get resource():MediaResourceBase{
            return (this._resource);
        }
        public function get loadState():String{
            return (this._loadState);
        }
        public function load():void{
            if (this.loader){
                if (this._loadState == LoadState.READY){
                    throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.ALREADY_READY)));
                };
                if (this._loadState == LoadState.LOADING){
                    throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.ALREADY_LOADING)));
                };
                this.loader.load(this);
            } else {
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.MUST_SET_LOADER)));
            };
        }
        public function unload():void{
            if (this.loader){
                if (this._loadState == LoadState.UNLOADING){
                    throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.ALREADY_UNLOADING)));
                };
                if (this._loadState == LoadState.UNINITIALIZED){
                    throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.ALREADY_UNLOADED)));
                };
                this.loader.unload(this);
            } else {
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.MUST_SET_LOADER)));
            };
        }
        public function get bytesLoaded():Number{
            return (this._bytesLoaded);
        }
        public function get bytesTotal():Number{
            return (this._bytesTotal);
        }
        final protected function setLoadState(_arg1:String):void{
            if (this._loadState != _arg1){
                this.loadStateChangeStart(_arg1);
                this._loadState = _arg1;
                this.loadStateChangeEnd();
            };
        }
        final protected function setBytesLoaded(_arg1:Number):void{
            if (((((isNaN(_arg1)) || ((_arg1 > this.bytesTotal)))) || ((_arg1 < 0)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            if (_arg1 != this._bytesLoaded){
                this.bytesLoadedChangeStart(_arg1);
                this._bytesLoaded = _arg1;
                this.bytesLoadedChangeEnd();
            };
        }
        final protected function setBytesTotal(_arg1:Number):void{
            if ((((_arg1 < this._bytesLoaded)) || ((_arg1 < 0)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            if (_arg1 != this._bytesTotal){
                this.bytesTotalChangeStart(_arg1);
                this._bytesTotal = _arg1;
                this.bytesTotalChangeEnd();
            };
        }
        protected function bytesLoadedChangeStart(_arg1:Number):void{
        }
        protected function bytesLoadedChangeEnd():void{
        }
        protected function bytesTotalChangeStart(_arg1:Number):void{
        }
        protected function bytesTotalChangeEnd():void{
            dispatchEvent(new LoadEvent(LoadEvent.BYTES_TOTAL_CHANGE, false, false, null, this._bytesTotal));
        }
        protected function loadStateChangeStart(_arg1:String):void{
        }
        protected function loadStateChangeEnd():void{
            dispatchEvent(new LoadEvent(LoadEvent.LOAD_STATE_CHANGE, false, false, this._loadState));
        }
        private function onLoadStateChange(_arg1:LoaderEvent):void{
            if (_arg1.loadTrait == this){
                this.setLoadState(_arg1.newState);
            };
        }

    }
}//package org.osmf.traits 
