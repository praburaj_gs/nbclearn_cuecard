﻿package org.osmf.traits {
    import org.osmf.events.*;

    public class BufferTrait extends MediaTraitBase {

        private var _buffering:Boolean = false;
        private var _bufferLength:Number = 0;
        private var _bufferTime:Number = 0;

        public function BufferTrait(){
            super(MediaTraitType.BUFFER);
        }
        public function get buffering():Boolean{
            return (this._buffering);
        }
        public function get bufferLength():Number{
            return (this._bufferLength);
        }
        public function get bufferTime():Number{
            return (this._bufferTime);
        }
        public function set bufferTime(_arg1:Number):void{
            if (((isNaN(_arg1)) || ((_arg1 < 0)))){
                _arg1 = 0;
            };
            if (_arg1 != this._bufferTime){
                this.bufferTimeChangeStart(_arg1);
                this._bufferTime = _arg1;
                this.bufferTimeChangeEnd();
            };
        }
        final protected function setBufferLength(_arg1:Number):void{
            if (_arg1 != this._bufferLength){
                this.bufferLengthChangeStart(_arg1);
                this._bufferLength = _arg1;
                this.bufferLengthChangeEnd();
            };
        }
        final protected function setBuffering(_arg1:Boolean):void{
            if (_arg1 != this._buffering){
                this.bufferingChangeStart(_arg1);
                this._buffering = _arg1;
                this.bufferingChangeEnd();
            };
        }
        protected function bufferingChangeStart(_arg1:Boolean):void{
        }
        protected function bufferingChangeEnd():void{
            dispatchEvent(new BufferEvent(BufferEvent.BUFFERING_CHANGE, false, false, this._buffering));
        }
        protected function bufferLengthChangeStart(_arg1:Number):void{
        }
        protected function bufferLengthChangeEnd():void{
        }
        protected function bufferTimeChangeStart(_arg1:Number):void{
        }
        protected function bufferTimeChangeEnd():void{
            dispatchEvent(new BufferEvent(BufferEvent.BUFFER_TIME_CHANGE, false, false, false, this._bufferTime));
        }

    }
}//package org.osmf.traits 
