﻿package org.osmf.traits {

    public class DRMTrait extends MediaTraitBase {

        private var _drmState:String = "uninitialized";
        private var _period:Number = 0;
        private var _endDate:Date;
        private var _startDate:Date;

        public function DRMTrait(){
            super(MediaTraitType.DRM);
        }
        public function authenticate(_arg1:String=null, _arg2:String=null):void{
        }
        public function authenticateWithToken(_arg1:Object):void{
        }
        public function get drmState():String{
            return (this._drmState);
        }
        public function get startDate():Date{
            return (this._startDate);
        }
        public function get endDate():Date{
            return (this._endDate);
        }
        public function get period():Number{
            return (this._period);
        }
        final protected function setPeriod(_arg1:Number):void{
            this._period = _arg1;
        }
        final protected function setStartDate(_arg1:Date):void{
            this._startDate = _arg1;
        }
        final protected function setEndDate(_arg1:Date):void{
            this._endDate = _arg1;
        }
        final protected function setDrmState(_arg1:String):void{
            this._drmState = _arg1;
        }

    }
}//package org.osmf.traits 
