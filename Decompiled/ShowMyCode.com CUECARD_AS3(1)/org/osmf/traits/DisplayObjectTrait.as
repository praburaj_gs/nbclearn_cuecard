﻿package org.osmf.traits {
    import flash.display.*;
    import org.osmf.events.*;

    public class DisplayObjectTrait extends MediaTraitBase {

        private var _displayObject:DisplayObject;
        private var _mediaWidth:Number = 0;
        private var _mediaHeight:Number = 0;

        public function DisplayObjectTrait(_arg1:DisplayObject, _arg2:Number=0, _arg3:Number=0){
            super(MediaTraitType.DISPLAY_OBJECT);
            this._displayObject = _arg1;
            this._mediaWidth = _arg2;
            this._mediaHeight = _arg3;
        }
        public function get displayObject():DisplayObject{
            return (this._displayObject);
        }
        public function get mediaWidth():Number{
            return (this._mediaWidth);
        }
        public function get mediaHeight():Number{
            return (this._mediaHeight);
        }
        final protected function setDisplayObject(_arg1:DisplayObject):void{
            var _local2:DisplayObject;
            if (this._displayObject != _arg1){
                this.displayObjectChangeStart(_arg1);
                _local2 = this._displayObject;
                this._displayObject = _arg1;
                this.displayObjectChangeEnd(_local2);
            };
        }
        final protected function setMediaSize(_arg1:Number, _arg2:Number):void{
            var _local3:Number;
            var _local4:Number;
            if (((!((_arg1 == this._mediaWidth))) || (!((_arg2 == this._mediaHeight))))){
                this.mediaSizeChangeStart(_arg1, _arg2);
                _local3 = this._mediaWidth;
                _local4 = this._mediaHeight;
                this._mediaWidth = _arg1;
                this._mediaHeight = _arg2;
                this.mediaSizeChangeEnd(_local3, _local4);
            };
        }
        protected function displayObjectChangeStart(_arg1:DisplayObject):void{
        }
        protected function displayObjectChangeEnd(_arg1:DisplayObject):void{
            dispatchEvent(new DisplayObjectEvent(DisplayObjectEvent.DISPLAY_OBJECT_CHANGE, false, false, _arg1, this._displayObject));
        }
        protected function mediaSizeChangeStart(_arg1:Number, _arg2:Number):void{
        }
        protected function mediaSizeChangeEnd(_arg1:Number, _arg2:Number):void{
            dispatchEvent(new DisplayObjectEvent(DisplayObjectEvent.MEDIA_SIZE_CHANGE, false, false, null, null, _arg1, _arg2, this._mediaWidth, this._mediaHeight));
        }

    }
}//package org.osmf.traits 
