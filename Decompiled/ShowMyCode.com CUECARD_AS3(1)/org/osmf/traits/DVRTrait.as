﻿package org.osmf.traits {
    import org.osmf.events.*;

    public class DVRTrait extends MediaTraitBase {

        private var _isRecording:Boolean;
        private var _windowDuration:Number;

        public function DVRTrait(_arg1:Boolean=false, _arg2:Number=-1){
            this._isRecording = _arg1;
            this._windowDuration = _arg2;
            super(MediaTraitType.DVR);
        }
        final public function get windowDuration():Number{
            return (this._windowDuration);
        }
        final public function get isRecording():Boolean{
            return (this._isRecording);
        }
        final protected function setIsRecording(_arg1:Boolean):void{
            if (_arg1 != this._isRecording){
                this.isRecordingChangeStart(_arg1);
                this._isRecording = _arg1;
                this.isRecordingChangeEnd();
            };
        }
        protected function isRecordingChangeStart(_arg1:Boolean):void{
        }
        protected function isRecordingChangeEnd():void{
            dispatchEvent(new DVREvent(DVREvent.IS_RECORDING_CHANGE));
        }

    }
}//package org.osmf.traits 
