﻿package org.osmf.traits {
    import org.osmf.events.*;

    public class SeekTrait extends MediaTraitBase {

        private var _timeTrait:TimeTrait;
        private var _seeking:Boolean;

        public function SeekTrait(_arg1:TimeTrait){
            super(MediaTraitType.SEEK);
            this._timeTrait = _arg1;
        }
        final public function get seeking():Boolean{
            return (this._seeking);
        }
        final public function seek(_arg1:Number):void{
            if (this.canSeekTo(_arg1)){
                this.setSeeking(true, _arg1);
            };
        }
        public function canSeekTo(_arg1:Number):Boolean{
            return (((this._timeTrait) ? (((((isNaN(_arg1) == false)) && ((_arg1 >= 0)))) && ((((_arg1 <= this._timeTrait.duration)) || ((_arg1 <= this._timeTrait.currentTime))))) : false));
        }
        final protected function get timeTrait():TimeTrait{
            return (this._timeTrait);
        }
        final protected function set timeTrait(_arg1:TimeTrait):void{
            this._timeTrait = _arg1;
        }
        final protected function setSeeking(_arg1:Boolean, _arg2:Number):void{
            this.seekingChangeStart(_arg1, _arg2);
            this._seeking = _arg1;
            this.seekingChangeEnd(_arg2);
        }
        protected function seekingChangeStart(_arg1:Boolean, _arg2:Number):void{
        }
        protected function seekingChangeEnd(_arg1:Number):void{
            dispatchEvent(new SeekEvent(SeekEvent.SEEKING_CHANGE, false, false, this.seeking, _arg1));
        }

    }
}//package org.osmf.traits 
