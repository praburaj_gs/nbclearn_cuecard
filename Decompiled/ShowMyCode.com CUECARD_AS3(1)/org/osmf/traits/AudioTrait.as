﻿package org.osmf.traits {
    import org.osmf.events.*;

    public class AudioTrait extends MediaTraitBase {

        private var _volume:Number = 1;
        private var _muted:Boolean = false;
        private var _pan:Number = 0;

        public function AudioTrait(){
            super(MediaTraitType.AUDIO);
        }
        public function get volume():Number{
            return (this._volume);
        }
        final public function set volume(_arg1:Number):void{
            if (isNaN(_arg1)){
                _arg1 = 0;
            } else {
                if (_arg1 > 1){
                    _arg1 = 1;
                } else {
                    if (_arg1 < 0){
                        _arg1 = 0;
                    };
                };
            };
            if (_arg1 != this._volume){
                this.volumeChangeStart(_arg1);
                this._volume = _arg1;
                this.volumeChangeEnd();
            };
        }
        public function get muted():Boolean{
            return (this._muted);
        }
        final public function set muted(_arg1:Boolean):void{
            if (_arg1 != this._muted){
                this.mutedChangeStart(_arg1);
                this._muted = _arg1;
                this.mutedChangeEnd();
            };
        }
        public function get pan():Number{
            return (this._pan);
        }
        final public function set pan(_arg1:Number):void{
            if (isNaN(_arg1)){
                _arg1 = 0;
            } else {
                if (_arg1 > 1){
                    _arg1 = 1;
                } else {
                    if (_arg1 < -1){
                        _arg1 = -1;
                    };
                };
            };
            if (_arg1 != this._pan){
                this.panChangeStart(_arg1);
                this._pan = _arg1;
                this.panChangeEnd();
            };
        }
        protected function volumeChangeStart(_arg1:Number):void{
        }
        protected function volumeChangeEnd():void{
            dispatchEvent(new AudioEvent(AudioEvent.VOLUME_CHANGE, false, false, false, this._volume));
        }
        protected function mutedChangeStart(_arg1:Boolean):void{
        }
        protected function mutedChangeEnd():void{
            dispatchEvent(new AudioEvent(AudioEvent.MUTED_CHANGE, false, false, this._muted));
        }
        protected function panChangeStart(_arg1:Number):void{
        }
        protected function panChangeEnd():void{
            dispatchEvent(new AudioEvent(AudioEvent.PAN_CHANGE, false, false, false, NaN, this._pan));
        }

    }
}//package org.osmf.traits 
