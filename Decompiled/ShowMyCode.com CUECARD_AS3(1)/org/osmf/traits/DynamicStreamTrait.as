﻿package org.osmf.traits {
    import org.osmf.events.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class DynamicStreamTrait extends MediaTraitBase {

        private var _autoSwitch:Boolean;
        private var _currentIndex:int = 0;
        private var _maxAllowedIndex:int = 0;
        private var _numDynamicStreams:int;
        private var _switching:Boolean;

        public function DynamicStreamTrait(_arg1:Boolean=true, _arg2:int=0, _arg3:int=1){
            super(MediaTraitType.DYNAMIC_STREAM);
            this._autoSwitch = _arg1;
            this._currentIndex = _arg2;
            this._numDynamicStreams = _arg3;
            this._maxAllowedIndex = (_arg3 - 1);
            this._switching = false;
        }
        public function get autoSwitch():Boolean{
            return (this._autoSwitch);
        }
        final public function set autoSwitch(_arg1:Boolean):void{
            if (this.autoSwitch != _arg1){
                this.autoSwitchChangeStart(_arg1);
                this._autoSwitch = _arg1;
                this.autoSwitchChangeEnd();
            };
        }
        public function get numDynamicStreams():int{
            return (this._numDynamicStreams);
        }
        public function get currentIndex():int{
            return (this._currentIndex);
        }
        public function get maxAllowedIndex():int{
            return (this._maxAllowedIndex);
        }
        final public function set maxAllowedIndex(_arg1:int):void{
            if ((((_arg1 < 0)) || ((_arg1 > (this._numDynamicStreams - 1))))){
                throw (new RangeError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_INVALID_INDEX)));
            };
            if (this.maxAllowedIndex != _arg1){
                this.maxAllowedIndexChangeStart(_arg1);
                this._maxAllowedIndex = _arg1;
                this.maxAllowedIndexChangeEnd();
            };
        }
        public function getBitrateForIndex(_arg1:int):Number{
            if ((((_arg1 > (this._numDynamicStreams - 1))) || ((_arg1 < 0)))){
                throw (new RangeError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_INVALID_INDEX)));
            };
            return (0);
        }
        public function get switching():Boolean{
            return (this._switching);
        }
        public function switchTo(_arg1:int):void{
            if (this.autoSwitch){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_STREAM_NOT_IN_MANUAL_MODE)));
            };
            if ((((_arg1 < 0)) || ((_arg1 > this.maxAllowedIndex)))){
                throw (new RangeError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_INVALID_INDEX)));
            };
            if (!this.switching){
                this.setSwitching(true, _arg1);
            };
        }
        final protected function setNumDynamicStreams(_arg1:int):void{
            if (_arg1 != this._numDynamicStreams){
                this._numDynamicStreams = _arg1;
                if (this.maxAllowedIndex >= this._numDynamicStreams){
                    this.maxAllowedIndex = Math.max(0, (this._numDynamicStreams - 1));
                };
                dispatchEvent(new DynamicStreamEvent(DynamicStreamEvent.NUM_DYNAMIC_STREAMS_CHANGE));
            };
        }
        final protected function setCurrentIndex(_arg1:int):void{
            this._currentIndex = _arg1;
        }
        final protected function setSwitching(_arg1:Boolean, _arg2:int):void{
            this.switchingChangeStart(_arg1, _arg2);
            this._switching = _arg1;
            if (_arg1 == false){
                this.setCurrentIndex(_arg2);
            };
            this.switchingChangeEnd(_arg2);
        }
        protected function autoSwitchChangeStart(_arg1:Boolean):void{
        }
        protected function autoSwitchChangeEnd():void{
            dispatchEvent(new DynamicStreamEvent(DynamicStreamEvent.AUTO_SWITCH_CHANGE, false, false, false, this._autoSwitch));
        }
        protected function switchingChangeStart(_arg1:Boolean, _arg2:int):void{
        }
        protected function switchingChangeEnd(_arg1:int):void{
            dispatchEvent(new DynamicStreamEvent(DynamicStreamEvent.SWITCHING_CHANGE, false, false, this.switching, this._autoSwitch));
        }
        protected function maxAllowedIndexChangeStart(_arg1:int):void{
        }
        protected function maxAllowedIndexChangeEnd():void{
        }

    }
}//package org.osmf.traits 
