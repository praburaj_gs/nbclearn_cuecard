﻿package org.osmf.traits {
    import flash.events.*;
    import org.osmf.media.*;
    import org.osmf.events.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class LoaderBase extends EventDispatcher {

        public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            return (false);
        }
        final public function load(_arg1:LoadTrait):void{
            this.validateLoad(_arg1);
            this.executeLoad(_arg1);
        }
        final public function unload(_arg1:LoadTrait):void{
            this.validateUnload(_arg1);
            this.executeUnload(_arg1);
        }
        protected function executeLoad(_arg1:LoadTrait):void{
        }
        protected function executeUnload(_arg1:LoadTrait):void{
        }
        final protected function updateLoadTrait(_arg1:LoadTrait, _arg2:String):void{
            var _local3:String;
            if (_arg2 != _arg1.loadState){
                _local3 = _arg1.loadState;
                dispatchEvent(new LoaderEvent(LoaderEvent.LOAD_STATE_CHANGE, false, false, this, _arg1, _local3, _arg2));
            };
        }
        private function validateLoad(_arg1:LoadTrait):void{
            if (_arg1 == null){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            if (_arg1.loadState == LoadState.READY){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.ALREADY_READY)));
            };
            if (_arg1.loadState == LoadState.LOADING){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.ALREADY_LOADING)));
            };
            if (this.canHandleResource(_arg1.resource) == false){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.LOADER_CANT_HANDLE_RESOURCE)));
            };
        }
        private function validateUnload(_arg1:LoadTrait):void{
            if (_arg1 == null){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            if (_arg1.loadState == LoadState.UNLOADING){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.ALREADY_UNLOADING)));
            };
            if (_arg1.loadState == LoadState.UNINITIALIZED){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.ALREADY_UNLOADED)));
            };
            if (this.canHandleResource(_arg1.resource) == false){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.LOADER_CANT_HANDLE_RESOURCE)));
            };
        }

    }
}//package org.osmf.traits 
