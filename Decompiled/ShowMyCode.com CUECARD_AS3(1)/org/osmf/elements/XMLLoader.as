﻿package org.osmf.elements {
    import flash.events.*;
    import org.osmf.media.*;
    import org.osmf.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.utils.*;

    public class XMLLoader extends ManifestLoaderBase {

        public function XMLLoader(_arg1:MediaFactory=null){
            if (_arg1 == null){
                _arg1 = new DefaultMediaFactory();
            };
            this.factory = _arg1;
            this.builders = getBuilders();
        }
        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            if ((_arg1 is StreamingXMLResource)){
                return (true);
            };
            return (false);
        }
        override protected function executeLoad(_arg1:LoadTrait):void{
            var manifest:* = null;
            var resourceData:* = null;
            var loadTrait:* = _arg1;
            this.loadTrait = loadTrait;
            updateLoadTrait(loadTrait, LoadState.LOADING);
            try {
                resourceData = (loadTrait.resource as StreamingXMLResource).manifest;
                parser = getParser(resourceData);
                parser.addEventListener(ParseEvent.PARSE_COMPLETE, onParserLoadComplete);
                parser.addEventListener(ParseEvent.PARSE_ERROR, onParserLoadError);
                parserTimer = new Timer(OSMFSettings.f4mParseTimeout, 1);
                parserTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onParserTimerComplete);
                parserTimer.start();
                parser.parse(resourceData, URL.normalizePathForURL(StreamingXMLResource(loadTrait.resource).url, true));
            } catch(parseError:Error) {
                updateLoadTrait(loadTrait, LoadState.LOAD_ERROR);
                loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(parseError.errorID, parseError.message)));
            };
        }

    }
}//package org.osmf.elements 
