﻿package org.osmf.elements {
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.traits.*;
    import org.osmf.utils.*;
    import org.osmf.elements.loaderClasses.*;

    public class ImageLoader extends LoaderBase {

        private static const MIME_TYPES_SUPPORTED:Vector.<String> = Vector.<String>(["image/png", "image/gif", "image/jpeg"]);
        private static const MEDIA_TYPES_SUPPORTED:Vector.<String> = Vector.<String>([MediaType.IMAGE]);

        private var checkPolicyFile:Boolean;

        public function ImageLoader(_arg1:Boolean=true){
            this.checkPolicyFile = _arg1;
        }
        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            var _local4:URL;
            var _local2:int = MediaTypeUtil.checkMetadataMatchWithResource(_arg1, MEDIA_TYPES_SUPPORTED, MIME_TYPES_SUPPORTED);
            if (_local2 != MediaTypeUtil.METADATA_MATCH_UNKNOWN){
                return ((_local2 == MediaTypeUtil.METADATA_MATCH_FOUND));
            };
            var _local3:URLResource = (_arg1 as URLResource);
            if (((!((_local3 == null))) && (!((_local3.url == null))))){
                _local4 = new URL(_local3.url);
                return (!((_local4.path.search(/\.gif$|\.jpg$|\.png$/i) == -1)));
            };
            return (false);
        }
        override protected function executeLoad(_arg1:LoadTrait):void{
            LoaderUtils.loadLoadTrait(_arg1, updateLoadTrait, false, this.checkPolicyFile);
        }
        override protected function executeUnload(_arg1:LoadTrait):void{
            LoaderUtils.unloadLoadTrait(_arg1, updateLoadTrait);
        }

    }
}//package org.osmf.elements 
