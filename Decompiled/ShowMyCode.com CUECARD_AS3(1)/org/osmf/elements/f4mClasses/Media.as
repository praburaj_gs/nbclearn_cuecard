﻿package org.osmf.elements.f4mClasses {
    import org.osmf.elements.f4mClasses.*;
    import flash.utils.*;

    class Media {

        public var url:String;
        public var bitrate:Number;
        public var type:String;
        public var label:String;
        public var language:String;
        public var alternate:Boolean;
        public var drmAdditionalHeader:DRMAdditionalHeader;
        public var bootstrapInfo:BootstrapInfo;
        public var metadata:Object;
        public var xmp:ByteArray;
        public var moov:ByteArray;
        public var width:Number;
        public var height:Number;
        public var multicastGroupspec:String;
        public var multicastStreamName:String;

    }
}//package org.osmf.elements.f4mClasses 
