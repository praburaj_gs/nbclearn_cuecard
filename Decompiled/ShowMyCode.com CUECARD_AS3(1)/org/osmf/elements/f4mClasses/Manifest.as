﻿package org.osmf.elements.f4mClasses {
    import __AS3__.vec.*;
    import org.osmf.net.httpstreaming.dvr.*;

    public class Manifest {

        public var id:String;
        public var label:String;
        public var lang:String;
        public var baseURL:String;
        public var urlIncludesFMSApplicationInstance:Boolean = false;
        public var duration:Number;
        public var mimeType:String;
        public var streamType:String;
        public var deliveryType:String;
        public var startTime:Date;
        public var bootstrapInfos:Vector.<BootstrapInfo>;
        public var drmAdditionalHeaders:Vector.<DRMAdditionalHeader>;
        public var media:Vector.<Media>;
        public var alternativeMedia:Vector.<Media>;
        public var dvrInfo:DVRInfo = null;
        public var bestEffortFetchInfo:BestEffortFetchInfo = null;

        public function Manifest(){
            this.bootstrapInfos = new Vector.<BootstrapInfo>();
            this.drmAdditionalHeaders = new Vector.<DRMAdditionalHeader>();
            this.media = new Vector.<Media>();
            this.alternativeMedia = new Vector.<Media>();
            super();
        }
    }
}//package org.osmf.elements.f4mClasses 
