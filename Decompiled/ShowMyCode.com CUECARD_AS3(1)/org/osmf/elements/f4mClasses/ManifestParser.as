﻿package org.osmf.elements.f4mClasses {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.metadata.*;
    import org.osmf.net.*;
    import org.osmf.net.httpstreaming.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.net.httpstreaming.dvr.*;
    import org.osmf.utils.*;

    public class ManifestParser extends EventDispatcher {

        private var parsing:Boolean = false;
        private var unfinishedLoads:Number = 0;
        private var isMulticast:Boolean;
        private var bitrateMissing:Boolean;
        private var mediaParser:BaseParser;
        private var dvrInfoParser:BaseParser;
        private var drmAdditionalHeaderParser:BaseParser;
        private var bootstrapInfoParser:BaseParser;
        private var bootstraps:Vector.<BootstrapInfo>;
        private var bestEffortFetchInfoParser:BaseParser;
        private var manifest:Manifest;

        public function ManifestParser(){
            this.mediaParser = this.buildMediaParser();
            this.mediaParser.addEventListener(ParseEvent.PARSE_COMPLETE, this.onMediaLoadComplete, false, 0, true);
            this.mediaParser.addEventListener(ParseEvent.PARSE_ERROR, this.onAdditionalLoadError, false, 0, true);
            this.dvrInfoParser = this.buildDVRInfoParser();
            this.dvrInfoParser.addEventListener(ParseEvent.PARSE_COMPLETE, this.onDVRInfoLoadComplete, false, 0, true);
            this.dvrInfoParser.addEventListener(ParseEvent.PARSE_ERROR, this.onAdditionalLoadError, false, 0, true);
            this.drmAdditionalHeaderParser = this.buildDRMAdditionalHeaderParser();
            this.drmAdditionalHeaderParser.addEventListener(ParseEvent.PARSE_COMPLETE, this.onDRMAdditionalHeaderLoadComplete, false, 0, true);
            this.drmAdditionalHeaderParser.addEventListener(ParseEvent.PARSE_ERROR, this.onAdditionalLoadError, false, 0, true);
            this.bootstrapInfoParser = this.buildBootstrapInfoParser();
            this.bootstrapInfoParser.addEventListener(ParseEvent.PARSE_COMPLETE, this.onBootstrapInfoLoadComplete, false, 0, true);
            this.bootstrapInfoParser.addEventListener(ParseEvent.PARSE_ERROR, this.onAdditionalLoadError, false, 0, true);
            this.bestEffortFetchInfoParser = this.buildBestEffortFetchInfoParser();
            this.bestEffortFetchInfoParser.addEventListener(ParseEvent.PARSE_COMPLETE, this.onBestEffortFetchLoadComplete, false, 0, true);
            this.bestEffortFetchInfoParser.addEventListener(ParseEvent.PARSE_ERROR, this.onAdditionalLoadError, false, 0, true);
        }
        public function parse(_arg1:String, _arg2:String=null, _arg3:Manifest=null, _arg4:String=""):void{
            var _local8:XML;
            var _local9:XML;
            var _local10:XML;
            var _local11:XML;
            var _local12:XML;
            if (!_arg1){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_VALUE_MISSING)));
            };
            this.parsing = true;
            if (!_arg3){
                _arg3 = new Manifest();
            };
            this.manifest = _arg3;
            this.isMulticast = false;
            this.bitrateMissing = false;
            var _local5:XML = new XML(_arg1);
            var _local6:Namespace = _local5.namespace();
            if (_local5._local6::id.length() > 0){
                _arg3.id = _local5._local6::id.text();
            };
            if (_local5._local6::label.length() > 0){
                _arg3.label = _local5._local6::label.text();
            };
            if (_local5._local6::lang.length() > 0){
                _arg3.lang = _local5._local6::lang.text();
            };
            if (_local5._local6::duration.length() > 0){
                _arg3.duration = _local5._local6::duration.text();
            };
            if (_local5._local6::startTime.length() > 0){
                _arg3.startTime = DateUtil.parseW3CDTF(_local5._local6::startTime.text());
            };
            if (_local5._local6::mimeType.length() > 0){
                _arg3.mimeType = _local5._local6::mimeType.text();
            };
            if (_local5._local6::streamType.length() > 0){
                _arg3.streamType = _local5._local6::streamType.text();
            };
            if (_local5._local6::deliveryType.length() > 0){
                _arg3.deliveryType = _local5._local6::deliveryType.text();
            };
            if (_local5._local6::baseURL.length() > 0){
                _arg3.baseURL = _local5._local6::baseURL.text();
            };
            if (_local5._local6::urlIncludesFMSApplicationInstance.length() > 0){
                _arg3.urlIncludesFMSApplicationInstance = (_local5._local6::urlIncludesFMSApplicationInstance.text() == "true");
            };
            var _local7:String = _arg2;
            if (_arg3.baseURL != null){
                _local7 = _arg3.baseURL;
            };
            _local7 = URL.normalizePathForURL(_local7, false);
            for each (_local8 in _local5._local6::dvrInfo) {
                this.unfinishedLoads++;
                this.parseDVRInfo(_local8, _local7);
                break;
            };
            for each (_local9 in _local5._local6::media) {
                this.unfinishedLoads++;
                this.parseMedia(_local9, _local7, _arg4);
            };
            for each (_local10 in _local5._local6::drmAdditionalHeader) {
                this.unfinishedLoads++;
                this.parseDRMAdditionalHeader(_local10, _local7, _arg4);
            };
            this.bootstraps = new Vector.<BootstrapInfo>();
            for each (_local11 in _local5._local6::bootstrapInfo) {
                this.unfinishedLoads++;
                this.parseBootstrapInfo(_local11, _local7, _arg4);
            };
            for each (_local12 in _local5._local6::bestEffortFetchInfo) {
                this.unfinishedLoads++;
                this.parseBestEffortFetchInfo(_local12, _local7);
                break;
            };
            this.generateRTMPBaseURL(_arg3);
            this.parsing = false;
            this.finishLoad(_arg3);
        }
        public function createResource(_arg1:Manifest, _arg2:MediaResourceBase):MediaResourceBase{
            var _local5:StreamingURLResource;
            var _local6:Media;
            var _local7:Vector.<String>;
            var _local8:String;
            var _local9:String;
            var _local10:URL;
            var _local13:String;
            var _local14:String;
            var _local15:DynamicStreamingResource;
            var _local16:Vector.<DynamicStreamingItem>;
            var _local17:String;
            var _local18:DynamicStreamingItem;
            var _local19:int;
            var _local20:StreamingURLResource;
            var _local21:StreamingXMLResource;
            var _local3:Metadata;
            var _local4:Metadata;
            if ((_arg2 is URLResource)){
                _local10 = new URL((_arg2 as URLResource).url);
            } else {
                if ((_arg2 is StreamingXMLResource)){
                    _local10 = new URL((_arg2 as StreamingXMLResource).url);
                };
            };
            var _local11:String = ("/" + _local10.path);
            _local11 = _local11.substr(0, _local11.lastIndexOf("/"));
            var _local12:String = ((((_local10.protocol + "://") + _local10.host) + ((_local10.port)!="") ? (":" + _local10.port) : "") + _local11);
            if (_arg1.media.length == 1){
                _local6 = (_arg1.media[0] as Media);
                _local8 = _local6.url;
                _local13 = null;
                if (URL.isAbsoluteURL(_local8)){
                    _local13 = _local6.url.substr(0, _local6.url.lastIndexOf("/"));
                } else {
                    if (_arg1.baseURL != null){
                        _local13 = _arg1.baseURL;
                    } else {
                        _local13 = _local12;
                    };
                };
                _local13 = URL.normalizeRootURL(_local13);
                if (((((((!((_local6.multicastGroupspec == null))) && ((_local6.multicastGroupspec.length > 0)))) && (!((_local6.multicastStreamName == null))))) && ((_local6.multicastStreamName.length > 0)))){
                    if (URL.isAbsoluteURL(_local8)){
                        _local5 = new MulticastResource(_local8, this.streamType(_arg1));
                    } else {
                        if (_arg1.baseURL != null){
                            _local5 = new MulticastResource((URL.normalizeRootURL(_arg1.baseURL) + URL.normalizeRelativeURL(_local8)), this.streamType(_arg1));
                        } else {
                            _local5 = new MulticastResource((URL.normalizeRootURL(_local12) + URL.normalizeRelativeURL(_local8)), this.streamType(_arg1));
                        };
                    };
                    MulticastResource(_local5).groupspec = _local6.multicastGroupspec;
                    MulticastResource(_local5).streamName = _local6.multicastStreamName;
                } else {
                    if (URL.isAbsoluteURL(_local8)){
                        _local5 = new StreamingURLResource(_local8, this.streamType(_arg1));
                    } else {
                        if (_arg1.baseURL != null){
                            _local5 = new StreamingURLResource((URL.normalizeRootURL(_arg1.baseURL) + URL.normalizeRelativeURL(_local8)), this.streamType(_arg1));
                        } else {
                            _local5 = new StreamingURLResource((URL.normalizeRootURL(_local12) + URL.normalizeRelativeURL(_local8)), this.streamType(_arg1));
                        };
                    };
                };
                _local5.urlIncludesFMSApplicationInstance = _arg1.urlIncludesFMSApplicationInstance;
                if (((!((_local6.bootstrapInfo == null))) && (((!((_local6.bootstrapInfo.data == null))) || (!((_local6.bootstrapInfo.url == null))))))){
                    _local7 = new Vector.<String>();
                    _local7.push(_local13);
                    _local9 = _local6.bootstrapInfo.url;
                    if (((!((_local6.bootstrapInfo.url == null))) && ((URL.isAbsoluteURL(_local6.bootstrapInfo.url) == false)))){
                        _local9 = (URL.normalizeRootURL(_local12) + URL.normalizeRelativeURL(_local9));
                        _local6.bootstrapInfo.url = _local9;
                    };
                    _local4 = new Metadata();
                    _local4.addValue(MetadataNamespaces.HTTP_STREAMING_BOOTSTRAP_KEY, _local6.bootstrapInfo);
                    if (_local7.length > 0){
                        _local4.addValue(MetadataNamespaces.HTTP_STREAMING_SERVER_BASE_URLS_KEY, _local7);
                    };
                };
                if (_local6.metadata != null){
                    if (_local4 == null){
                        _local4 = new Metadata();
                    };
                    _local4.addValue(MetadataNamespaces.HTTP_STREAMING_STREAM_METADATA_KEY, _local6.metadata);
                };
                if (_local6.xmp != null){
                    if (_local4 == null){
                        _local4 = new Metadata();
                    };
                    _local4.addValue(MetadataNamespaces.HTTP_STREAMING_XMP_METADATA_KEY, _local6.xmp);
                };
                if (_local6.drmAdditionalHeader != null){
                    _local3 = new Metadata();
                    if (((!((Media(_arg1.media[0]).drmAdditionalHeader == null))) && (!((Media(_arg1.media[0]).drmAdditionalHeader.data == null))))){
                        _local3.addValue(MetadataNamespaces.DRM_ADDITIONAL_HEADER_KEY, Media(_arg1.media[0]).drmAdditionalHeader.data);
                        _local5.drmContentData = this.extractDRMMetadata(Media(_arg1.media[0]).drmAdditionalHeader.data);
                    };
                };
                if (_local4 != null){
                    _local5.addMetadataValue(MetadataNamespaces.HTTP_STREAMING_METADATA, _local4);
                };
                if (_local3 != null){
                    _local5.addMetadataValue(MetadataNamespaces.DRM_METADATA, _local3);
                };
            } else {
                if (_arg1.media.length > 1){
                    _local14 = ((_arg1.baseURL)!=null) ? _arg1.baseURL : _local12;
                    _local14 = URL.normalizeRootURL(_local14);
                    _local7 = new Vector.<String>();
                    _local7.push(_local14);
                    _local15 = new DynamicStreamingResource(_local14, this.streamType(_arg1));
                    _local15.urlIncludesFMSApplicationInstance = _arg1.urlIncludesFMSApplicationInstance;
                    _local16 = new Vector.<DynamicStreamingItem>();
                    if (NetStreamUtils.isRTMPStream(_local14) == false){
                        _local4 = new Metadata();
                        _local15.addMetadataValue(MetadataNamespaces.HTTP_STREAMING_METADATA, _local4);
                        _local4.addValue(MetadataNamespaces.HTTP_STREAMING_SERVER_BASE_URLS_KEY, _local7);
                    };
                    for each (_local6 in _arg1.media) {
                        if (URL.isAbsoluteURL(_local6.url)){
                            _local17 = NetStreamUtils.getStreamNameFromURL(_local6.url, _local15.urlIncludesFMSApplicationInstance);
                        } else {
                            _local17 = _local6.url;
                        };
                        _local18 = new DynamicStreamingItem(_local17, _local6.bitrate, _local6.width, _local6.height);
                        _local16.push(_local18);
                        if (_local6.drmAdditionalHeader != null){
                            if (_local15.getMetadataValue(MetadataNamespaces.DRM_METADATA) == null){
                                _local3 = new Metadata();
                                _local15.addMetadataValue(MetadataNamespaces.DRM_METADATA, _local3);
                            };
                            if (((!((_local6.drmAdditionalHeader == null))) && (!((_local6.drmAdditionalHeader.data == null))))){
                                _local3.addValue(_local18.streamName, this.extractDRMMetadata(_local6.drmAdditionalHeader.data));
                                _local3.addValue((MetadataNamespaces.DRM_ADDITIONAL_HEADER_KEY + _local18.streamName), _local6.drmAdditionalHeader.data);
                            };
                        };
                        if (((!((_local6.bootstrapInfo == null))) && (((!((_local6.bootstrapInfo.url == null))) || (!((_local6.bootstrapInfo.data == null))))))){
                            _local9 = ((_local6.bootstrapInfo.url) ? _local6.bootstrapInfo.url : null);
                            if (((!((_local6.bootstrapInfo.url == null))) && ((URL.isAbsoluteURL(_local6.bootstrapInfo.url) == false)))){
                                _local9 = (URL.normalizeRootURL(_local12) + URL.normalizeRelativeURL(_local9));
                                _local6.bootstrapInfo.url = _local9;
                            };
                            if (_local4 != null){
                                _local4.addValue((MetadataNamespaces.HTTP_STREAMING_BOOTSTRAP_KEY + _local18.streamName), _local6.bootstrapInfo);
                            };
                        };
                        if (_local6.metadata != null){
                            if (_local4 != null){
                                _local4.addValue((MetadataNamespaces.HTTP_STREAMING_STREAM_METADATA_KEY + _local18.streamName), _local6.metadata);
                            };
                        };
                        if (_local6.xmp != null){
                            if (_local4 != null){
                                _local4.addValue((MetadataNamespaces.HTTP_STREAMING_XMP_METADATA_KEY + _local18.streamName), _local6.xmp);
                            };
                        };
                    };
                    _local15.streamItems = _local16;
                    if (_arg2.getMetadataValue(MetadataNamespaces.RESOURCE_INITIAL_INDEX) != null){
                        _local19 = (_arg2.getMetadataValue(MetadataNamespaces.RESOURCE_INITIAL_INDEX) as int);
                        if (_local19 < 0){
                            _local15.initialIndex = 0;
                        } else {
                            if (_local19 >= _local15.streamItems.length){
                                _local15.initialIndex = (_local15.streamItems.length - 1);
                            } else {
                                _local15.initialIndex = _local19;
                            };
                        };
                    };
                    _local5 = _local15;
                } else {
                    if (_arg1.baseURL == null){
                        throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_MEDIA_URL_MISSING)));
                    };
                    if (_arg1.media.length == 0){
                        throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_MEDIA_MISSING)));
                    };
                };
            };
            if (_arg1.mimeType != null){
                _local5.mediaType = MediaType.VIDEO;
                _local5.mimeType = _arg1.mimeType;
            };
            if ((_arg2 is URLResource)){
                _local20 = (_arg2 as StreamingURLResource);
                if (_local20 != null){
                    _local5.clipStartTime = _local20.clipStartTime;
                    _local5.clipEndTime = _local20.clipEndTime;
                };
            } else {
                if ((_arg2 is StreamingXMLResource)){
                    _local21 = (_arg2 as StreamingXMLResource);
                    _local5.clipStartTime = _local21.clipStartTime;
                    _local5.clipEndTime = _local21.clipEndTime;
                };
            };
            _local5.addMetadataValue(MetadataNamespaces.DERIVED_RESOURCE_METADATA, _arg2);
            HTTPStreamingUtils.addDVRInfoMetadataToResource(_arg1.dvrInfo, _local5);
            HTTPStreamingUtils.addBestEffortFetchInfoMetadataToResource(_arg1.bestEffortFetchInfo, _local5);
            if (NetStreamUtils.isRTMPStream(_local14) == false){
                this.addAlternativeMedia(_arg1, _local5, _local12);
            };
            this.bootstraps = null;
            return (_local5);
        }
        protected function buildMediaParser():BaseParser{
            return (new MediaParser());
        }
        protected function buildDVRInfoParser():BaseParser{
            return (new DVRInfoParser());
        }
        protected function buildDRMAdditionalHeaderParser():BaseParser{
            return (new DRMAdditionalHeaderParser());
        }
        protected function buildBootstrapInfoParser():BaseParser{
            return (new BootstrapInfoParser());
        }
        protected function buildBestEffortFetchInfoParser():BaseParser{
            return (new BestEffortFetchInfoParser());
        }
        protected function validateManifest(_arg1:Manifest, _arg2:Boolean, _arg3:Boolean):void{
            if ((((_arg1.media.length > 1)) && (_arg2))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.MULTICAST_NOT_SUPPORT_MBR)));
            };
            if (((((_arg1.media.length + _arg1.alternativeMedia.length) > 1)) && (_arg3))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_BITRATE_MISSING)));
            };
            if (_arg2){
                _arg1.streamType = StreamType.LIVE;
            };
        }
        protected function finishLoad(_arg1:Manifest):void{
            if (this.parsing){
                return;
            };
            if (this.unfinishedLoads > 0){
                return;
            };
            if (!_arg1){
                return;
            };
            this.validateManifest(_arg1, this.isMulticast, this.bitrateMissing);
            dispatchEvent(new ParseEvent(ParseEvent.PARSE_COMPLETE, false, false, _arg1));
        }
        private function parseMedia(_arg1:XML, _arg2:String, _arg3:String=""):void{
            this.mediaParser.parse(_arg1.toXMLString(), _arg2, _arg3);
        }
        private function parseDVRInfo(_arg1:XML, _arg2:String):void{
            this.dvrInfoParser.parse(_arg1.toXMLString(), _arg2);
        }
        private function parseDRMAdditionalHeader(_arg1:XML, _arg2:String, _arg3:String=""):void{
            this.drmAdditionalHeaderParser.parse(_arg1.toXMLString(), _arg2, _arg3);
        }
        private function parseBootstrapInfo(_arg1:XML, _arg2:String, _arg3:String=""):void{
            this.bootstrapInfoParser.parse(_arg1.toXMLString(), _arg2, _arg3);
        }
        private function parseBestEffortFetchInfo(_arg1:XML, _arg2:String, _arg3:String=""):void{
            this.bestEffortFetchInfoParser.parse(_arg1.toXMLString(), _arg2, _arg3);
        }
        private function generateRTMPBaseURL(_arg1:Manifest):void{
            var _local2:Media;
            if (_arg1.baseURL == null){
                for each (_local2 in _arg1.media) {
                    if (NetStreamUtils.isRTMPStream(_local2.url)){
                        _arg1.baseURL = _local2.url;
                        break;
                    };
                };
            };
        }
        private function isSupportedType(_arg1:String):Boolean{
            return ((((_arg1 == StreamingItemType.VIDEO)) || ((_arg1 == StreamingItemType.AUDIO))));
        }
        private function extractDRMMetadata(_arg1:ByteArray):ByteArray{
            var header:* = null;
            var encryption:* = null;
            var enc:* = null;
            var params:* = null;
            var version:* = null;
            var keyInfo:* = null;
            var keyInfoData:* = null;
            var drmMetadata:* = null;
            var decoder:* = null;
            var data:* = _arg1;
            var metadata:* = null;
            data.position = 0;
            data.objectEncoding = 0;
            try {
                header = data.readObject();
                encryption = data.readObject();
                enc = encryption["Encryption"];
                params = enc["Params"];
                version = enc["Version"].toString();
                keyInfo = params["KeyInfo"];
                keyInfoData = null;
                switch (version){
                    case "2":
                        keyInfoData = keyInfo["FMRMS_METADATA"];
                        break;
                    case "3":
                        keyInfoData = keyInfo["Data"];
                        break;
                };
                if (keyInfoData != null){
                    drmMetadata = (keyInfoData["Metadata"] as String);
                    decoder = new Base64Decoder();
                    decoder.decode(drmMetadata);
                    metadata = decoder.drain();
                };
            } catch(e:Error) {
                metadata = null;
            };
            return (metadata);
        }
        private function addAlternativeMedia(_arg1:Manifest, _arg2:StreamingURLResource, _arg3:String):void{
            var _local5:Metadata;
            var _local7:Media;
            var _local8:String;
            var _local9:Object;
            var _local10:StreamingItem;
            var _local11:String;
            if (_arg1.alternativeMedia.length == 0){
                return;
            };
            var _local4:Metadata = (_arg2.getMetadataValue(MetadataNamespaces.HTTP_STREAMING_METADATA) as Metadata);
            if (_local4 == null){
                _local4 = new Metadata();
                _arg2.addMetadataValue(MetadataNamespaces.HTTP_STREAMING_METADATA, _local4);
            };
            var _local6:Vector.<StreamingItem> = new Vector.<StreamingItem>();
            for each (_local7 in _arg1.alternativeMedia) {
                if (URL.isAbsoluteURL(_local7.url)){
                    _local8 = NetStreamUtils.getStreamNameFromURL(_local7.url, _arg2.urlIncludesFMSApplicationInstance);
                } else {
                    _local8 = _local7.url;
                };
                _local9 = new Object();
                _local9.label = _local7.label;
                _local9.language = _local7.language;
                _local10 = new StreamingItem(_local7.type, _local8, _local7.bitrate, _local9);
                _local6.push(_local10);
                if (_local7.drmAdditionalHeader != null){
                    if (_arg2.getMetadataValue(MetadataNamespaces.DRM_METADATA) == null){
                        _local5 = new Metadata();
                        _arg2.addMetadataValue(MetadataNamespaces.DRM_METADATA, _local5);
                    } else {
                        _local5 = (_arg2.getMetadataValue(MetadataNamespaces.DRM_METADATA) as Metadata);
                    };
                    if (((!((_local7.drmAdditionalHeader == null))) && (!((_local7.drmAdditionalHeader.data == null))))){
                        _local5.addValue(_local10.streamName, this.extractDRMMetadata(_local7.drmAdditionalHeader.data));
                        _local5.addValue((MetadataNamespaces.DRM_ADDITIONAL_HEADER_KEY + _local10.streamName), _local7.drmAdditionalHeader.data);
                    };
                };
                if (((!((_local7.bootstrapInfo == null))) && (((!((_local7.bootstrapInfo.url == null))) || (!((_local7.bootstrapInfo.data == null))))))){
                    _local11 = ((_local7.bootstrapInfo.url) ? _local7.bootstrapInfo.url : null);
                    if (((!((_local7.bootstrapInfo.url == null))) && ((URL.isAbsoluteURL(_local7.bootstrapInfo.url) == false)))){
                        _local11 = (URL.normalizeRootURL(_arg3) + URL.normalizeRelativeURL(_local11));
                        _local7.bootstrapInfo.url = _local11;
                    };
                    _local4.addValue((MetadataNamespaces.HTTP_STREAMING_BOOTSTRAP_KEY + _local10.streamName), _local7.bootstrapInfo);
                };
                if (_local7.metadata != null){
                    _local4.addValue((MetadataNamespaces.HTTP_STREAMING_STREAM_METADATA_KEY + _local10.streamName), _local7.metadata);
                };
                if (_local7.xmp != null){
                    _local4.addValue((MetadataNamespaces.HTTP_STREAMING_XMP_METADATA_KEY + _local10.streamName), _local7.xmp);
                };
            };
            _arg2.alternativeAudioStreamItems = _local6;
        }
        private function streamType(_arg1:Manifest):String{
            return ((((((_arg1.streamType == StreamType.LIVE)) && (!((_arg1.dvrInfo == null))))) ? StreamType.DVR : _arg1.streamType));
        }
        private function onMediaLoadComplete(_arg1:ParseEvent):void{
            var _local3:BootstrapInfo;
            var _local2:Media = (_arg1.data as Media);
            if (_local2){
                if (((!((_local2.multicastGroupspec == null))) && ((_local2.multicastGroupspec.length > 0)))){
                    this.isMulticast = true;
                };
                if (this.isSupportedType(_local2.type)){
                    if (_local2.label == null){
                        _local2.label = this.manifest.label;
                    };
                    if (_local2.language == null){
                        _local2.language = this.manifest.lang;
                    };
                    if (_local2.alternate){
                        if (_local2.type == StreamingItemType.AUDIO){
                            this.manifest.alternativeMedia.push(_local2);
                        };
                    } else {
                        this.manifest.media.push(_local2);
                    };
                };
                if (((this.bootstraps) && ((this.bootstraps.length > 0)))){
                    for each (_local3 in this.bootstraps) {
                        if (_local2.bootstrapInfo == null){
                            _local2.bootstrapInfo = _local3;
                            break;
                        };
                        if (_local2.bootstrapInfo.id == _local3.id){
                            _local2.bootstrapInfo = _local3;
                            break;
                        };
                    };
                };
                this.bitrateMissing = ((this.bitrateMissing) || (isNaN(_local2.bitrate)));
            };
            this.onAdditionalLoadComplete(_arg1);
        }
        private function onDVRInfoLoadComplete(_arg1:ParseEvent):void{
            this.manifest.dvrInfo = (_arg1.data as DVRInfo);
            this.onAdditionalLoadComplete(_arg1);
        }
        private function onDRMAdditionalHeaderLoadComplete(_arg1:ParseEvent):void{
            var _local4:Media;
            var _local2:DRMAdditionalHeader = (_arg1.data as DRMAdditionalHeader);
            this.manifest.drmAdditionalHeaders.push(_local2);
            var _local3:Vector.<Media> = this.manifest.media.concat(this.manifest.alternativeMedia);
            for each (_local4 in _local3) {
                if (((!((_local4.drmAdditionalHeader == null))) && ((_local4.drmAdditionalHeader.id == _local2.id)))){
                    _local4.drmAdditionalHeader = _local2;
                };
            };
            this.onAdditionalLoadComplete(_arg1);
        }
        private function onBootstrapInfoLoadComplete(_arg1:ParseEvent):void{
            var _local4:Media;
            var _local2:BootstrapInfo = (_arg1.data as BootstrapInfo);
            this.bootstraps.push(_local2);
            var _local3:Vector.<Media> = this.manifest.media.concat(this.manifest.alternativeMedia);
            for each (_local4 in _local3) {
                if (_local4.bootstrapInfo == null){
                    _local4.bootstrapInfo = _local2;
                } else {
                    if (_local4.bootstrapInfo.id == _local2.id){
                        _local4.bootstrapInfo = _local2;
                    };
                };
            };
            this.onAdditionalLoadComplete(_arg1);
        }
        private function onBestEffortFetchLoadComplete(_arg1:ParseEvent):void{
            this.manifest.bestEffortFetchInfo = (_arg1.data as BestEffortFetchInfo);
            this.onAdditionalLoadComplete(_arg1);
        }
        private function onAdditionalLoadComplete(_arg1:Event):void{
            this.unfinishedLoads--;
            if ((((this.unfinishedLoads == 0)) && (!(this.parsing)))){
                this.finishLoad(this.manifest);
            };
        }
        private function onAdditionalLoadError(_arg1:Event):void{
            dispatchEvent(new ParseEvent(ParseEvent.PARSE_ERROR));
        }

    }
}//package org.osmf.elements.f4mClasses 
