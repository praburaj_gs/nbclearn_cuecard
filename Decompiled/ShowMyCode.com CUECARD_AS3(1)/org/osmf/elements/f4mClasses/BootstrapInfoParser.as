﻿package org.osmf.elements.f4mClasses {
    import org.osmf.events.*;
    import org.osmf.utils.*;
    import org.osmf.elements.f4mClasses.utils.*;

    public class BootstrapInfoParser extends BaseParser {

        override public function parse(_arg1:String, _arg2:String=null, _arg3:String=""):void{
            var _local7:String;
            var _local8:Base64Decoder;
            var _local4:XML = new XML(_arg1);
            if (!_local4){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_VALUE_MISSING)));
            };
            var _local5:BootstrapInfo = new BootstrapInfo();
            var _local6:String;
            if (_local4.attribute("profile").length() > 0){
                _local5.profile = _local4.@profile;
            } else {
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_PROFILE_MISSING)));
            };
            if (_local4.attribute("id").length() > 0){
                _local5.id = (_arg3 + _local4.@id);
            } else {
                _local5.id = (_arg3 + F4MUtils.GLOBAL_ELEMENT_ID);
            };
            if (_local4.attribute("url").length() > 0){
                _local6 = _local4.@url;
                if (!URL.isAbsoluteURL(_local6)){
                    _local6 = (URL.normalizeRootURL(_arg2) + URL.normalizeRelativeURL(_local6));
                };
                _local5.url = _local6;
            } else {
                _local7 = _local4.text();
                _local8 = new Base64Decoder();
                _local8.decode(_local7);
                _local5.data = _local8.drain();
            };
            this.finishLoad(_local5);
        }
        protected function finishLoad(_arg1:BootstrapInfo):void{
            if (!_arg1){
                return;
            };
            dispatchEvent(new ParseEvent(ParseEvent.PARSE_COMPLETE, false, false, _arg1));
        }

    }
}//package org.osmf.elements.f4mClasses 
