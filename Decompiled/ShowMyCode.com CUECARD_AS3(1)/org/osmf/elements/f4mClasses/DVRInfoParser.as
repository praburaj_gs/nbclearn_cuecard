﻿package org.osmf.elements.f4mClasses {
    import org.osmf.events.*;
    import org.osmf.net.httpstreaming.dvr.*;
    import org.osmf.utils.*;
    import org.osmf.elements.f4mClasses.utils.*;

    public class DVRInfoParser extends BaseParser {

        override public function parse(_arg1:String, _arg2:String=null, _arg3:String=""):void{
            var _local7:Number;
            var _local8:String;
            var _local9:String;
            var _local4:XML = new XML(_arg1);
            if (!_local4){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_VALUE_MISSING)));
            };
            var _local5:Number = (F4MUtils.getVersion(_arg1).major as Number);
            var _local6:DVRInfo = new DVRInfo();
            if (_local4.attribute("id").length() > 0){
                _local6.id = _local4.@id;
            } else {
                _local6.id = F4MUtils.GLOBAL_ELEMENT_ID;
            };
            if (_local4.attribute("url").length() > 0){
                _local8 = _local4.@url;
                if (!URL.isAbsoluteURL(_local8)){
                    _local8 = (URL.normalizeRootURL(_arg2) + URL.normalizeRelativeURL(_local8));
                };
                _local6.url = _local8;
            };
            if (_local5 <= 1){
                if (_local4.attribute("beginOffset").length() > 0){
                    _local6.beginOffset = Math.max(0, parseInt(_local4.@beginOffset));
                };
                if (_local4.attribute("endOffset").length() > 0){
                    _local7 = new Number(_local4.@endOffset);
                    if ((((_local7 > 0)) && ((_local7 < 1)))){
                        _local6.endOffset = 1;
                    } else {
                        _local6.endOffset = Math.max(0, _local7);
                    };
                };
                _local6.windowDuration = -1;
            } else {
                if (_local4.attribute("windowDuration").length() > 0){
                    _local7 = parseInt(_local4.@windowDuration);
                    if (((isNaN(_local7)) || ((_local7 < 0)))){
                        _local6.windowDuration = -1;
                    } else {
                        _local6.windowDuration = _local7;
                    };
                } else {
                    _local6.windowDuration = -1;
                };
            };
            if (_local4.attribute("offline").length() > 0){
                _local9 = _local4.@offline;
                _local6.offline = (_local9.toLowerCase() == "true");
            };
            this.finishLoad(_local6);
        }
        protected function finishLoad(_arg1:DVRInfo):void{
            if (!_arg1){
                return;
            };
            dispatchEvent(new ParseEvent(ParseEvent.PARSE_COMPLETE, false, false, _arg1));
        }

    }
}//package org.osmf.elements.f4mClasses 
