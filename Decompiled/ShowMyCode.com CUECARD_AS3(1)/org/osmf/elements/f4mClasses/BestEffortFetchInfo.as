﻿package org.osmf.elements.f4mClasses {

    public class BestEffortFetchInfo {

        public static const DEFAULT_MAX_FORWARD_FETCHES:uint = 2;
        public static const DEFAULT_MAX_BACKWARD_FETCHES:uint = 2;

        public var maxForwardFetches:uint = 2;
        public var maxBackwardFetches:uint = 2;
        public var segmentDuration:uint = 0;
        public var fragmentDuration:uint = 0;

    }
}//package org.osmf.elements.f4mClasses 
