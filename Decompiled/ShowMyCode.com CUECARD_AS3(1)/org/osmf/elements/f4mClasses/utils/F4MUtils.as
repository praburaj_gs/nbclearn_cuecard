﻿package org.osmf.elements.f4mClasses.utils {
    import org.osmf.media.pluginClasses.*;

    public class F4MUtils {

        public static const GLOBAL_ELEMENT_ID:String = "global";

        public static function getVersion(_arg1:String):Object{
            var _local4:String;
            var _local2:XML = new XML(_arg1);
            var _local3:String = _local2.namespace().toString();
            var _local5:int = _local3.lastIndexOf("/");
            if (_local5 != -1){
                _local4 = _local3.substr((_local5 + 1));
            } else {
                _local4 = _local3;
            };
            return (VersionUtils.parseVersionString(_local4));
        }

    }
}//package org.osmf.elements.f4mClasses.utils 
