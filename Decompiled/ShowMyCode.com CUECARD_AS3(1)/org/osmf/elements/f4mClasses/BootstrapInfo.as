﻿package org.osmf.elements.f4mClasses {
    import flash.utils.*;

    public class BootstrapInfo {

        public var data:ByteArray;
        public var url:String;
        public var profile:String;
        public var id:String;

    }
}//package org.osmf.elements.f4mClasses 
