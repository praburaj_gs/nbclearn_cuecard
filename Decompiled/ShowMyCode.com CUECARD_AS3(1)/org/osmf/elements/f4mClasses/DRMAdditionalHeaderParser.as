﻿package org.osmf.elements.f4mClasses {
    import flash.events.*;
    import org.osmf.net.httpstreaming.*;
    import flash.net.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.utils.*;
    import org.osmf.elements.f4mClasses.utils.*;

    public class DRMAdditionalHeaderParser extends BaseParser {

        private var loadingInfo:Dictionary;

        override public function parse(_arg1:String, _arg2:String=null, _arg3:String=""):void{
            var _local7:String;
            var _local8:Base64Decoder;
            var _local9:URLLoader;
            var _local4:XML = new XML(_arg1);
            if (!_local4){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_VALUE_MISSING)));
            };
            var _local5:DRMAdditionalHeader = new DRMAdditionalHeader();
            var _local6:String;
            if (_local4.attribute("id").length() > 0){
                _local5.id = (_arg3 + _local4.@id);
            } else {
                _local5.id = (_arg3 + F4MUtils.GLOBAL_ELEMENT_ID);
            };
            if (_local4.attribute("url").length() > 0){
                _local6 = _local4.@url;
                if (!URL.isAbsoluteURL(_local6)){
                    _local6 = (URL.normalizeRootURL(_arg2) + URL.normalizeRelativeURL(_local6));
                };
                _local5.url = _local6;
            } else {
                _local7 = _local4.text();
                _local8 = new Base64Decoder();
                _local8.decode(_local7);
                _local5.data = _local8.drain();
            };
            if (_local6 != null){
                if (!this.loadingInfo){
                    this.loadingInfo = new Dictionary(true);
                };
                _local9 = new URLLoader();
                _local9.dataFormat = URLLoaderDataFormat.BINARY;
                _local9.addEventListener(Event.COMPLETE, this.onLoadComplete);
                _local9.addEventListener(IOErrorEvent.IO_ERROR, this.onLoadError);
                _local9.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onLoadError);
                this.loadingInfo[_local9] = _local5;
                _local9.load(new URLRequest(HTTPStreamingUtils.normalizeURL(_local6)));
            } else {
                this.finishLoad(_local5);
            };
        }
        protected function finishLoad(_arg1:DRMAdditionalHeader):void{
            if (!_arg1){
                return;
            };
            dispatchEvent(new ParseEvent(ParseEvent.PARSE_COMPLETE, false, false, _arg1));
        }
        private function onLoadComplete(_arg1:Event):void{
            var _local2:URLLoader = (_arg1.target as URLLoader);
            _local2.removeEventListener(Event.COMPLETE, this.onLoadComplete);
            _local2.removeEventListener(IOErrorEvent.IO_ERROR, this.onLoadError);
            _local2.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onLoadError);
            var _local3:DRMAdditionalHeader = this.loadingInfo[_local2];
            _local3.data = _local2.data;
            delete this.loadingInfo[_local2];
            this.finishLoad(_local3);
        }
        private function onLoadError(_arg1:Event):void{
            dispatchEvent(new ParseEvent(ParseEvent.PARSE_ERROR));
        }

    }
}//package org.osmf.elements.f4mClasses 
