﻿package org.osmf.elements.f4mClasses {
    import org.osmf.net.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.utils.*;
    import org.osmf.elements.f4mClasses.utils.*;

    public class MediaParser extends BaseParser {

        override public function parse(_arg1:String, _arg2:String=null, _arg3:String=""):void{
            var _local7:Base64Decoder;
            var _local8:String;
            var _local9:ByteArray;
            var _local10:String;
            var _local11:Object;
            var _local4:XML = new XML(_arg1);
            if (!_local4){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_VALUE_MISSING)));
            };
            var _local5:Media = new Media();
            var _local6:Namespace = _local4.namespace();
            if (_local4.attribute("url").length() > 0){
                _local8 = _local4.@url;
                if (!URL.isAbsoluteURL(_local8)){
                    _local8 = (URL.normalizeRootURL(_arg2) + URL.normalizeRelativeURL(_local8));
                };
                _local5.url = _local8;
            } else {
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_MEDIA_URL_MISSING)));
            };
            if (_local4.attribute("bitrate").length() > 0){
                _local5.bitrate = _local4.@bitrate;
            };
            if (_local4.attribute("drmAdditionalHeaderId").length() > 0){
                _local5.drmAdditionalHeader = new DRMAdditionalHeader();
                _local5.drmAdditionalHeader.id = (_arg3 + _local4.@drmAdditionalHeaderId);
            } else {
                _local5.drmAdditionalHeader = new DRMAdditionalHeader();
                _local5.drmAdditionalHeader.id = (_arg3 + F4MUtils.GLOBAL_ELEMENT_ID);
            };
            if (_local4.attribute("bootstrapInfoId").length() > 0){
                _local5.bootstrapInfo = new BootstrapInfo();
                _local5.bootstrapInfo.id = (_arg3 + _local4.@bootstrapInfoId);
            } else {
                _local5.bootstrapInfo = new BootstrapInfo();
                _local5.bootstrapInfo.id = (_arg3 + F4MUtils.GLOBAL_ELEMENT_ID);
            };
            if (_local4.attribute("height").length() > 0){
                _local5.height = _local4.@height;
            };
            if (_local4.attribute("width").length() > 0){
                _local5.width = _local4.@width;
            };
            if (_local4.attribute("groupspec").length() > 0){
                _local5.multicastGroupspec = _local4.@groupspec;
            };
            if (_local4.attribute("multicastStreamName").length() > 0){
                _local5.multicastStreamName = _local4.@multicastStreamName;
            };
            if (_local4.attribute("label").length() > 0){
                _local5.label = _local4.@label;
            };
            if (_local4.attribute("type").length() > 0){
                _local5.type = _local4.@type;
            } else {
                _local5.type = StreamingItemType.VIDEO;
            };
            if (_local4.attribute("lang").length() > 0){
                _local5.language = _local4.@lang;
            };
            if (((_local4.hasOwnProperty("@alternate")) || ((_local4.attribute("alternate").length() > 0)))){
                _local5.alternate = true;
            };
            if (_local4._local6::moov.length() > 0){
                _local7 = new Base64Decoder();
                _local7.decode(_local4._local6::moov.text());
                _local5.moov = _local7.drain();
            };
            if (_local4._local6::metadata.length() > 0){
                _local7 = new Base64Decoder();
                _local7.decode(_local4._local6::metadata.text());
                _local9 = _local7.drain();
                _local9.position = 0;
                _local9.objectEncoding = 0;
                try {
                    _local10 = (_local9.readObject() as String);
                    _local11 = _local9.readObject();
                    _local5.metadata = _local11;
                    if (((((isNaN(_local5.width)) || ((_local5.width == 0)))) && (_local5.metadata.hasOwnProperty("width")))){
                        _local5.width = _local5.metadata["width"];
                    };
                    if (((((isNaN(_local5.height)) || ((_local5.height == 0)))) && (_local5.metadata.hasOwnProperty("height")))){
                        _local5.height = _local5.metadata["height"];
                    };
                } catch(e:Error) {
                };
            };
            if (_local4._local6::xmpMetadata.length() > 0){
                _local7 = new Base64Decoder();
                _local7.decode(_local4._local6::xmpMetadata.text());
                _local5.xmp = _local7.drain();
            };
            this.validateMedia(_local5);
            this.finishLoad(_local5);
        }
        protected function validateMedia(_arg1:Media):void{
            if (((((_arg1) && (((((!((_arg1.multicastGroupspec == null))) && ((_arg1.multicastGroupspec.length > 0)))) && ((((_arg1.multicastStreamName == null)) || ((_arg1.multicastStreamName.length <= 0)))))))) || (((((!((_arg1.multicastStreamName == null))) && ((_arg1.multicastStreamName.length > 0)))) && ((((_arg1.multicastGroupspec == null)) || ((_arg1.multicastGroupspec.length <= 0)))))))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.MULTICAST_PARAMETER_INVALID)));
            };
        }
        protected function finishLoad(_arg1:Media):void{
            if (!_arg1){
                return;
            };
            dispatchEvent(new ParseEvent(ParseEvent.PARSE_COMPLETE, false, false, _arg1));
        }

    }
}//package org.osmf.elements.f4mClasses 
