﻿package org.osmf.elements.f4mClasses {
    import org.osmf.events.*;
    import org.osmf.utils.*;

    public class ExternalMediaParser extends MediaParser {

        override public function parse(_arg1:String, _arg2:String=null, _arg3:String=""):void{
            var _local4:XML = new XML(_arg1);
            if (!_local4){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_VALUE_MISSING)));
            };
            if (_local4.attribute("href").length() > 0){
                dispatchEvent(new ParseEvent(ParseEvent.PARSE_COMPLETE, false, false, null));
            } else {
                super.parse(_arg1, _arg2, _arg3);
            };
        }

    }
}//package org.osmf.elements.f4mClasses 
