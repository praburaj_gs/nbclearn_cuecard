﻿package org.osmf.elements.f4mClasses.builders {
    import org.osmf.elements.f4mClasses.*;

    public class BaseManifestBuilder {

        public function canParse(_arg1:String):Boolean{
            return (false);
        }
        public function build(_arg1:String):ManifestParser{
            return (null);
        }

    }
}//package org.osmf.elements.f4mClasses.builders 
