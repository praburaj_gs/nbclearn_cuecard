﻿package org.osmf.elements.f4mClasses.builders {
    import org.osmf.elements.f4mClasses.*;
    import org.osmf.media.pluginClasses.*;
    import org.osmf.elements.f4mClasses.utils.*;

    public class ManifestBuilder extends BaseManifestBuilder {

        private static const MINIMUM_VERSION:Object = VersionUtils.parseVersionString("0.0");
        private static const MAXIMUM_VERSION:Object = VersionUtils.parseVersionString("1.9");

        override public function canParse(_arg1:String):Boolean{
            var _local2:Object = this.getVersion(_arg1);
            return ((((((((_local2.major >= MINIMUM_VERSION.major)) && ((_local2.major <= MAXIMUM_VERSION.major)))) && ((_local2.minor >= MINIMUM_VERSION.minor)))) && ((_local2.minor <= MAXIMUM_VERSION.minor))));
        }
        override public function build(_arg1:String):ManifestParser{
            var _local2:ManifestParser = this.createParser();
            return (_local2);
        }
        protected function getVersion(_arg1:String):Object{
            return (F4MUtils.getVersion(_arg1));
        }
        protected function createParser():ManifestParser{
            return (new ManifestParser());
        }

    }
}//package org.osmf.elements.f4mClasses.builders 
