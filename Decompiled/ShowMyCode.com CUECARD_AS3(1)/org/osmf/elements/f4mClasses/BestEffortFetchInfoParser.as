﻿package org.osmf.elements.f4mClasses {
    import org.osmf.events.*;
    import org.osmf.utils.*;
    import org.osmf.elements.f4mClasses.utils.*;

    public class BestEffortFetchInfoParser extends BaseParser {

        override public function parse(_arg1:String, _arg2:String=null, _arg3:String=""):void{
            var _local7:Number;
            var _local4:XML = new XML(_arg1);
            if (!_local4){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_VALUE_MISSING)));
            };
            var _local5:Number = (F4MUtils.getVersion(_arg1).major as Number);
            var _local6:BestEffortFetchInfo = new BestEffortFetchInfo();
            if (_local4.attribute("segmentDuration").length() > 0){
                _local7 = Math.round((parseFloat(_local4.@segmentDuration) * 1000));
                if (((isNaN(_local7)) || ((_local7 <= 0)))){
                    throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_ERROR)));
                };
                _local6.segmentDuration = uint(_local7);
            } else {
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_VALUE_MISSING)));
            };
            if (_local4.attribute("fragmentDuration").length() > 0){
                _local7 = Math.round((parseFloat(_local4.@fragmentDuration) * 1000));
                if (((isNaN(_local7)) || ((_local7 <= 0)))){
                    throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_ERROR)));
                };
                _local6.fragmentDuration = uint(_local7);
            } else {
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.F4M_PARSE_VALUE_MISSING)));
            };
            if (_local4.attribute("maxForwardFetches").length() > 0){
                _local7 = parseInt(_local4.@maxForwardFetches);
                if (((!(isNaN(_local7))) && ((_local7 > 0)))){
                    _local6.maxForwardFetches = uint(_local7);
                };
            };
            if (_local4.attribute("maxBackwardFetches").length() > 0){
                _local7 = parseInt(_local4.@maxBackwardFetches);
                if (((!(isNaN(_local7))) && ((_local7 >= 0)))){
                    _local6.maxBackwardFetches = uint(_local7);
                };
            };
            this.finishLoad(_local6);
        }
        protected function finishLoad(_arg1:BestEffortFetchInfo):void{
            dispatchEvent(new ParseEvent(ParseEvent.PARSE_COMPLETE, false, false, _arg1));
        }

    }
}//package org.osmf.elements.f4mClasses 
