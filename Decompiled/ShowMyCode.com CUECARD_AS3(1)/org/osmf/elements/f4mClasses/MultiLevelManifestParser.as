﻿package org.osmf.elements.f4mClasses {
    import flash.events.*;
    import org.osmf.net.httpstreaming.*;
    import flash.net.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.utils.*;

    public class MultiLevelManifestParser extends ManifestParser {

        private var parsing:Boolean = false;
        private var unfinishedLoads:Number = 0;
        private var manifest:Manifest;
        private var queue:Array;
        private var baseURLs:Dictionary;
        private var loadingInfo:Dictionary;
        private var externalMediaCount:Number = 0;

        override public function parse(_arg1:String, _arg2:String=null, _arg3:Manifest=null, _arg4:String=""):void{
            var _local8:XML;
            var _local9:String;
            var _local10:URLLoader;
            var _local11:Info;
            this.parsing = true;
            this.manifest = new Manifest();
            var _local5:XML = new XML(_arg1);
            var _local6:Namespace = _local5.namespace();
            this.queue = [];
            this.queue.push(_local5);
            if (!this.baseURLs){
                this.baseURLs = new Dictionary(true);
            };
            var _local7:String = _arg2;
            if (_local5._local6::baseURL.length() > 0){
                _local7 = _local5._local6::baseURL.text();
            };
            _local7 = URL.normalizePathForURL(_local7, false);
            this.baseURLs[_local5] = _local7;
            for each (_local8 in _local5._local6::media) {
                if (_local8.attribute("href").length() > 0){
                    this.unfinishedLoads++;
                    _local9 = _local8.@href;
                    if (!URL.isAbsoluteURL(_local9)){
                        _local9 = (URL.normalizeRootURL(_local7) + URL.normalizeRelativeURL(_local9));
                    };
                    _local10 = new URLLoader();
                    _local10.addEventListener(Event.COMPLETE, this.onLoadComplete);
                    _local10.addEventListener(IOErrorEvent.IO_ERROR, this.onLoadError);
                    _local10.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onLoadError);
                    if (!this.loadingInfo){
                        this.loadingInfo = new Dictionary(true);
                    };
                    _local11 = new Info();
                    _local11.baseURL = URL.normalizeRootURL(URL.getRootUrl(_local9));
                    if (_local8.attribute("bitrate").length() > 0){
                        _local11.attributes.bitrate = _local8.@bitrate;
                    };
                    if (_local8.attribute("width").length() > 0){
                        _local11.attributes.width = _local8.@width;
                    };
                    if (_local8.attribute("height").length() > 0){
                        _local11.attributes.height = _local8.@height;
                    };
                    if (_local8.attribute("type").length() > 0){
                        _local11.attributes.type = _local8.@type;
                    };
                    if (((_local8.hasOwnProperty("@alternate")) || ((_local8.attribute("alternate").length() > 0)))){
                        _local11.attributes.alternate = "true";
                    };
                    if (_local8.attribute("label").length() > 0){
                        _local11.attributes.label = _local8.@label;
                    };
                    if (_local8.attribute("lang").length() > 0){
                        _local11.attributes.lang = _local8.@lang;
                    };
                    this.loadingInfo[_local10] = _local11;
                    _local10.load(new URLRequest(HTTPStreamingUtils.normalizeURL(_local9)));
                };
            };
            this.parsing = false;
            if (this.unfinishedLoads == 0){
                this.processQueue();
            };
        }
        override protected function finishLoad(_arg1:Manifest):void{
            if (!this.processQueue()){
                _arg1.baseURL = null;
                super.finishLoad(_arg1);
            };
        }
        override protected function buildMediaParser():BaseParser{
            return (new ExternalMediaParser());
        }
        private function processQueue():Boolean{
            var _local1:XML;
            var _local2:String;
            var _local3:String;
            if (this.parsing){
                return (true);
            };
            if (this.queue.length > 0){
                _local1 = (this.queue.pop() as XML);
                _local2 = this.baseURLs[_local1];
                this.externalMediaCount = (this.externalMediaCount + 1);
                _local3 = (("external" + this.externalMediaCount) + "_");
                super.parse(_local1.toXMLString(), _local2, this.manifest, _local3);
                return (true);
            };
            return (false);
        }
        private function onLoadComplete(_arg1:Event):void{
            var _local6:XML;
            var _local7:XMLList;
            var _local8:String;
            var _local9:int;
            var _local2:URLLoader = (_arg1.target as URLLoader);
            _local2.removeEventListener(Event.COMPLETE, this.onLoadComplete);
            _local2.removeEventListener(IOErrorEvent.IO_ERROR, this.onLoadError);
            _local2.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onLoadError);
            var _local3:XML = XML(URLLoader(_arg1.target).data);
            var _local4:Namespace = _local3.namespace();
            var _local5:Info = this.loadingInfo[_local2];
            delete this.loadingInfo[_local2];
            for each (_local6 in _local3._local4::media) {
                _local7 = describeType(_local5.attributes)..variable;
                while (_local9 < _local7.length()) {
                    _local8 = _local7[_local9].@name;
                    if (((!((_local5.attributes[_local8] == null))) && ((_local5.attributes[_local8].length > 0)))){
                        _local6.@[_local8] = _local5.attributes[_local8];
                    } else {
                        delete _local6.@[_local8];
                    };
                    _local9++;
                };
            };
            if (!this.baseURLs){
                this.baseURLs = new Dictionary(true);
            };
            this.baseURLs[_local3] = URL.normalizeRootURL(_local5.baseURL);
            this.queue.push(_local3);
            this.unfinishedLoads--;
            if (this.unfinishedLoads == 0){
                this.processQueue();
            };
        }
        private function onLoadError(_arg1:Event):void{
            this.unfinishedLoads--;
            dispatchEvent(new ParseEvent(ParseEvent.PARSE_ERROR));
        }

    }
}//package org.osmf.elements.f4mClasses 

class Info {

    public var baseURL:String;
    public var attributes:Attributes;

    public function Info(){
        this.attributes = new Attributes();
    }
}
class Attributes {

    public var bitrate:String;
    public var width:String;
    public var height:String;
    public var type:String;
    public var alternate:String;
    public var label:String;
    public var lang:String;

    public function Attributes(){
    }
}
