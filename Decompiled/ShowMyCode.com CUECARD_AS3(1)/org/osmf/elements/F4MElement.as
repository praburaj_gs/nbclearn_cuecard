﻿package org.osmf.elements {
    import org.osmf.media.*;

    public class F4MElement extends LoadFromDocumentElement {

        public function F4MElement(_arg1:MediaResourceBase=null, _arg2:F4MLoader=null){
            if (_arg2 == null){
                _arg2 = new F4MLoader();
            };
            super(_arg1, _arg2);
        }
    }
}//package org.osmf.elements 
