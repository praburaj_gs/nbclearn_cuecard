﻿package org.osmf.elements.compositeClasses {

    public final class CompositionMode {

        public static const PARALLEL:String = "parallel";
        public static const SERIAL:String = "serial";

    }
}//package org.osmf.elements.compositeClasses 
