﻿package org.osmf.elements.proxyClasses {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.metadata.*;
    import org.osmf.events.*;

    public class ProxyMetadata extends Metadata {

        private var proxiedMetadata:Metadata;

        public function ProxyMetadata(){
            this.proxiedMetadata = new Metadata();
            this.proxiedMetadata.addEventListener(MetadataEvent.VALUE_ADD, this.redispatchEvent);
            this.proxiedMetadata.addEventListener(MetadataEvent.VALUE_CHANGE, this.redispatchEvent);
            this.proxiedMetadata.addEventListener(MetadataEvent.VALUE_REMOVE, this.redispatchEvent);
        }
        public function set metadata(_arg1:Metadata):void{
            var _local2:String;
            this.proxiedMetadata.removeEventListener(MetadataEvent.VALUE_ADD, this.redispatchEvent);
            this.proxiedMetadata.removeEventListener(MetadataEvent.VALUE_CHANGE, this.redispatchEvent);
            this.proxiedMetadata.removeEventListener(MetadataEvent.VALUE_REMOVE, this.redispatchEvent);
            for each (_local2 in this.proxiedMetadata.keys) {
                _arg1.addValue(_local2, this.proxiedMetadata.getValue(_local2));
            };
            this.proxiedMetadata = _arg1;
            this.proxiedMetadata.addEventListener(MetadataEvent.VALUE_ADD, this.redispatchEvent);
            this.proxiedMetadata.addEventListener(MetadataEvent.VALUE_CHANGE, this.redispatchEvent);
            this.proxiedMetadata.addEventListener(MetadataEvent.VALUE_REMOVE, this.redispatchEvent);
        }
        override public function getValue(_arg1:String){
            return (this.proxiedMetadata.getValue(_arg1));
        }
        override public function addValue(_arg1:String, _arg2:Object):void{
            this.proxiedMetadata.addValue(_arg1, _arg2);
        }
        override public function removeValue(_arg1:String){
            return (this.proxiedMetadata.removeValue(_arg1));
        }
        override public function get keys():Vector.<String>{
            return (this.proxiedMetadata.keys);
        }
        private function redispatchEvent(_arg1:Event):void{
            dispatchEvent(_arg1.clone());
        }

    }
}//package org.osmf.elements.proxyClasses 
