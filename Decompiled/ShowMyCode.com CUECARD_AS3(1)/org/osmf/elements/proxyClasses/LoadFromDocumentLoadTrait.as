﻿package org.osmf.elements.proxyClasses {
    import org.osmf.media.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;

    public class LoadFromDocumentLoadTrait extends LoadTrait {

        private var _mediaElement:MediaElement;

        public function LoadFromDocumentLoadTrait(_arg1:LoaderBase, _arg2:MediaResourceBase){
            super(_arg1, _arg2);
        }
        override protected function loadStateChangeEnd():void{
            dispatchEvent(new LoadEvent(LoadEvent.LOAD_STATE_CHANGE, false, false, loadState));
        }
        public function set mediaElement(_arg1:MediaElement):void{
            this._mediaElement = _arg1;
        }
        public function get mediaElement():MediaElement{
            return (this._mediaElement);
        }

    }
}//package org.osmf.elements.proxyClasses 
