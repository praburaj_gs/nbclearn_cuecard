﻿package org.osmf.elements {
    import org.osmf.media.*;
    import org.osmf.traits.*;
    import org.osmf.elements.loaderClasses.*;

    public class SWFElement extends LoadableElementBase {

        public function SWFElement(_arg1:URLResource=null, _arg2:SWFLoader=null){
            if (_arg2 == null){
                _arg2 = new SWFLoader();
            };
            super(_arg1, _arg2);
        }
        override protected function createLoadTrait(_arg1:MediaResourceBase, _arg2:LoaderBase):LoadTrait{
            return (new LoaderLoadTrait(_arg2, _arg1));
        }
        override protected function processReadyState():void{
            var _local1:LoaderLoadTrait = (getTrait(MediaTraitType.LOAD) as LoaderLoadTrait);
            addTrait(MediaTraitType.DISPLAY_OBJECT, LoaderUtils.createDisplayObjectTrait(_local1.loader, this));
        }
        override protected function processUnloadingState():void{
            removeTrait(MediaTraitType.DISPLAY_OBJECT);
        }

    }
}//package org.osmf.elements 
