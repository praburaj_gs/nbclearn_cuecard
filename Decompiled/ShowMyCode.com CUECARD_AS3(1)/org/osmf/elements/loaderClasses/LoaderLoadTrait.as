﻿package org.osmf.elements.loaderClasses {
    import flash.events.*;
    import flash.display.*;
    import org.osmf.media.*;
    import org.osmf.traits.*;

    public class LoaderLoadTrait extends LoadTrait {

        private var _loader:Loader;

        public function LoaderLoadTrait(_arg1:LoaderBase, _arg2:MediaResourceBase){
            super(_arg1, _arg2);
        }
        public function get loader():Loader{
            return (this._loader);
        }
        public function set loader(_arg1:Loader):void{
            this._loader = _arg1;
        }
        override protected function loadStateChangeStart(_arg1:String):void{
            if (_arg1 == LoadState.LOADING){
                this.loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, this.onContentLoadProgress, false, 0, true);
            } else {
                if (_arg1 == LoadState.READY){
                    setBytesTotal(this.loader.contentLoaderInfo.bytesTotal);
                    setBytesLoaded(this.loader.contentLoaderInfo.bytesLoaded);
                    this.loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, this.onContentLoadProgress, false, 0, true);
                } else {
                    if (_arg1 == LoadState.UNINITIALIZED){
                        setBytesLoaded(0);
                    };
                };
            };
        }
        private function onContentLoadProgress(_arg1:ProgressEvent):void{
            setBytesTotal(_arg1.bytesTotal);
            setBytesLoaded(_arg1.bytesLoaded);
        }

    }
}//package org.osmf.elements.loaderClasses 
