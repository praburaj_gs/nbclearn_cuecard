﻿package org.osmf.elements.loaderClasses {
    import flash.events.*;
    import flash.display.*;
    import org.osmf.media.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import flash.geom.*;
    import flash.system.*;
    import flash.errors.*;

    public class LoaderUtils {

        private static const SWF_MIME_TYPE:String = "application/x-shockwave-flash";

        public static function createDisplayObjectTrait(_arg1:Loader, _arg2:MediaElement):DisplayObjectTrait{
            var _local3:DisplayObject;
            var _local4:Number = 0;
            var _local5:Number = 0;
            var _local6:LoaderInfo = _arg1.contentLoaderInfo;
            _local3 = _arg1;
            _local3.scrollRect = new Rectangle(0, 0, _local6.width, _local6.height);
            _local4 = _local6.width;
            _local5 = _local6.height;
            return (new DisplayObjectTrait(_local3, _local4, _local5));
        }
        public static function loadLoadTrait(_arg1:LoadTrait, _arg2:Function, _arg3:Boolean, _arg4:Boolean, _arg5:Function=null):void{
            var loader:* = null;
            var onLoadComplete:* = null;
            var onIOError:* = null;
            var onSecurityError:* = null;
            var loadTrait:* = _arg1;
            var updateLoadTraitFunction:* = _arg2;
            var useCurrentSecurityDomain:* = _arg3;
            var checkPolicyFile:* = _arg4;
            var validateLoadedContentFunction = _arg5;
            var toggleLoaderListeners:* = function (_arg1:Loader, _arg2:Boolean):void{
                if (_arg2){
                    _arg1.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
                    _arg1.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
                    _arg1.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
                } else {
                    _arg1.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoadComplete);
                    _arg1.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
                    _arg1.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
                };
            };
            onLoadComplete = function (_arg1:Event):void{
                var validated:* = false;
                var timer:* = null;
                var onTimer:* = null;
                var event:* = _arg1;
                toggleLoaderListeners(loader, false);
                if (loadTrait.loadState == LoadState.LOADING){
                    if (validateLoadedContentFunction != null){
                        validated = validateLoadedContentFunction(loader.content);
                        if (validated){
                            if (Capabilities.isDebugger){
                                onTimer = function (_arg1:TimerEvent):void{
                                    timer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimer);
                                    timer = null;
                                    loader.unloadAndStop();
                                    loader = null;
                                    loadLoadTrait(loadTrait, updateLoadTraitFunction, useCurrentSecurityDomain, false, null);
                                };
                                timer = new Timer(250, 1);
                                timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimer);
                                timer.start();
                            } else {
                                loader.unloadAndStop();
                                loader = null;
                                loadLoadTrait(loadTrait, updateLoadTraitFunction, useCurrentSecurityDomain, false, null);
                            };
                        } else {
                            loader.unloadAndStop();
                            loader = null;
                            updateLoadTraitFunction(loadTrait, LoadState.LOAD_ERROR);
                            loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.IO_ERROR)));
                        };
                    } else {
                        updateLoadTraitFunction(loadTrait, LoadState.READY);
                    };
                };
            };
            onIOError = function (_arg1:IOErrorEvent, _arg2:String=null):void{
                toggleLoaderListeners(loader, false);
                loader = null;
                updateLoadTraitFunction(loadTrait, LoadState.LOAD_ERROR);
                loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.IO_ERROR, ((_arg1) ? _arg1.text : _arg2))));
            };
            onSecurityError = function (_arg1:SecurityErrorEvent, _arg2:String=null):void{
                toggleLoaderListeners(loader, false);
                loader = null;
                updateLoadTraitFunction(loadTrait, LoadState.LOAD_ERROR);
                loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.SECURITY_ERROR, ((_arg1) ? _arg1.text : _arg2))));
            };
            var loaderLoadTrait:* = (loadTrait as LoaderLoadTrait);
            loader = new Loader();
            loaderLoadTrait.loader = loader;
            updateLoadTraitFunction(loadTrait, LoadState.LOADING);
            var context:* = new LoaderContext();
            var urlReq:* = new URLRequest((loadTrait.resource as URLResource).url.toString());
            context.checkPolicyFile = checkPolicyFile;
            if (((useCurrentSecurityDomain) && ((urlReq.url.search(/^file:\//i) == -1)))){
                context.securityDomain = SecurityDomain.currentDomain;
            };
            if (validateLoadedContentFunction != null){
                context.applicationDomain = new ApplicationDomain();
            };
            toggleLoaderListeners(loader, true);
            try {
                loader.load(urlReq, context);
            } catch(ioError:IOError) {
                onIOError(null, ioError.message);
            } catch(securityError:SecurityError) {
                onSecurityError(null, securityError.message);
            };
        }
        public static function unloadLoadTrait(_arg1:LoadTrait, _arg2:Function):void{
            var _local3:LoaderLoadTrait = (_arg1 as LoaderLoadTrait);
            _arg2(_arg1, LoadState.UNLOADING);
            _local3.loader.unloadAndStop();
            _arg2(_arg1, LoadState.UNINITIALIZED);
        }

    }
}//package org.osmf.elements.loaderClasses 
