﻿package org.osmf.elements {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.elements.f4mClasses.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.elements.f4mClasses.builders.*;
    import org.osmf.utils.*;
    import org.osmf.elements.proxyClasses.*;

    public class ManifestLoaderBase extends LoaderBase {

        protected var factory:MediaFactory;
        protected var builders:Vector.<BaseManifestBuilder>;
        protected var loadTrait:LoadTrait;
        protected var parserTimer:Timer;
        protected var parser:ManifestParser;

        protected function onParserTimerComplete(_arg1:TimerEvent):void{
            if (this.parserTimer){
                this.parserTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, this.onParserTimerComplete);
                this.parserTimer = null;
            };
            updateLoadTrait(this.loadTrait, LoadState.LOAD_ERROR);
            this.loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.F4M_FILE_INVALID, OSMFStrings.getString(OSMFStrings.F4M_PARSE_ERROR))));
        }
        protected function onParserLoadComplete(_arg1:ParseEvent):void{
            if (this.parserTimer){
                this.parserTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, this.onParserTimerComplete);
                this.parserTimer.stop();
                this.parserTimer = null;
            };
            this.parser.removeEventListener(ParseEvent.PARSE_COMPLETE, this.onParserLoadComplete);
            this.parser.removeEventListener(ParseEvent.PARSE_ERROR, this.onParserLoadError);
            var _local2:Manifest = (_arg1.data as Manifest);
            this.finishManifestLoad(_local2);
        }
        protected function onParserLoadError(_arg1:ParseEvent):void{
            if (this.parserTimer){
                this.parserTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, this.onParserTimerComplete);
                this.parserTimer.stop();
                this.parserTimer = null;
            };
            this.parser.removeEventListener(ParseEvent.PARSE_COMPLETE, this.onParserLoadComplete);
            this.parser.removeEventListener(ParseEvent.PARSE_ERROR, this.onParserLoadError);
            updateLoadTrait(this.loadTrait, LoadState.LOAD_ERROR);
            this.loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.F4M_FILE_INVALID, OSMFStrings.getString(OSMFStrings.F4M_PARSE_ERROR))));
        }
        protected function finishManifestLoad(_arg1:Manifest):void{
            var netResource:* = null;
            var loadedElem:* = null;
            var manifest:* = _arg1;
            try {
                netResource = this.parser.createResource(manifest, this.loadTrait.resource);
                loadedElem = this.factory.createMediaElement(netResource);
                if (((loadedElem.hasOwnProperty("defaultDuration")) && (!(isNaN(manifest.duration))))){
                    loadedElem["defaultDuration"] = manifest.duration;
                };
                LoadFromDocumentLoadTrait(this.loadTrait).mediaElement = loadedElem;
                updateLoadTrait(this.loadTrait, LoadState.READY);
            } catch(error:Error) {
                updateLoadTrait(loadTrait, LoadState.LOAD_ERROR);
                loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.F4M_FILE_INVALID, error.message)));
            };
        }
        override protected function executeUnload(_arg1:LoadTrait):void{
            updateLoadTrait(_arg1, LoadState.UNINITIALIZED);
        }
        protected function getBuilders():Vector.<BaseManifestBuilder>{
            var _local1:Vector.<BaseManifestBuilder> = new Vector.<BaseManifestBuilder>();
            _local1.push(new MultiLevelManifestBuilder());
            _local1.push(new ManifestBuilder());
            return (_local1);
        }
        protected function getParser(_arg1:String):ManifestParser{
            var _local2:ManifestParser;
            var _local3:BaseManifestBuilder;
            for each (_local3 in this.builders) {
                if (_local3.canParse(_arg1)){
                    _local2 = (_local3.build(_arg1) as ManifestParser);
                    break;
                };
            };
            return (_local2);
        }

    }
}//package org.osmf.elements 
