﻿package org.osmf.elements {
    import flash.display.*;
    import org.osmf.media.*;
    import org.osmf.traits.*;
    import org.osmf.elements.loaderClasses.*;

    public class ImageElement extends LoadableElementBase {

        private var _smoothing:Boolean;

        public function ImageElement(_arg1:URLResource=null, _arg2:ImageLoader=null){
            if (_arg2 == null){
                _arg2 = new ImageLoader();
            };
            super(_arg1, _arg2);
        }
        public function get smoothing():Boolean{
            return (this._smoothing);
        }
        public function set smoothing(_arg1:Boolean):void{
            if (this._smoothing != _arg1){
                this._smoothing = _arg1;
                this.applySmoothingSetting();
            };
        }
        override protected function createLoadTrait(_arg1:MediaResourceBase, _arg2:LoaderBase):LoadTrait{
            return (new LoaderLoadTrait(_arg2, _arg1));
        }
        override protected function processReadyState():void{
            var _local1:LoaderLoadTrait = (getTrait(MediaTraitType.LOAD) as LoaderLoadTrait);
            addTrait(MediaTraitType.DISPLAY_OBJECT, LoaderUtils.createDisplayObjectTrait(_local1.loader, this));
            this.applySmoothingSetting();
        }
        override protected function processUnloadingState():void{
            removeTrait(MediaTraitType.DISPLAY_OBJECT);
        }
        private function applySmoothingSetting():void{
            var _local2:Loader;
            var _local3:Bitmap;
            var _local1:DisplayObjectTrait = (getTrait(MediaTraitType.DISPLAY_OBJECT) as DisplayObjectTrait);
            if (_local1){
                _local2 = (_local1.displayObject as Loader);
                if (_local2 != null){
                    try {
                        _local3 = (_local2.content as Bitmap);
                        if (_local3 != null){
                            _local3.smoothing = this._smoothing;
                        };
                    } catch(error:SecurityError) {
                    };
                };
            };
        }

    }
}//package org.osmf.elements 
