﻿package org.osmf.elements {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.media.*;
    import org.osmf.elements.audioClasses.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class SoundLoader extends LoaderBase {

        private static const MIME_TYPES_SUPPORTED:Vector.<String> = Vector.<String>(["audio/mpeg"]);
        private static const MEDIA_TYPES_SUPPORTED:Vector.<String> = Vector.<String>([MediaType.AUDIO]);
        private static const MIN_BYTES_TO_RECEIVE:int = 16;

        private var checkPolicyFile:Boolean;

        public function SoundLoader(_arg1:Boolean=false){
            this.checkPolicyFile = _arg1;
        }
        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            var _local2:int = MediaTypeUtil.checkMetadataMatchWithResource(_arg1, MEDIA_TYPES_SUPPORTED, MIME_TYPES_SUPPORTED);
            if (_local2 != MediaTypeUtil.METADATA_MATCH_UNKNOWN){
                return ((_local2 == MediaTypeUtil.METADATA_MATCH_FOUND));
            };
            var _local3:URLResource = (_arg1 as URLResource);
            if ((((((_local3 == null)) || ((_local3.url == null)))) || ((_local3.url.length <= 0)))){
                return (false);
            };
            var _local4:URL = new URL(_local3.url);
            if (_local4.protocol == ""){
                return (!((_local4.path.search(/\.mp3$|\.m4a$/i) == -1)));
            };
            if (_local4.protocol.search(/file$|http$|https$/i) != -1){
                return ((((((((_local4.path == null)) || ((_local4.path.length <= 0)))) || ((_local4.path.indexOf(".") == -1)))) || (!((_local4.path.search(/\.mp3$|\.m4a$/i) == -1)))));
            };
            return (false);
        }
        override protected function executeLoad(_arg1:LoadTrait):void{
            var soundLoadTrait:* = null;
            var sound:* = null;
            var onProgress:* = null;
            var onIOError:* = null;
            var loadTrait:* = _arg1;
            var toggleSoundListeners:* = function (_arg1:Sound, _arg2:Boolean):void{
                if (_arg2){
                    _arg1.addEventListener(ProgressEvent.PROGRESS, onProgress);
                    _arg1.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
                } else {
                    _arg1.removeEventListener(ProgressEvent.PROGRESS, onProgress);
                    _arg1.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
                };
            };
            onProgress = function (_arg1:ProgressEvent):void{
                if ((((_arg1.bytesTotal >= MIN_BYTES_TO_RECEIVE)) && ((soundLoadTrait.loadState == LoadState.LOADING)))){
                    toggleSoundListeners(sound, false);
                    soundLoadTrait.sound = sound;
                    updateLoadTrait(soundLoadTrait, LoadState.READY);
                };
            };
            onIOError = function (_arg1:IOErrorEvent, _arg2:String=null):void{
                toggleSoundListeners(sound, false);
                updateLoadTrait(loadTrait, LoadState.LOAD_ERROR);
                loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.IO_ERROR, ((_arg1) ? _arg1.text : _arg2))));
            };
            var handleSecurityError:* = function (_arg1:String):void{
                toggleSoundListeners(sound, false);
                updateLoadTrait(loadTrait, LoadState.LOAD_ERROR);
                loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.SECURITY_ERROR, _arg1)));
            };
            soundLoadTrait = (loadTrait as SoundLoadTrait);
            updateLoadTrait(soundLoadTrait, LoadState.LOADING);
            sound = new Sound();
            toggleSoundListeners(sound, true);
            var urlRequest:* = new URLRequest((soundLoadTrait.resource as URLResource).url.toString());
            var context:* = new SoundLoaderContext(1000, this.checkPolicyFile);
            try {
                sound.load(urlRequest, context);
            } catch(ioError:IOError) {
                onIOError(null, ioError.message);
            } catch(securityError:SecurityError) {
                handleSecurityError(securityError.message);
            };
        }
        override protected function executeUnload(_arg1:LoadTrait):void{
            var _local2:SoundLoadTrait = (_arg1 as SoundLoadTrait);
            updateLoadTrait(_local2, LoadState.UNLOADING);
            try {
                if (_local2.sound != null){
                    _local2.sound.close();
                };
            } catch(error:IOError) {
            };
            updateLoadTrait(_local2, LoadState.UNINITIALIZED);
        }

    }
}//package org.osmf.elements 
