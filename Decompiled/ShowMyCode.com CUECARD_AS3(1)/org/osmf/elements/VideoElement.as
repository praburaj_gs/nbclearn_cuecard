﻿package org.osmf.elements {
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.net.*;
    import org.osmf.net.httpstreaming.*;
    import org.osmf.traits.*;
    import org.osmf.net.rtmpstreaming.*;

    public class VideoElement extends LightweightVideoElement {

        private var _alternateLoaders:Vector.<LoaderBase>;

        public function VideoElement(_arg1:MediaResourceBase=null, _arg2:NetLoader=null){
            super(null, null);
            super.loader = _arg2;
            this.resource = _arg1;
        }
        override public function set resource(_arg1:MediaResourceBase):void{
            loader = getLoaderForResource(_arg1, this.alternateLoaders);
            super.resource = _arg1;
        }
        private function get alternateLoaders():Vector.<LoaderBase>{
            if (this._alternateLoaders == null){
                this._alternateLoaders = new Vector.<LoaderBase>();
                this._alternateLoaders.push(new HTTPStreamingNetLoader());
                this._alternateLoaders.push(new RTMPDynamicStreamingNetLoader());
                this._alternateLoaders.push(new NetLoader());
            };
            return (this._alternateLoaders);
        }

    }
}//package org.osmf.elements 
