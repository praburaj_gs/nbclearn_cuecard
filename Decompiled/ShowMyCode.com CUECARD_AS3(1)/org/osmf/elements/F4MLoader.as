﻿package org.osmf.elements {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.elements.f4mClasses.*;
    import org.osmf.net.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.elements.f4mClasses.builders.*;
    import org.osmf.utils.*;
    import org.osmf.elements.proxyClasses.*;

    public class F4MLoader extends LoaderBase {

        public static const F4M_MIME_TYPE:String = "application/f4m+xml";
        private static const F4M_EXTENSION:String = "f4m";

        private var supportedMimeTypes:Vector.<String>;
        private var factory:MediaFactory;
        private var parser:ManifestParser;
        private var parserTimer:Timer;
        private var builders:Vector.<BaseManifestBuilder>;
        private var loadTrait:LoadTrait;

        public function F4MLoader(_arg1:MediaFactory=null){
            this.supportedMimeTypes = new Vector.<String>();
            super();
            this.supportedMimeTypes.push(F4M_MIME_TYPE);
            if (_arg1 == null){
                _arg1 = new DefaultMediaFactory();
            };
            this.factory = _arg1;
            this.builders = this.getBuilders();
        }
        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            var _local3:URLResource;
            var _local4:String;
            var _local5:DynamicStreamingResource;
            var _local2:int = MediaTypeUtil.checkMetadataMatchWithResource(_arg1, new Vector.<String>(), this.supportedMimeTypes);
            if (_local2 == MediaTypeUtil.METADATA_MATCH_FOUND){
                return (true);
            };
            if ((_arg1 is URLResource)){
                if ((_arg1 is DynamicStreamingResource)){
                    _local5 = DynamicStreamingResource(_arg1);
                    if (_local5.streamItems.length > 0){
                        return (false);
                    };
                };
                _local3 = URLResource(_arg1);
                _local4 = new URL(_local3.url).extension;
                return ((_local4 == F4M_EXTENSION));
            };
            return (false);
        }
        override protected function executeLoad(_arg1:LoadTrait):void{
            var manifest:* = null;
            var manifestLoader:* = null;
            var onError:* = null;
            var onComplete:* = null;
            var loadTrait:* = _arg1;
            onError = function (_arg1:ErrorEvent):void{
                manifestLoader.removeEventListener(Event.COMPLETE, onComplete);
                manifestLoader.removeEventListener(IOErrorEvent.IO_ERROR, onError);
                manifestLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
                updateLoadTrait(loadTrait, LoadState.LOAD_ERROR);
                loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(0, _arg1.text)));
            };
            onComplete = function (_arg1:Event):void{
                var resourceData:* = null;
                var event:* = _arg1;
                manifestLoader.removeEventListener(Event.COMPLETE, onComplete);
                manifestLoader.removeEventListener(IOErrorEvent.IO_ERROR, onError);
                manifestLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
                try {
                    resourceData = String((event.target as URLLoader).data);
                    parser = getParser(resourceData);
                    parser.addEventListener(ParseEvent.PARSE_COMPLETE, onParserLoadComplete);
                    parser.addEventListener(ParseEvent.PARSE_ERROR, onParserLoadError);
                    parserTimer = new Timer(OSMFSettings.f4mParseTimeout, 1);
                    parserTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onParserTimerComplete);
                    parserTimer.start();
                    parser.parse(resourceData, URL.normalizePathForURL(URLResource(loadTrait.resource).url, true));
                } catch(parseError:Error) {
                    updateLoadTrait(loadTrait, LoadState.LOAD_ERROR);
                    loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(parseError.errorID, parseError.message)));
                };
            };
            this.loadTrait = loadTrait;
            updateLoadTrait(loadTrait, LoadState.LOADING);
            manifestLoader = new URLLoader(new URLRequest(URLResource(loadTrait.resource).url));
            manifestLoader.addEventListener(Event.COMPLETE, onComplete);
            manifestLoader.addEventListener(IOErrorEvent.IO_ERROR, onError);
            manifestLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
        }
        private function onParserTimerComplete(_arg1:TimerEvent):void{
            if (this.parserTimer){
                this.parserTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, this.onParserTimerComplete);
                this.parserTimer = null;
            };
            updateLoadTrait(this.loadTrait, LoadState.LOAD_ERROR);
            this.loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.F4M_FILE_INVALID, OSMFStrings.getString(OSMFStrings.F4M_PARSE_ERROR))));
        }
        private function onParserLoadComplete(_arg1:ParseEvent):void{
            if (this.parserTimer){
                this.parserTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, this.onParserTimerComplete);
                this.parserTimer.stop();
                this.parserTimer = null;
            };
            this.parser.removeEventListener(ParseEvent.PARSE_COMPLETE, this.onParserLoadComplete);
            this.parser.removeEventListener(ParseEvent.PARSE_ERROR, this.onParserLoadError);
            var _local2:Manifest = (_arg1.data as Manifest);
            this.finishManifestLoad(_local2);
        }
        private function onParserLoadError(_arg1:ParseEvent):void{
            if (this.parserTimer){
                this.parserTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, this.onParserTimerComplete);
                this.parserTimer.stop();
                this.parserTimer = null;
            };
            this.parser.removeEventListener(ParseEvent.PARSE_COMPLETE, this.onParserLoadComplete);
            this.parser.removeEventListener(ParseEvent.PARSE_ERROR, this.onParserLoadError);
            updateLoadTrait(this.loadTrait, LoadState.LOAD_ERROR);
            this.loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.F4M_FILE_INVALID, OSMFStrings.getString(OSMFStrings.F4M_PARSE_ERROR))));
        }
        private function finishManifestLoad(_arg1:Manifest):void{
            var netResource:* = null;
            var loadedElem:* = null;
            var manifest:* = _arg1;
            try {
                netResource = this.parser.createResource(manifest, URLResource(this.loadTrait.resource));
                loadedElem = this.factory.createMediaElement(netResource);
                if (((loadedElem.hasOwnProperty("defaultDuration")) && (!(isNaN(manifest.duration))))){
                    loadedElem["defaultDuration"] = manifest.duration;
                };
                LoadFromDocumentLoadTrait(this.loadTrait).mediaElement = loadedElem;
                updateLoadTrait(this.loadTrait, LoadState.READY);
            } catch(error:Error) {
                updateLoadTrait(loadTrait, LoadState.LOAD_ERROR);
                loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.F4M_FILE_INVALID, error.message)));
            };
        }
        override protected function executeUnload(_arg1:LoadTrait):void{
            updateLoadTrait(_arg1, LoadState.UNINITIALIZED);
        }
        protected function getBuilders():Vector.<BaseManifestBuilder>{
            var _local1:Vector.<BaseManifestBuilder> = new Vector.<BaseManifestBuilder>();
            _local1.push(new MultiLevelManifestBuilder());
            _local1.push(new ManifestBuilder());
            return (_local1);
        }
        private function getParser(_arg1:String):ManifestParser{
            var _local2:ManifestParser;
            var _local3:BaseManifestBuilder;
            for each (_local3 in this.builders) {
                if (_local3.canParse(_arg1)){
                    _local2 = (_local3.build(_arg1) as ManifestParser);
                    break;
                };
            };
            return (_local2);
        }

    }
}//package org.osmf.elements 
