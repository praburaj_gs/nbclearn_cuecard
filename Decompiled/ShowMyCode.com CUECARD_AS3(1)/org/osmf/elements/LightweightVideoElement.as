﻿package org.osmf.elements {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.metadata.*;
    import org.osmf.net.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.media.videoClasses.*;
    import flash.media.*;
    import flash.system.*;
    import org.osmf.utils.*;
    import org.osmf.net.drm.*;

    public class LightweightVideoElement extends LoadableElementBase {

        private static const DRM_STATUS_CODE:String = "DRM.encryptedFLV";
        private static const DRM_NEEDS_AUTHENTICATION:int = 3330;

        private var displayObjectTrait:DisplayObjectTrait;
        private var defaultTimeTrait:ModifiableTimeTrait;
        private var stream:NetStream;
        private var embeddedCuePoints:TimelineMetadata;
        private var _smoothing:Boolean;
        private var _deblocking:int;
        private var videoSurface:VideoSurface;
        private var drmTrait:NetStreamDRMTrait;

        public function LightweightVideoElement(_arg1:MediaResourceBase=null, _arg2:NetLoader=null){
            if (_arg2 == null){
                _arg2 = new NetLoader();
            };
            super(_arg1, _arg2);
            if (!(((_arg1 == null)) || ((_arg1 is URLResource)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
        }
        public function get client():NetClient{
            return (((this.stream)!=null) ? (this.stream.client as NetClient) : null);
        }
        public function get defaultDuration():Number{
            return (((this.defaultTimeTrait) ? this.defaultTimeTrait.duration : NaN));
        }
        public function set defaultDuration(_arg1:Number):void{
            if (((isNaN(_arg1)) || ((_arg1 < 0)))){
                if (this.defaultTimeTrait != null){
                    removeTraitResolver(MediaTraitType.TIME);
                    this.defaultTimeTrait = null;
                };
            } else {
                if (this.defaultTimeTrait == null){
                    this.defaultTimeTrait = new ModifiableTimeTrait();
                    addTraitResolver(MediaTraitType.TIME, new DefaultTraitResolver(MediaTraitType.TIME, this.defaultTimeTrait));
                };
                this.defaultTimeTrait.duration = _arg1;
            };
        }
        public function get smoothing():Boolean{
            return (this._smoothing);
        }
        public function set smoothing(_arg1:Boolean):void{
            this._smoothing = _arg1;
            if (this.videoSurface != null){
                this.videoSurface.smoothing = _arg1;
            };
        }
        public function get deblocking():int{
            return (this._deblocking);
        }
        public function set deblocking(_arg1:int):void{
            this._deblocking = _arg1;
            if (this.videoSurface != null){
                this.videoSurface.deblocking = _arg1;
            };
        }
        public function get currentFPS():Number{
            return (((this.stream)!=null) ? this.stream.currentFPS : 0);
        }
        override protected function createLoadTrait(_arg1:MediaResourceBase, _arg2:LoaderBase):LoadTrait{
            return (new NetStreamLoadTrait(_arg2, _arg1));
        }
        protected function createVideo():Video{
            return (new Video());
        }
        override protected function processReadyState():void{
            var _local2:ByteArray;
            var _local1:NetStreamLoadTrait = (getTrait(MediaTraitType.LOAD) as NetStreamLoadTrait);
            this.stream = _local1.netStream;
            this.videoSurface = new VideoSurface(((OSMFSettings.enableStageVideo) && (OSMFSettings.supportsStageVideo)), this.createVideo);
            this.videoSurface.smoothing = this._smoothing;
            this.videoSurface.deblocking = this._deblocking;
            this.videoSurface.width = (this.videoSurface.height = 0);
            this.videoSurface.attachNetStream(this.stream);
            NetClient(this.stream.client).addHandler(NetStreamCodes.ON_META_DATA, this.onMetaData);
            NetClient(this.stream.client).addHandler(NetStreamCodes.ON_CUE_POINT, this.onCuePoint);
            this.stream.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatusEvent);
            _local1.connection.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatusEvent, false, 0, true);
            this.stream.addEventListener(DRMErrorEvent.DRM_ERROR, this.onDRMErrorEvent);
            _local2 = this.getDRMContentData(resource);
            if (((!((_local2 == null))) && ((_local2.bytesAvailable > 0)))){
                this.setupDRMTrait(_local2);
            } else {
                this.stream.addEventListener(StatusEvent.STATUS, this.onStatus);
                this.stream.addEventListener(DRMStatusEvent.DRM_STATUS, this.onDRMStatus);
            };
            this.finishLoad();
        }
        private function finishLoad():void{
            var timeTrait:* = null;
            var seekTrait:* = null;
            var reconnectStreams:* = false;
            var onDurationChange:* = null;
            var dsTrait:* = null;
            var aaTrait:* = null;
            var loadTrait:* = (getTrait(MediaTraitType.LOAD) as NetStreamLoadTrait);
            var dvrTrait:* = (loadTrait.getTrait(MediaTraitType.DVR) as DVRTrait);
            if (dvrTrait != null){
                addTrait(MediaTraitType.DVR, dvrTrait);
            };
            var audioTrait:* = (loadTrait.getTrait(MediaTraitType.AUDIO) as AudioTrait);
            if (audioTrait == null){
                audioTrait = new NetStreamAudioTrait(this.stream);
            };
            addTrait(MediaTraitType.AUDIO, audioTrait);
            var bufferTrait:* = (loadTrait.getTrait(MediaTraitType.BUFFER) as BufferTrait);
            if (bufferTrait == null){
                bufferTrait = new NetStreamBufferTrait(this.stream, this.videoSurface);
            };
            addTrait(MediaTraitType.BUFFER, bufferTrait);
            timeTrait = (loadTrait.getTrait(MediaTraitType.TIME) as TimeTrait);
            if (timeTrait == null){
                timeTrait = new NetStreamTimeTrait(this.stream, loadTrait.resource, this.defaultDuration);
            };
            addTrait(MediaTraitType.TIME, timeTrait);
            var displayObjectTrait:* = (loadTrait.getTrait(MediaTraitType.DISPLAY_OBJECT) as DisplayObjectTrait);
            if (displayObjectTrait == null){
                displayObjectTrait = new NetStreamDisplayObjectTrait(this.stream, this.videoSurface, NaN, NaN);
            };
            addTrait(MediaTraitType.DISPLAY_OBJECT, displayObjectTrait);
            var playTrait:* = (loadTrait.getTrait(MediaTraitType.PLAY) as PlayTrait);
            if (playTrait == null){
                reconnectStreams = false;
                reconnectStreams = (loader as NetLoader).reconnectStreams;
                playTrait = new NetStreamPlayTrait(this.stream, resource, reconnectStreams, loadTrait.connection);
            };
            addTrait(MediaTraitType.PLAY, playTrait);
            seekTrait = (loadTrait.getTrait(MediaTraitType.SEEK) as SeekTrait);
            if ((((seekTrait == null)) && (!((NetStreamUtils.getStreamType(resource) == StreamType.LIVE))))){
                seekTrait = new NetStreamSeekTrait(timeTrait, loadTrait, this.stream, this.videoSurface);
            };
            if (seekTrait != null){
                if (((isNaN(timeTrait.duration)) || ((timeTrait.duration == 0)))){
                    onDurationChange = function (_arg1:TimeEvent):void{
                        timeTrait.removeEventListener(TimeEvent.DURATION_CHANGE, onDurationChange);
                        addTrait(MediaTraitType.SEEK, seekTrait);
                    };
                    timeTrait.addEventListener(TimeEvent.DURATION_CHANGE, onDurationChange);
                } else {
                    addTrait(MediaTraitType.SEEK, seekTrait);
                };
            };
            var dsResource:* = (resource as DynamicStreamingResource);
            if (((!((dsResource == null))) && (!((loadTrait.switchManager == null))))){
                dsTrait = (loadTrait.getTrait(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait);
                if (dsTrait == null){
                    dsTrait = new NetStreamDynamicStreamTrait(this.stream, loadTrait.switchManager, dsResource);
                };
                addTrait(MediaTraitType.DYNAMIC_STREAM, dsTrait);
            };
            var sResource:* = (resource as StreamingURLResource);
            if (((((!((sResource == null))) && (!((sResource.alternativeAudioStreamItems == null))))) && ((sResource.alternativeAudioStreamItems.length > 0)))){
                aaTrait = (loadTrait.getTrait(MediaTraitType.ALTERNATIVE_AUDIO) as AlternativeAudioTrait);
                if (aaTrait == null){
                    aaTrait = new NetStreamAlternativeAudioTrait(this.stream, sResource);
                };
                addTrait(MediaTraitType.ALTERNATIVE_AUDIO, aaTrait);
            };
        }
        override protected function processUnloadingState():void{
            if (this.stream != null){
                this.stream.removeEventListener(NetStatusEvent.NET_STATUS, this.onNetStatusEvent);
                if (this.stream.client != null){
                    NetClient(this.stream.client).removeHandler(NetStreamCodes.ON_META_DATA, this.onMetaData);
                };
            };
            var _local1:NetStreamLoadTrait = (getTrait(MediaTraitType.LOAD) as NetStreamLoadTrait);
            if (((!((_local1 == null))) && (!((_local1.connection == null))))){
                _local1.connection.removeEventListener(NetStatusEvent.NET_STATUS, this.onNetStatusEvent);
            };
            removeTrait(MediaTraitType.AUDIO);
            removeTrait(MediaTraitType.BUFFER);
            removeTrait(MediaTraitType.PLAY);
            removeTrait(MediaTraitType.TIME);
            removeTrait(MediaTraitType.DISPLAY_OBJECT);
            removeTrait(MediaTraitType.SEEK);
            removeTrait(MediaTraitType.DYNAMIC_STREAM);
            removeTrait(MediaTraitType.ALTERNATIVE_AUDIO);
            removeTrait(MediaTraitType.DVR);
            if (this.stream != null){
                this.stream.removeEventListener(DRMErrorEvent.DRM_ERROR, this.onDRMErrorEvent);
                this.stream.removeEventListener(DRMStatusEvent.DRM_STATUS, this.onDRMStatus);
                this.stream.removeEventListener(StatusEvent.STATUS, this.onStatus);
            };
            if (this.drmTrait != null){
                this.drmTrait.removeEventListener(DRMEvent.DRM_STATE_CHANGE, this.reloadAfterAuth);
                removeTrait(MediaTraitType.DRM);
                this.drmTrait = null;
            };
            if (this.videoSurface != null){
                this.videoSurface.attachNetStream(null);
            };
            this.videoSurface = null;
            this.stream = null;
            this.displayObjectTrait = null;
        }
        private function onMetaData(_arg1:Object):void{
            var _local3:TimelineMetadata;
            var _local4:int;
            var _local5:CuePoint;
            var _local2:Array = _arg1.cuePoints;
            if (((!((_local2 == null))) && ((_local2.length > 0)))){
                _local3 = (getMetadata(CuePoint.DYNAMIC_CUEPOINTS_NAMESPACE) as TimelineMetadata);
                if (_local3 == null){
                    _local3 = new TimelineMetadata(this);
                    addMetadata(CuePoint.DYNAMIC_CUEPOINTS_NAMESPACE, _local3);
                };
                _local4 = 0;
                while (_local4 < _local2.length) {
                    _local5 = new CuePoint(_local2[_local4].type, _local2[_local4].time, _local2[_local4].name, _local2[_local4].parameters);
                    try {
                        _local3.addMarker(_local5);
                    } catch(error:ArgumentError) {
                    };
                    _local4++;
                };
            };
        }
        private function onCuePoint(_arg1:Object):void{
            if (this.embeddedCuePoints == null){
                this.embeddedCuePoints = new TimelineMetadata(this);
                addMetadata(CuePoint.EMBEDDED_CUEPOINTS_NAMESPACE, this.embeddedCuePoints);
            };
            var _local2:CuePoint = new CuePoint(_arg1.type, _arg1.time, _arg1.name, _arg1.parameters);
            try {
                this.embeddedCuePoints.addMarker(_local2);
            } catch(error:ArgumentError) {
            };
        }
        private function onUpdateComplete(_arg1:Event):void{
            (getTrait(MediaTraitType.LOAD) as LoadTrait).unload();
            (getTrait(MediaTraitType.LOAD) as LoadTrait).load();
        }
        private function onNetStatusEvent(_arg1:NetStatusEvent):void{
            var _local2:MediaError;
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_PLAY_FAILED:
                case NetStreamCodes.NETSTREAM_FAILED:
                    _local2 = new MediaError(MediaErrorCodes.NETSTREAM_PLAY_FAILED, _arg1.info.description);
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_STREAMNOTFOUND:
                    _local2 = new MediaError(MediaErrorCodes.NETSTREAM_STREAM_NOT_FOUND, _arg1.info.description);
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_FILESTRUCTUREINVALID:
                    _local2 = new MediaError(MediaErrorCodes.NETSTREAM_FILE_STRUCTURE_INVALID, _arg1.info.description);
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_NOSUPPORTEDTRACKFOUND:
                    _local2 = new MediaError(MediaErrorCodes.NETSTREAM_NO_SUPPORTED_TRACK_FOUND, _arg1.info.description);
                    break;
                case NetConnectionCodes.CONNECT_IDLE_TIME_OUT:
                    _local2 = new MediaError(MediaErrorCodes.NETCONNECTION_TIMEOUT, _arg1.info.description);
                    break;
            };
            if (_arg1.info.code == NetStreamCodes.NETSTREAM_DRM_UPDATE){
                this.update(SystemUpdaterType.DRM);
            };
            if (_local2 != null){
                dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, _local2));
            };
        }
        private function getDRMContentData(_arg1:MediaResourceBase):ByteArray{
            var _local3:Metadata;
            var _local4:String;
            var _local5:DynamicStreamingResource;
            var _local6:ByteArray;
            var _local7:Vector.<String>;
            var _local8:int;
            var _local9:String;
            var _local2:StreamingURLResource = (_arg1 as StreamingURLResource);
            if (_local2 != null){
                if (_local2.drmContentData != null){
                    return (_local2.drmContentData);
                };
                _local3 = (_arg1.getMetadataValue(MetadataNamespaces.DRM_METADATA) as Metadata);
                if (((!((_local3 == null))) && ((_local3.keys.length > 0)))){
                    _local4 = null;
                    _local5 = (_arg1 as DynamicStreamingResource);
                    if (((((!((_local5 == null))) && ((_local5.initialIndex > -1)))) && ((_local5.initialIndex < _local5.streamItems.length)))){
                        _local4 = _local5.streamItems[_local5.initialIndex].streamName;
                    };
                    _local6 = null;
                    if (_local4 != null){
                        _local6 = (_local3.getValue(_local4) as ByteArray);
                    };
                    if (_local6 == null){
                        _local7 = _local3.keys;
                        _local8 = 0;
                        do  {
                            _local9 = _local7[_local8];
                            if (_local9.indexOf(MetadataNamespaces.DRM_ADDITIONAL_HEADER_KEY) != 0){
                                _local6 = _local3.getValue(_local9);
                            };
                            _local8++;
                        } while ((((_local6 == null)) && ((_local8 < _local7.length))));
                    };
                    return (_local6);
                };
            };
            return (null);
        }
        private function onStatus(_arg1:StatusEvent):void{
            if ((((_arg1.code == DRM_STATUS_CODE)) && ((getTrait(MediaTraitType.DRM) == null)))){
                this.createDRMTrait();
            };
        }
        private function onDRMStatus(_arg1:DRMStatusEvent):void{
            this.drmTrait.inlineOnVoucher(_arg1);
        }
        private function reloadAfterAuth(_arg1:DRMEvent):void{
            var _local2:NetStreamLoadTrait;
            if (this.drmTrait.drmState == DRMState.AUTHENTICATION_COMPLETE){
                _local2 = (getTrait(MediaTraitType.LOAD) as NetStreamLoadTrait);
                if (_local2.loadState == LoadState.READY){
                    _local2.unload();
                };
                _local2.load();
            };
        }
        private function createDRMTrait():void{
            this.drmTrait = new NetStreamDRMTrait();
            addTrait(MediaTraitType.DRM, this.drmTrait);
        }
        private function setupDRMTrait(_arg1:ByteArray):void{
            this.createDRMTrait();
            this.drmTrait.drmMetadata = _arg1;
        }
        private function onDRMErrorEvent(_arg1:DRMErrorEvent):void{
            if (_arg1.errorID == DRM_NEEDS_AUTHENTICATION){
                this.drmTrait.addEventListener(DRMEvent.DRM_STATE_CHANGE, this.reloadAfterAuth);
                this.drmTrait.drmMetadata = _arg1.contentData;
            } else {
                if (_arg1.drmUpdateNeeded){
                    this.update(SystemUpdaterType.DRM);
                } else {
                    if (_arg1.systemUpdateNeeded){
                        this.update(SystemUpdaterType.SYSTEM);
                    } else {
                        this.drmTrait.inlineDRMFailed(new MediaError(_arg1.errorID));
                    };
                };
            };
        }
        private function update(_arg1:String):void{
            if (this.drmTrait == null){
                this.createDRMTrait();
            };
            var _local2:SystemUpdater = this.drmTrait.update(_arg1);
            _local2.addEventListener(Event.COMPLETE, this.onUpdateComplete);
        }

    }
}//package org.osmf.elements 
