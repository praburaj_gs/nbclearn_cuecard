﻿package org.osmf.elements {
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.metadata.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import org.osmf.utils.*;
    import org.osmf.elements.proxyClasses.*;

    public class ProxyElement extends MediaElement {

        private var _proxiedMetadata:ProxyMetadata;
        private var _proxiedElement:MediaElement;
        private var _blockedTraits:Vector.<String>;

        public function ProxyElement(_arg1:MediaElement=null){
            this.addEventListener(MediaElementEvent.TRAIT_ADD, this.onProxyTraitAdd, false, int.MAX_VALUE);
            this.addEventListener(MediaElementEvent.TRAIT_REMOVE, this.onProxyTraitRemove, false, int.MAX_VALUE);
            this.addEventListener(ContainerChangeEvent.CONTAINER_CHANGE, this.onProxyContainerChange);
            this.proxiedElement = _arg1;
        }
        public function get proxiedElement():MediaElement{
            return (this._proxiedElement);
        }
        public function set proxiedElement(_arg1:MediaElement):void{
            var _local2:String;
            if (_arg1 != this._proxiedElement){
                if (this._proxiedElement != null){
                    this.toggleMediaElementListeners(this._proxiedElement, false);
                    for each (_local2 in this._proxiedElement.traitTypes) {
                        if ((((super.hasTrait(_local2) == false)) && ((((this._blockedTraits == null)) || ((this._blockedTraits.indexOf(_local2) == -1)))))){
                            super.dispatchEvent(new MediaElementEvent(MediaElementEvent.TRAIT_REMOVE, false, false, _local2));
                        };
                    };
                };
                this._proxiedElement = _arg1;
                if (this._proxiedElement != null){
                    ProxyMetadata(metadata).metadata = this._proxiedElement.metadata;
                    this._proxiedElement.dispatchEvent(new ContainerChangeEvent(ContainerChangeEvent.CONTAINER_CHANGE, false, false, this._proxiedElement.container, container));
                    this.toggleMediaElementListeners(this._proxiedElement, true);
                    for each (_local2 in this._proxiedElement.traitTypes) {
                        if ((((super.hasTrait(_local2) == false)) && ((((this._blockedTraits == null)) || ((this._blockedTraits.indexOf(_local2) == -1)))))){
                            super.dispatchEvent(new MediaElementEvent(MediaElementEvent.TRAIT_ADD, false, false, _local2));
                        };
                    };
                };
            };
        }
        override public function get traitTypes():Vector.<String>{
            var _local2:String;
            var _local1:Vector.<String> = new Vector.<String>();
            for each (_local2 in MediaTraitType.ALL_TYPES) {
                if (this.hasTrait(_local2)){
                    _local1.push(_local2);
                };
            };
            return (_local1);
        }
        override public function hasTrait(_arg1:String):Boolean{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            return (!((this.getTrait(_arg1) == null)));
        }
        override public function getTrait(_arg1:String):MediaTraitBase{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            var _local2:MediaTraitBase;
            if (this.blocksTrait(_arg1) == false){
                _local2 = ((super.getTrait(_arg1)) || (((this.proxiedElement)!=null) ? this.proxiedElement.getTrait(_arg1) : null));
            };
            return (_local2);
        }
        override public function get resource():MediaResourceBase{
            return (((this.proxiedElement) ? this.proxiedElement.resource : null));
        }
        override public function set resource(_arg1:MediaResourceBase):void{
            if (this.proxiedElement != null){
                this.proxiedElement.resource = _arg1;
            };
        }
        override protected function addTrait(_arg1:String, _arg2:MediaTraitBase):void{
            if ((((((this.blocksTrait(_arg1) == false)) && (!((this.proxiedElement == null))))) && ((this.proxiedElement.hasTrait(_arg1) == true)))){
                super.dispatchEvent(new MediaElementEvent(MediaElementEvent.TRAIT_REMOVE, false, false, _arg1));
            };
            super.addTrait(_arg1, _arg2);
        }
        override protected function removeTrait(_arg1:String):MediaTraitBase{
            var _local2:MediaTraitBase = super.removeTrait(_arg1);
            if ((((((this.blocksTrait(_arg1) == false)) && (!((this.proxiedElement == null))))) && ((this.proxiedElement.hasTrait(_arg1) == true)))){
                super.dispatchEvent(new MediaElementEvent(MediaElementEvent.TRAIT_ADD, false, false, _arg1));
            };
            return (_local2);
        }
        override protected function createMetadata():Metadata{
            return (new ProxyMetadata());
        }
        final protected function get blockedTraits():Vector.<String>{
            if (this._blockedTraits == null){
                this._blockedTraits = new Vector.<String>();
            };
            return (this._blockedTraits);
        }
        final protected function set blockedTraits(_arg1:Vector.<String>):void{
            var _local4:String;
            if (_arg1 == this._blockedTraits){
                return;
            };
            var _local2:Array = [];
            var _local3:Array = [];
            if (this._proxiedElement != null){
                for each (_local4 in MediaTraitType.ALL_TYPES) {
                    if (_arg1.indexOf(_local4) != -1){
                        if ((((this._blockedTraits == null)) || ((this._blockedTraits.indexOf(_local4) == -1)))){
                            _local2.push(_local4);
                        };
                    } else {
                        if (((!((this._blockedTraits == null))) && (!((this._blockedTraits.indexOf(_local4) == -1))))){
                            _local3.push(_local4);
                        };
                    };
                };
            };
            if (this._proxiedElement != null){
                for each (_local4 in _local2) {
                    if (((this.proxiedElement.hasTrait(_local4)) || (super.hasTrait(_local4)))){
                        dispatchEvent(new MediaElementEvent(MediaElementEvent.TRAIT_REMOVE, false, false, _local4));
                    };
                };
                this._blockedTraits = _arg1;
                for each (_local4 in _local3) {
                    if (((this.proxiedElement.hasTrait(_local4)) || (super.hasTrait(_local4)))){
                        dispatchEvent(new MediaElementEvent(MediaElementEvent.TRAIT_ADD, false, false, _local4));
                    };
                };
            } else {
                this._blockedTraits = _arg1;
            };
        }
        private function toggleMediaElementListeners(_arg1:MediaElement, _arg2:Boolean):void{
            if (_arg2){
                this._proxiedElement.addEventListener(MediaErrorEvent.MEDIA_ERROR, this.onMediaError);
                this._proxiedElement.addEventListener(MediaElementEvent.TRAIT_ADD, this.onTraitAdd);
                this._proxiedElement.addEventListener(MediaElementEvent.TRAIT_REMOVE, this.onTraitRemove);
                this._proxiedElement.addEventListener(MediaElementEvent.METADATA_ADD, this.onMetadataEvent);
                this._proxiedElement.addEventListener(MediaElementEvent.METADATA_REMOVE, this.onMetadataEvent);
            } else {
                this._proxiedElement.removeEventListener(MediaErrorEvent.MEDIA_ERROR, this.onMediaError);
                this._proxiedElement.removeEventListener(MediaElementEvent.TRAIT_ADD, this.onTraitAdd);
                this._proxiedElement.removeEventListener(MediaElementEvent.TRAIT_REMOVE, this.onTraitRemove);
                this._proxiedElement.removeEventListener(MediaElementEvent.METADATA_ADD, this.onMetadataEvent);
                this._proxiedElement.removeEventListener(MediaElementEvent.METADATA_REMOVE, this.onMetadataEvent);
            };
        }
        private function onMediaError(_arg1:MediaErrorEvent):void{
            dispatchEvent(_arg1.clone());
        }
        private function onTraitAdd(_arg1:MediaElementEvent):void{
            this.processTraitsChangeEvent(_arg1);
        }
        private function onTraitRemove(_arg1:MediaElementEvent):void{
            this.processTraitsChangeEvent(_arg1);
        }
        private function onMetadataEvent(_arg1:MediaElementEvent):void{
            dispatchEvent(_arg1.clone());
        }
        private function onProxyContainerChange(_arg1:ContainerChangeEvent):void{
            if (this.proxiedElement != null){
                this.proxiedElement.dispatchEvent(_arg1.clone());
            };
        }
        private function onProxyTraitAdd(_arg1:MediaElementEvent):void{
            this.processProxyTraitsChangeEvent(_arg1);
        }
        private function onProxyTraitRemove(_arg1:MediaElementEvent):void{
            this.processProxyTraitsChangeEvent(_arg1);
        }
        private function processTraitsChangeEvent(_arg1:MediaElementEvent):void{
            if ((((this.blocksTrait(_arg1.traitType) == false)) && ((super.hasTrait(_arg1.traitType) == false)))){
                super.dispatchEvent(_arg1.clone());
            };
        }
        private function processProxyTraitsChangeEvent(_arg1:MediaElementEvent):void{
            if (this.blocksTrait(_arg1.traitType) == true){
                _arg1.stopImmediatePropagation();
            };
        }
        private function blocksTrait(_arg1:String):Boolean{
            return (((this._blockedTraits) && (!((this._blockedTraits.indexOf(_arg1) == -1)))));
        }

    }
}//package org.osmf.elements 
