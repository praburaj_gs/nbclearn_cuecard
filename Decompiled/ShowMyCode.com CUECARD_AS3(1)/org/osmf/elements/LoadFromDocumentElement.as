﻿package org.osmf.elements {
    import flash.events.*;
    import org.osmf.media.*;
    import org.osmf.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import org.osmf.utils.*;
    import org.osmf.elements.proxyClasses.*;

    public class LoadFromDocumentElement extends ProxyElement {

        private var _resource:MediaResourceBase;
        private var loadTrait:LoadFromDocumentLoadTrait;
        private var loader:LoaderBase;

        public function LoadFromDocumentElement(_arg1:MediaResourceBase=null, _arg2:LoaderBase=null){
            super(null);
            this.loader = _arg2;
            this.resource = _arg1;
            if (_arg2 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
        }
        override public function set resource(_arg1:MediaResourceBase):void{
            if (((!((this._resource == _arg1))) && (!((_arg1 == null))))){
                this._resource = _arg1;
                if ((_arg1 is StreamingXMLResource)){
                    this.loader = new XMLLoader();
                };
                this.loadTrait = new LoadFromDocumentLoadTrait(this.loader, this.resource);
                this.loadTrait.addEventListener(LoadEvent.LOAD_STATE_CHANGE, this.onLoadStateChange, false, int.MAX_VALUE);
                if (super.getTrait(MediaTraitType.LOAD) != null){
                    super.removeTrait(MediaTraitType.LOAD);
                };
                super.addTrait(MediaTraitType.LOAD, this.loadTrait);
            };
        }
        override public function get resource():MediaResourceBase{
            return (this._resource);
        }
        private function onLoaderStateChange(_arg1:Event):void{
            removeTrait(MediaTraitType.LOAD);
            proxiedElement = this.loadTrait.mediaElement;
        }
        private function onLoadStateChange(_arg1:LoadEvent):void{
            var proxiedLoadTrait:* = null;
            var onProxiedElementLoadStateChange:* = null;
            var event:* = _arg1;
            if (event.loadState == LoadState.READY){
                onProxiedElementLoadStateChange = function (_arg1:LoadEvent):void{
                    if (_arg1.loadState == LoadState.LOADING){
                        _arg1.stopImmediatePropagation();
                    } else {
                        proxiedLoadTrait.removeEventListener(LoadEvent.LOAD_STATE_CHANGE, onProxiedElementLoadStateChange);
                    };
                };
                event.stopImmediatePropagation();
                removeTrait(MediaTraitType.LOAD);
                proxiedLoadTrait = (this.loadTrait.mediaElement.getTrait(MediaTraitType.LOAD) as LoadTrait);
                proxiedLoadTrait.addEventListener(LoadEvent.LOAD_STATE_CHANGE, onProxiedElementLoadStateChange, false, int.MAX_VALUE);
                proxiedElement = this.loadTrait.mediaElement;
                if (proxiedLoadTrait.loadState == LoadState.UNINITIALIZED){
                    proxiedLoadTrait.load();
                };
            };
        }

    }
}//package org.osmf.elements 
