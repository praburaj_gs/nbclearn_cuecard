﻿package org.osmf.elements {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.net.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import org.osmf.elements.audioClasses.*;
    import org.osmf.utils.*;

    public class AudioElement extends LoadableElementBase {

        private var soundAdapter:SoundAdapter;
        private var stream:NetStream;
        private var defaultTimeTrait:ModifiableTimeTrait;
        private var _alternateLoaders:Vector.<LoaderBase>;

        public function AudioElement(_arg1:URLResource=null, _arg2:LoaderBase=null){
            super(_arg1, _arg2);
            if (!(((((_arg2 == null)) || ((_arg2 is NetLoader)))) || ((_arg2 is SoundLoader)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
        }
        public function get defaultDuration():Number{
            return (((this.defaultTimeTrait) ? this.defaultTimeTrait.duration : NaN));
        }
        public function set defaultDuration(_arg1:Number):void{
            if (((isNaN(_arg1)) || ((_arg1 < 0)))){
                if (this.defaultTimeTrait != null){
                    removeTraitResolver(MediaTraitType.TIME);
                    this.defaultTimeTrait = null;
                };
            } else {
                if (this.defaultTimeTrait == null){
                    this.defaultTimeTrait = new ModifiableTimeTrait();
                    addTraitResolver(MediaTraitType.TIME, new DefaultTraitResolver(MediaTraitType.TIME, this.defaultTimeTrait));
                };
                this.defaultTimeTrait.duration = _arg1;
            };
        }
        override public function set resource(_arg1:MediaResourceBase):void{
            loader = getLoaderForResource(_arg1, this.alternateLoaders);
            super.resource = _arg1;
        }
        override protected function createLoadTrait(_arg1:MediaResourceBase, _arg2:LoaderBase):LoadTrait{
            return ((((_arg2 is NetLoader)) ? new NetStreamLoadTrait(_arg2, _arg1) : new SoundLoadTrait(_arg2, _arg1)));
        }
        override protected function processReadyState():void{
            var _local2:TimeTrait;
            var _local4:Boolean;
            var _local5:SoundLoadTrait;
            var _local1:LoadTrait = (getTrait(MediaTraitType.LOAD) as LoadTrait);
            this.soundAdapter = null;
            this.stream = null;
            var _local3:NetStreamLoadTrait = (_local1 as NetStreamLoadTrait);
            if (_local3){
                this.stream = _local3.netStream;
                this.stream.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatusEvent);
                _local3.connection.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatusEvent, false, 0, true);
                _local4 = false;
                if ((loader is NetLoader)){
                    _local4 = (loader as NetLoader).reconnectStreams;
                };
                addTrait(MediaTraitType.PLAY, new NetStreamPlayTrait(this.stream, resource, _local4, _local3.connection));
                _local2 = new NetStreamTimeTrait(this.stream, resource, this.defaultDuration);
                addTrait(MediaTraitType.TIME, _local2);
                addTrait(MediaTraitType.SEEK, new NetStreamSeekTrait(_local2, _local1, this.stream));
                addTrait(MediaTraitType.AUDIO, new NetStreamAudioTrait(this.stream));
                addTrait(MediaTraitType.BUFFER, new NetStreamBufferTrait(this.stream));
            } else {
                _local5 = (_local1 as SoundLoadTrait);
                this.soundAdapter = new SoundAdapter(this, _local5.sound);
                addTrait(MediaTraitType.PLAY, new AudioPlayTrait(this.soundAdapter));
                _local2 = new AudioTimeTrait(this.soundAdapter);
                addTrait(MediaTraitType.TIME, _local2);
                addTrait(MediaTraitType.SEEK, new AudioSeekTrait(_local2, this.soundAdapter));
                addTrait(MediaTraitType.AUDIO, new AudioAudioTrait(this.soundAdapter));
            };
        }
        override protected function processUnloadingState():void{
            if (this.stream != null){
                this.stream.removeEventListener(NetStatusEvent.NET_STATUS, this.onNetStatusEvent);
            };
            var _local1:NetStreamLoadTrait = (getTrait(MediaTraitType.LOAD) as NetStreamLoadTrait);
            if (_local1 != null){
                _local1.connection.removeEventListener(NetStatusEvent.NET_STATUS, this.onNetStatusEvent);
            };
            removeTrait(MediaTraitType.PLAY);
            removeTrait(MediaTraitType.SEEK);
            removeTrait(MediaTraitType.TIME);
            removeTrait(MediaTraitType.AUDIO);
            removeTrait(MediaTraitType.BUFFER);
            if (this.soundAdapter != null){
                this.soundAdapter.pause();
            };
            this.soundAdapter = null;
            this.stream = null;
        }
        private function onNetStatusEvent(_arg1:NetStatusEvent):void{
            var _local2:MediaError;
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_PLAY_FAILED:
                case NetStreamCodes.NETSTREAM_FAILED:
                    _local2 = new MediaError(MediaErrorCodes.NETSTREAM_PLAY_FAILED, _arg1.info.description);
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_STREAMNOTFOUND:
                    _local2 = new MediaError(MediaErrorCodes.NETSTREAM_STREAM_NOT_FOUND, _arg1.info.description);
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_FILESTRUCTUREINVALID:
                    _local2 = new MediaError(MediaErrorCodes.NETSTREAM_FILE_STRUCTURE_INVALID, _arg1.info.description);
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_NOSUPPORTEDTRACKFOUND:
                    _local2 = new MediaError(MediaErrorCodes.NETSTREAM_NO_SUPPORTED_TRACK_FOUND, _arg1.info.description);
                    break;
                case NetConnectionCodes.CONNECT_IDLE_TIME_OUT:
                    _local2 = new MediaError(MediaErrorCodes.NETCONNECTION_TIMEOUT, _arg1.info.description);
                    break;
            };
            if (_local2 != null){
                dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, _local2));
            };
        }
        private function get alternateLoaders():Vector.<LoaderBase>{
            if (this._alternateLoaders == null){
                this._alternateLoaders = new Vector.<LoaderBase>();
                this._alternateLoaders.push(new SoundLoader());
                this._alternateLoaders.push(new NetLoader());
            };
            return (this._alternateLoaders);
        }

    }
}//package org.osmf.elements 
