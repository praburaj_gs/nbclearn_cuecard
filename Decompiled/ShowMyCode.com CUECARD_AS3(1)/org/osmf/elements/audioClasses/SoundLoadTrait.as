﻿package org.osmf.elements.audioClasses {
    import flash.events.*;
    import org.osmf.media.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.media.*;

    public class SoundLoadTrait extends LoadTrait {

        private var lastBytesTotal:Number;
        private var _sound:Sound;

        public function SoundLoadTrait(_arg1:LoaderBase, _arg2:MediaResourceBase){
            super(_arg1, _arg2);
        }
        public function get sound():Sound{
            return (this._sound);
        }
        public function set sound(_arg1:Sound):void{
            this._sound = _arg1;
        }
        override protected function loadStateChangeStart(_arg1:String):void{
            if (_arg1 == LoadState.READY){
                if (this._sound != null){
                    this._sound.addEventListener(Event.OPEN, this.bytesTotalCheckingHandler, false, 0, true);
                    this._sound.addEventListener(ProgressEvent.PROGRESS, this.bytesTotalCheckingHandler, false, 0, true);
                };
            } else {
                if (_arg1 == LoadState.UNINITIALIZED){
                    this._sound = null;
                };
            };
        }
        override public function get bytesLoaded():Number{
            return (((this._sound) ? this._sound.bytesLoaded : NaN));
        }
        override public function get bytesTotal():Number{
            return (((this._sound) ? this._sound.bytesTotal : NaN));
        }
        private function bytesTotalCheckingHandler(_arg1:Event):void{
            var _local2:LoadEvent;
            if (this.lastBytesTotal != this._sound.bytesTotal){
                _local2 = new LoadEvent(LoadEvent.BYTES_TOTAL_CHANGE, false, false, null, this._sound.bytesTotal);
                this.lastBytesTotal = this._sound.bytesTotal;
                dispatchEvent(_local2);
            };
        }

    }
}//package org.osmf.elements.audioClasses 
