﻿package org.osmf.elements.audioClasses {
    import org.osmf.traits.*;
    import flash.media.*;

    public class AudioAudioTrait extends AudioTrait {

        private var soundAdapter:SoundAdapter;

        public function AudioAudioTrait(_arg1:SoundAdapter){
            this.soundAdapter = _arg1;
            _arg1.soundTransform.volume = volume;
            _arg1.soundTransform.pan = pan;
        }
        override protected function volumeChangeStart(_arg1:Number):void{
            var _local2:SoundTransform = this.soundAdapter.soundTransform;
            _local2.volume = ((muted) ? 0 : _arg1);
            this.soundAdapter.soundTransform = _local2;
        }
        override protected function mutedChangeStart(_arg1:Boolean):void{
            var _local2:SoundTransform = this.soundAdapter.soundTransform;
            _local2.volume = ((_arg1) ? 0 : volume);
            this.soundAdapter.soundTransform = _local2;
        }
        override protected function panChangeStart(_arg1:Number):void{
            var _local2:SoundTransform = this.soundAdapter.soundTransform;
            _local2.pan = _arg1;
            this.soundAdapter.soundTransform = _local2;
        }

    }
}//package org.osmf.elements.audioClasses 
