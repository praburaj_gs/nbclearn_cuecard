﻿package org.osmf.elements.audioClasses {
    import flash.events.*;
    import org.osmf.traits.*;

    public class AudioTimeTrait extends TimeTrait {

        private var soundAdapter:SoundAdapter;

        public function AudioTimeTrait(_arg1:SoundAdapter){
            this.soundAdapter = _arg1;
            _arg1.addEventListener(ProgressEvent.PROGRESS, this.onDownloadProgress, false, 0, true);
            _arg1.addEventListener(SoundAdapter.DOWNLOAD_COMPLETE, this.onDownloadComplete, false, 0, true);
            _arg1.addEventListener(Event.COMPLETE, this.onPlaybackComplete, false, 0, true);
        }
        override public function get currentTime():Number{
            return (this.soundAdapter.currentTime);
        }
        private function onDownloadProgress(_arg1:Event):void{
            if (((!(isNaN(this.soundAdapter.estimatedDuration))) && ((this.soundAdapter.estimatedDuration > 0)))){
                this.soundAdapter.removeEventListener(ProgressEvent.PROGRESS, this.onDownloadProgress);
                setDuration(this.soundAdapter.estimatedDuration);
            };
        }
        private function onDownloadComplete(_arg1:Event):void{
            setDuration(this.soundAdapter.estimatedDuration);
        }
        private function onPlaybackComplete(_arg1:Event):void{
            signalComplete();
        }

    }
}//package org.osmf.elements.audioClasses 
