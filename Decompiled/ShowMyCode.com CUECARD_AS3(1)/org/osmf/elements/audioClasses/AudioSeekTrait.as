﻿package org.osmf.elements.audioClasses {
    import org.osmf.traits.*;

    public class AudioSeekTrait extends SeekTrait {

        private var soundAdapter:SoundAdapter;

        public function AudioSeekTrait(_arg1:TimeTrait, _arg2:SoundAdapter){
            super(_arg1);
            this.soundAdapter = _arg2;
        }
        override protected function seekingChangeStart(_arg1:Boolean, _arg2:Number):void{
            if (_arg1){
                this.soundAdapter.seek(_arg2);
            };
        }
        override protected function seekingChangeEnd(_arg1:Number):void{
            super.seekingChangeEnd(_arg1);
            if (seeking == true){
                setSeeking(false, _arg1);
            };
        }

    }
}//package org.osmf.elements.audioClasses 
