﻿package org.osmf.elements.audioClasses {
    import flash.events.*;
    import org.osmf.media.*;
    import org.osmf.events.*;
    import flash.media.*;

    public class SoundAdapter extends EventDispatcher {

        public static const DOWNLOAD_COMPLETE:String = "downloadComplete";

        private var owner:MediaElement;
        private var _soundTransform:SoundTransform;
        private var sound:Sound;
        private var playing:Boolean = false;
        private var channel:SoundChannel;
        private var lastStartTime:Number = 0;

        public function SoundAdapter(_arg1:MediaElement, _arg2:Sound){
            this.owner = _arg1;
            this.sound = _arg2;
            this._soundTransform = new SoundTransform();
            _arg2.addEventListener(Event.COMPLETE, this.onDownloadComplete, false, 0, true);
            _arg2.addEventListener(ProgressEvent.PROGRESS, this.onProgress, false, 0, true);
            _arg2.addEventListener(IOErrorEvent.IO_ERROR, this.onIOError, false, 0, true);
        }
        public function get currentTime():Number{
            return (((this.channel)!=null) ? (this.channel.position / 1000) : (this.lastStartTime / 1000));
        }
        public function get estimatedDuration():Number{
            return ((this.sound.length / ((1000 * this.sound.bytesLoaded) / this.sound.bytesTotal)));
        }
        public function get soundTransform():SoundTransform{
            return (this._soundTransform);
        }
        public function set soundTransform(_arg1:SoundTransform):void{
            this._soundTransform = _arg1;
            if (this.channel != null){
                this.channel.soundTransform = _arg1;
            };
        }
        public function play(_arg1:Number=-1):Boolean{
            var time:int = _arg1;
            var success:* = false;
            if (this.channel == null){
                try {
                    this.channel = this.sound.play(((time)!=-1) ? time : this.lastStartTime);
                } catch(error:ArgumentError) {
                    channel = null;
                };
                if (this.channel != null){
                    this.playing = true;
                    this.channel.soundTransform = this._soundTransform;
                    this.channel.addEventListener(Event.SOUND_COMPLETE, this.onSoundComplete);
                    success = true;
                } else {
                    this.owner.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.SOUND_PLAY_FAILED)));
                };
            };
            return (success);
        }
        public function pause():void{
            if (this.channel != null){
                this.lastStartTime = this.channel.position;
                this.clearChannel();
                this.playing = false;
            };
        }
        public function stop():void{
            if (this.channel != null){
                this.lastStartTime = 0;
                this.clearChannel();
                this.playing = false;
            };
        }
        public function seek(_arg1:Number):void{
            var _local2:Boolean = this.playing;
            if (this.channel != null){
                this.clearChannel();
            };
            this.play((_arg1 * 1000));
            if (_local2 == false){
                this.pause();
            };
        }
        private function clearChannel():void{
            if (this.channel != null){
                this.channel.removeEventListener(Event.SOUND_COMPLETE, this.onSoundComplete);
                this.channel.stop();
                this.channel = null;
            };
        }
        private function onSoundComplete(_arg1:Event):void{
            this.lastStartTime = this.channel.position;
            this.clearChannel();
            this.playing = false;
            dispatchEvent(new Event(Event.COMPLETE));
        }
        private function onDownloadComplete(_arg1:Event):void{
            dispatchEvent(new Event(DOWNLOAD_COMPLETE));
        }
        private function onProgress(_arg1:ProgressEvent):void{
            dispatchEvent(_arg1.clone());
        }
        private function onIOError(_arg1:IOErrorEvent):void{
            this.owner.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.IO_ERROR)));
        }

    }
}//package org.osmf.elements.audioClasses 
