﻿package org.osmf.elements.audioClasses {
    import flash.events.*;
    import org.osmf.traits.*;

    public class AudioPlayTrait extends PlayTrait {

        private var lastPlayFailed:Boolean = false;
        private var soundAdapter:SoundAdapter;

        public function AudioPlayTrait(_arg1:SoundAdapter){
            this.soundAdapter = _arg1;
            _arg1.addEventListener(Event.COMPLETE, this.onPlaybackComplete, false, 1, true);
        }
        override protected function playStateChangeStart(_arg1:String):void{
            if (_arg1 == PlayState.PLAYING){
                this.lastPlayFailed = !(this.soundAdapter.play());
            } else {
                if (_arg1 == PlayState.PAUSED){
                    this.soundAdapter.pause();
                } else {
                    if (_arg1 == PlayState.STOPPED){
                        this.soundAdapter.stop();
                    };
                };
            };
        }
        override protected function playStateChangeEnd():void{
            if (this.lastPlayFailed){
                stop();
                this.lastPlayFailed = false;
            } else {
                super.playStateChangeEnd();
            };
        }
        private function onPlaybackComplete(_arg1:Event):void{
            stop();
        }

    }
}//package org.osmf.elements.audioClasses 
