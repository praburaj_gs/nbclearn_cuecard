﻿package org.osmf.events {
    import flash.events.*;

    public class DRMEvent extends Event {

        public static const DRM_STATE_CHANGE:String = "drmStateChange";

        private var _drmState:String;
        private var _startDate:Date;
        private var _endDate:Date;
        private var _period:Number;
        private var _serverURL:String;
        private var _token:Object;
        private var _mediaError:MediaError;

        public function DRMEvent(_arg1:String, _arg2:String, _arg3:Boolean=false, _arg4:Boolean=false, _arg5:Date=null, _arg6:Date=null, _arg7:Number=0, _arg8:String=null, _arg9:Object=null, _arg10:MediaError=null){
            super(_arg1, _arg3, _arg4);
            this._drmState = _arg2;
            this._token = _arg9;
            this._mediaError = _arg10;
            this._startDate = _arg5;
            this._endDate = _arg6;
            this._period = _arg7;
            this._serverURL = _arg8;
        }
        public function get token():Object{
            return (this._token);
        }
        public function get mediaError():MediaError{
            return (this._mediaError);
        }
        public function get startDate():Date{
            return (this._startDate);
        }
        public function get endDate():Date{
            return (this._endDate);
        }
        public function get period():Number{
            return (this._period);
        }
        public function get drmState():String{
            return (this._drmState);
        }
        public function get serverURL():String{
            return (this._serverURL);
        }
        override public function clone():Event{
            return (new DRMEvent(type, this._drmState, bubbles, cancelable, this._startDate, this._endDate, this._period, this._serverURL, this._token, this._mediaError));
        }

    }
}//package org.osmf.events 
