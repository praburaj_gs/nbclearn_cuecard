﻿package org.osmf.events {
    import flash.events.*;

    public class TimeEvent extends Event {

        public static const CURRENT_TIME_CHANGE:String = "currentTimeChange";
        public static const DURATION_CHANGE:String = "durationChange";
        public static const COMPLETE:String = "complete";

        private var _time:Number;

        public function TimeEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Number=NaN){
            super(_arg1, _arg2, _arg3);
            this._time = _arg4;
        }
        public function get time():Number{
            return (this._time);
        }
        override public function clone():Event{
            return (new TimeEvent(type, bubbles, cancelable, this.time));
        }

    }
}//package org.osmf.events 
