﻿package org.osmf.events {
    import flash.events.*;

    public class AlternativeAudioEvent extends Event {

        public static const AUDIO_SWITCHING_CHANGE:String = "audioSwitchingChange";
        public static const NUM_ALTERNATIVE_AUDIO_STREAMS_CHANGE:String = "numAlternativeAudioStreamsChange";

        private var _switching:Boolean;

        public function AlternativeAudioEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Boolean=false){
            super(_arg1, _arg2, _arg3);
            this._switching = _arg4;
        }
        public function get switching():Boolean{
            return (this._switching);
        }
        override public function clone():Event{
            return (new AlternativeAudioEvent(type, bubbles, cancelable, this.switching));
        }

    }
}//package org.osmf.events 
