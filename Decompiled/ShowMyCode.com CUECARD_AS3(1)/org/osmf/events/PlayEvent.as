﻿package org.osmf.events {
    import flash.events.*;

    public class PlayEvent extends Event {

        public static const CAN_PAUSE_CHANGE:String = "canPauseChange";
        public static const PLAY_STATE_CHANGE:String = "playStateChange";
        public static const LIVE_STALL:String = "liveStall";
        public static const LIVE_RESUME:String = "liveResume";

        private var _playState:String;
        private var _canPause:Boolean;

        public function PlayEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:String=null, _arg5:Boolean=false){
            super(_arg1, _arg2, _arg3);
            this._playState = _arg4;
            this._canPause = _arg5;
        }
        override public function clone():Event{
            return (new PlayEvent(type, bubbles, cancelable, this.playState, this.canPause));
        }
        public function get playState():String{
            return (this._playState);
        }
        public function get canPause():Boolean{
            return (this._canPause);
        }

    }
}//package org.osmf.events 
