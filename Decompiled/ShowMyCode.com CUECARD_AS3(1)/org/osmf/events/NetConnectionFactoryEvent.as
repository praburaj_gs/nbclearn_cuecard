﻿package org.osmf.events {
    import flash.events.*;
    import org.osmf.media.*;
    import flash.net.*;

    public class NetConnectionFactoryEvent extends Event {

        public static const CREATION_COMPLETE:String = "creationComplete";
        public static const CREATION_ERROR:String = "creationError";

        private var _netConnection:NetConnection;
        private var _resource:URLResource;
        private var _mediaError:MediaError;

        public function NetConnectionFactoryEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:NetConnection=null, _arg5:URLResource=null, _arg6:MediaError=null){
            super(_arg1, _arg2, _arg3);
            this._netConnection = _arg4;
            this._resource = _arg5;
            this._mediaError = _arg6;
        }
        public function get netConnection():NetConnection{
            return (this._netConnection);
        }
        public function get resource():URLResource{
            return (this._resource);
        }
        public function get mediaError():MediaError{
            return (this._mediaError);
        }
        override public function clone():Event{
            return (new NetConnectionFactoryEvent(type, bubbles, cancelable, this._netConnection, this._resource, this._mediaError));
        }

    }
}//package org.osmf.events 
