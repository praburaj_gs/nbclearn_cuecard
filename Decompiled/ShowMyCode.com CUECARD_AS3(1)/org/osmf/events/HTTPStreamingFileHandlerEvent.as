﻿package org.osmf.events {
    import flash.events.*;
    import org.osmf.net.httpstreaming.f4f.*;
    import org.osmf.net.httpstreaming.flv.*;

    public class HTTPStreamingFileHandlerEvent extends HTTPStreamingEvent {

        public static const NOTIFY_BOOTSTRAP_BOX:String = "notifyBootstrapBox";

        private var _abst:AdobeBootstrapBox;

        public function HTTPStreamingFileHandlerEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Number=0, _arg5:FLVTagScriptDataObject=null, _arg6:String="normal", _arg7:AdobeBootstrapBox=null){
            super(_arg1, _arg2, _arg3, _arg4, _arg5, _arg6);
            this._abst = _arg7;
        }
        public function get bootstrapBox():AdobeBootstrapBox{
            return (this._abst);
        }
        override public function clone():Event{
            return (new HTTPStreamingFileHandlerEvent(type, bubbles, cancelable, fragmentDuration, scriptDataObject, scriptDataMode, this.bootstrapBox));
        }

    }
}//package org.osmf.events 
