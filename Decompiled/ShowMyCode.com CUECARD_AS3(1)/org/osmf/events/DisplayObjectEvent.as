﻿package org.osmf.events {
    import flash.events.*;
    import flash.display.*;

    public class DisplayObjectEvent extends Event {

        public static const DISPLAY_OBJECT_CHANGE:String = "displayObjectChange";
        public static const MEDIA_SIZE_CHANGE:String = "mediaSizeChange";

        private var _oldDisplayObject:DisplayObject;
        private var _newDisplayObject:DisplayObject;
        private var _oldWidth:Number;
        private var _oldHeight:Number;
        private var _newWidth:Number;
        private var _newHeight:Number;

        public function DisplayObjectEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:DisplayObject=null, _arg5:DisplayObject=null, _arg6:Number=NaN, _arg7:Number=NaN, _arg8:Number=NaN, _arg9:Number=NaN){
            super(_arg1, _arg2, _arg3);
            this._oldDisplayObject = _arg4;
            this._newDisplayObject = _arg5;
            this._oldWidth = _arg6;
            this._oldHeight = _arg7;
            this._newWidth = _arg8;
            this._newHeight = _arg9;
        }
        public function get oldDisplayObject():DisplayObject{
            return (this._oldDisplayObject);
        }
        public function get newDisplayObject():DisplayObject{
            return (this._newDisplayObject);
        }
        public function get oldWidth():Number{
            return (this._oldWidth);
        }
        public function get oldHeight():Number{
            return (this._oldHeight);
        }
        public function get newWidth():Number{
            return (this._newWidth);
        }
        public function get newHeight():Number{
            return (this._newHeight);
        }
        override public function clone():Event{
            return (new DisplayObjectEvent(type, bubbles, cancelable, this._oldDisplayObject, this._newDisplayObject, this._oldWidth, this._oldHeight, this._newWidth, this._newHeight));
        }

    }
}//package org.osmf.events 
