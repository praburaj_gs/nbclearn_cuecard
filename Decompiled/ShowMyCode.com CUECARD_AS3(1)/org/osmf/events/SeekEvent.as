﻿package org.osmf.events {
    import flash.events.*;

    public class SeekEvent extends Event {

        public static const SEEKING_CHANGE:String = "seekingChange";

        private var _seeking:Boolean = false;
        private var _time:Number;

        public function SeekEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Boolean=false, _arg5:Number=NaN){
            super(_arg1, _arg2, _arg3);
            this._seeking = _arg4;
            this._time = _arg5;
        }
        public function get seeking():Boolean{
            return (this._seeking);
        }
        public function get time():Number{
            return (this._time);
        }
        override public function clone():Event{
            return (new SeekEvent(type, bubbles, cancelable, this._seeking, this._time));
        }

    }
}//package org.osmf.events 
