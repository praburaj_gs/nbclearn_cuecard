﻿package org.osmf.events {
    import flash.events.*;

    public class AudioEvent extends Event {

        public static const VOLUME_CHANGE:String = "volumeChange";
        public static const MUTED_CHANGE:String = "mutedChange";
        public static const PAN_CHANGE:String = "panChange";

        private var _muted:Boolean;
        private var _volume:Number;
        private var _pan:Number;

        public function AudioEvent(_arg1:String, _arg2:Boolean, _arg3:Boolean, _arg4:Boolean=false, _arg5:Number=NaN, _arg6:Number=NaN){
            super(_arg1, _arg2, _arg3);
            this._muted = _arg4;
            this._volume = _arg5;
            this._pan = _arg6;
        }
        public function get muted():Boolean{
            return (this._muted);
        }
        public function get volume():Number{
            return (this._volume);
        }
        public function get pan():Number{
            return (this._pan);
        }
        override public function clone():Event{
            return (new AudioEvent(type, bubbles, cancelable, this._muted, this._volume, this._pan));
        }

    }
}//package org.osmf.events 
