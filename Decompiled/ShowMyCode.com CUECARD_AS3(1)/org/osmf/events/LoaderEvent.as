﻿package org.osmf.events {
    import flash.events.*;
    import org.osmf.traits.*;

    public class LoaderEvent extends Event {

        public static const LOAD_STATE_CHANGE:String = "loadStateChange";

        private var _loader:LoaderBase;
        private var _loadTrait:LoadTrait;
        private var _oldState:String;
        private var _newState:String;

        public function LoaderEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:LoaderBase=null, _arg5:LoadTrait=null, _arg6:String=null, _arg7:String=null){
            super(_arg1, _arg2, _arg3);
            this._loader = _arg4;
            this._loadTrait = _arg5;
            this._oldState = _arg6;
            this._newState = _arg7;
        }
        public function get loader():LoaderBase{
            return (this._loader);
        }
        public function get loadTrait():LoadTrait{
            return (this._loadTrait);
        }
        public function get oldState():String{
            return (this._oldState);
        }
        public function get newState():String{
            return (this._newState);
        }
        override public function clone():Event{
            return (new LoaderEvent(type, bubbles, cancelable, this.loader, this.loadTrait, this.oldState, this.newState));
        }

    }
}//package org.osmf.events 
