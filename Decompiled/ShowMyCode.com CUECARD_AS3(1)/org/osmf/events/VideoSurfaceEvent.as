﻿package org.osmf.events {
    import flash.events.*;

    public class VideoSurfaceEvent extends Event {

        public static const RENDER_CHANGE:String = "renderSwitch";

        private var _usesStageVideo:Boolean;

        public function VideoSurfaceEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Boolean=false){
            super(_arg1, _arg2, _arg3);
            this._usesStageVideo = _arg4;
        }
        public function get usesStageVideo():Boolean{
            return (this._usesStageVideo);
        }
        override public function clone():Event{
            return (new VideoSurfaceEvent(type, bubbles, cancelable, this._usesStageVideo));
        }

    }
}//package org.osmf.events 
