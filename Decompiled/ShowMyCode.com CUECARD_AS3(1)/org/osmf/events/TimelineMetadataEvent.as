﻿package org.osmf.events {
    import flash.events.*;
    import org.osmf.metadata.*;

    public class TimelineMetadataEvent extends MetadataEvent {

        public static const MARKER_TIME_REACHED:String = "markerTimeReached";
        public static const MARKER_DURATION_REACHED:String = "markerDurationReached";
        public static const MARKER_ADD:String = "markerAdd";
        public static const MARKER_REMOVE:String = "markerRemove";

        private var _marker:TimelineMarker;

        public function TimelineMetadataEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:TimelineMarker=null){
            super(_arg1, _arg2, _arg3, ("" + _arg4.time), _arg4);
            this._marker = _arg4;
        }
        public function get marker():TimelineMarker{
            return (this._marker);
        }
        override public function clone():Event{
            return (new TimelineMetadataEvent(type, bubbles, cancelable, this._marker));
        }

    }
}//package org.osmf.events 
