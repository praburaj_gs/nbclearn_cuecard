﻿package org.osmf.events {
    import flash.events.*;
    import org.osmf.media.*;

    public class PluginManagerEvent extends Event {

        public static const PLUGIN_LOAD:String = "pluginLoad";
        public static const PLUGIN_LOAD_ERROR:String = "pluginLoadError";

        private var _resource:MediaResourceBase;

        public function PluginManagerEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:MediaResourceBase=null){
            super(_arg1, _arg2, _arg3);
            this._resource = _arg4;
        }
        public function get resource():MediaResourceBase{
            return (this._resource);
        }
        override public function clone():Event{
            return (new PluginManagerEvent(type, bubbles, cancelable, this._resource));
        }

    }
}//package org.osmf.events 
