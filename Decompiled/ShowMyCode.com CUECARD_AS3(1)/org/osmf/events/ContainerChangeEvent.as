﻿package org.osmf.events {
    import flash.events.*;
    import org.osmf.containers.*;

    public class ContainerChangeEvent extends Event {

        public static const CONTAINER_CHANGE:String = "containerChange";

        private var _oldContainer:IMediaContainer;
        private var _newContainer:IMediaContainer;

        public function ContainerChangeEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:IMediaContainer=null, _arg5:IMediaContainer=null){
            super(_arg1, _arg2, _arg3);
            this._oldContainer = _arg4;
            this._newContainer = _arg5;
        }
        public function get oldContainer():IMediaContainer{
            return (this._oldContainer);
        }
        public function get newContainer():IMediaContainer{
            return (this._newContainer);
        }
        override public function clone():Event{
            return (new ContainerChangeEvent(type, bubbles, cancelable, this._oldContainer, this._newContainer));
        }

    }
}//package org.osmf.events 
