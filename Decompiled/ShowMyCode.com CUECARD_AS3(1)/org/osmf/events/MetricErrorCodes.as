﻿package org.osmf.events {
    import org.osmf.utils.*;

    public final class MetricErrorCodes {

        public static const INVALID_METRIC_TYPE:int = 1;
        private static const errorMap:Array = [{
            errorID:INVALID_METRIC_TYPE,
            message:OSMFStrings.METRIC_NOT_FOUND
        }];

        static function getMessageForErrorID(_arg1:int):String{
            var _local2 = "";
            var _local3:int;
            while (_local3 < errorMap.length) {
                if (errorMap[_local3].errorID == _arg1){
                    _local2 = OSMFStrings.getString(errorMap[_local3].message);
                    break;
                };
                _local3++;
            };
            return (_local2);
        }

    }
}//package org.osmf.events 
