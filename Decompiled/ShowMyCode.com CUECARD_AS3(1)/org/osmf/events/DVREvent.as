﻿package org.osmf.events {
    import flash.events.*;

    public class DVREvent extends Event {

        public static const IS_RECORDING_CHANGE:String = "isRecordingChange";

        public function DVREvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false){
            super(_arg1, _arg2, _arg3);
        }
        override public function clone():Event{
            return (new DVREvent(type, bubbles, cancelable));
        }

    }
}//package org.osmf.events 
