﻿package org.osmf.events {
    import flash.events.*;
    import flash.net.*;
    import org.osmf.net.httpstreaming.flv.*;

    public class HTTPStreamingIndexHandlerEvent extends HTTPStreamingEvent {

        public static const INDEX_READY:String = "indexReady";
        public static const RATES_READY:String = "ratesReady";
        public static const REQUEST_LOAD_INDEX:String = "requestLoadIndex";

        private var _streamNames:Array;
        private var _rates:Array;
        private var _request:URLRequest;
        private var _requestContext:Object;
        private var _binaryData:Boolean;
        private var _live:Boolean;
        private var _offset:Number;

        public function HTTPStreamingIndexHandlerEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Boolean=false, _arg5:Number=NaN, _arg6:Array=null, _arg7:Array=null, _arg8:URLRequest=null, _arg9:Object=null, _arg10:Boolean=true, _arg11:Number=0, _arg12:FLVTagScriptDataObject=null, _arg13:String="normal"){
            super(_arg1, _arg2, _arg3, _arg11, _arg12, _arg13);
            this._live = _arg4;
            this._offset = _arg5;
            this._streamNames = _arg6;
            this._rates = _arg7;
            this._request = _arg8;
            this._requestContext = _arg9;
            this._binaryData = _arg10;
        }
        public function get live():Boolean{
            return (this._live);
        }
        public function get offset():Number{
            return (this._offset);
        }
        public function get streamNames():Array{
            return (this._streamNames);
        }
        public function get rates():Array{
            return (this._rates);
        }
        public function get request():URLRequest{
            return (this._request);
        }
        public function get requestContext():Object{
            return (this._requestContext);
        }
        public function get binaryData():Boolean{
            return (this._binaryData);
        }
        override public function clone():Event{
            return (new HTTPStreamingIndexHandlerEvent(type, bubbles, cancelable, this.live, this.offset, this.streamNames, this.rates, this.request, this.requestContext, this.binaryData, fragmentDuration, scriptDataObject, scriptDataMode));
        }

    }
}//package org.osmf.events 
