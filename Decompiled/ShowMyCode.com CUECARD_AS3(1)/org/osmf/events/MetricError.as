﻿package org.osmf.events {

    public class MetricError extends Error {

        private var _detail:String;

        public function MetricError(_arg1:int, _arg2:String=null){
            super(this.getMessageForErrorID(_arg1), _arg1);
            this._detail = _arg2;
        }
        public function get detail():String{
            return (this._detail);
        }
        protected function getMessageForErrorID(_arg1:int):String{
            return (MetricErrorCodes.getMessageForErrorID(_arg1));
        }

    }
}//package org.osmf.events 
