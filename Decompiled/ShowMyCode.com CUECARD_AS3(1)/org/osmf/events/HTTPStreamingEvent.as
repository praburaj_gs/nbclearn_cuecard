﻿package org.osmf.events {
    import flash.events.*;
    import org.osmf.net.httpstreaming.*;
    import org.osmf.net.httpstreaming.flv.*;

    public class HTTPStreamingEvent extends Event {

        public static const TRANSITION:String = "transition";
        public static const TRANSITION_COMPLETE:String = "transitionComplete";
        public static const BEGIN_FRAGMENT:String = "beginFragment";
        public static const END_FRAGMENT:String = "endFragment";
        public static const DOWNLOAD_COMPLETE:String = "downloadComplete";
        public static const DOWNLOAD_ERROR:String = "downloadError";
        public static const DOWNLOAD_SKIP:String = "downloadSkip";
        public static const DOWNLOAD_CONTINUE:String = "downloadContinue";
        public static const DOWNLOAD_PROGRESS:String = "downloadProgress";
        public static const FRAGMENT_DURATION:String = "fragmentDuration";
        public static const FILE_ERROR:String = "fileError";
        public static const INDEX_ERROR:String = "indexError";
        public static const SCRIPT_DATA:String = "scriptData";
        public static const ACTION_NEEDED:String = "actionNeeded";
        public static const RUN_ALGORITHM:String = "runAlgorithm";

        private var _fragmentDuration:Number;
        private var _scriptDataObject:FLVTagScriptDataObject;
        private var _scriptDataMode:String;
        private var _url:String;
        private var _bytesDownloaded:uint;
        private var _reason:String;
        private var _downloader:HTTPStreamDownloader;

        public function HTTPStreamingEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Number=0, _arg5:FLVTagScriptDataObject=null, _arg6:String="normal", _arg7:String=null, _arg8:uint=0, _arg9:String="normal", _arg10:HTTPStreamDownloader=null){
            super(_arg1, _arg2, _arg3);
            this._fragmentDuration = _arg4;
            this._scriptDataObject = _arg5;
            this._scriptDataMode = _arg6;
            this._url = _arg7;
            this._bytesDownloaded = _arg8;
            this._reason = _arg9;
            this._downloader = _arg10;
        }
        public function get fragmentDuration():Number{
            return (this._fragmentDuration);
        }
        public function get scriptDataObject():FLVTagScriptDataObject{
            return (this._scriptDataObject);
        }
        public function get scriptDataMode():String{
            return (this._scriptDataMode);
        }
        public function get url():String{
            return (this._url);
        }
        public function get bytesDownloaded():uint{
            return (this._bytesDownloaded);
        }
        public function get reason():String{
            return (this._reason);
        }
        public function get downloader():HTTPStreamDownloader{
            return (this._downloader);
        }
        override public function clone():Event{
            return (new HTTPStreamingEvent(type, bubbles, cancelable, this.fragmentDuration, this.scriptDataObject, this.scriptDataMode, this._url, this._bytesDownloaded, this._reason, this._downloader));
        }

    }
}//package org.osmf.events 
