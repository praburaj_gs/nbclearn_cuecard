﻿package org.osmf.events {

    public class HTTPStreamingEventReason {

        public static const NORMAL:String = "normal";
        public static const BEST_EFFORT:String = "bestEffort";
        public static const TIMEOUT:String = "timeout";

    }
}//package org.osmf.events 
