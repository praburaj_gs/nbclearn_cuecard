﻿package org.osmf.events {
    import flash.events.*;

    public class MediaErrorEvent extends Event {

        public static const MEDIA_ERROR:String = "mediaError";

        private var _error:MediaError;

        public function MediaErrorEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:MediaError=null){
            super(_arg1, _arg2, _arg3);
            this._error = _arg4;
        }
        override public function clone():Event{
            return (new MediaErrorEvent(type, bubbles, cancelable, this.error));
        }
        public function get error():MediaError{
            return (this._error);
        }

    }
}//package org.osmf.events 
