﻿package org.osmf.events {
    import flash.events.*;

    public class BufferEvent extends Event {

        public static const BUFFERING_CHANGE:String = "bufferingChange";
        public static const BUFFER_TIME_CHANGE:String = "bufferTimeChange";

        private var _buffering:Boolean;
        private var _bufferTime:Number;

        public function BufferEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Boolean=false, _arg5:Number=NaN){
            super(_arg1, _arg2, _arg3);
            this._buffering = _arg4;
            this._bufferTime = _arg5;
        }
        public function get buffering():Boolean{
            return (this._buffering);
        }
        public function get bufferTime():Number{
            return (this._bufferTime);
        }
        override public function clone():Event{
            return (new BufferEvent(type, bubbles, cancelable, this._buffering, this._bufferTime));
        }

    }
}//package org.osmf.events 
