﻿package org.osmf.events {
    import flash.events.*;
    import org.osmf.media.*;

    public class MediaFactoryEvent extends Event {

        public static const PLUGIN_LOAD:String = "pluginLoad";
        public static const PLUGIN_LOAD_ERROR:String = "pluginLoadError";
        public static const MEDIA_ELEMENT_CREATE:String = "mediaElementCreate";

        private var _resource:MediaResourceBase;
        private var _mediaElement:MediaElement;

        public function MediaFactoryEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:MediaResourceBase=null, _arg5:MediaElement=null){
            super(_arg1, _arg2, _arg3);
            this._resource = _arg4;
            this._mediaElement = _arg5;
        }
        public function get resource():MediaResourceBase{
            return (this._resource);
        }
        public function get mediaElement():MediaElement{
            return (this._mediaElement);
        }
        override public function clone():Event{
            return (new MediaFactoryEvent(type, bubbles, cancelable, this._resource, this._mediaElement));
        }

    }
}//package org.osmf.events 
