﻿package org.osmf.events {
    import flash.events.*;

    public class MediaPlayerStateChangeEvent extends Event {

        public static const MEDIA_PLAYER_STATE_CHANGE:String = "mediaPlayerStateChange";

        private var _state:String;

        public function MediaPlayerStateChangeEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:String=null){
            super(_arg1, _arg2, _arg3);
            this._state = _arg4;
        }
        public function get state():String{
            return (this._state);
        }
        override public function clone():Event{
            return (new MediaPlayerStateChangeEvent(type, bubbles, cancelable, this._state));
        }

    }
}//package org.osmf.events 
