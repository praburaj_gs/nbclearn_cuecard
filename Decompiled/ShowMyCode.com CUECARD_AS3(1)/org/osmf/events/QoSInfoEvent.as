﻿package org.osmf.events {
    import flash.events.*;
    import org.osmf.net.qos.*;

    public class QoSInfoEvent extends Event {

        public static const QOS_UPDATE:String = "qosUpdate";

        private var _qosInfo:QoSInfo = null;

        public function QoSInfoEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:QoSInfo=null){
            super(_arg1, _arg2, _arg3);
            this._qosInfo = _arg4;
        }
        public function get qosInfo():QoSInfo{
            return (this._qosInfo);
        }
        override public function clone():Event{
            return (new QoSInfoEvent(type, bubbles, cancelable, this._qosInfo));
        }

    }
}//package org.osmf.events 
