﻿package org.osmf.events {
    import flash.events.*;

    public class DynamicStreamEvent extends Event {

        public static const SWITCHING_CHANGE:String = "switchingChange";
        public static const NUM_DYNAMIC_STREAMS_CHANGE:String = "numDynamicStreamsChange";
        public static const AUTO_SWITCH_CHANGE:String = "autoSwitchChange";

        private var _switching:Boolean;
        private var _autoSwitch:Boolean;

        public function DynamicStreamEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Boolean=false, _arg5:Boolean=false){
            super(_arg1, _arg2, _arg3);
            this._switching = _arg4;
            this._autoSwitch = _arg5;
        }
        public function get switching():Boolean{
            return (this._switching);
        }
        public function get autoSwitch():Boolean{
            return (this._autoSwitch);
        }
        override public function clone():Event{
            return (new DynamicStreamEvent(type, bubbles, cancelable, this._switching, this._autoSwitch));
        }

    }
}//package org.osmf.events 
