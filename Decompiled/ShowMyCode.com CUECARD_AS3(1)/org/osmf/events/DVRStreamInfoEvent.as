﻿package org.osmf.events {
    import flash.events.*;

    public class DVRStreamInfoEvent extends Event {

        public static const DVRSTREAMINFO:String = "DVRStreamInfo";

        private var _info:Object;

        public function DVRStreamInfoEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Object=null){
            super(_arg1, _arg2, _arg3);
            this._info = _arg4;
        }
        override public function clone():Event{
            return (new DVRStreamInfoEvent(type, bubbles, cancelable, this.info));
        }
        public function get info():Object{
            return (this._info);
        }

    }
}//package org.osmf.events 
