﻿package org.osmf.events {
    import flash.events.*;
    import org.osmf.metadata.*;

    public class MediaElementEvent extends Event {

        public static const TRAIT_ADD:String = "traitAdd";
        public static const TRAIT_REMOVE:String = "traitRemove";
        public static const METADATA_ADD:String = "metadataAdd";
        public static const METADATA_REMOVE:String = "metadataRemove";

        private var _traitType:String;
        private var _namespaceURL:String;
        private var _metadata:Metadata;

        public function MediaElementEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:String=null, _arg5:String=null, _arg6:Metadata=null){
            super(_arg1, _arg2, _arg3);
            this._traitType = _arg4;
            this._namespaceURL = _arg5;
            this._metadata = _arg6;
        }
        override public function clone():Event{
            return (new MediaElementEvent(type, bubbles, cancelable, this.traitType, this.namespaceURL, this.metadata));
        }
        public function get traitType():String{
            return (this._traitType);
        }
        public function get namespaceURL():String{
            return (this._namespaceURL);
        }
        public function get metadata():Metadata{
            return (this._metadata);
        }

    }
}//package org.osmf.events 
