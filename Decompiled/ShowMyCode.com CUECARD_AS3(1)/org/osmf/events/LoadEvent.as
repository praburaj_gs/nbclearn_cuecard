﻿package org.osmf.events {
    import flash.events.*;

    public class LoadEvent extends Event {

        public static const LOAD_STATE_CHANGE:String = "loadStateChange";
        public static const BYTES_LOADED_CHANGE:String = "bytesLoadedChange";
        public static const BYTES_TOTAL_CHANGE:String = "bytesTotalChange";

        private var _loadState:String;
        private var _bytes:Number;

        public function LoadEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:String=null, _arg5:Number=NaN){
            super(_arg1, _arg2, _arg3);
            this._loadState = _arg4;
            this._bytes = _arg5;
        }
        override public function clone():Event{
            return (new LoadEvent(type, bubbles, cancelable, this.loadState, this.bytes));
        }
        public function get loadState():String{
            return (this._loadState);
        }
        public function get bytes():Number{
            return (this._bytes);
        }

    }
}//package org.osmf.events 
