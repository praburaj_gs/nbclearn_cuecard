﻿package org.osmf.events {
    import flash.events.*;

    public class MediaElementChangeEvent extends Event {

        public static const MEDIA_ELEMENT_CHANGE:String = "mediaElementChange";

        public function MediaElementChangeEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false){
            super(_arg1, _arg2, _arg3);
        }
        override public function clone():Event{
            return (new MediaElementChangeEvent(type, bubbles, cancelable));
        }

    }
}//package org.osmf.events 
