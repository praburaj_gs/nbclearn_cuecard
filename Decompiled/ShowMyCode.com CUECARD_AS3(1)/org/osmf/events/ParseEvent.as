﻿package org.osmf.events {
    import flash.events.*;

    public class ParseEvent extends Event {

        public static const PARSE_COMPLETE:String = "parseComplete";
        public static const PARSE_ERROR:String = "parseError";

        public var data:Object;

        public function ParseEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Object=null){
            this.data = _arg4;
            super(_arg1, _arg2, _arg3);
        }
        override public function clone():Event{
            return (new ParseEvent(this.type, this.bubbles, this.cancelable, this.data));
        }

    }
}//package org.osmf.events 
