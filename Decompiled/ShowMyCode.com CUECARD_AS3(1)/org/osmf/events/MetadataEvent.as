﻿package org.osmf.events {
    import flash.events.*;

    public class MetadataEvent extends Event {

        public static const VALUE_ADD:String = "valueAdd";
        public static const VALUE_REMOVE:String = "valueRemove";
        public static const VALUE_CHANGE:String = "valueChange";

        private var _key:String;
        private var _value;
        private var _oldValue;

        public function MetadataEvent(_arg1:String, _arg2:Boolean=false, _arg3:Boolean=false, _arg4:String=null, _arg5=null, _arg6=null){
            super(_arg1, _arg2, _arg3);
            this._key = _arg4;
            this._value = _arg5;
            this._oldValue = _arg6;
        }
        public function get key():String{
            return (this._key);
        }
        public function get value(){
            return (this._value);
        }
        public function get oldValue(){
            return (this._oldValue);
        }
        override public function clone():Event{
            return (new MetadataEvent(type, bubbles, cancelable, this._key, this._value, this._oldValue));
        }

    }
}//package org.osmf.events 
