﻿package org.osmf.utils {
    import flash.utils.*;

    public class Base64Decoder {

        private static const ESCAPE_CHAR_CODE:Number = 61;
        private static const inverse:Array = [64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 62, 64, 64, 64, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64, 64, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64, 64, 64, 64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64];

        private var count:int = 0;
        private var data:ByteArray;
        private var filled:int = 0;
        private var work:Array;

        public function Base64Decoder(){
            this.work = [0, 0, 0, 0];
            super();
            this.data = new ByteArray();
        }
        private static function copyByteArray(_arg1:ByteArray, _arg2:ByteArray, _arg3:uint=0):void{
            var _local4:int = _arg1.position;
            _arg1.position = 0;
            _arg2.position = 0;
            var _local5:uint;
            while ((((_arg1.bytesAvailable > 0)) && ((_local5 < _arg3)))) {
                _arg2.writeByte(_arg1.readByte());
                _local5++;
            };
            _arg1.position = _local4;
            _arg2.position = 0;
        }

        public function decode(_arg1:String):void{
            var _local3:Number;
            var _local2:uint;
            for (;_local2 < _arg1.length;_local2++) {
                _local3 = _arg1.charCodeAt(_local2);
                if (_local3 == ESCAPE_CHAR_CODE){
                    var _local4 = this.count++;
                    this.work[_local4] = -1;
                } else {
                    if (inverse[_local3] != 64){
                        _local4 = this.count++;
                        this.work[_local4] = inverse[_local3];
                    } else {
                        continue;
                    };
                };
                if (this.count == 4){
                    this.count = 0;
                    this.data.writeByte(((this.work[0] << 2) | ((this.work[1] & 0xFF) >> 4)));
                    this.filled++;
                    if (this.work[2] == -1){
                        break;
                    };
                    this.data.writeByte(((this.work[1] << 4) | ((this.work[2] & 0xFF) >> 2)));
                    this.filled++;
                    if (this.work[3] == -1){
                        break;
                    };
                    this.data.writeByte(((this.work[2] << 6) | this.work[3]));
                    this.filled++;
                };
            };
        }
        public function drain():ByteArray{
            var _local1:ByteArray = new ByteArray();
            copyByteArray(this.data, _local1, this.filled);
            this.filled = 0;
            return (_local1);
        }

    }
}//package org.osmf.utils 
