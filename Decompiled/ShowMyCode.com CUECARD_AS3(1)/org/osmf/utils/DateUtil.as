﻿package org.osmf.utils {

    public class DateUtil {

        public static function parseW3CDTF(_arg1:String):Date{
            var finalDate:* = null;
            var dateStr:* = null;
            var timeStr:* = null;
            var dateArr:* = null;
            var year:* = NaN;
            var month:* = NaN;
            var date:* = NaN;
            var multiplier:* = NaN;
            var offsetHours:* = NaN;
            var offsetMinutes:* = NaN;
            var offsetStr:* = null;
            var timeArr:* = null;
            var hour:* = NaN;
            var minutes:* = NaN;
            var secondsArr:* = null;
            var seconds:* = NaN;
            var milliseconds:* = NaN;
            var utc:* = NaN;
            var offset:* = NaN;
            var eStr:* = null;
            var str:* = _arg1;
            try {
                dateStr = str.substring(0, str.indexOf("T"));
                timeStr = str.substring((str.indexOf("T") + 1), str.length);
                dateArr = dateStr.split("-");
                year = Number(dateArr.shift());
                month = Number(dateArr.shift());
                date = Number(dateArr.shift());
                if (timeStr.indexOf("Z") != -1){
                    multiplier = 1;
                    offsetHours = 0;
                    offsetMinutes = 0;
                    timeStr = timeStr.replace("Z", "");
                } else {
                    if (timeStr.indexOf("+") != -1){
                        multiplier = 1;
                        offsetStr = timeStr.substring((timeStr.indexOf("+") + 1), timeStr.length);
                        offsetHours = Number(offsetStr.substring(0, offsetStr.indexOf(":")));
                        offsetMinutes = Number(offsetStr.substring((offsetStr.indexOf(":") + 1), offsetStr.length));
                        timeStr = timeStr.substring(0, timeStr.indexOf("+"));
                    } else {
                        multiplier = -1;
                        offsetStr = timeStr.substring((timeStr.indexOf("-") + 1), timeStr.length);
                        offsetHours = Number(offsetStr.substring(0, offsetStr.indexOf(":")));
                        offsetMinutes = Number(offsetStr.substring((offsetStr.indexOf(":") + 1), offsetStr.length));
                        timeStr = timeStr.substring(0, timeStr.indexOf("-"));
                    };
                };
                timeArr = timeStr.split(":");
                hour = Number(timeArr.shift());
                minutes = Number(timeArr.shift());
                secondsArr = ((timeArr.length)>0) ? String(timeArr.shift()).split(".") : null;
                seconds = ((((!((secondsArr == null))) && ((secondsArr.length > 0)))) ? Number(secondsArr.shift()) : 0);
                milliseconds = ((((!((secondsArr == null))) && ((secondsArr.length > 0)))) ? Number(secondsArr.shift()) : 0);
                utc = Date.UTC(year, (month - 1), date, hour, minutes, seconds, milliseconds);
                offset = (((offsetHours * 3600000) + (offsetMinutes * 60000)) * multiplier);
                finalDate = new Date((utc - offset));
                if (finalDate.toString() == "Invalid Date"){
                    throw (new Error("This date does not conform to W3CDTF."));
                };
            } catch(e:Error) {
                eStr = (("Unable to parse the string [" + str) + "] into a date. ");
                eStr = (eStr + ("The internal error was: " + e.toString()));
                throw (new Error(eStr));
            };
            return (finalDate);
        }

    }
}//package org.osmf.utils 
