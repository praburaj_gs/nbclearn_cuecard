﻿package org.osmf.utils {

    public class URL {

        private var _rawUrl:String;
        private var _protocol:String;
        private var _userInfo:String;
        private var _host:String;
        private var _port:String;
        private var _path:String;
        private var _query:String;
        private var _fragment:String;

        public function URL(_arg1:String){
            this._rawUrl = _arg1;
            this._protocol = "";
            this._userInfo = "";
            this._host = "";
            this._port = "";
            this._path = "";
            this._query = "";
            this._fragment = "";
            if (((!((this._rawUrl == null))) && ((this._rawUrl.length > 0)))){
                this._rawUrl = this._rawUrl.replace(/^\s+|\s+$/g, "");
                this.parseUrl();
            };
        }
        public static function isAbsoluteURL(_arg1:String):Boolean{
            var _local2:URL = new URL(_arg1);
            return (_local2.absolute);
        }
        public static function getRootUrl(_arg1:String):String{
            var _local2:String = _arg1.substr(0, _arg1.lastIndexOf("/"));
            return (_local2);
        }
        public static function normalizeRootURL(_arg1:String):String{
            if (((!((_arg1 == null))) && (!((_arg1.charAt((_arg1.length - 1)) == "/"))))){
                return ((_arg1 + "/"));
            };
            return (_arg1);
        }
        public static function normalizeRelativeURL(_arg1:String):String{
            if (_arg1.charAt(0) == "/"){
                return (_arg1.substr(1));
            };
            return (_arg1);
        }
        public static function normalizePathForURL(_arg1:String, _arg2:Boolean):String{
            var _local5:String;
            var _local6:int;
            var _local3:String = _arg1;
            var _local4:URL = new URL(_arg1);
            if (_local4.absolute){
                _local3 = ((_local4.protocol + "://") + _local4.host);
                if (((!((_local4.port == null))) && ((_local4.port.length > 0)))){
                    _local3 = (_local3 + (":" + _local4.port));
                };
                _local5 = _local4.path;
                if (((!((_local5 == null))) && ((_local5.length > 0)))){
                    if (_arg2){
                        _local6 = _local5.lastIndexOf("/");
                        _local5 = _local5.substr(0, (_local6 + 1));
                    };
                    _local3 = (_local3 + ("/" + _local5));
                };
            };
            return (URL.normalizeRootURL(_local3));
        }

        public function get rawUrl():String{
            return (this._rawUrl);
        }
        public function get protocol():String{
            return (this._protocol);
        }
        public function set protocol(_arg1:String):void{
            if (_arg1 != null){
                this._protocol = _arg1.replace(/:\/?\/?$/, "");
                this._protocol = this._protocol.toLowerCase();
            };
        }
        public function get userInfo():String{
            return (this._userInfo);
        }
        public function set userInfo(_arg1:String):void{
            if (_arg1 != null){
                this._userInfo = _arg1.replace(/@$/, "");
            };
        }
        public function get host():String{
            return (this._host);
        }
        public function set host(_arg1:String):void{
            this._host = _arg1;
        }
        public function get port():String{
            return (this._port);
        }
        public function set port(_arg1:String):void{
            if (_arg1 != null){
                this._port = _arg1.replace(/(:)/, "");
            };
        }
        public function get path():String{
            return (this._path);
        }
        public function set path(_arg1:String):void{
            if (_arg1 != null){
                this._path = _arg1.replace(/^\//, "");
            };
        }
        public function get query():String{
            return (this._query);
        }
        public function set query(_arg1:String):void{
            if (_arg1 != null){
                this._query = _arg1.replace(/^\?/, "");
            };
        }
        public function get fragment():String{
            return (this._fragment);
        }
        public function set fragment(_arg1:String):void{
            if (_arg1 != null){
                this._fragment = _arg1.replace(/^#/, "");
            };
        }
        public function toString():String{
            return (this._rawUrl);
        }
        public function getParamValue(_arg1:String):String{
            if (this._query == null){
                return ("");
            };
            var _local2:RegExp = new RegExp((("[/?&]*" + _arg1) + "=([^&#]*)"), "i");
            var _local3:Array = this._query.match(_local2);
            var _local4:String = ((_local3)==null) ? "" : _local3[1];
            return (_local4);
        }
        public function get absolute():Boolean{
            return (!((this.protocol == "")));
        }
        public function get extension():String{
            var _local1:int = this.path.lastIndexOf("/");
            var _local2:int = this.path.lastIndexOf(".");
            if (((!((_local2 == -1))) && ((_local2 > _local1)))){
                return (this.path.substr((_local2 + 1)));
            };
            return ("");
        }
        private function parseUrl():void{
            var _local1:RegExp;
            var _local2:Array;
            var _local3:String;
            var _local4:RegExp;
            var _local5:Array;
            var _local6:String;
            if ((((this._rawUrl == null)) || ((this._rawUrl.length == 0)))){
                return;
            };
            if ((((this._rawUrl.search(/:\//) == -1)) && (!((this._rawUrl.indexOf(":") == (this._rawUrl.length - 1)))))){
                this.path = this._rawUrl;
            } else {
                _local1 = /^(rtmp|rtmp[tse]|rtmpte)(:\/[^\/])/i;
                _local2 = this._rawUrl.match(_local1);
                _local3 = this._rawUrl;
                if (_local2 != null){
                    _local3 = this._rawUrl.replace(/:\//, "://localhost/");
                };
                _local4 = /^([a-z+\w\+\.\-]+:\/?\/?)?([^\/?#]*)?(\/[^?#]*)?(\?[^#]*)?(\#.*)?/i;
                _local5 = _local3.match(_local4);
                if (_local5 != null){
                    this.protocol = _local5[1];
                    _local6 = _local5[2];
                    this.path = _local5[3];
                    this.query = _local5[4];
                    this.fragment = _local5[5];
                    _local4 = /^([!-~]+@)?([^\/?#:]*)(:[\d]*)?/i;
                    _local5 = _local6.match(_local4);
                    if (_local5 != null){
                        this.userInfo = _local5[1];
                        this.host = _local5[2];
                        this.port = _local5[3];
                    };
                };
            };
        }

    }
}//package org.osmf.utils 
