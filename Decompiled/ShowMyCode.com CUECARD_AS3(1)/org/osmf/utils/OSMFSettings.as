﻿package org.osmf.utils {
    import flash.system.*;

    public final class OSMFSettings {

        public static var enableStageVideo:Boolean = true;
        public static var hdsMinimumBufferTime:Number = 4;
        public static var hdsAdditionalBufferTime:Number = 2;
        public static var hdsBytesProcessingLimit:Number = 102400;
        public static var hdsBytesReadingLimit:Number = 102400;
        public static var hdsMainTimerInterval:int = 25;
        public static var hdsLiveStallTolerance:Number = 15;
        public static var hdsDefaultFragmentsThreshold:uint = 5;
        public static var hdsMinimumBootstrapRefreshInterval:uint = 2000;
        public static var hdsDVRLiveOffset:Number = 4;
        public static var hdsPureLiveOffset:Number = 5;
        public static var f4mParseTimeout:Number = 30000;
        public static var hdsMaximumRetries:Number = 5;
        public static var hdsTimeoutAdjustmentOnRetry:Number = 4000;
        public static var hdsFragmentDownloadTimeout:Number = 4000;
        public static var hdsIndexDownloadTimeout:Number = 4000;

        public static function get supportsStageVideo():Boolean{
            return (runtimeSupportsStageVideo(Capabilities.version));
        }
        static function runtimeSupportsStageVideo(_arg1:String):Boolean{
            if (_arg1 == null){
                return (false);
            };
            var _local2:Array = _arg1.split(" ");
            if (_local2.length < 2){
                return (false);
            };
            var _local3:String = _local2[0];
            var _local4:Array = _local2[1].split(",");
            if (_local4.length < 2){
                return (false);
            };
            var _local5:Number = parseInt(_local4[0]);
            var _local6:Number = parseInt(_local4[1]);
            return ((((_local5 > 10)) || ((((_local5 == 10)) && ((_local6 >= 2))))));
        }

    }
}//package org.osmf.utils 
