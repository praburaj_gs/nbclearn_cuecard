﻿package org.osmf.net.metrics {
    import org.osmf.net.qos.*;

    public class BufferLengthMetric extends MetricBase {

        public function BufferLengthMetric(_arg1:QoSInfoHistory){
            super(_arg1, MetricType.BUFFER_LENGTH);
        }
        override protected function getValueForced():MetricValue{
            var _local1:QoSInfo = qosInfoHistory.getLatestQoSInfo();
            if (((!(isNaN(_local1.bufferLength))) && ((_local1.bufferLength >= 0)))){
                return (new MetricValue(_local1.bufferLength, true));
            };
            return (new MetricValue(undefined, false));
        }

    }
}//package org.osmf.net.metrics 
