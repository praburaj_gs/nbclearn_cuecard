﻿package org.osmf.net.metrics {
    import org.osmf.net.qos.*;

    public class FPSMetric extends MetricBase {

        public function FPSMetric(_arg1:QoSInfoHistory){
            super(_arg1, MetricType.FPS);
        }
        override protected function getValueForced():MetricValue{
            var _local1:QoSInfo = qosInfoHistory.getLatestQoSInfo();
            if (((isNaN(_local1.maxFPS)) || ((_local1.maxFPS == 0)))){
                return (new MetricValue(undefined, false));
            };
            return (new MetricValue(_local1.maxFPS, true));
        }

    }
}//package org.osmf.net.metrics 
