﻿package org.osmf.net.metrics {
    import org.osmf.net.qos.*;

    public class BufferOccupationRatioMetric extends MetricBase {

        public function BufferOccupationRatioMetric(_arg1:QoSInfoHistory){
            super(_arg1, MetricType.BUFFER_OCCUPATION_RATIO);
        }
        override protected function getValueForced():MetricValue{
            var _local1:QoSInfo = qosInfoHistory.getLatestQoSInfo();
            if (((((((!(isNaN(_local1.bufferLength))) && (!(isNaN(_local1.bufferTime))))) && ((_local1.bufferTime > 0)))) && ((_local1.bufferLength >= 0)))){
                return (new MetricValue((_local1.bufferLength / _local1.bufferTime), true));
            };
            return (new MetricValue(undefined, false));
        }

    }
}//package org.osmf.net.metrics 
