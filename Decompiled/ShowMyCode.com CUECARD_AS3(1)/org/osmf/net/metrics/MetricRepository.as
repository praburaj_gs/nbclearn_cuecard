﻿package org.osmf.net.metrics {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import flash.utils.*;

    public class MetricRepository {

        private static const logger:Logger = Log.getLogger("org.osmf.net.metrics.MetricRepository");

        private var metrics:Dictionary;
        private var lastKey:String;
        private var lastKeyByteArray:ByteArray;
        private var _metricFactory:MetricFactory = null;

        public function MetricRepository(_arg1:MetricFactory){
            this.metrics = new Dictionary();
            if (_arg1 == null){
                throw (new ArgumentError("The metricFactory should not be null!"));
            };
            this._metricFactory = _arg1;
        }
        public function get metricFactory():MetricFactory{
            return (this._metricFactory);
        }
        public function getMetric(_arg1:String, ... _args):MetricBase{
            var _local5:Array;
            var _local6:MetricBase;
            var _local3:int = getTimer();
            var _local4:Array = _args.slice();
            _local4.splice(0, 0, _arg1);
            if (this.metrics[_arg1] == null){
                this.metrics[_arg1] = new Vector.<Array>();
            };
            for each (_local5 in this.metrics[_arg1]) {
                if (this.match(_local5.slice(1), _local4)){
                    logger.debug((((("The metric of type '" + _arg1) + "' with the desired parameters is already in the repository. Finding it took: ") + ((getTimer() - _local3) / 1000)) + " seconds."));
                    return (_local5[0]);
                };
            };
            _local6 = this._metricFactory.buildMetric.apply(null, _local4);
            _local4.splice(0, 0, _local6);
            this.metrics[_arg1].push(_local4);
            logger.debug((((("A new metric of type '" + _arg1) + "' had to be instantiated. This took: ") + ((getTimer() - _local3) / 1000)) + " seconds."));
            return (_local6);
        }
        private function match(_arg1, _arg2):Boolean{
            var _local3:uint;
            if (_arg1 == _arg2){
                return (true);
            };
            if ((((_arg1 == null)) && ((_arg2 == null)))){
                return (true);
            };
            if ((((((((_arg1 is Number)) && ((_arg2 is Number)))) && (isNaN(_arg1)))) && (isNaN(_arg2)))){
                return (true);
            };
            if (((((this.isVector(_arg1)) && (this.isVector(_arg2)))) || ((((_arg1 is Array)) && ((_arg2 is Array)))))){
                if (_arg1.length != _arg2.length){
                    return (false);
                };
                _local3 = 0;
                while (_local3 < _arg1.length) {
                    if (!this.match(_arg1[_local3], _arg2[_local3])){
                        return (false);
                    };
                    _local3++;
                };
                return (true);
            };
            return (false);
        }
        private function isVector(_arg1:Object):Boolean{
            var _local2:String = getQualifiedClassName(_arg1);
            return ((_local2.indexOf("__AS3__.vec::Vector.") === 0));
        }

    }
}//package org.osmf.net.metrics 
