﻿package org.osmf.net.metrics {
    import __AS3__.vec.*;
    import org.osmf.net.qos.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.utils.*;

    public class MetricFactory {

        private var items:Dictionary;
        private var _numItems:Number;
        private var _qosInfoHistory:QoSInfoHistory;

        public function MetricFactory(_arg1:QoSInfoHistory){
            this._qosInfoHistory = _arg1;
            this.items = new Dictionary();
            this._numItems = 0;
        }
        public function addItem(_arg1:MetricFactoryItem):void{
            if ((((_arg1 == null)) || ((_arg1.type == null)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            if (this.items[_arg1.type] == null){
                this._numItems++;
            };
            this.items[_arg1.type] = _arg1;
        }
        public function removeItem(_arg1:MetricFactoryItem):void{
            if ((((_arg1 == null)) || ((_arg1.type == null)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            if (this.items[_arg1.type] != null){
                this._numItems--;
            };
            this.items[_arg1.type] = null;
        }
        public function getItems():Vector.<MetricFactoryItem>{
            var _local2:MetricFactoryItem;
            var _local1:Vector.<MetricFactoryItem> = new Vector.<MetricFactoryItem>();
            for each (_local2 in this.items) {
                _local1.push(_local2);
            };
            return (_local1);
        }
        public function getItem(_arg1:String):MetricFactoryItem{
            return (this.items[_arg1]);
        }
        public function get numItems():Number{
            return (this._numItems);
        }
        public function buildMetric(_arg1:String, ... _args):MetricBase{
            if (this.items[_arg1] == null){
                throw (new MetricError(MetricErrorCodes.INVALID_METRIC_TYPE));
            };
            var _local3:MetricFactoryItem = (this.items[_arg1] as MetricFactoryItem);
            _args.splice(0, 0, this._qosInfoHistory);
            return (_local3.metricCreationFunction.apply(null, _args));
        }

    }
}//package org.osmf.net.metrics 
