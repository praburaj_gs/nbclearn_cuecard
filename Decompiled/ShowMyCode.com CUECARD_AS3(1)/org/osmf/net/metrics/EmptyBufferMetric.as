﻿package org.osmf.net.metrics {
    import org.osmf.logging.*;
    import org.osmf.net.qos.*;

    public class EmptyBufferMetric extends MetricBase {

        private static const logger:Logger = Log.getLogger("org.osmf.net.metrics.EmptyBufferMetric");

        public function EmptyBufferMetric(_arg1:QoSInfoHistory){
            super(_arg1, MetricType.EMPTY_BUFFER);
        }
        override protected function getValueForced():MetricValue{
            var _local1:QoSInfo = qosInfoHistory.getLatestQoSInfo();
            logger.info(("EmptyBuffer metric is valid and has value: " + _local1.emptyBufferOccurred));
            return (new MetricValue(_local1.emptyBufferOccurred, true));
        }

    }
}//package org.osmf.net.metrics 
