﻿package org.osmf.net.metrics {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.qos.*;
    import flash.utils.*;
    import flash.errors.*;

    public class MetricBase {

        private static const logger:Logger = Log.getLogger("org.osmf.net.metrics.MetricBase");

        private var _type:String = null;
        private var lastMachineTime:Number = NaN;
        private var lastValue:MetricValue;
        private var _qosInfoHistory:QoSInfoHistory;

        public function MetricBase(_arg1:QoSInfoHistory, _arg2:String){
            if (_arg1 == null){
                throw (new ArgumentError("qosInfoHistory cannot be null."));
            };
            this._qosInfoHistory = _arg1;
            this._type = _arg2;
            this.lastValue = new MetricValue(undefined, false);
        }
        public function get type():String{
            return (this._type);
        }
        final public function get value():MetricValue{
            return (this.getValue());
        }
        function getValue():MetricValue{
            var _local2:int;
            _local2 = getTimer();
            var _local1:Vector.<QoSInfo> = this._qosInfoHistory.getHistory();
            if (_local1.length > 0){
                if (_local1[0].timestamp != this.lastMachineTime){
                    this.lastValue = this.getValueForced();
                    this.lastMachineTime = _local1[0].timestamp;
                    logger.debug((((("The value of the '" + this._type) + "' metric was computed in: ") + ((getTimer() - _local2) / 1000)) + " seconds."));
                } else {
                    logger.debug((((("The value of the '" + this._type) + "' metric is cached. Fetching it took: ") + ((getTimer() - _local2) / 1000)) + " seconds."));
                };
                return (this.lastValue);
            };
            logger.info((("No QoS history available. Returning undefined value for metric '" + this._type) + "'."));
            return (new MetricValue(undefined, false));
        }
        protected function getValueForced():MetricValue{
            throw (new IllegalOperationError("The getValueForced() method must be overridden by the derived class."));
        }
        protected function get qosInfoHistory():QoSInfoHistory{
            return (this._qosInfoHistory);
        }

    }
}//package org.osmf.net.metrics 
