﻿package org.osmf.net.metrics {
    import __AS3__.vec.*;
    import org.osmf.net.qos.*;

    public class CurrentStatusMetric extends MetricBase {

        public function CurrentStatusMetric(_arg1:QoSInfoHistory){
            super(_arg1, MetricType.CURRENT_STATUS);
        }
        override protected function getValueForced():MetricValue{
            var _local1:QoSInfo = qosInfoHistory.getLatestQoSInfo();
            var _local2:Vector.<uint> = new Vector.<uint>();
            _local2.push(_local1.currentIndex);
            _local2.push(_local1.actualIndex);
            return (new MetricValue(_local2, true));
        }

    }
}//package org.osmf.net.metrics 
