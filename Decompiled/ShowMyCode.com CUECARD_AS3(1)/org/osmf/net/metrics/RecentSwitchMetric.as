﻿package org.osmf.net.metrics {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.qos.*;

    public class RecentSwitchMetric extends MetricBase {

        private static const logger:Logger = Log.getLogger("org.osmf.net.metrics.RecentSwitchMetric");

        public function RecentSwitchMetric(_arg1:QoSInfoHistory){
            super(_arg1, MetricType.RECENT_SWITCH);
        }
        override protected function getValueForced():MetricValue{
            var _local3:FragmentDetails;
            var _local4:FragmentDetails;
            var _local1:Vector.<QoSInfo> = qosInfoHistory.getHistory(2);
            var _local2:int;
            if (_local1.length >= 2){
                _local3 = _local1[0].lastDownloadedFragmentDetails;
                _local4 = _local1[1].lastDownloadedFragmentDetails;
                if ((((_local3 == null)) || ((_local4 == null)))){
                    logger.info("Recent switch metric is invalid, since fragment details are not present in QoS");
                    return (new MetricValue(undefined, false));
                };
                _local2 = (_local3.index - _local4.index);
            } else {
                logger.info("Recent switch metric is invalid, since fragment details are not present in QoS");
                return (new MetricValue(undefined, false));
            };
            logger.info(("Recent switch metric is valid and has value: " + _local2));
            return (new MetricValue(_local2, true));
        }

    }
}//package org.osmf.net.metrics 
