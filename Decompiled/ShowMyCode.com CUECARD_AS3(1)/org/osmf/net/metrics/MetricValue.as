﻿package org.osmf.net.metrics {

    public class MetricValue {

        private var _value;
        private var _valid:Boolean;

        public function MetricValue(_arg1, _arg2:Boolean=true){
            this._value = _arg1;
            this._valid = _arg2;
        }
        public function get value(){
            return (this._value);
        }
        public function get valid():Boolean{
            return (this._valid);
        }

    }
}//package org.osmf.net.metrics 
