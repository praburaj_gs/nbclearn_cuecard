﻿package org.osmf.net.metrics {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.*;
    import org.osmf.net.qos.*;

    public class ActualBitrateMetric extends MetricBase {

        private static const logger:Logger = Log.getLogger("org.osmf.net.metrics.ActualBitrateMetric");

        private var _maxFragments:uint;

        public function ActualBitrateMetric(_arg1:QoSInfoHistory, _arg2:uint=5){
            super(_arg1, MetricType.ACTUAL_BITRATE);
            this.maxFragments = _arg2;
        }
        public function get maxFragments():uint{
            return (this._maxFragments);
        }
        public function set maxFragments(_arg1:uint):void{
            if (_arg1 < 1){
                throw (new ArgumentError("Invalid value for 'maxFragments'."));
            };
            this._maxFragments = _arg1;
        }
        override protected function getValueForced():MetricValue{
            var _local5:QoSInfo;
            var _local7:FragmentDetails;
            var _local1:Vector.<QoSInfo> = qosInfoHistory.getHistory(this._maxFragments);
            var _local2:int = _local1[0].actualIndex;
            var _local3:Number = 0;
            var _local4:Number = 0;
            logger.debug("Fragments considered in computation:");
            for each (_local5 in _local1) {
                if (_local5.lastDownloadedFragmentDetails.index == _local2){
                    _local7 = _local5.lastDownloadedFragmentDetails;
                    _local3 = (_local3 + _local7.size);
                    _local4 = (_local4 + _local7.playDuration);
                    logger.debug((((" Fragment. Size = " + _local7.size) + "; playDuration = ") + _local7.playDuration));
                };
            };
            if (_local4 == 0){
                logger.info("Actual bitrate metric is invalid, as the total play duration of the fragments is 0.");
                return (new MetricValue(undefined, false));
            };
            var _local6:Number = (((_local3 / _local4) * 8) / 1000);
            logger.info(("Actual bitrate metric is valid and has value: " + ABRUtils.roundNumber(_local6)));
            return (new MetricValue(_local6, true));
        }

    }
}//package org.osmf.net.metrics 
