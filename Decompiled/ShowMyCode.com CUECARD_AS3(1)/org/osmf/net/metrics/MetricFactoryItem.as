﻿package org.osmf.net.metrics {
    import org.osmf.utils.*;

    public class MetricFactoryItem {

        private var _type:String;
        private var _metricCreationFunction:Function;

        public function MetricFactoryItem(_arg1:String, _arg2:Function){
            if ((((_arg1 == null)) || ((_arg2 == null)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            this._type = _arg1;
            this._metricCreationFunction = _arg2;
        }
        public function get type():String{
            return (this._type);
        }
        public function get metricCreationFunction():Function{
            return (this._metricCreationFunction);
        }

    }
}//package org.osmf.net.metrics 
