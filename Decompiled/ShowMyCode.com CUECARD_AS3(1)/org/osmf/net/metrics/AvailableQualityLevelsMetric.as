﻿package org.osmf.net.metrics {
    import org.osmf.net.qos.*;

    public class AvailableQualityLevelsMetric extends MetricBase {

        public function AvailableQualityLevelsMetric(_arg1:QoSInfoHistory){
            super(_arg1, MetricType.AVAILABLE_QUALITY_LEVELS);
        }
        override protected function getValueForced():MetricValue{
            var _local1:QoSInfo = qosInfoHistory.getHistory(1)[0];
            return (new MetricValue(_local1.availableQualityLevels, true));
        }

    }
}//package org.osmf.net.metrics 
