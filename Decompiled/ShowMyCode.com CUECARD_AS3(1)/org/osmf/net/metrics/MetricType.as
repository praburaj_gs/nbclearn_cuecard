﻿package org.osmf.net.metrics {

    public final class MetricType {

        public static const FRAGMENT_COUNT:String = "org.osmf.net.metrics.fragmentCount";
        public static const BANDWIDTH:String = "org.osmf.net.metrics.bandwidth";
        public static const AVAILABLE_QUALITY_LEVELS:String = "org.osmf.net.metrics.availableQualityLevels";
        public static const CURRENT_STATUS:String = "org.osmf.net.metrics.currentStatus";
        public static const ACTUAL_BITRATE:String = "org.osmf.net.metrics.actualBitrate";
        public static const FPS:String = "org.osmf.net.metrics.fps";
        public static const DROPPED_FPS:String = "org.osmf.net.metrics.droppedFPS";
        public static const BUFFER_OCCUPATION_RATIO:String = "org.osmf.net.metrics.bufferOccupationRatio";
        public static const BUFFER_LENGTH:String = "org.osmf.net.metrics.bufferLength";
        public static const BUFFER_FRAGMENTS:String = "org.osmf.net.metrics.bufferFragments";
        public static const EMPTY_BUFFER:String = "org.osmf.net.metrics.emptyBuffer";
        public static const RECENT_SWITCH:String = "org.osmf.net.metrics.recentSwitch";

    }
}//package org.osmf.net.metrics 
