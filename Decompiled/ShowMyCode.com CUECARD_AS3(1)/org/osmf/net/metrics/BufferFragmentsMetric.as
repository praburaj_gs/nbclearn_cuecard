﻿package org.osmf.net.metrics {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.qos.*;

    public class BufferFragmentsMetric extends MetricBase {

        private static const logger:Logger = Log.getLogger("org.osmf.net.metrics.BufferFragmentsMetric");

        public function BufferFragmentsMetric(_arg1:QoSInfoHistory){
            super(_arg1, MetricType.BUFFER_FRAGMENTS);
        }
        override protected function getValueForced():MetricValue{
            var _local5:QoSInfo;
            var _local1:Vector.<QoSInfo> = qosInfoHistory.getHistory();
            var _local2:Number = _local1[0].bufferLength;
            if (((isNaN(_local2)) || ((_local2 < 0)))){
                logger.info("Buffer fragments metric is not valid, as the bufferLength is not available in the QoSInfo");
                return (new MetricValue(undefined, false));
            };
            var _local3:Number = 0;
            var _local4:Number = 0;
            while (_local4 < _local1.length) {
                _local5 = _local1[_local4];
                _local3 = (_local3 + _local5.lastDownloadedFragmentDetails.playDuration);
                if (_local3 > _local2){
                    break;
                };
                _local4++;
            };
            logger.info(("Buffer fragments metric is valid and has value: " + _local4));
            return (new MetricValue(_local4, true));
        }

    }
}//package org.osmf.net.metrics 
