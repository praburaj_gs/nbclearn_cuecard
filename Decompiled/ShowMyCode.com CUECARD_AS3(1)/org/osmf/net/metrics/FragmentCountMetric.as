﻿package org.osmf.net.metrics {
    import org.osmf.net.qos.*;

    public class FragmentCountMetric extends MetricBase {

        public function FragmentCountMetric(_arg1:QoSInfoHistory){
            super(_arg1, MetricType.FRAGMENT_COUNT);
        }
        override protected function getValueForced():MetricValue{
            return (new MetricValue(qosInfoHistory.length, true));
        }

    }
}//package org.osmf.net.metrics 
