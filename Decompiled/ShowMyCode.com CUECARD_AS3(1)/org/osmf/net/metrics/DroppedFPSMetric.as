﻿package org.osmf.net.metrics {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.*;
    import org.osmf.net.qos.*;

    public class DroppedFPSMetric extends MetricBase {

        private static const MINIMUM_CONTINUOUS_PLAYBACK_DURATION:Number = 1;
        private static const MINIMUM_TOTAL_PLAYBACK_DURATION:Number = 2;
        private static const logger:Logger = Log.getLogger("org.osmf.net.metrics.DroppedFPSMetric");

        private var _desiredSampleLength:Number = 10;

        public function DroppedFPSMetric(_arg1:QoSInfoHistory, _arg2:Number=10){
            super(_arg1, MetricType.DROPPED_FPS);
            this.desiredSampleLength = _arg2;
        }
        public function get desiredSampleLength():Number{
            return (this._desiredSampleLength);
        }
        public function set desiredSampleLength(_arg1:Number):void{
            if (((isNaN(_arg1)) || ((_arg1 < 0)))){
                throw (new ArgumentError("Invalid desiredSampleLength"));
            };
            this._desiredSampleLength = _arg1;
        }
        override protected function getValueForced():MetricValue{
            var _local6:Vector.<PlaybackDetails>;
            var _local7:PlaybackDetails;
            var _local1:Vector.<QoSInfo> = qosInfoHistory.getHistory();
            var _local2:Number = 0;
            var _local3:Number = 0;
            var _local4:uint = _local1[0].currentIndex;
            logger.debug((("Computing dfps metric for currently playing index (" + _local4) + ")."));
            var _local5:uint;
            while (_local5 < _local1.length) {
                _local6 = _local1[_local5].playbackDetailsRecord;
                for each (_local7 in _local6) {
                    if ((((_local7.duration < 0)) || ((_local7.droppedFrames < 0)))){
                        logger.warn("The QoSInfo playback details are corrupt (negative value found for playback duration or dropped frames.");
                        return (new MetricValue(undefined, false));
                    };
                    if ((((_local7.index == _local4)) && ((_local7.duration >= MINIMUM_CONTINUOUS_PLAYBACK_DURATION)))){
                        _local2 = (_local2 + _local7.duration);
                        _local3 = (_local3 + _local7.droppedFrames);
                        break;
                    };
                };
                if (_local2 >= this.desiredSampleLength){
                    break;
                };
                _local5++;
            };
            if (_local2 < MINIMUM_TOTAL_PLAYBACK_DURATION){
                logger.info((("DroppedFPS metric is not valid as there is not enough data in the QoSInfoHistory regarding playback of the current stream (index " + _local4) + ")."));
                logger.debug((((("There were " + _local2) + " seconds available and the required amount of data is: ") + MINIMUM_TOTAL_PLAYBACK_DURATION) + " seconds."));
                return (new MetricValue(undefined, false));
            };
            logger.info(("DroppedFPS metric is valid and has value: " + ABRUtils.roundNumber((_local3 / _local2))));
            return (new MetricValue((_local3 / _local2), true));
        }

    }
}//package org.osmf.net.metrics 
