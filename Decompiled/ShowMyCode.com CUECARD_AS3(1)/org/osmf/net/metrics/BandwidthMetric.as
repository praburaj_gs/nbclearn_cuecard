﻿package org.osmf.net.metrics {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.*;
    import org.osmf.net.qos.*;

    public class BandwidthMetric extends MetricBase {

        private static const logger:Logger = Log.getLogger("org.osmf.net.metrics.BandwidthMetric");

        private var _weights:Vector.<Number>;

        public function BandwidthMetric(_arg1:QoSInfoHistory, _arg2:Vector.<Number>){
            super(_arg1, MetricType.BANDWIDTH);
            ABRUtils.validateWeights(_arg2);
            this._weights = _arg2.slice();
        }
        public function get weights():Vector.<Number>{
            return (this._weights);
        }
        override protected function getValueForced():MetricValue{
            var _local5:FragmentDetails;
            var _local6:Number;
            var _local1:Vector.<QoSInfo> = qosInfoHistory.getHistory(this._weights.length);
            var _local2:Number = 0;
            var _local3:Number = 0;
            var _local4:uint;
            while (_local4 < _local1.length) {
                _local5 = _local1[_local4].lastDownloadedFragmentDetails;
                _local6 = (_local5.size / _local5.downloadDuration);
                _local2 = (_local2 + (_local6 * this._weights[_local4]));
                _local3 = (_local3 + this._weights[_local4]);
                _local4++;
            };
            _local2 = (_local2 / _local3);
            logger.info((("Bandwidth metric is valid and has value: " + ABRUtils.roundNumber(_local2)) + " B/s"));
            return (new MetricValue(_local2, true));
        }

    }
}//package org.osmf.net.metrics 
