﻿package org.osmf.net.metrics {
    import __AS3__.vec.*;
    import org.osmf.net.qos.*;

    public class DefaultMetricFactory extends MetricFactory {

        public function DefaultMetricFactory(_arg1:QoSInfoHistory){
            super(_arg1);
            this.init();
        }
        private function init():void{
            addItem(new MetricFactoryItem(MetricType.ACTUAL_BITRATE, function (_arg1:QoSInfoHistory, _arg2:uint=5):MetricBase{
                return (new ActualBitrateMetric(_arg1, _arg2));
            }));
            addItem(new MetricFactoryItem(MetricType.FRAGMENT_COUNT, function (_arg1:QoSInfoHistory):MetricBase{
                return (new FragmentCountMetric(_arg1));
            }));
            addItem(new MetricFactoryItem(MetricType.AVAILABLE_QUALITY_LEVELS, function (_arg1:QoSInfoHistory):MetricBase{
                return (new AvailableQualityLevelsMetric(_arg1));
            }));
            addItem(new MetricFactoryItem(MetricType.CURRENT_STATUS, function (_arg1:QoSInfoHistory):MetricBase{
                return (new CurrentStatusMetric(_arg1));
            }));
            addItem(new MetricFactoryItem(MetricType.BANDWIDTH, function (_arg1:QoSInfoHistory, _arg2:Vector.<Number>):MetricBase{
                return (new BandwidthMetric(_arg1, _arg2));
            }));
            addItem(new MetricFactoryItem(MetricType.FPS, function (_arg1:QoSInfoHistory):MetricBase{
                return (new FPSMetric(_arg1));
            }));
            addItem(new MetricFactoryItem(MetricType.DROPPED_FPS, function (_arg1:QoSInfoHistory, _arg2:Number=10):MetricBase{
                return (new DroppedFPSMetric(_arg1, _arg2));
            }));
            addItem(new MetricFactoryItem(MetricType.BUFFER_OCCUPATION_RATIO, function (_arg1:QoSInfoHistory):MetricBase{
                return (new BufferOccupationRatioMetric(_arg1));
            }));
            addItem(new MetricFactoryItem(MetricType.BUFFER_LENGTH, function (_arg1:QoSInfoHistory):MetricBase{
                return (new BufferLengthMetric(_arg1));
            }));
            addItem(new MetricFactoryItem(MetricType.BUFFER_FRAGMENTS, function (_arg1:QoSInfoHistory):MetricBase{
                return (new BufferFragmentsMetric(_arg1));
            }));
            addItem(new MetricFactoryItem(MetricType.EMPTY_BUFFER, function (_arg1:QoSInfoHistory):MetricBase{
                return (new EmptyBufferMetric(_arg1));
            }));
            addItem(new MetricFactoryItem(MetricType.RECENT_SWITCH, function (_arg1:QoSInfoHistory):MetricBase{
                return (new RecentSwitchMetric(_arg1));
            }));
        }

    }
}//package org.osmf.net.metrics 
