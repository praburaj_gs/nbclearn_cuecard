﻿package org.osmf.net.drm {
    import flash.events.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import flash.system.*;
    import org.osmf.utils.*;
    import flash.net.drm.*;
    import org.osmf.net.drm.*;
    import flash.errors.*;

    class DRMServices extends EventDispatcher {

        private static const DRM_AUTHENTICATION_FAILED:int = 3301;
        private static const DRM_NEEDS_AUTHENTICATION:int = 3330;
        private static const DRM_CONTENT_NOT_YET_VALID:int = 3331;

        private static var updater:SystemUpdater;

        private var _drmState:String = "uninitialized";
        private var lastToken:ByteArray;
        private var drmContentData:DRMContentData;
        private var voucher:DRMVoucher;
        private var drmManager:DRMManager;

        public function DRMServices(){
            this.drmManager = DRMManager.getDRMManager();
        }
        public static function convertToken(_arg1:Object):ByteArray{
            var _local3:String;
            var _local2:ByteArray;
            if (_arg1 != null){
                _local2 = (_arg1 as ByteArray);
                if (_local2 == null){
                    _local3 = _arg1.toString();
                    if (((!((_local3 == null))) && ((_local3.length > 0)))){
                        _local2 = new ByteArray();
                        _local2.writeUTFBytes(_local3);
                        _local2.position = 0;
                    };
                };
            };
            return (_local2);
        }

        public function get drmState():String{
            return (this._drmState);
        }
        public function set drmMetadata(_arg1:Object):void{
            var onComplete:* = null;
            var value:* = _arg1;
            this.lastToken = null;
            if ((value is DRMContentData)){
                this.drmContentData = (value as DRMContentData);
                this.retrieveVoucher();
            } else {
                try {
                    this.drmContentData = new DRMContentData((value as ByteArray));
                    this.retrieveVoucher();
                } catch(argError:ArgumentError) {
                    updateDRMState(DRMState.AUTHENTICATION_ERROR, new MediaError(argError.errorID, "DRMContentData invalid"));
                } catch(error:IllegalOperationError) {
                    onComplete = function (_arg1:Event):void{
                        updater.removeEventListener(Event.COMPLETE, onComplete);
                        drmMetadata = value;
                    };
                    update(SystemUpdaterType.DRM);
                    updater.addEventListener(Event.COMPLETE, onComplete);
                };
            };
        }
        public function get drmMetadata():Object{
            return (this.drmContentData);
        }
        public function authenticate(_arg1:String=null, _arg2:String=null):void{
            if (this.drmContentData == null){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.DRM_METADATA_NOT_SET)));
            };
            this.drmManager.addEventListener(DRMAuthenticationErrorEvent.AUTHENTICATION_ERROR, this.authError);
            this.drmManager.addEventListener(DRMAuthenticationCompleteEvent.AUTHENTICATION_COMPLETE, this.authComplete);
            if ((((_arg2 == null)) && ((_arg1 == null)))){
                this.retrieveVoucher();
            } else {
                this.drmManager.authenticate(this.drmContentData.serverURL, this.drmContentData.domain, _arg1, _arg2);
            };
        }
        public function authenticateWithToken(_arg1:Object):void{
            if (this.drmContentData == null){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.DRM_METADATA_NOT_SET)));
            };
            this.drmManager.setAuthenticationToken(this.drmContentData.serverURL, this.drmContentData.domain, convertToken(_arg1));
            this.retrieveVoucher();
        }
        public function get startDate():Date{
            if (this.voucher != null){
                return (((this.voucher.playbackTimeWindow) ? this.voucher.playbackTimeWindow.startDate : this.voucher.voucherStartDate));
            };
            return (null);
        }
        public function get endDate():Date{
            if (this.voucher != null){
                return (((this.voucher.playbackTimeWindow) ? this.voucher.playbackTimeWindow.endDate : this.voucher.voucherEndDate));
            };
            return (null);
        }
        public function get period():Number{
            if (this.voucher != null){
                return (((this.voucher.playbackTimeWindow) ? this.voucher.playbackTimeWindow.period : ((((this.voucher.voucherEndDate) && (this.voucher.voucherStartDate))) ? ((this.voucher.voucherEndDate.time - this.voucher.voucherStartDate.time) / 1000) : 0)));
            };
            return (NaN);
        }
        public function inlineDRMFailed(_arg1:MediaError):void{
            this.updateDRMState(DRMState.AUTHENTICATION_ERROR, _arg1);
        }
        public function inlineOnVoucher(_arg1:DRMStatusEvent):void{
            this.drmContentData = _arg1.contentData;
            this.onVoucherLoaded(_arg1);
        }
        public function update(_arg1:String):SystemUpdater{
            this.updateDRMState(DRMState.DRM_SYSTEM_UPDATING);
            if (updater == null){
                updater = new SystemUpdater();
                this.toggleErrorListeners(updater, true);
                updater.update(_arg1);
            } else {
                this.toggleErrorListeners(updater, true);
            };
            return (updater);
        }
        private function retrieveVoucher():void{
            this.updateDRMState(DRMState.AUTHENTICATING);
            this.drmManager.addEventListener(DRMErrorEvent.DRM_ERROR, this.onDRMError);
            this.drmManager.addEventListener(DRMStatusEvent.DRM_STATUS, this.onVoucherLoaded);
            this.drmManager.loadVoucher(this.drmContentData, LoadVoucherSetting.ALLOW_SERVER);
        }
        private function onVoucherLoaded(_arg1:DRMStatusEvent):void{
            var _local2:Date;
            if (_arg1.contentData == this.drmContentData){
                _local2 = new Date();
                if (((_arg1.voucher) && ((((((_arg1.voucher.voucherEndDate == null)) || ((_arg1.voucher.voucherEndDate.time >= _local2.time)))) && ((((_arg1.voucher.voucherStartDate == null)) || ((_arg1.voucher.voucherStartDate.time <= _local2.time)))))))){
                    this.voucher = _arg1.voucher;
                    this.removeEventListeners();
                    if (this.voucher.playbackTimeWindow == null){
                        this.updateDRMState(DRMState.AUTHENTICATION_COMPLETE, null, this.voucher.voucherStartDate, this.voucher.voucherEndDate, this.period, this.lastToken);
                    } else {
                        this.updateDRMState(DRMState.AUTHENTICATION_COMPLETE, null, this.voucher.playbackTimeWindow.startDate, this.voucher.playbackTimeWindow.endDate, this.voucher.playbackTimeWindow.period, this.lastToken);
                    };
                } else {
                    this.forceRefreshVoucher();
                };
            };
        }
        private function forceRefreshVoucher():void{
            this.drmManager.loadVoucher(this.drmContentData, LoadVoucherSetting.FORCE_REFRESH);
        }
        private function onDRMError(_arg1:DRMErrorEvent):void{
            if (_arg1.contentData == this.drmContentData){
                switch (_arg1.errorID){
                    case DRM_CONTENT_NOT_YET_VALID:
                        this.forceRefreshVoucher();
                        break;
                    case DRM_NEEDS_AUTHENTICATION:
                        this.updateDRMState(DRMState.AUTHENTICATION_NEEDED, null, null, null, 0, null, _arg1.contentData.serverURL);
                        break;
                    default:
                        this.removeEventListeners();
                        this.updateDRMState(DRMState.AUTHENTICATION_ERROR, new MediaError(_arg1.errorID, _arg1.text));
                };
            };
        }
        private function removeEventListeners():void{
            this.drmManager.removeEventListener(DRMErrorEvent.DRM_ERROR, this.onDRMError);
            this.drmManager.removeEventListener(DRMStatusEvent.DRM_STATUS, this.onVoucherLoaded);
        }
        private function authComplete(_arg1:DRMAuthenticationCompleteEvent):void{
            this.drmManager.removeEventListener(DRMAuthenticationErrorEvent.AUTHENTICATION_ERROR, this.authError);
            this.drmManager.removeEventListener(DRMAuthenticationCompleteEvent.AUTHENTICATION_COMPLETE, this.authComplete);
            this.lastToken = _arg1.token;
            this.retrieveVoucher();
        }
        private function authError(_arg1:DRMAuthenticationErrorEvent):void{
            this.drmManager.removeEventListener(DRMAuthenticationErrorEvent.AUTHENTICATION_ERROR, this.authError);
            this.drmManager.removeEventListener(DRMAuthenticationCompleteEvent.AUTHENTICATION_COMPLETE, this.authComplete);
            this.updateDRMState(DRMState.AUTHENTICATION_ERROR, new MediaError(_arg1.errorID, _arg1.toString()));
        }
        private function toggleErrorListeners(_arg1:SystemUpdater, _arg2:Boolean):void{
            if (_arg2){
                _arg1.addEventListener(Event.COMPLETE, this.onUpdateComplete);
                _arg1.addEventListener(Event.CANCEL, this.onUpdateComplete);
                _arg1.addEventListener(IOErrorEvent.IO_ERROR, this.onUpdateError);
                _arg1.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onUpdateError);
                _arg1.addEventListener(StatusEvent.STATUS, this.onUpdateError);
            } else {
                _arg1.removeEventListener(Event.COMPLETE, this.onUpdateComplete);
                _arg1.removeEventListener(Event.CANCEL, this.onUpdateComplete);
                _arg1.removeEventListener(IOErrorEvent.IO_ERROR, this.onUpdateError);
                _arg1.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onUpdateError);
                _arg1.removeEventListener(StatusEvent.STATUS, this.onUpdateError);
            };
        }
        private function onUpdateComplete(_arg1:Event):void{
            this.toggleErrorListeners(updater, false);
        }
        private function onUpdateError(_arg1:Event):void{
            this.toggleErrorListeners(updater, false);
            this.updateDRMState(DRMState.AUTHENTICATION_ERROR, new MediaError(MediaErrorCodes.DRM_SYSTEM_UPDATE_ERROR, _arg1.toString()));
        }
        private function updateDRMState(_arg1:String, _arg2:MediaError=null, _arg3:Date=null, _arg4:Date=null, _arg5:Number=0, _arg6:Object=null, _arg7:String=null):void{
            this._drmState = _arg1;
            dispatchEvent(new DRMEvent(DRMEvent.DRM_STATE_CHANGE, _arg1, false, false, _arg3, _arg4, _arg5, _arg7, _arg6, _arg2));
        }

    }
}//package org.osmf.net.drm 
