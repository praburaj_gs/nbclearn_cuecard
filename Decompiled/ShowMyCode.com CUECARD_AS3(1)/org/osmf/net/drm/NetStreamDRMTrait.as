﻿package org.osmf.net.drm {
    import flash.events.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.system.*;

    public class NetStreamDRMTrait extends DRMTrait {

        private var drmServices:DRMServices;

        public function NetStreamDRMTrait(){
            this.drmServices = new DRMServices();
            super();
            this.drmServices.addEventListener(DRMEvent.DRM_STATE_CHANGE, this.onStateChange);
        }
        public function set drmMetadata(_arg1:Object):void{
            if (_arg1 != this.drmServices.drmMetadata){
                this.drmServices.drmMetadata = _arg1;
            };
        }
        public function get drmMetadata():Object{
            return (this.drmServices.drmMetadata);
        }
        public function update(_arg1:String):SystemUpdater{
            return (this.drmServices.update(_arg1));
        }
        override public function authenticate(_arg1:String=null, _arg2:String=null):void{
            this.drmServices.authenticate(_arg1, _arg2);
        }
        override public function authenticateWithToken(_arg1:Object):void{
            this.drmServices.authenticateWithToken(_arg1);
        }
        public function inlineDRMFailed(_arg1:MediaError):void{
            this.drmServices.inlineDRMFailed(_arg1);
        }
        public function inlineOnVoucher(_arg1:DRMStatusEvent):void{
            this.drmServices.inlineOnVoucher(_arg1);
        }
        private function onStateChange(_arg1:DRMEvent):void{
            setPeriod(_arg1.period);
            setStartDate(_arg1.startDate);
            setEndDate(_arg1.endDate);
            setDrmState(_arg1.drmState);
            dispatchEvent(new DRMEvent(DRMEvent.DRM_STATE_CHANGE, drmState, false, false, startDate, endDate, period, _arg1.serverURL, _arg1.token, _arg1.mediaError));
        }

    }
}//package org.osmf.net.drm 
