﻿package org.osmf.net {
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import flash.net.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.utils.*;

    public class NetConnectionFactory extends NetConnectionFactoryBase {

        private static const DEFAULT_TIMEOUT:Number = 10000;
        private static const DEFAULT_PORTS:String = "1935,443,80";
        private static const DEFAULT_PROTOCOLS_FOR_RTMP:String = "rtmp,rtmpt,rtmps";
        private static const DEFAULT_PROTOCOLS_FOR_RTMPE:String = "rtmpe,rtmpte";
        private static const DEFAULT_CONNECTION_ATTEMPT_INTERVAL:Number = 200;
        private static const PROTOCOL_RTMP:String = "rtmp";
        private static const PROTOCOL_RTMPS:String = "rtmps";
        private static const PROTOCOL_RTMPT:String = "rtmpt";
        private static const PROTOCOL_RTMPE:String = "rtmpe";
        private static const PROTOCOL_RTMPTE:String = "rtmpte";
        private static const PROTOCOL_HTTP:String = "http";
        private static const PROTOCOL_HTTPS:String = "https";
        private static const PROTOCOL_FILE:String = "file";
        private static const PROTOCOL_EMPTY:String = "";
        private static const MP3_EXTENSION:String = ".mp3";

        private var shareNetConnections:Boolean;
        private var negotiator:NetNegotiator;
        private var connectionDictionary:Dictionary;
        private var keyDictionary:Dictionary;
        private var pendingDictionary:Dictionary;
        private var _connectionAttemptInterval:Number = 200;
        private var _timeout:Number = 10000;

        public function NetConnectionFactory(_arg1:Boolean=true){
            this.shareNetConnections = _arg1;
        }
        public function get timeout():Number{
            return (this._timeout);
        }
        public function set timeout(_arg1:Number):void{
            this._timeout = _arg1;
        }
        public function get connectionAttemptInterval():Number{
            return (this._connectionAttemptInterval);
        }
        public function set connectionAttemptInterval(_arg1:Number):void{
            this._connectionAttemptInterval = _arg1;
        }
        override public function create(_arg1:URLResource):void{
            var key:* = null;
            var pendingConnections:* = null;
            var urlIncludesFMSApplicationInstance:* = false;
            var netConnectionURLs:* = null;
            var netConnections:* = null;
            var j:* = 0;
            var negotiator:* = null;
            var onConnected:* = null;
            var onConnectionFailed:* = null;
            var resource:* = _arg1;
            key = this.createNetConnectionKey(resource);
            if (this.connectionDictionary == null){
                this.connectionDictionary = new Dictionary();
                this.keyDictionary = new Dictionary();
                this.pendingDictionary = new Dictionary();
            };
            var sharedConnection:* = (this.connectionDictionary[key] as SharedConnection);
            var connectionsUnderway:* = (this.pendingDictionary[key] as Vector.<URLResource>);
            if (((!((sharedConnection == null))) && (this.shareNetConnections))){
                sharedConnection.count++;
                dispatchEvent(new NetConnectionFactoryEvent(NetConnectionFactoryEvent.CREATION_COMPLETE, false, false, sharedConnection.netConnection, resource));
            } else {
                if (connectionsUnderway != null){
                    connectionsUnderway.push(resource);
                } else {
                    onConnected = function (_arg1:NetConnectionFactoryEvent):void{
                        var _local5:NetConnectionFactoryEvent;
                        var _local6:URLResource;
                        var _local7:SharedConnection;
                        var _local8:SharedConnection;
                        negotiator.removeEventListener(NetConnectionFactoryEvent.CREATION_COMPLETE, onConnected);
                        negotiator.removeEventListener(NetConnectionFactoryEvent.CREATION_ERROR, onConnectionFailed);
                        var _local2:Vector.<NetConnectionFactoryEvent> = new Vector.<NetConnectionFactoryEvent>();
                        var _local3:Vector.<URLResource> = pendingDictionary[key];
                        var _local4:Number = 0;
                        while (_local4 < _local3.length) {
                            _local6 = (_local3[_local4] as URLResource);
                            if (shareNetConnections){
                                _local7 = (connectionDictionary[key] as SharedConnection);
                                if (_local7 != null){
                                    _local7.count++;
                                } else {
                                    _local8 = new SharedConnection();
                                    _local8.count = 1;
                                    _local8.netConnection = _arg1.netConnection;
                                    connectionDictionary[key] = _local8;
                                    keyDictionary[_local8.netConnection] = key;
                                };
                            };
                            _local2.push(new NetConnectionFactoryEvent(NetConnectionFactoryEvent.CREATION_COMPLETE, false, false, _arg1.netConnection, _local6));
                            _local4++;
                        };
                        delete pendingDictionary[key];
                        for each (_local5 in _local2) {
                            dispatchEvent(_local5);
                        };
                    };
                    onConnectionFailed = function (_arg1:NetConnectionFactoryEvent):void{
                        var _local3:URLResource;
                        negotiator.removeEventListener(NetConnectionFactoryEvent.CREATION_COMPLETE, onConnected);
                        negotiator.removeEventListener(NetConnectionFactoryEvent.CREATION_ERROR, onConnectionFailed);
                        var _local2:Vector.<URLResource> = pendingDictionary[key];
                        for each (_local3 in _local2) {
                            dispatchEvent(new NetConnectionFactoryEvent(NetConnectionFactoryEvent.CREATION_ERROR, false, false, null, _local3, _arg1.mediaError));
                        };
                        delete pendingDictionary[key];
                    };
                    pendingConnections = new Vector.<URLResource>();
                    pendingConnections.push(resource);
                    this.pendingDictionary[key] = pendingConnections;
                    urlIncludesFMSApplicationInstance = (((resource is StreamingURLResource)) ? StreamingURLResource(resource).urlIncludesFMSApplicationInstance : false);
                    netConnectionURLs = this.createNetConnectionURLs(resource.url, urlIncludesFMSApplicationInstance);
                    netConnections = new Vector.<NetConnection>();
                    j = 0;
                    while (j < netConnectionURLs.length) {
                        netConnections.push(this.createNetConnection());
                        j = (j + 1);
                    };
                    negotiator = new NetNegotiator(this._connectionAttemptInterval, this._timeout);
                    negotiator.addEventListener(NetConnectionFactoryEvent.CREATION_COMPLETE, onConnected);
                    negotiator.addEventListener(NetConnectionFactoryEvent.CREATION_ERROR, onConnectionFailed);
                    negotiator.createNetConnection(resource, netConnectionURLs, netConnections);
                };
            };
        }
        override public function closeNetConnection(_arg1:NetConnection):void{
            var _local2:String;
            var _local3:SharedConnection;
            if (this.shareNetConnections){
                _local2 = (this.keyDictionary[_arg1] as String);
                if (_local2 != null){
                    _local3 = (this.connectionDictionary[_local2] as SharedConnection);
                    _local3.count--;
                    if (_local3.count == 0){
                        _arg1.close();
                        delete this.connectionDictionary[_local2];
                        delete this.keyDictionary[_arg1];
                    };
                };
            } else {
                super.closeNetConnection(_arg1);
            };
        }
        protected function createNetConnectionKey(_arg1:URLResource):String{
            var _local2:FMSURL = new FMSURL(_arg1.url);
            return (((((_local2.protocol + _local2.host) + _local2.port) + _local2.appName) + _local2.instanceName));
        }
        protected function createNetConnection():NetConnection{
            return (new NetConnection());
        }
        protected function createNetConnectionURLs(_arg1:String, _arg2:Boolean=false):Vector.<String>{
            var _local5:PortProtocol;
            var _local3:Vector.<String> = new Vector.<String>();
            var _local4:Vector.<PortProtocol> = this.buildPortProtocolSequence(_arg1);
            for each (_local5 in _local4) {
                _local3.push(this.buildConnectionAddress(_arg1, _arg2, _local5));
            };
            return (_local3);
        }
        private function buildPortProtocolSequence(_arg1:String):Vector.<PortProtocol>{
            var _local9:int;
            var _local10:PortProtocol;
            var _local2:Vector.<PortProtocol> = new Vector.<PortProtocol>();
            var _local3:URL = new URL(_arg1);
            var _local4:String = ((_local3.port)=="") ? DEFAULT_PORTS : _local3.port;
            var _local5 = "";
            switch (_local3.protocol){
                case PROTOCOL_RTMP:
                    _local5 = DEFAULT_PROTOCOLS_FOR_RTMP;
                    break;
                case PROTOCOL_RTMPE:
                    _local5 = DEFAULT_PROTOCOLS_FOR_RTMPE;
                    break;
                case PROTOCOL_RTMPS:
                case PROTOCOL_RTMPT:
                case PROTOCOL_RTMPTE:
                    _local5 = _local3.protocol;
                    break;
            };
            var _local6:Array = _local4.split(",");
            var _local7:Array = _local5.split(",");
            var _local8:int;
            while (_local8 < _local7.length) {
                _local9 = 0;
                while (_local9 < _local6.length) {
                    _local10 = new PortProtocol();
                    _local10.protocol = _local7[_local8];
                    _local10.port = _local6[_local9];
                    _local2.push(_local10);
                    _local9++;
                };
                _local8++;
            };
            return (_local2);
        }
        private function buildConnectionAddress(_arg1:String, _arg2:Boolean, _arg3:PortProtocol):String{
            var _local4:FMSURL = new FMSURL(_arg1, _arg2);
            var _local5:String = (((((((_arg3.protocol + "://") + _local4.host) + ":") + _arg3.port) + "/") + _local4.appName) + ((_local4.useInstance) ? ("/" + _local4.instanceName) : ""));
            if (((!((_local4.query == null))) && (!((_local4.query == ""))))){
                _local5 = (_local5 + ("?" + _local4.query));
            };
            return (_local5);
        }

    }
}//package org.osmf.net 

import flash.net.*;

class SharedConnection {

    public var count:Number;
    public var netConnection:NetConnection;

    public function SharedConnection(){
    }
}
