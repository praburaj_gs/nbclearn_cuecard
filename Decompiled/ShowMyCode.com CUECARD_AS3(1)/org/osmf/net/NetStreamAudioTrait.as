﻿package org.osmf.net {
    import flash.net.*;
    import org.osmf.traits.*;
    import flash.media.*;

    public class NetStreamAudioTrait extends AudioTrait {

        private var netStream:NetStream;

        public function NetStreamAudioTrait(_arg1:NetStream){
            this.netStream = _arg1;
        }
        override protected function volumeChangeStart(_arg1:Number):void{
            var _local2:SoundTransform = this.netStream.soundTransform;
            _local2.volume = ((muted) ? 0 : _arg1);
            this.netStream.soundTransform = _local2;
        }
        override protected function mutedChangeStart(_arg1:Boolean):void{
            var _local2:SoundTransform = this.netStream.soundTransform;
            _local2.volume = ((_arg1) ? 0 : volume);
            this.netStream.soundTransform = _local2;
        }
        override protected function panChangeStart(_arg1:Number):void{
            var _local2:SoundTransform = this.netStream.soundTransform;
            _local2.pan = _arg1;
            this.netStream.soundTransform = _local2;
        }

    }
}//package org.osmf.net 
