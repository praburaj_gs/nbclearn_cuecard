﻿package org.osmf.net.dvr {
    import flash.events.*;
    import org.osmf.media.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.net.dvr.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    class DVRCastDVRTrait extends DVRTrait {

        private var connection:NetConnection;
        private var stream:NetStream;
        private var streamInfo:DVRCastStreamInfo;
        private var recordingInfo:DVRCastRecordingInfo;
        private var streamInfoUpdateTimer:Timer;
        private var streamInfoRetriever:DVRCastStreamInfoRetriever;
        private var offset:Number;

        public function DVRCastDVRTrait(_arg1:NetConnection, _arg2:NetStream, _arg3:MediaResourceBase){
            if (((!((_arg1 == null))) && (!((_arg2 == null))))){
                this.stream = _arg2;
                this.streamInfo = (_arg3.getMetadataValue(DVRCastConstants.STREAM_INFO_KEY) as DVRCastStreamInfo);
                this.recordingInfo = (_arg3.getMetadataValue(DVRCastConstants.RECORDING_INFO_KEY) as DVRCastRecordingInfo);
                this.streamInfoRetriever = new DVRCastStreamInfoRetriever(_arg1, this.streamInfo.streamName);
                this.streamInfoRetriever.addEventListener(Event.COMPLETE, this.onStreamInfoRetrieverComplete);
                this.streamInfoUpdateTimer = new Timer(DVRCastConstants.STREAM_INFO_UPDATE_DELAY);
                this.streamInfoUpdateTimer.addEventListener(TimerEvent.TIMER, this.onStreamInfoUpdateTimer);
                this.streamInfoUpdateTimer.start();
                super(this.streamInfo.isRecording);
                this.updateProperties();
            } else {
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
        }
        override protected function isRecordingChangeStart(_arg1:Boolean):void{
            if (_arg1){
                this.recordingInfo.startDuration = this.streamInfo.currentLength;
                this.recordingInfo.startTime = new Date();
            };
        }
        private function updateProperties():void{
            setIsRecording(this.streamInfo.isRecording);
        }
        private function onStreamInfoUpdateTimer(_arg1:TimerEvent):void{
            this.streamInfoRetriever.retrieve();
        }
        private function onStreamInfoRetrieverComplete(_arg1:Event):void{
            if (this.streamInfoRetriever.streamInfo != null){
                this.streamInfo.readFromDVRCastStreamInfo(this.streamInfoRetriever.streamInfo);
                this.updateProperties();
            } else {
                dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.DVRCAST_STREAM_INFO_RETRIEVAL_FAILED)));
            };
        }

    }
}//package org.osmf.net.dvr 
