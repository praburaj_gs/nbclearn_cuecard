﻿package org.osmf.net.dvr {
    import flash.events.*;
    import flash.net.*;
    import flash.utils.*;
    import org.osmf.net.dvr.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    class DVRCastStreamInfoRetriever extends EventDispatcher {

        private var connection:NetConnection;
        private var streamName:String;
        private var retries:Number;
        private var timer:Timer;
        private var _streamInfo:DVRCastStreamInfo;
        private var _error:Object;

        public function DVRCastStreamInfoRetriever(_arg1:NetConnection, _arg2:String){
            if ((((_arg1 == null)) || ((_arg2 == null)))){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            this.connection = _arg1;
            this.streamName = _arg2;
        }
        public function get streamInfo():DVRCastStreamInfo{
            return (this._streamInfo);
        }
        public function get error():Object{
            return (this._error);
        }
        public function retrieve(_arg1:int=5, _arg2:Number=3):void{
            if (!isNaN(this.retries)){
            } else {
                _arg1 = ((_arg1) || (1));
                this._streamInfo = null;
                this._error = (this._error = {message:OSMFStrings.getString(OSMFStrings.DVR_MAXIMUM_RPC_ATTEMPTS).replace("%i", _arg1)});
                this.retries = _arg1;
                this.timer = new Timer((_arg2 * 1000), 1);
                this.getStreamInfo();
            };
        }
        private function getStreamInfo():void{
            var _local1:Responder = new TestableResponder(this.onGetStreamInfoResult, this.onServerCallError);
            this.retries--;
            this.connection.call(DVRCastConstants.RPC_GET_STREAM_INFO, _local1, this.streamName);
        }
        private function onGetStreamInfoResult(_arg1:Object):void{
            if (((_arg1) && ((_arg1.code == DVRCastConstants.RESULT_GET_STREAM_INFO_SUCCESS)))){
                this._error = null;
                this._streamInfo = new DVRCastStreamInfo(_arg1.data);
                this.complete();
            } else {
                if (((_arg1) && ((_arg1.code == DVRCastConstants.RESULT_GET_STREAM_INFO_RETRY)))){
                    if (this.retries != 0){
                        this.timer.addEventListener(TimerEvent.TIMER_COMPLETE, this.onTimerComplete);
                        this.timer.start();
                    } else {
                        this.complete();
                    };
                } else {
                    this._error = {message:(OSMFStrings.getString(OSMFStrings.DVR_UNEXPECTED_SERVER_RESPONSE) + _arg1.code)};
                    this.complete();
                };
            };
        }
        private function onServerCallError(_arg1:Object):void{
            this._error = _arg1;
            this.complete();
        }
        private function onTimerComplete(_arg1:TimerEvent):void{
            this.timer.removeEventListener(TimerEvent.TIMER_COMPLETE, this.onTimerComplete);
            this.getStreamInfo();
        }
        private function complete():void{
            this.retries = NaN;
            this.timer = null;
            dispatchEvent(new Event(Event.COMPLETE));
        }

    }
}//package org.osmf.net.dvr 
