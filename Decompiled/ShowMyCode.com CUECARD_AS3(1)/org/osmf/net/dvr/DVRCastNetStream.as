﻿package org.osmf.net.dvr {
    import org.osmf.media.*;
    import flash.net.*;

    public class DVRCastNetStream extends NetStream {

        private var recordingInfo:DVRCastRecordingInfo;

        public function DVRCastNetStream(_arg1:NetConnection, _arg2:MediaResourceBase){
            super(_arg1);
            this.recordingInfo = (_arg2.getMetadataValue(DVRCastConstants.RECORDING_INFO_KEY) as DVRCastRecordingInfo);
        }
        override public function play(... _args):void{
            super.play(_args[0], this.recordingInfo.startOffset, -1);
        }
        override public function play2(_arg1:NetStreamPlayOptions):void{
            if (_arg1){
                _arg1.start = this.recordingInfo.startOffset;
                _arg1.len = -1;
            };
            super.play2(_arg1);
        }

    }
}//package org.osmf.net.dvr 
