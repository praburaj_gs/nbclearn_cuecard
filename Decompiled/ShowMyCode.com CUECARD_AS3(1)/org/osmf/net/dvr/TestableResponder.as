﻿package org.osmf.net.dvr {
    import flash.net.*;

    class TestableResponder extends Responder {

        private var _result:Function;
        private var _status:Function;

        public function TestableResponder(_arg1:Function, _arg2:Function=null){
            this._result = _arg1;
            this._status = _arg2;
            super(_arg1, _arg2);
        }
        function get result():Function{
            return (this._result);
        }
        function get status():Function{
            return (this._status);
        }

    }
}//package org.osmf.net.dvr 
