﻿package org.osmf.net.dvr {
    import org.osmf.media.*;
    import org.osmf.net.*;
    import flash.net.*;
    import org.osmf.traits.*;
    import org.osmf.net.rtmpstreaming.*;

    public class DVRCastNetLoader extends RTMPDynamicStreamingNetLoader {

        public function DVRCastNetLoader(_arg1:DVRCastNetConnectionFactory=null){
            if (_arg1 == null){
                _arg1 = new DVRCastNetConnectionFactory();
            };
            super(_arg1);
        }
        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            var _local2:Boolean;
            var _local3:StreamingURLResource;
            if (super.canHandleResource(_arg1)){
                _local3 = (_arg1 as StreamingURLResource);
                if (_local3 != null){
                    _local2 = (((_local3.streamType == StreamType.DVR)) && (NetStreamUtils.isRTMPStream(_local3.url)));
                };
            };
            return (_local2);
        }
        override protected function createNetStream(_arg1:NetConnection, _arg2:URLResource):NetStream{
            return (new DVRCastNetStream(_arg1, _arg2));
        }
        override protected function processFinishLoading(_arg1:NetStreamLoadTrait):void{
            _arg1.setTrait(new DVRCastDVRTrait(_arg1.connection, _arg1.netStream, _arg1.resource));
            _arg1.setTrait(new DVRCastTimeTrait(_arg1.connection, _arg1.netStream, _arg1.resource));
            updateLoadTrait(_arg1, LoadState.READY);
        }

    }
}//package org.osmf.net.dvr 
