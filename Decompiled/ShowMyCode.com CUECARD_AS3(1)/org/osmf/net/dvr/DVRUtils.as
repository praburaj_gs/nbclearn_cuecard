﻿package org.osmf.net.dvr {

    public class DVRUtils {

        public static function calculateOffset(_arg1:Number, _arg2:Number, _arg3:Number):Number{
            var _local4:Number = 0;
            if (_arg2 != 0){
                if (_arg3 > _arg2){
                    _local4 = (_arg3 - _arg2);
                } else {
                    _local4 = Math.min(_arg1, _arg3);
                };
            } else {
                if (_arg1 != 0){
                    _local4 = Math.min(_arg1, _arg3);
                };
            };
            return (_local4);
        }

    }
}//package org.osmf.net.dvr 
