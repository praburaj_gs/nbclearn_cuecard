﻿package org.osmf.net.dvr {

    public class DVRCastRecordingInfo {

        public var startTime:Date = null;
        public var startDuration:Number;
        public var startOffset:Number;

    }
}//package org.osmf.net.dvr 
