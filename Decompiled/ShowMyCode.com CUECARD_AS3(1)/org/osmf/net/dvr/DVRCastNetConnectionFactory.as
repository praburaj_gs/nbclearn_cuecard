﻿package org.osmf.net.dvr {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.net.*;
    import flash.net.*;
    import org.osmf.events.*;
    import flash.utils.*;

    public class DVRCastNetConnectionFactory extends NetConnectionFactoryBase {

        private var innerFactory:NetConnectionFactoryBase;
        private var subscribedStreams:Dictionary;

        public function DVRCastNetConnectionFactory(_arg1:NetConnectionFactoryBase=null){
            this.subscribedStreams = new Dictionary();
            this.innerFactory = ((_arg1) || (new NetConnectionFactory()));
            this.innerFactory.addEventListener(NetConnectionFactoryEvent.CREATION_COMPLETE, this.onCreationComplete);
            this.innerFactory.addEventListener(NetConnectionFactoryEvent.CREATION_ERROR, this.onCreationError);
            super();
        }
        override public function create(_arg1:URLResource):void{
            this.innerFactory.create(_arg1);
        }
        override public function closeNetConnection(_arg1:NetConnection):void{
            var _local2:String = this.subscribedStreams[_arg1];
            if (_local2 != null){
                _arg1.call(DVRCastConstants.RPC_UNSUBSCRIBE, null, _local2);
                delete this.subscribedStreams[_arg1];
            };
            this.innerFactory.closeNetConnection(_arg1);
        }
        private function onCreationComplete(_arg1:NetConnectionFactoryEvent):void{
            var urlResource:* = null;
            var netConnection:* = null;
            var streamNames:* = null;
            var totalRpcSubscribeInvocation:* = 0;
            var streamingResource:* = null;
            var onStreamSubscriptionResult:* = null;
            var onStreamInfoRetrieverComplete:* = null;
            var onServerCallError:* = null;
            var items:* = null;
            var i:* = 0;
            var event:* = _arg1;
            onStreamSubscriptionResult = function (_arg1:Object):void{
                var _local2:DVRCastStreamInfoRetriever;
                totalRpcSubscribeInvocation--;
                if (totalRpcSubscribeInvocation <= 0){
                    _local2 = new DVRCastStreamInfoRetriever(netConnection, streamNames[0]);
                    _local2.addEventListener(Event.COMPLETE, onStreamInfoRetrieverComplete);
                    _local2.retrieve();
                };
            };
            onStreamInfoRetrieverComplete = function (_arg1:Event):void{
                var _local3:DVRCastRecordingInfo;
                var _local2:DVRCastStreamInfoRetriever = (_arg1.target as DVRCastStreamInfoRetriever);
                removeEventListener(NetConnectionFactoryEvent.CREATION_COMPLETE, onCreationComplete);
                if (_local2.streamInfo != null){
                    if (_local2.streamInfo.offline == true){
                        dispatchEvent(new NetConnectionFactoryEvent(NetConnectionFactoryEvent.CREATION_ERROR, false, false, netConnection, urlResource, new MediaError(MediaErrorCodes.DVRCAST_CONTENT_OFFLINE)));
                        i = 0;
                        while (i < streamNames.length) {
                            netConnection.call(DVRCastConstants.RPC_UNSUBSCRIBE, null, streamNames[i]);
                            i++;
                        };
                        netConnection = null;
                    } else {
                        _local3 = new DVRCastRecordingInfo();
                        _local3.startDuration = _local2.streamInfo.currentLength;
                        _local3.startOffset = calculateOffset(_local2.streamInfo);
                        _local3.startTime = new Date();
                        streamingResource.addMetadataValue(DVRCastConstants.STREAM_INFO_KEY, _local2.streamInfo);
                        streamingResource.addMetadataValue(DVRCastConstants.RECORDING_INFO_KEY, _local3);
                        subscribedStreams[netConnection] = streamNames[0];
                        dispatchEvent(new NetConnectionFactoryEvent(NetConnectionFactoryEvent.CREATION_COMPLETE, false, false, netConnection, urlResource));
                    };
                } else {
                    onServerCallError(_local2.error);
                };
            };
            onServerCallError = function (_arg1:Object):void{
                dispatchEvent(new NetConnectionFactoryEvent(NetConnectionFactoryEvent.CREATION_ERROR, false, false, netConnection, urlResource, new MediaError(MediaErrorCodes.DVRCAST_SUBSCRIBE_FAILED, ((_arg1) ? _arg1.message : ""))));
            };
            urlResource = (event.resource as URLResource);
            netConnection = event.netConnection;
            streamNames = new Vector.<String>();
            totalRpcSubscribeInvocation = 0;
            event.stopImmediatePropagation();
            streamingResource = (urlResource as StreamingURLResource);
            var urlIncludesFMSApplicationInstance:* = ((streamingResource) ? streamingResource.urlIncludesFMSApplicationInstance : false);
            var dynamicResource:* = (streamingResource as DynamicStreamingResource);
            if (dynamicResource != null){
                items = dynamicResource.streamItems;
                totalRpcSubscribeInvocation = items.length;
                i = 0;
                while (i < items.length) {
                    streamNames.push(items[i].streamName);
                    i = (i + 1);
                };
            } else {
                totalRpcSubscribeInvocation = 1;
                streamNames.push(NetStreamUtils.getStreamNameFromURL(urlResource.url, urlIncludesFMSApplicationInstance));
            };
            var responder:* = new TestableResponder(onStreamSubscriptionResult, onServerCallError);
            i = 0;
            while (i < streamNames.length) {
                event.netConnection.call(DVRCastConstants.RPC_SUBSCRIBE, responder, streamNames[i]);
                i = (i + 1);
            };
        }
        private function onCreationError(_arg1:NetConnectionFactoryEvent):void{
            dispatchEvent(_arg1.clone());
        }
        private function calculateOffset(_arg1:DVRCastStreamInfo):Number{
            return (DVRUtils.calculateOffset(_arg1.beginOffset, _arg1.endOffset, _arg1.currentLength));
        }

    }
}//package org.osmf.net.dvr 
