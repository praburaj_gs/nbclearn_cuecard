﻿package org.osmf.net.dvr {
    import flash.events.*;
    import org.osmf.media.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.net.dvr.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    class DVRCastTimeTrait extends TimeTrait {

        private var durationUpdateTimer:Timer;
        private var oldDuration:Number;
        private var stream:NetStream;
        private var streamInfo:DVRCastStreamInfo;
        private var recordingInfo:DVRCastRecordingInfo;

        public function DVRCastTimeTrait(_arg1:NetConnection, _arg2:NetStream, _arg3:MediaResourceBase){
            super(NaN);
            if ((((_arg1 == null)) || ((_arg2 == null)))){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            this.stream = _arg2;
            _arg2.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus);
            this.durationUpdateTimer = new Timer(DVRCastConstants.LOCAL_DURATION_UPDATE_INTERVAL);
            this.durationUpdateTimer.addEventListener(TimerEvent.TIMER, this.onDurationUpdateTimer);
            this.durationUpdateTimer.start();
            this.streamInfo = (_arg3.getMetadataValue(DVRCastConstants.STREAM_INFO_KEY) as DVRCastStreamInfo);
            this.recordingInfo = (_arg3.getMetadataValue(DVRCastConstants.RECORDING_INFO_KEY) as DVRCastRecordingInfo);
        }
        override public function get duration():Number{
            var _local1:Number;
            if (this.streamInfo.isRecording){
                _local1 = ((this.recordingInfo.startDuration - this.recordingInfo.startOffset) + ((new Date().time - this.recordingInfo.startTime.time) / 1000));
            } else {
                _local1 = (this.streamInfo.currentLength - this.recordingInfo.startOffset);
            };
            _local1 = ((isNaN(_local1)) ? NaN : Math.max(0, _local1));
            return (_local1);
        }
        override public function get currentTime():Number{
            return (this.stream.time);
        }
        private function onDurationUpdateTimer(_arg1:TimerEvent):void{
            var _local2:Number = this.duration;
            if (_local2 != this.oldDuration){
                this.oldDuration = _local2;
                dispatchEvent(new TimeEvent(TimeEvent.DURATION_CHANGE, false, false, _local2));
            };
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            if (_arg1.info.code == "NetStream.Play.Stop"){
                if (this.durationUpdateTimer){
                    this.durationUpdateTimer.stop();
                };
                signalComplete();
            };
        }

    }
}//package org.osmf.net.dvr 
