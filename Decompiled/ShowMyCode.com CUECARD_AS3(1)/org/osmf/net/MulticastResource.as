﻿package org.osmf.net {
    import __AS3__.vec.*;
    import flash.utils.*;

    public class MulticastResource extends StreamingURLResource {

        private var _groupspec:String;
        private var _streamName:String;

        public function MulticastResource(_arg1:String, _arg2:String=null, _arg3:String=null, _arg4:Vector.<Object>=null, _arg5:Boolean=false, _arg6:ByteArray=null){
            super(_arg1, StreamType.LIVE, NaN, NaN, _arg4, _arg5, _arg6);
            this._groupspec = _arg2;
            this._streamName = _arg3;
        }
        public function get groupspec():String{
            return (this._groupspec);
        }
        public function set groupspec(_arg1:String):void{
            this._groupspec = _arg1;
        }
        public function get streamName():String{
            return (this._streamName);
        }
        public function set streamName(_arg1:String):void{
            this._streamName = _arg1;
        }

    }
}//package org.osmf.net 
