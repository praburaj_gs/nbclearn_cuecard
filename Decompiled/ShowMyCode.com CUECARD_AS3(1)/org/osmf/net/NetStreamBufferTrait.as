﻿package org.osmf.net {
    import flash.events.*;
    import flash.net.*;
    import org.osmf.traits.*;
    import org.osmf.media.videoClasses.*;

    public class NetStreamBufferTrait extends BufferTrait {

        private var netStream:NetStream;
        private var videoSurface:VideoSurface;

        public function NetStreamBufferTrait(_arg1:NetStream, _arg2:VideoSurface=null){
            this.netStream = _arg1;
            this.videoSurface = _arg2;
            bufferTime = _arg1.bufferTime;
            _arg1.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus, false, 0, true);
        }
        override public function get bufferLength():Number{
            return (this.netStream.bufferLength);
        }
        override protected function bufferTimeChangeStart(_arg1:Number):void{
            this.netStream.bufferTime = _arg1;
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_PLAY_START:
                case NetStreamCodes.NETSTREAM_BUFFER_EMPTY:
                    bufferTime = this.netStream.bufferTime;
                    setBuffering(true);
                    if (this.netStream.bufferTime == 0){
                        setBuffering(false);
                    };
                    if (this.bufferNotSupported){
                        setBuffering(false);
                    };
                    break;
                case NetStreamCodes.NETSTREAM_SEEK_START:
                    setBuffering(true);
                    break;
                case NetStreamCodes.NETSTREAM_BUFFER_FLUSH:
                case NetStreamCodes.NETSTREAM_BUFFER_FULL:
                    setBuffering(false);
                    break;
            };
        }
        private function get bufferNotSupported():Boolean{
            return ((((((this.netStream.bytesLoaded == 0)) && (!((this.videoSurface == null))))) && ((this.videoSurface.info.renderStatus == "accelerated"))));
        }

    }
}//package org.osmf.net 
