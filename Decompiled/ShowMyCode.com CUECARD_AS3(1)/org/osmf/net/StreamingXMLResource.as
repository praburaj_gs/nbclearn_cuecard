﻿package org.osmf.net {
    import org.osmf.media.*;

    public class StreamingXMLResource extends MediaResourceBase {

        private var _manifest:String;
        private var _url:String;
        private var _clipStartTime:Number;
        private var _clipEndTime:Number;

        public function StreamingXMLResource(_arg1:String, _arg2:String=null, _arg3:Number=NaN, _arg4:Number=NaN){
            var _local5:XML;
            var _local6:Namespace;
            var _local7:String;
            super();
            this._manifest = _arg1;
            if (_arg2 != null){
                this._url = _arg2;
            } else {
                _local5 = new XML(_arg1);
                _local6 = _local5.namespace();
                _local7 = _local5._local6::baseURL.text();
                if (_local7 != null){
                    this._url = _local7;
                } else {
                    throw (new Error("The baseURL was not specified neither via the baseURL parameter, nor via the manifest <baseURL> tag."));
                };
            };
            if (this._url != null){
                if (this._url.charAt((this._url.length - 1)) != "/"){
                    this._url = (this._url + "/");
                };
                this._url = (this._url + "manifest.f4m");
            };
            this._clipStartTime = _arg3;
            this._clipEndTime = _arg4;
        }
        public function get clipStartTime():Number{
            return (this._clipStartTime);
        }
        public function set clipStartTime(_arg1:Number):void{
            this._clipStartTime = _arg1;
        }
        public function get clipEndTime():Number{
            return (this._clipEndTime);
        }
        public function set clipEndTime(_arg1:Number):void{
            this._clipEndTime = _arg1;
        }
        public function get manifest():String{
            return (this._manifest);
        }
        public function get url():String{
            return (this._url);
        }

    }
}//package org.osmf.net 
