﻿package org.osmf.net {
    import org.osmf.traits.*;

    public class ModifiableTimeTrait extends TimeTrait {

        public function ModifiableTimeTrait(_arg1:Number=NaN){
            super(_arg1);
        }
        public function set duration(_arg1:Number):void{
            super.setDuration(_arg1);
        }

    }
}//package org.osmf.net 
