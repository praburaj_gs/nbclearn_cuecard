﻿package org.osmf.net.rules {
    import org.osmf.net.metrics.*;
    import flash.errors.*;

    public class RuleBase {

        protected var _metricRepository:MetricRepository;

        public function RuleBase(_arg1:MetricRepository){
            this._metricRepository = _arg1;
        }
        public function get metricRepository():MetricRepository{
            return (this._metricRepository);
        }
        public function getRecommendation():Recommendation{
            throw (new IllegalOperationError("The getRecommendation() method must be overridden by the derived class."));
        }

    }
}//package org.osmf.net.rules 
