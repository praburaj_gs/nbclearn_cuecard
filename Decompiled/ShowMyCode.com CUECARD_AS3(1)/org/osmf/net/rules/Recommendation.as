﻿package org.osmf.net.rules {

    public class Recommendation {

        private var _ruleType:String;
        private var _bitrate:Number;
        private var _confidence:Number;

        public function Recommendation(_arg1:String, _arg2:Number, _arg3:Number){
            if (_arg1 == null){
                throw (new ArgumentError("ruleType cannot be null."));
            };
            if (((((isNaN(_arg3)) || ((_arg3 < 0)))) || ((_arg3 > 1)))){
                throw (new ArgumentError("Invalid confidence!"));
            };
            if (((isNaN(_arg2)) || ((_arg2 < 0)))){
                throw (new ArgumentError("Invalid bitrate!"));
            };
            this._ruleType = _arg1;
            this._bitrate = _arg2;
            this._confidence = _arg3;
        }
        public function get ruleType():String{
            return (this._ruleType);
        }
        public function get bitrate():Number{
            return (this._bitrate);
        }
        public function get confidence():Number{
            return (this._confidence);
        }

    }
}//package org.osmf.net.rules 
