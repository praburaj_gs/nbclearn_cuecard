﻿package org.osmf.net.rules {
    import org.osmf.logging.*;
    import org.osmf.net.*;
    import org.osmf.net.metrics.*;

    public class EmptyBufferRule extends RuleBase {

        private static const logger:Logger = Log.getLogger("org.osmf.net.rules.EmptyBufferRule");

        private var _scaleDownFactor:Number;
        private var emptyBufferInterruptionMetric:MetricBase;
        private var actualBitrateMetric:MetricBase;
        private var currentStatusMetric:MetricBase;
        private var availableQualityLevelsMetric:MetricBase;

        public function EmptyBufferRule(_arg1:MetricRepository, _arg2:Number){
            super(_arg1);
            this.scaleDownFactor = _arg2;
        }
        public function get scaleDownFactor():Number{
            return (this._scaleDownFactor);
        }
        public function set scaleDownFactor(_arg1:Number):void{
            if (((((isNaN(_arg1)) || ((_arg1 < 0)))) || ((_arg1 > 1)))){
                throw (new ArgumentError("Invalid scaleDownFactor"));
            };
            this._scaleDownFactor = _arg1;
        }
        override public function getRecommendation():Recommendation{
            if (this.emptyBufferInterruptionMetric == null){
                this.emptyBufferInterruptionMetric = metricRepository.getMetric(MetricType.EMPTY_BUFFER);
            };
            var _local1:MetricValue = this.emptyBufferInterruptionMetric.value;
            if (((!(_local1.valid)) || ((_local1.value == false)))){
                logger.info(("The EmptyBuffer metric is invalid or reports no recent interruptions. " + "Returning a zero-confidence recommendation."));
                return (new Recommendation(RuleType.EMPTY_BUFFER, 0, 0));
            };
            if (this.actualBitrateMetric == null){
                this.actualBitrateMetric = metricRepository.getMetric(MetricType.ACTUAL_BITRATE);
            };
            if (this.availableQualityLevelsMetric == null){
                this.availableQualityLevelsMetric = metricRepository.getMetric(MetricType.AVAILABLE_QUALITY_LEVELS);
            };
            if (this.currentStatusMetric == null){
                this.currentStatusMetric = metricRepository.getMetric(MetricType.CURRENT_STATUS);
            };
            var _local2:Number = RuleUtils.computeActualBitrate(this.actualBitrateMetric, this.availableQualityLevelsMetric, this.currentStatusMetric);
            if (isNaN(_local2)){
                logger.info("An empty buffer playback interruption occurred, but some required metrics are invalid. Recommending switch to lowest quality.");
                return (new Recommendation(RuleType.EMPTY_BUFFER, 0, 1));
            };
            var _local3:Number = (_local2 * this.scaleDownFactor);
            logger.info((("Recommend: bitrate = " + ABRUtils.roundNumber(_local3)) + " kbps; confidence = 1"));
            return (new Recommendation(RuleType.EMPTY_BUFFER, _local3, 1));
        }

    }
}//package org.osmf.net.rules 
