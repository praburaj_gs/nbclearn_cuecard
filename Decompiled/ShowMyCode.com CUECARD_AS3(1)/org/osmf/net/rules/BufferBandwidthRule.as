﻿package org.osmf.net.rules {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.*;
    import org.osmf.net.metrics.*;

    public class BufferBandwidthRule extends BandwidthRule {

        private static const logger:Logger = Log.getLogger("org.osmf.net.rules.BufferBandwidthRule");

        private var _bufferFragmentsThreshold:Number;
        private var actualBitrateMetric:MetricBase;
        private var bufferFragmentsMetric:MetricBase;
        private var currentStatusMetric:MetricBase;
        private var availableQualityLevelsMetric:MetricBase;

        public function BufferBandwidthRule(_arg1:MetricRepository, _arg2:Vector.<Number>, _arg3:Number){
            super(_arg1, _arg2);
            this.bufferFragmentsThreshold = _arg3;
        }
        public function get bufferFragmentsThreshold():Number{
            return (this._bufferFragmentsThreshold);
        }
        public function set bufferFragmentsThreshold(_arg1:Number):void{
            if (((isNaN(_arg1)) || ((_arg1 < 0)))){
                throw (new ArgumentError("Invalid bufferLengthThreshold"));
            };
            this._bufferFragmentsThreshold = _arg1;
        }
        override public function getRecommendation():Recommendation{
            var _local4:Number;
            var _local5:MetricValue;
            var _local6:Number;
            var _local1:Recommendation = super.getRecommendation();
            var _local2:Number = _local1.bitrate;
            var _local3:Number = _local1.confidence;
            _local1 = null;
            if (_local3 > 0){
                if (this.actualBitrateMetric == null){
                    this.actualBitrateMetric = metricRepository.getMetric(MetricType.ACTUAL_BITRATE);
                };
                if (this.availableQualityLevelsMetric == null){
                    this.availableQualityLevelsMetric = metricRepository.getMetric(MetricType.AVAILABLE_QUALITY_LEVELS);
                };
                if (this.currentStatusMetric == null){
                    this.currentStatusMetric = metricRepository.getMetric(MetricType.CURRENT_STATUS);
                };
                _local4 = RuleUtils.computeActualBitrate(this.actualBitrateMetric, this.availableQualityLevelsMetric, this.currentStatusMetric);
                if (isNaN(_local4)){
                    return (new Recommendation(RuleType.BUFFER_BANDWIDTH, 0, 0));
                };
                if (_local2 < _local4){
                    if (this.bufferFragmentsMetric == null){
                        this.bufferFragmentsMetric = metricRepository.getMetric(MetricType.BUFFER_FRAGMENTS);
                    };
                    _local5 = this.bufferFragmentsMetric.value;
                    if (_local5.valid){
                        _local6 = _local5.value;
                        if (_local6 > this._bufferFragmentsThreshold){
                            logger.info(((((("There are more fragments in the buffer (" + _local6) + ") than required by the threshold (") + this._bufferFragmentsThreshold) + "). Recommending actual bitrate: ") + _local4));
                            _local2 = _local4;
                        };
                    };
                };
            };
            logger.info(((("Recommend: bitrate = " + ABRUtils.roundNumber(_local2)) + "; confidence = ") + ABRUtils.roundNumber(_local3)));
            return (new Recommendation(RuleType.BUFFER_BANDWIDTH, _local2, _local3));
        }

    }
}//package org.osmf.net.rules 
