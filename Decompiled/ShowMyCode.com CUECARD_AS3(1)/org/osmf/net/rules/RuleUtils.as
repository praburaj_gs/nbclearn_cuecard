﻿package org.osmf.net.rules {
    import __AS3__.vec.*;
    import org.osmf.net.qos.*;
    import org.osmf.net.metrics.*;

    public class RuleUtils {

        public static function computeActualBitrate(_arg1:MetricBase, _arg2:MetricBase, _arg3:MetricBase):Number{
            var _local5:MetricValue;
            var _local6:MetricValue;
            var _local7:uint;
            var _local8:Vector.<QualityLevel>;
            var _local4:MetricValue = _arg1.value;
            if (_local4.valid){
                return ((_local4.value as Number));
            };
            _local5 = _arg2.value;
            _local6 = _arg3.value;
            if (((!(_local5.valid)) || (!(_local6.valid)))){
                return (Number.NaN);
            };
            _local7 = (_local6.value as Vector.<uint>)[1];
            _local8 = (_local5.value as Vector.<QualityLevel>);
            return (_local8[_local7].bitrate);
        }

    }
}//package org.osmf.net.rules 
