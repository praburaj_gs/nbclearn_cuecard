﻿package org.osmf.net.rules {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.*;
    import org.osmf.net.metrics.*;

    public class AfterUpSwitchBufferBandwidthRule extends BufferBandwidthRule {

        private static const logger:Logger = Log.getLogger("org.osmf.net.rules.AfterUpSwitchBufferBandwidthRule");

        private var _minBandwidthToBitrateRatio:Number;
        private var upSwitchMetric:MetricBase;
        private var actualBitrateMetric:MetricBase;
        private var availableQualityLevelsMetric:MetricBase;
        private var currentStatusMetric:MetricBase;

        public function AfterUpSwitchBufferBandwidthRule(_arg1:MetricRepository, _arg2:Number, _arg3:Number){
            super(_arg1, new <Number>[1], _arg2);
            this.minBandwidthToBitrateRatio = _arg3;
        }
        public function get minBandwidthToBitrateRatio():Number{
            return (this._minBandwidthToBitrateRatio);
        }
        public function set minBandwidthToBitrateRatio(_arg1:Number):void{
            if (((isNaN(_arg1)) || ((_arg1 < 0)))){
                throw (new ArgumentError("Invalid value for minBandwidthToBitrateRatio"));
            };
            this._minBandwidthToBitrateRatio = _arg1;
        }
        override public function getRecommendation():Recommendation{
            if (this.upSwitchMetric == null){
                this.upSwitchMetric = metricRepository.getMetric(MetricType.RECENT_SWITCH);
            };
            var _local1:MetricValue = this.upSwitchMetric.value;
            if (((!(_local1.valid)) || (((_local1.value as int) <= 0)))){
                logger.info("No up-switch is reported to have occurred recently so return a zero-confidence recommendation.");
                return (new Recommendation(RuleType.AFTER_UP_SWITCH_BUFFER_BANDWIDTH, 0, 0));
            };
            var _local2:Recommendation = super.getRecommendation();
            if (_local2.confidence == 0){
                logger.info("The BandwidthBufferRule returns a zero-confidence recommendations. In this case, we do so as well.");
                return (new Recommendation(RuleType.AFTER_UP_SWITCH_BUFFER_BANDWIDTH, 0, 0));
            };
            var _local3:Number = _local2.bitrate;
            if (this.actualBitrateMetric == null){
                this.actualBitrateMetric = metricRepository.getMetric(MetricType.ACTUAL_BITRATE);
            };
            if (this.availableQualityLevelsMetric == null){
                this.availableQualityLevelsMetric = metricRepository.getMetric(MetricType.AVAILABLE_QUALITY_LEVELS);
            };
            if (this.currentStatusMetric == null){
                this.currentStatusMetric = metricRepository.getMetric(MetricType.CURRENT_STATUS);
            };
            var _local4:Number = RuleUtils.computeActualBitrate(this.actualBitrateMetric, this.availableQualityLevelsMetric, this.currentStatusMetric);
            if (isNaN(_local4)){
                return (new Recommendation(RuleType.AFTER_UP_SWITCH_BUFFER_BANDWIDTH, 0, 0));
            };
            var _local5:Number = 0;
            if ((_local3 / _local4) < this.minBandwidthToBitrateRatio){
                _local5 = 1;
            };
            logger.info(((("Recommend: bitrate = " + ABRUtils.roundNumber(_local3)) + " kbps; confidence = ") + ABRUtils.roundNumber(_local5)));
            return (new Recommendation(RuleType.AFTER_UP_SWITCH_BUFFER_BANDWIDTH, _local3, _local5));
        }

    }
}//package org.osmf.net.rules 
