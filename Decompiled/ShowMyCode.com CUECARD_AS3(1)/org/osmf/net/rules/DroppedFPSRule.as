﻿package org.osmf.net.rules {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.*;
    import org.osmf.net.metrics.*;

    public class DroppedFPSRule extends RuleBase {

        private static const logger:Logger = Log.getLogger("org.osmf.net.rules.DroppedFPSRule");

        private var _minimumFPS:Number;
        private var _maximumDroppedFPSRatio:Number;
        private var _desiredSampleLength:Number;
        private var actualBitrateMetric:MetricBase;
        private var currentStatusMetric:MetricBase;
        private var availableQualityLevelsMetric:MetricBase;
        private var fpsMetric:MetricBase;
        private var droppedFPSMetric:MetricBase;

        public function DroppedFPSRule(_arg1:MetricRepository, _arg2:Number, _arg3:Number){
            super(_arg1);
            this.desiredSampleLength = _arg2;
            this.maximumDroppedFPSRatio = _arg3;
        }
        public function get desiredSampleLength():Number{
            return (this._desiredSampleLength);
        }
        public function set desiredSampleLength(_arg1:Number):void{
            if (((isNaN(_arg1)) || ((_arg1 <= 0)))){
                throw (new ArgumentError("Invalid desiredSampleLength"));
            };
            this._desiredSampleLength = _arg1;
        }
        public function get maximumDroppedFPSRatio():Number{
            return (this._maximumDroppedFPSRatio);
        }
        public function set maximumDroppedFPSRatio(_arg1:Number):void{
            if (((((isNaN(_arg1)) || ((_arg1 < 0)))) || ((_arg1 > 1)))){
                throw (new ArgumentError("Invalid maximumDroppedFPSRatio"));
            };
            this._maximumDroppedFPSRatio = _arg1;
        }
        override public function getRecommendation():Recommendation{
            if (this.actualBitrateMetric == null){
                this.actualBitrateMetric = metricRepository.getMetric(MetricType.ACTUAL_BITRATE);
            };
            if (this.currentStatusMetric == null){
                this.currentStatusMetric = metricRepository.getMetric(MetricType.CURRENT_STATUS);
            };
            var _local1:MetricValue = this.actualBitrateMetric.value;
            var _local2:MetricValue = this.currentStatusMetric.value;
            if (!_local2.valid){
                logger.info("One of the required metrics is not valid so return a zero-confidence recommendation.");
                return (new Recommendation(RuleType.DROPPED_FPS, 0, 0));
            };
            var _local3:uint = (_local2.value as Vector.<uint>)[0];
            var _local4:uint = (_local2.value as Vector.<uint>)[1];
            if (_local3 != _local4){
                logger.info((((((("The current index (" + _local3) + ") is different than the ") + "actual index (") + _local4) + "), so the rule does not apply. ") + "Returning a zero-confidence recommendation."));
                return (new Recommendation(RuleType.DROPPED_FPS, 0, 0));
            };
            if (this.availableQualityLevelsMetric == null){
                this.availableQualityLevelsMetric = metricRepository.getMetric(MetricType.AVAILABLE_QUALITY_LEVELS);
            };
            var _local5:Number = RuleUtils.computeActualBitrate(this.actualBitrateMetric, this.availableQualityLevelsMetric, this.currentStatusMetric);
            if (isNaN(_local5)){
                return (new Recommendation(RuleType.DROPPED_FPS, 0, 0));
            };
            if (this.fpsMetric == null){
                this.fpsMetric = metricRepository.getMetric(MetricType.FPS);
            };
            if (this.droppedFPSMetric == null){
                this.droppedFPSMetric = metricRepository.getMetric(MetricType.DROPPED_FPS, this._desiredSampleLength);
            };
            var _local6:MetricValue = this.fpsMetric.value;
            var _local7:MetricValue = this.droppedFPSMetric.value;
            if (((!(_local6.valid)) || (!(_local7.valid)))){
                logger.info("One of the required metrics is not valid so return a zero-confidence recommendation.");
                return (new Recommendation(RuleType.DROPPED_FPS, 0, 0));
            };
            var _local8:Number = _local7.value;
            var _local9:Number = _local6.value;
            if (_local8 > _local9){
                _local8 = _local9;
            };
            var _local10:Number = (_local5 * (1 - (_local8 / _local9)));
            var _local11:Number = 0;
            if ((((this._maximumDroppedFPSRatio == 0)) && ((_local8 == 0)))){
                logger.info(("The maximumDroppedFPSRatio is 0 and we had no dropped frames. " + "Return a zero-confidence recommendation."));
                return (new Recommendation(RuleType.DROPPED_FPS, 0, 0));
            };
            if ((_local8 / _local9) > this._maximumDroppedFPSRatio){
                _local11 = 1;
            } else {
                _local11 = (_local8 / (_local9 * this._maximumDroppedFPSRatio));
            };
            logger.info(((("Recommend: bitrate = " + ABRUtils.roundNumber(_local10)) + " kbps; confidence = ") + ABRUtils.roundNumber(_local11)));
            return (new Recommendation(RuleType.DROPPED_FPS, _local10, _local11));
        }

    }
}//package org.osmf.net.rules 
