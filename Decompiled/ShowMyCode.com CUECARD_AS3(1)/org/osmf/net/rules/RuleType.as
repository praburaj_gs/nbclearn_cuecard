﻿package org.osmf.net.rules {

    public final class RuleType {

        public static const BANDWIDTH:String = "org.osmf.net.rules.bandwidth";
        public static const DROPPED_FPS:String = "org.osmf.net.rules.droppedFPS";
        public static const BUFFER_BANDWIDTH:String = "org.osmf.net.rules.bufferBandwidth";
        public static const EMPTY_BUFFER:String = "org.osmf.net.rules.emptyBuffer";
        public static const AFTER_UP_SWITCH_BUFFER_BANDWIDTH:String = "org.osmf.net.rules.afterUpSwitchBufferBandwidth";

    }
}//package org.osmf.net.rules 
