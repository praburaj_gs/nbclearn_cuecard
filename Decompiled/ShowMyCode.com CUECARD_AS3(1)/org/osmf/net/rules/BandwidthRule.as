﻿package org.osmf.net.rules {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.*;
    import org.osmf.net.metrics.*;

    public class BandwidthRule extends RuleBase {

        private static const logger:Logger = Log.getLogger("org.osmf.net.rules.BandwidthRule");

        private var _weights:Vector.<Number>;
        private var bandwidthMetric:MetricBase;
        private var fragmentCountMetric:MetricBase;

        public function BandwidthRule(_arg1:MetricRepository, _arg2:Vector.<Number>){
            super(_arg1);
            ABRUtils.validateWeights(_arg2);
            this._weights = _arg2.slice();
        }
        public function get weights():Vector.<Number>{
            return (this._weights);
        }
        override public function getRecommendation():Recommendation{
            if (this.bandwidthMetric == null){
                this.bandwidthMetric = _metricRepository.getMetric(MetricType.BANDWIDTH, this._weights);
            };
            if (this.fragmentCountMetric == null){
                this.fragmentCountMetric = _metricRepository.getMetric(MetricType.FRAGMENT_COUNT);
            };
            if (((!(this.bandwidthMetric.value.valid)) || (!(this.fragmentCountMetric.value.valid)))){
                logger.info("One of the required metrics is not valid so return a zero-confidence recommendation.");
                return (new Recommendation(RuleType.BANDWIDTH, 0, 0));
            };
            var _local1:Number = (((this.bandwidthMetric.value.value as Number) / 1000) * 8);
            logger.info((("Computed bitrate: " + ABRUtils.roundNumber(_local1)) + " kbps"));
            var _local2:uint = (this.fragmentCountMetric.value.value as uint);
            if (_local2 > this._weights.length){
                _local2 = this._weights.length;
            };
            var _local3:Number = 0;
            var _local4:Number = 0;
            var _local5:uint;
            while (_local5 < this._weights.length) {
                _local4 = (_local4 + this._weights[_local5]);
                if (_local5 < _local2){
                    _local3 = (_local3 + this._weights[_local5]);
                };
                _local5++;
            };
            _local3 = (_local3 / _local4);
            logger.info(((("Recommend: bitrate = " + ABRUtils.roundNumber(_local1)) + " kbps; confidence = ") + ABRUtils.roundNumber(_local3)));
            return (new Recommendation(RuleType.BANDWIDTH, _local1, _local3));
        }

    }
}//package org.osmf.net.rules 
