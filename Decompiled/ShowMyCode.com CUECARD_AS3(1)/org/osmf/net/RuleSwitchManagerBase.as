﻿package org.osmf.net {
    import flash.events.*;
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.events.*;
    import org.osmf.net.metrics.*;
    import org.osmf.net.rules.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class RuleSwitchManagerBase extends NetStreamSwitchManagerBase {

        private static const logger:Logger = Log.getLogger("org.osmf.net.RuleSwitchManagerBase");

        private var _metricRepository:MetricRepository;
        private var _emergencyRules:Vector.<RuleBase> = null;
        private var switcher:NetStreamSwitcher;
        private var notifier:EventDispatcher;

        public function RuleSwitchManagerBase(_arg1:EventDispatcher, _arg2:NetStreamSwitcher, _arg3:MetricRepository, _arg4:Vector.<RuleBase>=null, _arg5:Boolean=true){
            if (_arg1 == null){
                throw (new ArgumentError("Invalid netStream"));
            };
            if (_arg2 == null){
                throw (new ArgumentError("Invalid switcher"));
            };
            if (_arg3 == null){
                throw (new ArgumentError("Invalid metric repository"));
            };
            this.notifier = _arg1;
            this.switcher = _arg2;
            this._metricRepository = _arg3;
            if (_arg4 != null){
                this._emergencyRules = _arg4.slice();
            };
            this.autoSwitch = _arg5;
        }
        override public function set autoSwitch(_arg1:Boolean):void{
            super.autoSwitch = _arg1;
            if (_arg1){
                this.notifier.addEventListener(HTTPStreamingEvent.RUN_ALGORITHM, this.onRunAlgorithm);
            } else {
                this.notifier.removeEventListener(HTTPStreamingEvent.RUN_ALGORITHM, this.onRunAlgorithm);
            };
        }
        public function get actualIndex():int{
            return (this.switcher.actualIndex);
        }
        public function get metricRepository():MetricRepository{
            return (this._metricRepository);
        }
        public function get emergencyRules():Vector.<RuleBase>{
            return (this._emergencyRules);
        }
        public function getNewIndex():uint{
            throw (new IllegalOperationError("The getNewIndex() function must be overriden by the subclass."));
        }
        public function getNewEmergencyIndex(_arg1:Number):uint{
            throw (new IllegalOperationError("The getNewEmergencyIndex() function must be overriden by the subclass."));
        }
        override public function get currentIndex():uint{
            return (this.switcher.currentIndex);
        }
        override public function switchTo(_arg1:int):void{
            if (!_autoSwitch){
                if ((((_arg1 < 0)) || ((_arg1 > maxAllowedIndex)))){
                    throw (new RangeError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_INVALID_INDEX)));
                };
                logger.debug(("switchTo() - manually switching to index: " + _arg1));
                this.switcher.switchTo(_arg1);
            } else {
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_STREAM_NOT_IN_MANUAL_MODE)));
            };
        }
        private function onRunAlgorithm(_arg1:HTTPStreamingEvent):void{
            var _local3:RuleBase;
            var _local4:uint;
            var _local5:Recommendation;
            var _local2:Number = Number.POSITIVE_INFINITY;
            for each (_local3 in this._emergencyRules) {
                _local5 = _local3.getRecommendation();
                if (_local5.confidence == 1){
                    _local2 = _local5.bitrate;
                };
            };
            _local4 = 0;
            if (_local2 < Number.POSITIVE_INFINITY){
                _local4 = this.getNewEmergencyIndex(_local2);
            } else {
                _local4 = this.getNewIndex();
            };
            if (_local4 != this.switcher.actualIndex){
                _local4 = Math.min(_local4, maxAllowedIndex);
            } else {
                if (this.switcher.actualIndex > maxAllowedIndex){
                    _local4 = maxAllowedIndex;
                };
            };
            if (((!((_local4 == this.switcher.actualIndex))) && (!(this.switcher.switching)))){
                logger.info(("Automatically switching to new index: " + _local4));
                this.switcher.switchTo(_local4);
            };
        }

    }
}//package org.osmf.net 
