﻿package org.osmf.net {
    import flash.events.*;
    import flash.net.*;
    import org.osmf.traits.*;
    import org.osmf.utils.*;

    public class NetStreamDynamicStreamTrait extends DynamicStreamTrait {

        private var netStream:NetStream;
        private var switchManager:NetStreamSwitchManagerBase;
        private var inSetSwitching:Boolean;
        private var dsResource:DynamicStreamingResource;
        private var indexToSwitchTo:int;
        private var index:int;

        public function NetStreamDynamicStreamTrait(_arg1:NetStream, _arg2:NetStreamSwitchManagerBase, _arg3:DynamicStreamingResource){
            super(_arg2.autoSwitch, _arg2.currentIndex, _arg3.streamItems.length);
            this.netStream = _arg1;
            this.switchManager = _arg2;
            this.dsResource = _arg3;
            _arg1.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus, false, int.MAX_VALUE);
            NetClient(_arg1.client).addHandler(NetStreamCodes.ON_PLAY_STATUS, this.onPlayStatus);
        }
        override public function dispose():void{
            this.netStream = null;
            this.switchManager = null;
        }
        override public function getBitrateForIndex(_arg1:int):Number{
            if ((((_arg1 > (numDynamicStreams - 1))) || ((_arg1 < 0)))){
                throw (new RangeError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_INVALID_INDEX)));
            };
            return (this.dsResource.streamItems[_arg1].bitrate);
        }
        override protected function switchingChangeStart(_arg1:Boolean, _arg2:int):void{
            if (((_arg1) && (!(this.inSetSwitching)))){
                this.indexToSwitchTo = _arg2;
            };
        }
        override protected function switchingChangeEnd(_arg1:int):void{
            var _local2:int;
            if (((!(switching)) || (this.inSetSwitching))){
                super.switchingChangeEnd(_arg1);
            } else {
                _local2 = -1;
                if (this.switchManager.hasOwnProperty("actualIndex")){
                    _local2 = this.switchManager["actualIndex"];
                };
                if (this.indexToSwitchTo != _local2){
                    this.switchManager.switchTo(this.indexToSwitchTo);
                } else {
                    setSwitching(false, this.indexToSwitchTo);
                };
            };
        }
        override protected function autoSwitchChangeStart(_arg1:Boolean):void{
            this.switchManager.autoSwitch = _arg1;
        }
        override protected function maxAllowedIndexChangeStart(_arg1:int):void{
            this.switchManager.maxAllowedIndex = _arg1;
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            var _local2:int;
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_PLAY_START:
                    if (_arg1.info.details){
                        this.index = this.dsResource.indexFromName(_arg1.info.details);
                        if (this.index != this.currentIndex){
                            this.inSetSwitching = true;
                            setSwitching(true, this.index);
                            this.inSetSwitching = false;
                        };
                        setSwitching(false, this.index);
                    };
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_TRANSITION:
                    this.index = this.dsResource.indexFromName(_arg1.info.details);
                    if (this.index >= 0){
                        _local2 = -1;
                        if (this.switchManager.hasOwnProperty("actualIndex")){
                            _local2 = this.switchManager["actualIndex"];
                        };
                        if (this.index != _local2){
                            this.inSetSwitching = true;
                            setSwitching(true, this.index);
                            this.inSetSwitching = false;
                        };
                    };
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_FAILED:
                    setSwitching(false, currentIndex);
                    break;
            };
        }
        private function onPlayStatus(_arg1:Object):void{
            switch (_arg1.code){
                case NetStreamCodes.NETSTREAM_PLAY_TRANSITION_COMPLETE:
                    if (((!(_arg1.hasOwnProperty("details"))) || ((this.dsResource.indexFromName(_arg1.details) >= 0)))){
                        setSwitching(false, this.switchManager.currentIndex);
                    };
                    break;
            };
        }

    }
}//package org.osmf.net 
