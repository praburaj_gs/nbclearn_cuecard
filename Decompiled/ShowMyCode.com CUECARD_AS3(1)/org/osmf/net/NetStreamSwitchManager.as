﻿package org.osmf.net {
    import flash.events.*;
    import __AS3__.vec.*;
    import flash.net.*;
    import flash.utils.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class NetStreamSwitchManager extends NetStreamSwitchManagerBase {

        private static const RULE_CHECK_INTERVAL:Number = 500;
        private static const DEFAULT_MAX_UP_SWITCHES_PER_STREAM_ITEM:int = 3;
        private static const DEFAULT_WAIT_DURATION_AFTER_DOWN_SWITCH:int = 30000;
        private static const DEFAULT_CLEAR_FAILED_COUNTS_INTERVAL:Number = 300000;

        private var netStream:NetStream;
        private var dsResource:DynamicStreamingResource;
        private var switchingRules:Vector.<SwitchingRuleBase>;
        private var metrics:NetStreamMetricsBase;
        private var checkRulesTimer:Timer;
        private var clearFailedCountsTimer:Timer;
        private var actualIndex:int = -1;
        private var oldStreamName:String;
        private var switching:Boolean;
        private var _currentIndex:int;
        private var lastTransitionIndex:int = -1;
        private var connection:NetConnection;
        private var dsiFailedCounts:Vector.<int>;
        private var failedDSI:Dictionary;
        private var _bandwidthLimit:Number = 0;

        public function NetStreamSwitchManager(_arg1:NetConnection, _arg2:NetStream, _arg3:DynamicStreamingResource, _arg4:NetStreamMetricsBase, _arg5:Vector.<SwitchingRuleBase>, _arg6:Boolean=true){
            this.connection = _arg1;
            this.netStream = _arg2;
            this.dsResource = _arg3;
            this.metrics = _arg4;
            _arg4.resource = _arg3;
            this.switchingRules = ((_arg5) || (new Vector.<SwitchingRuleBase>()));
            this._currentIndex = Math.max(0, Math.min(this.maxAllowedIndex, this.dsResource.initialIndex));
            this.checkRulesTimer = new Timer(RULE_CHECK_INTERVAL);
            this.checkRulesTimer.addEventListener(TimerEvent.TIMER, this.checkRules);
            super.autoSwitch = _arg6;
            this.failedDSI = new Dictionary();
            this.initDSIFailedCounts();
            this._bandwidthLimit = (((1.4 * _arg3.streamItems[(_arg3.streamItems.length - 1)].bitrate) * 1000) / 8);
            _arg2.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus);
            NetClient(_arg2.client).addHandler(NetStreamCodes.ON_PLAY_STATUS, this.onPlayStatus, int.MAX_VALUE);
        }
        override public function set autoSwitch(_arg1:Boolean):void{
            super.autoSwitch = _arg1;
            if (_autoSwitch){
                this.checkRulesTimer.start();
            } else {
                this.checkRulesTimer.stop();
            };
        }
        override public function get currentIndex():uint{
            return (this._currentIndex);
        }
        override public function get maxAllowedIndex():int{
            var _local1:int = (this.dsResource.streamItems.length - 1);
            return ((((_local1 < super.maxAllowedIndex)) ? _local1 : super.maxAllowedIndex));
        }
        override public function set maxAllowedIndex(_arg1:int):void{
            if (_arg1 > this.dsResource.streamItems.length){
                throw (new RangeError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_INVALID_INDEX)));
            };
            super.maxAllowedIndex = _arg1;
            this.metrics.maxAllowedIndex = _arg1;
        }
        override public function switchTo(_arg1:int):void{
            if (!_autoSwitch){
                if ((((_arg1 < 0)) || ((_arg1 > this.maxAllowedIndex)))){
                    throw (new RangeError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_INVALID_INDEX)));
                };
                if (this.actualIndex == -1){
                    this.prepareForSwitching();
                };
                this.executeSwitch(_arg1);
            } else {
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_STREAM_NOT_IN_MANUAL_MODE)));
            };
        }
        protected function canAutoSwitchNow(_arg1:int):Boolean{
            var _local2:int;
            if (this.dsiFailedCounts[_arg1] >= 1){
                _local2 = getTimer();
                if ((_local2 - this.failedDSI[_arg1]) < DEFAULT_WAIT_DURATION_AFTER_DOWN_SWITCH){
                    return (false);
                };
            } else {
                if (this.dsiFailedCounts[_arg1] > DEFAULT_MAX_UP_SWITCHES_PER_STREAM_ITEM){
                    return (false);
                };
            };
            return (true);
        }
        final protected function get bandwidthLimit():Number{
            return (this._bandwidthLimit);
        }
        final protected function set bandwidthLimit(_arg1:Number):void{
            this._bandwidthLimit = _arg1;
        }
        protected function doCheckRules():void{
            this.checkRules(null);
        }
        private function executeSwitch(_arg1:int):void{
            var _local2:NetStreamPlayOptions = new NetStreamPlayOptions();
            var _local3:Object = NetStreamUtils.getPlayArgsForResource(this.dsResource);
            _local2.start = _local3.start;
            _local2.len = _local3.len;
            _local2.streamName = this.dsResource.streamItems[_arg1].streamName;
            var _local4:String = this.oldStreamName;
            if (((!((_local4 == null))) && ((_local4.indexOf("?") >= 0)))){
                _local2.oldStreamName = _local4.substr(0, _local4.indexOf("?"));
            } else {
                _local2.oldStreamName = this.oldStreamName;
            };
            _local2.transition = NetStreamPlayTransitions.SWITCH;
            this.switching = true;
            this.netStream.play2(_local2);
            this.oldStreamName = this.dsResource.streamItems[_arg1].streamName;
            if ((((_arg1 < this.actualIndex)) && (_autoSwitch))){
                this.incrementDSIFailedCount(this.actualIndex);
                this.failedDSI[this.actualIndex] = getTimer();
            };
        }
        private function checkRules(_arg1:TimerEvent):void{
            var _local4:int;
            if ((((((this.switchingRules == null)) || (this.switching))) || ((this.dsResource == null)))){
                return;
            };
            var _local2:int = int.MAX_VALUE;
            var _local3:int;
            while (_local3 < this.switchingRules.length) {
                _local4 = this.switchingRules[_local3].getNewIndex();
                if (((!((_local4 == -1))) && ((_local4 < _local2)))){
                    _local2 = _local4;
                };
                _local3++;
            };
            if (((((!((_local2 == -1))) && (!((_local2 == int.MAX_VALUE))))) && (!((_local2 == this.actualIndex))))){
                _local2 = Math.min(_local2, this.maxAllowedIndex);
            };
            if ((((((_local2 == -1)) || ((_local2 == int.MAX_VALUE)))) && ((this.actualIndex > this.maxAllowedIndex)))){
                _local2 = this.maxAllowedIndex;
            };
            if (((((((((!((_local2 == -1))) && (!((_local2 == int.MAX_VALUE))))) && (!((_local2 == this.actualIndex))))) && (!(this.switching)))) && ((_local2 <= this.maxAllowedIndex)))){
                if (this.actualIndex == -1){
                    this.prepareForSwitching();
                };
                if (this.canAutoSwitchNow(_local2)){
                    this.executeSwitch(_local2);
                };
            };
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_PLAY_START:
                    if (this.actualIndex == -1){
                        this.prepareForSwitching();
                    } else {
                        if (((_autoSwitch) && ((this.checkRulesTimer.running == false)))){
                            this.checkRulesTimer.start();
                        };
                    };
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_TRANSITION:
                    this.actualIndex = this.dsResource.indexFromName(_arg1.info.details);
                    if (this.actualIndex > -1){
                        this.switching = false;
                        this.metrics.currentIndex = this.actualIndex;
                        this.lastTransitionIndex = this.actualIndex;
                    };
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_FAILED:
                    this.switching = false;
                    break;
                case NetStreamCodes.NETSTREAM_SEEK_NOTIFY:
                    this.switching = false;
                    if (this.lastTransitionIndex >= 0){
                        this._currentIndex = this.lastTransitionIndex;
                    };
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_STOP:
                    this.checkRulesTimer.stop();
                    break;
            };
        }
        private function onPlayStatus(_arg1:Object):void{
            switch (_arg1.code){
                case NetStreamCodes.NETSTREAM_PLAY_TRANSITION_COMPLETE:
                    if (this.lastTransitionIndex >= 0){
                        this._currentIndex = this.lastTransitionIndex;
                        this.lastTransitionIndex = -1;
                    };
                    break;
            };
        }
        private function prepareForSwitching():void{
            this.initDSIFailedCounts();
            this.metrics.resource = this.dsResource;
            this.actualIndex = 0;
            this.lastTransitionIndex = -1;
            if ((((this.dsResource.initialIndex >= 0)) && ((this.dsResource.initialIndex < this.dsResource.streamItems.length)))){
                this.actualIndex = this.dsResource.initialIndex;
            };
            if (_autoSwitch){
                this.checkRulesTimer.start();
            };
            this.setThrottleLimits((this.dsResource.streamItems.length - 1));
            this.metrics.currentIndex = this.actualIndex;
        }
        private function initDSIFailedCounts():void{
            if (this.dsiFailedCounts != null){
                this.dsiFailedCounts.length = 0;
                this.dsiFailedCounts = null;
            };
            this.dsiFailedCounts = new Vector.<int>();
            var _local1:int;
            while (_local1 < this.dsResource.streamItems.length) {
                this.dsiFailedCounts.push(0);
                _local1++;
            };
        }
        private function incrementDSIFailedCount(_arg1:int):void{
            var _local2 = this.dsiFailedCounts;
            var _local3 = _arg1;
            var _local4 = (_local2[_local3] + 1);
            _local2[_local3] = _local4;
            if (this.dsiFailedCounts[_arg1] > DEFAULT_MAX_UP_SWITCHES_PER_STREAM_ITEM){
                if (this.clearFailedCountsTimer == null){
                    this.clearFailedCountsTimer = new Timer(DEFAULT_CLEAR_FAILED_COUNTS_INTERVAL, 1);
                    this.clearFailedCountsTimer.addEventListener(TimerEvent.TIMER, this.clearFailedCounts);
                };
                this.clearFailedCountsTimer.start();
            };
        }
        private function clearFailedCounts(_arg1:TimerEvent):void{
            this.clearFailedCountsTimer.removeEventListener(TimerEvent.TIMER, this.clearFailedCounts);
            this.clearFailedCountsTimer = null;
            this.initDSIFailedCounts();
        }
        private function setThrottleLimits(_arg1:int):void{
            this.connection.call("setBandwidthLimit", null, this._bandwidthLimit, this._bandwidthLimit);
        }

    }
}//package org.osmf.net 
