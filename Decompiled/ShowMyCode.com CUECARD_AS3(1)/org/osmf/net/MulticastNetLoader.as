﻿package org.osmf.net {
    import flash.events.*;
    import org.osmf.media.*;
    import flash.net.*;
    import org.osmf.traits.*;

    public class MulticastNetLoader extends NetLoader {

        public function MulticastNetLoader(_arg1:NetConnectionFactoryBase=null){
            var _local2:NetConnectionFactory;
            if (_arg1 == null){
                _local2 = new NetConnectionFactory();
                _local2.timeout = 60000;
            };
            super(((_arg1)!=null) ? _arg1 : _local2);
        }
        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            var _local2:MulticastResource = (_arg1 as MulticastResource);
            return (((((((((!((_local2 == null))) && (!((_local2.groupspec == null))))) && ((_local2.groupspec.length > 0)))) && (!((_local2.streamName == null))))) && ((_local2.streamName.length > 0))));
        }
        override protected function createNetStream(_arg1:NetConnection, _arg2:URLResource):NetStream{
            var _local3:MulticastResource = (_arg2 as MulticastResource);
            var _local4:NetStream = new NetStream(_arg1, _local3.groupspec);
            return (_local4);
        }
        private function doProcessCreationComplete(_arg1:NetConnection, _arg2:LoadTrait, _arg3:NetConnectionFactoryBase=null):void{
            super.processCreationComplete(_arg1, _arg2, _arg3);
        }
        override protected function processCreationComplete(_arg1:NetConnection, _arg2:LoadTrait, _arg3:NetConnectionFactoryBase=null):void{
            var netLoadTrait:* = null;
            var netGroup:* = null;
            var onNetStatus:* = null;
            var connection:* = _arg1;
            var loadTrait:* = _arg2;
            var factory = _arg3;
            onNetStatus = function (_arg1:NetStatusEvent):void{
                switch (_arg1.info.code){
                    case "NetGroup.Connect.Success":
                        connection.removeEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
                        netLoadTrait.netGroup = netGroup;
                        doProcessCreationComplete(connection, loadTrait, factory);
                        break;
                    case "NetGroup.Connect.Failed":
                    case "NetGroup.Connect.Rejected":
                        connection.removeEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
                        updateLoadTrait(loadTrait, LoadState.LOAD_ERROR);
                        break;
                };
            };
            netLoadTrait = (loadTrait as NetStreamLoadTrait);
            var multicastResource:* = (netLoadTrait.resource as MulticastResource);
            connection.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
            netGroup = new NetGroup(connection, multicastResource.groupspec);
        }

    }
}//package org.osmf.net 
