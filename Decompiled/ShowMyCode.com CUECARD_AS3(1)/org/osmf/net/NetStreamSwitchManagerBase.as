﻿package org.osmf.net {
    import flash.events.*;

    public class NetStreamSwitchManagerBase extends EventDispatcher {

        protected var _autoSwitch:Boolean;
        protected var _maxAllowedIndex:int;

        public function NetStreamSwitchManagerBase(){
            this._autoSwitch = true;
            this._maxAllowedIndex = int.MAX_VALUE;
        }
        public function get autoSwitch():Boolean{
            return (this._autoSwitch);
        }
        public function set autoSwitch(_arg1:Boolean):void{
            this._autoSwitch = _arg1;
        }
        public function get currentIndex():uint{
            return (0);
        }
        public function get maxAllowedIndex():int{
            return (this._maxAllowedIndex);
        }
        public function set maxAllowedIndex(_arg1:int):void{
            this._maxAllowedIndex = _arg1;
        }
        public function switchTo(_arg1:int):void{
        }

    }
}//package org.osmf.net 
