﻿package org.osmf.net {
    import flash.events.*;
    import flash.display.*;
    import flash.net.*;
    import org.osmf.traits.*;
    import org.osmf.media.videoClasses.*;

    public class NetStreamDisplayObjectTrait extends DisplayObjectTrait {

        private var videoSurface:VideoSurface;
        private var netStream:NetStream;

        public function NetStreamDisplayObjectTrait(_arg1:NetStream, _arg2:DisplayObject, _arg3:Number=0, _arg4:Number=0){
            super(_arg2, _arg3, _arg4);
            this.netStream = _arg1;
            this.videoSurface = (_arg2 as VideoSurface);
            NetClient(_arg1.client).addHandler(NetStreamCodes.ON_META_DATA, this.onMetaData);
            if ((this.videoSurface is VideoSurface)){
                this.videoSurface.addEventListener(Event.ADDED_TO_STAGE, this.onStage);
            };
        }
        private function onStage(_arg1:Event):void{
            this.videoSurface.removeEventListener(Event.ADDED_TO_STAGE, this.onStage);
            this.videoSurface.addEventListener(Event.ENTER_FRAME, this.onFrame);
        }
        private function onFrame(_arg1:Event):void{
            if (((!((this.videoSurface.videoWidth == 0))) && (!((this.videoSurface.videoHeight == 0))))){
                if (((!((this.videoSurface.videoWidth == mediaWidth))) && (!((this.videoSurface.videoHeight == mediaHeight))))){
                    this.newMediaSize(this.videoSurface.videoWidth, this.videoSurface.videoHeight);
                };
                this.videoSurface.removeEventListener(Event.ENTER_FRAME, this.onFrame);
            };
        }
        private function onMetaData(_arg1:Object):void{
            if (((((!(isNaN(_arg1.width))) && (!(isNaN(_arg1.height))))) && (((!((_arg1.width == mediaWidth))) || (!((_arg1.height == mediaHeight))))))){
                this.newMediaSize(_arg1.width, _arg1.height);
            };
        }
        private function newMediaSize(_arg1:Number, _arg2:Number):void{
            if (((((!((this.videoSurface == null))) && ((this.videoSurface.width == 0)))) && ((this.videoSurface.height == 0)))){
                this.videoSurface.width = _arg1;
                this.videoSurface.height = _arg2;
            };
            setMediaSize(_arg1, _arg2);
        }

    }
}//package org.osmf.net 
