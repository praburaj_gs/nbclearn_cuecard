﻿package org.osmf.net {
    import org.osmf.media.*;
    import org.osmf.metadata.*;
    import org.osmf.utils.*;

    public class NetStreamUtils {

        public static const PLAY_START_ARG_ANY:int = -2;
        public static const PLAY_START_ARG_LIVE:int = -1;
        public static const PLAY_START_ARG_RECORDED:int = 0;
        public static const PLAY_LEN_ARG_ALL:int = -1;

        public static function getStreamNameFromURL(_arg1:String, _arg2:Boolean=false):String{
            var _local4:FMSURL;
            var _local3 = "";
            if (_arg1 != null){
                if (isRTMPStream(_arg1)){
                    _local4 = new FMSURL(_arg1, _arg2);
                    _local3 = _local4.streamName;
                    if (((!((_local4.query == null))) && (!((_local4.query == ""))))){
                        _local3 = (_local3 + ("?" + _local4.query));
                    };
                } else {
                    _local3 = _arg1;
                };
            };
            return (_local3);
        }
        public static function isStreamingResource(_arg1:MediaResourceBase):Boolean{
            var _local3:URLResource;
            var _local2:Boolean;
            if (_arg1 != null){
                _local3 = (_arg1 as URLResource);
                if (_local3 != null){
                    _local2 = NetStreamUtils.isRTMPStream(_local3.url);
                    if (_local2 == false){
                        _local2 = !((_local3.getMetadataValue(MetadataNamespaces.HTTP_STREAMING_METADATA) == null));
                    };
                };
            };
            return (_local2);
        }
        public static function isRTMPStream(_arg1:String):Boolean{
            var _local3:URL;
            var _local4:String;
            var _local2:Boolean;
            if (_arg1 != null){
                _local3 = new URL(_arg1);
                _local4 = _local3.protocol;
                if (((!((_local4 == null))) && ((_local4.length > 0)))){
                    _local2 = !((_local4.search(/^rtmp$|rtmp[tse]$|rtmpte$/i) == -1));
                };
            };
            return (_local2);
        }
        public static function getStreamType(_arg1:MediaResourceBase):String{
            var _local2:String = StreamType.RECORDED;
            var _local3:StreamingURLResource = (_arg1 as StreamingURLResource);
            if (_local3 != null){
                _local2 = _local3.streamType;
            };
            return (_local2);
        }
        public static function getPlayArgsForResource(_arg1:MediaResourceBase):Object{
            var _local4:StreamingURLResource;
            var _local2:Number = PLAY_START_ARG_ANY;
            var _local3:Number = PLAY_LEN_ARG_ALL;
            switch (getStreamType(_arg1)){
                case StreamType.LIVE_OR_RECORDED:
                    _local2 = PLAY_START_ARG_ANY;
                    break;
                case StreamType.LIVE:
                    _local2 = PLAY_START_ARG_LIVE;
                    break;
                case StreamType.RECORDED:
                    _local2 = PLAY_START_ARG_RECORDED;
                    break;
            };
            if (((!((_local2 == PLAY_START_ARG_LIVE))) && (!((_arg1 == null))))){
                _local4 = (_arg1 as StreamingURLResource);
                if (((!((_local4 == null))) && (isStreamingResource(_local4)))){
                    if (!isNaN(_local4.clipStartTime)){
                        _local2 = _local4.clipStartTime;
                    };
                    if (!isNaN(_local4.clipEndTime)){
                        _local2 = Math.max(0, _local2);
                        _local3 = Math.max(0, (_local4.clipEndTime - _local2));
                    };
                };
            };
            return ({
                start:_local2,
                len:_local3
            });
        }

    }
}//package org.osmf.net 
