﻿package org.osmf.net {
    import flash.net.*;
    import org.osmf.traits.*;
    import org.osmf.utils.*;

    public class NetStreamAlternativeAudioTrait extends AlternativeAudioTrait {

        private var _netStream:NetStream;
        private var _streamingResource:StreamingURLResource;
        private var _transitionInProgress:Boolean = false;
        private var _activeTransitionIndex:int = -1;
        private var _activeTransitionStreamName:String = null;
        private var _lastTransitionIndex:int = -2;
        private var _lastTransitionStreamName:String = null;

        public function NetStreamAlternativeAudioTrait(_arg1:NetStream, _arg2:StreamingURLResource){
            super(_arg2.alternativeAudioStreamItems.length);
            this._streamingResource = _arg2;
            this._netStream = _arg1;
            if (((!((this._netStream == null))) && (!((this._netStream.client == null))))){
                NetClient(_arg1.client).addHandler(NetStreamCodes.ON_PLAY_STATUS, this.onPlayStatus);
            };
        }
        override public function dispose():void{
            this._netStream = null;
            this._streamingResource = null;
        }
        override public function getItemForIndex(_arg1:int):StreamingItem{
            if ((((_arg1 <= INVALID_TRANSITION_INDEX)) || ((_arg1 >= numAlternativeAudioStreams)))){
                throw (new RangeError(OSMFStrings.getString(OSMFStrings.ALTERNATIVEAUDIO_INVALID_INDEX)));
            };
            if (_arg1 == DEFAULT_TRANSITION_INDEX){
                return (null);
            };
            return (this._streamingResource.alternativeAudioStreamItems[_arg1]);
        }
        override protected function endSwitching(_arg1:int):void{
            if (switching){
                this.executeSwitching(_indexToSwitchTo);
            };
            super.endSwitching(_arg1);
        }
        protected function executeSwitching(_arg1:int):void{
            var _local2:Object;
            var _local3:NetStreamPlayOptions;
            if (this._lastTransitionIndex != _arg1){
                this._activeTransitionIndex = _arg1;
                if (this._activeTransitionIndex > DEFAULT_TRANSITION_INDEX){
                    this._activeTransitionStreamName = this._streamingResource.alternativeAudioStreamItems[this._activeTransitionIndex].streamName;
                } else {
                    this._activeTransitionStreamName = null;
                };
                this._transitionInProgress = true;
                _local2 = NetStreamUtils.getPlayArgsForResource(this._streamingResource);
                _local3 = new NetStreamPlayOptions();
                _local3.start = _local2.start;
                _local3.len = _local2.len;
                _local3.streamName = this._activeTransitionStreamName;
                _local3.oldStreamName = this.prepareStreamName(this._lastTransitionStreamName);
                _local3.transition = NetStreamPlayTransitions.SWAP;
                this._netStream.play2(_local3);
            };
        }
        private function onPlayStatus(_arg1:Object):void{
            switch (_arg1.code){
                case NetStreamCodes.NETSTREAM_PLAY_TRANSITION_COMPLETE:
                    if (((this._transitionInProgress) && ((this._activeTransitionIndex > INVALID_TRANSITION_INDEX)))){
                        this._lastTransitionIndex = this._activeTransitionIndex;
                        this._lastTransitionStreamName = this._activeTransitionStreamName;
                        this._transitionInProgress = false;
                        this._activeTransitionIndex = INVALID_TRANSITION_INDEX;
                        this._activeTransitionStreamName = null;
                        setSwitching(false, this._lastTransitionIndex);
                    };
                    break;
            };
        }
        private function prepareStreamName(_arg1:String):String{
            if (((!((_arg1 == null))) && ((_arg1.indexOf("?") >= 0)))){
                return (_arg1.substr(0, _arg1.indexOf("?")));
            };
            return (_arg1);
        }

    }
}//package org.osmf.net 
