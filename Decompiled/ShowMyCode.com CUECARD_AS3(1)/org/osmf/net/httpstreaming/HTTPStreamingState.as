﻿package org.osmf.net.httpstreaming {

    class HTTPStreamingState {

        static const INIT:String = "init";
        static const LOAD:String = "load";
        static const WAIT:String = "wait";
        static const BEGIN_FRAGMENT:String = "beginFragment";
        static const END_FRAGMENT:String = "endFragment";
        static const PLAY:String = "play";
        static const READ:String = "read";
        static const SEEK:String = "seek";
        static const STOP:String = "stop";
        static const HALT:String = "halt";

    }
}//package org.osmf.net.httpstreaming 
