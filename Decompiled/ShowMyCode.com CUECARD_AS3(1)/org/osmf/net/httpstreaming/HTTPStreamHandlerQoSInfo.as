﻿package org.osmf.net.httpstreaming {
    import __AS3__.vec.*;
    import org.osmf.net.qos.*;

    public class HTTPStreamHandlerQoSInfo {

        private var _availableQualityLevels:Vector.<QualityLevel>;
        private var _actualIndex:uint;
        private var _lastFragmentDetails:FragmentDetails;

        public function HTTPStreamHandlerQoSInfo(_arg1:Vector.<QualityLevel>, _arg2:uint, _arg3:FragmentDetails=null){
            this._availableQualityLevels = _arg1;
            this._actualIndex = _arg2;
            this._lastFragmentDetails = _arg3;
        }
        public function get availableQualityLevels():Vector.<QualityLevel>{
            return (this._availableQualityLevels);
        }
        public function get actualIndex():uint{
            return (this._actualIndex);
        }
        public function get lastFragmentDetails():FragmentDetails{
            return (this._lastFragmentDetails);
        }

    }
}//package org.osmf.net.httpstreaming 
