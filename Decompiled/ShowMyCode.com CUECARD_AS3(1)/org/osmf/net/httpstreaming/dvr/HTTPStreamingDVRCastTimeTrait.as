﻿package org.osmf.net.httpstreaming.dvr {
    import flash.events.*;
    import org.osmf.net.*;
    import org.osmf.net.httpstreaming.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;

    public class HTTPStreamingDVRCastTimeTrait extends TimeTrait {

        private var _connection:NetConnection;
        private var _stream:HTTPNetStream;
        private var _dvrInfo:DVRInfo;

        public function HTTPStreamingDVRCastTimeTrait(_arg1:NetConnection, _arg2:HTTPNetStream, _arg3:DVRInfo){
            super(NaN);
            this._connection = _arg1;
            this._stream = _arg2;
            this._dvrInfo = _arg3;
            this._stream.addEventListener(DVRStreamInfoEvent.DVRSTREAMINFO, this.onDVRStreamInfo);
            this._stream.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus);
            NetClient(this._stream.client).addHandler(NetStreamCodes.ON_PLAY_STATUS, this.onPlayStatus);
        }
        override public function get duration():Number{
            if (this._dvrInfo == null){
                return (NaN);
            };
            return (this._dvrInfo.curLength);
        }
        override public function get currentTime():Number{
            return (this._stream.time);
        }
        private function onDVRStreamInfo(_arg1:DVRStreamInfoEvent):void{
            this._dvrInfo = (_arg1.info as DVRInfo);
            setDuration(this._dvrInfo.curLength);
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_PLAY_UNPUBLISH_NOTIFY:
                    signalComplete();
                    break;
            };
        }
        private function onPlayStatus(_arg1:Object):void{
            switch (_arg1.code){
                case NetStreamCodes.NETSTREAM_PLAY_COMPLETE:
                    signalComplete();
                    break;
            };
        }

    }
}//package org.osmf.net.httpstreaming.dvr 
