﻿package org.osmf.net.httpstreaming.dvr {
    import org.osmf.net.httpstreaming.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;

    public class HTTPStreamingDVRCastDVRTrait extends DVRTrait {

        private var _connection:NetConnection;
        private var _stream:HTTPNetStream;
        private var _dvrInfo:DVRInfo;

        public function HTTPStreamingDVRCastDVRTrait(_arg1:NetConnection, _arg2:HTTPNetStream, _arg3:DVRInfo){
            this._connection = _arg1;
            this._stream = _arg2;
            this._dvrInfo = _arg3;
            this._stream.addEventListener(DVRStreamInfoEvent.DVRSTREAMINFO, this.onDVRStreamInfo);
            super(_arg3.isRecording, _arg3.windowDuration);
        }
        private function onDVRStreamInfo(_arg1:DVRStreamInfoEvent):void{
            this._dvrInfo = (_arg1.info as DVRInfo);
            setIsRecording((((this._dvrInfo == null)) ? false : this._dvrInfo.isRecording));
        }

    }
}//package org.osmf.net.httpstreaming.dvr 
