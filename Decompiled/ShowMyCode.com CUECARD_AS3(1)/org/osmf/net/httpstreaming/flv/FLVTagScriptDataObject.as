﻿package org.osmf.net.httpstreaming.flv {
    import flash.net.*;

    public class FLVTagScriptDataObject extends FLVTag {

        public function FLVTagScriptDataObject(_arg1:int=18){
            super(_arg1);
        }
        public function get objects():Array{
            var _local1:Array = new Array();
            bytes.position = TAG_HEADER_BYTE_COUNT;
            while (bytes.bytesAvailable) {
                _local1.push(bytes.readObject());
            };
            return (_local1);
        }
        public function set objects(_arg1:Array):void{
            var _local2:Object;
            bytes.objectEncoding = ObjectEncoding.AMF0;
            bytes.length = TAG_HEADER_BYTE_COUNT;
            bytes.position = TAG_HEADER_BYTE_COUNT;
            for each (_local2 in _arg1) {
                bytes.writeObject(_local2);
            };
            dataSize = (bytes.length - TAG_HEADER_BYTE_COUNT);
        }

    }
}//package org.osmf.net.httpstreaming.flv 
