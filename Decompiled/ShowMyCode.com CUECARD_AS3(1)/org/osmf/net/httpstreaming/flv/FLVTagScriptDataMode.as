﻿package org.osmf.net.httpstreaming.flv {

    public class FLVTagScriptDataMode {

        public static const NORMAL:String = "normal";
        public static const FIRST:String = "first";
        public static const IMMEDIATE:String = "immediate";

    }
}//package org.osmf.net.httpstreaming.flv 
