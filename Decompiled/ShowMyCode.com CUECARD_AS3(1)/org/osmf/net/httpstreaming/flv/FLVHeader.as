﻿package org.osmf.net.httpstreaming.flv {
    import flash.utils.*;

    public class FLVHeader {

        public static const MIN_FILE_HEADER_BYTE_COUNT:int = 9;

        private var _hasVideoTags:Boolean = true;
        private var _hasAudioTags:Boolean = true;
        private var offset:uint;

        public function FLVHeader(_arg1:IDataInput=null){
            if (_arg1 != null){
                this.readHeader(_arg1);
                this.readRest(_arg1);
            };
        }
        public function get hasAudioTags():Boolean{
            return (this._hasAudioTags);
        }
        public function set hasAudioTags(_arg1:Boolean):void{
            this._hasAudioTags = _arg1;
        }
        public function get hasVideoTags():Boolean{
            return (this._hasVideoTags);
        }
        public function set hasVideoTags(_arg1:Boolean):void{
            this._hasVideoTags = _arg1;
        }
        public function write(_arg1:IDataOutput):void{
            _arg1.writeByte(70);
            _arg1.writeByte(76);
            _arg1.writeByte(86);
            _arg1.writeByte(1);
            var _local2:uint;
            if (this._hasAudioTags){
                _local2 = (_local2 | 4);
            };
            if (this._hasVideoTags){
                _local2 = (_local2 | 1);
            };
            _arg1.writeByte(_local2);
            var _local3:uint = MIN_FILE_HEADER_BYTE_COUNT;
            _arg1.writeUnsignedInt(_local3);
            var _local4:uint;
            _arg1.writeUnsignedInt(_local4);
        }
        function readHeader(_arg1:IDataInput):void{
            if (_arg1.bytesAvailable < MIN_FILE_HEADER_BYTE_COUNT){
                throw (new Error("FLVHeader() input too short"));
            };
            if (_arg1.readByte() != 70){
                throw (new Error("FLVHeader readHeader() Signature[0] not 'F'"));
            };
            if (_arg1.readByte() != 76){
                throw (new Error("FLVHeader readHeader() Signature[1] not 'L'"));
            };
            if (_arg1.readByte() != 86){
                throw (new Error("FLVHeader readHeader() Signature[2] not 'V'"));
            };
            if (_arg1.readByte() != 1){
                throw (new Error("FLVHeader readHeader() Version not 0x01"));
            };
            var _local2:int = _arg1.readByte();
            this._hasAudioTags = (((_local2 & 4)) ? true : false);
            this._hasVideoTags = (((_local2 & 1)) ? true : false);
            this.offset = _arg1.readUnsignedInt();
            if (this.offset < MIN_FILE_HEADER_BYTE_COUNT){
                throw (new Error("FLVHeader() offset smaller than minimum"));
            };
        }
        function readRest(_arg1:IDataInput):void{
            var _local2:ByteArray;
            if (this.offset > MIN_FILE_HEADER_BYTE_COUNT){
                if ((this.offset - MIN_FILE_HEADER_BYTE_COUNT) < (_arg1.bytesAvailable - FLVTag.PREV_TAG_BYTE_COUNT)){
                    throw (new Error("FLVHeader() input too short for nonstandard offset"));
                };
                _local2 = new ByteArray();
                _arg1.readBytes(_local2, 0, (this.offset - MIN_FILE_HEADER_BYTE_COUNT));
            };
            if (_arg1.bytesAvailable < FLVTag.PREV_TAG_BYTE_COUNT){
                throw (new Error("FLVHeader() input too short for previousTagSize0"));
            };
            _arg1.readUnsignedInt();
        }
        function get restBytesNeeded():int{
            return ((FLVTag.PREV_TAG_BYTE_COUNT + (this.offset - MIN_FILE_HEADER_BYTE_COUNT)));
        }

    }
}//package org.osmf.net.httpstreaming.flv 
