﻿package org.osmf.net.httpstreaming.flv {
    import flash.utils.*;

    public class FLVParser {

        private var state:String;
        private var savedBytes:ByteArray;
        private var currentTag:FLVTag = null;
        private var flvHeader:FLVHeader;

        public function FLVParser(_arg1:Boolean){
            this.savedBytes = new ByteArray();
            if (_arg1){
                this.state = FLVParserState.FILE_HEADER;
            } else {
                this.state = FLVParserState.TYPE;
            };
        }
        public function flush(_arg1:IDataOutput):void{
            _arg1.writeBytes(this.savedBytes);
        }
        public function parse(_arg1:IDataInput, _arg2:Boolean, _arg3:Function):void{
            var _local5:IDataInput;
            var _local7:int;
            var _local4:Boolean;
            var _local6:Date = new Date();
            while (_local4) {
                switch (this.state){
                    case FLVParserState.FILE_HEADER:
                        _local5 = this.byteSource(_arg1, FLVHeader.MIN_FILE_HEADER_BYTE_COUNT);
                        if (_local5 != null){
                            this.flvHeader = new FLVHeader();
                            this.flvHeader.readHeader(_local5);
                            this.state = FLVParserState.FILE_HEADER_REST;
                        } else {
                            _local4 = false;
                        };
                        break;
                    case FLVParserState.FILE_HEADER_REST:
                        _local5 = this.byteSource(_arg1, this.flvHeader.restBytesNeeded);
                        if (_local5 != null){
                            this.flvHeader.readRest(_local5);
                            this.state = FLVParserState.TYPE;
                        } else {
                            _local4 = false;
                        };
                        break;
                    case FLVParserState.TYPE:
                        _local5 = this.byteSource(_arg1, 1);
                        if (_local5 != null){
                            _local7 = _local5.readByte();
                            switch (_local7){
                                case FLVTag.TAG_TYPE_AUDIO:
                                case FLVTag.TAG_TYPE_ENCRYPTED_AUDIO:
                                    this.currentTag = new FLVTagAudio(_local7);
                                    break;
                                case FLVTag.TAG_TYPE_VIDEO:
                                case FLVTag.TAG_TYPE_ENCRYPTED_VIDEO:
                                    this.currentTag = new FLVTagVideo(_local7);
                                    break;
                                case FLVTag.TAG_TYPE_SCRIPTDATAOBJECT:
                                case FLVTag.TAG_TYPE_ENCRYPTED_SCRIPTDATAOBJECT:
                                    this.currentTag = new FLVTagScriptDataObject(_local7);
                                    break;
                                default:
                                    this.currentTag = new FLVTag(_local7);
                            };
                            this.state = FLVParserState.HEADER;
                        } else {
                            _local4 = false;
                        };
                        break;
                    case FLVParserState.HEADER:
                        _local5 = this.byteSource(_arg1, (FLVTag.TAG_HEADER_BYTE_COUNT - 1));
                        if (_local5 != null){
                            this.currentTag.readRemainingHeader(_local5);
                            if (this.currentTag.dataSize){
                                this.state = FLVParserState.DATA;
                            } else {
                                this.state = FLVParserState.PREV_TAG;
                            };
                        } else {
                            _local4 = false;
                        };
                        break;
                    case FLVParserState.DATA:
                        _local5 = this.byteSource(_arg1, this.currentTag.dataSize);
                        if (_local5 != null){
                            this.currentTag.readData(_local5);
                            this.state = FLVParserState.PREV_TAG;
                        } else {
                            _local4 = false;
                        };
                        break;
                    case FLVParserState.PREV_TAG:
                        _local5 = this.byteSource(_arg1, FLVTag.PREV_TAG_BYTE_COUNT);
                        if (_local5 != null){
                            this.currentTag.readPrevTag(_local5);
                            this.state = FLVParserState.TYPE;
                            _local4 = _arg3(this.currentTag);
                        } else {
                            _local4 = false;
                        };
                        break;
                    default:
                        throw (new Error("FLVParser state machine in unknown state"));
                };
            };
            if (_arg2){
                _arg1.readBytes(this.savedBytes, this.savedBytes.length);
            };
        }
        private function byteSource(_arg1:IDataInput, _arg2:int):IDataInput{
            var _local3:int;
            if ((this.savedBytes.bytesAvailable + _arg1.bytesAvailable) < _arg2){
                return (null);
            };
            if (this.savedBytes.bytesAvailable){
                _local3 = (_arg2 - this.savedBytes.bytesAvailable);
                if (_local3 > 0){
                    _arg1.readBytes(this.savedBytes, this.savedBytes.length, _local3);
                };
                return (this.savedBytes);
            };
            this.savedBytes.length = 0;
            return (_arg1);
        }

    }
}//package org.osmf.net.httpstreaming.flv 
