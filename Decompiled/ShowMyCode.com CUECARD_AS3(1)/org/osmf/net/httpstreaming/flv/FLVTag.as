﻿package org.osmf.net.httpstreaming.flv {
    import flash.utils.*;

    public class FLVTag {

        public static const TAG_TYPE_AUDIO:int = 8;
        public static const TAG_TYPE_VIDEO:int = 9;
        public static const TAG_TYPE_SCRIPTDATAOBJECT:int = 18;
        public static const TAG_FLAG_ENCRYPTED:int = 32;
        public static const TAG_TYPE_ENCRYPTED_AUDIO:int = 40;
        public static const TAG_TYPE_ENCRYPTED_VIDEO:int = 41;
        public static const TAG_TYPE_ENCRYPTED_SCRIPTDATAOBJECT:int = 50;
        public static const TAG_HEADER_BYTE_COUNT:int = 11;
        public static const PREV_TAG_BYTE_COUNT:int = 4;

        protected var bytes:ByteArray = null;

        public function FLVTag(_arg1:int){
            this.bytes = new ByteArray();
            this.bytes.length = TAG_HEADER_BYTE_COUNT;
            this.bytes[0] = _arg1;
        }
        public function read(_arg1:IDataInput):void{
            this.readType(_arg1);
            this.readRemainingHeader(_arg1);
            this.readData(_arg1);
            this.readPrevTag(_arg1);
        }
        public function readType(_arg1:IDataInput):void{
            if (_arg1.bytesAvailable < 1){
                throw (new Error("FLVTag.readType() input too short"));
            };
            _arg1.readBytes(this.bytes, 0, 1);
        }
        public function readRemaining(_arg1:IDataInput):void{
            this.readRemainingHeader(_arg1);
            this.readData(_arg1);
            this.readPrevTag(_arg1);
        }
        public function readRemainingHeader(_arg1:IDataInput):void{
            if (_arg1.bytesAvailable < 10){
                throw (new Error("FLVTag.readHeader() input too short"));
            };
            _arg1.readBytes(this.bytes, 1, (TAG_HEADER_BYTE_COUNT - 1));
        }
        public function readData(_arg1:IDataInput):void{
            if (this.dataSize > 0){
                if (_arg1.bytesAvailable < this.dataSize){
                    throw (new Error("FLVTag().readData input shorter than dataSize"));
                };
                _arg1.readBytes(this.bytes, TAG_HEADER_BYTE_COUNT, this.dataSize);
            };
        }
        public function readPrevTag(_arg1:IDataInput):void{
            if (_arg1.bytesAvailable < 4){
                throw (new Error("FLVTag.readPrevTag() input too short"));
            };
            _arg1.readUnsignedInt();
        }
        public function write(_arg1:IDataOutput):void{
            _arg1.writeBytes(this.bytes, 0, (TAG_HEADER_BYTE_COUNT + this.dataSize));
            _arg1.writeUnsignedInt((TAG_HEADER_BYTE_COUNT + this.dataSize));
        }
        public function get tagType():uint{
            return (this.bytes[0]);
        }
        public function set tagType(_arg1:uint):void{
            this.bytes[0] = _arg1;
        }
        public function get isEncrpted():Boolean{
            return ((((this.bytes[0] & TAG_FLAG_ENCRYPTED)) ? true : false));
        }
        public function get dataSize():uint{
            return ((((this.bytes[1] << 16) | (this.bytes[2] << 8)) | this.bytes[3]));
        }
        public function set dataSize(_arg1:uint):void{
            this.bytes[1] = ((_arg1 >> 16) & 0xFF);
            this.bytes[2] = ((_arg1 >> 8) & 0xFF);
            this.bytes[3] = (_arg1 & 0xFF);
            this.bytes.length = (TAG_HEADER_BYTE_COUNT + _arg1);
        }
        public function get timestamp():uint{
            return (((((this.bytes[7] << 24) | (this.bytes[4] << 16)) | (this.bytes[5] << 8)) | this.bytes[6]));
        }
        public function set timestamp(_arg1:uint):void{
            this.bytes[7] = ((_arg1 >> 24) & 0xFF);
            this.bytes[4] = ((_arg1 >> 16) & 0xFF);
            this.bytes[5] = ((_arg1 >> 8) & 0xFF);
            this.bytes[6] = (_arg1 & 0xFF);
        }
        public function get data():ByteArray{
            var _local1:ByteArray = new ByteArray();
            _local1.writeBytes(this.bytes, TAG_HEADER_BYTE_COUNT, this.dataSize);
            return (_local1);
        }
        public function set data(_arg1:ByteArray):void{
            this.bytes.length = (TAG_HEADER_BYTE_COUNT + _arg1.length);
            this.bytes.position = TAG_HEADER_BYTE_COUNT;
            this.bytes.writeBytes(_arg1, 0, _arg1.length);
            this.dataSize = _arg1.length;
        }

    }
}//package org.osmf.net.httpstreaming.flv 
