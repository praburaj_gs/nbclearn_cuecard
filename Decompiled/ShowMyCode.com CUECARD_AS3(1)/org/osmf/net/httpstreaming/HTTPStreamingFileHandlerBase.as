﻿package org.osmf.net.httpstreaming {
    import flash.events.*;
    import flash.utils.*;
    import flash.errors.*;

    public class HTTPStreamingFileHandlerBase extends EventDispatcher {

        public function beginProcessFile(_arg1:Boolean, _arg2:Number):void{
            throw (new IllegalOperationError("The beginProcessFile() method must be overridden by HttpStreamingFileHandlerBase's derived class."));
        }
        public function get inputBytesNeeded():Number{
            throw (new IllegalOperationError("The inputBytesNeeded() method must be overridden by HttpStreamingFileHandlerBase's derived class."));
        }
        public function processFileSegment(_arg1:IDataInput):ByteArray{
            throw (new IllegalOperationError("The processFileSegment() method must be overridden by HttpStreamingFileHandlerBase's derived class."));
        }
        public function endProcessFile(_arg1:IDataInput):ByteArray{
            throw (new IllegalOperationError("The endProcessFile() method must be overridden by HttpStreamingFileHandlerBase's derived class."));
        }
        public function flushFileSegment(_arg1:IDataInput):ByteArray{
            throw (new IllegalOperationError("The flushFileSegment() method must be overridden by HttpStreamingFileHandlerBase's derived class."));
        }

    }
}//package org.osmf.net.httpstreaming 
