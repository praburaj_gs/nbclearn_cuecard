﻿package org.osmf.net.httpstreaming {
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.metadata.*;
    import org.osmf.elements.f4mClasses.*;
    import org.osmf.net.*;
    import flash.utils.*;
    import org.osmf.net.httpstreaming.dvr.*;
    import org.osmf.net.httpstreaming.f4f.*;
    import org.osmf.utils.*;

    public class HTTPStreamingUtils {

        public static function createHTTPStreamingMetadata(_arg1:String, _arg2:ByteArray, _arg3:Vector.<String>):Metadata{
            var _local4:Metadata = new Metadata();
            var _local5:BootstrapInfo = new BootstrapInfo();
            if (((!((_arg1 == null))) && ((_arg1.length > 0)))){
                _local5.url = _arg1;
            };
            _local5.data = _arg2;
            _local4.addValue(MetadataNamespaces.HTTP_STREAMING_BOOTSTRAP_KEY, _local5);
            if (((!((_arg3 == null))) && ((_arg3.length > 0)))){
                _local4.addValue(MetadataNamespaces.HTTP_STREAMING_SERVER_BASE_URLS_KEY, _arg3);
            };
            return (_local4);
        }
        public static function createHTTPStreamingResource(_arg1:MediaResourceBase, _arg2:String):MediaResourceBase{
            var _local4:Object;
            var _local5:ByteArray;
            var _local6:Vector.<String>;
            if (_arg2 == null){
                return (null);
            };
            var _local3:BootstrapInfo;
            var _local7:Metadata = (_arg1.getMetadataValue(MetadataNamespaces.HTTP_STREAMING_METADATA) as Metadata);
            if (_local7 == null){
                return (null);
            };
            var _local8:Metadata = new Metadata();
            _local3 = (_local7.getValue((MetadataNamespaces.HTTP_STREAMING_BOOTSTRAP_KEY + _arg2)) as BootstrapInfo);
            if (_local3 != null){
                _local8.addValue(MetadataNamespaces.HTTP_STREAMING_BOOTSTRAP_KEY, _local3);
            };
            _local4 = _local7.getValue((MetadataNamespaces.HTTP_STREAMING_STREAM_METADATA_KEY + _arg2));
            if (_local4 != null){
                _local8.addValue(MetadataNamespaces.HTTP_STREAMING_STREAM_METADATA_KEY, _local4);
            };
            _local5 = (_local7.getValue((MetadataNamespaces.HTTP_STREAMING_XMP_METADATA_KEY + _arg2)) as ByteArray);
            if (_local5 != null){
                _local8.addValue(MetadataNamespaces.HTTP_STREAMING_XMP_METADATA_KEY, _local5);
            };
            _local6 = (_local7.getValue(MetadataNamespaces.HTTP_STREAMING_SERVER_BASE_URLS_KEY) as Vector.<String>);
            if (_local6 != null){
                _local8.addValue(MetadataNamespaces.HTTP_STREAMING_SERVER_BASE_URLS_KEY, _local6);
            };
            var _local9 = "";
            if (((!((_local6 == null))) && ((_local6.length > 0)))){
                _local9 = _local6[0].toString();
            };
            var _local10:String = _arg2;
            if (!URL.isAbsoluteURL(_local10)){
                _local10 = (URL.normalizeRootURL(_local9) + URL.normalizeRelativeURL(_local10));
            };
            var _local11:MediaResourceBase = new StreamingURLResource(_local10);
            _local11.addMetadataValue(MetadataNamespaces.HTTP_STREAMING_METADATA, _local8);
            var _local12:DVRInfo = generateDVRInfo((_arg1.getMetadataValue(MetadataNamespaces.DVR_METADATA) as Metadata));
            addDVRInfoMetadataToResource(_local12, _local11);
            var _local13:BestEffortFetchInfo = generateBestEffortFetchInfo((_arg1.getMetadataValue(MetadataNamespaces.BEST_EFFORT_FETCH_METADATA) as Metadata));
            addBestEffortFetchInfoMetadataToResource(_local13, _local11);
            return (_local11);
        }
        public static function addDVRInfoMetadataToResource(_arg1:DVRInfo, _arg2:MediaResourceBase):void{
            if (_arg1 == null){
                return;
            };
            var _local3:Metadata = new Metadata();
            _local3.addValue(MetadataNamespaces.HTTP_STREAMING_DVR_BEGIN_OFFSET_KEY, _arg1.beginOffset);
            _local3.addValue(MetadataNamespaces.HTTP_STREAMING_DVR_END_OFFSET_KEY, _arg1.endOffset);
            _local3.addValue(MetadataNamespaces.HTTP_STREAMING_DVR_WINDOW_DURATION_KEY, _arg1.windowDuration);
            _local3.addValue(MetadataNamespaces.HTTP_STREAMING_DVR_OFFLINE_KEY, _arg1.offline);
            _local3.addValue(MetadataNamespaces.HTTP_STREAMING_DVR_ID_KEY, _arg1.id);
            _arg2.addMetadataValue(MetadataNamespaces.DVR_METADATA, _local3);
        }
        public static function addBestEffortFetchInfoMetadataToResource(_arg1:BestEffortFetchInfo, _arg2:MediaResourceBase):void{
            if (_arg1 == null){
                return;
            };
            var _local3:Metadata = new Metadata();
            _local3.addValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_MAX_FORWARD_FETCHES, _arg1.maxForwardFetches);
            _local3.addValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_MAX_BACKWARD_FETCHES, _arg1.maxBackwardFetches);
            _local3.addValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_SEGMENT_DURATION, _arg1.segmentDuration);
            _local3.addValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_FRAGMENT_DURATION, _arg1.fragmentDuration);
            _arg2.addMetadataValue(MetadataNamespaces.BEST_EFFORT_FETCH_METADATA, _local3);
        }
        public static function createF4FIndexInfo(_arg1:URLResource):HTTPStreamingF4FIndexInfo{
            var _local6:Vector.<String>;
            var _local7:Vector.<HTTPStreamingF4FStreamInfo>;
            var _local8:DVRInfo;
            var _local9:BestEffortFetchInfo;
            var _local2:HTTPStreamingF4FIndexInfo;
            var _local3:Metadata = (_arg1.getMetadataValue(MetadataNamespaces.HTTP_STREAMING_METADATA) as Metadata);
            var _local4:Metadata = (_arg1.getMetadataValue(MetadataNamespaces.DVR_METADATA) as Metadata);
            var _local5:Metadata = (_arg1.getMetadataValue(MetadataNamespaces.BEST_EFFORT_FETCH_METADATA) as Metadata);
            if (_local3 != null){
                _local6 = (_local3.getValue(MetadataNamespaces.HTTP_STREAMING_SERVER_BASE_URLS_KEY) as Vector.<String>);
                _local7 = generateStreamInfos(_arg1);
                _local8 = generateDVRInfo(_local4);
                _local9 = generateBestEffortFetchInfo(_local5);
                _local2 = new HTTPStreamingF4FIndexInfo(((((!((_local6 == null))) && ((_local6.length > 0)))) ? _local6[0] : null), _local7, _local8, _local9);
            };
            return (_local2);
        }
        public static function normalizeURL(_arg1:String):String{
            var _local4:String;
            var _local7:int;
            var _local2 = "";
            var _local3 = "";
            if (_arg1.indexOf("http://") == 0){
                _local3 = "http://";
            } else {
                if (_arg1.indexOf("https://") == 0){
                    _local3 = "https://";
                };
            };
            if (_local3.length > 0){
                _local4 = _arg1.substr(_local3.length);
            } else {
                _local4 = _arg1;
            };
            var _local5:Array = _local4.split("/");
            var _local6:int = _local5.indexOf("..");
            while (_local6 >= 0) {
                _local5.splice((_local6 - 1), 2);
                _local6 = _local5.indexOf("..");
            };
            _local2 = _local3;
            if (_local5.length > 0){
                _local7 = 0;
                while (_local7 < _local5.length) {
                    if (_local7 != 0){
                        _local2 = (_local2 + "/");
                    };
                    _local2 = (_local2 + (_local5[_local7] as String));
                    _local7++;
                };
            };
            return (_local2);
        }
        private static function generateDVRInfo(_arg1:Metadata):DVRInfo{
            if (_arg1 == null){
                return (null);
            };
            var _local2:DVRInfo = new DVRInfo();
            _local2.id = "";
            _local2.beginOffset = NaN;
            _local2.endOffset = NaN;
            _local2.windowDuration = NaN;
            _local2.offline = false;
            if (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_DVR_ID_KEY) != null){
                _local2.id = (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_DVR_ID_KEY) as String);
            };
            if (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_DVR_BEGIN_OFFSET_KEY) != null){
                _local2.beginOffset = (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_DVR_BEGIN_OFFSET_KEY) as uint);
            };
            if (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_DVR_END_OFFSET_KEY) != null){
                _local2.endOffset = (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_DVR_END_OFFSET_KEY) as uint);
            };
            if (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_DVR_WINDOW_DURATION_KEY) != null){
                _local2.windowDuration = (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_DVR_WINDOW_DURATION_KEY) as int);
            };
            if (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_DVR_OFFLINE_KEY) != null){
                _local2.offline = (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_DVR_OFFLINE_KEY) as Boolean);
            };
            return (_local2);
        }
        private static function generateBestEffortFetchInfo(_arg1:Metadata):BestEffortFetchInfo{
            if (_arg1 == null){
                return (null);
            };
            var _local2:BestEffortFetchInfo = new BestEffortFetchInfo();
            if (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_MAX_FORWARD_FETCHES) != null){
                _local2.maxForwardFetches = (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_MAX_FORWARD_FETCHES) as uint);
            };
            if (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_MAX_BACKWARD_FETCHES) != null){
                _local2.maxBackwardFetches = (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_MAX_BACKWARD_FETCHES) as uint);
            };
            if (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_SEGMENT_DURATION) != null){
                _local2.segmentDuration = (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_SEGMENT_DURATION) as uint);
            };
            if (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_FRAGMENT_DURATION) != null){
                _local2.fragmentDuration = (_arg1.getValue(MetadataNamespaces.HTTP_STREAMING_BEST_EFFORT_FETCH_FRAGMENT_DURATION) as uint);
            };
            return (_local2);
        }
        private static function generateStreamInfos(_arg1:URLResource):Vector.<HTTPStreamingF4FStreamInfo>{
            var _local8:Object;
            var _local9:ByteArray;
            var _local10:DynamicStreamingItem;
            var _local11:String;
            var _local2:Vector.<HTTPStreamingF4FStreamInfo> = new Vector.<HTTPStreamingF4FStreamInfo>();
            var _local3:Metadata = (_arg1.getMetadataValue(MetadataNamespaces.DRM_METADATA) as Metadata);
            var _local4:Metadata = (_arg1.getMetadataValue(MetadataNamespaces.HTTP_STREAMING_METADATA) as Metadata);
            var _local5:ByteArray;
            var _local6:BootstrapInfo;
            var _local7:DynamicStreamingResource = (_arg1 as DynamicStreamingResource);
            if (_local7 != null){
                for each (_local10 in _local7.streamItems) {
                    _local5 = null;
                    _local6 = null;
                    _local8 = null;
                    _local9 = null;
                    if (_local3 != null){
                        _local5 = (_local3.getValue((MetadataNamespaces.DRM_ADDITIONAL_HEADER_KEY + _local10.streamName)) as ByteArray);
                    };
                    if (_local4 != null){
                        _local6 = (_local4.getValue((MetadataNamespaces.HTTP_STREAMING_BOOTSTRAP_KEY + _local10.streamName)) as BootstrapInfo);
                        _local8 = _local4.getValue((MetadataNamespaces.HTTP_STREAMING_STREAM_METADATA_KEY + _local10.streamName));
                        _local9 = (_local4.getValue((MetadataNamespaces.HTTP_STREAMING_XMP_METADATA_KEY + _local10.streamName)) as ByteArray);
                    };
                    _local2.push(new HTTPStreamingF4FStreamInfo(_local6, _local10.streamName, _local10.bitrate, _local5, _local8, _local9));
                };
            } else {
                if (_local3 != null){
                    _local5 = (_local3.getValue(MetadataNamespaces.DRM_ADDITIONAL_HEADER_KEY) as ByteArray);
                };
                if (_local4 != null){
                    _local6 = (_local4.getValue(MetadataNamespaces.HTTP_STREAMING_BOOTSTRAP_KEY) as BootstrapInfo);
                    _local8 = _local4.getValue(MetadataNamespaces.HTTP_STREAMING_STREAM_METADATA_KEY);
                    _local9 = (_local4.getValue(MetadataNamespaces.HTTP_STREAMING_XMP_METADATA_KEY) as ByteArray);
                };
                _local11 = _arg1.url;
                _local2.push(new HTTPStreamingF4FStreamInfo(_local6, _local11, NaN, _local5, _local8, _local9));
            };
            return (_local2);
        }

    }
}//package org.osmf.net.httpstreaming 
