﻿package org.osmf.net.httpstreaming {
    import flash.events.*;
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.*;
    import org.osmf.net.qos.*;
    import org.osmf.net.metrics.*;
    import org.osmf.net.rules.*;

    public class DefaultHTTPStreamingSwitchManager extends RuleSwitchManagerBase {

        private static const ACTUAL_BITRATE_MAX_FRAGMENTS:uint = 10;
        private static const logger:Logger = Log.getLogger("org.osmf.net.httpstreaming.DefaultHTTPStreamingSwitchManager");

        private var _minReliability:Number;
        private var _maxReliabilityRecordSize:uint;
        private var _minReliabilityRecordSize:uint;
        private var _normalRuleWeights:Vector.<Number>;
        private var _climbFactor:Number;
        private var _maxUpSwitchLimit:int;
        private var _maxDownSwitchLimit:int;
        private var decisionHistory:Vector.<Switch>;
        private var availableQualityLevelsMetric:MetricBase;
        private var actualBitrateMetric:MetricBase;
        private var _normalRules:Vector.<RuleBase>;

        public function DefaultHTTPStreamingSwitchManager(_arg1:EventDispatcher, _arg2:NetStreamSwitcher, _arg3:MetricRepository, _arg4:Vector.<RuleBase>=null, _arg5:Boolean=true, _arg6:Vector.<RuleBase>=null, _arg7:Vector.<Number>=null, _arg8:Number=0.85, _arg9:uint=5, _arg10:uint=30, _arg11:Number=0.9, _arg12:int=1, _arg13:int=2){
            super(_arg1, _arg2, _arg3, _arg4, _arg5);
            this.setNormalRules(_arg6);
            this.normalRuleWeights = _arg7;
            this.minReliability = _arg8;
            this.minReliabilityRecordSize = _arg9;
            this.maxReliabilityRecordSize = _arg10;
            this.climbFactor = _arg11;
            this.maxUpSwitchLimit = _arg12;
            this.maxDownSwitchLimit = _arg13;
            this.decisionHistory = new Vector.<Switch>();
            this.pushToHistory(currentIndex);
            this.availableQualityLevelsMetric = _arg3.getMetric(MetricType.AVAILABLE_QUALITY_LEVELS);
            this.actualBitrateMetric = _arg3.getMetric(MetricType.ACTUAL_BITRATE, ACTUAL_BITRATE_MAX_FRAGMENTS);
        }
        public function get normalRules():Vector.<RuleBase>{
            return (this._normalRules);
        }
        public function get normalRuleWeights():Vector.<Number>{
            return (this._normalRuleWeights);
        }
        public function set normalRuleWeights(_arg1:Vector.<Number>):void{
            ABRUtils.validateWeights(_arg1, this.normalRules.length);
            this._normalRuleWeights = _arg1.slice();
        }
        public function get minReliability():Number{
            return (this._minReliability);
        }
        public function set minReliability(_arg1:Number):void{
            if (((((isNaN(_arg1)) || ((_arg1 < 0)))) || ((_arg1 > 1)))){
                throw (new ArgumentError("The minReliability must be a number between 0 and 1."));
            };
            this._minReliability = _arg1;
        }
        public function get minReliabilityRecordSize():uint{
            return (this._minReliabilityRecordSize);
        }
        public function set minReliabilityRecordSize(_arg1:uint):void{
            if (_arg1 < 2){
                throw (new ArgumentError("The minReliabilityRecordSize must be equal or greater than 2."));
            };
            this._minReliabilityRecordSize = _arg1;
        }
        public function get maxReliabilityRecordSize():uint{
            return (this._maxReliabilityRecordSize);
        }
        public function set maxReliabilityRecordSize(_arg1:uint):void{
            if (_arg1 < this.minReliabilityRecordSize){
                this._maxReliabilityRecordSize = this.minReliabilityRecordSize;
                logger.warn(("Cannot set the maxReliabilityRecordSize to a lower value than minReliabilityRecordSize. Setting it to " + this.minReliabilityRecordSize));
            } else {
                this._maxReliabilityRecordSize = _arg1;
            };
        }
        public function get climbFactor():Number{
            return (this._climbFactor);
        }
        public function set climbFactor(_arg1:Number):void{
            if (((isNaN(_arg1)) || ((_arg1 <= 0)))){
                throw (new ArgumentError("The climbFactor must be a number greater than 0."));
            };
            this._climbFactor = _arg1;
        }
        public function get maxUpSwitchLimit():int{
            return (this._maxUpSwitchLimit);
        }
        public function set maxUpSwitchLimit(_arg1:int):void{
            if (_arg1 < 1){
                this._maxUpSwitchLimit = -1;
            } else {
                this._maxUpSwitchLimit = _arg1;
            };
        }
        public function get maxDownSwitchLimit():int{
            return (this._maxDownSwitchLimit);
        }
        public function set maxDownSwitchLimit(_arg1:int):void{
            if (_arg1 < 1){
                this._maxDownSwitchLimit = -1;
            } else {
                this._maxDownSwitchLimit = _arg1;
            };
        }
        public function getCurrentReliability(_arg1:uint):Number{
            var _local2:uint;
            var _local3:uint;
            var _local4:uint;
            while (_local4 < (this.decisionHistory.length - 1)) {
                if (this.decisionHistory[_local4].index == _arg1){
                    _local3++;
                    if (this.decisionHistory[(_local4 + 1)].index < this.decisionHistory[_local4].index){
                        if (this.decisionHistory[(_local4 + 1)].emergency == true){
                            return (0);
                        };
                        _local2++;
                    };
                };
                _local4++;
            };
            if (this.decisionHistory.length < this._minReliabilityRecordSize){
                return (Number.NaN);
            };
            if (_local3 == 0){
                return (Number.NaN);
            };
            if (_local2 == 0){
                return (1);
            };
            return ((1 - ((_local2 * _local2) / (_local3 * Math.floor((this.decisionHistory.length / 2))))));
        }
        override public function getNewIndex():uint{
            var _local2:QualityLevel;
            var _local8:Recommendation;
            var _local9:Number;
            var _local10:Number;
            var _local11:Number;
            var _local1:uint;
            var _local3:Number = 0;
            var _local4:Number = 0;
            var _local5:MetricValue = this.availableQualityLevelsMetric.value;
            if (!_local5.valid){
                throw (new Error("The available quality levels metric should always be valid"));
            };
            var _local6:Vector.<QualityLevel> = _local5.value;
            var _local7:uint;
            while (_local7 < this.normalRules.length) {
                _local8 = this.normalRules[_local7].getRecommendation();
                _local9 = (_local8.confidence * this.normalRuleWeights[_local7]);
                _local3 = (_local3 + (_local8.bitrate * _local9));
                _local4 = (_local4 + _local9);
                _local7++;
            };
            if (_local4 == 0){
                _local1 = actualIndex;
                logger.info(("No information from the rules. Recommeding actual index (currently downloading): " + _local1));
            } else {
                _local10 = (_local3 / _local4);
                _local11 = this.getCurrentActualBitrate();
                if (_local10 > _local11){
                    _local10 = (_local11 + (this._climbFactor * (_local10 - _local11)));
                };
                _local1 = this.getMaxIndex(_local10);
                logger.info(((("Ideal bitrate: " + ABRUtils.roundNumber(_local10)) + "; Chosen index: ") + _local1));
            };
            this.pushToHistory(_local1);
            return (_local1);
        }
        override public function getNewEmergencyIndex(_arg1:Number):uint{
            var _local2:uint = this.getMaxIndex(_arg1, true);
            this.pushToHistory(_local2, (_local2 < actualIndex));
            return (_local2);
        }
        protected function setNormalRules(_arg1:Vector.<RuleBase>):void{
            if ((((_arg1 == null)) || ((_arg1.length == 0)))){
                throw (new ArgumentError("You must provide at least one normal rule"));
            };
            this._normalRules = _arg1.slice();
        }
        protected function getMaxIndex(_arg1:Number, _arg2:Boolean=false):uint{
            var _local8:QualityLevel;
            var _local3:MetricValue = this.availableQualityLevelsMetric.value;
            if (!_local3.valid){
                throw (new Error("The available quality levels metric should always be valid"));
            };
            var _local4:Vector.<QualityLevel> = _local3.value;
            logger.debug((("Determining the index of the highest quality rendition that has a bitrate smaller or equal than: " + _arg1) + " kbps"));
            this.logSwitchHistory();
            this.logReliabilities(_local4);
            var _local5:QualityLevel = _local4[0];
            var _local6:uint;
            while (_local6 < _local4.length) {
                _local8 = _local4[_local6];
                if ((((((_local8.bitrate > _local5.bitrate)) && ((_local8.bitrate <= _arg1)))) && (this.isReliable(_local8.index)))){
                    _local5 = _local8;
                };
                _local6++;
            };
            logger.debug((((("The quality level before applying switch constraints: index = " + _local5.index) + "; bitrate = ") + _local5.bitrate) + " kbps"));
            var _local7:uint = _local5.index;
            if (((!(_arg2)) && (!((_local7 == actualIndex))))){
                if (_local7 > actualIndex){
                    if (this.maxUpSwitchLimit > 0){
                        while (((((((_local7 - actualIndex) > this.maxUpSwitchLimit)) || (!(this.isReliable(_local7))))) && ((_local7 > actualIndex)))) {
                            _local7--;
                        };
                    };
                } else {
                    if (this.maxDownSwitchLimit > 0){
                        while (((((((actualIndex - _local7) > this.maxDownSwitchLimit)) || (!(this.isReliable(_local7))))) && ((_local7 < actualIndex)))) {
                            _local7++;
                        };
                    };
                };
                _local5 = _local4[_local7];
            };
            logger.info((((("The index to be played is: " + _local5.index) + " (") + _local5.bitrate) + " kbps)"));
            return (_local5.index);
        }
        protected function isReliable(_arg1:uint):Boolean{
            var _local2:Number = this.getCurrentReliability(_arg1);
            if (isNaN(_local2)){
                return (true);
            };
            return ((_local2 > this._minReliability));
        }
        private function getCurrentActualBitrate():Number{
            var _local5:QualityLevel;
            var _local1:Number = 0;
            var _local2:MetricValue = this.availableQualityLevelsMetric.value;
            if (!_local2.valid){
                throw (new Error("The available quality levels metric should always be valid"));
            };
            var _local3:Vector.<QualityLevel> = _local2.value;
            var _local4:MetricValue = this.actualBitrateMetric.value;
            if (_local4.valid){
                _local1 = (_local4.value as Number);
            } else {
                for each (_local5 in _local3) {
                    if (_local5.index == actualIndex){
                        _local1 = _local5.bitrate;
                    };
                };
            };
            return (_local1);
        }
        private function pushToHistory(_arg1:uint, _arg2:Boolean=false):void{
            var _local3:Switch = new Switch();
            _local3.index = _arg1;
            _local3.emergency = _arg2;
            this.decisionHistory.push(_local3);
            if (this.decisionHistory.length > this._maxReliabilityRecordSize){
                this.decisionHistory.shift();
            };
        }
        private function logSwitchHistory():void{
            var _local1 = "";
            var _local2:uint;
            while (_local2 < this.decisionHistory.length) {
                _local1 = (_local1 + this.decisionHistory[_local2].index);
                if (this.decisionHistory[_local2].emergency){
                    _local1 = (_local1 + " (emergency)");
                };
                _local1 = (_local1 + " -> ");
                _local2++;
            };
            logger.debug(("Current switch history: " + _local1));
        }
        private function logReliabilities(_arg1:Vector.<QualityLevel>):void{
            var _local2:QualityLevel;
            logger.debug("Current reliabilities are:");
            for each (_local2 in _arg1) {
                logger.debug((((((" Index: " + _local2.index) + "; Bitrate: ") + _local2.bitrate) + " kbps; Reliability: ") + this.getCurrentReliability(_local2.index)));
            };
        }

    }
}//package org.osmf.net.httpstreaming 

class Switch {

    public var index:uint = 0;
    public var emergency:Boolean = false;

    public function Switch(){
    }
}
