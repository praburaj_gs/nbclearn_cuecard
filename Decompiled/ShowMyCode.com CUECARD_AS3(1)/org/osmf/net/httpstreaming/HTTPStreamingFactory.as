﻿package org.osmf.net.httpstreaming {
    import org.osmf.media.*;
    import flash.errors.*;

    public class HTTPStreamingFactory {

        public function createFileHandler(_arg1:MediaResourceBase):HTTPStreamingFileHandlerBase{
            throw (new IllegalOperationError("The createFileHandler() method must be overriden by derived class."));
        }
        public function createIndexHandler(_arg1:MediaResourceBase, _arg2:HTTPStreamingFileHandlerBase):HTTPStreamingIndexHandlerBase{
            throw (new IllegalOperationError("The createIndexHandler() method must be overriden by derived class."));
        }
        public function createIndexInfo(_arg1:MediaResourceBase):HTTPStreamingIndexInfoBase{
            throw (new IllegalOperationError("The createIndexInfo() methods must be overriden by derived class."));
        }

    }
}//package org.osmf.net.httpstreaming 
