﻿package org.osmf.net.httpstreaming {

    public interface IHTTPStreamHandler {

        function get source():IHTTPStreamSource;
        function get isOpen():Boolean;
        function get streamName():String;
        function get qosInfo():HTTPStreamHandlerQoSInfo;
        function open(_arg1:String):void;
        function close():void;
        function getDVRInfo(_arg1:Object):void;
        function changeQualityLevel(_arg1:String):void;

    }
}//package org.osmf.net.httpstreaming 
