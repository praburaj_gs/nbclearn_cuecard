﻿package org.osmf.net.httpstreaming {
    import flash.events.*;
    import org.osmf.logging.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.net.httpstreaming.flv.*;
    import org.osmf.utils.*;

    public class HTTPStreamMixer extends EventDispatcher implements IHTTPStreamSource {

        private static const FILTER_NONE:uint = 0;
        private static const FILTER_VIDEO:uint = 1;
        private static const FILTER_AUDIO:uint = 2;
        private static const FILTER_DATA:uint = 4;
        private static const FILTER_ALL:uint = 0xFF;
        private static const HIGH_PRIORITY:int = 10000;
        private static const logger:Logger = Log.getLogger("org.osmf.net.httpstreaming.HTTPStreamMixer");

        private var _dispatcher:IEventDispatcher = null;
        private var _currentTime:uint = 0;
        private var _mediaTime:int = -1;
        private var _alternateTime:int = -1;
        private var _mediaTag:FLVTag = null;
        private var _mediaTagDataLoaded:Boolean = true;
        private var _mediaInput:ByteArray;
        private var _mediaFilterTags:uint = 0;
        private var _mediaHandler:IHTTPStreamHandler = null;
        private var _desiredMediaHandler:IHTTPStreamHandler = null;
        private var _mediaNeedsInitialization:Boolean = false;
        private var _mediaNeedsMoreData:Boolean = false;
        private var _alternateTag:FLVTag = null;
        private var _alternateTagDataLoaded:Boolean = true;
        private var _alternateInput:ByteArray;
        private var _alternateFilterTags:uint = 0;
        private var _alternateHandler:IHTTPStreamHandler = null;
        private var _desiredAlternateHandler:IHTTPStreamHandler = null;
        private var _alternateNeedsInitialization:Boolean = false;
        private var _alternateNeedsMoreData:Boolean = false;
        private var _alternateNeedsSynchronization:Boolean = true;
        private var _alternateIgnored:Boolean = false;
        private var _state:String = null;
        private var _fragmentDuration:Number = 0;
        private var previouslyLoggedState:String = null;
        private var checkVideoFrame:Boolean = false;
        private var droppedVideoFrames:int = 0;
        private var droppedAudioFrames:int = 0;
        private var totalDroppedAudioFrames:int = 0;
        private var totalDroppedVideoFrames:int = 0;

        public function HTTPStreamMixer(_arg1:IEventDispatcher){
            this._mediaInput = new ByteArray();
            this._alternateInput = new ByteArray();
            super();
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            this._dispatcher = _arg1;
            addEventListener(DVRStreamInfoEvent.DVRSTREAMINFO, this.onDVRStreamInfo, false, HIGH_PRIORITY, true);
            addEventListener(HTTPStreamingEvent.SCRIPT_DATA, this.onScriptData, false, HIGH_PRIORITY, true);
            addEventListener(HTTPStreamingEvent.BEGIN_FRAGMENT, this.onBeginFragment, false, HIGH_PRIORITY, true);
            addEventListener(HTTPStreamingEvent.END_FRAGMENT, this.onEndFragment, false, HIGH_PRIORITY, true);
            addEventListener(HTTPStreamingEvent.TRANSITION, this.onHTTPStreamingEvent, false, HIGH_PRIORITY, true);
            addEventListener(HTTPStreamingEvent.TRANSITION_COMPLETE, this.onHTTPStreamingEvent, false, HIGH_PRIORITY, true);
            addEventListener(HTTPStreamingEvent.DOWNLOAD_ERROR, this.onHTTPStreamingEvent, false, HIGH_PRIORITY, true);
            addEventListener(HTTPStreamingEvent.FRAGMENT_DURATION, this.onFragmentDuration, false, HIGH_PRIORITY, true);
            addEventListener(HTTPStreamingEvent.DOWNLOAD_COMPLETE, this.onHTTPStreamingEvent, false, HIGH_PRIORITY, true);
            this.setState(HTTPStreamingState.INIT);
            this._alternateIgnored = true;
        }
        public function get isReady():Boolean{
            return (((!((this.video == null))) && (this.video.source.isReady)));
        }
        public function get endOfStream():Boolean{
            return (((!((this.video == null))) && (this.video.source.endOfStream)));
        }
        public function get hasErrors():Boolean{
            return (((!((this.video == null))) && (this.video.source.hasErrors)));
        }
        public function get isLiveStalled():Boolean{
            return (((((!((this.video == null))) && (!((this.video.source == null))))) && (this.video.source.isLiveStalled)));
        }
        public function close():void{
            this.setState(HTTPStreamingState.HALT);
            this.clearBuffers();
            if (this._alternateHandler != null){
                this._alternateHandler.close();
            };
            if (((!((this._desiredAlternateHandler == null))) && (!((this._desiredAlternateHandler == this._alternateHandler))))){
                this._desiredAlternateHandler.close();
            };
            if (this._mediaHandler != null){
                this._mediaHandler.close();
            };
            if (((!((this._desiredMediaHandler == null))) && (!((this._desiredMediaHandler == this._mediaHandler))))){
                this._desiredMediaHandler.close();
            };
        }
        public function seek(_arg1:Number):void{
            this.setState(HTTPStreamingState.SEEK);
            this.clearBuffers();
            this._currentTime = 0;
            this._alternateIgnored = (this._alternateHandler == null);
            this.updateFilters();
            if (this._mediaHandler != null){
                this._mediaHandler.source.seek(_arg1);
            };
            if (((!((this._desiredMediaHandler == null))) && (!((this._desiredMediaHandler == this._mediaHandler))))){
                this._desiredMediaHandler.source.seek(_arg1);
            };
            if (this._alternateHandler != null){
                this._alternateHandler.source.seek(_arg1);
            };
            if (((!((this._desiredAlternateHandler == null))) && (!((this._desiredAlternateHandler == this._alternateHandler))))){
                this._desiredAlternateHandler.source.seek(_arg1);
            };
        }
        public function getBytes():ByteArray{
            return (this.doSomeProcessingAndGetBytes());
        }
        public function get audio():IHTTPStreamHandler{
            return (this._desiredAlternateHandler);
        }
        public function set audio(_arg1:IHTTPStreamHandler):void{
            if (this._desiredAlternateHandler != _arg1){
                logger.debug((((_arg1 == null)) ? "No audio source." : "Specific audio source selected."));
                this._desiredAlternateHandler = _arg1;
                this._alternateNeedsInitialization = true;
                this._dispatcher.dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.TRANSITION, false, false, NaN, null, null, ((this._desiredAlternateHandler)!=null) ? this._desiredAlternateHandler.streamName : null));
            };
        }
        public function get video():IHTTPStreamHandler{
            return (this._desiredMediaHandler);
        }
        public function set video(_arg1:IHTTPStreamHandler):void{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            if (this._desiredMediaHandler != _arg1){
                logger.debug("Video source selected.");
                this._desiredMediaHandler = _arg1;
                this._mediaNeedsInitialization = true;
            };
        }
        public function get fragmentDuration():Number{
            return (this._fragmentDuration);
        }
        public function get isBestEffortFetchEnabled():Boolean{
            return (((((!((this._desiredMediaHandler == null))) && (!((this._desiredMediaHandler.source == null))))) && (this._desiredMediaHandler.source.isBestEffortFetchEnabled)));
        }
        protected function doSomeProcessingAndGetBytes():ByteArray{
            var _local2:int;
            var _local3:ByteArray;
            var _local4:ByteArray;
            var _local1:ByteArray;
            switch (this._state){
                case HTTPStreamingState.INIT:
                    break;
                case HTTPStreamingState.SEEK:
                    this.setState(HTTPStreamingState.READ);
                    break;
                case HTTPStreamingState.READ:
                    _local1 = this.internalMixBytes();
                    if (_local1.length == 0){
                        _local1 = null;
                    } else {
                        _local1.position = 0;
                    };
                    if (((((this._mediaNeedsInitialization) && ((this._mediaTag == null)))) || (((this._alternateNeedsInitialization) && ((this._alternateTag == null)))))){
                        this.updateHandlers();
                        this.updateFilters();
                    };
                    if (((this._alternateNeedsSynchronization) && (((!((this._mediaTime == -1))) || (!((this._alternateTime == -1))))))){
                        this._alternateNeedsSynchronization = false;
                        if (this._alternateHandler != null){
                            _local2 = ((this._alternateTime)!=-1) ? this._alternateTime : this._mediaTime;
                            logger.debug((((((("Synchronizing alternate packets. (currentTime = " + this._currentTime) + ", mediaTime = ") + this._mediaTime) + ", alternateSyncTime =") + _local2) + ")."));
                            this._alternateHandler.source.seek((_local2 / 1000));
                        };
                    };
                    this._mediaNeedsMoreData = ((((!(this._mediaTagDataLoaded)) || ((this._mediaInput.bytesAvailable == 0)))) || (((!((this._mediaInput.bytesAvailable == 0))) && ((this._mediaTag == null)))));
                    this._alternateNeedsMoreData = ((((!(this._alternateTagDataLoaded)) || ((this._alternateInput.bytesAvailable == 0)))) || (((!((this._alternateInput.bytesAvailable == 0))) && ((this._alternateTag == null)))));
                    if (((this._mediaNeedsMoreData) || (this._alternateNeedsMoreData))){
                        _local3 = null;
                        if (((((((!(this._alternateIgnored)) && (this._alternateNeedsMoreData))) && (!((this._alternateHandler == null))))) && (this._alternateHandler.source.isReady))){
                            _local3 = this._alternateHandler.source.getBytes();
                            if ((((_local3 == null)) && (((this._alternateHandler.source.hasErrors) || (this._alternateHandler.source.endOfStream))))){
                                logger.debug("Alternate audio track unavailable.");
                                this._dispatcher.dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.ACTION_NEEDED, false, false, NaN, null, null, ((this._alternateHandler)!=null) ? this._alternateHandler.streamName : null));
                                this._alternateIgnored = true;
                                this.updateFilters();
                            };
                        };
                        _local4 = null;
                        if (((((this._mediaNeedsMoreData) && (!((this._mediaHandler == null))))) && (this._mediaHandler.source.isReady))){
                            _local4 = this._mediaHandler.source.getBytes();
                        };
                        if (((!((_local4 == null))) || (!((_local3 == null))))){
                            this.updateBuffers(_local4, _local3);
                        };
                    };
                    break;
            };
            return (_local1);
        }
        private function internalMixBytes():ByteArray{
            var _local3:int;
            var _local4:Number;
            var _local1:ByteArray = new ByteArray();
            var _local2:Boolean;
            while (_local2) {
                if (this._mediaTag == null){
                    this._mediaTagDataLoaded = false;
                };
                while (((!(this._mediaTagDataLoaded)) && (this._mediaInput.bytesAvailable))) {
                    if ((((this._mediaTag == null)) && ((this._mediaInput.bytesAvailable < FLVTag.TAG_HEADER_BYTE_COUNT)))){
                        return (_local1);
                    };
                    if (this._mediaTag == null){
                        this._mediaTag = this.createTag(this._mediaInput.readByte());
                        this._mediaTag.readRemainingHeader(this._mediaInput);
                    };
                    if (!this._mediaTagDataLoaded){
                        if (this._mediaInput.bytesAvailable < (this._mediaTag.dataSize + FLVTag.PREV_TAG_BYTE_COUNT)){
                            return (_local1);
                        };
                        if (this.shouldFilterTag(this._mediaTag, this._mediaFilterTags)){
                            if ((this._mediaTag is FLVTagVideo)){
                                this.droppedVideoFrames++;
                                this.totalDroppedVideoFrames++;
                            };
                            this._mediaInput.position = (this._mediaInput.position + (this._mediaTag.dataSize + FLVTag.PREV_TAG_BYTE_COUNT));
                            this._mediaTag = null;
                        } else {
                            this._mediaTag.readData(this._mediaInput);
                            this._mediaTag.readPrevTag(this._mediaInput);
                            this._mediaTagDataLoaded = true;
                            this.updateTimes(this._mediaTag);
                        };
                    };
                };
                if (this._alternateTag == null){
                    this._alternateTagDataLoaded = false;
                };
                while (((((!(this._alternateIgnored)) && (!(this._alternateTagDataLoaded)))) && (this._alternateInput.bytesAvailable))) {
                    if ((((this._alternateTag == null)) && ((this._alternateInput.bytesAvailable < FLVTag.TAG_HEADER_BYTE_COUNT)))){
                        return (_local1);
                    };
                    if (this._alternateTag == null){
                        this._alternateTag = this.createTag(this._alternateInput.readByte());
                        this._alternateTag.readRemainingHeader(this._alternateInput);
                    };
                    if (!this._alternateTagDataLoaded){
                        if (this._alternateInput.bytesAvailable < (this._alternateTag.dataSize + FLVTag.PREV_TAG_BYTE_COUNT)){
                            return (_local1);
                        };
                        if (this.shouldFilterTag(this._alternateTag, this._alternateFilterTags)){
                            if ((this._alternateTag is FLVTagAudio)){
                                this.droppedAudioFrames++;
                                this.totalDroppedAudioFrames++;
                            };
                            this._alternateInput.position = (this._alternateInput.position + (this._alternateTag.dataSize + FLVTag.PREV_TAG_BYTE_COUNT));
                            this._alternateTag = null;
                        } else {
                            this._alternateTag.readData(this._alternateInput);
                            this._alternateTag.readPrevTag(this._alternateInput);
                            this._alternateTagDataLoaded = true;
                            this.updateTimes(this._alternateTag);
                        };
                    };
                };
                if (((this._mediaTagDataLoaded) || (this._alternateTagDataLoaded))){
                    if (((this.checkVideoFrame) && ((this._mediaTag is FLVTagVideo)))){
                        this.checkVideoFrame = false;
                        _local3 = FLVTagVideo(this._mediaTag).frameType;
                        _local4 = this._mediaTag.timestamp;
                        if (_local3 != FLVTagVideo.FRAME_TYPE_KEYFRAME){
                            logger.warn((("Frame at " + _local4) + " is not a key frame. This could lead to video not being displayed."));
                        };
                    };
                    if (this._alternateIgnored){
                        this._currentTime = this._mediaTag.timestamp;
                        this._mediaTag.write(_local1);
                        this._mediaTag = null;
                        _local2 = true;
                    } else {
                        if ((((this._mediaTime > -1)) || ((this._alternateTime > -1)))){
                            if (((((((!((this._alternateTag == null))) && (this._alternateTagDataLoaded))) && ((this._alternateTag.timestamp >= this._currentTime)))) && ((this._alternateTag.timestamp <= this._mediaTime)))){
                                this._currentTime = this._alternateTag.timestamp;
                                this._alternateTag.write(_local1);
                                this._alternateTag = null;
                            } else {
                                if (((((((!((this._mediaTag == null))) && (this._mediaTagDataLoaded))) && ((this._mediaTag.timestamp >= this._currentTime)))) && ((this._mediaTag.timestamp <= this._alternateTime)))){
                                    this._currentTime = this._mediaTag.timestamp;
                                    this._mediaTag.write(_local1);
                                    this._mediaTag = null;
                                };
                            };
                            _local2 = ((this._mediaInput.bytesAvailable) && (this._alternateInput.bytesAvailable));
                        } else {
                            if (((!((this._alternateTime == -1))) && (this._alternateNeedsSynchronization))){
                                this._alternateNeedsSynchronization = false;
                            };
                            _local2 = false;
                        };
                    };
                } else {
                    _local2 = false;
                };
            };
            return (_local1);
        }
        private function updateBuffers(_arg1:IDataInput, _arg2:IDataInput):void{
            var _local3:ByteArray = new ByteArray();
            if ((((((this._mediaTag == null)) && (!((this._mediaInput.position == 0))))) && ((this._mediaInput.bytesAvailable < (this._mediaInput.length / 2))))){
                this._mediaInput.readBytes(_local3, 0, this._mediaInput.bytesAvailable);
                this._mediaInput.clear();
                _local3.readBytes(this._mediaInput, 0, _local3.bytesAvailable);
                _local3.clear();
            };
            if ((((((this._alternateTag == null)) && (!((this._alternateInput.position == 0))))) && ((this._alternateInput.bytesAvailable < (this._alternateInput.length / 2))))){
                this._alternateInput.readBytes(_local3, 0, this._alternateInput.bytesAvailable);
                this._alternateInput.clear();
                _local3.readBytes(this._alternateInput, 0, _local3.bytesAvailable);
                _local3.clear();
            };
            if (((!((_arg1 == null))) && (_arg1.bytesAvailable))){
                _arg1.readBytes(this._mediaInput, this._mediaInput.length, _arg1.bytesAvailable);
            };
            if (((!((_arg2 == null))) && (_arg2.bytesAvailable))){
                _arg2.readBytes(this._alternateInput, this._alternateInput.length, _arg2.bytesAvailable);
            };
        }
        private function clearBuffers():void{
            this.clearAlternateBuffers();
            this.clearMediaBuffers();
        }
        private function clearAlternateBuffers():void{
            this._alternateTime = -1;
            this._alternateTag = null;
            this._alternateTagDataLoaded = false;
            this._alternateInput.clear();
        }
        private function clearMediaBuffers():void{
            this._mediaTime = -1;
            this._mediaTag = null;
            this._mediaTagDataLoaded = false;
            this._mediaInput.clear();
        }
        private function createTag(_arg1:int):FLVTag{
            var _local2:FLVTag;
            switch (_arg1){
                case FLVTag.TAG_TYPE_AUDIO:
                case FLVTag.TAG_TYPE_ENCRYPTED_AUDIO:
                    _local2 = new FLVTagAudio(_arg1);
                    break;
                case FLVTag.TAG_TYPE_VIDEO:
                case FLVTag.TAG_TYPE_ENCRYPTED_VIDEO:
                    _local2 = new FLVTagVideo(_arg1);
                    break;
                case FLVTag.TAG_TYPE_SCRIPTDATAOBJECT:
                case FLVTag.TAG_TYPE_ENCRYPTED_SCRIPTDATAOBJECT:
                    _local2 = new FLVTagScriptDataObject(_arg1);
                    break;
                default:
                    _local2 = new FLVTag(_arg1);
            };
            return (_local2);
        }
        private function shouldFilterTag(_arg1:FLVTag, _arg2:uint):Boolean{
            if (_arg1 == null){
                return (true);
            };
            if (_arg1.timestamp < this._currentTime){
                return (true);
            };
            switch (_arg1.tagType){
                case FLVTag.TAG_TYPE_AUDIO:
                case FLVTag.TAG_TYPE_ENCRYPTED_AUDIO:
                    return ((((FILTER_AUDIO & _arg2)) || ((_arg1.timestamp < this._alternateTime))));
                case FLVTag.TAG_TYPE_VIDEO:
                case FLVTag.TAG_TYPE_ENCRYPTED_VIDEO:
                    return ((((FILTER_VIDEO & _arg2)) || ((_arg1.timestamp < this._mediaTime))));
                case FLVTag.TAG_TYPE_SCRIPTDATAOBJECT:
                case FLVTag.TAG_TYPE_ENCRYPTED_SCRIPTDATAOBJECT:
                    return ((((FILTER_DATA & _arg2)) || ((_arg1.timestamp < this._mediaTime))));
            };
            return (false);
        }
        private function updateTimes(_arg1:FLVTag):void{
            if (_arg1 != null){
                if ((_arg1 is FLVTagAudio)){
                    this._alternateTime = _arg1.timestamp;
                } else {
                    this._mediaTime = _arg1.timestamp;
                };
            };
        }
        private function updateHandlers():void{
            if (this._mediaNeedsInitialization){
                if (this._mediaHandler != this._desiredMediaHandler){
                    if (this._mediaHandler != null){
                        this._mediaHandler.close();
                        this._mediaHandler = null;
                    };
                    this._mediaHandler = this._desiredMediaHandler;
                    this.clearMediaBuffers();
                };
                this._mediaNeedsInitialization = false;
            };
            if (this._alternateNeedsInitialization){
                if (this._alternateHandler != this._desiredAlternateHandler){
                    if (this._alternateHandler != null){
                        this._alternateHandler.close();
                        this._alternateHandler = null;
                    };
                    this._alternateHandler = this._desiredAlternateHandler;
                    this.clearAlternateBuffers();
                    this._alternateNeedsSynchronization = true;
                    this._alternateIgnored = (this._alternateHandler == null);
                };
                this._alternateNeedsInitialization = false;
                this._dispatcher.dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.TRANSITION_COMPLETE, false, false, NaN, null, null, ((this._alternateHandler)!=null) ? this._alternateHandler.streamName : null));
            };
        }
        private function updateFilters():void{
            if (this._alternateIgnored){
                this._mediaFilterTags = FILTER_NONE;
                this._alternateFilterTags = FILTER_ALL;
            } else {
                this._mediaFilterTags = FILTER_AUDIO;
                this._alternateFilterTags = FILTER_VIDEO;
            };
        }
        private function setState(_arg1:String):void{
            this._state = _arg1;
            if (this._state != this.previouslyLoggedState){
                logger.debug(("State = " + this._state));
                this.previouslyLoggedState = this._state;
            };
        }
        private function onScriptData(_arg1:HTTPStreamingEvent):void{
            var _local2:*;
            if (((!((this._alternateHandler == null))) && ((this._alternateHandler.streamName == _arg1.url)))){
                _local2 = _arg1.scriptDataObject.objects[0];
                if ((((_local2 == "onMetaData")) || ((_local2 == "onXMPData")))){
                    return;
                };
            };
            this._dispatcher.dispatchEvent(_arg1);
        }
        private function onDVRStreamInfo(_arg1:DVRStreamInfoEvent):void{
            this._dispatcher.dispatchEvent(_arg1);
        }
        private function onBeginFragment(_arg1:HTTPStreamingEvent):void{
            if (((!((this._mediaHandler == null))) && ((this._mediaHandler.streamName == _arg1.url)))){
                if (((!((this._alternateHandler == null))) && (this._alternateIgnored))){
                    this._alternateIgnored = false;
                    this._alternateNeedsSynchronization = true;
                };
                logger.debug((((((((("dvf=" + this.droppedVideoFrames) + "(") + this.totalDroppedVideoFrames) + "), daf=") + this.droppedAudioFrames) + "(") + this.totalDroppedAudioFrames) + ")."));
                this.droppedVideoFrames = 0;
                this.droppedVideoFrames = 0;
                this.checkVideoFrame = true;
                this._dispatcher.dispatchEvent(_arg1);
            };
        }
        private function onEndFragment(_arg1:HTTPStreamingEvent):void{
            if (((!((this._mediaHandler == null))) && ((this._mediaHandler.streamName == _arg1.url)))){
                this._dispatcher.dispatchEvent(_arg1);
            };
        }
        private function onHTTPStreamingEvent(_arg1:HTTPStreamingEvent):void{
            this._dispatcher.dispatchEvent(_arg1);
        }
        private function onFragmentDuration(_arg1:HTTPStreamingEvent):void{
            if (((!((this._mediaHandler == null))) && ((this._mediaHandler.streamName == _arg1.url)))){
                this._fragmentDuration = _arg1.fragmentDuration;
            };
        }

    }
}//package org.osmf.net.httpstreaming 
