﻿package org.osmf.net.httpstreaming {
    import flash.events.*;
    import flash.errors.*;

    public class HTTPStreamingIndexHandlerBase extends EventDispatcher {

        public function initialize(_arg1:Object):void{
            throw (new IllegalOperationError("The initialize() method must be overridden by the derived class."));
        }
        public function dispose():void{
            throw (new IllegalOperationError("The dispose() method must be overridden by the derived class."));
        }
        public function processIndexData(_arg1, _arg2:Object):void{
            throw (new IllegalOperationError("The processIndexData() method must be overridden by the derived class."));
        }
        public function getFileForTime(_arg1:Number, _arg2:int):HTTPStreamRequest{
            throw (new IllegalOperationError("The getFileForTime() method must be overridden by the derived class."));
        }
        public function getNextFile(_arg1:int):HTTPStreamRequest{
            throw (new IllegalOperationError("The getNextFile() method must be overridden by the derived class."));
        }
        public function dvrGetStreamInfo(_arg1:Object):void{
            throw (new IllegalOperationError("The dvrGetStreamInfo() method must be overridden by the derived class."));
        }
        public function get isBestEffortFetchEnabled():Boolean{
            throw (new IllegalOperationError("The isBestEffortFetchEnabled method must be overridden by the derived class."));
        }

    }
}//package org.osmf.net.httpstreaming 
