﻿package org.osmf.net.httpstreaming.f4f {

    class LocalRandomAccessEntry {

        private var _time:Number;
        private var _offset:Number;

        public function get time():Number{
            return (this._time);
        }
        public function set time(_arg1:Number):void{
            this._time = _arg1;
        }
        public function get offset():Number{
            return (this._offset);
        }
        public function set offset(_arg1:Number):void{
            this._offset = _arg1;
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
