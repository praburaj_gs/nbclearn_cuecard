﻿package org.osmf.net.httpstreaming.f4f {
    import flash.events.*;
    import __AS3__.vec.*;
    import flash.utils.*;
    import org.osmf.net.httpstreaming.f4f.*;
    import flash.errors.*;

    class BoxParser extends EventDispatcher {

        private static const FULL_BOX_FIELD_FLAGS_LENGTH:uint = 3;
        private static const AFRA_MASK_LONG_ID:uint = 128;
        private static const AFRA_MASK_LONG_OFFSET:uint = 64;
        private static const AFRA_MASK_GLOBAL_ENTRIES:uint = 32;

        private var _ba:ByteArray;

        public function BoxParser(){
            this._ba = null;
        }
        public function init(_arg1:ByteArray):void{
            this._ba = _arg1;
            this._ba.position = 0;
        }
        public function getNextBoxInfo():BoxInfo{
            if ((((this._ba == null)) || ((this._ba.bytesAvailable < (F4FConstants.FIELD_SIZE_LENGTH + F4FConstants.FIELD_TYPE_LENGTH))))){
                return (null);
            };
            var _local1:Number = this._ba.readUnsignedInt();
            var _local2:String = this._ba.readUTFBytes(F4FConstants.FIELD_TYPE_LENGTH);
            return (new BoxInfo(_local1, _local2));
        }
        public function getBoxes():Vector.<Box>{
            var _local3:AdobeBootstrapBox;
            var _local4:AdobeFragmentRandomAccessBox;
            var _local5:MediaDataBox;
            var _local1:Vector.<Box> = new Vector.<Box>();
            var _local2:BoxInfo = this.getNextBoxInfo();
            while (_local2 != null) {
                if (_local2.type == F4FConstants.BOX_TYPE_ABST){
                    _local3 = new AdobeBootstrapBox();
                    this.parseAdobeBootstrapBox(_local2, _local3);
                    _local1.push(_local3);
                } else {
                    if (_local2.type == F4FConstants.BOX_TYPE_AFRA){
                        _local4 = new AdobeFragmentRandomAccessBox();
                        this.parseAdobeFragmentRandomAccessBox(_local2, _local4);
                        _local1.push(_local4);
                    } else {
                        if (_local2.type == F4FConstants.BOX_TYPE_MDAT){
                            _local5 = new MediaDataBox();
                            this.parseMediaDataBox(_local2, _local5);
                            _local1.push(_local5);
                        } else {
                            this._ba.position = ((this._ba.position + _local2.size) - (F4FConstants.FIELD_SIZE_LENGTH + F4FConstants.FIELD_TYPE_LENGTH));
                        };
                    };
                };
                _local2 = this.getNextBoxInfo();
                if (((!((_local2 == null))) && ((_local2.size <= 0)))){
                    break;
                };
            };
            return (_local1);
        }
        public function readFragmentRandomAccessBox(_arg1:BoxInfo):AdobeFragmentRandomAccessBox{
            var _local2:AdobeFragmentRandomAccessBox = new AdobeFragmentRandomAccessBox();
            this.parseAdobeFragmentRandomAccessBox(_arg1, _local2);
            return (_local2);
        }
        public function readAdobeBootstrapBox(_arg1:BoxInfo):AdobeBootstrapBox{
            var _local2:AdobeBootstrapBox = new AdobeBootstrapBox();
            this.parseAdobeBootstrapBox(_arg1, _local2);
            return (_local2);
        }
        function readLongUIntToNumber():Number{
            if ((((this._ba == null)) || ((this._ba.bytesAvailable < 8)))){
                throw (new IllegalOperationError("not enough length for readLongUIntToNumer"));
            };
            var _local1:Number = this._ba.readUnsignedInt();
            _local1 = (_local1 * 4294967296);
            _local1 = (_local1 + this._ba.readUnsignedInt());
            return (_local1);
        }
        private function readUnsignedInt():uint{
            if ((((this._ba == null)) || ((this._ba.bytesAvailable < 4)))){
                throw (new IllegalOperationError("not enough length for readUnsignedInt"));
            };
            return (this._ba.readUnsignedInt());
        }
        private function readBytes(_arg1:ByteArray, _arg2:uint=0, _arg3:uint=0):void{
            if ((((this._ba == null)) || ((this._ba.bytesAvailable < _arg3)))){
                throw (new IllegalOperationError(("not enough length for readBytes: " + _arg3)));
            };
            return (this._ba.readBytes(_arg1, _arg2, _arg3));
        }
        private function readUnsignedByte():uint{
            if ((((this._ba == null)) || ((this._ba.bytesAvailable < 1)))){
                throw (new IllegalOperationError("not enough length for readUnsingedByte"));
            };
            return (this._ba.readUnsignedByte());
        }
        private function readBytesToUint(_arg1:uint):uint{
            var _local4:uint;
            if ((((this._ba == null)) || ((this._ba.bytesAvailable < _arg1)))){
                throw (new IllegalOperationError("not enough length for readUnsingedByte"));
            };
            if (_arg1 > 4){
                throw (new IllegalOperationError("number of bytes to read must be equal or less than 4"));
            };
            var _local2:uint;
            var _local3:uint;
            while (_local3 < _arg1) {
                _local2 = (_local2 << 8);
                _local4 = this._ba.readUnsignedByte();
                _local2 = (_local2 + _local4);
                _local3++;
            };
            return (_local2);
        }
        private function readString():String{
            var _local3:uint;
            var _local1:uint = this._ba.position;
            while (this._ba.position < this._ba.length) {
                _local3 = this._ba.readByte();
                if (_local3 == 0){
                    break;
                };
            };
            var _local2:uint = (this._ba.position - _local1);
            this._ba.position = _local1;
            return (this._ba.readUTFBytes(_local2));
        }
        private function parseBox(_arg1:BoxInfo, _arg2:Box):void{
            var _local5:ByteArray;
            var _local3:Number = _arg1.size;
            var _local4:uint = (F4FConstants.FIELD_SIZE_LENGTH + F4FConstants.FIELD_TYPE_LENGTH);
            if (_arg1.size == F4FConstants.FLAG_USE_LARGE_SIZE){
                _local3 = this.readLongUIntToNumber();
                _local4 = (_local4 + F4FConstants.FIELD_LARGE_SIZE_LENGTH);
            };
            if (_arg1.type == F4FConstants.EXTENDED_TYPE){
                _local5 = new ByteArray();
                this.readBytes(_local5, 0, F4FConstants.FIELD_EXTENDED_TYPE_LENGTH);
                _local4 = (_local4 + F4FConstants.FIELD_EXTENDED_TYPE_LENGTH);
            };
            _arg2.size = _local3;
            _arg2.type = _arg1.type;
            _arg2.boxLength = _local4;
        }
        private function parseFullBox(_arg1:BoxInfo, _arg2:FullBox):void{
            this.parseBox(_arg1, _arg2);
            _arg2.version = this.readUnsignedByte();
            _arg2.flags = this.readBytesToUint(FULL_BOX_FIELD_FLAGS_LENGTH);
        }
        private function parseAdobeBootstrapBox(_arg1:BoxInfo, _arg2:AdobeBootstrapBox):void{
            var _local13:AdobeSegmentRunTable;
            var _local14:AdobeFragmentRunTable;
            this.parseFullBox(_arg1, _arg2);
            _arg2.bootstrapVersion = this.readUnsignedInt();
            var _local3:uint = this.readUnsignedByte();
            _arg2.profile = (_local3 >> 6);
            _arg2.live = ((_local3 & 32) == 32);
            _arg2.update = ((_local3 & 1) == 1);
            _arg2.timeScale = this.readUnsignedInt();
            _arg2.currentMediaTime = this.readLongUIntToNumber();
            _arg2.smpteTimeCodeOffset = this.readLongUIntToNumber();
            _arg2.movieIdentifier = this.readString();
            var _local4:uint = this.readUnsignedByte();
            var _local5:Vector.<String> = new Vector.<String>();
            var _local6:int;
            while (_local6 < _local4) {
                _local5.push(this.readString());
                _local6++;
            };
            _arg2.serverBaseURLs = _local5;
            var _local7:uint = this.readUnsignedByte();
            var _local8:Vector.<String> = new Vector.<String>();
            _local6 = 0;
            while (_local6 < _local7) {
                _local8.push(this.readString());
                _local6++;
            };
            _arg2.qualitySegmentURLModifiers = _local8;
            _arg2.drmData = this.readString();
            _arg2.metadata = this.readString();
            var _local9:uint = this.readUnsignedByte();
            var _local10:Vector.<AdobeSegmentRunTable> = new Vector.<AdobeSegmentRunTable>();
            _local6 = 0;
            while (_local6 < _local9) {
                _arg1 = this.getNextBoxInfo();
                if (_arg1.type == F4FConstants.BOX_TYPE_ASRT){
                    _local13 = new AdobeSegmentRunTable();
                    this.parseAdobeSegmentRunTable(_arg1, _local13);
                    _local10.push(_local13);
                } else {
                    throw (new IllegalOperationError(("Unexpected data structure: " + _arg1.type)));
                };
                _local6++;
            };
            _arg2.segmentRunTables = _local10;
            var _local11:uint = this.readUnsignedByte();
            var _local12:Vector.<AdobeFragmentRunTable> = new Vector.<AdobeFragmentRunTable>();
            _local6 = 0;
            while (_local6 < _local11) {
                _arg1 = this.getNextBoxInfo();
                if (_arg1.type == F4FConstants.BOX_TYPE_AFRT){
                    _local14 = new AdobeFragmentRunTable();
                    this.parseAdobeFragmentRunTable(_arg1, _local14);
                    _local12.push(_local14);
                } else {
                    throw (new IllegalOperationError(("Unexpected data structure: " + _arg1.type)));
                };
                _local6++;
            };
            _arg2.fragmentRunTables = _local12;
        }
        private function parseAdobeSegmentRunTable(_arg1:BoxInfo, _arg2:AdobeSegmentRunTable):void{
            this.parseFullBox(_arg1, _arg2);
            var _local3:uint = this.readUnsignedByte();
            var _local4:Vector.<String> = new Vector.<String>();
            var _local5:uint;
            while (_local5 < _local3) {
                _local4.push(this.readString());
                _local5++;
            };
            _arg2.qualitySegmentURLModifiers = _local4;
            var _local6:uint = this.readUnsignedInt();
            _local5 = 0;
            while (_local5 < _local6) {
                _arg2.addSegmentFragmentPair(new SegmentFragmentPair(this.readUnsignedInt(), this.readUnsignedInt()));
                _local5++;
            };
        }
        private function parseAdobeFragmentRunTable(_arg1:BoxInfo, _arg2:AdobeFragmentRunTable):void{
            var _local7:FragmentDurationPair;
            this.parseFullBox(_arg1, _arg2);
            _arg2.timeScale = this.readUnsignedInt();
            var _local3:uint = this.readUnsignedByte();
            var _local4:Vector.<String> = new Vector.<String>();
            var _local5:uint;
            while (_local5 < _local3) {
                _local4.push(this.readString());
                _local5++;
            };
            _arg2.qualitySegmentURLModifiers = _local4;
            var _local6:uint = this.readUnsignedInt();
            _local5 = 0;
            while (_local5 < _local6) {
                _local7 = new FragmentDurationPair();
                this.parseFragmentDurationPair(_local7);
                _arg2.addFragmentDurationPair(_local7);
                _local5++;
            };
        }
        private function parseFragmentDurationPair(_arg1:FragmentDurationPair):void{
            _arg1.firstFragment = this.readUnsignedInt();
            _arg1.durationAccrued = this.readLongUIntToNumber();
            _arg1.duration = this.readUnsignedInt();
            if (_arg1.duration == 0){
                _arg1.discontinuityIndicator = this.readUnsignedByte();
            };
        }
        private function parseAdobeFragmentRandomAccessBox(_arg1:BoxInfo, _arg2:AdobeFragmentRandomAccessBox):void{
            var _local11:LocalRandomAccessEntry;
            var _local12:GlobalRandomAccessEntry;
            this.parseFullBox(_arg1, _arg2);
            var _local3:uint = this.readBytesToUint(1);
            var _local4 = ((_local3 & AFRA_MASK_LONG_ID) > 0);
            var _local5 = ((_local3 & AFRA_MASK_LONG_OFFSET) > 0);
            var _local6 = ((_local3 & AFRA_MASK_GLOBAL_ENTRIES) > 0);
            _arg2.timeScale = this.readUnsignedInt();
            var _local7:uint = this.readUnsignedInt();
            var _local8:Vector.<LocalRandomAccessEntry> = new Vector.<LocalRandomAccessEntry>();
            var _local9:uint;
            while (_local9 < _local7) {
                _local11 = new LocalRandomAccessEntry();
                this.parseLocalRandomAccessEntry(_local11, _local5);
                _local8.push(_local11);
                _local9++;
            };
            _arg2.localRandomAccessEntries = _local8;
            var _local10:Vector.<GlobalRandomAccessEntry> = new Vector.<GlobalRandomAccessEntry>();
            if (_local6){
                _local7 = this.readUnsignedInt();
                _local9 = 0;
                while (_local9 < _local7) {
                    _local12 = new GlobalRandomAccessEntry();
                    this.parseGlobalRandomAccessEntry(_local12, _local4, _local5);
                    _local10.push(_local12);
                    _local9++;
                };
            };
            _arg2.globalRandomAccessEntries = _local10;
        }
        private function parseLocalRandomAccessEntry(_arg1:LocalRandomAccessEntry, _arg2:Boolean):void{
            _arg1.time = this.readLongUIntToNumber();
            if (_arg2){
                _arg1.offset = this.readLongUIntToNumber();
            } else {
                _arg1.offset = this.readUnsignedInt();
            };
        }
        private function parseGlobalRandomAccessEntry(_arg1:GlobalRandomAccessEntry, _arg2:Boolean, _arg3:Boolean):void{
            _arg1.time = this.readLongUIntToNumber();
            if (_arg2){
                _arg1.segment = this.readUnsignedInt();
                _arg1.fragment = this.readUnsignedInt();
            } else {
                _arg1.segment = this.readBytesToUint(2);
                _arg1.fragment = this.readBytesToUint(2);
            };
            if (_arg3){
                _arg1.afraOffset = this.readLongUIntToNumber();
                _arg1.offsetFromAfra = this.readLongUIntToNumber();
            } else {
                _arg1.afraOffset = this.readUnsignedInt();
                _arg1.offsetFromAfra = this.readUnsignedInt();
            };
        }
        private function parseMediaDataBox(_arg1:BoxInfo, _arg2:MediaDataBox):void{
            this.parseBox(_arg1, _arg2);
            var _local3:ByteArray = new ByteArray();
            this.readBytes(_local3, 0, (_arg2.size - _arg2.boxLength));
            _arg2.data = _local3;
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
