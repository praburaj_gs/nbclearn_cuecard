﻿package org.osmf.net.httpstreaming.f4f {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.elements.f4mClasses.*;
    import org.osmf.net.httpstreaming.*;
    import flash.net.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.net.dvr.*;
    import org.osmf.net.httpstreaming.dvr.*;
    import org.osmf.net.httpstreaming.flv.*;
    import org.osmf.utils.*;

    public class HTTPStreamingF4FIndexHandler extends HTTPStreamingIndexHandlerBase {

        public static const DEFAULT_FRAGMENTS_THRESHOLD:uint = 5;
        private static const BEST_EFFORT_STATE_OFF:String = "off";
        private static const BEST_EFFORT_STATE_PLAY:String = "play";
        private static const BEST_EFFORT_STATE_SEEK_BACKWARD:String = "seekBackward";
        private static const BEST_EFFORT_STATE_SEEK_FORWARD:String = "seekForward";
        private static const BEST_EFFORT_PLAY_SITUAUTION_NORMAL:String = "normal";
        private static const BEST_EFFORT_PLAY_SITUAUTION_DROPOUT:String = "dropout";
        private static const BEST_EFFORT_PLAY_SITUAUTION_LIVENESS:String = "liveness";
        private static const BEST_EFFORT_PLAY_SITUAUTION_DONE:String = "done";

        private var _currentQuality:int = -1;
        private var _currentAdditionalHeader:ByteArray = null;
        private var _currentFAI:FragmentAccessInformation = null;
        private var _pureLiveOffset:Number = NaN;
        private var _f4fIndexInfo:HTTPStreamingF4FIndexInfo = null;
        private var _bootstrapBoxes:Vector.<AdobeBootstrapBox> = null;
        private var _bootstrapBoxesURLs:Vector.<String> = null;
        private var _streamInfos:Vector.<HTTPStreamingF4FStreamInfo> = null;
        private var _streamNames:Array = null;
        private var _streamQualityRates:Array = null;
        private var _serverBaseURL:String = null;
        private var _delay:Number = 0.05;
        private var _indexUpdating:Boolean = false;
        private var _pendingIndexLoads:int = 0;
        private var _pendingIndexUpdates:int = 0;
        private var _pendingIndexUrls:Object;
        private var _invokedFromDvrGetStreamInfo:Boolean = false;
        private var playInProgress:Boolean;
        private var bootstrapUpdateTimer:Timer;
        private var bootstrapUpdateInterval:Number = 4000;
        private var _bestEffortInited:Boolean = false;
        private var _bestEffortEnabled:Boolean = false;
        private var _bestEffortState:String = "off";
        private var _bestEffortSeekTime:Number = 0;
        private var _bestEffortDownloaderMonitor:EventDispatcher;
        private var _bestEffortFailedFetches:uint = 0;
        private var _bestEffortDownloadReply:String = null;
        private var _bestEffortNeedsToFireFragmentDuration:Boolean = false;
        private var _bestEffortF4FHandler:HTTPStreamingF4FFileHandler;
        private var _bestEffortSeekBuffer:ByteArray;
        private var _bestEffortNotifyBootstrapBoxInfo:Object = null;
        private var _bestEffortLivenessRestartPoint:uint = 0;
        private var _bestEffortLastGoodFragmentDownloadTime:Date = null;

        public function HTTPStreamingF4FIndexHandler(_arg1:HTTPStreamingFileHandlerBase, _arg2:uint=5){
            this._pendingIndexUrls = new Object();
            this._bestEffortDownloaderMonitor = new EventDispatcher();
            this._bestEffortF4FHandler = new HTTPStreamingF4FFileHandler();
            this._bestEffortSeekBuffer = new ByteArray();
            super();
            _arg1.addEventListener(HTTPStreamingFileHandlerEvent.NOTIFY_BOOTSTRAP_BOX, this.onBootstrapBox);
            this._bestEffortF4FHandler.addEventListener(HTTPStreamingFileHandlerEvent.NOTIFY_BOOTSTRAP_BOX, this.onBestEffortF4FHandlerNotifyBootstrapBox);
        }
        override public function dvrGetStreamInfo(_arg1:Object):void{
            this._invokedFromDvrGetStreamInfo = true;
            this.playInProgress = false;
            this.initialize(_arg1);
        }
        override public function initialize(_arg1:Object):void{
            var _local2:AdobeBootstrapBox;
            var _local5:HTTPStreamingF4FStreamInfo;
            var _local6:BootstrapInfo;
            this._f4fIndexInfo = (_arg1 as HTTPStreamingF4FIndexInfo);
            if ((((((this._f4fIndexInfo == null)) || ((this._f4fIndexInfo.streamInfos == null)))) || ((this._f4fIndexInfo.streamInfos.length <= 0)))){
                dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.INDEX_ERROR));
                return;
            };
            this._indexUpdating = false;
            this._pendingIndexLoads = 0;
            this._pendingIndexUpdates = 0;
            this._pendingIndexUrls = new Object();
            this.playInProgress = false;
            this._pureLiveOffset = NaN;
            this._serverBaseURL = this._f4fIndexInfo.serverBaseURL;
            this._streamInfos = this._f4fIndexInfo.streamInfos;
            var _local3:int = this._streamInfos.length;
            this._streamQualityRates = [];
            this._streamNames = [];
            this._bootstrapBoxesURLs = new Vector.<String>(_local3);
            this._bootstrapBoxes = new Vector.<AdobeBootstrapBox>(_local3);
            var _local4:int;
            while (_local4 < _local3) {
                _local5 = this._streamInfos[_local4];
                if (_local5 != null){
                    this._streamQualityRates[_local4] = _local5.bitrate;
                    this._streamNames[_local4] = _local5.streamName;
                    _local6 = _local5.bootstrapInfo;
                    if ((((_local6 == null)) || ((((_local6.url == null)) && ((_local6.data == null)))))){
                        dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.INDEX_ERROR));
                        return;
                    };
                    if (_local6.data != null){
                        _local2 = this.processBootstrapData(_local6.data, _local4);
                        if (_local2 == null){
                            dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.INDEX_ERROR));
                            return;
                        };
                        this._bootstrapBoxes[_local4] = _local2;
                    } else {
                        this._bootstrapBoxesURLs[_local4] = HTTPStreamingUtils.normalizeURL(_local6.url);
                        this._pendingIndexLoads++;
                        this.dispatchIndexLoadRequest(_local4);
                    };
                };
                _local4++;
            };
            if (this._pendingIndexLoads == 0){
                this.notifyRatesReady();
                this.notifyIndexReady(0);
            };
        }
        override public function dispose():void{
            this.destroyBootstrapUpdateTimer();
        }
        override public function processIndexData(_arg1, _arg2:Object):void{
            var _local5:String;
            var _local3:int = (_arg2 as int);
            var _local4:AdobeBootstrapBox = this.processBootstrapData(_arg1, _local3);
            if (_local4 == null){
                dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.INDEX_ERROR));
                return;
            };
            if (!this._indexUpdating){
                this._pendingIndexLoads--;
            } else {
                this._pendingIndexUpdates--;
                _local5 = this._bootstrapBoxesURLs[_local3];
                if (((!((_local5 == null))) && (this._pendingIndexUrls.hasOwnProperty(_local5)))){
                    this._pendingIndexUrls[_local5].active = false;
                };
                if (this._pendingIndexUpdates == 0){
                    this._indexUpdating = false;
                };
            };
            this.updateBootstrapBox(_local3, _local4, true);
            if ((((this._pendingIndexLoads == 0)) && (!(this._indexUpdating)))){
                this.notifyRatesReady();
                this.notifyIndexReady(_local3);
            };
        }
        override public function getFileForTime(_arg1:Number, _arg2:int):HTTPStreamRequest{
            var _local4:HTTPStreamRequest;
            if ((((((_arg2 < 0)) || ((_arg2 >= this._streamInfos.length)))) || ((_arg1 < 0)))){
                return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
            };
            this._bestEffortState = BEST_EFFORT_STATE_OFF;
            var _local3:AdobeBootstrapBox = this._bootstrapBoxes[_arg2];
            if (_local3 == null){
                return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
            };
            if (((!(this.playInProgress)) && (this.isStopped(_local3)))){
                this.destroyBootstrapUpdateTimer();
                return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
            };
            this.updateMetadata(_arg2);
            var _local5:Number = (_arg1 * _local3.timeScale);
            if (this._bestEffortEnabled){
                _local4 = this.getFirstRequestForBestEffortSeek(_local5, _arg2, _local3);
            } else {
                _local4 = this.getSeekRequestForNormalFetch(_local5, _arg2, _local3);
            };
            return (_local4);
        }
        private function getSeekRequestForNormalFetch(_arg1:Number, _arg2:int, _arg3:AdobeBootstrapBox):HTTPStreamRequest{
            var _local4:HTTPStreamRequest;
            var _local5:Boolean;
            var _local6:Number = _arg3.currentMediaTime;
            var _local7:Boolean = _arg3.contentComplete();
            var _local8:AdobeFragmentRunTable = this.getFragmentRunTable(_arg3);
            if (_arg1 <= _local6){
                if (_local8 != null){
                    this._currentFAI = _local8.findFragmentIdByTime(_arg1, _local6, ((_local7) ? false : _arg3.live));
                };
                if ((((this._currentFAI == null)) || (this.fragmentOverflow(_arg3, this._currentFAI.fragId)))){
                    if (((!(_arg3.live)) || (_local7))){
                        return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
                    };
                    return (this.initiateLivenessFailure(_arg2));
                };
                return (this.initiateNormalDownload(_arg3, _arg2));
            };
            if (_arg3.live){
                return (this.initiateBootstrapRefresh(_arg2));
            };
            return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
        }
        override public function getNextFile(_arg1:int):HTTPStreamRequest{
            if ((((_arg1 < 0)) || ((_arg1 >= this._streamInfos.length)))){
                return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
            };
            var _local2:AdobeBootstrapBox = this._bootstrapBoxes[_arg1];
            if (_local2 == null){
                return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
            };
            if (((!(this.playInProgress)) && (this.isStopped(_local2)))){
                this.destroyBootstrapUpdateTimer();
                return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
            };
            this.updateMetadata(_arg1);
            var _local3:HTTPStreamRequest;
            if (this._bestEffortEnabled){
                if ((((this._bestEffortState == BEST_EFFORT_STATE_OFF)) || ((this._bestEffortState == BEST_EFFORT_STATE_PLAY)))){
                    _local3 = this.getNextRequestForBestEffortPlay(_arg1, _local2);
                } else {
                    _local3 = this.getNextRequestForBestEffortSeek(_arg1, _local2);
                };
            } else {
                _local3 = this.getNextRequestForNormalPlay(_arg1, _local2);
            };
            return (_local3);
        }
        private function getNextRequestForNormalPlay(_arg1:int, _arg2:AdobeBootstrapBox):HTTPStreamRequest{
            var _local3:HTTPStreamRequest;
            var _local4:Number = _arg2.currentMediaTime;
            var _local5:Boolean = _arg2.contentComplete();
            var _local6:FragmentAccessInformation = this._currentFAI;
            var _local7:AdobeFragmentRunTable = this.getFragmentRunTable(_arg2);
            if (_local6 == null){
                this._currentFAI = null;
            };
            if (_local7 != null){
                this._currentFAI = _local7.validateFragment((_local6.fragId + 1), _local4, ((_local5) ? false : _arg2.live));
            };
            if ((((this._currentFAI == null)) || (this.fragmentOverflow(_arg2, this._currentFAI.fragId)))){
                this._currentFAI = _local6;
                if (((!(_arg2.live)) || (_local5))){
                    return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
                };
                this._currentFAI = _local6;
                return (this.initiateLivenessFailure(_arg1));
            };
            return (this.initiateNormalDownload(_arg2, _arg1));
        }
        private function initiateLivenessFailure(_arg1:int):HTTPStreamRequest{
            var _local2:Number;
            this.adjustDelay();
            this.refreshBootstrapBox(_arg1);
            if (this._bestEffortEnabled){
                _local2 = Math.max(((this._f4fIndexInfo.bestEffortFetchInfo.fragmentDuration / 2) / 1000), 1);
            } else {
                _local2 = this._delay;
            };
            return (new HTTPStreamRequest(HTTPStreamRequestKind.LIVE_STALL, null, _local2));
        }
        private function initiateBootstrapRefresh(_arg1:int):HTTPStreamRequest{
            this.adjustDelay();
            this.refreshBootstrapBox(_arg1);
            return (new HTTPStreamRequest(HTTPStreamRequestKind.RETRY, null, this._delay));
        }
        private function initiateNormalDownload(_arg1:AdobeBootstrapBox, _arg2:int):HTTPStreamRequest{
            this.stopListeningToBestEffortDownload();
            this._bestEffortLivenessRestartPoint = this._currentFAI.fragId;
            this._bestEffortLastGoodFragmentDownloadTime = new Date();
            this.playInProgress = true;
            this.updateQuality(_arg2);
            this.notifyFragmentDuration((this._currentFAI.fragDuration / _arg1.timeScale));
            return (new HTTPStreamRequest(HTTPStreamRequestKind.DOWNLOAD, this.getFragmentUrl(_arg2, this._currentFAI)));
        }
        private function fragmentOverflow(_arg1:AdobeBootstrapBox, _arg2:uint):Boolean{
            var _local3:AdobeFragmentRunTable = _arg1.fragmentRunTables[0];
            var _local4:FragmentDurationPair = _local3.fragmentDurationPairs[0];
            var _local5:AdobeSegmentRunTable = _arg1.segmentRunTables[0];
            return ((((_local5 == null)) || ((((_local5.totalFragments + _local4.firstFragment) - 1) < _arg2))));
        }
        private function isStopped(_arg1:AdobeBootstrapBox):Boolean{
            var _local3:AdobeFragmentRunTable;
            var _local2:Boolean;
            if (this._f4fIndexInfo.dvrInfo != null){
                _local2 = this._f4fIndexInfo.dvrInfo.offline;
            } else {
                if (((!((_arg1 == null))) && (_arg1.live))){
                    _local3 = this.getFragmentRunTable(_arg1);
                    if (_local3 != null){
                        _local2 = _local3.tableComplete();
                    };
                };
            };
            return (_local2);
        }
        private function getFragmentUrl(_arg1:int, _arg2:FragmentAccessInformation):String{
            var _local3:AdobeBootstrapBox = this._bootstrapBoxes[_arg1];
            var _local4:AdobeFragmentRunTable = this.getFragmentRunTable(_local3);
            var _local5:FragmentDurationPair = _local4.fragmentDurationPairs[0];
            var _local6:uint = _local3.findSegmentId(((_arg2.fragId - _local5.firstFragment) + 1));
            return (this.constructFragmentRequest(this._serverBaseURL, this._streamNames[_arg1], _local6, _arg2.fragId));
        }
        protected function constructFragmentRequest(_arg1:String, _arg2:String, _arg3:uint, _arg4:uint):String{
            var _local5 = "";
            if (((!((_arg2 == null))) && (!((_arg2.indexOf("http") == 0))))){
                _local5 = (_arg1 + "/");
            };
            _local5 = (_local5 + _arg2);
            var _local6:URL = new URL(_local5);
            new URL(_local5).path = (_local6.path + ((("Seg" + _arg3) + "-Frag") + _arg4));
            _local5 = ((_local6.protocol + "://") + _local6.host);
            if (((!((_local6.port == null))) && ((_local6.port.length > 0)))){
                _local5 = (_local5 + (":" + _local6.port));
            };
            _local5 = (_local5 + ("/" + _local6.path));
            if (((!((_local6.query == null))) && ((_local6.query.length > 0)))){
                _local5 = (_local5 + ("?" + _local6.query));
            };
            if (((!((_local6.fragment == null))) && ((_local6.fragment.length > 0)))){
                _local5 = (_local5 + ("#" + _local6.fragment));
            };
            return (_local5);
        }
        private function getFragmentRunTable(_arg1:AdobeBootstrapBox):AdobeFragmentRunTable{
            if (_arg1 == null){
                return (null);
            };
            return (_arg1.fragmentRunTables[0]);
        }
        private function adjustDelay():void{
            if (this._delay < 1){
                this._delay = (this._delay * 2);
                if (this._delay > 1){
                    this._delay = 1;
                };
            };
        }
        private function refreshBootstrapBox(_arg1:uint):void{
            var _local7:Date;
            var _local2:String = this._bootstrapBoxesURLs[_arg1];
            if (_local2 == null){
                return;
            };
            var _local3:Object;
            if (this._pendingIndexUrls.hasOwnProperty(_local2)){
                _local3 = this._pendingIndexUrls[_local2];
            } else {
                _local3 = new Object();
                _local3["active"] = false;
                _local3["date"] = null;
                this._pendingIndexUrls[_local2] = _local3;
            };
            var _local4:Boolean = _local3.active;
            var _local5:Date = new Date();
            var _local6:Number = 0;
            if (((!(_local4)) && ((OSMFSettings.hdsMinimumBootstrapRefreshInterval > 0)))){
                _local7 = _local3["date"];
                _local6 = Number.MAX_VALUE;
                if (_local7 != null){
                    _local6 = (_local5.valueOf() - _local7.valueOf());
                };
                _local4 = (_local6 < OSMFSettings.hdsMinimumBootstrapRefreshInterval);
            };
            if (!_local4){
                this._pendingIndexUrls[_local2].date = _local5;
                this._pendingIndexUrls[_local2].active = true;
                this._pendingIndexUpdates++;
                this._indexUpdating = true;
                this.dispatchIndexLoadRequest(_arg1);
            };
        }
        private function updateBootstrapBox(_arg1:int, _arg2:AdobeBootstrapBox, _arg3:Boolean):void{
            if (this.shouldAcceptBootstrapBox(_arg1, _arg2, _arg3)){
                this._bootstrapBoxes[_arg1] = _arg2;
                this._delay = 0.05;
                if (_arg1 == this._currentQuality){
                    this.dispatchDVRStreamInfo(_arg2);
                };
            };
        }
        private function shouldAcceptBootstrapBox(_arg1:int, _arg2:AdobeBootstrapBox, _arg3:Boolean):Boolean{
            var _local7:AdobeFragmentRunTable;
            var _local8:AdobeSegmentRunTable;
            var _local9:uint;
            var _local10:uint;
            var _local4:AdobeBootstrapBox = this._bootstrapBoxes[_arg1];
            if ((((((_arg2 == null)) || ((_arg2.fragmentRunTables.length == 0)))) || ((_arg2.segmentRunTables.length == 0)))){
                return (false);
            };
            var _local5:AdobeFragmentRunTable = _arg2.fragmentRunTables[0];
            var _local6:AdobeSegmentRunTable = _arg2.segmentRunTables[0];
            if ((((_local5 == null)) || ((_local6 == null)))){
                return (false);
            };
            if (_local5.firstFragmentId == 0){
                return (false);
            };
            if (_local4 == null){
                return (true);
            };
            if (_local4.live != _arg2.live){
                return (false);
            };
            if (!_local4.live){
                if (_arg2.version != _local4.version){
                    return ((_arg2.version > _local4.version));
                };
                return ((_arg2.currentMediaTime > _local4.currentMediaTime));
            };
            if (!_arg3){
                return (false);
            };
            _local7 = _local4.fragmentRunTables[0];
            _local8 = _local4.segmentRunTables[0];
            if (_arg2.currentMediaTime != _local4.currentMediaTime){
                return ((_arg2.currentMediaTime > _local4.currentMediaTime));
            };
            if (_local5.firstFragmentId != _local7.firstFragmentId){
                return ((_local5.firstFragmentId < _local7.firstFragmentId));
            };
            _local9 = _local5.countGapFragments();
            _local10 = _local7.countGapFragments();
            if (_local9 != _local10){
                return ((_local9 < _local10));
            };
            if (((_arg2.contentComplete()) && (!(_local4.contentComplete())))){
                return (true);
            };
            return (false);
        }
        private function processBootstrapData(_arg1, _arg2:Object):AdobeBootstrapBox{
            var boxes:* = null;
            var data:* = _arg1;
            var indexContext:* = _arg2;
            var parser:* = new BoxParser();
            data.position = 0;
            parser.init(data);
            try {
                boxes = parser.getBoxes();
            } catch(e:Error) {
                boxes = null;
            };
            if ((((boxes == null)) || ((boxes.length < 1)))){
                return (null);
            };
            var bootstrapBox:* = (boxes[0] as AdobeBootstrapBox);
            if (bootstrapBox == null){
                return (null);
            };
            if ((((this._serverBaseURL == null)) || ((this._serverBaseURL.length <= 0)))){
                if ((((bootstrapBox.serverBaseURLs == null)) || ((bootstrapBox.serverBaseURLs.length <= 0)))){
                    return (null);
                };
                this._serverBaseURL = bootstrapBox.serverBaseURLs[0];
            };
            return (bootstrapBox);
        }
        private function updateQuality(_arg1:int):void{
            var _local2:ByteArray;
            var _local3:ByteArray;
            if (_arg1 != this._currentQuality){
                _local2 = this._currentAdditionalHeader;
                _local3 = this._streamInfos[_arg1].additionalHeader;
                this._currentQuality = _arg1;
                this._currentAdditionalHeader = _local3;
                if (((!((_local3 == null))) && (!((_local3 == _local2))))){
                    this.dispatchAdditionalHeader(_local3);
                };
            };
        }
        private function updateMetadata(_arg1:int):void{
            var _local2:AdobeBootstrapBox;
            if (_arg1 != this._currentQuality){
                _local2 = this._bootstrapBoxes[_arg1];
                if (_local2 != null){
                    this.notifyTotalDuration((_local2.totalDuration / _local2.timeScale), _arg1);
                };
            };
        }
        private function dispatchAdditionalHeader(_arg1:ByteArray):void{
            var _local2:FLVTagScriptDataObject = new FLVTagScriptDataObject();
            _local2.data = _arg1;
            dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.SCRIPT_DATA, false, false, 0, _local2, FLVTagScriptDataMode.FIRST));
        }
        private function dispatchDVRStreamInfo(_arg1:AdobeBootstrapBox):void{
            var _local4:Number;
            var _local5:Number;
            var _local6:Number;
            var _local7:Number;
            var _local2:AdobeFragmentRunTable = this.getFragmentRunTable(_arg1);
            var _local3:DVRInfo = this._f4fIndexInfo.dvrInfo;
            if (_local3 != null){
                _local3.isRecording = !(_local2.tableComplete());
                _local4 = (_arg1.totalDuration / _arg1.timeScale);
                _local5 = (_arg1.currentMediaTime / _arg1.timeScale);
                if (isNaN(_local3.startTime)){
                    if (!_local3.isRecording){
                        _local3.startTime = 0;
                    } else {
                        _local6 = (((((_local3.beginOffset < 0)) || (isNaN(_local3.beginOffset)))) ? 0 : _local3.beginOffset);
                        _local7 = (((((_local3.endOffset < 0)) || (isNaN(_local3.endOffset)))) ? 0 : _local3.endOffset);
                        _local3.startTime = DVRUtils.calculateOffset(_local6, _local7, _local4);
                    };
                    _local3.startTime = (_local3.startTime + (_local2.fragmentDurationPairs[0].durationAccrued / _arg1.timeScale));
                    if (_local3.startTime > _local5){
                        _local3.startTime = _local5;
                    };
                };
                _local3.curLength = (_local5 - _local3.startTime);
                if (((!((_local3.windowDuration == -1))) && ((_local3.curLength > _local3.windowDuration)))){
                    _local3.startTime = (_local3.startTime + (_local3.curLength - _local3.windowDuration));
                    _local3.curLength = _local3.windowDuration;
                };
                dispatchEvent(new DVRStreamInfoEvent(DVRStreamInfoEvent.DVRSTREAMINFO, false, false, _local3));
            };
        }
        private function dispatchIndexLoadRequest(_arg1:int):void{
            dispatchEvent(new HTTPStreamingIndexHandlerEvent(HTTPStreamingIndexHandlerEvent.REQUEST_LOAD_INDEX, false, false, false, NaN, null, null, new URLRequest(this._bootstrapBoxesURLs[_arg1]), _arg1, true));
        }
        private function notifyRatesReady():void{
            dispatchEvent(new HTTPStreamingIndexHandlerEvent(HTTPStreamingIndexHandlerEvent.RATES_READY, false, false, false, NaN, this._streamNames, this._streamQualityRates));
        }
        private function notifyIndexReady(_arg1:int):void{
            var _local2:AdobeBootstrapBox = this._bootstrapBoxes[_arg1];
            var _local3:AdobeFragmentRunTable = this.getFragmentRunTable(_local2);
            if (!this._bestEffortInited){
                this._bestEffortEnabled = ((!((this._f4fIndexInfo.bestEffortFetchInfo == null))) && (_local2.live));
                this._bestEffortInited = true;
            };
            this.dispatchDVRStreamInfo(_local2);
            if (!this._invokedFromDvrGetStreamInfo){
                if (((((_local2.live) && ((this._f4fIndexInfo.dvrInfo == null)))) && (isNaN(this._pureLiveOffset)))){
                    this._pureLiveOffset = (_local2.currentMediaTime - (OSMFSettings.hdsPureLiveOffset * _local2.timeScale));
                    if (this._pureLiveOffset < 0){
                        this._pureLiveOffset = NaN;
                    } else {
                        this._pureLiveOffset = (this._pureLiveOffset / _local2.timeScale);
                    };
                };
                if (((((_local2.live) && (!((this._f4fIndexInfo.dvrInfo == null))))) && (!((this._f4fIndexInfo.dvrInfo.windowDuration == -1))))){
                    this.initializeBootstrapUpdateTimer();
                };
                if (_local3.tableComplete()){
                    this.destroyBootstrapUpdateTimer();
                };
                dispatchEvent(new HTTPStreamingIndexHandlerEvent(HTTPStreamingIndexHandlerEvent.INDEX_READY, false, false, _local2.live, this._pureLiveOffset));
            };
            this._invokedFromDvrGetStreamInfo = false;
        }
        private function notifyTotalDuration(_arg1:Number, _arg2:int):void{
            var _local3:Object = this._streamInfos[_arg2].streamMetadata;
            if (_local3 == null){
                _local3 = new Object();
            };
            _local3.duration = _arg1;
            var _local4:FLVTagScriptDataObject = new FLVTagScriptDataObject();
            _local4.objects = ["onMetaData", _local3];
            dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.SCRIPT_DATA, false, false, 0, _local4, FLVTagScriptDataMode.IMMEDIATE));
        }
        private function notifyFragmentDuration(_arg1:Number):void{
            this.bootstrapUpdateInterval = (_arg1 * 1000);
            if (this.bootstrapUpdateInterval < OSMFSettings.hdsMinimumBootstrapRefreshInterval){
                this.bootstrapUpdateInterval = OSMFSettings.hdsMinimumBootstrapRefreshInterval;
            };
            dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.FRAGMENT_DURATION, false, false, _arg1, null, null));
        }
        private function initializeBootstrapUpdateTimer():void{
            if (this.bootstrapUpdateTimer == null){
                this.bootstrapUpdateTimer = new Timer(this.bootstrapUpdateInterval);
                this.bootstrapUpdateTimer.addEventListener(TimerEvent.TIMER, this.onBootstrapUpdateTimer);
                this.bootstrapUpdateTimer.start();
            };
        }
        private function destroyBootstrapUpdateTimer():void{
            if (this.bootstrapUpdateTimer != null){
                this.bootstrapUpdateTimer.removeEventListener(TimerEvent.TIMER, this.onBootstrapUpdateTimer);
                this.bootstrapUpdateTimer = null;
            };
        }
        private function onBootstrapBox(_arg1:HTTPStreamingFileHandlerEvent):void{
            this.updateBootstrapBox(this._currentQuality, _arg1.bootstrapBox, false);
            this.notifyFragmentDurationForBestEffort(_arg1.bootstrapBox);
        }
        private function onBootstrapUpdateTimer(_arg1:TimerEvent):void{
            if (this._currentQuality != -1){
                this.refreshBootstrapBox(this._currentQuality);
                this.bootstrapUpdateTimer.delay = this.bootstrapUpdateInterval;
            };
        }
        private function getFirstRequestForBestEffortSeek(_arg1:Number, _arg2:int, _arg3:AdobeBootstrapBox):HTTPStreamRequest{
            this.bestEffortLog(("Initiating best effort seek " + _arg1));
            this._bestEffortState = BEST_EFFORT_STATE_SEEK_BACKWARD;
            this._bestEffortSeekTime = _arg1;
            this._bestEffortFailedFetches = 0;
            this._bestEffortLastGoodFragmentDownloadTime = null;
            return (this.getNextRequestForBestEffortSeek(_arg2, _arg3));
        }
        private function getNextRequestForBestEffortSeek(_arg1:int, _arg2:AdobeBootstrapBox):HTTPStreamRequest{
            var _local3:AdobeFragmentRunTable = this.getFragmentRunTable(_arg2);
            if (_local3 == null){
                this.bestEffortLog("Best effort done because the bootstrap box was invalid");
                return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
            };
            this.stopListeningToBestEffortDownload();
            this._currentFAI = null;
            var _local4:uint = this.doBestEffortSeek(_arg2, _local3);
            if (_local4 != 0){
                this.bestEffortLog(("Best effort seek fetch for fragment " + _local4));
                this._bestEffortF4FHandler.beginProcessFile(true, this._bestEffortSeekTime);
                this._bestEffortSeekBuffer.length = 0;
                this._bestEffortSeekBuffer.position = 0;
                return (this.initiateBestEffortRequest(_local4, _arg1));
            };
            this._bestEffortState = BEST_EFFORT_STATE_OFF;
            this._currentFAI = _local3.getFragmentWithTimeGreq(this._bestEffortSeekTime);
            if (this._currentFAI == null){
                this.bestEffortLog("Best effort done because there were no bootstrap entries");
                this._bestEffortState = BEST_EFFORT_STATE_OFF;
                this._bestEffortLivenessRestartPoint = (Math.max(this.guessFragmentIdForTime(this._bestEffortSeekTime), 1) - 1);
                this._currentFAI = new FragmentAccessInformation();
                this._currentFAI.fragId = this._bestEffortLivenessRestartPoint;
                return (this.getNextFile(_arg1));
            };
            this.bestEffortLog(("Normal seek request for fragment " + this._currentFAI.fragId));
            return (this.initiateNormalDownload(_arg2, _arg1));
        }
        private function guessFragmentIdForTime(_arg1:Number):uint{
            return ((uint(Math.floor((_arg1 / this._f4fIndexInfo.bestEffortFetchInfo.fragmentDuration))) + 1));
        }
        private function doBestEffortSeek(_arg1:AdobeBootstrapBox, _arg2:AdobeFragmentRunTable):uint{
            var _local5:Number;
            var _local6:uint;
            if (this._bestEffortSeekTime >= _arg1.currentMediaTime){
                this.bestEffortLog("Seek time greter than current media time.");
                return (0);
            };
            if (!_arg2.isTimeInGap(this._bestEffortSeekTime, this._f4fIndexInfo.bestEffortFetchInfo.fragmentDuration)){
                this.bestEffortLog("Found seek time in FRT");
                return (0);
            };
            var _local3:FragmentAccessInformation = _arg2.getFragmentWithIdGreq(0);
            if (_local3 != null){
                _local5 = (_local3.fragmentEndTime - _local3.fragDuration);
                if (this._bestEffortSeekTime < _local5){
                    this.bestEffortLog("Seek time before first bootstrap entry time.");
                    return (0);
                };
            };
            if (this._bestEffortState == BEST_EFFORT_STATE_SEEK_BACKWARD){
                _local6 = this.doBestEffortSeekBackward(_arg2);
                if (_local6 != 0){
                    return (_local6);
                };
                this._bestEffortState = BEST_EFFORT_STATE_SEEK_FORWARD;
                this._bestEffortFailedFetches = 0;
            };
            var _local4:uint = this.doBestEffortSeekForward(_arg2);
            if (_local4 != 0){
                return (_local4);
            };
            return (0);
        }
        private function doBestEffortSeekBackward(_arg1:AdobeFragmentRunTable):uint{
            if (this._bestEffortFailedFetches >= this._f4fIndexInfo.bestEffortFetchInfo.maxBackwardFetches){
                this.bestEffortLog("Best effort seek backward failing due to too many failures");
                return (0);
            };
            var _local2:uint = this.guessFragmentIdForTime(this._bestEffortSeekTime);
            if (_local2 <= (this._bestEffortFailedFetches + 1)){
                this.bestEffortLog("Best effort seek backward hit fragment 0");
                return (0);
            };
            var _local3:uint = (_local2 - (this._bestEffortFailedFetches + 1));
            if (!_arg1.isFragmentInGap(_local3)){
                this.bestEffortLog(("Best effort seek backward hit an existing fragment " + _local3));
                return (0);
            };
            this.bestEffortLog(("Best effort seek backward fetch " + _local3));
            return (_local3);
        }
        private function doBestEffortSeekForward(_arg1:AdobeFragmentRunTable):uint{
            if (this._bestEffortFailedFetches >= this._f4fIndexInfo.bestEffortFetchInfo.maxForwardFetches){
                this.bestEffortLog("Best effort seek failing due to too many failures");
                return (0);
            };
            var _local2:uint = (this.guessFragmentIdForTime(this._bestEffortSeekTime) + this._bestEffortFailedFetches);
            if (!_arg1.isFragmentInGap(_local2)){
                this.bestEffortLog(("Best effort seek forward hit an existing fragment " + _local2));
                return (0);
            };
            this.bestEffortLog(("Best effort seek forward fetch " + _local2));
            return (_local2);
        }
        private function getNextRequestForBestEffortPlay(_arg1:int, _arg2:AdobeBootstrapBox):HTTPStreamRequest{
            var _local8:String;
            var _local9:Date;
            var _local10:Number;
            var _local11:FragmentAccessInformation;
            var _local3:AdobeFragmentRunTable = this.getFragmentRunTable(_arg2);
            if ((((((((((this._currentFAI == null)) || ((_local3 == null)))) || ((_arg2 == null)))) || ((_arg2.segmentRunTables.length < 1)))) || ((_arg2.segmentRunTables[0].segmentFragmentPairs.length < 1)))){
                this.bestEffortLog("Best effort in a weird state.");
                return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
            };
            var _local4:AdobeSegmentRunTable = _arg2.segmentRunTables[0];
            var _local5:uint = (this._currentFAI.fragId + 1);
            var _local6:uint = _local3.firstFragmentId;
            if (_local6 == 0){
                this.bestEffortLog("Best effort in a weird state.");
                return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
            };
            var _local7:uint = (_local6 + _local4.totalFragments);
            if (_local5 >= _local7){
                if (_arg2.contentComplete()){
                    _local8 = BEST_EFFORT_PLAY_SITUAUTION_DONE;
                } else {
                    _local8 = BEST_EFFORT_PLAY_SITUAUTION_LIVENESS;
                };
            } else {
                if (_local3.isFragmentInGap(_local5)){
                    _local8 = BEST_EFFORT_PLAY_SITUAUTION_DROPOUT;
                } else {
                    _local8 = BEST_EFFORT_PLAY_SITUAUTION_NORMAL;
                };
            };
            if ((((_local8 == BEST_EFFORT_PLAY_SITUAUTION_DROPOUT)) || ((_local8 == BEST_EFFORT_PLAY_SITUAUTION_LIVENESS)))){
                this.bestEffortLog(("Best effort in " + _local8));
                if ((((_local8 == BEST_EFFORT_PLAY_SITUAUTION_LIVENESS)) && (!((this._bestEffortLastGoodFragmentDownloadTime == null))))){
                    _local9 = new Date();
                    _local10 = (this._bestEffortLastGoodFragmentDownloadTime.valueOf() + Math.max(this._f4fIndexInfo.bestEffortFetchInfo.fragmentDuration, 1000));
                    if (_local9.valueOf() < _local10){
                        return (this.initiateBootstrapRefresh(_arg1));
                    };
                };
                if (this._bestEffortState == BEST_EFFORT_STATE_OFF){
                    this.bestEffortLog("Best effort play start");
                    this._bestEffortState = BEST_EFFORT_STATE_PLAY;
                    this._bestEffortFailedFetches = 0;
                };
                if (this._bestEffortFailedFetches < this._f4fIndexInfo.bestEffortFetchInfo.maxForwardFetches){
                    return (this.initiateBestEffortRequest(_local5, _arg1));
                };
                this.bestEffortLog("Best effort play failing due to too many failures");
            };
            if (_local8 == BEST_EFFORT_PLAY_SITUAUTION_LIVENESS){
                this.bestEffortLog("Best effort in liveness");
                this._bestEffortState = BEST_EFFORT_STATE_OFF;
                this._currentFAI.fragId = this._bestEffortLivenessRestartPoint;
                return (this.initiateLivenessFailure(_arg1));
            };
            if (_local8 == BEST_EFFORT_PLAY_SITUAUTION_DONE){
                this.bestEffortLog("Best effort done");
                return (new HTTPStreamRequest(HTTPStreamRequestKind.DONE));
            };
            _local11 = this._currentFAI;
            this._currentFAI = _local3.getFragmentWithIdGreq(_local5);
            if (this._currentFAI == null){
                this._currentFAI = _local11;
                this.bestEffortLog("Best effort done because there were no bootstrap entries");
                return (this.initiateBootstrapRefresh(_arg1));
            };
            this._bestEffortState = BEST_EFFORT_STATE_OFF;
            this.bestEffortLog(("Normal play request for fragment " + this._currentFAI.fragId));
            return (this.initiateNormalDownload(_arg2, _arg1));
        }
        private function initiateBestEffortRequest(_arg1:uint, _arg2:int):HTTPStreamRequest{
            this.stopListeningToBestEffortDownload();
            this._currentFAI = new FragmentAccessInformation();
            this._currentFAI.fragId = _arg1;
            this._currentFAI.fragDuration = 0;
            this._currentFAI.fragmentEndTime = 0;
            this.playInProgress = true;
            this.updateQuality(_arg2);
            this.bootstrapUpdateInterval = OSMFSettings.hdsMinimumBootstrapRefreshInterval;
            var _local3:uint = uint(Math.ceil((Number(_arg1) / (this._f4fIndexInfo.bestEffortFetchInfo.segmentDuration / this._f4fIndexInfo.bestEffortFetchInfo.fragmentDuration))));
            var _local4:String = this.constructFragmentRequest(this._serverBaseURL, this._streamNames[_arg2], _local3, _arg1);
            this.bestEffortLog(((((("Best effort fetch for fragment " + _arg1) + " with url ") + _local4) + ". State is ") + this._bestEffortState));
            this._bestEffortDownloadReply = null;
            this._bestEffortNeedsToFireFragmentDuration = false;
            this._bestEffortDownloaderMonitor = new EventDispatcher();
            this._bestEffortDownloaderMonitor.addEventListener(HTTPStreamingEvent.DOWNLOAD_COMPLETE, this.onBestEffortDownloadComplete);
            this._bestEffortDownloaderMonitor.addEventListener(HTTPStreamingEvent.DOWNLOAD_ERROR, this.onBestEffortDownloadError);
            var _local5:HTTPStreamRequest = new HTTPStreamRequest(HTTPStreamRequestKind.BEST_EFFORT_DOWNLOAD, _local4, -1, this._bestEffortDownloaderMonitor);
            this.adjustDelay();
            this.refreshBootstrapBox(_arg2);
            return (_local5);
        }
        private function stopListeningToBestEffortDownload():void{
            if (this._bestEffortDownloaderMonitor != null){
                this._bestEffortDownloaderMonitor.removeEventListener(HTTPStreamingEvent.DOWNLOAD_COMPLETE, this.onBestEffortDownloadComplete);
                this._bestEffortDownloaderMonitor.removeEventListener(HTTPStreamingEvent.DOWNLOAD_ERROR, this.onBestEffortDownloadError);
                this._bestEffortDownloaderMonitor = null;
            };
        }
        private function bufferAndParseDownloadedBestEffortBytes(_arg1:String, _arg2:HTTPStreamDownloader):void{
            var downloaderAvailableBytes:* = 0;
            var downloadInput:* = null;
            var url:* = _arg1;
            var downloader:* = _arg2;
            if (this._bestEffortDownloadReply != null){
                return;
            };
            this._bestEffortNotifyBootstrapBoxInfo = {
                downloader:downloader,
                url:url
            };
            try {
                downloaderAvailableBytes = downloader.totalAvailableBytes;
                if (downloaderAvailableBytes > 0){
                    downloadInput = downloader.getBytes(downloaderAvailableBytes);
                    if (downloadInput != null){
                        downloadInput.readBytes(this._bestEffortSeekBuffer, this._bestEffortSeekBuffer.length, downloaderAvailableBytes);
                    };
                    while ((((((this._bestEffortF4FHandler.inputBytesNeeded > 0)) && ((this._bestEffortF4FHandler.inputBytesNeeded <= this._bestEffortSeekBuffer.bytesAvailable)))) && ((this._bestEffortDownloadReply == null)))) {
                        this._bestEffortF4FHandler.processFileSegment(this._bestEffortSeekBuffer);
                    };
                    if (this._bestEffortDownloadReply == HTTPStreamingEvent.DOWNLOAD_CONTINUE){
                        downloader.clearSavedBytes();
                        this._bestEffortSeekBuffer.position = 0;
                        downloader.appendToSavedBytes(this._bestEffortSeekBuffer, this._bestEffortSeekBuffer.length);
                        this._bestEffortSeekBuffer.length = 0;
                    };
                };
            } finally {
                this._bestEffortNotifyBootstrapBoxInfo = null;
            };
        }
        private function onBestEffortF4FHandlerNotifyBootstrapBox(_arg1:HTTPStreamingFileHandlerEvent):void{
            var _local2:String = (this._bestEffortNotifyBootstrapBoxInfo.url as String);
            var _local3:HTTPStreamDownloader = (this._bestEffortNotifyBootstrapBoxInfo.downloader as HTTPStreamDownloader);
            if (this._bestEffortDownloadReply != null){
                this.bestEffortLog("Best effort found a bootstrap box in the downloaded fragment, but we already replied.");
                return;
            };
            var _local4:AdobeBootstrapBox = _arg1.bootstrapBox;
            var _local5:AdobeFragmentRunTable = this.getFragmentRunTable(_local4);
            if (_local5 == null){
                this.bestEffortLog("Best effort download contained an invalid bootstrap box.");
                this.skipBestEffortFetch(_local2, _local3);
                return;
            };
            if (_local5.fragmentDurationPairs.length != 1){
                this.bestEffortLog("Best effort download has an FRT with more than 1 entry.");
                this.skipBestEffortFetch(_local2, _local3);
                return;
            };
            var _local6:FragmentDurationPair = _local5.fragmentDurationPairs[0];
            if (_local6.duration == 0){
                this.bestEffortLog("Best effort download FDP was a discontinuity.");
                this.skipBestEffortFetch(_local2, _local3);
                return;
            };
            var _local7:Number = (_local6.durationAccrued + _local6.duration);
            if (this._bestEffortSeekTime < _local7){
                this.bestEffortLog("Best effort found the desired time within the downloaded fragment.");
                this.continueBestEffortFetch(_local2, _local3);
            } else {
                this.bestEffortLog("Best effort didn't find the desired time within the downloaded fragment.");
                this.skipBestEffortFetch(_local2, _local3);
                this._bestEffortState = BEST_EFFORT_STATE_SEEK_FORWARD;
                this._bestEffortFailedFetches = 0;
            };
        }
        private function onBestEffortDownloadComplete(_arg1:HTTPStreamingEvent):void{
            if ((((this._bestEffortDownloaderMonitor == null)) || (!((this._bestEffortDownloaderMonitor == (_arg1.target as IEventDispatcher)))))){
                return;
            };
            this.bestEffortLog("Best effort download complete");
            this.stopListeningToBestEffortDownload();
            var _local2:HTTPStreamingEvent = new HTTPStreamingEvent(_arg1.type, _arg1.bubbles, _arg1.cancelable, _arg1.fragmentDuration, _arg1.scriptDataObject, _arg1.scriptDataMode, _arg1.url, _arg1.bytesDownloaded, HTTPStreamingEventReason.BEST_EFFORT, _arg1.downloader);
            dispatchEvent(_local2);
            if (this._bestEffortDownloadReply != null){
                return;
            };
            switch (this._bestEffortState){
                case BEST_EFFORT_STATE_PLAY:
                case BEST_EFFORT_STATE_SEEK_FORWARD:
                    this.continueBestEffortFetch(_arg1.url, _arg1.downloader);
                    break;
                case BEST_EFFORT_STATE_SEEK_BACKWARD:
                    this.bufferAndParseDownloadedBestEffortBytes(_arg1.url, _arg1.downloader);
                    if (this._bestEffortDownloadReply == null){
                        this.skipBestEffortFetch(_arg1.url, _arg1.downloader);
                    };
                    break;
                default:
                    this.bestEffortLog((("Best effort download complete received while in unexpected state (" + this._bestEffortState) + ")"));
            };
        }
        private function onBestEffortDownloadError(_arg1:HTTPStreamingEvent):void{
            if ((((this._bestEffortDownloaderMonitor == null)) || (!((this._bestEffortDownloaderMonitor == (_arg1.target as IEventDispatcher)))))){
                return;
            };
            this.stopListeningToBestEffortDownload();
            if (this._bestEffortDownloadReply != null){
                this.bestEffortLog("Best effort download error after we already decided to skip or continue.");
                dispatchEvent(_arg1);
            } else {
                if (_arg1.reason == HTTPStreamingEventReason.TIMEOUT){
                    this.bestEffortLog("Best effort download timed out");
                    dispatchEvent(_arg1);
                } else {
                    this.bestEffortLog("Best effort download error.");
                    this._bestEffortFailedFetches++;
                    this.skipBestEffortFetch(_arg1.url, _arg1.downloader);
                };
            };
        }
        private function skipBestEffortFetch(_arg1:String, _arg2:HTTPStreamDownloader):void{
            if (this._bestEffortDownloadReply != null){
                this.bestEffortLog(("Best effort wanted to skip fragment, but we're already replied with " + this._bestEffortDownloadReply));
                return;
            };
            this.bestEffortLog("Best effort skipping fragment.");
            var _local3:HTTPStreamingEvent = new HTTPStreamingEvent(HTTPStreamingEvent.DOWNLOAD_SKIP, false, false, 0, null, FLVTagScriptDataMode.NORMAL, _arg1, 0, HTTPStreamingEventReason.BEST_EFFORT, _arg2);
            dispatchEvent(_local3);
            this._bestEffortDownloadReply = HTTPStreamingEvent.DOWNLOAD_SKIP;
            this._bestEffortNeedsToFireFragmentDuration = false;
        }
        private function continueBestEffortFetch(_arg1:String, _arg2:HTTPStreamDownloader):void{
            if (this._bestEffortDownloadReply != null){
                this.bestEffortLog(("Best effort wanted to continue, but we're already replied with " + this._bestEffortDownloadReply));
                return;
            };
            this.bestEffortLog("Best effort received a desirable fragment.");
            var _local3:HTTPStreamingEvent = new HTTPStreamingEvent(HTTPStreamingEvent.DOWNLOAD_CONTINUE, false, false, 0, null, FLVTagScriptDataMode.NORMAL, _arg1, 0, HTTPStreamingEventReason.BEST_EFFORT, _arg2);
            this._bestEffortLivenessRestartPoint = this._currentFAI.fragId;
            this._bestEffortLastGoodFragmentDownloadTime = new Date();
            dispatchEvent(_local3);
            this._bestEffortDownloadReply = HTTPStreamingEvent.DOWNLOAD_CONTINUE;
            this._bestEffortNeedsToFireFragmentDuration = true;
            this._bestEffortState = BEST_EFFORT_STATE_OFF;
        }
        private function errorBestEffortFetch(_arg1:String, _arg2:HTTPStreamDownloader):void{
            this.bestEffortLog("Best effort fetch error.");
            var _local3:HTTPStreamingEvent = new HTTPStreamingEvent(HTTPStreamingEvent.DOWNLOAD_ERROR, false, false, 0, null, FLVTagScriptDataMode.NORMAL, _arg1, 0, HTTPStreamingEventReason.BEST_EFFORT, _arg2);
            dispatchEvent(_local3);
            this._bestEffortDownloadReply = HTTPStreamingEvent.DOWNLOAD_ERROR;
            this._bestEffortNeedsToFireFragmentDuration = false;
        }
        private function notifyFragmentDurationForBestEffort(_arg1:AdobeBootstrapBox):void{
            if (((!(this._bestEffortNeedsToFireFragmentDuration)) || ((_arg1 == null)))){
                return;
            };
            this._bestEffortNeedsToFireFragmentDuration = false;
            var _local2:AdobeFragmentRunTable = this.getFragmentRunTable(_arg1);
            if (_local2 == null){
                return;
            };
            var _local3:Number = _local2.getFragmentDuration(this._currentFAI.fragId);
            if (_local3 == 0){
                return;
            };
            this.bestEffortLog("Best effort fetch firing the fragment duration.");
            dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.FRAGMENT_DURATION, false, false, (_local3 / _arg1.timeScale), null, null));
        }
        private function bestEffortLog(_arg1:String):void{
        }
        override public function get isBestEffortFetchEnabled():Boolean{
            return (this._bestEffortEnabled);
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
