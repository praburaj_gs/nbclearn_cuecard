﻿package org.osmf.net.httpstreaming.f4f {

    class SegmentFragmentPair {

        private var _firstSegment:uint;
        private var _fragmentsPerSegment:uint;
        private var _fragmentsAccrued:uint;

        public function SegmentFragmentPair(_arg1:uint, _arg2:uint){
            this._firstSegment = _arg1;
            this._fragmentsPerSegment = _arg2;
        }
        public function get firstSegment():uint{
            return (this._firstSegment);
        }
        public function get fragmentsPerSegment():uint{
            return (this._fragmentsPerSegment);
        }
        public function set fragmentsAccrued(_arg1:uint):void{
            this._fragmentsAccrued = _arg1;
        }
        public function get fragmentsAccrued():uint{
            return (this._fragmentsAccrued);
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
