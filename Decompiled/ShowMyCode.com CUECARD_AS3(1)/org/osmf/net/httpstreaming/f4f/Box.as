﻿package org.osmf.net.httpstreaming.f4f {

    class Box {

        private var _size:Number;
        private var _type:String;
        private var _boxLength:uint;

        public function get size():Number{
            return (this._size);
        }
        public function set size(_arg1:Number):void{
            this._size = _arg1;
        }
        public function get type():String{
            return (this._type);
        }
        public function set type(_arg1:String):void{
            this._type = _arg1;
        }
        public function get boxLength():uint{
            return (this._boxLength);
        }
        public function set boxLength(_arg1:uint):void{
            this._boxLength = _arg1;
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
