﻿package org.osmf.net.httpstreaming.f4f {
    import org.osmf.elements.f4mClasses.*;
    import flash.utils.*;

    public class HTTPStreamingF4FStreamInfo {

        private var _streamName:String;
        private var _bitrate:Number;
        private var _bootstrap:BootstrapInfo;
        private var _additionalHeader:ByteArray;
        private var _streamMetadata:Object;
        private var _xmpMetadata:ByteArray;

        public function HTTPStreamingF4FStreamInfo(_arg1:BootstrapInfo, _arg2:String, _arg3:Number, _arg4:ByteArray, _arg5:Object, _arg6:ByteArray){
            this._streamName = _arg2;
            this._bitrate = _arg3;
            this._additionalHeader = _arg4;
            this._bootstrap = _arg1;
            this._streamMetadata = _arg5;
            this._xmpMetadata = _arg6;
        }
        public function get streamName():String{
            return (this._streamName);
        }
        public function get bitrate():Number{
            return (this._bitrate);
        }
        public function get additionalHeader():ByteArray{
            return (this._additionalHeader);
        }
        public function get bootstrapInfo():BootstrapInfo{
            return (this._bootstrap);
        }
        public function get streamMetadata():Object{
            return (this._streamMetadata);
        }
        public function get xmpMetadata():ByteArray{
            return (this._xmpMetadata);
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
