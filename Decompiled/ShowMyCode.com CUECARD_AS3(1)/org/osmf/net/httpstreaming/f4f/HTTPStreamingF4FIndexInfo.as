﻿package org.osmf.net.httpstreaming.f4f {
    import __AS3__.vec.*;
    import org.osmf.elements.f4mClasses.*;
    import org.osmf.net.httpstreaming.*;
    import org.osmf.net.httpstreaming.dvr.*;

    public class HTTPStreamingF4FIndexInfo extends HTTPStreamingIndexInfoBase {

        private var _serverBaseURL:String;
        private var _dvrInfo:DVRInfo;
        private var _befInfo:BestEffortFetchInfo;
        private var _streamInfos:Vector.<HTTPStreamingF4FStreamInfo>;

        public function HTTPStreamingF4FIndexInfo(_arg1:String=null, _arg2:Vector.<HTTPStreamingF4FStreamInfo>=null, _arg3:DVRInfo=null, _arg4:BestEffortFetchInfo=null){
            this._serverBaseURL = _arg1;
            this._streamInfos = _arg2;
            this._dvrInfo = _arg3;
            this._befInfo = _arg4;
        }
        public function get serverBaseURL():String{
            return (this._serverBaseURL);
        }
        public function get streamInfos():Vector.<HTTPStreamingF4FStreamInfo>{
            return (this._streamInfos);
        }
        public function get dvrInfo():DVRInfo{
            return (this._dvrInfo);
        }
        public function get bestEffortFetchInfo():BestEffortFetchInfo{
            return (this._befInfo);
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
