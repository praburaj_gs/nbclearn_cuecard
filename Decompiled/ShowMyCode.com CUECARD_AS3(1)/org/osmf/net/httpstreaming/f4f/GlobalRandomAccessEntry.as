﻿package org.osmf.net.httpstreaming.f4f {

    class GlobalRandomAccessEntry {

        private var _time:Number;
        private var _segment:uint;
        private var _fragment:uint;
        private var _afraOffset:Number;
        private var _offsetFromAfra:Number;

        public function get time():Number{
            return (this._time);
        }
        public function set time(_arg1:Number):void{
            this._time = _arg1;
        }
        public function get segment():uint{
            return (this._segment);
        }
        public function set segment(_arg1:uint):void{
            this._segment = _arg1;
        }
        public function get fragment():uint{
            return (this._fragment);
        }
        public function set fragment(_arg1:uint):void{
            this._fragment = _arg1;
        }
        public function get afraOffset():Number{
            return (this._afraOffset);
        }
        public function set afraOffset(_arg1:Number):void{
            this._afraOffset = _arg1;
        }
        public function get offsetFromAfra():Number{
            return (this._offsetFromAfra);
        }
        public function set offsetFromAfra(_arg1:Number):void{
            this._offsetFromAfra = _arg1;
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
