﻿package org.osmf.net.httpstreaming.f4f {

    class FullBox extends Box {

        private var _version:uint;
        private var _flags:uint;

        public function get version():uint{
            return (this._version);
        }
        public function set version(_arg1:uint):void{
            this._version = _arg1;
        }
        public function get flags():uint{
            return (this._flags);
        }
        public function set flags(_arg1:uint):void{
            this._flags = _arg1;
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
