﻿package org.osmf.net.httpstreaming.f4f {
    import __AS3__.vec.*;
    import org.osmf.net.httpstreaming.f4f.*;

    class AdobeFragmentRunTable extends FullBox {

        private var _timeScale:uint;
        private var _qualitySegmentURLModifiers:Vector.<String>;
        private var _fragmentDurationPairs:Vector.<FragmentDurationPair>;

        public function AdobeFragmentRunTable(){
            this._fragmentDurationPairs = new Vector.<FragmentDurationPair>();
        }
        public function get timeScale():uint{
            return (this._timeScale);
        }
        public function set timeScale(_arg1:uint):void{
            this._timeScale = _arg1;
        }
        public function get qualitySegmentURLModifiers():Vector.<String>{
            return (this._qualitySegmentURLModifiers);
        }
        public function set qualitySegmentURLModifiers(_arg1:Vector.<String>):void{
            this._qualitySegmentURLModifiers = _arg1;
        }
        public function get fragmentDurationPairs():Vector.<FragmentDurationPair>{
            return (this._fragmentDurationPairs);
        }
        public function addFragmentDurationPair(_arg1:FragmentDurationPair):void{
            this._fragmentDurationPairs.push(_arg1);
        }
        public function findFragmentIdByTime(_arg1:Number, _arg2:Number, _arg3:Boolean=false):FragmentAccessInformation{
            if (this._fragmentDurationPairs.length <= 0){
                return (null);
            };
            var _local4:FragmentDurationPair;
            var _local5:uint = 1;
            while (_local5 < this._fragmentDurationPairs.length) {
                _local4 = this._fragmentDurationPairs[_local5];
                if (_local4.durationAccrued >= _arg1){
                    return (this.validateFragment(this.calculateFragmentId(this._fragmentDurationPairs[(_local5 - 1)], _arg1), _arg2, _arg3));
                };
                _local5++;
            };
            return (this.validateFragment(this.calculateFragmentId(this._fragmentDurationPairs[(this._fragmentDurationPairs.length - 1)], _arg1), _arg2, _arg3));
        }
        public function validateFragment(_arg1:uint, _arg2:Number, _arg3:Boolean=false):FragmentAccessInformation{
            var _local7:FragmentDurationPair;
            var _local8:FragmentDurationPair;
            var _local9:Number;
            var _local10:Number;
            var _local11:Number;
            var _local12:FragmentDurationPair;
            var _local4:uint = (this._fragmentDurationPairs.length - 1);
            var _local5:FragmentAccessInformation;
            var _local6:uint;
            while (_local6 < _local4) {
                _local7 = this._fragmentDurationPairs[_local6];
                _local8 = this._fragmentDurationPairs[(_local6 + 1)];
                if ((((_local7.firstFragment <= _arg1)) && ((_arg1 < _local8.firstFragment)))){
                    if (_local7.duration <= 0){
                        _local5 = this.getNextValidFragment((_local6 + 1), _arg2);
                    } else {
                        _local5 = new FragmentAccessInformation();
                        _local5.fragId = _arg1;
                        _local5.fragDuration = _local7.duration;
                        _local5.fragmentEndTime = (_local7.durationAccrued + (_local7.duration * ((_arg1 - _local7.firstFragment) + 1)));
                    };
                    break;
                };
                if ((((_local7.firstFragment <= _arg1)) && (this.endOfStreamEntry(_local8)))){
                    if (_local7.duration > 0){
                        _local9 = (_arg2 - _local7.durationAccrued);
                        _local10 = (((_arg1 - _local7.firstFragment) + 1) * _local7.duration);
                        _local11 = ((_arg1 - _local7.firstFragment) * _local7.duration);
                        if (_local9 > _local11){
                            if (((!(_arg3)) || ((((_local11 + _local7.duration) + _local7.durationAccrued) <= _arg2)))){
                                _local5 = new FragmentAccessInformation();
                                _local5.fragId = _arg1;
                                _local5.fragDuration = _local7.duration;
                                if (_local9 >= _local10){
                                    _local5.fragmentEndTime = (_local7.durationAccrued + _local10);
                                } else {
                                    _local5.fragmentEndTime = (_local7.durationAccrued + _local9);
                                };
                                break;
                            };
                        };
                    };
                };
                _local6++;
            };
            if (_local5 == null){
                _local12 = this._fragmentDurationPairs[_local4];
                if ((((_local12.duration > 0)) && ((_arg1 >= _local12.firstFragment)))){
                    _local9 = (_arg2 - _local12.durationAccrued);
                    _local10 = (((_arg1 - _local12.firstFragment) + 1) * _local12.duration);
                    _local11 = ((_arg1 - _local12.firstFragment) * _local12.duration);
                    if (_local9 > _local11){
                        if (((!(_arg3)) || ((((_local11 + _local12.duration) + _local12.durationAccrued) <= _arg2)))){
                            _local5 = new FragmentAccessInformation();
                            _local5.fragId = _arg1;
                            _local5.fragDuration = _local12.duration;
                            if (_local9 >= _local10){
                                _local5.fragmentEndTime = (_local12.durationAccrued + _local10);
                            } else {
                                _local5.fragmentEndTime = (_local12.durationAccrued + _local9);
                            };
                        };
                    };
                };
            };
            return (_local5);
        }
        private function getNextValidFragment(_arg1:uint, _arg2:Number):FragmentAccessInformation{
            var _local5:FragmentDurationPair;
            var _local3:FragmentAccessInformation;
            var _local4:uint = _arg1;
            while (_local4 < this._fragmentDurationPairs.length) {
                _local5 = this._fragmentDurationPairs[_local4];
                if (_local5.duration > 0){
                    _local3 = new FragmentAccessInformation();
                    _local3.fragId = _local5.firstFragment;
                    _local3.fragDuration = _local5.duration;
                    _local3.fragmentEndTime = (_local5.durationAccrued + _local5.duration);
                    break;
                };
                _local4++;
            };
            return (_local3);
        }
        private function endOfStreamEntry(_arg1:FragmentDurationPair):Boolean{
            return ((((_arg1.duration == 0)) && ((_arg1.discontinuityIndicator == 0))));
        }
        public function fragmentsLeft(_arg1:uint, _arg2:Number):uint{
            if ((((this._fragmentDurationPairs == null)) || ((this._fragmentDurationPairs.length == 0)))){
                return (0);
            };
            var _local3:FragmentDurationPair = (this._fragmentDurationPairs[(this.fragmentDurationPairs.length - 1)] as FragmentDurationPair);
            var _local4:uint = (((((_arg2 - _local3.durationAccrued) / _local3.duration) + _local3.firstFragment) - _arg1) - 1);
            return (_local4);
        }
        public function tableComplete():Boolean{
            if ((((this._fragmentDurationPairs == null)) || ((this._fragmentDurationPairs.length <= 0)))){
                return (false);
            };
            var _local1:FragmentDurationPair = (this._fragmentDurationPairs[(this.fragmentDurationPairs.length - 1)] as FragmentDurationPair);
            return ((((_local1.duration == 0)) && ((_local1.discontinuityIndicator == 0))));
        }
        public function adjustEndEntryDurationAccrued(_arg1:Number):void{
            var _local2:FragmentDurationPair = this._fragmentDurationPairs[(this._fragmentDurationPairs.length - 1)];
            if (_local2.duration == 0){
                _local2.durationAccrued = _arg1;
            };
        }
        public function getFragmentDuration(_arg1:uint):Number{
            var _local2:FragmentDurationPair;
            var _local3:uint;
            while ((((_local3 < this._fragmentDurationPairs.length)) && ((this._fragmentDurationPairs[_local3].firstFragment <= _arg1)))) {
                _local3++;
            };
            if (_local3){
                return (this._fragmentDurationPairs[(_local3 - 1)].duration);
            };
            return (0);
        }
        private function findNextValidFragmentDurationPair(_arg1:uint):FragmentDurationPair{
            var _local3:FragmentDurationPair;
            var _local2:uint = _arg1;
            while (_local2 < this._fragmentDurationPairs.length) {
                _local3 = this._fragmentDurationPairs[_local2];
                if (_local3.duration > 0){
                    return (_local3);
                };
                _local2++;
            };
            return (null);
        }
        private function findPrevValidFragmentDurationPair(_arg1:uint):FragmentDurationPair{
            var _local3:FragmentDurationPair;
            var _local2:uint = _arg1;
            if (_local2 > this._fragmentDurationPairs.length){
                _local2 = this._fragmentDurationPairs.length;
            };
            while (_local2 > 0) {
                _local3 = this._fragmentDurationPairs[(_local2 - 1)];
                if (_local3.duration > 0){
                    return (_local3);
                };
                _local2--;
            };
            return (null);
        }
        private function calculateFragmentId(_arg1:FragmentDurationPair, _arg2:Number):uint{
            if (_arg1.duration <= 0){
                return (_arg1.firstFragment);
            };
            var _local3:Number = (_arg2 - _arg1.durationAccrued);
            return ((_arg1.firstFragment + uint((_local3 / _arg1.duration))));
        }
        public function get firstFragmentId():uint{
            var _local1:FragmentDurationPair = this.findNextValidFragmentDurationPair(0);
            if (_local1 == null){
                return (0);
            };
            return (_local1.firstFragment);
        }
        public function isFragmentInGap(_arg1:uint):Boolean{
            var inGap:* = false;
            var fragmentId:* = _arg1;
            inGap = false;
            this.forEachGap(function (_arg1:Object):Boolean{
                var _local2:FragmentDurationPair = (_arg1.fdp as FragmentDurationPair);
                var _local3:FragmentDurationPair = (_arg1.nextFdp as FragmentDurationPair);
                var _local4:Number = _local2.firstFragment;
                var _local5:Number = _local3.firstFragment;
                if ((((_local4 <= fragmentId)) && ((fragmentId < _local5)))){
                    inGap = true;
                };
                return (!(inGap));
            });
            return (inGap);
        }
        public function isTimeInGap(_arg1:Number, _arg2:uint):Boolean{
            var inGap:* = false;
            var time:* = _arg1;
            var fragmentInterval:* = _arg2;
            inGap = false;
            this.forEachGap(function (_arg1:Object):Boolean{
                var _local2:FragmentDurationPair = (_arg1.fdp as FragmentDurationPair);
                var _local3:FragmentDurationPair = (_arg1.prevFdp as FragmentDurationPair);
                var _local4:FragmentDurationPair = (_arg1.nextFdp as FragmentDurationPair);
                var _local5:Number = (_local3.durationAccrued + (_local3.duration * (_local2.firstFragment - _local3.firstFragment)));
                var _local6:Number = _local4.durationAccrued;
                var _local7:Number = ((Math.max(_local2.firstFragment, 1) - 1) * fragmentInterval);
                var _local8:Number = ((Math.max(_local4.firstFragment, (_local2.firstFragment + 1), 1) - 1) * fragmentInterval);
                var _local9:Number = Math.min(_local5, _local7);
                var _local10:Number = Math.max(_local6, _local8);
                if ((((_local9 <= time)) && ((time < _local10)))){
                    inGap = true;
                };
                return (!(inGap));
            });
            return (inGap);
        }
        public function countGapFragments():uint{
            var count:* = 0;
            count = 0;
            this.forEachGap(function (_arg1:Object):void{
                var _local2:FragmentDurationPair = (_arg1.fdp as FragmentDurationPair);
                var _local3:FragmentDurationPair = (_arg1.nextFdp as FragmentDurationPair);
                var _local4:Number = _local2.firstFragment;
                var _local5:Number = uint(Math.max(_local3.firstFragment, _local4));
                count = (count + (_local5 - _local4));
            });
            return (count);
        }
        private function forEachGap(_arg1:Function):void{
            var _local3:FragmentDurationPair;
            var _local4:FragmentDurationPair;
            var _local5:FragmentDurationPair;
            var _local6:Boolean;
            if (this._fragmentDurationPairs.length <= 0){
                return;
            };
            var _local2:uint;
            while (_local2 < this._fragmentDurationPairs.length) {
                _local3 = this._fragmentDurationPairs[_local2];
                if (((!((_local3.duration == 0))) || (!((_local3.discontinuityIndicator == 2))))){
                } else {
                    _local4 = this.findPrevValidFragmentDurationPair(_local2);
                    if ((((_local4 == null)) || ((_local4.firstFragment > _local3.firstFragment)))){
                    } else {
                        _local5 = this.findNextValidFragmentDurationPair((_local2 + 1));
                        if ((((_local5 == null)) || ((_local3.firstFragment > _local5.firstFragment)))){
                        } else {
                            _local6 = _arg1({
                                fdp:_local3,
                                prevFdp:_local4,
                                nextFdp:_local5
                            });
                            if (!_local6){
                                return;
                            };
                        };
                    };
                };
                _local2++;
            };
        }
        public function getFragmentWithIdGreq(_arg1:uint):FragmentAccessInformation{
            var fragmentId:* = _arg1;
            var desiredFdp:* = null;
            var desiredFragmentId:* = 0;
            this.forEachInterval(function (_arg1:Object):Boolean{
                var _local2:FragmentDurationPair = (_arg1.fdp as FragmentDurationPair);
                var _local3:Boolean = (_arg1.isLast as Boolean);
                var _local4:uint = (_arg1.startFragmentId as uint);
                var _local5:uint = (_arg1.endFragmentId as uint);
                if (fragmentId < _local4){
                    desiredFdp = _local2;
                    desiredFragmentId = _local4;
                    return (false);
                };
                if (_local3){
                    desiredFdp = _local2;
                    desiredFragmentId = fragmentId;
                    return (false);
                };
                if (fragmentId < _local5){
                    desiredFdp = _local2;
                    desiredFragmentId = fragmentId;
                    return (false);
                };
                return (true);
            });
            if (desiredFdp == null){
                return (null);
            };
            if (desiredFragmentId < desiredFdp.firstFragment){
                desiredFragmentId = desiredFdp.firstFragment;
            };
            var fai:* = new FragmentAccessInformation();
            fai.fragId = desiredFragmentId;
            fai.fragDuration = desiredFdp.duration;
            fai.fragmentEndTime = (desiredFdp.durationAccrued + (((desiredFragmentId - desiredFdp.firstFragment) + 1) * desiredFdp.duration));
            return (fai);
        }
        public function getFragmentWithTimeGreq(_arg1:Number):FragmentAccessInformation{
            var fragmentTime:* = _arg1;
            var desiredFdp:* = null;
            var desiredFragmentStartTime:* = 0;
            this.forEachInterval(function (_arg1:Object):Boolean{
                var _local2:FragmentDurationPair = (_arg1.fdp as FragmentDurationPair);
                var _local3:Boolean = (_arg1.isLast as Boolean);
                var _local4:Number = (_arg1.startTime as Number);
                var _local5:Number = (_arg1.endTime as Number);
                if (fragmentTime < _local4){
                    desiredFdp = _local2;
                    desiredFragmentStartTime = _local4;
                    return (false);
                };
                if (_local3){
                    desiredFdp = _local2;
                    desiredFragmentStartTime = fragmentTime;
                    return (false);
                };
                if (fragmentTime < _local5){
                    desiredFdp = _local2;
                    desiredFragmentStartTime = fragmentTime;
                    return (false);
                };
                return (true);
            });
            if (desiredFdp == null){
                return (null);
            };
            var desiredFragmentId:* = this.calculateFragmentId(desiredFdp, desiredFragmentStartTime);
            var fai:* = new FragmentAccessInformation();
            fai.fragId = desiredFragmentId;
            fai.fragDuration = desiredFdp.duration;
            fai.fragmentEndTime = (desiredFdp.durationAccrued + (((desiredFragmentId - desiredFdp.firstFragment) + 1) * desiredFdp.duration));
            return (fai);
        }
        private function forEachInterval(_arg1:Function):void{
            var _local3:FragmentDurationPair;
            var _local4:uint;
            var _local5:Number;
            var _local6:Boolean;
            var _local7:uint;
            var _local8:uint;
            var _local9:Number;
            var _local10:Boolean;
            var _local2:uint;
            for (;_local2 < this._fragmentDurationPairs.length;_local2++) {
                _local3 = this._fragmentDurationPairs[_local2];
                if (_local3.duration == 0){
                } else {
                    _local4 = _local3.firstFragment;
                    _local5 = _local3.durationAccrued;
                    _local6 = true;
                    _local7 = (_local2 + 1);
                    while (_local7 < this._fragmentDurationPairs.length) {
                        if (((((((!((this._fragmentDurationPairs[_local7].duration == 0))) || ((this._fragmentDurationPairs[_local7].discontinuityIndicator == 1)))) || ((this._fragmentDurationPairs[_local7].discontinuityIndicator == 2)))) || ((this._fragmentDurationPairs[_local7].discontinuityIndicator == 3)))){
                            _local6 = false;
                            break;
                        };
                        _local7++;
                    };
                    if (_local6){
                        _local8 = 0;
                        _local9 = Number.NaN;
                    } else {
                        _local8 = this._fragmentDurationPairs[_local7].firstFragment;
                        if (_local4 > _local8){
                            continue;
                        };
                        _local9 = (_local5 + ((_local8 - _local4) * _local3.duration));
                    };
                    _local10 = _arg1({
                        fdp:_local3,
                        isLast:_local6,
                        startFragmentId:_local4,
                        endFragmentId:_local8,
                        startTime:_local5,
                        endTime:_local9
                    });
                    if (((!(_local10)) || (_local6))){
                        return;
                    };
                };
            };
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
