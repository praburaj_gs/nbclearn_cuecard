﻿package org.osmf.net.httpstreaming.f4f {
    import org.osmf.net.httpstreaming.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.utils.*;

    public class HTTPStreamingF4FFileHandler extends HTTPStreamingFileHandlerBase {

        private var _afra:AdobeFragmentRandomAccessBox;
        private var _ba:ByteArray;
        private var _boxInfoPending:Boolean;
        private var _bytesNeeded:uint;
        private var _bytesReadSinceAfraStart:uint;
        private var _countingReadBytes:Boolean;
        private var _mdatBytesPending:uint;
        private var _nextBox:BoxInfo;
        private var _parser:BoxParser;
        private var _seekToTime:Number;
        private var _mdatBytesOffset:Number;
        private var _processRequestWasSeek:Boolean = false;

        public function HTTPStreamingF4FFileHandler(){
            this._parser = new BoxParser();
            super();
        }
        override public function beginProcessFile(_arg1:Boolean, _arg2:Number):void{
            this._processRequestWasSeek = _arg1;
            this._seekToTime = ((_arg1) ? _arg2 : 0);
            this._bytesNeeded = (((F4FConstants.FIELD_SIZE_LENGTH + F4FConstants.FIELD_TYPE_LENGTH) + F4FConstants.FIELD_LARGE_SIZE_LENGTH) + F4FConstants.FIELD_EXTENDED_TYPE_LENGTH);
            this._bytesReadSinceAfraStart = 0;
            this._countingReadBytes = false;
            this._boxInfoPending = true;
            this._nextBox = null;
        }
        override public function get inputBytesNeeded():Number{
            return (this._bytesNeeded);
        }
        override public function processFileSegment(_arg1:IDataInput):ByteArray{
            var _local4:uint;
            var _local5:AdobeBootstrapBox;
            if (_arg1.bytesAvailable < this._bytesNeeded){
                return (null);
            };
            var _local2:ByteArray;
            var _local3:Number = (F4FConstants.FIELD_SIZE_LENGTH + F4FConstants.FIELD_TYPE_LENGTH);
            if (this._boxInfoPending){
                this._ba = new ByteArray();
                _arg1.readBytes(this._ba, 0, _local3);
                if (this._countingReadBytes){
                    this._bytesReadSinceAfraStart = (this._bytesReadSinceAfraStart + _local3);
                };
                this._parser.init(this._ba);
                this._nextBox = this._parser.getNextBoxInfo();
                if (this._nextBox.size == F4FConstants.FLAG_USE_LARGE_SIZE){
                    _local3 = (_local3 + F4FConstants.FIELD_LARGE_SIZE_LENGTH);
                    this._ba.position = 0;
                    _arg1.readBytes(this._ba, 0, F4FConstants.FIELD_LARGE_SIZE_LENGTH);
                    if (this._countingReadBytes){
                        this._bytesReadSinceAfraStart = (this._bytesReadSinceAfraStart + F4FConstants.FIELD_LARGE_SIZE_LENGTH);
                    };
                    this._nextBox.size = this._parser.readLongUIntToNumber();
                };
                this._boxInfoPending = false;
                if (this._nextBox.type == F4FConstants.BOX_TYPE_MDAT){
                    this._bytesNeeded = 0;
                    this._mdatBytesPending = (this._nextBox.size - _local3);
                } else {
                    this._bytesNeeded = (this._nextBox.size - _local3);
                    this._mdatBytesPending = 0;
                    if (this._nextBox.type == F4FConstants.BOX_TYPE_AFRA){
                        this._bytesReadSinceAfraStart = _local3;
                        this._countingReadBytes = true;
                    };
                };
            } else {
                if (this._bytesNeeded > 0){
                    _local4 = this._ba.position;
                    _arg1.readBytes(this._ba, this._ba.length, (this._nextBox.size - _local3));
                    if (this._countingReadBytes){
                        this._bytesReadSinceAfraStart = (this._bytesReadSinceAfraStart + (this._nextBox.size - _local3));
                    };
                    this._ba.position = _local4;
                    if (this._nextBox.type == F4FConstants.BOX_TYPE_ABST){
                        _local5 = this._parser.readAdobeBootstrapBox(this._nextBox);
                        if (_local5 != null){
                            dispatchEvent(new HTTPStreamingFileHandlerEvent(HTTPStreamingFileHandlerEvent.NOTIFY_BOOTSTRAP_BOX, false, false, 0, null, null, _local5));
                        };
                    } else {
                        if (this._nextBox.type == F4FConstants.BOX_TYPE_AFRA){
                            this._afra = this._parser.readFragmentRandomAccessBox(this._nextBox);
                            this.processSeekToTime();
                        } else {
                            if (this._nextBox.type == F4FConstants.BOX_TYPE_MOOF){
                            };
                        };
                    };
                    this._bytesNeeded = (((F4FConstants.FIELD_SIZE_LENGTH + F4FConstants.FIELD_TYPE_LENGTH) + F4FConstants.FIELD_LARGE_SIZE_LENGTH) + F4FConstants.FIELD_EXTENDED_TYPE_LENGTH);
                    this._boxInfoPending = true;
                    this._nextBox = null;
                } else {
                    _local2 = this.getMDATBytes(_arg1, false);
                };
            };
            return (_local2);
        }
        override public function endProcessFile(_arg1:IDataInput):ByteArray{
            if (this._bytesNeeded > 0){
                dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.FILE_ERROR, false, false, 0, null, null));
            };
            return (this.getMDATBytes(_arg1, true));
        }
        override public function flushFileSegment(_arg1:IDataInput):ByteArray{
            return (null);
        }
        private function getMDATBytes(_arg1:IDataInput, _arg2:Boolean):ByteArray{
            var _local3:ByteArray;
            var _local4:uint;
            if (_arg1 == null){
                return (null);
            };
            this.skipSeekBytes(_arg1);
            if (this._mdatBytesPending > 0){
                _local4 = (((this._mdatBytesPending < _arg1.bytesAvailable)) ? this._mdatBytesPending : _arg1.bytesAvailable);
                if (((!(_arg2)) && ((_local4 > OSMFSettings.hdsBytesReadingLimit)))){
                    _local4 = OSMFSettings.hdsBytesReadingLimit;
                };
                _local3 = new ByteArray();
                this._mdatBytesPending = (this._mdatBytesPending - _local4);
                _arg1.readBytes(_local3, 0, _local4);
            };
            return (_local3);
        }
        private function skipSeekBytes(_arg1:IDataInput):void{
            var _local2:uint;
            var _local3:ByteArray;
            if (this._bytesReadSinceAfraStart < this._mdatBytesOffset){
                _local2 = (this._mdatBytesOffset - this._bytesReadSinceAfraStart);
                if (_arg1.bytesAvailable < _local2){
                    _local2 = _arg1.bytesAvailable;
                };
                _local3 = new ByteArray();
                _arg1.readBytes(_local3, 0, _local2);
                this._bytesReadSinceAfraStart = (this._bytesReadSinceAfraStart + _local2);
                this._mdatBytesPending = (this._mdatBytesPending - _local2);
            };
        }
        private function processSeekToTime():void{
            var _local1:Number = 0;
            var _local2:LocalRandomAccessEntry;
            if (this._seekToTime <= 0){
                this._mdatBytesOffset = 0;
            } else {
                _local2 = this.getMDATBytesOffset(this._seekToTime);
                if (_local2 != null){
                    this._mdatBytesOffset = _local2.offset;
                    _local1 = _local2.time;
                } else {
                    this._mdatBytesOffset = 0;
                };
            };
        }
        private function getMDATBytesOffset(_arg1:Number):LocalRandomAccessEntry{
            return (((isNaN(_arg1)) ? null : this._afra.findNearestKeyFrameOffset((_arg1 * this._afra.timeScale))));
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
