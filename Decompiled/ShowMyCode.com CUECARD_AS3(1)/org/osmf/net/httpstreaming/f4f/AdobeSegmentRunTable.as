﻿package org.osmf.net.httpstreaming.f4f {
    import __AS3__.vec.*;
    import org.osmf.net.httpstreaming.f4f.*;

    class AdobeSegmentRunTable extends FullBox {

        private var _qualitySegmentURLModifiers:Vector.<String>;
        private var _segmentFragmentPairs:Vector.<SegmentFragmentPair>;

        public function AdobeSegmentRunTable(){
            this._segmentFragmentPairs = new Vector.<SegmentFragmentPair>();
        }
        public function get qualitySegmentURLModifiers():Vector.<String>{
            return (this._qualitySegmentURLModifiers);
        }
        public function set qualitySegmentURLModifiers(_arg1:Vector.<String>):void{
            this._qualitySegmentURLModifiers = _arg1;
        }
        public function get segmentFragmentPairs():Vector.<SegmentFragmentPair>{
            return (this._segmentFragmentPairs);
        }
        public function addSegmentFragmentPair(_arg1:SegmentFragmentPair):void{
            var _local2:SegmentFragmentPair = (((this._segmentFragmentPairs.length <= 0)) ? null : this._segmentFragmentPairs[(this._segmentFragmentPairs.length - 1)]);
            var _local3:uint;
            if (_local2 != null){
                _local3 = (_local2.fragmentsAccrued + ((_arg1.firstSegment - _local2.firstSegment) * _local2.fragmentsPerSegment));
            };
            _arg1.fragmentsAccrued = _local3;
            this._segmentFragmentPairs.push(_arg1);
        }
        public function findSegmentIdByFragmentId(_arg1:uint):uint{
            var _local2:SegmentFragmentPair;
            if (_arg1 < 1){
                return (0);
            };
            var _local3:uint = 1;
            while (_local3 < this._segmentFragmentPairs.length) {
                _local2 = this._segmentFragmentPairs[_local3];
                if (_local2.fragmentsAccrued >= _arg1){
                    return (this.calculateSegmentId(this._segmentFragmentPairs[(_local3 - 1)], _arg1));
                };
                _local3++;
            };
            return (this.calculateSegmentId(this._segmentFragmentPairs[(this._segmentFragmentPairs.length - 1)], _arg1));
        }
        public function get totalFragments():uint{
            return ((this._segmentFragmentPairs[(this._segmentFragmentPairs.length - 1)].fragmentsPerSegment + this._segmentFragmentPairs[(this._segmentFragmentPairs.length - 1)].fragmentsAccrued));
        }
        private function calculateSegmentId(_arg1:SegmentFragmentPair, _arg2:uint):uint{
            return ((_arg1.firstSegment + int((((_arg2 - _arg1.fragmentsAccrued) - 1) / _arg1.fragmentsPerSegment))));
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
