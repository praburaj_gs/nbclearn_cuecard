﻿package org.osmf.net.httpstreaming.f4f {
    import __AS3__.vec.*;

    public class AdobeBootstrapBox extends FullBox {

        private var _bootstrapVersion:uint;
        private var _profile:uint;
        private var _live:Boolean;
        private var _update:Boolean;
        private var _timeScale:uint;
        private var _currentMediaTime:Number;
        private var _smpteTimeCodeOffset:Number;
        private var _movieIdentifier:String;
        private var _serverEntryCount:uint;
        private var _serverBaseURLs:Vector.<String>;
        private var _qualitySegmentURLModifiers:Vector.<String>;
        private var _drmData:String;
        private var _metadata:String;
        private var _segmentRunTables:Vector.<AdobeSegmentRunTable>;
        private var _fragmentRunTables:Vector.<AdobeFragmentRunTable>;

        public function get bootstrapVersion():uint{
            return (this._bootstrapVersion);
        }
        public function set bootstrapVersion(_arg1:uint):void{
            this._bootstrapVersion = _arg1;
        }
        public function get profile():uint{
            return (this._profile);
        }
        public function set profile(_arg1:uint):void{
            this._profile = _arg1;
        }
        public function get live():Boolean{
            return (this._live);
        }
        public function set live(_arg1:Boolean):void{
            this._live = _arg1;
        }
        public function get update():Boolean{
            return (this._update);
        }
        public function set update(_arg1:Boolean):void{
            this._update = _arg1;
        }
        public function get timeScale():uint{
            return (this._timeScale);
        }
        public function set timeScale(_arg1:uint):void{
            this._timeScale = _arg1;
        }
        public function get currentMediaTime():Number{
            return (this._currentMediaTime);
        }
        public function set currentMediaTime(_arg1:Number):void{
            this._currentMediaTime = _arg1;
        }
        public function get smpteTimeCodeOffset():Number{
            return (this._smpteTimeCodeOffset);
        }
        public function set smpteTimeCodeOffset(_arg1:Number):void{
            this._smpteTimeCodeOffset = _arg1;
        }
        public function get movieIdentifier():String{
            return (this._movieIdentifier);
        }
        public function set movieIdentifier(_arg1:String):void{
            this._movieIdentifier = _arg1;
        }
        public function get serverBaseURLs():Vector.<String>{
            return (this._serverBaseURLs);
        }
        public function set serverBaseURLs(_arg1:Vector.<String>):void{
            this._serverBaseURLs = _arg1;
        }
        public function get qualitySegmentURLModifiers():Vector.<String>{
            return (this._qualitySegmentURLModifiers);
        }
        public function set qualitySegmentURLModifiers(_arg1:Vector.<String>):void{
            this._qualitySegmentURLModifiers = _arg1;
        }
        public function get drmData():String{
            return (this._drmData);
        }
        public function set drmData(_arg1:String):void{
            this._drmData = _arg1;
        }
        public function get metadata():String{
            return (this._metadata);
        }
        public function set metadata(_arg1:String):void{
            this._metadata = _arg1;
        }
        public function get segmentRunTables():Vector.<AdobeSegmentRunTable>{
            return (this._segmentRunTables);
        }
        public function set segmentRunTables(_arg1:Vector.<AdobeSegmentRunTable>):void{
            this._segmentRunTables = _arg1;
        }
        public function get fragmentRunTables():Vector.<AdobeFragmentRunTable>{
            return (this._fragmentRunTables);
        }
        public function set fragmentRunTables(_arg1:Vector.<AdobeFragmentRunTable>):void{
            var _local2:AdobeFragmentRunTable;
            this._fragmentRunTables = _arg1;
            if (((!((_arg1 == null))) && ((_arg1.length > 0)))){
                _local2 = _arg1[(_arg1.length - 1)];
                _local2.adjustEndEntryDurationAccrued(this._currentMediaTime);
            };
        }
        public function findSegmentId(_arg1:uint):uint{
            return (this._segmentRunTables[0].findSegmentIdByFragmentId(_arg1));
        }
        public function get totalFragments():uint{
            var _local1:AdobeFragmentRunTable = this._fragmentRunTables[(this._fragmentRunTables.length - 1)];
            var _local2:Vector.<FragmentDurationPair> = _local1.fragmentDurationPairs;
            var _local3:FragmentDurationPair = _local2[(_local2.length - 1)];
            if (_local3.duration == 0){
                _local3 = _local2[(_local2.length - 2)];
            };
            var _local4:Number = (this._currentMediaTime - _local3.durationAccrued);
            var _local5:uint = ((_local4)<=0) ? 0 : (_local4 / _local3.duration);
            return (((_local3.firstFragment + _local5) - 1));
        }
        public function get totalDuration():uint{
            if ((((this._fragmentRunTables == null)) || ((this._fragmentRunTables.length < 1)))){
                return (0);
            };
            var _local1:AdobeFragmentRunTable = this._fragmentRunTables[0];
            return ((this._currentMediaTime - _local1.fragmentDurationPairs[0].durationAccrued));
        }
        public function contentComplete():Boolean{
            var _local1:AdobeFragmentRunTable = this._fragmentRunTables[(this._fragmentRunTables.length - 1)];
            return (_local1.tableComplete());
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
