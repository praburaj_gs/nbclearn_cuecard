﻿package org.osmf.net.httpstreaming.f4f {
    import flash.utils.*;

    class MediaDataBox extends Box {

        private var _data:ByteArray;

        public function get data():ByteArray{
            return (this._data);
        }
        public function set data(_arg1:ByteArray):void{
            this._data = _arg1;
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
