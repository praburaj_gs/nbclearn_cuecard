﻿package org.osmf.net.httpstreaming.f4f {
    import org.osmf.media.*;
    import org.osmf.net.httpstreaming.*;

    public class HTTPStreamingF4FFactory extends HTTPStreamingFactory {

        override public function createFileHandler(_arg1:MediaResourceBase):HTTPStreamingFileHandlerBase{
            return (new HTTPStreamingF4FFileHandler());
        }
        override public function createIndexHandler(_arg1:MediaResourceBase, _arg2:HTTPStreamingFileHandlerBase):HTTPStreamingIndexHandlerBase{
            return (new HTTPStreamingF4FIndexHandler(_arg2));
        }
        override public function createIndexInfo(_arg1:MediaResourceBase):HTTPStreamingIndexInfoBase{
            return (HTTPStreamingUtils.createF4FIndexInfo((_arg1 as URLResource)));
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
