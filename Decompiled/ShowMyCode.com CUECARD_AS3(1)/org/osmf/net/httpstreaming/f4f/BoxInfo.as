﻿package org.osmf.net.httpstreaming.f4f {

    class BoxInfo {

        private var _size:Number;
        private var _type:String;

        public function BoxInfo(_arg1:Number, _arg2:String){
            this._size = _arg1;
            this._type = _arg2;
        }
        public function get size():Number{
            return (this._size);
        }
        public function set size(_arg1:Number):void{
            this._size = _arg1;
        }
        public function get type():String{
            return (this._type);
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
