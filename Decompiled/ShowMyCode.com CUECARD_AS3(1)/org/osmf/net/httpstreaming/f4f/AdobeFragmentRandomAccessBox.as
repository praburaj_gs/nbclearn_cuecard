﻿package org.osmf.net.httpstreaming.f4f {
    import __AS3__.vec.*;
    import org.osmf.net.httpstreaming.f4f.*;

    class AdobeFragmentRandomAccessBox extends FullBox {

        private var _timeScale:uint;
        private var _localRandomAccessEntries:Vector.<LocalRandomAccessEntry>;
        private var _globalRandomAccessEntries:Vector.<GlobalRandomAccessEntry>;

        public function get timeScale():uint{
            return (this._timeScale);
        }
        public function set timeScale(_arg1:uint):void{
            this._timeScale = _arg1;
        }
        public function get localRandomAccessEntries():Vector.<LocalRandomAccessEntry>{
            return (this._localRandomAccessEntries);
        }
        public function set localRandomAccessEntries(_arg1:Vector.<LocalRandomAccessEntry>):void{
            this._localRandomAccessEntries = _arg1;
        }
        public function get globalRandomAccessEntries():Vector.<GlobalRandomAccessEntry>{
            return (this._globalRandomAccessEntries);
        }
        public function set globalRandomAccessEntries(_arg1:Vector.<GlobalRandomAccessEntry>):void{
            this._globalRandomAccessEntries = _arg1;
        }
        public function findNearestKeyFrameOffset(_arg1:Number):LocalRandomAccessEntry{
            var _local3:LocalRandomAccessEntry;
            var _local2:int = (this._localRandomAccessEntries.length - 1);
            while (_local2 >= 0) {
                _local3 = this._localRandomAccessEntries[_local2];
                if (_local3.time <= _arg1){
                    return (_local3);
                };
                _local2--;
            };
            return (null);
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
