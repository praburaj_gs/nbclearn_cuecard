﻿package org.osmf.net.httpstreaming.f4f {

    class FragmentDurationPair {

        private var _firstFragment:uint;
        private var _duration:uint;
        private var _durationAccrued:Number;
        private var _discontinuityIndicator:uint = 0;

        public function get firstFragment():uint{
            return (this._firstFragment);
        }
        public function set firstFragment(_arg1:uint):void{
            this._firstFragment = _arg1;
        }
        public function get duration():uint{
            return (this._duration);
        }
        public function set duration(_arg1:uint):void{
            this._duration = _arg1;
        }
        public function get durationAccrued():Number{
            return (this._durationAccrued);
        }
        public function set durationAccrued(_arg1:Number):void{
            this._durationAccrued = _arg1;
        }
        public function get discontinuityIndicator():uint{
            return (this._discontinuityIndicator);
        }
        public function set discontinuityIndicator(_arg1:uint):void{
            this._discontinuityIndicator = _arg1;
        }

    }
}//package org.osmf.net.httpstreaming.f4f 
