﻿package org.osmf.net.httpstreaming {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.net.*;
    import flash.net.*;
    import org.osmf.net.qos.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.net.httpstreaming.dvr.*;
    import org.osmf.net.httpstreaming.flv.*;
    import org.osmf.utils.*;

    public class HTTPNetStream extends NetStream {

        private static const HIGH_PRIORITY:int = 2147483647;

        private var _desiredBufferTime_Min:Number = 0;
        private var _desiredBufferTime_Max:Number = 0;
        private var _mainTimer:Timer = null;
        private var _state:String = "init";
        private var _playStreamName:String = null;
        private var _playStart:Number = -1;
        private var _playForDuration:Number = -1;
        private var _resource:URLResource = null;
        private var _factory:HTTPStreamingFactory = null;
        private var _mixer:HTTPStreamMixer = null;
        private var _videoHandler:IHTTPStreamHandler = null;
        private var _source:IHTTPStreamSource = null;
        private var _qualityLevelNeedsChanging:Boolean = false;
        private var _desiredQualityStreamName:String = null;
        private var _audioStreamNeedsChanging:Boolean = false;
        private var _desiredAudioStreamName:String = null;
        private var _seekTarget:Number = -1;
        private var _enhancedSeekTarget:Number = -1;
        private var _enhancedSeekTags:Vector.<FLVTag>;
        private var _notifyPlayStartPending:Boolean = false;
        private var _notifyPlayUnpublishPending:Boolean = false;
        private var _initialTime:Number = -1;
        private var _seekTime:Number = -1;
        private var _lastValidTimeTime:Number = 0;
        private var _initializeFLVParser:Boolean = false;
        private var _flvParser:FLVParser = null;
        private var _flvParserDone:Boolean = true;
        private var _flvParserProcessed:uint;
        private var _flvParserIsSegmentStart:Boolean = false;
        private var _insertScriptDataTags:Vector.<FLVTagScriptDataObject> = null;
        private var _fileTimeAdjustment:Number = 0;
        private var _mediaFragmentDuration:Number = 0;
        private var _dvrInfo:DVRInfo = null;
        private var _waitForDRM:Boolean = false;
        private var maxFPS:Number = 0;
        private var playbackDetailsRecorder:NetStreamPlaybackDetailsRecorder = null;
        private var lastTransitionIndex:int = -1;
        private var lastTransitionStreamURL:String = null;
        private var lastTime:Number = NaN;
        private var timeBeforeSeek:Number = NaN;
        private var seeking:Boolean = false;
        private var emptyBufferInterruptionSinceLastQoSUpdate:Boolean = false;
        private var _bytesLoaded:uint = 0;
        private var _wasSourceLiveStalled:Boolean = false;
        private var _issuedLiveStallNetStatus:Boolean = false;
        private var _wasBufferEmptied:Boolean = false;
        private var _isPlaying:Boolean = false;
        private var _isPaused:Boolean = false;
        private var _liveStallStartTime:Date;

        public function HTTPNetStream(_arg1:NetConnection, _arg2:HTTPStreamingFactory, _arg3:URLResource=null){
            super(_arg1);
            this._resource = _arg3;
            this._factory = _arg2;
            addEventListener(DVRStreamInfoEvent.DVRSTREAMINFO, this.onDVRStreamInfo);
            addEventListener(HTTPStreamingEvent.SCRIPT_DATA, this.onScriptData);
            addEventListener(HTTPStreamingEvent.BEGIN_FRAGMENT, this.onBeginFragment);
            addEventListener(HTTPStreamingEvent.END_FRAGMENT, this.onEndFragment);
            addEventListener(HTTPStreamingEvent.TRANSITION, this.onTransition);
            addEventListener(HTTPStreamingEvent.TRANSITION_COMPLETE, this.onTransitionComplete);
            addEventListener(HTTPStreamingEvent.ACTION_NEEDED, this.onActionNeeded);
            addEventListener(HTTPStreamingEvent.DOWNLOAD_ERROR, this.onDownloadError);
            addEventListener(HTTPStreamingEvent.DOWNLOAD_COMPLETE, this.onDownloadComplete);
            addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus, false, HIGH_PRIORITY, true);
            addEventListener(DRMErrorEvent.DRM_ERROR, this.onDRMError);
            addEventListener(DRMStatusEvent.DRM_STATUS, this.onDRMStatus);
            this.bufferTime = OSMFSettings.hdsMinimumBufferTime;
            this.bufferTimeMax = 0;
            this.setState(HTTPStreamingState.INIT);
            this.createSource(_arg3);
            this._mainTimer = new Timer(OSMFSettings.hdsMainTimerInterval);
            this._mainTimer.addEventListener(TimerEvent.TIMER, this.onMainTimer);
        }
        override public function set client(_arg1:Object):void{
            super.client = _arg1;
            if ((((client is NetClient)) && ((this._resource is DynamicStreamingResource)))){
                this.playbackDetailsRecorder = new NetStreamPlaybackDetailsRecorder(this, (client as NetClient), (this._resource as DynamicStreamingResource));
            };
        }
        override public function play(... _args):void{
            this.processPlayParameters(_args);
            super.play(null);
            var _local2:FLVHeader = new FLVHeader();
            var _local3:ByteArray = new ByteArray();
            _local2.write(_local3);
            this.attemptAppendBytes(_local3);
            this._mainTimer.start();
            this._initialTime = -1;
            this._seekTime = -1;
            this._isPlaying = true;
            this._isPaused = false;
            this._notifyPlayStartPending = true;
            this._notifyPlayUnpublishPending = false;
            this.changeSourceTo(this._playStreamName, this._playStart);
        }
        override public function pause():void{
            this._isPaused = true;
            super.pause();
        }
        override public function resume():void{
            this._isPaused = false;
            super.resume();
        }
        override public function play2(_arg1:NetStreamPlayOptions):void{
            switch (_arg1.transition){
                case NetStreamPlayTransitions.RESET:
                    this.play(_arg1.streamName, _arg1.start, _arg1.len);
                    break;
                case NetStreamPlayTransitions.SWITCH:
                    this.changeQualityLevelTo(_arg1.streamName);
                    break;
                case NetStreamPlayTransitions.SWAP:
                    this.changeAudioStreamTo(_arg1.streamName);
                    break;
                default:
                    super.play2(_arg1);
            };
        }
        override public function seek(_arg1:Number):void{
            if (_arg1 < 0){
                _arg1 = 0;
            };
            if (this._state != HTTPStreamingState.INIT){
                if (this._initialTime < 0){
                    this._seekTarget = (_arg1 + 0);
                } else {
                    this._seekTarget = (_arg1 + this._initialTime);
                };
                this.setState(HTTPStreamingState.SEEK);
                dispatchEvent(new NetStatusEvent(NetStatusEvent.NET_STATUS, false, false, {
                    code:NetStreamCodes.NETSTREAM_SEEK_START,
                    level:"status"
                }));
            };
            this._notifyPlayUnpublishPending = false;
        }
        override public function close():void{
            if (this._videoHandler != null){
                this._videoHandler.close();
            };
            if (this._mixer != null){
                this._mixer.close();
            };
            this._mainTimer.stop();
            this.notifyPlayStop();
            this.setState(HTTPStreamingState.HALT);
            super.close();
        }
        override public function set bufferTime(_arg1:Number):void{
            super.bufferTime = _arg1;
            this._desiredBufferTime_Min = Math.max(OSMFSettings.hdsMinimumBufferTime, _arg1);
            this._desiredBufferTime_Max = (this._desiredBufferTime_Min + OSMFSettings.hdsAdditionalBufferTime);
        }
        override public function get time():Number{
            if ((((this._seekTime >= 0)) && ((this._initialTime >= 0)))){
                this._lastValidTimeTime = ((super.time + this._seekTime) - this._initialTime);
            };
            return (this._lastValidTimeTime);
        }
        override public function get bytesLoaded():uint{
            return (this._bytesLoaded);
        }
        public function DVRGetStreamInfo(_arg1:Object):void{
            if (this._source.isReady){
            } else {
                this._videoHandler.getDVRInfo(_arg1);
            };
        }
        public function get isBestEffortFetchEnabled():Boolean{
            return (((!((this._source == null))) && (this._source.isBestEffortFetchEnabled)));
        }
        private function setState(_arg1:String):void{
            this._state = _arg1;
        }
        private function processPlayParameters(_arg1:Array):void{
            if (_arg1.length < 1){
                throw (new Error("HTTPNetStream.play() requires at least one argument"));
            };
            this._playStreamName = _arg1[0];
            this._playStart = 0;
            if (_arg1.length >= 2){
                this._playStart = Number(_arg1[1]);
            };
            this._playForDuration = -1;
            if (_arg1.length >= 3){
                this._playForDuration = Number(_arg1[2]);
            };
        }
        private function changeSourceTo(_arg1:String, _arg2:Number):void{
            this._initializeFLVParser = true;
            this._seekTarget = _arg2;
            this._videoHandler.open(_arg1);
            this.setState(HTTPStreamingState.SEEK);
        }
        private function changeQualityLevelTo(_arg1:String):void{
            this._qualityLevelNeedsChanging = true;
            this._desiredQualityStreamName = _arg1;
            if (((this._source.isReady) && (((!((this._videoHandler == null))) && (!((this._videoHandler.streamName == this._desiredQualityStreamName))))))){
                this._videoHandler.changeQualityLevel(this._desiredQualityStreamName);
                this._qualityLevelNeedsChanging = false;
                this._desiredQualityStreamName = null;
            };
            this._notifyPlayUnpublishPending = false;
        }
        private function changeAudioStreamTo(_arg1:String):void{
            var _local2:MediaResourceBase;
            if (this._mixer == null){
                this._audioStreamNeedsChanging = false;
                this._desiredAudioStreamName = null;
                return;
            };
            this._audioStreamNeedsChanging = true;
            this._desiredAudioStreamName = _arg1;
            if (((this._videoHandler.isOpen) && ((((((this._mixer.audio == null)) && (!((this._desiredAudioStreamName == null))))) || (((!((this._mixer.audio == null))) && (!((this._mixer.audio.streamName == this._desiredAudioStreamName))))))))){
                _local2 = HTTPStreamingUtils.createHTTPStreamingResource(this._resource, this._desiredAudioStreamName);
                if (_local2 != null){
                    this._mixer.audio = new HTTPStreamSource(this._factory, _local2, this._mixer);
                    this._mixer.audio.open(this._desiredAudioStreamName);
                } else {
                    this._mixer.audio = null;
                };
                this._audioStreamNeedsChanging = false;
                this._desiredAudioStreamName = null;
            };
            this._notifyPlayUnpublishPending = false;
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_BUFFER_EMPTY:
                    this.emptyBufferInterruptionSinceLastQoSUpdate = true;
                    this._wasBufferEmptied = true;
                    if (this._state == HTTPStreamingState.HALT){
                        if (this._notifyPlayUnpublishPending){
                            this.notifyPlayUnpublish();
                            this._notifyPlayUnpublishPending = false;
                        };
                    };
                    break;
                case NetStreamCodes.NETSTREAM_BUFFER_FULL:
                    this._wasBufferEmptied = false;
                    break;
                case NetStreamCodes.NETSTREAM_BUFFER_FLUSH:
                    this._wasBufferEmptied = false;
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_STREAMNOTFOUND:
                    this.close();
                    break;
                case NetStreamCodes.NETSTREAM_SEEK_NOTIFY:
                    if (!_arg1.info.hasOwnProperty("sentFromHTTPNetStream")){
                        _arg1.stopImmediatePropagation();
                    };
                    break;
            };
            if (_arg1.info.code == NetStreamCodes.NETSTREAM_DRM_UPDATE){
                this._waitForDRM = true;
            };
        }
        private function onMainTimer(_arg1:TimerEvent):void{
            var _local2:int;
            var _local3:Boolean;
            var _local4:Object;
            var _local5:FLVTagScriptDataObject;
            var _local6:ByteArray;
            var _local7:Object;
            var _local8:FLVTagScriptDataObject;
            var _local9:ByteArray;
            if (((this.seeking) && (!((this.time == this.timeBeforeSeek))))){
                this.seeking = false;
                this.timeBeforeSeek = Number.NaN;
                dispatchEvent(new NetStatusEvent(NetStatusEvent.NET_STATUS, false, false, {
                    code:NetStreamCodes.NETSTREAM_SEEK_NOTIFY,
                    level:"status",
                    seekPoint:this.time,
                    sentFromHTTPNetStream:true
                }));
            };
            if (currentFPS > this.maxFPS){
                this.maxFPS = currentFPS;
            };
            switch (this._state){
                case HTTPStreamingState.INIT:
                    break;
                case HTTPStreamingState.WAIT:
                    if (((!(this._waitForDRM)) && ((((this.bufferLength < this._desiredBufferTime_Min)) || (this.checkIfExtraBufferingNeeded()))))){
                        this.setState(HTTPStreamingState.PLAY);
                    };
                    break;
                case HTTPStreamingState.SEEK:
                    if (this._source.isReady){
                        this.timeBeforeSeek = this.time;
                        this.seeking = true;
                        this._flvParser = null;
                        if (this._enhancedSeekTags != null){
                            this._enhancedSeekTags.length = 0;
                            this._enhancedSeekTags = null;
                        };
                        this._enhancedSeekTarget = this._seekTarget;
                        super.seek(0);
                        appendBytesAction(NetStreamAppendBytesAction.RESET_SEEK);
                        this._wasBufferEmptied = true;
                        if (this.playbackDetailsRecorder != null){
                            if (this.playbackDetailsRecorder.playingIndex != this.lastTransitionIndex){
                                _local7 = new Object();
                                _local7.code = NetStreamCodes.NETSTREAM_PLAY_TRANSITION_COMPLETE;
                                _local7.level = "status";
                                _local7.details = this.lastTransitionStreamURL;
                                _local8 = new FLVTagScriptDataObject();
                                _local8.objects = ["onPlayStatus", _local7];
                                this.insertScriptDataTag(_local8);
                            };
                        };
                        this._seekTime = -1;
                        this._source.seek(this._seekTarget);
                        this.setState(HTTPStreamingState.WAIT);
                    };
                    break;
                case HTTPStreamingState.PLAY:
                    if (this._notifyPlayStartPending){
                        this._notifyPlayStartPending = false;
                        this.notifyPlayStart();
                    };
                    if (this._qualityLevelNeedsChanging){
                        this.changeQualityLevelTo(this._desiredQualityStreamName);
                    };
                    if (this._audioStreamNeedsChanging){
                        this.changeAudioStreamTo(this._desiredAudioStreamName);
                    };
                    _local2 = 0;
                    _local3 = true;
                    while (_local3) {
                        _local9 = this._source.getBytes();
                        this.issueLivenessEventsIfNeeded();
                        if (_local9 != null){
                            _local2 = (_local2 + this.processAndAppend(_local9));
                        };
                        if (((((!((this._state == HTTPStreamingState.PLAY))) || ((_local9 == null)))) || ((_local2 >= OSMFSettings.hdsBytesProcessingLimit)))){
                            _local3 = false;
                        };
                    };
                    if (this._state == HTTPStreamingState.PLAY){
                        if (_local2 > 0){
                            if (this._waitForDRM){
                                this.setState(HTTPStreamingState.WAIT);
                            } else {
                                if (this.checkIfExtraBufferingNeeded()){
                                } else {
                                    if (this.bufferLength > this._desiredBufferTime_Max){
                                        this.setState(HTTPStreamingState.WAIT);
                                    };
                                };
                            };
                        } else {
                            if (this._source.endOfStream){
                                super.bufferTime = 0.1;
                                this.setState(HTTPStreamingState.STOP);
                            };
                        };
                    };
                    break;
                case HTTPStreamingState.STOP:
                    appendBytesAction(NetStreamAppendBytesAction.END_SEQUENCE);
                    appendBytesAction(NetStreamAppendBytesAction.RESET_SEEK);
                    _local4 = new Object();
                    _local4.code = NetStreamCodes.NETSTREAM_PLAY_COMPLETE;
                    _local4.level = "status";
                    _local5 = new FLVTagScriptDataObject();
                    _local5.objects = ["onPlayStatus", _local4];
                    _local6 = new ByteArray();
                    _local5.write(_local6);
                    this.attemptAppendBytes(_local6);
                    appendBytesAction(NetStreamAppendBytesAction.END_SEQUENCE);
                    this.setState(HTTPStreamingState.HALT);
                    break;
                case HTTPStreamingState.HALT:
                    break;
            };
        }
        private function checkIfExtraBufferingNeeded():Boolean{
            if (((((!(this._wasBufferEmptied)) || (!(this._isPlaying)))) || (this._isPaused))){
                return (false);
            };
            if (this.bufferLength > (this._desiredBufferTime_Max + 30)){
                return (false);
            };
            return (true);
        }
        private function issueLivenessEventsIfNeeded():void{
            if (((this._source.isLiveStalled) && (this._wasBufferEmptied))){
                if (!this._wasSourceLiveStalled){
                    this._wasSourceLiveStalled = true;
                    this._liveStallStartTime = new Date();
                    this._issuedLiveStallNetStatus = false;
                };
                if (this.shouldIssueLiveStallNetStatus()){
                    dispatchEvent(new NetStatusEvent(NetStatusEvent.NET_STATUS, false, false, {
                        code:NetStreamCodes.NETSTREAM_PLAY_LIVE_STALL,
                        level:"status"
                    }));
                    this._issuedLiveStallNetStatus = true;
                };
            } else {
                if (((this._wasSourceLiveStalled) && (this._issuedLiveStallNetStatus))){
                    dispatchEvent(new NetStatusEvent(NetStatusEvent.NET_STATUS, false, false, {
                        code:NetStreamCodes.NETSTREAM_PLAY_LIVE_RESUME,
                        level:"status"
                    }));
                };
                this._wasSourceLiveStalled = false;
            };
        }
        private function shouldIssueLiveStallNetStatus():Boolean{
            if (this._issuedLiveStallNetStatus){
                return (false);
            };
            if (!this._wasBufferEmptied){
                return (false);
            };
            var _local1:Number = (((this.bufferLength + Math.max(OSMFSettings.hdsLiveStallTolerance, 0)) + 1) * 1000);
            var _local2:Date = new Date();
            if (_local2.valueOf() < (this._liveStallStartTime.valueOf() + _local1)){
                return (false);
            };
            return (true);
        }
        private function onDVRStreamInfo(_arg1:DVRStreamInfoEvent):void{
            this._dvrInfo = (_arg1.info as DVRInfo);
            this._initialTime = this._dvrInfo.startTime;
        }
        private function onBeginFragment(_arg1:HTTPStreamingEvent):void{
            if ((((((((this._initialTime < 0)) || ((this._seekTime < 0)))) || (this._insertScriptDataTags))) || ((this._playForDuration >= 0)))){
                if (this._flvParser == null){
                    if ((((this._enhancedSeekTarget >= 0)) || ((this._playForDuration >= 0)))){
                        this._flvParserIsSegmentStart = true;
                    };
                    this._flvParser = new FLVParser(false);
                };
                this._flvParserDone = false;
            };
        }
        private function onEndFragment(_arg1:HTTPStreamingEvent):void{
            if (this._videoHandler == null){
                return;
            };
            var _local2:Date = new Date();
            var _local3:Number = _local2.getTime();
            var _local4:HTTPStreamHandlerQoSInfo = this._videoHandler.qosInfo;
            var _local5:Vector.<QualityLevel>;
            var _local6:uint;
            var _local7:FragmentDetails;
            if (_local4 != null){
                _local5 = _local4.availableQualityLevels;
                _local6 = _local4.actualIndex;
                _local7 = _local4.lastFragmentDetails;
            };
            var _local8:Vector.<PlaybackDetails>;
            var _local9 = -1;
            if (this.playbackDetailsRecorder != null){
                _local8 = this.playbackDetailsRecorder.computeAndGetRecord();
                _local9 = this.playbackDetailsRecorder.playingIndex;
            };
            var _local10:QoSInfo = new QoSInfo(_local3, this.time, _local5, _local9, _local6, _local7, this.maxFPS, _local8, info, bufferLength, bufferTime, this.emptyBufferInterruptionSinceLastQoSUpdate);
            dispatchEvent(new QoSInfoEvent(QoSInfoEvent.QOS_UPDATE, false, false, _local10));
            this.emptyBufferInterruptionSinceLastQoSUpdate = false;
            dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.RUN_ALGORITHM));
        }
        private function onTransition(_arg1:HTTPStreamingEvent):void{
            if ((this._resource is DynamicStreamingResource)){
                this.lastTransitionIndex = (this._resource as DynamicStreamingResource).indexFromName(_arg1.url);
                this.lastTransitionStreamURL = _arg1.url;
            };
            dispatchEvent(new NetStatusEvent(NetStatusEvent.NET_STATUS, false, false, {
                code:NetStreamCodes.NETSTREAM_PLAY_TRANSITION,
                level:"status",
                details:_arg1.url
            }));
        }
        private function onTransitionComplete(_arg1:HTTPStreamingEvent):void{
            this.onActionNeeded(_arg1);
            var _local2:Object = new Object();
            _local2.code = NetStreamCodes.NETSTREAM_PLAY_TRANSITION_COMPLETE;
            _local2.level = "status";
            _local2.details = _arg1.url;
            var _local3:FLVTagScriptDataObject = new FLVTagScriptDataObject();
            _local3.objects = ["onPlayStatus", _local2];
            this.insertScriptDataTag(_local3);
        }
        private function onDownloadError(_arg1:HTTPStreamingEvent):void{
            dispatchEvent(new NetStatusEvent(NetStatusEvent.NET_STATUS, false, false, {
                code:NetStreamCodes.NETSTREAM_PLAY_STREAMNOTFOUND,
                level:"error",
                details:_arg1.url
            }));
        }
        private function onDownloadComplete(_arg1:HTTPStreamingEvent):void{
            this._bytesLoaded = (this._bytesLoaded + _arg1.bytesDownloaded);
        }
        private function notifyPlayStart():void{
            dispatchEvent(new NetStatusEvent(NetStatusEvent.NET_STATUS, false, false, {
                code:NetStreamCodes.NETSTREAM_PLAY_START,
                level:"status"
            }));
        }
        private function notifyPlayStop():void{
            dispatchEvent(new NetStatusEvent(NetStatusEvent.NET_STATUS, false, false, {
                code:NetStreamCodes.NETSTREAM_PLAY_STOP,
                level:"status"
            }));
        }
        private function notifyPlayUnpublish():void{
            dispatchEvent(new NetStatusEvent(NetStatusEvent.NET_STATUS, false, false, {
                code:NetStreamCodes.NETSTREAM_PLAY_UNPUBLISH_NOTIFY,
                level:"status"
            }));
        }
        private function insertScriptDataTag(_arg1:FLVTagScriptDataObject, _arg2:Boolean=false):void{
            if (!this._insertScriptDataTags){
                this._insertScriptDataTags = new Vector.<FLVTagScriptDataObject>();
            };
            if (_arg2){
                this._insertScriptDataTags.unshift(_arg1);
            } else {
                this._insertScriptDataTags.push(_arg1);
            };
        }
        private function consumeAllScriptDataTags(_arg1:Number):int{
            var _local2:int;
            var _local3:int;
            var _local4:ByteArray;
            var _local5:FLVTagScriptDataObject;
            _local3 = 0;
            while (_local3 < this._insertScriptDataTags.length) {
                _local4 = new ByteArray();
                _local5 = this._insertScriptDataTags[_local3];
                if (_local5 != null){
                    _local5.timestamp = _arg1;
                    _local5.write(_local4);
                    this.attemptAppendBytes(_local4);
                    _local2 = (_local2 + _local4.length);
                };
                _local3++;
            };
            this._insertScriptDataTags.length = 0;
            this._insertScriptDataTags = null;
            return (_local2);
        }
        private function processAndAppend(_arg1:ByteArray):uint{
            var _local2:ByteArray;
            if (((!(_arg1)) || ((_arg1.length == 0)))){
                return (0);
            };
            var _local3:uint;
            if (this._flvParser == null){
                _local2 = _arg1;
            } else {
                this._flvParserProcessed = 0;
                _arg1.position = 0;
                this._flvParser.parse(_arg1, true, this.onTag);
                _local3 = (_local3 + this._flvParserProcessed);
                if (!this._flvParserDone){
                    return (_local3);
                };
                _local2 = new ByteArray();
                this._flvParser.flush(_local2);
                this._flvParser = null;
            };
            _local3 = (_local3 + _local2.length);
            if (this._state != HTTPStreamingState.STOP){
                this.attemptAppendBytes(_local2);
            };
            return (_local3);
        }
        private function doConsumeAllScriptDataTags(_arg1:uint):void{
            if (this._insertScriptDataTags != null){
                this._flvParserProcessed = (this._flvParserProcessed + this.consumeAllScriptDataTags(_arg1));
            };
        }
        private function onTag(_arg1:FLVTag):Boolean{
            var _local2:int;
            var _local5:FLVTagVideo;
            var _local6:int;
            var _local7:Boolean;
            var _local8:FLVTag;
            var _local9:FLVTagVideo;
            var _local10:int;
            var _local11:int;
            var _local12:FLVTagVideo;
            var _local3:Number = ((_arg1.timestamp / 1000) + this._fileTimeAdjustment);
            while (_local3 < this._initialTime) {
                _local3 = (_local3 + 4294967.296);
            };
            if (this._playForDuration >= 0){
                if (this._initialTime >= 0){
                    if (_local3 > (this._initialTime + this._playForDuration)){
                        this.setState(HTTPStreamingState.STOP);
                        this._flvParserDone = true;
                        if (this._seekTime < 0){
                            this._seekTime = (this._playForDuration + this._initialTime);
                        };
                        return (false);
                    };
                };
            };
            if (this._enhancedSeekTarget < 0){
                if (this._initialTime < 0){
                    this._initialTime = ((this._dvrInfo)!=null) ? this._dvrInfo.startTime : _local3;
                };
                if (this._seekTime < 0){
                    this._seekTime = _local3;
                };
            } else {
                if (_local3 < this._enhancedSeekTarget){
                    if (this._enhancedSeekTags == null){
                        this._enhancedSeekTags = new Vector.<FLVTag>();
                    };
                    if ((_arg1 is FLVTagVideo)){
                        if (this._flvParserIsSegmentStart){
                            _local5 = new FLVTagVideo();
                            _local5.timestamp = _arg1.timestamp;
                            _local5.codecID = FLVTagVideo(_arg1).codecID;
                            _local5.frameType = FLVTagVideo.FRAME_TYPE_INFO;
                            _local5.infoPacketValue = FLVTagVideo.INFO_PACKET_SEEK_START;
                            this._enhancedSeekTags.push(_local5);
                            this._flvParserIsSegmentStart = false;
                        };
                        this._enhancedSeekTags.push(_arg1);
                    } else {
                        if ((((_arg1 is FLVTagScriptDataObject)) || ((((_arg1 is FLVTagAudio)) && (FLVTagAudio(_arg1).isCodecConfiguration))))){
                            this._enhancedSeekTags.push(_arg1);
                        };
                    };
                } else {
                    this.doConsumeAllScriptDataTags(_arg1.timestamp);
                    this._enhancedSeekTarget = -1;
                    if (this._seekTime < 0){
                        this._seekTime = _local3;
                    };
                    if (this._initialTime < 0){
                        this._initialTime = _local3;
                    };
                    if (((!((this._enhancedSeekTags == null))) && ((this._enhancedSeekTags.length > 0)))){
                        _local7 = false;
                        _local2 = 0;
                        while (_local2 < this._enhancedSeekTags.length) {
                            _local8 = this._enhancedSeekTags[_local2];
                            if (_local8.tagType == FLVTag.TAG_TYPE_VIDEO){
                                _local9 = (_local8 as FLVTagVideo);
                                if ((((_local9.codecID == FLVTagVideo.CODEC_ID_AVC)) && ((_local9.avcPacketType == FLVTagVideo.AVC_PACKET_TYPE_NALU)))){
                                    _local10 = (_arg1.timestamp - _local9.timestamp);
                                    _local11 = _local9.avcCompositionTimeOffset;
                                    _local11 = (_local11 - _local10);
                                    _local9.avcCompositionTimeOffset = _local11;
                                };
                                _local6 = _local9.codecID;
                                _local7 = true;
                            };
                            _local8.timestamp = _arg1.timestamp;
                            var _local4:ByteArray = new ByteArray();
                            _local8.write(_local4);
                            this._flvParserProcessed = (this._flvParserProcessed + _local4.length);
                            this.attemptAppendBytes(_local4);
                            _local2++;
                        };
                        if (_local7){
                            _local12 = new FLVTagVideo();
                            _local12.timestamp = _arg1.timestamp;
                            _local12.codecID = _local6;
                            _local12.frameType = FLVTagVideo.FRAME_TYPE_INFO;
                            _local12.infoPacketValue = FLVTagVideo.INFO_PACKET_SEEK_END;
                            _local4 = new ByteArray();
                            _local12.write(_local4);
                            this._flvParserProcessed = (this._flvParserProcessed + _local4.length);
                            this.attemptAppendBytes(_local4);
                        };
                        this._enhancedSeekTags = null;
                    };
                    _local4 = new ByteArray();
                    _arg1.write(_local4);
                    this._flvParserProcessed = (this._flvParserProcessed + _local4.length);
                    this.attemptAppendBytes(_local4);
                    if (this._playForDuration >= 0){
                        return (true);
                    };
                    this._flvParserDone = true;
                    return (false);
                };
                return (true);
            };
            this.doConsumeAllScriptDataTags(_arg1.timestamp);
            _local4 = new ByteArray();
            _arg1.write(_local4);
            this.attemptAppendBytes(_local4);
            this._flvParserProcessed = (this._flvParserProcessed + _local4.length);
            if (this._playForDuration >= 0){
                if ((((this._source.fragmentDuration >= 0)) && (this._flvParserIsSegmentStart))){
                    this._flvParserIsSegmentStart = false;
                    _local3 = ((_arg1.timestamp / 1000) + this._fileTimeAdjustment);
                    if ((_local3 + this._source.fragmentDuration) >= (this._initialTime + this._playForDuration)){
                        return (true);
                    };
                    this._flvParserDone = true;
                    return (false);
                };
                return (true);
            };
            this._flvParserDone = true;
            return (false);
        }
        private function onScriptData(_arg1:HTTPStreamingEvent):void{
            var _local2:*;
            var _local3:*;
            if ((((_arg1.scriptDataMode == null)) || ((_arg1.scriptDataObject == null)))){
                return;
            };
            switch (_arg1.scriptDataMode){
                case FLVTagScriptDataMode.NORMAL:
                    this.insertScriptDataTag(_arg1.scriptDataObject, false);
                    break;
                case FLVTagScriptDataMode.FIRST:
                    this.insertScriptDataTag(_arg1.scriptDataObject, true);
                    break;
                case FLVTagScriptDataMode.IMMEDIATE:
                    if (client){
                        _local2 = _arg1.scriptDataObject.objects[0];
                        _local3 = _arg1.scriptDataObject.objects[1];
                        if (client.hasOwnProperty(_local2)){
                            var _local4 = client;
                            _local4[_local2](_local3);
                        };
                    };
                    break;
            };
        }
        private function onActionNeeded(_arg1:HTTPStreamingEvent):void{
            var _local2:FLVHeader;
            var _local3:ByteArray;
            if (this._mixer != null){
                appendBytesAction(NetStreamAppendBytesAction.RESET_BEGIN);
                _local2 = new FLVHeader();
                _local3 = new ByteArray();
                _local2.write(_local3);
                this.attemptAppendBytes(_local3);
            };
        }
        private function attemptAppendBytes(_arg1:ByteArray):void{
            appendBytes(_arg1);
        }
        protected function createSource(_arg1:URLResource):void{
            var _local4:HTTPStreamSource;
            var _local2:IHTTPStreamSource;
            var _local3:StreamingURLResource = (_arg1 as StreamingURLResource);
            if ((((((_local3 == null)) || ((_local3.alternativeAudioStreamItems == null)))) || ((_local3.alternativeAudioStreamItems.length == 0)))){
                _local4 = new HTTPStreamSource(this._factory, this._resource, this);
                this._source = _local4;
                this._videoHandler = _local4;
            } else {
                this._mixer = new HTTPStreamMixer(this);
                this._mixer.video = new HTTPStreamSource(this._factory, this._resource, this._mixer);
                this._source = this._mixer;
                this._videoHandler = this._mixer.video;
            };
        }
        private function onDRMError(_arg1:DRMErrorEvent):void{
            this._waitForDRM = true;
            this.setState(HTTPStreamingState.WAIT);
        }
        private function onDRMStatus(_arg1:DRMStatusEvent):void{
            if (_arg1.voucher != null){
                this._waitForDRM = false;
            };
        }

    }
}//package org.osmf.net.httpstreaming 
