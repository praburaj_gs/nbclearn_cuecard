﻿package org.osmf.net.httpstreaming {
    import flash.utils.*;

    public interface IHTTPStreamSource {

        function get isReady():Boolean;
        function get endOfStream():Boolean;
        function get isLiveStalled():Boolean;
        function get hasErrors():Boolean;
        function seek(_arg1:Number):void;
        function getBytes():ByteArray;
        function get fragmentDuration():Number;
        function get isBestEffortFetchEnabled():Boolean;

    }
}//package org.osmf.net.httpstreaming 
