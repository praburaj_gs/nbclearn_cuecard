﻿package org.osmf.net.httpstreaming {
    import flash.events.*;
    import org.osmf.logging.*;
    import flash.net.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.net.httpstreaming.flv.*;
    import org.osmf.utils.*;

    public class HTTPStreamDownloader {

        private static const logger:Logger = Log.getLogger("org.osmf.net.httpstreaming.HTTPStreamDownloader");

        private var _isOpen:Boolean = false;
        private var _isComplete:Boolean = false;
        private var _hasData:Boolean = false;
        private var _hasErrors:Boolean = false;
        private var _savedBytes:ByteArray = null;
        private var _urlStream:URLStream = null;
        private var _request:URLRequest = null;
        private var _dispatcher:IEventDispatcher = null;
        private var _downloadBeginDate:Date = null;
        private var _downloadEndDate:Date = null;
        private var _downloadDuration:Number = 0;
        private var _downloadBytesCount:Number = 0;
        private var _timeoutTimer:Timer = null;
        private var _timeoutInterval:Number = 1000;
        private var _currentRetry:Number = 0;

        public function get isOpen():Boolean{
            return (this._isOpen);
        }
        public function get isComplete():Boolean{
            return (this._isComplete);
        }
        public function get hasData():Boolean{
            return (this._hasData);
        }
        public function get hasErrors():Boolean{
            return (this._hasErrors);
        }
        public function get downloadDuration():Number{
            return (this._downloadDuration);
        }
        public function get downloadBytesCount():Number{
            return (this._downloadBytesCount);
        }
        public function open(_arg1:URLRequest, _arg2:IEventDispatcher, _arg3:Number):void{
            if (((this.isOpen) || (((!((this._urlStream == null))) && (this._urlStream.connected))))){
                this.close();
            };
            if (_arg1 == null){
                throw (new ArgumentError("Null request in HTTPStreamDownloader open method."));
            };
            this._isComplete = false;
            this._hasData = false;
            this._hasErrors = false;
            this._dispatcher = _arg2;
            if (this._savedBytes == null){
                this._savedBytes = new ByteArray();
            };
            if (this._urlStream == null){
                this._urlStream = new URLStream();
                this._urlStream.addEventListener(Event.OPEN, this.onOpen);
                this._urlStream.addEventListener(Event.COMPLETE, this.onComplete);
                this._urlStream.addEventListener(ProgressEvent.PROGRESS, this.onProgress);
                this._urlStream.addEventListener(IOErrorEvent.IO_ERROR, this.onError);
                this._urlStream.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onError);
            };
            if ((((this._timeoutTimer == null)) && (!((_arg3 == -1))))){
                this._timeoutTimer = new Timer(_arg3, 1);
                this._timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, this.onTimeout);
            };
            if (this._urlStream != null){
                this._timeoutInterval = _arg3;
                this._request = _arg1;
                logger.debug(((((("Loading (timeout=" + this._timeoutInterval) + ", retry=") + this._currentRetry) + "):") + this._request.url.toString()));
                this._downloadBeginDate = null;
                this._downloadBytesCount = 0;
                this.startTimeoutMonitor(this._timeoutInterval);
                this._urlStream.load(this._request);
            };
        }
        public function close(_arg1:Boolean=false):void{
            if (this._request != null){
                logger.debug(("Closing :" + this._request.url.toString()));
            };
            this.stopTimeoutMonitor();
            this._isOpen = false;
            this._isComplete = false;
            this._hasData = false;
            this._hasErrors = false;
            this._request = null;
            if (this._timeoutTimer != null){
                this._timeoutTimer.stop();
                if (_arg1){
                    this._timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, this.onTimeout);
                    this._timeoutTimer = null;
                };
            };
            if (this._urlStream != null){
                if (this._urlStream.connected){
                    this._urlStream.close();
                };
                if (_arg1){
                    this._urlStream.removeEventListener(Event.OPEN, this.onOpen);
                    this._urlStream.removeEventListener(Event.COMPLETE, this.onComplete);
                    this._urlStream.removeEventListener(ProgressEvent.PROGRESS, this.onProgress);
                    this._urlStream.removeEventListener(IOErrorEvent.IO_ERROR, this.onError);
                    this._urlStream.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onError);
                    this._urlStream = null;
                };
            };
            if (this._savedBytes != null){
                this._savedBytes.length = 0;
                if (_arg1){
                    this._savedBytes = null;
                };
            };
        }
        public function get totalAvailableBytes():int{
            if (!this.isOpen){
                return (0);
            };
            return ((this._savedBytes.bytesAvailable + this._urlStream.bytesAvailable));
        }
        public function getBytes(_arg1:int=0):IDataInput{
            var _local3:int;
            if (((!(this.isOpen)) || ((_arg1 < 0)))){
                return (null);
            };
            if (_arg1 == 0){
                _arg1 = 1;
            };
            var _local2:int = this.totalAvailableBytes;
            if (_local2 == 0){
                this._hasData = false;
            };
            if (_local2 < _arg1){
                return (null);
            };
            if (this._savedBytes.bytesAvailable){
                _local3 = (_arg1 - this._savedBytes.bytesAvailable);
                if (_local3 > 0){
                    this._urlStream.readBytes(this._savedBytes, this._savedBytes.length, _local3);
                };
                return (this._savedBytes);
            };
            this._savedBytes.length = 0;
            return (this._urlStream);
        }
        public function clearSavedBytes():void{
            if (this._savedBytes == null){
                return;
            };
            this._savedBytes.length = 0;
            this._savedBytes.position = 0;
        }
        public function appendToSavedBytes(_arg1:IDataInput, _arg2:uint):void{
            if (this._savedBytes == null){
                return;
            };
            _arg1.readBytes(this._savedBytes, this._savedBytes.length, _arg2);
        }
        public function saveRemainingBytes():void{
            if (this._savedBytes == null){
                return;
            };
            if (((((!((this._urlStream == null))) && (this._urlStream.connected))) && (this._urlStream.bytesAvailable))){
                this._urlStream.readBytes(this._savedBytes, this._savedBytes.length);
            };
        }
        public function toString():String{
            return ("HTTPStreamSource");
        }
        private function onOpen(_arg1:Event):void{
            this._isOpen = true;
        }
        private function onComplete(_arg1:Event):void{
            var _local2:HTTPStreamingEvent;
            if (this._downloadBeginDate == null){
                this._downloadBeginDate = new Date();
            };
            this._downloadEndDate = new Date();
            this._downloadDuration = ((this._downloadEndDate.valueOf() - this._downloadBeginDate.valueOf()) / 1000);
            this._isComplete = true;
            this._hasErrors = false;
            logger.debug((((((("Loading complete. It took " + this._downloadDuration) + " sec and ") + this._currentRetry) + " retries to download ") + this._downloadBytesCount) + " bytes."));
            if (this._dispatcher != null){
                _local2 = new HTTPStreamingEvent(HTTPStreamingEvent.DOWNLOAD_COMPLETE, false, false, 0, null, FLVTagScriptDataMode.NORMAL, this._request.url, this._downloadBytesCount, HTTPStreamingEventReason.NORMAL, this);
                this._dispatcher.dispatchEvent(_local2);
            };
        }
        private function onProgress(_arg1:ProgressEvent):void{
            var _local2:HTTPStreamingEvent;
            if (this._downloadBeginDate == null){
                this._downloadBeginDate = new Date();
            };
            if (this._downloadBytesCount == 0){
                if (this._timeoutTimer != null){
                    this.stopTimeoutMonitor();
                };
                this._currentRetry = 0;
                this._downloadBytesCount = _arg1.bytesTotal;
                logger.debug((((("Loaded " + _arg1.bytesLoaded) + " bytes from ") + this._downloadBytesCount) + " bytes."));
            };
            this._hasData = true;
            if (this._dispatcher != null){
                _local2 = new HTTPStreamingEvent(HTTPStreamingEvent.DOWNLOAD_PROGRESS, false, false, 0, null, FLVTagScriptDataMode.NORMAL, this._request.url, 0, HTTPStreamingEventReason.NORMAL, this);
                this._dispatcher.dispatchEvent(_local2);
            };
        }
        private function onError(_arg1:Event):void{
            var _local2:String;
            var _local3:HTTPStreamingEvent;
            if (this._timeoutTimer != null){
                this.stopTimeoutMonitor();
            };
            if (this._downloadBeginDate == null){
                this._downloadBeginDate = new Date();
            };
            this._downloadEndDate = new Date();
            this._downloadDuration = ((this._downloadEndDate.valueOf() - this._downloadBeginDate.valueOf()) / 1000);
            this._isComplete = false;
            this._hasErrors = true;
            logger.error((((((("Loading failed. It took " + this._downloadDuration) + " sec and ") + this._currentRetry) + " retries to fail while downloading [") + this._request.url) + "]."));
            logger.error(("URLStream error event: " + _arg1));
            if (this._dispatcher != null){
                _local2 = HTTPStreamingEventReason.NORMAL;
                if (_arg1.type == Event.CANCEL){
                    _local2 = HTTPStreamingEventReason.TIMEOUT;
                };
                _local3 = new HTTPStreamingEvent(HTTPStreamingEvent.DOWNLOAD_ERROR, false, false, 0, null, FLVTagScriptDataMode.NORMAL, this._request.url, 0, _local2, this);
                this._dispatcher.dispatchEvent(_local3);
            };
        }
        private function startTimeoutMonitor(_arg1:Number):void{
            if (this._timeoutTimer != null){
                if (_arg1 > 0){
                    this._timeoutTimer.delay = _arg1;
                };
                this._timeoutTimer.reset();
                this._timeoutTimer.start();
            };
        }
        private function stopTimeoutMonitor():void{
            if (this._timeoutTimer != null){
                this._timeoutTimer.stop();
            };
        }
        private function onTimeout(_arg1:TimerEvent):void{
            logger.error((("Timeout while trying to download [" + this._request.url) + "]"));
            logger.error("Canceling and retrying the download.");
            if (OSMFSettings.hdsMaximumRetries > -1){
                this._currentRetry++;
            };
            if ((((OSMFSettings.hdsMaximumRetries == -1)) || (((!((OSMFSettings.hdsMaximumRetries == -1))) && ((this._currentRetry < OSMFSettings.hdsMaximumRetries)))))){
                this.open(this._request, this._dispatcher, (this._timeoutInterval + OSMFSettings.hdsTimeoutAdjustmentOnRetry));
            } else {
                this.close();
                this.onError(new Event(Event.CANCEL));
            };
        }

    }
}//package org.osmf.net.httpstreaming 
