﻿package org.osmf.net.httpstreaming {
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.metadata.*;
    import org.osmf.net.*;
    import flash.net.*;
    import org.osmf.net.qos.*;
    import org.osmf.events.*;
    import org.osmf.net.metrics.*;
    import org.osmf.net.rules.*;
    import org.osmf.traits.*;
    import org.osmf.net.httpstreaming.dvr.*;
    import org.osmf.net.httpstreaming.f4f.*;

    public class HTTPStreamingNetLoader extends NetLoader {

        protected static const BANDWIDTH_BUFFER_RULE_WEIGHTS:Vector.<Number> = new <Number>[7, 3];
;
        protected static const BANDWIDTH_BUFFER_RULE_BUFFER_FRAGMENTS_THRESHOLD:uint = 2;
        protected static const AFTER_UP_SWITCH_BANDWIDTH_BUFFER_RULE_BUFFER_FRAGMENTS_THRESHOLD:uint = 2;
        protected static const AFTER_UP_SWITCH_BANDWIDTH_BUFFER_RULE_MIN_RATIO:Number = 0.5;
        protected static const EMPTY_BUFFER_RULE_SCALE_DOWN_FACTOR:Number = 0.4;
        private static const QOS_MAX_HISTORY_LENGTH:Number = 10;

        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            return (!(((_arg1.getMetadataValue(MetadataNamespaces.HTTP_STREAMING_METADATA) as Metadata) == null)));
        }
        override protected function createNetStream(_arg1:NetConnection, _arg2:URLResource):NetStream{
            var _local3:HTTPStreamingFactory = this.createHTTPStreamingFactory();
            var _local4:HTTPNetStream = new HTTPNetStream(_arg1, _local3, _arg2);
            return (_local4);
        }
        override protected function createNetStreamSwitchManager(_arg1:NetConnection, _arg2:NetStream, _arg3:DynamicStreamingResource):NetStreamSwitchManagerBase{
            var _local4:QoSInfoHistory = this.createNetStreamQoSInfoHistory(_arg2);
            var _local5:MetricFactory = this.createMetricFactory(_local4);
            var _local6:MetricRepository = new MetricRepository(_local5);
            var _local7:Vector.<RuleBase> = new Vector.<RuleBase>();
            var _local8:Vector.<Number> = new Vector.<Number>();
            _local7.push(new BufferBandwidthRule(_local6, BANDWIDTH_BUFFER_RULE_WEIGHTS, BANDWIDTH_BUFFER_RULE_BUFFER_FRAGMENTS_THRESHOLD));
            _local8.push(1);
            var _local9:Vector.<RuleBase> = new Vector.<RuleBase>();
            _local9.push(new DroppedFPSRule(_local6, 10, 0.1));
            _local9.push(new EmptyBufferRule(_local6, EMPTY_BUFFER_RULE_SCALE_DOWN_FACTOR));
            _local9.push(new AfterUpSwitchBufferBandwidthRule(_local6, AFTER_UP_SWITCH_BANDWIDTH_BUFFER_RULE_BUFFER_FRAGMENTS_THRESHOLD, AFTER_UP_SWITCH_BANDWIDTH_BUFFER_RULE_MIN_RATIO));
            var _local10:NetStreamSwitcher = new NetStreamSwitcher(_arg2, _arg3);
            return (new DefaultHTTPStreamingSwitchManager(_arg2, _local10, _local6, _local9, true, _local7, _local8));
        }
        override protected function processFinishLoading(_arg1:NetStreamLoadTrait):void{
            var netStream:* = null;
            var onDVRStreamInfo:* = null;
            var loadTrait:* = _arg1;
            onDVRStreamInfo = function (_arg1:DVRStreamInfoEvent):void{
                netStream.removeEventListener(DVRStreamInfoEvent.DVRSTREAMINFO, onDVRStreamInfo);
                loadTrait.setTrait(new HTTPStreamingDVRCastDVRTrait(loadTrait.connection, netStream, (_arg1.info as DVRInfo)));
                loadTrait.setTrait(new HTTPStreamingDVRCastTimeTrait(loadTrait.connection, netStream, (_arg1.info as DVRInfo)));
                updateLoadTrait(loadTrait, LoadState.READY);
            };
            var resource:* = (loadTrait.resource as URLResource);
            if (!this.dvrMetadataPresent(resource)){
                updateLoadTrait(loadTrait, LoadState.READY);
                return;
            };
            netStream = (loadTrait.netStream as HTTPNetStream);
            netStream.addEventListener(DVRStreamInfoEvent.DVRSTREAMINFO, onDVRStreamInfo);
            netStream.DVRGetStreamInfo(null);
        }
        protected function createNetStreamQoSInfoHistory(_arg1:NetStream):QoSInfoHistory{
            return (new QoSInfoHistory(_arg1, QOS_MAX_HISTORY_LENGTH));
        }
        protected function createMetricFactory(_arg1:QoSInfoHistory):MetricFactory{
            return (new DefaultMetricFactory(_arg1));
        }
        protected function createHTTPStreamingFactory():HTTPStreamingFactory{
            return (new HTTPStreamingF4FFactory());
        }
        private function dvrMetadataPresent(_arg1:URLResource):Boolean{
            var _local2:Metadata = (_arg1.getMetadataValue(MetadataNamespaces.DVR_METADATA) as Metadata);
            return (!((_local2 == null)));
        }

    }
}//package org.osmf.net.httpstreaming 
