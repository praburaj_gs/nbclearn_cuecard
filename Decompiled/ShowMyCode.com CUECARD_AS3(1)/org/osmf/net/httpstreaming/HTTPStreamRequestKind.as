﻿package org.osmf.net.httpstreaming {

    public class HTTPStreamRequestKind {

        public static const RETRY:String = "retry";
        public static const DOWNLOAD:String = "download";
        public static const BEST_EFFORT_DOWNLOAD:String = "bestEffortDownload";
        public static const LIVE_STALL:String = "liveStall";
        public static const DONE:String = "done";

    }
}//package org.osmf.net.httpstreaming 
