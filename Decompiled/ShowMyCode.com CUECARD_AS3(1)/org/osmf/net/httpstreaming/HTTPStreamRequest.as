﻿package org.osmf.net.httpstreaming {
    import flash.events.*;
    import flash.net.*;

    public class HTTPStreamRequest {

        private var _kind:String = null;
        private var _retryAfter:int = -1;
        private var _bestEffortDownloaderMonitor:IEventDispatcher = null;
        private var _urlRequest:URLRequest = null;

        public function HTTPStreamRequest(_arg1:String, _arg2:String=null, _arg3:Number=-1, _arg4:IEventDispatcher=null){
            this._kind = _arg1;
            if (_arg2){
                this._urlRequest = new URLRequest(HTTPStreamingUtils.normalizeURL(_arg2));
            } else {
                this._urlRequest = null;
            };
            this._retryAfter = _arg3;
            this._bestEffortDownloaderMonitor = _arg4;
        }
        public function get kind():String{
            return (this._kind);
        }
        public function get retryAfter():int{
            return (this._retryAfter);
        }
        public function get bestEffortDownloaderMonitor():IEventDispatcher{
            return (this._bestEffortDownloaderMonitor);
        }
        public function get urlRequest():URLRequest{
            return (this._urlRequest);
        }
        public function get url():String{
            if (this._urlRequest == null){
                return (null);
            };
            return (this._urlRequest.url);
        }
        public function toString():String{
            var _local1:String = ("[HTTPStreamRequest kind=" + this.kind);
            switch (this.kind){
                case HTTPStreamRequestKind.BEST_EFFORT_DOWNLOAD:
                case HTTPStreamRequestKind.DOWNLOAD:
                    _local1 = (_local1 + (", url=" + this.url));
                    break;
                case HTTPStreamRequestKind.LIVE_STALL:
                case HTTPStreamRequestKind.RETRY:
                    _local1 = (_local1 + (", retryAfter=" + this.retryAfter));
                    break;
                case HTTPStreamRequestKind.DONE:
            };
            _local1 = (_local1 + "]");
            return (_local1);
        }

    }
}//package org.osmf.net.httpstreaming 
