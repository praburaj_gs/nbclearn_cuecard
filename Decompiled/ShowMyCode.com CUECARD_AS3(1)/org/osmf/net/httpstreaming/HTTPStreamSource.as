﻿package org.osmf.net.httpstreaming {
    import flash.events.*;
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.net.qos.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.net.httpstreaming.dvr.*;
    import org.osmf.utils.*;

    public class HTTPStreamSource implements IHTTPStreamSource, IHTTPStreamHandler {

        private static const logger:Logger = Log.getLogger("org.osmf.net.httpstreaming.HTTPStreamSource");

        private var _dispatcher:IEventDispatcher = null;
        private var _resource:MediaResourceBase = null;
        private var _qosInfo:HTTPStreamHandlerQoSInfo;
        private var _downloader:HTTPStreamDownloader = null;
        private var _request:HTTPStreamRequest = null;
        private var _indexHandler:HTTPStreamingIndexHandlerBase = null;
        private var _fileHandler:HTTPStreamingFileHandlerBase = null;
        private var _indexInfo:HTTPStreamingIndexInfoBase = null;
        private var _streamName:String = null;
        private var _seekTarget:Number = -1;
        private var _didBeginSeek:Boolean = false;
        private var _didCompleteSeek:Boolean = false;
        private var _streamNames:Array = null;
        private var _qualityRates:Array = null;
        private var _numQualityLevels:int = 0;
        private var _qualityLevel:int = 0;
        private var _qualityLevelChanged:Boolean = false;
        private var _desiredQualityLevel:int = -1;
        private var _desiredQualityStreamName:String = null;
        private var _qualityAndStreamNameInSync:Boolean = false;
        private var _fragmentDuration:Number = 0;
        private var _endFragment:Boolean = false;
        private var _indexDownloaderMonitor:EventDispatcher;
        private var _indexDownloader:HTTPStreamDownloader;
        private var _currentIndexDownloadEvent:HTTPStreamingIndexHandlerEvent = null;
        private var _pendingIndexDownloadRequests:Vector.<HTTPStreamingIndexHandlerEvent>;
        private var _pendingIndexDownloadRequestsLenght:int = 0;
        private var _hasErrors:Boolean = false;
        private var _isReady:Boolean = false;
        private var _ratesAreReady:Boolean = false;
        private var _endOfStream:Boolean = false;
        private var _isLive:Boolean = false;
        private var _offset:Number = -1;
        private var _dvrInfo:DVRInfo = null;
        private var _state:String = null;
        private var _retryAfterTime:Number = -1;
        private var _bestEffortDownloadResult:String = null;
        private var _isLiveStalled:Boolean = false;
        private var previouslyLoggedState:String = null;
        private var loggedStreamName:String = null;

        public function HTTPStreamSource(_arg1:HTTPStreamingFactory, _arg2:MediaResourceBase, _arg3:IEventDispatcher){
            this._indexDownloaderMonitor = new EventDispatcher();
            this._indexDownloader = new HTTPStreamDownloader();
            this._pendingIndexDownloadRequests = new Vector.<HTTPStreamingIndexHandlerEvent>();
            super();
            if (_arg3 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            if (_arg2 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            this._dispatcher = _arg3;
            this._resource = _arg2;
            this._fileHandler = _arg1.createFileHandler(_arg2);
            if (this._fileHandler == null){
                throw (new ArgumentError("Null file handler in HTTPStreamSourceHandler constructor. Probably invalid factory object or resource."));
            };
            this._indexHandler = _arg1.createIndexHandler(_arg2, this._fileHandler);
            if (this._indexHandler == null){
                throw (new ArgumentError("Null index handler in HTTPStreamSourceHandler constructor. Probably invalid factory object or resource."));
            };
            this._indexInfo = _arg1.createIndexInfo(_arg2);
            if (this._indexInfo == null){
                logger.warn("Null index info in HTTPStreamSourceHandler constructor. Probably invalid factory object or resource.");
            };
            this._fileHandler.addEventListener(HTTPStreamingEvent.FRAGMENT_DURATION, this.onFragmentDuration);
            this._fileHandler.addEventListener(HTTPStreamingEvent.SCRIPT_DATA, this.onScriptData);
            this._fileHandler.addEventListener(HTTPStreamingEvent.FILE_ERROR, this.onError);
            this._indexHandler.addEventListener(HTTPStreamingIndexHandlerEvent.INDEX_READY, this.onIndexReady);
            this._indexHandler.addEventListener(HTTPStreamingIndexHandlerEvent.RATES_READY, this.onRatesReady);
            this._indexHandler.addEventListener(HTTPStreamingIndexHandlerEvent.REQUEST_LOAD_INDEX, this.onRequestLoadIndex);
            this._indexHandler.addEventListener(DVRStreamInfoEvent.DVRSTREAMINFO, this.onDVRStreamInfo);
            this._indexHandler.addEventListener(HTTPStreamingEvent.FRAGMENT_DURATION, this.onFragmentDuration);
            this._indexHandler.addEventListener(HTTPStreamingEvent.SCRIPT_DATA, this.onScriptData);
            this._indexHandler.addEventListener(HTTPStreamingEvent.INDEX_ERROR, this.onError);
            this._indexHandler.addEventListener(HTTPStreamingEvent.DOWNLOAD_CONTINUE, this.onBestEffortDownloadEvent);
            this._indexHandler.addEventListener(HTTPStreamingEvent.DOWNLOAD_SKIP, this.onBestEffortDownloadEvent);
            this._indexHandler.addEventListener(HTTPStreamingEvent.DOWNLOAD_COMPLETE, this.onBestEffortDownloadEvent);
            this._indexHandler.addEventListener(HTTPStreamingEvent.DOWNLOAD_ERROR, this.onBestEffortDownloadEvent);
            this._indexDownloaderMonitor.addEventListener(HTTPStreamingEvent.DOWNLOAD_COMPLETE, this.onIndexComplete);
            this._indexDownloaderMonitor.addEventListener(HTTPStreamingEvent.DOWNLOAD_ERROR, this.onIndexError);
            this.setState(HTTPStreamingState.INIT);
            logger.debug("Provider initialized.");
        }
        public function get source():IHTTPStreamSource{
            return (this);
        }
        public function get isReady():Boolean{
            return (this._isReady);
        }
        public function get endOfStream():Boolean{
            return (this._endOfStream);
        }
        public function get hasErrors():Boolean{
            return (this._hasErrors);
        }
        public function get isLiveStalled():Boolean{
            return (this._isLiveStalled);
        }
        public function get streamName():String{
            return (this._streamName);
        }
        public function get qosInfo():HTTPStreamHandlerQoSInfo{
            return (this._qosInfo);
        }
        public function get isOpen():Boolean{
            return (!((this._streamName == null)));
        }
        public function open(_arg1:String):void{
            if (this._streamName != null){
                this.close();
            };
            this._streamName = _arg1;
            this._qualityAndStreamNameInSync = false;
            if (this._streamName == null){
                this.loggedStreamName = this._streamName;
            } else {
                this.loggedStreamName = this._streamName.substr(this._streamName.lastIndexOf("/"));
            };
            logger.debug((("Opening stream [ " + this.loggedStreamName) + " ]. "));
            this._indexHandler.initialize(((this._indexInfo)!=null) ? this._indexInfo : _arg1);
        }
        public function close():void{
            logger.debug((("Closing stream [ " + this.loggedStreamName) + " ]. "));
            if (this._downloader != null){
                this._downloader.close();
            };
            this._indexHandler.dispose();
            this._endFragment = true;
            this._endOfStream = true;
            this._streamName = null;
        }
        public function seek(_arg1:Number):void{
            this._endOfStream = false;
            this._hasErrors = false;
            this._isLiveStalled = false;
            this._seekTarget = _arg1;
            this._didBeginSeek = false;
            this._didCompleteSeek = false;
            if (this._seekTarget < 0){
                if (this._dvrInfo != null){
                    this._seekTarget = Math.floor(((this._dvrInfo.startTime + this._dvrInfo.curLength) - OSMFSettings.hdsDVRLiveOffset));
                } else {
                    if (this._isLive){
                        this._seekTarget = Math.floor(this._offset);
                    } else {
                        this._seekTarget = 0;
                    };
                };
            };
            logger.debug((((("Seeking to " + this._seekTarget) + " in stream [ ") + this.loggedStreamName) + " ]. "));
            this.setState(HTTPStreamingState.SEEK);
        }
        public function getBytes():ByteArray{
            return (this.doSomeProcessingAndGetBytes());
        }
        public function getDVRInfo(_arg1:Object):void{
            logger.debug("Loading dvr information.");
            this._indexHandler.dvrGetStreamInfo(((this._indexInfo)!=null) ? this._indexInfo : _arg1);
        }
        public function changeQualityLevel(_arg1:String):void{
            var _local3:int;
            logger.debug(("Prepare to switch the quality level to " + _arg1));
            var _local2 = -1;
            if (this._streamNames != null){
                _local3 = 0;
                while (_local3 < this._streamNames.length) {
                    if (_arg1 == this._streamNames[_local3]){
                        _local2 = _local3;
                        break;
                    };
                    _local3++;
                };
            };
            if (_local2 == -1){
                throw (new Error("Quality level cannot be set at this time."));
            };
            if (_local2 != this._desiredQualityLevel){
                this.beginQualityLevelChange(_local2);
            };
        }
        public function get isBestEffortFetchEnabled():Boolean{
            return (((!((this._indexHandler == null))) && (this._indexHandler.isBestEffortFetchEnabled)));
        }
        public function get fragmentDuration():Number{
            return (this._fragmentDuration);
        }
        protected function doSomeProcessingAndGetBytes():ByteArray{
            var _local3:Date;
            var _local4:Boolean;
            var _local5:Vector.<QualityLevel>;
            var _local6:String;
            var _local7:String;
            var _local8:FragmentDetails;
            var _local9:IEventDispatcher;
            var _local10:uint;
            var _local1:ByteArray;
            var _local2:IDataInput;
            switch (this._state){
                case HTTPStreamingState.INIT:
                    break;
                case HTTPStreamingState.SEEK:
                    if (this._downloader != null){
                        this._downloader.close();
                        this._fileHandler.flushFileSegment(this._downloader.getBytes());
                    };
                    this.setState(HTTPStreamingState.LOAD);
                    break;
                case HTTPStreamingState.WAIT:
                    _local3 = new Date();
                    if (_local3.getTime() > this._retryAfterTime){
                        this.setState(HTTPStreamingState.LOAD);
                    };
                    break;
                case HTTPStreamingState.LOAD:
                    if (this._qualityLevelChanged){
                        this.endQualityLevelChange();
                    };
                    this._fragmentDuration = -1;
                    this._endOfStream = false;
                    _local4 = false;
                    if (!this._didBeginSeek){
                        this._request = this._indexHandler.getFileForTime(this._seekTarget, this._qualityLevel);
                        _local4 = true;
                    } else {
                        this._request = this._indexHandler.getNextFile(this._qualityLevel);
                    };
                    this._isLiveStalled = (this._request.kind == HTTPStreamRequestKind.LIVE_STALL);
                    switch (this._request.kind){
                        case HTTPStreamRequestKind.DOWNLOAD:
                        case HTTPStreamRequestKind.BEST_EFFORT_DOWNLOAD:
                            if (_local4){
                                this._didBeginSeek = true;
                            };
                            if (this._downloader == null){
                                this._downloader = new HTTPStreamDownloader();
                            };
                            _local9 = this._dispatcher;
                            if (this._request.kind == HTTPStreamRequestKind.BEST_EFFORT_DOWNLOAD){
                                _local9 = this._request.bestEffortDownloaderMonitor;
                                this._bestEffortDownloadResult = null;
                            };
                            logger.debug(("downloader.open " + this._request.url));
                            this._downloader.open(this._request.urlRequest, _local9, OSMFSettings.hdsFragmentDownloadTimeout);
                            this.setState(HTTPStreamingState.BEGIN_FRAGMENT);
                            break;
                        case HTTPStreamRequestKind.RETRY:
                        case HTTPStreamRequestKind.LIVE_STALL:
                            _local3 = new Date();
                            this._retryAfterTime = (_local3.getTime() + (1000 * this._request.retryAfter));
                            this.setState(HTTPStreamingState.WAIT);
                            break;
                        case HTTPStreamRequestKind.DONE:
                            this._endFragment = true;
                            this._endOfStream = true;
                            if (this._downloader != null){
                                _local1 = this._fileHandler.flushFileSegment(this._downloader.getBytes());
                            };
                            this.setState(HTTPStreamingState.STOP);
                            break;
                    };
                    break;
                case HTTPStreamingState.BEGIN_FRAGMENT:
                    if (this._request.kind == HTTPStreamRequestKind.BEST_EFFORT_DOWNLOAD){
                        logger.debug(("_bestEffortDownloadResult = " + this._bestEffortDownloadResult));
                        if (this._bestEffortDownloadResult == null){
                            break;
                        };
                        if (this._bestEffortDownloadResult == HTTPStreamingEvent.DOWNLOAD_ERROR){
                            break;
                        };
                        if (this._bestEffortDownloadResult == HTTPStreamingEvent.DOWNLOAD_SKIP){
                            this.setState(HTTPStreamingState.LOAD);
                            break;
                        };
                        if (this._bestEffortDownloadResult == HTTPStreamingEvent.DOWNLOAD_CONTINUE){
                        } else {
                            break;
                        };
                        this._isLiveStalled = false;
                    };
                    this._endFragment = false;
                    this._hasErrors = false;
                    if (!this._didCompleteSeek){
                        this._fileHandler.beginProcessFile(true, this._seekTarget);
                        this._didCompleteSeek = true;
                    } else {
                        this._fileHandler.beginProcessFile(false, 0);
                    };
                    this._dispatcher.dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.BEGIN_FRAGMENT, false, true, NaN, null, null, this._streamName));
                    this.setState(HTTPStreamingState.READ);
                    break;
                case HTTPStreamingState.READ:
                    if (this._downloader != null){
                        _local2 = this._downloader.getBytes(this._fileHandler.inputBytesNeeded);
                        if (_local2 != null){
                            _local1 = this._fileHandler.processFileSegment(_local2);
                        } else {
                            this._endFragment = ((((((!((this._downloader == null))) && (this._downloader.isOpen))) && (this._downloader.isComplete))) && (!(this._downloader.hasData)));
                            this._hasErrors = ((!((this._downloader == null))) && (this._downloader.hasErrors));
                        };
                    };
                    if (this._state == HTTPStreamingState.READ){
                        if (this._endFragment){
                            if (this._downloader != null){
                                this._downloader.saveRemainingBytes();
                            };
                            this.setState(HTTPStreamingState.END_FRAGMENT);
                        };
                    };
                    break;
                case HTTPStreamingState.END_FRAGMENT:
                    if (this._downloader != null){
                        _local2 = this._downloader.getBytes();
                        if (_local2 != null){
                            _local1 = this._fileHandler.endProcessFile(_local2);
                        };
                    };
                    _local5 = new Vector.<QualityLevel>();
                    _local10 = 0;
                    while (_local10 < this._qualityRates.length) {
                        _local5.push(new QualityLevel(_local10, this._qualityRates[_local10], this._streamNames[_local10]));
                        _local10++;
                    };
                    _local6 = this._request.urlRequest.url;
                    _local7 = this._request.urlRequest.url.substr(_local6.lastIndexOf("Seg"));
                    _local8 = new FragmentDetails(this._downloader.downloadBytesCount, this._fragmentDuration, this._downloader.downloadDuration, this._qualityLevel, _local7);
                    this._qosInfo = new HTTPStreamHandlerQoSInfo(_local5, this._qualityLevel, _local8);
                    this._dispatcher.dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.END_FRAGMENT, false, true, NaN, null, null, this._streamName));
                    this.setState(HTTPStreamingState.LOAD);
                    break;
            };
            return (_local1);
        }
        protected function setState(_arg1:String):void{
            this._state = _arg1;
            if (this._state != this.previouslyLoggedState){
                logger.debug(("State = " + this._state));
                this.previouslyLoggedState = this._state;
            };
        }
        private function onDVRStreamInfo(_arg1:DVRStreamInfoEvent):void{
            this._dvrInfo = (_arg1.info as DVRInfo);
            this._dispatcher.dispatchEvent(_arg1);
        }
        private function onIndexReady(_arg1:HTTPStreamingIndexHandlerEvent):void{
            this._isReady = true;
            this._isLive = _arg1.live;
            this._offset = _arg1.offset;
            logger.debug((((((("Stream [ " + this.loggedStreamName) + " ] refreshed. ( offset = ") + this._offset) + ", live = ") + this._isLive) + ")."));
            if (!this._qualityAndStreamNameInSync){
                logger.debug((((("Stream name [ " + this.loggedStreamName) + " ] and quality level [") + this._qualityLevel) + "] are not in sync."));
                this._qualityAndStreamNameInSync = true;
                this.changeQualityLevel(this._streamName);
            };
        }
        private function onRatesReady(_arg1:HTTPStreamingIndexHandlerEvent):void{
            this._ratesAreReady = true;
            this._qualityRates = _arg1.rates;
            this._streamNames = _arg1.streamNames;
            this._numQualityLevels = this._qualityRates.length;
        }
        private function onRequestLoadIndex(_arg1:HTTPStreamingIndexHandlerEvent):void{
            this._pendingIndexDownloadRequests[this._pendingIndexDownloadRequestsLenght] = _arg1;
            this._pendingIndexDownloadRequestsLenght++;
            if (this._currentIndexDownloadEvent == null){
                this._currentIndexDownloadEvent = _arg1;
                this._indexDownloader.open(this._currentIndexDownloadEvent.request, this._indexDownloaderMonitor, OSMFSettings.hdsIndexDownloadTimeout);
            };
        }
        private function onIndexComplete(_arg1:HTTPStreamingEvent):void{
            this._dispatcher.dispatchEvent(_arg1);
            var _local2:IDataInput = this._indexDownloader.getBytes(this._indexDownloader.downloadBytesCount);
            var _local3:ByteArray = new ByteArray();
            _local2.readBytes(_local3, 0, _local2.bytesAvailable);
            _local3.position = 0;
            this._indexHandler.processIndexData(_local3, this._currentIndexDownloadEvent.requestContext);
            this.processPendingIndexLoadingRequest();
        }
        private function onIndexError(_arg1:HTTPStreamingEvent):void{
            logger.error("Attempting to download the index file (bootstrap) caused error!");
            if (this._indexDownloader != null){
                this._indexDownloader.close();
            };
            this._currentIndexDownloadEvent = null;
            this._dispatcher.dispatchEvent(_arg1);
        }
        private function processPendingIndexLoadingRequest():void{
            this._pendingIndexDownloadRequests.shift();
            this._pendingIndexDownloadRequestsLenght--;
            if (this._pendingIndexDownloadRequestsLenght == 0){
                if (this._indexDownloader != null){
                    this._indexDownloader.close();
                };
                this._currentIndexDownloadEvent = null;
            } else {
                this._currentIndexDownloadEvent = this._pendingIndexDownloadRequests[0];
                this._indexDownloader.open(this._currentIndexDownloadEvent.request, this._indexDownloaderMonitor, OSMFSettings.hdsIndexDownloadTimeout);
            };
        }
        private function onFragmentDuration(_arg1:HTTPStreamingEvent):void{
            this._fragmentDuration = _arg1.fragmentDuration;
        }
        private function onScriptData(_arg1:HTTPStreamingEvent):void{
            this._dispatcher.dispatchEvent(new HTTPStreamingEvent(_arg1.type, _arg1.bubbles, _arg1.cancelable, _arg1.fragmentDuration, _arg1.scriptDataObject, _arg1.scriptDataMode, this._streamName));
        }
        private function onError(_arg1:HTTPStreamingEvent):void{
            logger.error(("error: " + _arg1));
            this._dispatcher.dispatchEvent(_arg1);
        }
        private function beginQualityLevelChange(_arg1:int):void{
            this._qualityLevelChanged = true;
            this._desiredQualityLevel = _arg1;
            this._desiredQualityStreamName = this._streamNames[this._desiredQualityLevel];
            this._dispatcher.dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.TRANSITION, false, false, NaN, null, null, this._desiredQualityStreamName));
            logger.debug((((("Quality level switch in progress. The next chunk will use the quality level [" + this._desiredQualityLevel) + "] with stream [") + this._desiredQualityStreamName) + " ]."));
        }
        private function endQualityLevelChange():void{
            this._qualityLevel = this._desiredQualityLevel;
            this._streamName = this._desiredQualityStreamName;
            if (this._streamName == null){
                this.loggedStreamName = this._streamName;
            } else {
                this.loggedStreamName = this._streamName.substr(this._streamName.lastIndexOf("/"));
            };
            this._desiredQualityLevel = -1;
            this._desiredQualityStreamName = null;
            this._qualityLevelChanged = false;
            this._dispatcher.dispatchEvent(new HTTPStreamingEvent(HTTPStreamingEvent.TRANSITION_COMPLETE, false, false, NaN, null, null, this._streamName));
            logger.debug((((("Quality level switch completed. The current quality level [" + this._qualityLevel) + "] with stream [") + this.loggedStreamName) + " ]."));
        }
        private function onBestEffortDownloadEvent(_arg1:HTTPStreamingEvent):void{
            if (_arg1.type == HTTPStreamingEvent.DOWNLOAD_COMPLETE){
                this.forwardEventToDispatcher(_arg1);
            } else {
                if (this._bestEffortDownloadResult != null){
                    return;
                };
                this._bestEffortDownloadResult = _arg1.type;
                this.forwardEventToDispatcher(_arg1);
            };
        }
        private function forwardEventToDispatcher(_arg1:Event):void{
            if (this._dispatcher != null){
                this._dispatcher.dispatchEvent(_arg1);
            };
        }

    }
}//package org.osmf.net.httpstreaming 
