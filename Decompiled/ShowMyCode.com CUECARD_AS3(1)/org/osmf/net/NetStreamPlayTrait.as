﻿package org.osmf.net {
    import flash.events.*;
    import org.osmf.media.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import org.osmf.utils.*;

    public class NetStreamPlayTrait extends PlayTrait {

        private static const NETCONNECTION_FAILURE_ERROR_CODE:int = 2154;

        private var streamStarted:Boolean;
        private var netStream:NetStream;
        private var netConnection:NetConnection;
        private var urlResource:URLResource;
        private var multicastResource:MulticastResource;
        private var reconnectStreams:Boolean;

        public function NetStreamPlayTrait(_arg1:NetStream, _arg2:MediaResourceBase, _arg3:Boolean, _arg4:NetConnection){
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            this.netStream = _arg1;
            this.netConnection = _arg4;
            this.urlResource = (_arg2 as URLResource);
            this.multicastResource = (_arg2 as MulticastResource);
            this.reconnectStreams = _arg3;
            _arg1.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus, false, 1, true);
            NetClient(_arg1.client).addHandler(NetStreamCodes.ON_PLAY_STATUS, this.onPlayStatus, 1);
        }
        override protected function playStateChangeStart(_arg1:String):void{
            var _local2:Object;
            var _local3:StreamingURLResource;
            var _local4:Boolean;
            var _local5:String;
            var _local6:Number;
            var _local7:Number;
            var _local8:DynamicStreamingResource;
            var _local9:NetStreamPlayOptions;
            if (_arg1 == PlayState.PLAYING){
                if (this.streamStarted){
                    if (this.multicastResource != null){
                        this.netStream.play(this.multicastResource.streamName, -1, -1);
                    } else {
                        this.netStream.resume();
                    };
                } else {
                    if (this.urlResource != null){
                        _local3 = (this.urlResource as StreamingURLResource);
                        _local4 = ((_local3) ? _local3.urlIncludesFMSApplicationInstance : false);
                        _local5 = NetStreamUtils.getStreamNameFromURL(this.urlResource.url, _local4);
                        _local2 = NetStreamUtils.getPlayArgsForResource(this.urlResource);
                        _local6 = _local2.start;
                        _local7 = _local2.len;
                        _local8 = (this.urlResource as DynamicStreamingResource);
                        if (_local8 != null){
                            _local9 = new NetStreamPlayOptions();
                            _local9.start = _local6;
                            _local9.len = _local7;
                            _local9.streamName = _local8.streamItems[_local8.initialIndex].streamName;
                            _local9.transition = NetStreamPlayTransitions.RESET;
                            this.doPlay2(_local9);
                        } else {
                            if (((((this.reconnectStreams) && (!((_local3 == null))))) && (NetStreamUtils.isRTMPStream(_local3.url)))){
                                _local9 = new NetStreamPlayOptions();
                                _local9.start = _local6;
                                _local9.len = _local7;
                                _local9.transition = NetStreamPlayTransitions.RESET;
                                _local9.streamName = _local5;
                                this.doPlay2(_local9);
                            } else {
                                if (((((!((this.multicastResource == null))) && (!((this.multicastResource.groupspec == null))))) && ((this.multicastResource.groupspec.length > 0)))){
                                    this.doPlay(this.multicastResource.streamName, _local6, _local7);
                                } else {
                                    this.doPlay(_local5, _local6, _local7);
                                };
                            };
                        };
                    };
                };
            } else {
                if (this.multicastResource != null){
                    this.netStream.play(false);
                } else {
                    this.netStream.pause();
                };
            };
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_PLAY_FAILED:
                case NetStreamCodes.NETSTREAM_PLAY_FILESTRUCTUREINVALID:
                case NetStreamCodes.NETSTREAM_PLAY_STREAMNOTFOUND:
                case NetStreamCodes.NETSTREAM_PLAY_NOSUPPORTEDTRACKFOUND:
                case NetStreamCodes.NETSTREAM_FAILED:
                    this.netStream.pause();
                    this.streamStarted = false;
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_STOP:
                    if (((!((this.urlResource == null))) && ((NetStreamUtils.isStreamingResource(this.urlResource) == false)))){
                        stop();
                    };
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_LIVE_STALL:
                    dispatchEvent(new PlayEvent(PlayEvent.LIVE_STALL));
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_LIVE_RESUME:
                    dispatchEvent(new PlayEvent(PlayEvent.LIVE_RESUME));
                    break;
            };
        }
        private function onPlayStatus(_arg1:Object):void{
            switch (_arg1.code){
                case NetStreamCodes.NETSTREAM_PLAY_COMPLETE:
                    stop();
                    break;
            };
        }
        private function doPlay(... _args):void{
            var args:* = _args;
            try {
                this.netStream.play.apply(this, args);
                this.streamStarted = true;
            } catch(error:Error) {
                streamStarted = false;
                stop();
                dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.NETSTREAM_PLAY_FAILED)));
            };
        }
        private function doPlay2(_arg1:NetStreamPlayOptions):void{
            this.netStream.play2(_arg1);
            this.streamStarted = true;
        }

    }
}//package org.osmf.net 
