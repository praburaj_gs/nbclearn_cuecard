﻿package org.osmf.net {

    public class PortProtocol {

        private var _port:int;
        private var _protocol:String;

        public function get port():int{
            return (this._port);
        }
        public function set port(_arg1:int):void{
            this._port = _arg1;
        }
        public function get protocol():String{
            return (this._protocol);
        }
        public function set protocol(_arg1:String):void{
            this._protocol = _arg1;
        }

    }
}//package org.osmf.net 
