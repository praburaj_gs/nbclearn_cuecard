﻿package org.osmf.net {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.metadata.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.utils.*;

    public class NetLoader extends LoaderBase {

        private static const PROTOCOL_RTMP:String = "rtmp";
        private static const PROTOCOL_RTMPS:String = "rtmps";
        private static const PROTOCOL_RTMPT:String = "rtmpt";
        private static const PROTOCOL_RTMPE:String = "rtmpe";
        private static const PROTOCOL_RTMPTE:String = "rtmpte";
        private static const PROTOCOL_RTMFP:String = "rtmfp";
        private static const PROTOCOL_HTTP:String = "http";
        private static const PROTOCOL_HTTPS:String = "https";
        private static const PROTOCOL_FILE:String = "file";
        private static const PROTOCOL_EMPTY:String = "";
        private static const MEDIA_TYPES_SUPPORTED:Vector.<String> = Vector.<String>([MediaType.VIDEO]);
        private static const MIME_TYPES_SUPPORTED:Vector.<String> = Vector.<String>(["video/x-flv", "video/x-f4v", "video/mp4", "video/mp4v-es", "video/x-m4v", "video/3gpp", "video/3gpp2", "video/quicktime"]);
        private static const STREAM_RECONNECT_TIMEOUT:Number = 120000;
        private static const STREAM_RECONNECT_TIMER_INTERVAL:int = 1000;

        private var netConnectionFactory:NetConnectionFactoryBase;
        private var pendingLoads:Dictionary;
        private var oldConnectionURLs:Dictionary;
        private var _reconnectStreams:Boolean = true;
        private var _reconnectTimeout:Number;

        public function NetLoader(_arg1:NetConnectionFactoryBase=null){
            this.pendingLoads = new Dictionary();
            this.oldConnectionURLs = new Dictionary();
            super();
            this._reconnectTimeout = STREAM_RECONNECT_TIMEOUT;
            this.netConnectionFactory = ((_arg1) || (new NetConnectionFactory()));
            this.netConnectionFactory.addEventListener(NetConnectionFactoryEvent.CREATION_COMPLETE, this.onCreationComplete);
            this.netConnectionFactory.addEventListener(NetConnectionFactoryEvent.CREATION_ERROR, this.onCreationError);
        }
        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            var _local2:int = MediaTypeUtil.checkMetadataMatchWithResource(_arg1, MEDIA_TYPES_SUPPORTED, MIME_TYPES_SUPPORTED);
            if (_local2 != MediaTypeUtil.METADATA_MATCH_UNKNOWN){
                return ((_local2 == MediaTypeUtil.METADATA_MATCH_FOUND));
            };
            var _local3:URLResource = (_arg1 as URLResource);
            var _local4:RegExp = new RegExp(".flv$|.f4v$|.m3u8$|.mov$|.mp4$|.mp4v$|.m4v$|.3gp$|.3gpp2$|.3g2$", "i");
            var _local5:URL = ((_local3)!=null) ? new URL(_local3.url) : null;
            if ((((((_local5 == null)) || ((_local5.rawUrl == null)))) || ((_local5.rawUrl.length <= 0)))){
                return (false);
            };
            if (_local5.protocol == ""){
                return (_local4.test(_local5.path));
            };
            if (NetStreamUtils.isRTMPStream(_local5.rawUrl)){
                return (true);
            };
            if (_local5.protocol.search(/file$|http$|https$/i) != -1){
                return ((((((((_local5.path == null)) || ((_local5.path.length <= 0)))) || ((_local5.extension.length == 0)))) || (_local4.test(_local5.path))));
            };
            return (false);
        }
        protected function createNetStream(_arg1:NetConnection, _arg2:URLResource):NetStream{
            var _local3:NetStream = new NetStream(_arg1);
            var _local4:StreamingURLResource = (_arg2 as StreamingURLResource);
            if (((((!((_local4 == null))) && ((_local4.streamType == StreamType.LIVE)))) && ((_local3.bufferTime == 0)))){
                _local3.bufferTime = 0.1;
            };
            return (_local3);
        }
        protected function createNetStreamSwitchManager(_arg1:NetConnection, _arg2:NetStream, _arg3:DynamicStreamingResource):NetStreamSwitchManagerBase{
            return (null);
        }
        protected function processFinishLoading(_arg1:NetStreamLoadTrait):void{
            updateLoadTrait(_arg1, LoadState.READY);
        }
        override protected function executeLoad(_arg1:LoadTrait):void{
            updateLoadTrait(_arg1, LoadState.LOADING);
            var _local2:URL = new URL((_arg1.resource as URLResource).url);
            switch (_local2.protocol){
                case PROTOCOL_RTMP:
                case PROTOCOL_RTMPS:
                case PROTOCOL_RTMPT:
                case PROTOCOL_RTMPE:
                case PROTOCOL_RTMPTE:
                case PROTOCOL_RTMFP:
                    this.startLoadingRTMP(_arg1);
                    break;
                case PROTOCOL_HTTP:
                case PROTOCOL_HTTPS:
                case PROTOCOL_FILE:
                case PROTOCOL_EMPTY:
                    this.startLoadingHTTP(_arg1);
                    break;
                default:
                    updateLoadTrait(_arg1, LoadState.LOAD_ERROR);
                    _arg1.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.URL_SCHEME_INVALID)));
            };
        }
        override protected function executeUnload(_arg1:LoadTrait):void{
            updateLoadTrait(_arg1, LoadState.UNLOADING);
            var _local2:NetStreamLoadTrait = (_arg1 as NetStreamLoadTrait);
            if (_local2 != null){
                if (_local2.netStream != null){
                    _local2.netStream.close();
                };
                if (_local2.netConnectionFactory != null){
                    _local2.netConnectionFactory.closeNetConnection(_local2.connection);
                } else {
                    if (_local2.connection != null){
                        _local2.connection.close();
                    };
                };
            };
            if (this.oldConnectionURLs != null){
                delete this.oldConnectionURLs[_arg1.resource];
            };
            updateLoadTrait(_arg1, LoadState.UNINITIALIZED);
        }
        private function finishLoading(_arg1:NetConnection, _arg2:LoadTrait, _arg3:NetConnectionFactoryBase=null):void{
            var _local5:NetStream;
            var _local6:DynamicStreamingResource;
            var _local4:NetStreamLoadTrait = (_arg2 as NetStreamLoadTrait);
            if (_local4 != null){
                _local4.connection = _arg1;
                _local5 = this.createNetStream(_arg1, (_local4.resource as URLResource));
                _local5.client = new NetClient();
                _local4.netStream = _local5;
                _local6 = (_arg2.resource as DynamicStreamingResource);
                if (_local6 != null){
                    _local4.switchManager = this.createNetStreamSwitchManager(_arg1, _local5, _local6);
                };
                _local4.netConnectionFactory = _arg3;
                if (((((this._reconnectStreams) && ((_local4.resource is URLResource)))) && (this.supportsStreamReconnect((_local4.resource as URLResource))))){
                    this.setupStreamReconnect(_local4);
                };
                this.processFinishLoading((_arg2 as NetStreamLoadTrait));
            };
        }
        private function supportsStreamReconnect(_arg1:URLResource):Boolean{
            var _local3:String;
            var _local4:Array;
            var _local5:int;
            var _local6:int;
            var _local7:int;
            var _local2:Boolean;
            if (NetStreamUtils.isRTMPStream(_arg1.url)){
                _local3 = (_arg1.getMetadataValue(MetadataNamespaces.FMS_SERVER_VERSION_METADATA) as String);
                if (((!((_local3 == null))) && ((_local3.length > 0)))){
                    _local4 = _local3.split(",");
                    if (_local4.length >= 3){
                        _local5 = _local4[0];
                        _local6 = _local4[1];
                        _local7 = _local4[2];
                        if ((((((_local5 < 3)) || ((((_local5 == 3)) && ((_local6 < 5)))))) || ((((((_local5 == 3)) && ((_local6 == 5)))) && ((_local7 < 3)))))){
                            _local2 = false;
                        };
                    };
                };
            } else {
                _local2 = false;
            };
            return (_local2);
        }
        private function startLoadingRTMP(_arg1:LoadTrait):void{
            this.addPendingLoad(_arg1);
            this.netConnectionFactory.create((_arg1.resource as URLResource));
        }
        private function onCreationComplete(_arg1:NetConnectionFactoryEvent):void{
            this.processCreationComplete(_arg1.netConnection, this.findAndRemovePendingLoad(_arg1.resource), (_arg1.currentTarget as NetConnectionFactoryBase));
        }
        protected function processCreationComplete(_arg1:NetConnection, _arg2:LoadTrait, _arg3:NetConnectionFactoryBase=null):void{
            this.finishLoading(_arg1, _arg2, _arg3);
        }
        private function onCreationError(_arg1:NetConnectionFactoryEvent):void{
            var _local2:LoadTrait = this.findAndRemovePendingLoad(_arg1.resource);
            if (_local2 != null){
                _local2.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, _arg1.mediaError));
                updateLoadTrait(_local2, LoadState.LOAD_ERROR);
            };
        }
        private function startLoadingHTTP(_arg1:LoadTrait):void{
            var _local2:NetConnection = new NetConnection();
            _local2.client = new NetClient();
            _local2.connect(null);
            this.finishLoading(_local2, _arg1);
        }
        private function addPendingLoad(_arg1:LoadTrait):void{
            if (this.pendingLoads[_arg1.resource] == null){
                this.pendingLoads[_arg1.resource] = [_arg1];
            } else {
                this.pendingLoads[_arg1.resource].push(_arg1);
            };
        }
        private function findAndRemovePendingLoad(_arg1:URLResource):LoadTrait{
            var _local4:int;
            var _local2:LoadTrait;
            var _local3:Array = this.pendingLoads[_arg1];
            if (_local3 != null){
                if (_local3.length == 1){
                    _local2 = (_local3[0] as LoadTrait);
                    delete this.pendingLoads[_arg1];
                } else {
                    _local4 = 0;
                    while (_local4 < _local3.length) {
                        _local2 = _local3[_local4];
                        if (_local2.resource == _arg1){
                            _local3.splice(_local4, 1);
                            break;
                        };
                        _local4++;
                    };
                };
            };
            return (_local2);
        }
        public function get reconnectTimeout():Number{
            return (this._reconnectTimeout);
        }
        public function set reconnectTimeout(_arg1:Number):void{
            if (_arg1 < 0){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            this._reconnectTimeout = _arg1;
        }
        protected function setReconnectStreams(_arg1:Boolean):void{
            this._reconnectStreams = _arg1;
        }
        public function get reconnectStreams():Boolean{
            return (this._reconnectStreams);
        }
        protected function createReconnectNetConnection():NetConnection{
            return (new NetConnection());
        }
        protected function reconnect(_arg1:NetConnection, _arg2:URLResource):void{
            var _local3:String = (this.oldConnectionURLs[_arg2] as String);
            if (((((!((_local3 == null))) && ((_local3.length > 0)))) && (!((_arg1 == null))))){
                _arg1.connect(_local3);
            };
        }
        protected function reconnectStream(_arg1:NetStreamLoadTrait):void{
            var _local2:NetStreamPlayOptions = new NetStreamPlayOptions();
            _arg1.netStream.attach(_arg1.connection);
            _local2.transition = NetStreamPlayTransitions.RESUME;
            var _local3:URLResource = (_arg1.resource as URLResource);
            var _local4:Boolean = (((_local3 as StreamingURLResource))!=null) ? (_local3 as StreamingURLResource).urlIncludesFMSApplicationInstance : false;
            var _local5:String = NetStreamUtils.getStreamNameFromURL(_local3.url, _local4);
            _local2.streamName = _local5;
            _arg1.netStream.play2(_local2);
        }
        private function setupStreamReconnect(_arg1:NetStreamLoadTrait):void{
            var netConnection:* = null;
            var reconnectTimer:* = null;
            var timeoutTimer:* = null;
            var streamIsPaused:* = false;
            var bufferIsEmpty:* = false;
            var reconnectHasTimedOut:* = false;
            var fmsIdleTimeoutReached:* = false;
            var onNetStatus:* = null;
            var onTimeoutTimer:* = null;
            var onReconnectTimer:* = null;
            var loadTrait:* = _arg1;
            var setupReconnectTimer:* = function (_arg1:Boolean=true):void{
                if (_arg1){
                    reconnectTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onReconnectTimer);
                } else {
                    reconnectTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onReconnectTimer);
                    reconnectTimer = null;
                };
            };
            var setupTimeoutTimer:* = function (_arg1:Boolean=true):void{
                if (_arg1){
                    if (_reconnectTimeout > 0){
                        timeoutTimer = new Timer(_reconnectTimeout, 1);
                        timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimeoutTimer);
                    };
                } else {
                    if (timeoutTimer != null){
                        timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimeoutTimer);
                        timeoutTimer = null;
                    };
                };
            };
            var setupNetConnectionListeners:* = function (_arg1:Boolean=true):void{
                if (_arg1){
                    netConnection.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
                } else {
                    netConnection.removeEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
                };
            };
            var setupNetStreamListeners:* = function (_arg1:Boolean=true):void{
                if (loadTrait.netStream != null){
                    if (_arg1){
                        loadTrait.netStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
                    } else {
                        loadTrait.netStream.removeEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
                    };
                };
            };
            onNetStatus = function (_arg1:NetStatusEvent):void{
                var _local2:NetConnection;
                switch (_arg1.info.code){
                    case NetConnectionCodes.CONNECT_SUCCESS:
                        _local2 = loadTrait.connection;
                        loadTrait.connection = netConnection;
                        oldConnectionURLs[loadTrait.resource] = netConnection.uri;
                        if (timeoutTimer != null){
                            timeoutTimer.stop();
                        };
                        reconnectStream(loadTrait);
                        if (loadTrait.netConnectionFactory != null){
                            loadTrait.netConnectionFactory.closeNetConnection(_local2);
                        } else {
                            _local2.close();
                        };
                        break;
                    case NetConnectionCodes.CONNECT_IDLE_TIME_OUT:
                        fmsIdleTimeoutReached = true;
                        break;
                    case NetConnectionCodes.CONNECT_CLOSED:
                    case NetConnectionCodes.CONNECT_FAILED:
                        if ((((((loadTrait.loadState == LoadState.READY)) && (!(reconnectHasTimedOut)))) && (!(fmsIdleTimeoutReached)))){
                            reconnectTimer.start();
                            if (((((bufferIsEmpty) || ((loadTrait.netStream.bufferLength == 0)))) || (streamIsPaused))){
                                if (timeoutTimer != null){
                                    timeoutTimer.start();
                                } else {
                                    reconnectHasTimedOut = true;
                                    setupReconnectTimer(false);
                                    setupNetConnectionListeners(false);
                                    setupNetStreamListeners(false);
                                    setupTimeoutTimer(false);
                                };
                            };
                        } else {
                            setupReconnectTimer(false);
                            setupNetConnectionListeners(false);
                            setupNetStreamListeners(false);
                            setupTimeoutTimer(false);
                        };
                        break;
                    case NetStreamCodes.NETSTREAM_PAUSE_NOTIFY:
                        streamIsPaused = true;
                        break;
                    case NetStreamCodes.NETSTREAM_UNPAUSE_NOTIFY:
                        streamIsPaused = false;
                        break;
                    case NetStreamCodes.NETSTREAM_BUFFER_EMPTY:
                        if (!netConnection.connected){
                            if (timeoutTimer != null){
                                timeoutTimer.start();
                            } else {
                                reconnectHasTimedOut = true;
                            };
                        } else {
                            bufferIsEmpty = true;
                        };
                        break;
                    case NetStreamCodes.NETSTREAM_BUFFER_FULL:
                        bufferIsEmpty = false;
                        break;
                };
            };
            onTimeoutTimer = function (_arg1:TimerEvent):void{
                reconnectHasTimedOut = true;
            };
            onReconnectTimer = function (_arg1:TimerEvent):void{
                if (reconnectHasTimedOut){
                    return;
                };
                if (netConnection === loadTrait.connection){
                    setupNetConnectionListeners(false);
                    netConnection = createReconnectNetConnection();
                    netConnection.client = new NetClient();
                    setupNetConnectionListeners();
                };
                reconnect(netConnection, (loadTrait.resource as URLResource));
            };
            netConnection = loadTrait.connection;
            reconnectTimer = new Timer(STREAM_RECONNECT_TIMER_INTERVAL, 1);
            this.oldConnectionURLs[loadTrait.resource] = netConnection.uri;
            streamIsPaused = false;
            bufferIsEmpty = false;
            reconnectHasTimedOut = false;
            fmsIdleTimeoutReached = false;
            setupNetConnectionListeners();
            setupNetStreamListeners();
            setupReconnectTimer();
            setupTimeoutTimer();
        }

    }
}//package org.osmf.net 
