﻿package org.osmf.net {

    class FMSHost {

        private var _host:String;
        private var _port:String;

        public function FMSHost(_arg1:String, _arg2:String="1935"){
            this._host = _arg1;
            this._port = _arg2;
        }
        public function get host():String{
            return (this._host);
        }
        public function set host(_arg1:String):void{
            this._host = _arg1;
        }
        public function get port():String{
            return (this._port);
        }
        public function set port(_arg1:String):void{
            this._port = _arg1;
        }

    }
}//package org.osmf.net 
