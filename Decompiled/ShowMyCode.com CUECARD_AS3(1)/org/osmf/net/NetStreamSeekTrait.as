﻿package org.osmf.net {
    import flash.events.*;
    import flash.net.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.media.videoClasses.*;

    public class NetStreamSeekTrait extends SeekTrait {

        private var videoSurface:VideoSurface = null;
        private var loadTrait:LoadTrait;
        private var audioDelay:Number = 0;
        private var seekBugTimer:Timer;
        private var netStream:NetStream;
        private var expectedTime:Number;
        private var previousTime:Number;
        private var suppressSeekNotifyEvent:Boolean = false;

        public function NetStreamSeekTrait(_arg1:TimeTrait, _arg2:LoadTrait, _arg3:NetStream, _arg4:VideoSurface=null){
            super(_arg1);
            this.netStream = _arg3;
            this.videoSurface = _arg4;
            this.loadTrait = _arg2;
            if (_arg3 != null){
                if (_arg3.client != null){
                    NetClient(_arg3.client).addHandler(NetStreamCodes.ON_META_DATA, this.onMetaData);
                };
                _arg3.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus);
            };
            this.seekBugTimer = new Timer(10, 100);
            this.seekBugTimer.addEventListener(TimerEvent.TIMER, this.onSeekBugTimer, false, 0, true);
            this.seekBugTimer.addEventListener(TimerEvent.TIMER_COMPLETE, this.onSeekBugTimerDone, false, 0, true);
        }
        override public function canSeekTo(_arg1:Number):Boolean{
            var _local3:Number;
            var _local2:Boolean = super.canSeekTo(_arg1);
            if (((((((_local2) && (!(isNaN(this.loadTrait.bytesTotal))))) && ((this.loadTrait.bytesTotal > 0)))) && (this.useSeekLimitation))){
                _local3 = (timeTrait.duration * Number((this.loadTrait.bytesLoaded / this.loadTrait.bytesTotal)));
                _local2 = (_arg1 <= _local3);
            };
            return (_local2);
        }
        override protected function seekingChangeStart(_arg1:Boolean, _arg2:Number):void{
            if (_arg1){
                this.suppressSeekNotifyEvent = false;
                this.previousTime = (this.netStream.time - this.audioDelay);
                this.expectedTime = _arg2;
                this.netStream.seek((_arg2 + this.audioDelay));
                if (this.previousTime == this.expectedTime){
                    this.seekBugTimer.start();
                    this.suppressSeekNotifyEvent = true;
                };
            };
        }
        override protected function seekingChangeEnd(_arg1:Number):void{
            var _local2:NetStreamTimeTrait;
            super.seekingChangeEnd(_arg1);
            if ((((seeking == true)) && (!((this.videoSurface == null))))){
                _local2 = (timeTrait as NetStreamTimeTrait);
                if (((!((_local2 == null))) && (((_local2.currentTime + _local2.audioDelay) >= _local2.duration)))){
                    this.videoSurface.clear();
                };
            };
        }
        private function onMetaData(_arg1:Object):void{
            this.audioDelay = ((_arg1.hasOwnProperty("audiodelay")) ? _arg1.audiodelay : 0);
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_SEEK_NOTIFY:
                    this.runSeekBugTimer();
                    break;
                case NetStreamCodes.NETSTREAM_SEEK_INVALIDTIME:
                case NetStreamCodes.NETSTREAM_SEEK_FAILED:
                    setSeeking(false, this.previousTime);
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_START:
                case NetStreamCodes.NETSTREAM_PLAY_RESET:
                case NetStreamCodes.NETSTREAM_PAUSE_NOTIFY:
                case NetStreamCodes.NETSTREAM_PLAY_STOP:
                case NetStreamCodes.NETSTREAM_UNPAUSE_NOTIFY:
                    if (((seeking) && ((this.seekBugTimer.running == false)))){
                        this.runSeekBugTimer();
                    };
                    break;
            };
        }
        private function runSeekBugTimer():void{
            if (this.suppressSeekNotifyEvent == false){
                this.seekBugTimer.start();
            } else {
                this.suppressSeekNotifyEvent = false;
            };
        }
        private function onSeekBugTimer(_arg1:TimerEvent):void{
            if (((!((this.previousTime == (this.netStream.time - this.audioDelay)))) || ((this.previousTime == this.expectedTime)))){
                this.onSeekBugTimerDone(null);
            };
        }
        private function onSeekBugTimerDone(_arg1:TimerEvent):void{
            this.seekBugTimer.reset();
            setSeeking(false, this.expectedTime);
        }
        private function get useSeekLimitation():Boolean{
            return (!((((((this.loadTrait.bytesLoaded == 0)) && (!((this.videoSurface == null))))) && ((this.videoSurface.info.renderStatus == "accelerated")))));
        }

    }
}//package org.osmf.net 
