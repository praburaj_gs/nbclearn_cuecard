﻿package org.osmf.net {

    public class DynamicStreamingItem {

        private var _bitrate:Number;
        private var _stream:String;
        private var _width:int;
        private var _height:int;

        public function DynamicStreamingItem(_arg1:String, _arg2:Number, _arg3:int=-1, _arg4:int=-1){
            this._stream = _arg1;
            this._bitrate = _arg2;
            this._width = _arg3;
            this._height = _arg4;
        }
        public function get streamName():String{
            return (this._stream);
        }
        public function set streamName(_arg1:String):void{
            this._stream = _arg1;
        }
        public function get bitrate():Number{
            return (this._bitrate);
        }
        public function set bitrate(_arg1:Number):void{
            this._bitrate = _arg1;
        }
        public function get width():int{
            return (this._width);
        }
        public function set width(_arg1:int):void{
            this._width = _arg1;
        }
        public function get height():int{
            return (this._height);
        }
        public function set height(_arg1:int):void{
            this._height = _arg1;
        }

    }
}//package org.osmf.net 
