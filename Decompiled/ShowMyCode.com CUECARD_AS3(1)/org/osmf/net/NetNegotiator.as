﻿package org.osmf.net {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.metadata.*;
    import org.osmf.net.*;
    import flash.net.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import flash.errors.*;

    class NetNegotiator extends EventDispatcher {

        private var resource:URLResource;
        private var netConnectionURLs:Vector.<String>;
        private var netConnections:Vector.<NetConnection>;
        private var netConnectionArguments:Vector.<Object>;
        private var failedConnectionCount:int;
        private var timeOutTimer:Timer;
        private var connectionTimer:Timer;
        private var attemptIndex:int;
        private var mediaError:MediaError;
        private var connectionAttemptInterval:Number;
        private var _timeout:Number;

        public function NetNegotiator(_arg1:Number, _arg2:Number=10000):void{
            this.connectionAttemptInterval = _arg1;
            this._timeout = _arg2;
        }
        public function get timeout():Number{
            return (this._timeout);
        }
        public function set timeout(_arg1:Number):void{
            this._timeout = _arg1;
        }
        public function createNetConnection(_arg1:URLResource, _arg2:Vector.<String>, _arg3:Vector.<NetConnection>):void{
            this.resource = _arg1;
            this.netConnectionURLs = _arg2;
            this.netConnections = _arg3;
            var _local4:StreamingURLResource = (_arg1 as StreamingURLResource);
            if (((((!((_local4 == null))) && (!((_local4.connectionArguments == null))))) && ((_local4.connectionArguments.length > 0)))){
                this.netConnectionArguments = _local4.connectionArguments;
            };
            this.initializeConnectionAttempts();
            this.tryToConnect(null);
        }
        private function initializeConnectionAttempts():void{
            this.timeOutTimer = new Timer(this._timeout, 1);
            this.timeOutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, this.masterTimeout);
            this.timeOutTimer.start();
            this.connectionTimer = new Timer(this.connectionAttemptInterval);
            this.connectionTimer.addEventListener(TimerEvent.TIMER, this.tryToConnect);
            this.connectionTimer.start();
            this.failedConnectionCount = 0;
            this.attemptIndex = 0;
        }
        private function tryToConnect(_arg1:TimerEvent):void{
            var rs:* = null;
            var host:* = null;
            var args:* = null;
            var arg:* = null;
            var evt:* = _arg1;
            this.netConnections[this.attemptIndex].addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus, false, 0, true);
            this.netConnections[this.attemptIndex].addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onNetSecurityError, false, 0, true);
            this.netConnections[this.attemptIndex].addEventListener(AsyncErrorEvent.ASYNC_ERROR, this.onAsyncError, false, 0, true);
            this.netConnections[this.attemptIndex].client = new NetClient();
            try {
                rs = (this.resource as MulticastResource);
                if (((((!((rs == null))) && (!((rs.groupspec == null))))) && ((rs.groupspec.length > 0)))){
                    NetConnection(this.netConnections[this.attemptIndex]).connect(rs.url);
                } else {
                    host = this.netConnectionURLs[this.attemptIndex];
                    args = [host];
                    if (this.netConnectionArguments != null){
                        for each (arg in this.netConnectionArguments) {
                            args.push(arg);
                        };
                    };
                    NetConnection(this.netConnections[this.attemptIndex]).connect.apply(this.netConnections[this.attemptIndex], args);
                };
                this.attemptIndex++;
                if (this.attemptIndex >= this.netConnectionURLs.length){
                    this.connectionTimer.stop();
                };
            } catch(ioError:IOError) {
                handleFailedConnectionSession(new MediaError(MediaErrorCodes.IO_ERROR, ioError.message), netConnectionURLs[attemptIndex]);
            } catch(argumentError:ArgumentError) {
                handleFailedConnectionSession(new MediaError(MediaErrorCodes.ARGUMENT_ERROR, argumentError.message), netConnectionURLs[attemptIndex]);
            } catch(securityError:SecurityError) {
                handleFailedConnectionSession(new MediaError(MediaErrorCodes.SECURITY_ERROR, securityError.message), netConnectionURLs[attemptIndex]);
            };
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            var index:* = 0;
            var tempTimer:* = null;
            var onTempTimer:* = null;
            var event:* = _arg1;
            switch (event.info.code){
                case NetConnectionCodes.CONNECT_INVALIDAPP:
                    this.handleFailedConnectionSession(new MediaError(MediaErrorCodes.NETCONNECTION_APPLICATION_INVALID, event.info.description), NetConnection(event.target).uri);
                    break;
                case NetConnectionCodes.CONNECT_REJECTED:
                    if (((event.info.hasOwnProperty("ex")) && ((event.info.ex.code == 302)))){
                        onTempTimer = function (_arg1:TimerEvent):void{
                            tempTimer.removeEventListener(TimerEvent.TIMER, onTempTimer);
                            tempTimer.stop();
                            tryToConnect(null);
                        };
                        index = this.netConnections.indexOf((event.target as NetConnection));
                        this.netConnectionURLs[index] = event.info.ex.redirect;
                        this.attemptIndex = index;
                        tempTimer = new Timer(100, 1);
                        tempTimer.addEventListener(TimerEvent.TIMER, onTempTimer);
                        tempTimer.start();
                    } else {
                        this.handleFailedConnectionSession(new MediaError(MediaErrorCodes.NETCONNECTION_REJECTED, event.info.description), NetConnection(event.target).uri);
                    };
                    break;
                case NetConnectionCodes.CONNECT_FAILED:
                    this.failedConnectionCount++;
                    if (this.failedConnectionCount >= this.netConnectionURLs.length){
                        this.handleFailedConnectionSession(new MediaError(MediaErrorCodes.NETCONNECTION_FAILED), NetConnection(event.target).uri);
                    };
                    break;
                case NetConnectionCodes.CONNECT_SUCCESS:
                    if (((event.info.hasOwnProperty("data")) && (event.info.data.hasOwnProperty("version")))){
                        this.resource.addMetadataValue(MetadataNamespaces.FMS_SERVER_VERSION_METADATA, event.info.data.version);
                    };
                    this.shutDownUnsuccessfulConnections();
                    dispatchEvent(new NetConnectionFactoryEvent(NetConnectionFactoryEvent.CREATION_COMPLETE, false, false, (event.currentTarget as NetConnection), this.resource));
                    break;
                case "NetStream.Publish.Start":
                    break;
            };
        }
        private function shutDownUnsuccessfulConnections():void{
            var _local2:NetConnection;
            this.timeOutTimer.stop();
            this.connectionTimer.stop();
            var _local1:int;
            while (_local1 < this.netConnections.length) {
                _local2 = this.netConnections[_local1];
                if (!_local2.connected){
                    _local2.removeEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus);
                    _local2.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onNetSecurityError);
                    _local2.removeEventListener(AsyncErrorEvent.ASYNC_ERROR, this.onAsyncError);
                    _local2.close();
                    this.netConnections.splice(_local1, 1);
                } else {
                    _local1++;
                };
            };
        }
        private function handleFailedConnectionSession(_arg1:MediaError, _arg2:String):void{
            this.shutDownUnsuccessfulConnections();
            dispatchEvent(new NetConnectionFactoryEvent(NetConnectionFactoryEvent.CREATION_ERROR, false, false, null, this.resource, _arg1));
        }
        private function onNetSecurityError(_arg1:SecurityErrorEvent):void{
            this.handleFailedConnectionSession(new MediaError(MediaErrorCodes.SECURITY_ERROR, _arg1.text), NetConnection(_arg1.target).uri);
        }
        private function onAsyncError(_arg1:AsyncErrorEvent):void{
            this.handleFailedConnectionSession(new MediaError(MediaErrorCodes.ASYNC_ERROR, _arg1.text), NetConnection(_arg1.target).uri);
        }
        private function masterTimeout(_arg1:TimerEvent):void{
            this.handleFailedConnectionSession(new MediaError(MediaErrorCodes.NETCONNECTION_TIMEOUT, ("" + this._timeout)), "");
        }

    }
}//package org.osmf.net 
