﻿package org.osmf.net {
    import flash.events.*;
    import org.osmf.media.*;
    import flash.net.*;
    import org.osmf.traits.*;

    public class NetStreamTimeTrait extends TimeTrait {

        private var durationOffset:Number = 0;
        private var _audioDelay:Number = 0;
        private var netStream:NetStream;
        private var resource:MediaResourceBase;
        private var multicast:Boolean = false;

        public function NetStreamTimeTrait(_arg1:NetStream, _arg2:MediaResourceBase, _arg3:Number=NaN){
            this.netStream = _arg1;
            NetClient(_arg1.client).addHandler(NetStreamCodes.ON_META_DATA, this.onMetaData);
            NetClient(_arg1.client).addHandler(NetStreamCodes.ON_PLAY_STATUS, this.onPlayStatus);
            _arg1.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus, false, 0, true);
            this.resource = _arg2;
            if (isNaN(_arg3) == false){
                setDuration(_arg3);
            };
            var _local4:MulticastResource = (_arg2 as MulticastResource);
            if (((((!((_local4 == null))) && (!((_local4.groupspec == null))))) && ((_local4.groupspec.length > 0)))){
                this.multicast = true;
                setDuration(Number.MAX_VALUE);
            };
        }
        override public function get currentTime():Number{
            if (this.multicast){
                return (0);
            };
            if (this.durationOffset == (duration - (this.netStream.time - this._audioDelay))){
                return (((this.netStream.time - this._audioDelay) + this.durationOffset));
            };
            return ((this.netStream.time - this._audioDelay));
        }
        private function onMetaData(_arg1:Object):void{
            var _local2:Object = NetStreamUtils.getPlayArgsForResource(this.resource);
            this._audioDelay = ((_arg1.hasOwnProperty("audiodelay")) ? _arg1.audiodelay : 0);
            var _local3:Number = Math.max(0, _local2.start);
            var _local4:Number = _local2.len;
            if (_local4 == NetStreamUtils.PLAY_LEN_ARG_ALL){
                _local4 = Number.MAX_VALUE;
            };
            setDuration(Math.min(((_arg1.duration - this._audioDelay) - _local3), _local4));
        }
        private function onPlayStatus(_arg1:Object):void{
            switch (_arg1.code){
                case NetStreamCodes.NETSTREAM_PLAY_COMPLETE:
                    this.signalComplete();
            };
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_PLAY_STOP:
                    if (NetStreamUtils.isStreamingResource(this.resource) == false){
                        this.signalComplete();
                    };
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_UNPUBLISH_NOTIFY:
                    this.signalComplete();
                    break;
            };
        }
        override protected function signalComplete():void{
            if ((this.netStream.time - this._audioDelay) != duration){
                this.durationOffset = (duration - (this.netStream.time - this._audioDelay));
            };
            super.signalComplete();
        }
        function get audioDelay():Number{
            return (this._audioDelay);
        }

    }
}//package org.osmf.net 
