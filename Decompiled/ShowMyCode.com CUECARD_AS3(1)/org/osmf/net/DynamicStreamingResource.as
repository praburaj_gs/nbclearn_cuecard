﻿package org.osmf.net {
    import __AS3__.vec.*;
    import org.osmf.utils.*;

    public class DynamicStreamingResource extends StreamingURLResource {

        private var _streamItems:Vector.<DynamicStreamingItem>;
        private var _initialIndex:int;

        public function DynamicStreamingResource(_arg1:String, _arg2:String=null){
            super(_arg1, _arg2);
            this._initialIndex = 0;
        }
        public function get host():String{
            return (url);
        }
        public function get streamItems():Vector.<DynamicStreamingItem>{
            if (this._streamItems == null){
                this._streamItems = new Vector.<DynamicStreamingItem>();
            };
            return (this._streamItems);
        }
        public function set streamItems(_arg1:Vector.<DynamicStreamingItem>):void{
            this._streamItems = _arg1;
            if (_arg1 != null){
                _arg1.sort(this.compareStreamItems);
            };
        }
        public function get initialIndex():int{
            return (this._initialIndex);
        }
        public function set initialIndex(_arg1:int):void{
            if ((((this._streamItems == null)) || ((_arg1 >= this._streamItems.length)))){
                throw (new RangeError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            this._initialIndex = _arg1;
        }
        public function indexFromName(_arg1:String):int{
            var _local2:int;
            while (_local2 < this._streamItems.length) {
                if ((((this._streamItems[_local2].streamName.indexOf(_arg1) == 0)) || ((this._streamItems[_local2].streamName.indexOf(("mp4:" + _arg1)) == 0)))){
                    return (_local2);
                };
                _local2++;
            };
            return (-1);
        }
        private function compareStreamItems(_arg1:DynamicStreamingItem, _arg2:DynamicStreamingItem):Number{
            var _local3:Number = -1;
            if (_arg1.bitrate == _arg2.bitrate){
                _local3 = 0;
            } else {
                if (_arg1.bitrate > _arg2.bitrate){
                    _local3 = 1;
                };
            };
            return (_local3);
        }

    }
}//package org.osmf.net 
