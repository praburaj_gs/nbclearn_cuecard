﻿package org.osmf.net {

    public class SwitchingRuleBase {

        private var _metrics:NetStreamMetricsBase;

        public function SwitchingRuleBase(_arg1:NetStreamMetricsBase){
            this._metrics = _arg1;
        }
        public function getNewIndex():int{
            return (-1);
        }
        protected function get metrics():NetStreamMetricsBase{
            return (this._metrics);
        }

    }
}//package org.osmf.net 
