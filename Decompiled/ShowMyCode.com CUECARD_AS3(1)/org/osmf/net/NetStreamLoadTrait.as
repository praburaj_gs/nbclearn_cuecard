﻿package org.osmf.net {
    import flash.events.*;
    import org.osmf.media.*;
    import org.osmf.net.httpstreaming.*;
    import flash.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class NetStreamLoadTrait extends LoadTrait {

        private var _connection:NetConnection;
        private var _switchManager:NetStreamSwitchManagerBase;
        private var traits:Dictionary;
        private var _netConnectionFactory:NetConnectionFactoryBase;
        private var isStreamingResource:Boolean;
        private var _netStream:NetStream;
        private var _netGroup:NetGroup;

        public function NetStreamLoadTrait(_arg1:LoaderBase, _arg2:MediaResourceBase){
            this.traits = new Dictionary();
            super(_arg1, _arg2);
            this.isStreamingResource = NetStreamUtils.isStreamingResource(_arg2);
        }
        public function get connection():NetConnection{
            return (this._connection);
        }
        public function set connection(_arg1:NetConnection):void{
            this._connection = _arg1;
        }
        public function get netStream():NetStream{
            return (this._netStream);
        }
        public function set netStream(_arg1:NetStream):void{
            this._netStream = _arg1;
        }
        public function get switchManager():NetStreamSwitchManagerBase{
            return (this._switchManager);
        }
        public function set switchManager(_arg1:NetStreamSwitchManagerBase):void{
            this._switchManager = _arg1;
        }
        public function setTrait(_arg1:MediaTraitBase):void{
            if (_arg1 == null){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            this.traits[_arg1.traitType] = _arg1;
        }
        public function getTrait(_arg1:String):MediaTraitBase{
            return (this.traits[_arg1]);
        }
        public function get netConnectionFactory():NetConnectionFactoryBase{
            return (this._netConnectionFactory);
        }
        public function set netConnectionFactory(_arg1:NetConnectionFactoryBase):void{
            this._netConnectionFactory = _arg1;
        }
        override protected function loadStateChangeStart(_arg1:String):void{
            if (_arg1 == LoadState.READY){
                if (((!(this.isStreamingResource)) && ((((this.netStream.bytesTotal <= 0)) || ((this.netStream.bytesTotal == uint.MAX_VALUE)))))){
                    this.netStream.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus);
                };
            } else {
                if (_arg1 == LoadState.UNINITIALIZED){
                    this.netStream = null;
                    dispatchEvent(new LoadEvent(LoadEvent.BYTES_LOADED_CHANGE, false, false, null, this.bytesLoaded));
                    dispatchEvent(new LoadEvent(LoadEvent.BYTES_TOTAL_CHANGE, false, false, null, this.bytesTotal));
                };
            };
        }
        override public function get bytesLoaded():Number{
            if (((!((this.netStream == null))) && (((!(this.isStreamingResource)) || ((this.netStream is HTTPNetStream)))))){
                return (this.netStream.bytesLoaded);
            };
            return (NaN);
        }
        override public function get bytesTotal():Number{
            return (((this.isStreamingResource) ? NaN : ((this.netStream)!=null) ? this.netStream.bytesTotal : NaN));
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            if (((!((this.netStream == null))) && ((this.netStream.bytesTotal > 0)))){
                dispatchEvent(new LoadEvent(LoadEvent.BYTES_TOTAL_CHANGE, false, false, null, this.netStream.bytesTotal));
                this.netStream.removeEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus);
            };
        }
        public function get netGroup():NetGroup{
            return (this._netGroup);
        }
        public function set netGroup(_arg1:NetGroup):void{
            this._netGroup = _arg1;
        }

    }
}//package org.osmf.net 
