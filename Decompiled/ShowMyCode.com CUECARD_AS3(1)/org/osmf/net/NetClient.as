﻿package org.osmf.net {
    import flash.utils.*;

    public dynamic class NetClient extends Proxy {

        private var handlers:Dictionary;

        public function NetClient(){
            this.handlers = new Dictionary();
            super();
        }
        public function addHandler(_arg1:String, _arg2:Function, _arg3:int=0):void{
            var _local5:Boolean;
            var _local6:int;
            var _local7:Object;
            var _local4:Array = ((this.handlers.hasOwnProperty(_arg1)) ? this.handlers[_arg1] : this.handlers[_arg1] = []);
            if (_local4.indexOf(_arg2) == -1){
                _local5 = false;
                _arg3 = Math.max(0, _arg3);
                if (_arg3 > 0){
                    _local6 = 0;
                    while (_local6 < _local4.length) {
                        _local7 = _local4[_local6];
                        if (_local7.priority < _arg3){
                            _local4.splice(_local6, 0, {
                                handler:_arg2,
                                priority:_arg3
                            });
                            _local5 = true;
                            break;
                        };
                        _local6++;
                    };
                };
                if (!_local5){
                    _local4.push({
                        handler:_arg2,
                        priority:_arg3
                    });
                };
            };
        }
        public function removeHandler(_arg1:String, _arg2:Function):void{
            var _local3:Array;
            var _local4:int;
            var _local5:Object;
            if (this.handlers.hasOwnProperty(_arg1)){
                _local3 = this.handlers[_arg1];
                _local4 = 0;
                while (_local4 < _local3.length) {
                    _local5 = _local3[_local4];
                    if (_local5.handler == _arg2){
                        _local3.splice(_local4, 1);
                        break;
                    };
                    _local4++;
                };
            };
        }
        override "http://www.adobe.com/2006/actionscript/flash/proxy"?? function callProperty(_arg1, ... _args){
            return (this.invokeHandlers(_arg1, _args));
        }
        override "http://www.adobe.com/2006/actionscript/flash/proxy"?? function getProperty(_arg1){
            var result:* = undefined;
            var name:* = _arg1;
            if (this.handlers.hasOwnProperty(name)){
                result = function (){
                    return (invokeHandlers(arguments.callee.name, arguments));
                };
                result.name = name;
            };
            return (result);
        }
        override "http://www.adobe.com/2006/actionscript/flash/proxy"?? function hasProperty(_arg1):Boolean{
            return (this.handlers.hasOwnProperty(_arg1));
        }
        private function invokeHandlers(_arg1:String, _arg2:Array){
            var _local3:Array;
            var _local4:Array;
            var _local5:Object;
            if (this.handlers.hasOwnProperty(_arg1)){
                _local3 = [];
                _local4 = this.handlers[_arg1];
                for each (_local5 in _local4) {
                    _local3.push(_local5.handler.apply(null, _arg2));
                };
            };
            return (_local3);
        }

    }
}//package org.osmf.net 
