﻿package org.osmf.net {

    public final class StreamingItemType {

        public static const VIDEO:String = "video";
        public static const AUDIO:String = "audio";

    }
}//package org.osmf.net 
