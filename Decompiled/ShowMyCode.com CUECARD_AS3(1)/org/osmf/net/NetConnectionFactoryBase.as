﻿package org.osmf.net {
    import flash.events.*;
    import org.osmf.media.*;
    import flash.net.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class NetConnectionFactoryBase extends EventDispatcher {

        public function create(_arg1:URLResource):void{
            throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.FUNCTION_MUST_BE_OVERRIDDEN)));
        }
        public function closeNetConnection(_arg1:NetConnection):void{
            _arg1.close();
        }

    }
}//package org.osmf.net 
