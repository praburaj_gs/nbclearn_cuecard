﻿package org.osmf.net {

    public class StreamingItem {

        private var _type:String = null;
        private var _streamName:String = null;
        private var _bitrate:Number;
        private var _info:Object = null;

        public function StreamingItem(_arg1:String, _arg2:String, _arg3:Number=0, _arg4:Object=null){
            this._type = _arg1;
            this._streamName = _arg2;
            this._bitrate = _arg3;
            this._info = (((_arg4 == null)) ? new Object() : _arg4);
        }
        public function get type():String{
            return (this._type);
        }
        public function get streamName():String{
            return (this._streamName);
        }
        public function get bitrate():Number{
            return (this._bitrate);
        }
        public function get info():Object{
            return (this._info);
        }

    }
}//package org.osmf.net 
