﻿package org.osmf.net {
    import flash.events.*;
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import flash.net.*;
    import org.osmf.net.qos.*;
    import flash.utils.*;

    public class NetStreamPlaybackDetailsRecorder {

        private static const DFPS_AFTER_TRANSITION_IGNORE_TIME:Number = 2000;
        private static const logger:Logger = Log.getLogger("org.osmf.net.httpstreaming.PlaybackDetailsRecorder");

        private var _playingIndex:uint;
        private var netStream:NetStream;
        private var lastDroppedFrames:Number;
        private var lastNetStreamTime:Number;
        private var lastTransitionTime:Number = -INF;
        private var resource:DynamicStreamingResource;
        private var playbackDetailsRecord:Vector.<PlaybackDetails>;
        private var timer:Timer;
        private var seeking:Boolean = false;

        public function NetStreamPlaybackDetailsRecorder(_arg1:NetStream, _arg2:NetClient, _arg3:DynamicStreamingResource){
            this.netStream = _arg1;
            this.resource = _arg3;
            this._playingIndex = Math.max(_arg3.initialIndex, 0);
            this.resetRecord();
            this.timer = new Timer(DFPS_AFTER_TRANSITION_IGNORE_TIME, 1);
            this.timer.addEventListener(TimerEvent.TIMER, this.onTimer);
            _arg1.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus);
            _arg2.addHandler("onPlayStatus", this.onPlayStatus);
        }
        public function computeAndGetRecord():Vector.<PlaybackDetails>{
            logger.debug("Received command to compute now.");
            if (!this.seeking){
                logger.info(("Computing the record for the currently playing index: " + this._playingIndex));
                this.performComputation();
            } else {
                logger.info("The NetStream is currently seeking. Providing cached record.");
            };
            var _local1:Vector.<PlaybackDetails> = this.playbackDetailsRecord.slice();
            this.resetRecord();
            return (_local1);
        }
        public function get playingIndex():uint{
            return (this._playingIndex);
        }
        public function resetRecord():void{
            this.playbackDetailsRecord = new Vector.<PlaybackDetails>();
            this.lastNetStreamTime = this.netStream.time;
            this.lastDroppedFrames = this.netStream.info.droppedFrames;
            logger.debug(((("Resetting playback details record. lastNetStreamTime = " + this.lastNetStreamTime) + "; lastDroppedFrames = ") + this.lastDroppedFrames));
        }
        private function onTimer(_arg1:TimerEvent):void{
            this.timer.reset();
            logger.debug(((("Ignoring " + (this.netStream.info.droppedFrames - this.lastDroppedFrames)) + " dropped frames, ") + "because they are right after a transition."));
            this.lastDroppedFrames = this.netStream.info.droppedFrames;
        }
        private function performComputation():void{
            var _local6:PlaybackDetails;
            var _local7:PlaybackDetails;
            var _local1:Number = this.netStream.time;
            var _local2:Number = this.netStream.info.droppedFrames;
            if ((getTimer() - this.lastTransitionTime) < DFPS_AFTER_TRANSITION_IGNORE_TIME){
                logger.debug(((("Ignoring " + (_local2 - this.lastDroppedFrames)) + " dropped frames, ") + "because they are right after a transition."));
                this.lastDroppedFrames = _local2;
            };
            var _local3:Number = (_local1 - this.lastNetStreamTime);
            var _local4:Number = (_local2 - this.lastDroppedFrames);
            logger.debug(((((((("Recording playback details for played index (" + this._playingIndex) + "): ") + this._playingIndex) + ", ") + _local3) + ", ") + _local4));
            var _local5:Boolean;
            for each (_local6 in this.playbackDetailsRecord) {
                if (_local6.index == this._playingIndex){
                    _local5 = true;
                    _local6.duration = (_local6.duration + _local3);
                    _local6.droppedFrames = (_local6.droppedFrames + _local4);
                    break;
                };
            };
            if (!_local5){
                _local7 = new PlaybackDetails(this._playingIndex, _local3, _local4);
                this.playbackDetailsRecord.push(_local7);
            };
            this.lastNetStreamTime = _local1;
            this.lastDroppedFrames = _local2;
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            logger.debug(("onNetStatus() - event.info.code=" + _arg1.info.code));
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_SEEK_START:
                    logger.debug("Seek initiated, need to pause computation");
                    this.performComputation();
                    this.seeking = true;
                    break;
                case NetStreamCodes.NETSTREAM_SEEK_NOTIFY:
                    logger.debug(("Seek complete to time: " + this.netStream.time));
                    this.resetRecord();
                    this.seeking = false;
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_STOP:
                    this.resetRecord();
                    break;
            };
        }
        private function onPlayStatus(_arg1:Object):void{
            var _local2:int;
            switch (_arg1.code){
                case NetStreamCodes.NETSTREAM_PLAY_TRANSITION_COMPLETE:
                    _local2 = this.resource.indexFromName(_arg1.details);
                    if (_local2 >= 0){
                        logger.debug((((("Transition complete from " + this._playingIndex) + " to ") + _local2) + ". Computing playback details."));
                        if (!this.seeking){
                            this.performComputation();
                        };
                        this._playingIndex = _local2;
                        this.timer.start();
                        this.lastTransitionTime = getTimer();
                    };
                    break;
            };
        }

    }
}//package org.osmf.net 
