﻿package org.osmf.net {
    import flash.events.*;
    import org.osmf.logging.*;
    import flash.net.*;
    import org.osmf.utils.*;

    public class NetStreamSwitcher extends EventDispatcher {

        private static const logger:Logger = Log.getLogger("org.osmf.net.NetStreamSwitcher");

        private var oldStreamName:String;
        private var netStream:NetStream = null;
        private var dsResource:DynamicStreamingResource = null;
        private var _currentIndex:uint = 0;
        private var _actualIndex:int = -1;
        private var _switching:Boolean;

        public function NetStreamSwitcher(_arg1:NetStream, _arg2:DynamicStreamingResource){
            if (_arg1 == null){
                throw (new ArgumentError("Invalid netStream"));
            };
            if (_arg2 == null){
                throw (new ArgumentError("Invalid dynamic streaming resource"));
            };
            this.netStream = _arg1;
            this.dsResource = _arg2;
            this._currentIndex = Math.max(0, _arg2.initialIndex);
            _arg1.addEventListener(NetStatusEvent.NET_STATUS, this.onNetStatus);
            var _local3:NetClient = (_arg1.client as NetClient);
            if (_local3 != null){
                NetClient(_arg1.client).addHandler(NetStreamCodes.ON_PLAY_STATUS, this.onPlayStatus, int.MAX_VALUE);
            } else {
                throw (new Error("The netStream does not have a NetClient associated."));
            };
        }
        public function get currentIndex():uint{
            return (this._currentIndex);
        }
        public function get actualIndex():int{
            return ((((this._actualIndex == -1)) ? this._currentIndex : this._actualIndex));
        }
        public function get switching():Boolean{
            return (this._switching);
        }
        public function switchTo(_arg1:int):void{
            if (_arg1 < 0){
                throw (new RangeError(OSMFStrings.getString(OSMFStrings.STREAMSWITCH_INVALID_INDEX)));
            };
            if (this._actualIndex == -1){
                this.prepareForSwitching();
            };
            this.executeSwitch(_arg1);
        }
        private function setCurrentIndex(_arg1:uint):void{
            var _local2:uint = this._currentIndex;
            this._currentIndex = _arg1;
        }
        private function setActualIndex(_arg1:int):void{
            var _local2:int = this._actualIndex;
            this._actualIndex = _arg1;
        }
        private function executeSwitch(_arg1:int):void{
            var _local2:NetStreamPlayOptions = new NetStreamPlayOptions();
            var _local3:Object = NetStreamUtils.getPlayArgsForResource(this.dsResource);
            _local2.start = _local3.start;
            _local2.len = _local3.len;
            _local2.streamName = this.dsResource.streamItems[_arg1].streamName;
            var _local4:String = this.oldStreamName;
            if (((!((_local4 == null))) && ((_local4.indexOf("?") >= 0)))){
                _local2.oldStreamName = _local4.substr(0, _local4.indexOf("?"));
            } else {
                _local2.oldStreamName = this.oldStreamName;
            };
            _local2.transition = NetStreamPlayTransitions.SWITCH;
            logger.debug((((("executeSwitch() - Switching to index " + _arg1) + " at ") + Math.round(this.dsResource.streamItems[_arg1].bitrate)) + " kbps"));
            this._switching = true;
            this.netStream.play2(_local2);
            this.oldStreamName = this.dsResource.streamItems[_arg1].streamName;
        }
        private function prepareForSwitching():void{
            this._actualIndex = 0;
            if ((((this.dsResource.initialIndex >= 0)) && ((this.dsResource.initialIndex < this.dsResource.streamItems.length)))){
                this._actualIndex = this.dsResource.initialIndex;
            };
            logger.debug((((("prepareForSwitching() - Starting with stream index " + this._actualIndex) + " at ") + Math.round(this.dsResource.streamItems[this._actualIndex].bitrate)) + " kbps"));
        }
        private function onNetStatus(_arg1:NetStatusEvent):void{
            var _local2:int;
            logger.debug(("onNetStatus() - event.info.code = " + _arg1.info.code));
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_PLAY_START:
                    if (this._actualIndex == -1){
                        this.prepareForSwitching();
                    };
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_TRANSITION:
                    _local2 = this.dsResource.indexFromName(_arg1.info.details);
                    if (_local2 >= 0){
                        this.setActualIndex(_local2);
                        logger.debug(("event.info.details (index) = " + _local2));
                        if (this._actualIndex > -1){
                            this._switching = false;
                        };
                    };
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_FAILED:
                    this._switching = false;
                    break;
                case NetStreamCodes.NETSTREAM_SEEK_NOTIFY:
                    this._switching = false;
                    this.setCurrentIndex(this.actualIndex);
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_STOP:
                    break;
            };
        }
        private function onPlayStatus(_arg1:Object):void{
            var _local2:int;
            switch (_arg1.code){
                case NetStreamCodes.NETSTREAM_PLAY_TRANSITION_COMPLETE:
                    _local2 = this.dsResource.indexFromName(_arg1.details);
                    if (_local2 >= 0){
                        this.setCurrentIndex(_local2);
                        logger.debug((((("onPlayStatus() - Transition complete to index: " + _local2) + " at ") + Math.round(this.dsResource.streamItems[_local2].bitrate)) + " kbps"));
                    };
                    break;
            };
        }

    }
}//package org.osmf.net 
