﻿package org.osmf.net {
    import __AS3__.vec.*;

    public class ABRUtils {

        public static function validateWeights(_arg1:Vector.<Number>, _arg2:int=-1):void{
            var _local4:Number;
            if (_arg1 == null){
                throw (new ArgumentError("The weights vector is null."));
            };
            if ((((_arg2 > -1)) && (!((_arg1.length == _arg2))))){
                throw (new ArgumentError("Invalid number of weights."));
            };
            var _local3:Boolean;
            for each (_local4 in _arg1) {
                if (((isNaN(_local4)) || ((_local4 < 0)))){
                    throw (new ArgumentError("Invalid weight in weights Vector."));
                };
                if (_local4 > 0){
                    _local3 = true;
                };
            };
            if (!_local3){
                throw (new ArgumentError("At least one weight must be greater than 0."));
            };
        }
        public static function roundNumber(_arg1:Number):Number{
            if (isNaN(_arg1)){
                return (_arg1);
            };
            return ((Math.round((_arg1 * 1000)) / 1000));
        }

    }
}//package org.osmf.net 
