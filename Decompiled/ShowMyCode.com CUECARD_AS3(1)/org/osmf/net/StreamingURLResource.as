﻿package org.osmf.net {
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import flash.utils.*;

    public class StreamingURLResource extends URLResource {

        private var _streamType:String;
        private var _clipStartTime:Number;
        private var _clipEndTime:Number;
        private var _connectionArguments:Vector.<Object>;
        private var _drmContentData:ByteArray;
        private var _urlIncludesFMSApplicationInstance:Boolean = false;
        private var _alternativeAudioStreamItems:Vector.<StreamingItem> = null;

        public function StreamingURLResource(_arg1:String, _arg2:String=null, _arg3:Number=NaN, _arg4:Number=NaN, _arg5:Vector.<Object>=null, _arg6:Boolean=false, _arg7:ByteArray=null){
            this._streamType = ((_arg2) || (StreamType.RECORDED));
            this._clipStartTime = _arg3;
            this._clipEndTime = _arg4;
            this._urlIncludesFMSApplicationInstance = _arg6;
            this._drmContentData = _arg7;
            this._connectionArguments = _arg5;
            super(_arg1);
        }
        public function get streamType():String{
            return (this._streamType);
        }
        public function set streamType(_arg1:String):void{
            this._streamType = _arg1;
        }
        public function get clipStartTime():Number{
            return (this._clipStartTime);
        }
        public function set clipStartTime(_arg1:Number):void{
            this._clipStartTime = _arg1;
        }
        public function get clipEndTime():Number{
            return (this._clipEndTime);
        }
        public function set clipEndTime(_arg1:Number):void{
            this._clipEndTime = _arg1;
        }
        public function get connectionArguments():Vector.<Object>{
            return (this._connectionArguments);
        }
        public function set connectionArguments(_arg1:Vector.<Object>):void{
            this._connectionArguments = _arg1;
        }
        public function get drmContentData():ByteArray{
            return (this._drmContentData);
        }
        public function set drmContentData(_arg1:ByteArray):void{
            this._drmContentData = _arg1;
        }
        public function get urlIncludesFMSApplicationInstance():Boolean{
            return (this._urlIncludesFMSApplicationInstance);
        }
        public function set urlIncludesFMSApplicationInstance(_arg1:Boolean):void{
            this._urlIncludesFMSApplicationInstance = _arg1;
        }
        public function get alternativeAudioStreamItems():Vector.<StreamingItem>{
            if (this._alternativeAudioStreamItems == null){
                this._alternativeAudioStreamItems = new Vector.<StreamingItem>();
            };
            return (this._alternativeAudioStreamItems);
        }
        public function set alternativeAudioStreamItems(_arg1:Vector.<StreamingItem>):void{
            this._alternativeAudioStreamItems = _arg1;
        }

    }
}//package org.osmf.net 
