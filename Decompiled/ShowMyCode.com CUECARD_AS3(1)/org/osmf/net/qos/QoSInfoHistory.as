﻿package org.osmf.net.qos {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import flash.net.*;
    import org.osmf.events.*;

    public class QoSInfoHistory {

        public static const DEFAULT_HISTORY_LENGTH:Number = 10;
        private static const logger:Logger = Log.getLogger("org.osmf.net.qos.QoSInfoHistory");

        private var history:Vector.<QoSInfo>;
        private var _maxHistoryLength:uint = 0;

        public function QoSInfoHistory(_arg1:NetStream, _arg2:uint=10){
            this.history = new Vector.<QoSInfo>();
            this.maxHistoryLength = _arg2;
            _arg1.addEventListener(QoSInfoEvent.QOS_UPDATE, this.onQoSUpdate);
        }
        public function get length():uint{
            return (this.history.length);
        }
        public function getHistory(_arg1:uint=0):Vector.<QoSInfo>{
            if (_arg1 == 0){
                return (this.history.slice());
            };
            return (this.history.slice(0, _arg1));
        }
        public function getLatestQoSInfo():QoSInfo{
            if (this.history.length > 0){
                return (this.history[0]);
            };
            return (null);
        }
        public function get maxHistoryLength():uint{
            return (this._maxHistoryLength);
        }
        public function set maxHistoryLength(_arg1:uint):void{
            if (_arg1 == 0){
                throw (new ArgumentError("maxHistoryLength needs to be greater than 0."));
            };
            this._maxHistoryLength = _arg1;
            this.trimHistory();
        }
        public function flush():void{
            this.history = new Vector.<QoSInfo>();
        }
        function addQoSInfo(_arg1:QoSInfo):void{
            logger.info("Adding new QoSInfo:");
            _arg1.logInformation();
            this.history.splice(0, 0, _arg1);
            this.trimHistory();
        }
        private function onQoSUpdate(_arg1:QoSInfoEvent):void{
            this.addQoSInfo(_arg1.qosInfo);
        }
        private function trimHistory():void{
            if (this.history.length > this._maxHistoryLength){
                this.history.length = this._maxHistoryLength;
            };
        }

    }
}//package org.osmf.net.qos 
