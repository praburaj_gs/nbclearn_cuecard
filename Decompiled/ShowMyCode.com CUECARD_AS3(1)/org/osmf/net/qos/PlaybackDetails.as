﻿package org.osmf.net.qos {

    public class PlaybackDetails {

        private var _index:uint;
        private var _duration:Number;
        private var _droppedFrames:Number;

        public function PlaybackDetails(_arg1:uint, _arg2:Number, _arg3:Number){
            this._index = _arg1;
            this._duration = _arg2;
            this._droppedFrames = _arg3;
        }
        public function get index():uint{
            return (this._index);
        }
        public function get duration():Number{
            return (this._duration);
        }
        public function set duration(_arg1:Number):void{
            this._duration = _arg1;
        }
        public function get droppedFrames():Number{
            return (this._droppedFrames);
        }
        public function set droppedFrames(_arg1:Number):void{
            this._droppedFrames = _arg1;
        }

    }
}//package org.osmf.net.qos 
