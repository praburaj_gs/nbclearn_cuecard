﻿package org.osmf.net.qos {

    public class QualityLevel {

        private var _index:uint;
        private var _bitrate:Number;
        private var _streamName:String;

        public function QualityLevel(_arg1:uint, _arg2:Number, _arg3:String=null){
            this._index = _arg1;
            this._bitrate = _arg2;
            this._streamName = _arg3;
        }
        public function get index():uint{
            return (this._index);
        }
        public function get bitrate():Number{
            return (this._bitrate);
        }
        public function get streamName():String{
            return (this._streamName);
        }

    }
}//package org.osmf.net.qos 
