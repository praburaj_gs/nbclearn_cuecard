﻿package org.osmf.net.qos {

    public class FragmentDetails {

        private var _index:uint;
        private var _size:Number;
        private var _playDuration:Number;
        private var _downloadDuration:Number;
        private var _fragmentIdentifier:String;

        public function FragmentDetails(_arg1:Number, _arg2:Number, _arg3:Number, _arg4:uint, _arg5:String=null){
            this._size = _arg1;
            this._playDuration = _arg2;
            this._downloadDuration = _arg3;
            this._index = _arg4;
            this._fragmentIdentifier = _arg5;
        }
        public function get size():Number{
            return (this._size);
        }
        public function get playDuration():Number{
            return (this._playDuration);
        }
        public function get downloadDuration():Number{
            return (this._downloadDuration);
        }
        public function get index():uint{
            return (this._index);
        }
        public function get fragmentIdentifier():String{
            return (this._fragmentIdentifier);
        }

    }
}//package org.osmf.net.qos 
