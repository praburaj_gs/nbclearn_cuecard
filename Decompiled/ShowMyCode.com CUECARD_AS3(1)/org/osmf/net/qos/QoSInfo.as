﻿package org.osmf.net.qos {
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import org.osmf.net.*;
    import flash.net.*;

    public class QoSInfo {

        private static const logger:Logger = Log.getLogger("org.osmf.net.qos.QoSInfo");

        private var _currentIndex:int = -1;
        private var _actualIndex:int = -1;
        private var _lastDownloadedFragmentDetails:FragmentDetails = null;
        private var _timestamp:Number = NaN;
        private var _playheadTime:Number = NaN;
        private var _availableQualityLevels:Vector.<QualityLevel> = null;
        private var _maxFPS:Number = NaN;
        private var _nsInfo:NetStreamInfo = null;
        private var _playbackDetailsRecord:Vector.<PlaybackDetails> = null;
        private var _bufferTime:Number = NaN;
        private var _bufferLength:Number = NaN;
        private var _emptyBufferOccurred:Boolean = false;

        public function QoSInfo(_arg1:Number=NaN, _arg2:Number=NaN, _arg3:Vector.<QualityLevel>=null, _arg4:int=-1, _arg5:int=-1, _arg6:FragmentDetails=null, _arg7:Number=NaN, _arg8:Vector.<PlaybackDetails>=null, _arg9:NetStreamInfo=null, _arg10:Number=NaN, _arg11:Number=NaN, _arg12:Boolean=false){
            this._timestamp = _arg1;
            this._playheadTime = _arg2;
            this._availableQualityLevels = _arg3;
            this._currentIndex = _arg4;
            this._actualIndex = _arg5;
            this._lastDownloadedFragmentDetails = _arg6;
            this._maxFPS = _arg7;
            this._playbackDetailsRecord = _arg8;
            this._nsInfo = _arg9;
            this._bufferLength = _arg10;
            this._bufferTime = _arg11;
            this._emptyBufferOccurred = _arg12;
        }
        public function get timestamp():Number{
            return (this._timestamp);
        }
        public function get playheadTime():Number{
            return (this._playheadTime);
        }
        public function get availableQualityLevels():Vector.<QualityLevel>{
            return (this._availableQualityLevels);
        }
        public function get currentIndex():int{
            return (this._currentIndex);
        }
        public function get actualIndex():int{
            return (this._actualIndex);
        }
        public function get lastDownloadedFragmentDetails():FragmentDetails{
            return (this._lastDownloadedFragmentDetails);
        }
        public function get maxFPS():Number{
            return (this._maxFPS);
        }
        public function get nsInfo():NetStreamInfo{
            return (this._nsInfo);
        }
        public function get playbackDetailsRecord():Vector.<PlaybackDetails>{
            return (this._playbackDetailsRecord);
        }
        public function get bufferLength():Number{
            return (this._bufferLength);
        }
        public function get bufferTime():Number{
            return (this._bufferTime);
        }
        public function get emptyBufferOccurred():Boolean{
            return (this._emptyBufferOccurred);
        }
        public function logInformation():void{
            var _local1:QualityLevel;
            var _local2:Date;
            var _local3:FragmentDetails;
            var _local5:PlaybackDetails;
            logger.info(" Available quality levels: (index, streamName, bitrate)");
            for each (_local1 in this.availableQualityLevels) {
                logger.info(((((((("  (" + _local1.index) + ", ") + _local1.streamName) + ", ") + _local1.bitrate) + " kbps") + ")"));
            };
            logger.info((" currentIndex (playing): " + this.currentIndex));
            logger.info((" actualIndex (downloading): " + this.actualIndex));
            logger.info((" Max FPS: " + ABRUtils.roundNumber(this.maxFPS)));
            logger.info((" EmptyBufferInterruption: " + this.emptyBufferOccurred));
            _local2 = new Date(this.timestamp);
            logger.info((((((((" Machine time: " + this.timestamp) + " (") + _local2.toLocaleString()) + ".") + _local2.milliseconds) + "); Playhead time: ") + this.playheadTime));
            _local3 = this.lastDownloadedFragmentDetails;
            if (_local3 != null){
                logger.info(((((((((((((((" Last downloaded fragment details:" + " (index = ") + _local3.index) + ", size = ") + _local3.size) + " B") + ", downloadDuration = ") + _local3.downloadDuration) + " s") + ", playDuration = ") + _local3.playDuration) + " s") + ", fragmentIdentifier = ") + _local3.fragmentIdentifier) + ")"));
            };
            var _local4:Vector.<PlaybackDetails> = this.playbackDetailsRecord;
            if (_local4 != null){
                logger.info(" Since last QoS update, the following quality levels have been played: ");
                for each (_local5 in _local4) {
                    logger.info(((((("  index = " + _local5.index) + ", duration = ") + ABRUtils.roundNumber(_local5.duration)) + " s, dropped frames = ") + _local5.droppedFrames));
                };
            };
        }

    }
}//package org.osmf.net.qos 
