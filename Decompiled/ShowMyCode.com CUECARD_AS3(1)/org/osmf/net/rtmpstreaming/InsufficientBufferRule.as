﻿package org.osmf.net.rtmpstreaming {
    import flash.events.*;
    import org.osmf.net.*;

    public class InsufficientBufferRule extends SwitchingRuleBase {

        private var _panic:Boolean;
        private var _moreDetail:String;
        private var minBufferLength:Number;

        public function InsufficientBufferRule(_arg1:RTMPNetStreamMetrics, _arg2:Number=2){
            super(_arg1);
            this._panic = false;
            this.minBufferLength = _arg2;
            _arg1.netStream.addEventListener(NetStatusEvent.NET_STATUS, this.monitorNetStatus, false, 0, true);
        }
        override public function getNewIndex():int{
            var _local1 = -1;
            if (((this._panic) || ((((this.rtmpMetrics.netStream.bufferLength < this.minBufferLength)) && ((this.rtmpMetrics.netStream.bufferLength > this.rtmpMetrics.netStream.bufferTime)))))){
                _local1 = 0;
            };
            return (_local1);
        }
        private function monitorNetStatus(_arg1:NetStatusEvent):void{
            switch (_arg1.info.code){
                case NetStreamCodes.NETSTREAM_BUFFER_FULL:
                    this._panic = false;
                    break;
                case NetStreamCodes.NETSTREAM_BUFFER_EMPTY:
                    if (Math.round(this.rtmpMetrics.netStream.time) != 0){
                        this._panic = true;
                        this._moreDetail = "Buffer was empty";
                    };
                    break;
                case NetStreamCodes.NETSTREAM_PLAY_INSUFFICIENTBW:
                    this._panic = true;
                    this._moreDetail = "Stream had insufficient bandwidth";
                    break;
            };
        }
        private function get rtmpMetrics():RTMPNetStreamMetrics{
            return ((metrics as RTMPNetStreamMetrics));
        }

    }
}//package org.osmf.net.rtmpstreaming 
