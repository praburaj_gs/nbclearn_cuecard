﻿package org.osmf.net.rtmpstreaming {
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.net.*;
    import flash.net.*;

    public class RTMPDynamicStreamingNetLoader extends NetLoader {

        public function RTMPDynamicStreamingNetLoader(_arg1:NetConnectionFactoryBase=null){
            super(_arg1);
        }
        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            var _local2:DynamicStreamingResource = (_arg1 as DynamicStreamingResource);
            return (((((!((_local2 == null))) && (NetStreamUtils.isRTMPStream(_local2.host)))) || (super.canHandleResource(_arg1))));
        }
        override protected function createNetStreamSwitchManager(_arg1:NetConnection, _arg2:NetStream, _arg3:DynamicStreamingResource):NetStreamSwitchManagerBase{
            var _local4:RTMPNetStreamMetrics = new RTMPNetStreamMetrics(_arg2);
            return (new NetStreamSwitchManager(_arg1, _arg2, _arg3, _local4, this.getDefaultSwitchingRules(_local4)));
        }
        private function getDefaultSwitchingRules(_arg1:RTMPNetStreamMetrics):Vector.<SwitchingRuleBase>{
            var _local2:Vector.<SwitchingRuleBase> = new Vector.<SwitchingRuleBase>();
            _local2.push(new SufficientBandwidthRule(_arg1));
            _local2.push(new InsufficientBandwidthRule(_arg1));
            _local2.push(new DroppedFramesRule(_arg1));
            _local2.push(new InsufficientBufferRule(_arg1));
            return (_local2);
        }
        override protected function reconnectStream(_arg1:NetStreamLoadTrait):void{
            var _local3:NetStreamPlayOptions;
            var _local4:DynamicStreamingItem;
            var _local5:String;
            var _local2:DynamicStreamingResource = (_arg1.resource as DynamicStreamingResource);
            if (_local2 == null){
                super.reconnectStream(_arg1);
            } else {
                _local3 = new NetStreamPlayOptions();
                _arg1.netStream.attach(_arg1.connection);
                _local3.transition = NetStreamPlayTransitions.RESUME;
                _local4 = _local2.streamItems[_arg1.switchManager.currentIndex];
                _local5 = _local4.streamName;
                _local3.streamName = _local5;
                _arg1.netStream.play2(_local3);
            };
        }

    }
}//package org.osmf.net.rtmpstreaming 
