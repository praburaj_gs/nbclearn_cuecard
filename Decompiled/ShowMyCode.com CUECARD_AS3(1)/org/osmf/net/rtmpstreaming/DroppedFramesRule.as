﻿package org.osmf.net.rtmpstreaming {
    import org.osmf.net.*;
    import flash.utils.*;

    public class DroppedFramesRule extends SwitchingRuleBase {

        private static const LOCK_INTERVAL:Number = 30000;

        private var downSwitchByOne:int;
        private var downSwitchByTwo:int;
        private var downSwitchToZero:int;
        private var lockLevel:Number;
        private var lastLockTime:Number;

        public function DroppedFramesRule(_arg1:NetStreamMetricsBase, _arg2:int=10, _arg3:int=20, _arg4:int=24){
            super(_arg1);
            this.downSwitchByOne = _arg2;
            this.downSwitchByTwo = _arg3;
            this.downSwitchToZero = _arg4;
            this.lastLockTime = 0;
            this.lockLevel = int.MAX_VALUE;
        }
        override public function getNewIndex():int{
            var _local2:String;
            var _local1 = -1;
            if (metrics.averageDroppedFPS > this.downSwitchToZero){
                _local1 = 0;
                _local2 = ((("Average droppedFPS of " + Math.round(metrics.averageDroppedFPS)) + " > ") + this.downSwitchToZero);
            } else {
                if (metrics.averageDroppedFPS > this.downSwitchByTwo){
                    _local1 = ((((metrics.currentIndex - 2) < 0)) ? 0 : (metrics.currentIndex - 2));
                    _local2 = ((("Average droppedFPS of " + Math.round(metrics.averageDroppedFPS)) + " > ") + this.downSwitchByTwo);
                } else {
                    if (metrics.averageDroppedFPS > this.downSwitchByOne){
                        _local1 = ((((metrics.currentIndex - 1) < 0)) ? 0 : (metrics.currentIndex - 1));
                        _local2 = ((("Average droppedFPS of " + Math.round(metrics.averageDroppedFPS)) + " > ") + this.downSwitchByOne);
                    };
                };
            };
            if (((!((_local1 == -1))) && ((_local1 < metrics.currentIndex)))){
                this.lockIndex(_local1);
            };
            if ((((_local1 == -1)) && (this.isLocked(metrics.currentIndex)))){
                _local1 = metrics.currentIndex;
            };
            return (_local1);
        }
        private function lockIndex(_arg1:int):void{
            if (!this.isLocked(_arg1)){
                this.lockLevel = _arg1;
                this.lastLockTime = getTimer();
            };
        }
        private function isLocked(_arg1:int):Boolean{
            return ((((_arg1 >= this.lockLevel)) && (((getTimer() - this.lastLockTime) < LOCK_INTERVAL))));
        }

    }
}//package org.osmf.net.rtmpstreaming 
