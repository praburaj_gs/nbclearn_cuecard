﻿package org.osmf.net.rtmpstreaming {
    import org.osmf.net.*;

    public class InsufficientBandwidthRule extends SwitchingRuleBase {

        private var bitrateMultiplier:Number;

        public function InsufficientBandwidthRule(_arg1:RTMPNetStreamMetrics, _arg2:Number=1.15){
            super(_arg1);
            this.bitrateMultiplier = _arg2;
        }
        override public function getNewIndex():int{
            var _local2:String;
            var _local3:int;
            var _local1 = -1;
            if (this.rtmpMetrics.averageMaxBytesPerSecond != 0){
                _local3 = this.rtmpMetrics.currentIndex;
                while (_local3 >= 0) {
                    if (((this.rtmpMetrics.averageMaxBytesPerSecond * 8) / 0x0400) > (this.rtmpMetrics.resource.streamItems[_local3].bitrate * this.bitrateMultiplier)){
                        _local1 = _local3;
                        break;
                    };
                    _local3--;
                };
                _local1 = ((_local1)==this.rtmpMetrics.currentIndex) ? -1 : _local1;
            };
            return (_local1);
        }
        private function get rtmpMetrics():RTMPNetStreamMetrics{
            return ((metrics as RTMPNetStreamMetrics));
        }

    }
}//package org.osmf.net.rtmpstreaming 
