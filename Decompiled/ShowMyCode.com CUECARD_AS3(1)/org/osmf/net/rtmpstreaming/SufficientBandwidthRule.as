﻿package org.osmf.net.rtmpstreaming {
    import org.osmf.net.*;

    public class SufficientBandwidthRule extends SwitchingRuleBase {

        private static const BANDWIDTH_SAFETY_MULTIPLE:Number = 1.15;
        private static const MIN_DROPPED_FPS:int = 2;

        public function SufficientBandwidthRule(_arg1:RTMPNetStreamMetrics){
            super(_arg1);
        }
        override public function getNewIndex():int{
            var _local2:String;
            var _local3:int;
            var _local1 = -1;
            if (this.rtmpMetrics.averageMaxBytesPerSecond != 0){
                _local3 = (this.rtmpMetrics.resource.streamItems.length - 1);
                while (_local3 >= 0) {
                    if (((this.rtmpMetrics.averageMaxBytesPerSecond * 8) / 0x0400) > (this.rtmpMetrics.resource.streamItems[_local3].bitrate * BANDWIDTH_SAFETY_MULTIPLE)){
                        _local1 = _local3;
                        break;
                    };
                    _local3--;
                };
                if (_local1 > this.rtmpMetrics.currentIndex){
                    _local1 = (((((this.rtmpMetrics.droppedFPS < MIN_DROPPED_FPS)) && ((this.rtmpMetrics.netStream.bufferLength > this.rtmpMetrics.netStream.bufferTime)))) ? _local1 : -1);
                } else {
                    _local1 = -1;
                };
            };
            return (_local1);
        }
        private function get rtmpMetrics():RTMPNetStreamMetrics{
            return ((metrics as RTMPNetStreamMetrics));
        }

    }
}//package org.osmf.net.rtmpstreaming 
