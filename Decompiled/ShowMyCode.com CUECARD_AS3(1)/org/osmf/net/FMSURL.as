﻿package org.osmf.net {
    import __AS3__.vec.*;
    import org.osmf.utils.*;

    public class FMSURL extends URL {

        private static const APPNAME_START_INDEX:uint = 0;
        private static const INSTANCENAME_START_INDEX:uint = 2;
        private static const STREAMNAME_START_INDEX:uint = 4;
        private static const DEFAULT_INSTANCE_NAME:String = "_definst_";
        public static const MP4_STREAM:String = "mp4";
        public static const MP3_STREAM:String = "mp3";
        public static const ID3_STREAM:String = "id3";
        public static const QUERY_STRING_STREAM:String = "streamName";
        public static const QUERY_STRING_STREAMTYPE:String = "streamType";

        private var _useInstance:Boolean;
        private var _appName:String;
        private var _instanceName:String;
        private var _streamName:String;
        private var _fileFormat:String;
        private var _origins:Vector.<FMSHost>;
        private var _edges:Vector.<FMSHost>;

        public function FMSURL(_arg1:String, _arg2:Boolean=false){
            super(_arg1);
            this._useInstance = _arg2;
            this._appName = "";
            this._instanceName = "";
            this._streamName = "";
            this._fileFormat = "";
            this.parsePath();
            this.parseQuery();
        }
        public function get useInstance():Boolean{
            return (this._useInstance);
        }
        public function get appName():String{
            return (this._appName);
        }
        public function get instanceName():String{
            return (this._instanceName);
        }
        public function get streamName():String{
            return (this._streamName);
        }
        public function get fileFormat():String{
            return (this._fileFormat);
        }
        public function get edges():Vector.<FMSHost>{
            return (this._edges);
        }
        public function get origins():Vector.<FMSHost>{
            return (this._origins);
        }
        private function parsePath():void{
            var _local7:RegExp;
            var _local8:uint;
            var _local9:int;
            if ((((path == null)) || ((path.length == 0)))){
                this._streamName = getParamValue(QUERY_STRING_STREAM);
                this._fileFormat = getParamValue(QUERY_STRING_STREAMTYPE);
                return;
            };
            var _local1:RegExp = /(\/)/;
            var _local2:Array = path.split(_local1);
            if (_local2 != null){
                this._appName = _local2[APPNAME_START_INDEX];
                this._instanceName = "";
                this._streamName = "";
                _local7 = new RegExp(("^.*/" + DEFAULT_INSTANCE_NAME), "i");
                if (path.search(_local7) > -1){
                    this._useInstance = true;
                };
                _local8 = STREAMNAME_START_INDEX;
                if (this._useInstance){
                    this._instanceName = _local2[INSTANCENAME_START_INDEX];
                } else {
                    _local8 = INSTANCENAME_START_INDEX;
                };
                _local9 = _local8;
                while (_local9 < _local2.length) {
                    this._streamName = (this._streamName + _local2[_local9]);
                    _local9++;
                };
                if ((((this._streamName == null)) || ((this._streamName == "")))){
                    this._streamName = getParamValue(QUERY_STRING_STREAM);
                };
                if (this._streamName.search(/^mp4:/i) > -1){
                    this._fileFormat = MP4_STREAM;
                } else {
                    if (this._streamName.search(/^mp3:/i) > -1){
                        this._fileFormat = MP3_STREAM;
                    } else {
                        if (this._streamName.search(/^id3:/i) > -1){
                            this._fileFormat = ID3_STREAM;
                        };
                    };
                };
                if ((((this._fileFormat == null)) || ((this._fileFormat == "")))){
                    this._fileFormat = getParamValue(QUERY_STRING_STREAMTYPE);
                };
            };
            var _local3:int = this._streamName.indexOf("/mp4:");
            var _local4:int = this._streamName.indexOf("/mp3:");
            var _local5:int = this._streamName.indexOf("/id3:");
            var _local6 = -1;
            if (_local3 > 0){
                _local6 = _local3;
            } else {
                if (_local4 > 0){
                    _local6 = _local4;
                } else {
                    if (_local5 > 0){
                        _local6 = _local5;
                    };
                };
            };
            if (((this.useInstance) && ((_local6 > 0)))){
                this._instanceName = (this._instanceName + "/");
                this._instanceName = (this._instanceName + this._streamName.substr(0, _local6));
                this._streamName = this.streamName.substr((_local6 + 1));
            };
        }
        private function parseQuery():void{
            var _local12:int;
            var _local13:int;
            var _local14:FMSURL;
            if ((((((query == null)) || ((query.length == 0)))) || ((query.search(/:\//) == -1)))){
                return;
            };
            var _local1:Array = query.split("?");
            var _local2:int;
            while (_local2 < _local1.length) {
                _local12 = _local1[_local2].toString().search(/:\//);
                if (_local12 == -1){
                    _local1.splice(_local2, 1);
                };
                _local2++;
            };
            var _local3:Boolean;
            var _local4:int;
            if (_local1.length >= 2){
                _local3 = true;
                _local4 = (_local1.length - 1);
            };
            var _local5 = "";
            var _local6 = "";
            var _local7:int;
            var _local8:int;
            var _local9:int;
            var _local10:int;
            var _local11:int;
            while (_local11 < _local1.length) {
                _local13 = _local1[_local11].toString().search(/:\//);
                _local9 = (_local13 + 2);
                if (_local1[_local11].charAt(_local9) == "/"){
                    _local9++;
                };
                _local7 = _local1[_local11].indexOf(":", _local9);
                _local8 = _local1[_local11].indexOf("/", _local9);
                if ((((_local8 < 0)) && ((_local7 < 0)))){
                    _local5 = _local1[_local11].slice(_local9);
                } else {
                    if ((((_local7 >= 0)) && ((_local7 < _local8)))){
                        _local10 = _local7;
                        _local5 = _local1[_local11].slice(_local9, _local10);
                        _local9 = (_local10 + 1);
                        _local10 = _local8;
                        _local6 = _local1[_local11].slice(_local9, _local10);
                    } else {
                        if (_local1[_local11].indexOf("://") != -1){
                            _local10 = _local8;
                            _local5 = _local1[_local11].slice(_local9, _local10);
                        } else {
                            _local10 = _local1[_local11].indexOf("/");
                            _local5 = "localhost";
                        };
                    };
                };
                if (_local11 == _local4){
                    if (this._origins == null){
                        this._origins = new Vector.<FMSHost>();
                    };
                    this._origins.push(new FMSHost(_local5, _local6));
                    _local14 = new FMSURL(_local1[_local11], this._useInstance);
                    if (this._appName == ""){
                        this._appName = _local14.appName;
                    };
                    if (((this._useInstance) && ((this._instanceName == "")))){
                        this._instanceName = _local14.instanceName;
                    };
                    if (this._streamName == ""){
                        this._streamName = _local14.streamName;
                    };
                } else {
                    if (((!((_local1[_local11] == query))) && (_local3))){
                        if (this._edges == null){
                            this._edges = new Vector.<FMSHost>();
                        };
                        this._edges.push(new FMSHost(_local5, _local6));
                    };
                };
                _local11++;
            };
        }

    }
}//package org.osmf.net 
