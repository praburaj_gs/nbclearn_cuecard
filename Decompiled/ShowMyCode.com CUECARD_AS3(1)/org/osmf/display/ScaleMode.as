﻿package org.osmf.display {

    public final class ScaleMode {

        public static const NONE:String = "none";
        public static const STRETCH:String = "stretch";
        public static const LETTERBOX:String = "letterbox";
        public static const ZOOM:String = "zoom";

    }
}//package org.osmf.display 
