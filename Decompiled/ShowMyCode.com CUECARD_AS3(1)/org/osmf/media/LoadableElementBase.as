﻿package org.osmf.media {
    import __AS3__.vec.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;

    public class LoadableElementBase extends MediaElement {

        private var _loader:LoaderBase;

        public function LoadableElementBase(_arg1:MediaResourceBase=null, _arg2:LoaderBase=null){
            this._loader = _arg2;
            this.resource = _arg1;
        }
        override public function set resource(_arg1:MediaResourceBase):void{
            super.resource = _arg1;
            this.updateLoadTrait();
        }
        final protected function get loader():LoaderBase{
            return (this._loader);
        }
        final protected function set loader(_arg1:LoaderBase):void{
            this._loader = _arg1;
        }
        protected function createLoadTrait(_arg1:MediaResourceBase, _arg2:LoaderBase):LoadTrait{
            return (new LoadTrait(this._loader, _arg1));
        }
        protected function processLoadingState():void{
        }
        protected function processReadyState():void{
        }
        protected function processUnloadingState():void{
        }
        protected function getLoaderForResource(_arg1:MediaResourceBase, _arg2:Vector.<LoaderBase>):LoaderBase{
            var _local4:Boolean;
            var _local5:LoaderBase;
            var _local3:LoaderBase = this.loader;
            if (((!((_arg1 == null))) && ((((this.loader == null)) || ((this.loader.canHandleResource(_arg1) == false)))))){
                _local4 = false;
                for each (_local5 in _arg2) {
                    if ((((this.loader == null)) || (!((this.loader == _local5))))){
                        if (_local5.canHandleResource(_arg1)){
                            _local3 = _local5;
                            break;
                        };
                    };
                };
                if ((((_local3 == null)) && (!((_arg2 == null))))){
                    _local3 = _arg2[(_arg2.length - 1)];
                };
            };
            return (_local3);
        }
        private function onLoadStateChange(_arg1:LoadEvent):void{
            if (_arg1.loadState == LoadState.LOADING){
                this.processLoadingState();
            } else {
                if (_arg1.loadState == LoadState.READY){
                    this.processReadyState();
                } else {
                    if (_arg1.loadState == LoadState.UNLOADING){
                        this.processUnloadingState();
                    };
                };
            };
        }
        private function updateLoadTrait():void{
            var _local1:LoadTrait = (getTrait(MediaTraitType.LOAD) as LoadTrait);
            if (_local1 != null){
                if (_local1.loadState == LoadState.READY){
                    _local1.unload();
                };
                _local1.removeEventListener(LoadEvent.LOAD_STATE_CHANGE, this.onLoadStateChange);
                removeTrait(MediaTraitType.LOAD);
            };
            if (this.loader != null){
                _local1 = this.createLoadTrait(resource, this.loader);
                _local1.addEventListener(LoadEvent.LOAD_STATE_CHANGE, this.onLoadStateChange, false, 10);
                addTrait(MediaTraitType.LOAD, _local1);
            };
        }

    }
}//package org.osmf.media 
