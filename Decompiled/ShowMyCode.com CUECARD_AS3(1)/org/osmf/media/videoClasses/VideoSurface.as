﻿package org.osmf.media.videoClasses {
    import flash.events.*;
    import flash.display.*;
    import org.osmf.logging.*;
    import flash.net.*;
    import org.osmf.events.*;
    import flash.geom.*;
    import flash.media.*;

    public class VideoSurface extends Sprite {

        private static const logger:Logger = Log.getLogger("org.osmf.media.videoClasses.VideoSurface");

        static var videoSurfaceManager:VideoSurfaceManager = null;
        static var stageVideoInUseCount:int = 0;
        static var stageVideoCount:int = 0;

        var createVideo:Function;
        var stageVideo = null;
        var video:Video = null;
        private var currentVideoRenderer = null;
        private var netStream:NetStream;
        private var surfaceRect:Rectangle;
        private var invalidSurfaceRect:Boolean = false;
        private var surfaceShape:Shape = null;
        private var _deblocking:int = 0;
        private var _smoothing:Boolean = false;
        private var _visible:Boolean = true;
        private var renderStatus:String;

        public function VideoSurface(_arg1:Boolean=true, _arg2:Function=null){
            var useStageVideo:Boolean = _arg1;
            var createVideo = _arg2;
            this.surfaceRect = new Rectangle(0, 0, 0, 0);
            super();
            this.doubleClickEnabled = true;
            if (createVideo != null){
                this.createVideo = createVideo;
            } else {
                this.createVideo = function ():Video{
                    return (new Video());
                };
            };
            if (useStageVideo){
                this.register();
            } else {
                this.switchRenderer(this.createVideo());
            };
        }
        private static function updateRect(_arg1:Rectangle):Rectangle{
            var _local2:Rectangle = _arg1;
            if (isNaN(_local2.x)){
                _local2.x = 0;
            };
            if (isNaN(_local2.y)){
                _local2.y = 0;
            };
            if (isNaN(_local2.width)){
                _local2.width = 0;
            };
            if (isNaN(_local2.height)){
                _local2.height = 0;
            };
            return (_local2);
        }

        public function get info():VideoSurfaceInfo{
            return (new VideoSurfaceInfo(!((this.stageVideo == null)), this.renderStatus, stageVideoInUseCount, stageVideoCount));
        }
        public function attachNetStream(_arg1:NetStream):void{
            this.netStream = _arg1;
            if (this.currentVideoRenderer){
                this.currentVideoRenderer.attachNetStream(_arg1);
            };
        }
        public function clear(_arg1:Boolean=false):void{
            if (this.currentVideoRenderer){
                if (this.currentVideoRenderer == this.video){
                    this.video.clear();
                } else {
                    if (_arg1){
                        this.stageVideo.depth = 0;
                        this.stageVideo.viewPort = new Rectangle(0, 0, 0, 0);
                    };
                };
            };
        }
        public function get deblocking():int{
            return (this._deblocking);
        }
        public function set deblocking(_arg1:int):void{
            if (this._deblocking != _arg1){
                this._deblocking = _arg1;
                if ((this.currentVideoRenderer is Video)){
                    this.currentVideoRenderer.deblocking = this._deblocking;
                };
            };
        }
        public function get smoothing():Boolean{
            return (this._smoothing);
        }
        public function set smoothing(_arg1:Boolean):void{
            if (this._smoothing != _arg1){
                this._smoothing = _arg1;
                if ((this.currentVideoRenderer is Video)){
                    this.currentVideoRenderer.smoothing = this._smoothing;
                };
            };
        }
        override public function set visible(_arg1:Boolean):void{
            this._visible = _arg1;
            if (videoSurfaceManager){
                if (this._visible){
                    videoSurfaceManager.provideRenderer(this);
                } else {
                    videoSurfaceManager.releaseRenderer(this);
                };
            };
        }
        override public function get visible():Boolean{
            return (this._visible);
        }
        public function get videoHeight():int{
            return (((this.currentVideoRenderer) ? this.currentVideoRenderer.videoHeight : this.surfaceRect.height));
        }
        public function get videoWidth():int{
            return (((this.currentVideoRenderer) ? this.currentVideoRenderer.videoWidth : this.surfaceRect.width));
        }
        override public function set x(_arg1:Number):void{
            super.x = _arg1;
            this.surfaceRect.x = 0;
            this.updateView();
        }
        override public function set y(_arg1:Number):void{
            super.y = _arg1;
            this.surfaceRect.y = 0;
            this.updateView();
        }
        override public function get height():Number{
            return (this.surfaceRect.height);
        }
        override public function set height(_arg1:Number):void{
            if (this.surfaceRect.height != _arg1){
                this.surfaceRect.height = _arg1;
                this.updateView();
            };
        }
        override public function get width():Number{
            return (this.surfaceRect.width);
        }
        override public function set width(_arg1:Number):void{
            if (this.surfaceRect.width != _arg1){
                this.surfaceRect.width = _arg1;
                this.updateView();
            };
        }
        function updateView():void{
            var _local2:Rectangle;
            if (this.currentVideoRenderer == null){
                return;
            };
            var _local1:Rectangle = updateRect(this.surfaceRect);
            if (this.currentVideoRenderer == this.stageVideo){
                _local2 = new Rectangle();
                _local2.topLeft = localToGlobal(_local1.topLeft);
                _local2.bottomRight = localToGlobal(_local1.bottomRight);
                this.stageVideo.viewPort = _local2;
                if (this.surfaceShape == null){
                    this.surfaceShape = new Shape();
                };
                this.surfaceShape.graphics.clear();
                this.surfaceShape.graphics.drawRect(0, 0, _local1.width, _local1.height);
                this.surfaceShape.alpha = 0;
                addChild(this.surfaceShape);
            } else {
                this.currentVideoRenderer.x = _local1.x;
                this.currentVideoRenderer.y = _local1.y;
                this.currentVideoRenderer.height = _local1.height;
                this.currentVideoRenderer.width = _local1.width;
            };
        }
        function switchRenderer(_arg1):void{
            if (this.currentVideoRenderer == _arg1){
                logger.info("switchRenderer reusing the same renderer. Do nothing");
                return;
            };
            logger.info("switchRenderer. currentVideoRenderer = {0}; the new renderer = {1}", ((this.currentVideoRenderer)!=null) ? this.currentVideoRenderer.toString() : "null", _arg1);
            if (this.currentVideoRenderer){
                this.currentVideoRenderer.attachNetStream(null);
                if (this.currentVideoRenderer == this.video){
                    this.video = null;
                    removeChild(this.currentVideoRenderer);
                } else {
                    if (this.stageVideo != null){
                        this.stageVideo.viewPort = new Rectangle(0, 0, 0, 0);
                    };
                    this.stageVideo = null;
                    if (this.surfaceShape != null){
                        this.surfaceShape.graphics.clear();
                        removeChild(this.surfaceShape);
                        this.surfaceShape = null;
                    };
                };
            };
            this.currentVideoRenderer = _arg1;
            if (this.currentVideoRenderer){
                this.currentVideoRenderer.attachNetStream(this.netStream);
                if ((this.currentVideoRenderer is DisplayObject)){
                    this.video = this.currentVideoRenderer;
                    this.video.deblocking = this._deblocking;
                    this.video.smoothing = this._smoothing;
                    addChild(this.currentVideoRenderer);
                } else {
                    this.stageVideo = this.currentVideoRenderer;
                };
                this.updateView();
                this.currentVideoRenderer.addEventListener("renderState", this.onRenderState);
            };
            if ((((this.currentVideoRenderer == null)) || ((this.currentVideoRenderer is DisplayObject)))){
                dispatchEvent(new VideoSurfaceEvent(VideoSurfaceEvent.RENDER_CHANGE, true, false, false));
            } else {
                dispatchEvent(new VideoSurfaceEvent(VideoSurfaceEvent.RENDER_CHANGE, true, false, true));
            };
        }
        private function onRenderState(_arg1:Event):void{
            if (_arg1.hasOwnProperty("status")){
                this.renderStatus = _arg1["status"];
            };
        }
        private function register():void{
            if (videoSurfaceManager == null){
                videoSurfaceManager = new VideoSurfaceManager();
            };
            videoSurfaceManager.registerVideoSurface(this);
        }

    }
}//package org.osmf.media.videoClasses 
