﻿package org.osmf.media.videoClasses {
    import flash.events.*;
    import flash.display.*;
    import org.osmf.logging.*;
    import flash.utils.*;
    import org.osmf.media.videoClasses.*;
    import flash.media.*;

    class VideoSurfaceManager {

        private static const logger:Logger = Log.getLogger("org.osmf.media.videoClasses.VideoSurfaceManager");
        private static const AVAILABILITY:String = "availability";
        private static const AVAILABLE:String = "available";
        private static const UNAVAILABLE:String = "unavailable";

        var activeVideoSurfaces:Dictionary;
        private var _stage:Stage = null;
        private var stageVideoIsAvailable:Boolean = false;

        public function VideoSurfaceManager(){
            this.activeVideoSurfaces = new Dictionary(true);
            super();
        }
        public function registerVideoSurface(_arg1:VideoSurface):void{
            _arg1.addEventListener(Event.ADDED_TO_STAGE, this.onAddedToStage);
            _arg1.addEventListener(Event.REMOVED_FROM_STAGE, this.onRemovedFromStage);
        }
        public function get stageVideoInUseCount():int{
            var _local2:*;
            var _local1:int;
            for each (_local2 in this.activeVideoSurfaces) {
                if (((_local2) && ((_local2 is StageVideo)))){
                    _local1++;
                };
            };
            return (_local1);
        }
        public function get stageVideoCount():int{
            return (((this._stage) ? this._stage.stageVideos.length : 0));
        }
        function registerStage(_arg1:Stage):void{
            this._stage = _arg1;
            this._stage.addEventListener("stageVideoAvailability", this.onStageVideoAvailability);
            this.stageVideoIsAvailable = ((this._stage.hasOwnProperty("stageVideos")) && ((this._stage.stageVideos.length > 0)));
        }
        function provideRenderer(_arg1:VideoSurface):void{
            if (_arg1 == null){
                return;
            };
            this.switchRenderer(_arg1);
        }
        function releaseRenderer(_arg1:VideoSurface):void{
            _arg1.clear(true);
            this.activeVideoSurfaces[_arg1] = null;
            _arg1.switchRenderer(null);
        }
        private function onStageVideoAvailability(_arg1:Event):void{
            var _local2:Boolean;
            var _local3:*;
            var _local4:VideoSurface;
            if (!_arg1.hasOwnProperty(AVAILABILITY)){
                logger.warn("stageVideoAvailability event received. No {0} property", AVAILABILITY);
                return;
            };
            _local2 = (_arg1[AVAILABILITY] == AVAILABLE);
            if (this.stageVideoIsAvailable != _local2){
                logger.info("stageVideoAvailability changed. Previous value = {0}; Current value = {1}", this.stageVideoIsAvailable, _local2);
                this.stageVideoIsAvailable = _local2;
                for (_local3 in this.activeVideoSurfaces) {
                    _local4 = (_local3 as VideoSurface);
                    if (((!((_local4 == null))) && (!((_local4.info.stageVideoInUse == this.stageVideoIsAvailable))))){
                        this.switchRenderer(_local4);
                    };
                };
            };
        }
        private function onAddedToStage(_arg1:Event):void{
            if (this._stage == null){
                this.registerStage(_arg1.target.stage);
            };
            this.provideRenderer((_arg1.target as VideoSurface));
        }
        private function onRemovedFromStage(_arg1:Event):void{
            this.releaseRenderer((_arg1.target as VideoSurface));
        }
        private function onStageVideoRenderState(_arg1:Event):void{
            var _local2:*;
            var _local3:VideoSurface;
            if (_arg1["status"] == UNAVAILABLE){
                for (_local2 in this.activeVideoSurfaces) {
                    _local3 = (_local2 as VideoSurface);
                    if (_arg1.target == _local3.stageVideo){
                        _local3.stageVideo = null;
                        this.switchRenderer(_local3);
                        break;
                    };
                };
            };
        }
        private function switchRenderer(_arg1:VideoSurface):void{
            var _local2:*;
            var _local3:StageVideo;
            var _local4:int;
            var _local5:*;
            var _local6:int;
            var _local7:int;
            if (!this.stageVideoIsAvailable){
                if (_arg1.video == null){
                    _arg1.video = _arg1.createVideo();
                };
                _local2 = _arg1.video;
            } else {
                _local3 = null;
                _local4 = 0;
                while (_local4 < this._stage.stageVideos.length) {
                    _local3 = this._stage.stageVideos[_local4];
                    for (_local5 in this.activeVideoSurfaces) {
                        if (_local3 == this.activeVideoSurfaces[_local5]){
                            _local3 = null;
                        };
                    };
                    if (_local3 != null){
                        break;
                    };
                    _local4++;
                };
                if (_local3 != null){
                    _local6 = 0;
                    _local7 = 0;
                    while (_local7 < this._stage.stageVideos.length) {
                        if (_local6 < this._stage.stageVideos[_local7].depth){
                            _local6 = this._stage.stageVideos[_local7].depth;
                        };
                        _local7++;
                    };
                    this.activeVideoSurfaces[_arg1] = _local3;
                    _arg1.stageVideo = _local3;
                    _local2 = _local3;
                    _local3.depth = (_local6 + 1);
                    _local2.addEventListener("renderState", this.onStageVideoRenderState);
                } else {
                    if (_arg1.video == null){
                        _arg1.video = _arg1.createVideo();
                    };
                    _local2 = _arg1.video;
                };
            };
            this.activeVideoSurfaces[_arg1] = _local2;
            _arg1.switchRenderer(_local2);
            VideoSurface.stageVideoCount = this.stageVideoCount;
            VideoSurface.stageVideoInUseCount = this.stageVideoInUseCount;
        }

    }
}//package org.osmf.media.videoClasses 
