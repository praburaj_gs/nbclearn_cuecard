﻿package org.osmf.media.videoClasses {

    public final class VideoSurfaceInfo {

        public static const UNAVAILABLE:String = "unavailable";
        public static const SOFTWARE:String = "software";
        public static const ACCELERATED:String = "accelerated";

        protected var _stageVideoInUse:Boolean;
        protected var _renderStatus:String;
        protected var _stageVideoInUseCount:int;
        protected var _stageVideoCount:int;

        public function VideoSurfaceInfo(_arg1:Boolean, _arg2:String, _arg3:int, _arg4:int){
            this._stageVideoInUse = _arg1;
            this._renderStatus = _arg2;
            this._stageVideoInUseCount = _arg3;
            this._stageVideoCount = _arg4;
        }
        public function get stageVideoInUse():Boolean{
            return (this._stageVideoInUse);
        }
        public function get renderStatus():String{
            return (this._renderStatus);
        }
        public function get stageVideoInUseCount():int{
            return (this._stageVideoInUseCount);
        }
        public function get stageVideoCount():int{
            return (this._stageVideoCount);
        }

    }
}//package org.osmf.media.videoClasses 
