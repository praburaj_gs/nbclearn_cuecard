﻿package org.osmf.media {

    public class URLResource extends MediaResourceBase {

        private var _url:String;

        public function URLResource(_arg1:String){
            this._url = _arg1;
        }
        public function get url():String{
            return (this._url);
        }

    }
}//package org.osmf.media 
