﻿package org.osmf.media {
    import flash.events.*;
    import org.osmf.traits.*;
    import org.osmf.utils.*;

    class MediaTraitResolver extends EventDispatcher {

        private var _type:String;
        private var _resolvedTrait:MediaTraitBase;

        public function MediaTraitResolver(_arg1:String){
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            this._type = _arg1;
        }
        final public function get type():String{
            return (this._type);
        }
        final protected function setResolvedTrait(_arg1:MediaTraitBase):void{
            if (_arg1 != this._resolvedTrait){
                if (this._resolvedTrait){
                    this._resolvedTrait = null;
                    dispatchEvent(new Event(Event.CHANGE));
                };
                this._resolvedTrait = _arg1;
                dispatchEvent(new Event(Event.CHANGE));
            };
        }
        final public function get resolvedTrait():MediaTraitBase{
            return (this._resolvedTrait);
        }
        final public function addTrait(_arg1:MediaTraitBase):void{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            if (_arg1.traitType != this.type){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            this.processAddTrait(_arg1);
        }
        final public function removeTrait(_arg1:MediaTraitBase):MediaTraitBase{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            if (_arg1.traitType != this.type){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            return (this.processRemoveTrait(_arg1));
        }
        protected function processAddTrait(_arg1:MediaTraitBase):void{
        }
        protected function processRemoveTrait(_arg1:MediaTraitBase):MediaTraitBase{
            return (null);
        }

    }
}//package org.osmf.media 
