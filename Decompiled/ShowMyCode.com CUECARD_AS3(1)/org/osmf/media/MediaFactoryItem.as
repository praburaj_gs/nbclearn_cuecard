﻿package org.osmf.media {
    import org.osmf.utils.*;

    public class MediaFactoryItem {

        private var _id:String;
        private var _canHandleResourceFunction:Function;
        private var _mediaElementCreationFunction:Function;
        private var _type:String;

        public function MediaFactoryItem(_arg1:String, _arg2:Function, _arg3:Function, _arg4:String=null){
            if ((((((_arg1 == null)) || ((_arg2 == null)))) || ((_arg3 == null)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            _arg4 = ((_arg4) || (MediaFactoryItemType.STANDARD));
            this._id = _arg1;
            this._canHandleResourceFunction = _arg2;
            this._mediaElementCreationFunction = _arg3;
            this._type = _arg4;
        }
        public function get id():String{
            return (this._id);
        }
        public function get canHandleResourceFunction():Function{
            return (this._canHandleResourceFunction);
        }
        public function get mediaElementCreationFunction():Function{
            return (this._mediaElementCreationFunction);
        }
        public function get type():String{
            return (this._type);
        }

    }
}//package org.osmf.media 
