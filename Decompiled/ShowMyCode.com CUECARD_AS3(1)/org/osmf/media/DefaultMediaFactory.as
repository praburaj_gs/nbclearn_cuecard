﻿package org.osmf.media {
    import org.osmf.net.*;
    import org.osmf.net.httpstreaming.*;
    import org.osmf.net.dvr.*;
    import org.osmf.net.rtmpstreaming.*;
    import org.osmf.elements.*;

    public class DefaultMediaFactory extends MediaFactory {

        private var rtmpStreamingNetLoader:RTMPDynamicStreamingNetLoader;
        private var f4mLoader:F4MLoader;
        private var dvrCastLoader:DVRCastNetLoader;
        private var netLoader:NetLoader;
        private var imageLoader:ImageLoader;
        private var swfLoader:SWFLoader;
        private var soundLoader:SoundLoader;
        private var httpStreamingNetLoader:HTTPStreamingNetLoader;
        private var multicastLoader:MulticastNetLoader;

        public function DefaultMediaFactory(){
            this.init();
        }
        private function init():void{
            this.f4mLoader = new F4MLoader(this);
            addItem(new MediaFactoryItem("org.osmf.elements.f4m", this.f4mLoader.canHandleResource, function ():MediaElement{
                return (new F4MElement(null, f4mLoader));
            }));
            this.dvrCastLoader = new DVRCastNetLoader();
            addItem(new MediaFactoryItem("org.osmf.elements.video.dvr.dvrcast", this.dvrCastLoader.canHandleResource, function ():MediaElement{
                return (new VideoElement(null, dvrCastLoader));
            }));
            this.httpStreamingNetLoader = new HTTPStreamingNetLoader();
            addItem(new MediaFactoryItem("org.osmf.elements.video.httpstreaming", this.httpStreamingNetLoader.canHandleResource, function ():MediaElement{
                return (new VideoElement(null, httpStreamingNetLoader));
            }));
            this.multicastLoader = new MulticastNetLoader();
            addItem(new MediaFactoryItem("org.osmf.elements.video.rtmfp.multicast", this.multicastLoader.canHandleResource, function ():MediaElement{
                return (new VideoElement(null, multicastLoader));
            }));
            this.rtmpStreamingNetLoader = new RTMPDynamicStreamingNetLoader();
            addItem(new MediaFactoryItem("org.osmf.elements.video.rtmpdynamicStreaming", this.rtmpStreamingNetLoader.canHandleResource, function ():MediaElement{
                return (new VideoElement(null, rtmpStreamingNetLoader));
            }));
            this.netLoader = new NetLoader();
            addItem(new MediaFactoryItem("org.osmf.elements.video", this.netLoader.canHandleResource, function ():MediaElement{
                return (new VideoElement(null, netLoader));
            }));
            this.soundLoader = new SoundLoader();
            addItem(new MediaFactoryItem("org.osmf.elements.audio", this.soundLoader.canHandleResource, function ():MediaElement{
                return (new AudioElement(null, soundLoader));
            }));
            addItem(new MediaFactoryItem("org.osmf.elements.audio.streaming", this.netLoader.canHandleResource, function ():MediaElement{
                return (new AudioElement(null, netLoader));
            }));
            this.imageLoader = new ImageLoader();
            addItem(new MediaFactoryItem("org.osmf.elements.image", this.imageLoader.canHandleResource, function ():MediaElement{
                return (new ImageElement(null, imageLoader));
            }));
            this.swfLoader = new SWFLoader();
            addItem(new MediaFactoryItem("org.osmf.elements.swf", this.swfLoader.canHandleResource, function ():MediaElement{
                return (new SWFElement(null, swfLoader));
            }));
        }

    }
}//package org.osmf.media 
