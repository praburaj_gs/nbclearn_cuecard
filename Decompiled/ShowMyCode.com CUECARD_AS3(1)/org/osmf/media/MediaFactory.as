﻿package org.osmf.media {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.events.*;
    import flash.utils.*;
    import org.osmf.elements.*;
    import org.osmf.utils.*;
    import org.osmf.media.pluginClasses.*;

    public class MediaFactory extends EventDispatcher {

        private var pluginManager:PluginManager;
        private var allItems:Dictionary;

        public function MediaFactory(){
            this.allItems = new Dictionary();
        }
        private static function getItemsByResource(_arg1:MediaResourceBase, _arg2:Vector.<MediaFactoryItem>):Vector.<MediaFactoryItem>{
            var _local4:MediaFactoryItem;
            var _local3:Vector.<MediaFactoryItem> = new Vector.<MediaFactoryItem>();
            for each (_local4 in _arg2) {
                if (_local4.canHandleResourceFunction(_arg1)){
                    _local3.push(_local4);
                };
            };
            return (_local3);
        }
        private static function getIndexOfItem(_arg1:String, _arg2:Vector.<MediaFactoryItem>):int{
            var _local4:MediaFactoryItem;
            var _local3:int;
            while (_local3 < _arg2.length) {
                _local4 = (_arg2[_local3] as MediaFactoryItem);
                if (_local4.id == _arg1){
                    return (_local3);
                };
                _local3++;
            };
            return (-1);
        }

        public function addItem(_arg1:MediaFactoryItem):void{
            if ((((_arg1 == null)) || ((_arg1.id == null)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            var _local2:Vector.<MediaFactoryItem> = this.findOrCreateItems(_arg1.type);
            var _local3:int = getIndexOfItem(_arg1.id, _local2);
            if (_local3 != -1){
                _local2[_local3] = _arg1;
            } else {
                _local2.push(_arg1);
            };
        }
        public function removeItem(_arg1:MediaFactoryItem):void{
            var _local3:int;
            if ((((_arg1 == null)) || ((_arg1.id == null)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            var _local2:Vector.<MediaFactoryItem> = this.allItems[_arg1.type];
            if (_local2 != null){
                _local3 = _local2.indexOf(_arg1);
                if (_local3 != -1){
                    _local2.splice(_local3, 1);
                };
            };
        }
        public function get numItems():int{
            var _local2:String;
            var _local3:Vector.<MediaFactoryItem>;
            var _local1:int;
            for each (_local2 in MediaFactoryItemType.ALL_TYPES) {
                _local3 = this.allItems[_local2];
                if (_local3 != null){
                    _local1 = (_local1 + _local3.length);
                };
            };
            return (_local1);
        }
        public function getItemAt(_arg1:int):MediaFactoryItem{
            var _local3:String;
            var _local4:Vector.<MediaFactoryItem>;
            var _local2:MediaFactoryItem;
            if (_arg1 >= 0){
                for each (_local3 in MediaFactoryItemType.ALL_TYPES) {
                    _local4 = this.allItems[_local3];
                    if (_local4 != null){
                        if (_arg1 < _local4.length){
                            _local2 = _local4[_arg1];
                            break;
                        };
                        _arg1 = (_arg1 - _local4.length);
                    };
                };
            };
            return (_local2);
        }
        public function getItemById(_arg1:String):MediaFactoryItem{
            var _local3:String;
            var _local4:Vector.<MediaFactoryItem>;
            var _local5:int;
            var _local2:MediaFactoryItem;
            for each (_local3 in MediaFactoryItemType.ALL_TYPES) {
                _local4 = this.allItems[_local3];
                if (_local4 != null){
                    _local5 = getIndexOfItem(_arg1, _local4);
                    if (_local5 != -1){
                        _local2 = _local4[_local5];
                        break;
                    };
                };
            };
            return (_local2);
        }
        public function loadPlugin(_arg1:MediaResourceBase):void{
            this.createPluginManager();
            this.pluginManager.loadPlugin(_arg1);
        }
        public function createMediaElement(_arg1:MediaResourceBase):MediaElement{
            var _local3:MediaElement;
            this.createPluginManager();
            var _local2:MediaElement = this.createMediaElementByResource(_arg1, MediaFactoryItemType.STANDARD);
            if (_local2 != null){
                _local3 = this.createMediaElementByResource(_local2.resource, MediaFactoryItemType.PROXY, _local2);
                _local2 = ((_local3)!=null) ? _local3 : _local2;
                dispatchEvent(new MediaFactoryEvent(MediaFactoryEvent.MEDIA_ELEMENT_CREATE, false, false, null, _local2));
            };
            return (_local2);
        }
        protected function resolveItems(_arg1:MediaResourceBase, _arg2:Vector.<MediaFactoryItem>):MediaFactoryItem{
            var _local5:MediaFactoryItem;
            if ((((_arg1 == null)) || ((_arg2 == null)))){
                return (null);
            };
            var _local3:MediaFactoryItem;
            var _local4:int;
            while (_local4 < _arg2.length) {
                _local5 = (_arg2[_local4] as MediaFactoryItem);
                if (_local5.id.indexOf("org.osmf") == -1){
                    return (_local5);
                };
                if (_local3 == null){
                    _local3 = _local5;
                };
                _local4++;
            };
            return (_local3);
        }
        private function findOrCreateItems(_arg1:String):Vector.<MediaFactoryItem>{
            if (this.allItems[_arg1] == null){
                this.allItems[_arg1] = new Vector.<MediaFactoryItem>();
            };
            return ((this.allItems[_arg1] as Vector.<MediaFactoryItem>));
        }
        private function createMediaElementByResource(_arg1:MediaResourceBase, _arg2:String, _arg3:MediaElement=null):MediaElement{
            var _local6:MediaFactoryItem;
            var _local7:MediaElement;
            var _local8:int;
            var _local9:MediaFactoryItem;
            var _local10:ProxyElement;
            var _local4:MediaElement;
            var _local5:Vector.<MediaFactoryItem> = getItemsByResource(_arg1, this.allItems[_arg2]);
            if (_arg2 == MediaFactoryItemType.STANDARD){
                _local6 = (this.resolveItems(_arg1, _local5) as MediaFactoryItem);
                if (_local6 != null){
                    _local4 = this.invokeMediaElementCreationFunction(_local6);
                };
            } else {
                if (_arg2 == MediaFactoryItemType.PROXY){
                    _local7 = _arg3;
                    _local8 = _local5.length;
                    while (_local8 > 0) {
                        _local9 = (_local5[(_local8 - 1)] as MediaFactoryItem);
                        _local10 = (this.invokeMediaElementCreationFunction(_local9) as ProxyElement);
                        if (_local10 != null){
                            _local10.proxiedElement = _local7;
                            _local7 = _local10;
                        };
                        _local8--;
                    };
                    _local4 = _local7;
                };
            };
            if (_local4 != null){
                _local4.resource = _arg1;
            };
            return (_local4);
        }
        private function onPluginLoad(_arg1:PluginManagerEvent):void{
            dispatchEvent(new MediaFactoryEvent(MediaFactoryEvent.PLUGIN_LOAD, false, false, _arg1.resource));
        }
        private function onPluginLoadError(_arg1:PluginManagerEvent):void{
            dispatchEvent(new MediaFactoryEvent(MediaFactoryEvent.PLUGIN_LOAD_ERROR, false, false, _arg1.resource));
        }
        private function invokeMediaElementCreationFunction(_arg1:MediaFactoryItem):MediaElement{
            var _local2:MediaElement;
            try {
                _local2 = _arg1.mediaElementCreationFunction();
            } catch(error:Error) {
            };
            return (_local2);
        }
        private function createPluginManager():void{
            if (this.pluginManager == null){
                this.pluginManager = new PluginManager(this);
                this.pluginManager.addEventListener(PluginManagerEvent.PLUGIN_LOAD, this.onPluginLoad);
                this.pluginManager.addEventListener(PluginManagerEvent.PLUGIN_LOAD_ERROR, this.onPluginLoadError);
            };
        }

    }
}//package org.osmf.media 
