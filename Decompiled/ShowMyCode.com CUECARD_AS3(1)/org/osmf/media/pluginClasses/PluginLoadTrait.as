﻿package org.osmf.media.pluginClasses {
    import flash.display.*;
    import org.osmf.media.*;
    import org.osmf.traits.*;

    class PluginLoadTrait extends LoadTrait {

        private var _pluginInfo:PluginInfo;
        private var _loader:Loader;

        public function PluginLoadTrait(_arg1:LoaderBase, _arg2:MediaResourceBase){
            super(_arg1, _arg2);
        }
        public function get pluginInfo():PluginInfo{
            return (this._pluginInfo);
        }
        public function set pluginInfo(_arg1:PluginInfo):void{
            this._pluginInfo = _arg1;
        }
        public function get loader():Loader{
            return (this._loader);
        }
        public function set loader(_arg1:Loader):void{
            this._loader = _arg1;
        }

    }
}//package org.osmf.media.pluginClasses 
