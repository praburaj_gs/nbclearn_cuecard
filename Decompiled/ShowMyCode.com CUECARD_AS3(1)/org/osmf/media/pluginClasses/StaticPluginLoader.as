﻿package org.osmf.media.pluginClasses {
    import org.osmf.media.*;
    import org.osmf.traits.*;

    class StaticPluginLoader extends PluginLoader {

        public function StaticPluginLoader(_arg1:MediaFactory, _arg2:String){
            super(_arg1, _arg2);
        }
        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            return ((_arg1 is PluginInfoResource));
        }
        override protected function executeLoad(_arg1:LoadTrait):void{
            updateLoadTrait(_arg1, LoadState.LOADING);
            var _local2:PluginInfoResource = (_arg1.resource as PluginInfoResource);
            var _local3:PluginInfo = _local2.pluginInfo;
            loadFromPluginInfo(_arg1, _local3);
        }
        override protected function executeUnload(_arg1:LoadTrait):void{
            var _local2:PluginLoadTrait = (_arg1 as PluginLoadTrait);
            var _local3:PluginInfo = _local2.pluginInfo;
            updateLoadTrait(_arg1, LoadState.UNLOADING);
            unloadFromPluginInfo(_local3);
            updateLoadTrait(_arg1, LoadState.UNINITIALIZED);
        }

    }
}//package org.osmf.media.pluginClasses 
