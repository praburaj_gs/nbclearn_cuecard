﻿package org.osmf.media.pluginClasses {
    import org.osmf.media.*;

    class PluginEntry {

        private var _pluginElement:MediaElement;
        private var _state:PluginLoadingState;

        public function PluginEntry(_arg1:MediaElement, _arg2:PluginLoadingState){
            this._pluginElement = _arg1;
            this._state = _arg2;
        }
        public function get pluginElement():MediaElement{
            return (this._pluginElement);
        }
        public function get state():PluginLoadingState{
            return (this._state);
        }
        public function set state(_arg1:PluginLoadingState):void{
            this._state = _arg1;
        }

    }
}//package org.osmf.media.pluginClasses 
