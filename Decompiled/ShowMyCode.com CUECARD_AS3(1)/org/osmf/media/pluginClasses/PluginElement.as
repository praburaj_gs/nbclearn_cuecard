﻿package org.osmf.media.pluginClasses {
    import org.osmf.media.*;
    import org.osmf.traits.*;

    class PluginElement extends LoadableElementBase {

        public function PluginElement(_arg1:PluginLoader, _arg2:MediaResourceBase=null){
            super(_arg2, _arg1);
        }
        override protected function createLoadTrait(_arg1:MediaResourceBase, _arg2:LoaderBase):LoadTrait{
            return (new PluginLoadTrait(_arg2, _arg1));
        }

    }
}//package org.osmf.media.pluginClasses 
