﻿package org.osmf.media.pluginClasses {
    import flash.display.*;
    import org.osmf.media.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import org.osmf.utils.*;
    import org.osmf.media.pluginClasses.*;

    class PluginLoader extends LoaderBase {

        private static const FRAMEWORK_VERSION_PROPERTY_NAME:String = "frameworkVersion";
        private static const IS_FRAMEWORK_VERSION_SUPPORTED_PROPERTY_NAME:String = "isFrameworkVersionSupported";

        private var minimumSupportedFrameworkVersion:String;
        private var mediaFactory:MediaFactory;

        public function PluginLoader(_arg1:MediaFactory, _arg2:String){
            this.mediaFactory = _arg1;
            this.minimumSupportedFrameworkVersion = _arg2;
        }
        protected function unloadFromPluginInfo(_arg1:PluginInfo):void{
            var _local2:int;
            var _local3:MediaFactoryItem;
            var _local4:MediaFactoryItem;
            if (_arg1 != null){
                _local2 = 0;
                while (_local2 < _arg1.numMediaFactoryItems) {
                    _local3 = _arg1.getMediaFactoryItemAt(_local2);
                    _local4 = this.mediaFactory.getItemById(_local3.id);
                    if (_local4 != null){
                        this.mediaFactory.removeItem(_local4);
                    };
                    _local2++;
                };
            };
        }
        protected function loadFromPluginInfo(_arg1:LoadTrait, _arg2:PluginInfo, _arg3:Loader=null):void{
            var passedMediaFactory:* = null;
            var i:* = 0;
            var pluginLoadTrait:* = null;
            var item:* = null;
            var loadTrait:* = _arg1;
            var pluginInfo:* = _arg2;
            var loader = _arg3;
            var invalidImplementation:* = false;
            if (pluginInfo != null){
                if (this.isPluginCompatible(pluginInfo)){
                    try {
                        passedMediaFactory = (loadTrait.resource.getMetadataValue(PluginInfo.PLUGIN_MEDIAFACTORY_NAMESPACE) as MediaFactory);
                        if (passedMediaFactory == null){
                            loadTrait.resource.addMetadataValue(PluginInfo.PLUGIN_MEDIAFACTORY_NAMESPACE, this.mediaFactory);
                        };
                        pluginInfo.initializePlugin(loadTrait.resource);
                        i = 0;
                        while (i < pluginInfo.numMediaFactoryItems) {
                            item = pluginInfo.getMediaFactoryItemAt(i);
                            if (item == null){
                                throw (new RangeError());
                            };
                            this.mediaFactory.addItem(item);
                            i = (i + 1);
                        };
                        pluginLoadTrait = (loadTrait as PluginLoadTrait);
                        pluginLoadTrait.pluginInfo = pluginInfo;
                        pluginLoadTrait.loader = loader;
                        updateLoadTrait(pluginLoadTrait, LoadState.READY);
                    } catch(error:RangeError) {
                        invalidImplementation = true;
                    };
                } else {
                    updateLoadTrait(loadTrait, LoadState.LOAD_ERROR);
                    loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.PLUGIN_VERSION_INVALID)));
                };
            } else {
                invalidImplementation = true;
            };
            if (invalidImplementation){
                updateLoadTrait(loadTrait, LoadState.LOAD_ERROR);
                loadTrait.dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.PLUGIN_IMPLEMENTATION_INVALID)));
            };
        }
        protected function isPluginCompatible(_arg1:Object):Boolean{
            var _local5:Function;
            var _local2:Boolean;
            var _local3:String = ((_arg1.hasOwnProperty(FRAMEWORK_VERSION_PROPERTY_NAME)) ? _arg1[FRAMEWORK_VERSION_PROPERTY_NAME] : null);
            var _local4:Boolean = this.isPluginVersionSupported(_local3);
            if (_local4){
                _local5 = ((_arg1.hasOwnProperty(IS_FRAMEWORK_VERSION_SUPPORTED_PROPERTY_NAME)) ? (_arg1[IS_FRAMEWORK_VERSION_SUPPORTED_PROPERTY_NAME] as Function) : null);
                if (_local5 != null){
                    try {
                        _local2 = _local5(Version.version);
                    } catch(error:Error) {
                    };
                };
            };
            return (_local2);
        }
        private function isPluginVersionSupported(_arg1:String):Boolean{
            if ((((_arg1 == null)) || ((_arg1.length == 0)))){
                return (false);
            };
            var _local2:Object = VersionUtils.parseVersionString(this.minimumSupportedFrameworkVersion);
            var _local3:Object = VersionUtils.parseVersionString(_arg1);
            return ((((_local3.major > _local2.major)) || ((((_local3.major == _local2.major)) && ((_local3.minor >= _local2.minor))))));
        }

    }
}//package org.osmf.media.pluginClasses 
