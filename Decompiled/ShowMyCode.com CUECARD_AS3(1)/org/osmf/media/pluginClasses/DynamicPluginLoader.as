﻿package org.osmf.media.pluginClasses {
    import flash.display.*;
    import org.osmf.media.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import org.osmf.elements.*;
    import org.osmf.elements.loaderClasses.*;
    import org.osmf.media.pluginClasses.*;

    class DynamicPluginLoader extends PluginLoader {

        private static const PLUGININFO_PROPERTY_NAME:String = "pluginInfo";

        public function DynamicPluginLoader(_arg1:MediaFactory, _arg2:String){
            super(_arg1, _arg2);
        }
        override public function canHandleResource(_arg1:MediaResourceBase):Boolean{
            return (new SWFLoader().canHandleResource(_arg1));
        }
        override protected function executeLoad(_arg1:LoadTrait):void{
            var swfLoader:* = null;
            var loaderLoadTrait:* = null;
            var onSWFLoaderStateChange:* = null;
            var onLoadError:* = null;
            var loadTrait:* = _arg1;
            onSWFLoaderStateChange = function (_arg1:LoaderEvent):void{
                var _local2:DisplayObject;
                var _local3:PluginInfo;
                if (_arg1.newState == LoadState.READY){
                    swfLoader.removeEventListener(LoaderEvent.LOAD_STATE_CHANGE, onSWFLoaderStateChange);
                    loaderLoadTrait.removeEventListener(MediaErrorEvent.MEDIA_ERROR, onLoadError);
                    _local2 = loaderLoadTrait.loader.content;
                    _local3 = (_local2[PLUGININFO_PROPERTY_NAME] as PluginInfo);
                    loadFromPluginInfo(loadTrait, _local3, loaderLoadTrait.loader);
                } else {
                    if (_arg1.newState == LoadState.LOAD_ERROR){
                        swfLoader.removeEventListener(LoaderEvent.LOAD_STATE_CHANGE, onSWFLoaderStateChange);
                        updateLoadTrait(loadTrait, _arg1.newState);
                    };
                };
            };
            onLoadError = function (_arg1:MediaErrorEvent):void{
                loaderLoadTrait.removeEventListener(MediaErrorEvent.MEDIA_ERROR, onLoadError);
                loadTrait.dispatchEvent(_arg1.clone());
            };
            updateLoadTrait(loadTrait, LoadState.LOADING);
            swfLoader = new SWFLoader(true);
            swfLoader.validateLoadedContentFunction = this.validateLoadedContent;
            swfLoader.addEventListener(LoaderEvent.LOAD_STATE_CHANGE, onSWFLoaderStateChange);
            loaderLoadTrait = new LoaderLoadTrait(swfLoader, loadTrait.resource);
            loaderLoadTrait.addEventListener(MediaErrorEvent.MEDIA_ERROR, onLoadError);
            swfLoader.load(loaderLoadTrait);
        }
        override protected function executeUnload(_arg1:LoadTrait):void{
            updateLoadTrait(_arg1, LoadState.UNLOADING);
            var _local2:PluginLoadTrait = (_arg1 as PluginLoadTrait);
            unloadFromPluginInfo(_local2.pluginInfo);
            _local2.loader.unloadAndStop();
            updateLoadTrait(_arg1, LoadState.UNINITIALIZED);
        }
        private function validateLoadedContent(_arg1:DisplayObject):Boolean{
            var _local2:Object = ((_arg1.hasOwnProperty(PLUGININFO_PROPERTY_NAME)) ? _arg1[PLUGININFO_PROPERTY_NAME] : null);
            return (((_local2)!=null) ? isPluginCompatible(_local2) : false);
        }

    }
}//package org.osmf.media.pluginClasses 
