﻿package org.osmf.media.pluginClasses {

    class PluginLoadingState {

        public static const LOADING:PluginLoadingState = new PluginLoadingState("Loading");
;
        public static const LOADED:PluginLoadingState = new PluginLoadingState("Loaded");
;

        private var _state:String;

        public function PluginLoadingState(_arg1:String){
            this._state = _arg1;
        }
        public function get state():String{
            return (this._state);
        }

    }
}//package org.osmf.media.pluginClasses 
