﻿package org.osmf.media.pluginClasses {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.media.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.utils.*;

    public class PluginManager extends EventDispatcher {

        private static const STATIC_PLUGIN_MEDIA_INFO_ID:String = "org.osmf.plugins.StaticPluginLoader";
        private static const DYNAMIC_PLUGIN_MEDIA_INFO_ID:String = "org.osmf.plugins.DynamicPluginLoader";

        private var _mediaFactory:MediaFactory;
        private var _pluginFactory:MediaFactory;
        private var _pluginMap:Dictionary;
        private var _pluginList:Vector.<PluginEntry>;
        private var notificationFunctions:Vector.<Function>;
        private var createdElements:Dictionary;
        private var minimumSupportedFrameworkVersion:String;
        private var staticPluginLoader:StaticPluginLoader;
        private var dynamicPluginLoader:DynamicPluginLoader;

        public function PluginManager(_arg1:MediaFactory){
            this._mediaFactory = _arg1;
            this._mediaFactory.addEventListener(MediaFactoryEvent.MEDIA_ELEMENT_CREATE, this.onMediaElementCreate);
            this.minimumSupportedFrameworkVersion = Version.lastAPICompatibleVersion;
            this.initPluginFactory();
            this._pluginMap = new Dictionary();
            this._pluginList = new Vector.<PluginEntry>();
        }
        public function loadPlugin(_arg1:MediaResourceBase):void{
            var identifier:* = null;
            var pluginEntry:* = null;
            var onLoadStateChange:* = null;
            var onMediaError:* = null;
            var pluginElement:* = null;
            var loadTrait:* = null;
            var resource:* = _arg1;
            onLoadStateChange = function (_arg1:LoadEvent):void{
                var _local2:PluginLoadTrait;
                if (_arg1.loadState == LoadState.READY){
                    pluginEntry.state = PluginLoadingState.LOADED;
                    _pluginList.push(pluginEntry);
                    _local2 = (pluginElement.getTrait(MediaTraitType.LOAD) as PluginLoadTrait);
                    if (_local2.pluginInfo.mediaElementCreationNotificationFunction != null){
                        invokeMediaElementCreationNotificationForCreatedMediaElements(_local2.pluginInfo.mediaElementCreationNotificationFunction);
                        if (notificationFunctions == null){
                            notificationFunctions = new Vector.<Function>();
                        };
                        notificationFunctions.push(_local2.pluginInfo.mediaElementCreationNotificationFunction);
                    };
                    dispatchEvent(new PluginManagerEvent(PluginManagerEvent.PLUGIN_LOAD, false, false, resource));
                } else {
                    if (_arg1.loadState == LoadState.LOAD_ERROR){
                        delete _pluginMap[identifier];
                        dispatchEvent(new PluginManagerEvent(PluginManagerEvent.PLUGIN_LOAD_ERROR, false, false, resource));
                    };
                };
            };
            onMediaError = function (_arg1:MediaErrorEvent):void{
                dispatchEvent(_arg1.clone());
            };
            if (resource == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            identifier = this.getPluginIdentifier(resource);
            pluginEntry = (this._pluginMap[identifier] as PluginEntry);
            if (pluginEntry != null){
                dispatchEvent(new PluginManagerEvent(PluginManagerEvent.PLUGIN_LOAD, false, false, resource));
            } else {
                pluginElement = this._pluginFactory.createMediaElement(resource);
                if (pluginElement != null){
                    pluginEntry = new PluginEntry(pluginElement, PluginLoadingState.LOADING);
                    this._pluginMap[identifier] = pluginEntry;
                    loadTrait = (pluginElement.getTrait(MediaTraitType.LOAD) as LoadTrait);
                    if (loadTrait != null){
                        loadTrait.addEventListener(LoadEvent.LOAD_STATE_CHANGE, onLoadStateChange);
                        loadTrait.addEventListener(MediaErrorEvent.MEDIA_ERROR, onMediaError);
                        loadTrait.load();
                    } else {
                        dispatchEvent(new PluginManagerEvent(PluginManagerEvent.PLUGIN_LOAD_ERROR, false, false, resource));
                    };
                } else {
                    dispatchEvent(new PluginManagerEvent(PluginManagerEvent.PLUGIN_LOAD_ERROR, false, false, resource));
                };
            };
        }
        public function get mediaFactory():MediaFactory{
            return (this._mediaFactory);
        }
        private function getPluginIdentifier(_arg1:MediaResourceBase):Object{
            var _local2:Object;
            if ((_arg1 is URLResource)){
                _local2 = (_arg1 as URLResource).url;
            } else {
                if ((_arg1 is PluginInfoResource)){
                    _local2 = (_arg1 as PluginInfoResource).pluginInfo;
                };
            };
            return (_local2);
        }
        private function initPluginFactory():void{
            this._pluginFactory = new MediaFactory();
            this.staticPluginLoader = new StaticPluginLoader(this.mediaFactory, this.minimumSupportedFrameworkVersion);
            this.dynamicPluginLoader = new DynamicPluginLoader(this.mediaFactory, this.minimumSupportedFrameworkVersion);
            var _local1:MediaFactoryItem = new MediaFactoryItem(STATIC_PLUGIN_MEDIA_INFO_ID, this.staticPluginLoader.canHandleResource, this.createStaticPluginElement);
            this._pluginFactory.addItem(_local1);
            var _local2:MediaFactoryItem = new MediaFactoryItem(DYNAMIC_PLUGIN_MEDIA_INFO_ID, this.dynamicPluginLoader.canHandleResource, this.createDynamicPluginElement);
            this._pluginFactory.addItem(_local2);
        }
        private function createStaticPluginElement():MediaElement{
            return (new PluginElement(this.staticPluginLoader));
        }
        private function createDynamicPluginElement():MediaElement{
            return (new PluginElement(this.dynamicPluginLoader));
        }
        private function onMediaElementCreate(_arg1:MediaFactoryEvent):void{
            this.invokeMediaElementCreationNotifications(_arg1.mediaElement);
            if (this.createdElements == null){
                this.createdElements = new Dictionary(true);
            };
            this.createdElements[_arg1.mediaElement] = true;
        }
        private function invokeMediaElementCreationNotifications(_arg1:MediaElement):void{
            var _local2:Function;
            for each (_local2 in this.notificationFunctions) {
                this.invokeMediaElementCreationNotificationFunction(_local2, _arg1);
            };
        }
        private function invokeMediaElementCreationNotificationFunction(_arg1:Function, _arg2:MediaElement):void{
            try {
                _arg1.call(null, _arg2);
            } catch(error:Error) {
            };
        }
        private function invokeMediaElementCreationNotificationForCreatedMediaElements(_arg1:Function):void{
            var _local2:Object;
            for (_local2 in this.createdElements) {
                this.invokeMediaElementCreationNotificationFunction(_arg1, (_local2 as MediaElement));
            };
        }

    }
}//package org.osmf.media.pluginClasses 
