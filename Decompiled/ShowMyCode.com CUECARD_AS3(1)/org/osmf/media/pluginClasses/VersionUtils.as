﻿package org.osmf.media.pluginClasses {

    public class VersionUtils {

        public static function parseVersionString(_arg1:String):Object{
            var _local2:Array = _arg1.split(".");
            var _local3:int;
            var _local4:int;
            if (_local2.length >= 1){
                _local3 = parseInt(_local2[0]);
            };
            if (_local2.length >= 2){
                _local4 = parseInt(_local2[1]);
                if (_local4 < 10){
                    _local4 = (_local4 * 10);
                };
            };
            return ({
                major:_local3,
                minor:_local4
            });
        }

    }
}//package org.osmf.media.pluginClasses 
