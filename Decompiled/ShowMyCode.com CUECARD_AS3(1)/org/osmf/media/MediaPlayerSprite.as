﻿package org.osmf.media {
    import flash.display.*;
    import org.osmf.events.*;
    import org.osmf.layout.*;
    import org.osmf.containers.*;

    public class MediaPlayerSprite extends Sprite {

        private var _scaleMode:String = "letterbox";
        private var _media:MediaElement;
        private var _mediaPlayer:MediaPlayer;
        private var _mediaFactory:MediaFactory;
        private var _mediaContainer:MediaContainer;

        public function MediaPlayerSprite(_arg1:MediaPlayer=null, _arg2:MediaContainer=null, _arg3:MediaFactory=null){
            this._mediaPlayer = ((_arg1) ? _arg1 : new MediaPlayer());
            this._mediaFactory = _arg3;
            this._mediaContainer = ((_arg2) ? _arg2 : new MediaContainer());
            this._mediaPlayer.addEventListener(MediaElementChangeEvent.MEDIA_ELEMENT_CHANGE, this.onMediaElementChange);
            addChild(this._mediaContainer);
            if (this._mediaPlayer.media != null){
                this.media = this._mediaPlayer.media;
            };
        }
        public function get media():MediaElement{
            return (this._media);
        }
        public function set media(_arg1:MediaElement):void{
            var _local2:LayoutMetadata;
            if (this._media != _arg1){
                if (((this._media) && (this._mediaContainer.containsMediaElement(this._media)))){
                    this._mediaContainer.removeMediaElement(this._media);
                };
                this._media = _arg1;
                if (((this._media) && ((this._media.getMetadata(LayoutMetadata.LAYOUT_NAMESPACE) == null)))){
                    _local2 = new LayoutMetadata();
                    _local2.scaleMode = this._scaleMode;
                    _local2.verticalAlign = VerticalAlign.MIDDLE;
                    _local2.horizontalAlign = HorizontalAlign.CENTER;
                    _local2.percentWidth = 100;
                    _local2.percentHeight = 100;
                    this._media.addMetadata(LayoutMetadata.LAYOUT_NAMESPACE, _local2);
                };
                this._mediaPlayer.media = _arg1;
                if (((_arg1) && (!(this._mediaContainer.containsMediaElement(_arg1))))){
                    this._mediaContainer.addMediaElement(_arg1);
                };
            };
        }
        public function get resource():MediaResourceBase{
            return (((this._media) ? this._media.resource : null));
        }
        public function set resource(_arg1:MediaResourceBase):void{
            this.media = ((_arg1) ? this.mediaFactory.createMediaElement(_arg1) : null);
        }
        public function get mediaPlayer():MediaPlayer{
            return (this._mediaPlayer);
        }
        public function get mediaContainer():MediaContainer{
            return (this._mediaContainer);
        }
        public function get mediaFactory():MediaFactory{
            this._mediaFactory = ((this._mediaFactory) ? this._mediaFactory : new DefaultMediaFactory());
            return (this._mediaFactory);
        }
        public function get scaleMode():String{
            return (this._scaleMode);
        }
        public function set scaleMode(_arg1:String):void{
            var _local2:LayoutMetadata;
            this._scaleMode = _arg1;
            if (this._media){
                _local2 = (this._media.getMetadata(LayoutMetadata.LAYOUT_NAMESPACE) as LayoutMetadata);
                _local2.scaleMode = _arg1;
            };
        }
        override public function set width(_arg1:Number):void{
            this._mediaContainer.width = _arg1;
        }
        override public function set height(_arg1:Number):void{
            this._mediaContainer.height = _arg1;
        }
        override public function get width():Number{
            return (this._mediaContainer.width);
        }
        override public function get height():Number{
            return (this._mediaContainer.height);
        }
        private function onMediaElementChange(_arg1:MediaElementChangeEvent):void{
            this.media = this._mediaPlayer.media;
        }

    }
}//package org.osmf.media 
