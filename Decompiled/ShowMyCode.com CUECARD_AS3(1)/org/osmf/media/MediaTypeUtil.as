﻿package org.osmf.media {
    import __AS3__.vec.*;

    public class MediaTypeUtil {

        public static const METADATA_MATCH_FOUND:int = 0;
        public static const METADATA_CONFLICTS_FOUND:int = 1;
        public static const METADATA_MATCH_UNKNOWN:int = 2;

        public static function checkMetadataMatchWithResource(_arg1:MediaResourceBase, _arg2:Vector.<String>, _arg3:Vector.<String>):int{
            var _local4:String = ((_arg1) ? _arg1.mediaType : null);
            var _local5:String = ((_arg1) ? _arg1.mimeType : null);
            return (checkMetadataMatch(_local4, _local5, _arg2, _arg3));
        }
        public static function checkMetadataMatch(_arg1:String, _arg2:String, _arg3:Vector.<String>, _arg4:Vector.<String>):int{
            if (_arg1 != null){
                if (_arg2 != null){
                    return (((((matchType(_arg1, _arg3)) && (matchType(_arg2, _arg4)))) ? METADATA_MATCH_FOUND : METADATA_CONFLICTS_FOUND));
                };
                return (((matchType(_arg1, _arg3)) ? METADATA_MATCH_FOUND : METADATA_CONFLICTS_FOUND));
            };
            if (_arg2 != null){
                return (((matchType(_arg2, _arg4)) ? METADATA_MATCH_FOUND : METADATA_CONFLICTS_FOUND));
            };
            return (METADATA_MATCH_UNKNOWN);
        }
        private static function matchType(_arg1:String, _arg2:Vector.<String>):Boolean{
            var _local3:int;
            while (_local3 < _arg2.length) {
                if (_arg1 == _arg2[_local3]){
                    return (true);
                };
                _local3++;
            };
            return (false);
        }

    }
}//package org.osmf.media 
