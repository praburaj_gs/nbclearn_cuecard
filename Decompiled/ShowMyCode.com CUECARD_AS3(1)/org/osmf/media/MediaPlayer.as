﻿package org.osmf.media {
    import flash.events.*;
    import flash.display.*;
    import org.osmf.net.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class MediaPlayer extends TraitEventDispatcher {

        private static const DEFAULT_UPDATE_INTERVAL:Number = 250;

        private var lastCurrentTime:Number = 0;
        private var lastBytesLoaded:Number = NaN;
        private var _autoPlay:Boolean = true;
        private var _autoRewind:Boolean = true;
        private var _loop:Boolean = false;
        private var _currentTimeUpdateInterval:Number = 250;
        private var _currentTimeTimer:Timer;
        private var _state:String;
        private var _bytesLoadedUpdateInterval:Number = 250;
        private var _bytesLoadedTimer:Timer;
        private var inExecuteAutoRewind:Boolean = false;
        private var inSeek:Boolean = false;
        private var mediaAtEnd:Boolean = false;
        private var mediaPlayerVolume:Number = 1;
        private var mediaPlayerVolumeSet:Boolean = false;
        private var mediaPlayerMuted:Boolean = false;
        private var mediaPlayerMutedSet:Boolean = false;
        private var mediaPlayerAudioPan:Number = 0;
        private var mediaPlayerAudioPanSet:Boolean = false;
        private var mediaPlayerBufferTime:Number = 0;
        private var mediaPlayerBufferTimeSet:Boolean = false;
        private var mediaPlayerMaxAllowedDynamicStreamIndex:int = 0;
        private var mediaPlayerMaxAllowedDynamicStreamIndexSet:Boolean = false;
        private var mediaPlayerAutoDynamicStreamSwitch:Boolean = true;
        private var mediaPlayerAutoDynamicStreamSwitchSet:Boolean = false;
        private var _canPlay:Boolean;
        private var _canSeek:Boolean;
        private var _temporal:Boolean;
        private var _hasAudio:Boolean;
        private var _hasDisplayObject:Boolean;
        private var _canLoad:Boolean;
        private var _canBuffer:Boolean;
        private var _isDynamicStream:Boolean;
        private var _hasAlternativeAudio:Boolean;
        private var _hasDRM:Boolean;

        public function MediaPlayer(_arg1:MediaElement=null){
            this._currentTimeTimer = new Timer(DEFAULT_UPDATE_INTERVAL);
            this._bytesLoadedTimer = new Timer(DEFAULT_UPDATE_INTERVAL);
            super();
            this._state = MediaPlayerState.UNINITIALIZED;
            this.media = _arg1;
            this._currentTimeTimer.addEventListener(TimerEvent.TIMER, this.onCurrentTimeTimer, false, 0, true);
            this._bytesLoadedTimer.addEventListener(TimerEvent.TIMER, this.onBytesLoadedTimer, false, 0, true);
        }
        override public function set media(_arg1:MediaElement):void{
            var _local2:String;
            var _local3:LoadTrait;
            if (_arg1 != media){
                this.mediaAtEnd = false;
                if (media != null){
                    this.inExecuteAutoRewind = false;
                    if (this.playing){
                        (this.getTraitOrThrow(MediaTraitType.PLAY) as PlayTrait).stop();
                    };
                    if (this.canLoad){
                        _local3 = (media.getTrait(MediaTraitType.LOAD) as LoadTrait);
                        if (_local3.loadState == LoadState.READY){
                            _local3.unload();
                        };
                    };
                    this.setState(MediaPlayerState.UNINITIALIZED);
                    if (media){
                        media.removeEventListener(MediaElementEvent.TRAIT_ADD, this.onTraitAdd);
                        media.removeEventListener(MediaElementEvent.TRAIT_REMOVE, this.onTraitRemove);
                        media.removeEventListener(MediaErrorEvent.MEDIA_ERROR, this.onMediaError);
                        for each (_local2 in media.traitTypes) {
                            this.updateTraitListeners(_local2, false);
                        };
                    };
                };
                super.media = _arg1;
                if (media != null){
                    media.addEventListener(MediaElementEvent.TRAIT_ADD, this.onTraitAdd);
                    media.addEventListener(MediaElementEvent.TRAIT_REMOVE, this.onTraitRemove);
                    media.addEventListener(MediaErrorEvent.MEDIA_ERROR, this.onMediaError);
                    if (media.hasTrait(MediaTraitType.LOAD) == false){
                        this.processReadyState();
                    };
                    for each (_local2 in media.traitTypes) {
                        this.updateTraitListeners(_local2, true);
                    };
                };
                dispatchEvent(new MediaElementChangeEvent(MediaElementChangeEvent.MEDIA_ELEMENT_CHANGE));
            };
        }
        public function set autoRewind(_arg1:Boolean):void{
            this._autoRewind = _arg1;
        }
        public function get autoRewind():Boolean{
            return (this._autoRewind);
        }
        public function set autoPlay(_arg1:Boolean):void{
            this._autoPlay = _arg1;
        }
        public function get autoPlay():Boolean{
            return (this._autoPlay);
        }
        public function set loop(_arg1:Boolean):void{
            this._loop = _arg1;
        }
        public function get loop():Boolean{
            return (this._loop);
        }
        public function set currentTimeUpdateInterval(_arg1:Number):void{
            if (this._currentTimeUpdateInterval != _arg1){
                this._currentTimeUpdateInterval = _arg1;
                if (((isNaN(this._currentTimeUpdateInterval)) || ((this._currentTimeUpdateInterval <= 0)))){
                    this._currentTimeTimer.stop();
                } else {
                    this._currentTimeTimer.delay = this._currentTimeUpdateInterval;
                    if (this.temporal){
                        this._currentTimeTimer.start();
                    };
                };
            };
        }
        public function get currentTimeUpdateInterval():Number{
            return (this._currentTimeUpdateInterval);
        }
        public function set bytesLoadedUpdateInterval(_arg1:Number):void{
            if (this._bytesLoadedUpdateInterval != _arg1){
                this._bytesLoadedUpdateInterval = _arg1;
                if (((isNaN(this._bytesLoadedUpdateInterval)) || ((this._bytesLoadedUpdateInterval <= 0)))){
                    this._bytesLoadedTimer.stop();
                } else {
                    this._bytesLoadedTimer.delay = this._bytesLoadedUpdateInterval;
                    if (this.canLoad){
                        this._bytesLoadedTimer.start();
                    };
                };
            };
        }
        public function get bytesLoadedUpdateInterval():Number{
            return (this._bytesLoadedUpdateInterval);
        }
        public function get state():String{
            return (this._state);
        }
        public function get canPlay():Boolean{
            return (this._canPlay);
        }
        public function get canPause():Boolean{
            return (((this.canPlay) ? (this.getTraitOrThrow(MediaTraitType.PLAY) as PlayTrait).canPause : false));
        }
        public function get canSeek():Boolean{
            return (this._canSeek);
        }
        public function get temporal():Boolean{
            return (this._temporal);
        }
        public function get hasAudio():Boolean{
            return (this._hasAudio);
        }
        public function get isDynamicStream():Boolean{
            return (this._isDynamicStream);
        }
        public function get hasAlternativeAudio():Boolean{
            return (this._hasAlternativeAudio);
        }
        public function get canLoad():Boolean{
            return (this._canLoad);
        }
        public function get canBuffer():Boolean{
            return (this._canBuffer);
        }
        public function get hasDRM():Boolean{
            return (this._hasDRM);
        }
        public function get volume():Number{
            return (((this.hasAudio) ? AudioTrait(this.getTraitOrThrow(MediaTraitType.AUDIO)).volume : this.mediaPlayerVolume));
        }
        public function set volume(_arg1:Number):void{
            var _local2:Boolean;
            if (this.hasAudio){
                (this.getTraitOrThrow(MediaTraitType.AUDIO) as AudioTrait).volume = _arg1;
            } else {
                if (_arg1 != this.mediaPlayerVolume){
                    _local2 = true;
                };
            };
            this.mediaPlayerVolume = _arg1;
            this.mediaPlayerVolumeSet = true;
            if (_local2){
                dispatchEvent(new AudioEvent(AudioEvent.VOLUME_CHANGE, false, false, false, _arg1));
            };
        }
        public function get muted():Boolean{
            return (((this.hasAudio) ? AudioTrait(this.getTraitOrThrow(MediaTraitType.AUDIO)).muted : this.mediaPlayerMuted));
        }
        public function set muted(_arg1:Boolean):void{
            var _local2:Boolean;
            if (this.hasAudio){
                (this.getTraitOrThrow(MediaTraitType.AUDIO) as AudioTrait).muted = _arg1;
            } else {
                if (_arg1 != this.mediaPlayerMuted){
                    _local2 = true;
                };
            };
            this.mediaPlayerMuted = _arg1;
            this.mediaPlayerMutedSet = true;
            if (_local2){
                dispatchEvent(new AudioEvent(AudioEvent.MUTED_CHANGE, false, false, _arg1));
            };
        }
        public function get audioPan():Number{
            return (((this.hasAudio) ? AudioTrait(this.getTraitOrThrow(MediaTraitType.AUDIO)).pan : this.mediaPlayerAudioPan));
        }
        public function set audioPan(_arg1:Number):void{
            var _local2:Boolean;
            if (this.hasAudio){
                (this.getTraitOrThrow(MediaTraitType.AUDIO) as AudioTrait).pan = _arg1;
            } else {
                if (_arg1 != this.mediaPlayerAudioPan){
                    _local2 = true;
                };
            };
            this.mediaPlayerAudioPan = _arg1;
            this.mediaPlayerAudioPanSet = true;
            if (_local2){
                dispatchEvent(new AudioEvent(AudioEvent.PAN_CHANGE, false, false, false, NaN, _arg1));
            };
        }
        public function get paused():Boolean{
            return (((this.canPlay) ? ((this.getTraitOrThrow(MediaTraitType.PLAY) as PlayTrait).playState == PlayState.PAUSED) : false));
        }
        public function pause():void{
            (this.getTraitOrThrow(MediaTraitType.PLAY) as PlayTrait).pause();
        }
        public function get playing():Boolean{
            return (((this.canPlay) ? ((this.getTraitOrThrow(MediaTraitType.PLAY) as PlayTrait).playState == PlayState.PLAYING) : false));
        }
        public function play():void{
            if (((((((this.canPlay) && (this.canSeek))) && (this.canSeekTo(0)))) && (this.mediaAtEnd))){
                this.executeAutoRewind(true);
            } else {
                (this.getTraitOrThrow(MediaTraitType.PLAY) as PlayTrait).play();
            };
        }
        public function get seeking():Boolean{
            return (((this.canSeek) ? (this.getTraitOrThrow(MediaTraitType.SEEK) as SeekTrait).seeking : false));
        }
        public function seek(_arg1:Number):void{
            this.inSeek = true;
            (this.getTraitOrThrow(MediaTraitType.SEEK) as SeekTrait).seek(_arg1);
            this.inSeek = false;
        }
        public function canSeekTo(_arg1:Number):Boolean{
            return (((this.canSeek) ? (this.getTraitOrThrow(MediaTraitType.SEEK) as SeekTrait).canSeekTo(_arg1) : false));
        }
        public function stop():void{
            (this.getTraitOrThrow(MediaTraitType.PLAY) as PlayTrait).stop();
            if (this.canSeek){
                this.executeAutoRewind(false);
            };
        }
        public function get mediaWidth():Number{
            return (((this._hasDisplayObject) ? (this.getTraitOrThrow(MediaTraitType.DISPLAY_OBJECT) as DisplayObjectTrait).mediaWidth : NaN));
        }
        public function get mediaHeight():Number{
            return (((this._hasDisplayObject) ? (this.getTraitOrThrow(MediaTraitType.DISPLAY_OBJECT) as DisplayObjectTrait).mediaHeight : NaN));
        }
        public function get autoDynamicStreamSwitch():Boolean{
            return (((this.isDynamicStream) ? (this.getTraitOrThrow(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait).autoSwitch : this.mediaPlayerAutoDynamicStreamSwitch));
        }
        public function set autoDynamicStreamSwitch(_arg1:Boolean):void{
            var _local2:Boolean;
            if (this.isDynamicStream){
                (this.getTraitOrThrow(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait).autoSwitch = _arg1;
            } else {
                if (_arg1 != this.mediaPlayerAutoDynamicStreamSwitch){
                    _local2 = true;
                };
            };
            this.mediaPlayerAutoDynamicStreamSwitch = _arg1;
            this.mediaPlayerAutoDynamicStreamSwitchSet = true;
            if (_local2){
                dispatchEvent(new DynamicStreamEvent(DynamicStreamEvent.AUTO_SWITCH_CHANGE, false, false, this.dynamicStreamSwitching, this.mediaPlayerAutoDynamicStreamSwitch));
            };
        }
        public function get currentDynamicStreamIndex():int{
            return (((this.isDynamicStream) ? (this.getTraitOrThrow(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait).currentIndex : 0));
        }
        public function get numDynamicStreams():int{
            return (((this.isDynamicStream) ? (this.getTraitOrThrow(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait).numDynamicStreams : 0));
        }
        public function getBitrateForDynamicStreamIndex(_arg1:int):Number{
            return ((this.getTraitOrThrow(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait).getBitrateForIndex(_arg1));
        }
        public function get currentAlternativeAudioStreamIndex():int{
            return (((this.hasAlternativeAudio) ? (this.getTraitOrThrow(MediaTraitType.ALTERNATIVE_AUDIO) as AlternativeAudioTrait).currentIndex : -1));
        }
        public function get numAlternativeAudioStreams():int{
            return (((this.hasAlternativeAudio) ? (this.getTraitOrThrow(MediaTraitType.ALTERNATIVE_AUDIO) as AlternativeAudioTrait).numAlternativeAudioStreams : 0));
        }
        public function getAlternativeAudioItemAt(_arg1:int):StreamingItem{
            return ((this.getTraitOrThrow(MediaTraitType.ALTERNATIVE_AUDIO) as AlternativeAudioTrait).getItemForIndex(_arg1));
        }
        public function get maxAllowedDynamicStreamIndex():int{
            return (((this.isDynamicStream) ? (this.getTraitOrThrow(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait).maxAllowedIndex : this.mediaPlayerMaxAllowedDynamicStreamIndex));
        }
        public function set maxAllowedDynamicStreamIndex(_arg1:int):void{
            if (this.isDynamicStream){
                (this.getTraitOrThrow(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait).maxAllowedIndex = _arg1;
            };
            this.mediaPlayerMaxAllowedDynamicStreamIndex = _arg1;
            this.mediaPlayerMaxAllowedDynamicStreamIndexSet = true;
        }
        public function get dynamicStreamSwitching():Boolean{
            return (((this.isDynamicStream) ? (this.getTraitOrThrow(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait).switching : false));
        }
        public function get alternativeAudioStreamSwitching():Boolean{
            return (((this.hasAlternativeAudio) ? (this.getTraitOrThrow(MediaTraitType.ALTERNATIVE_AUDIO) as AlternativeAudioTrait).switching : false));
        }
        public function switchDynamicStreamIndex(_arg1:int):void{
            (this.getTraitOrThrow(MediaTraitType.DYNAMIC_STREAM) as DynamicStreamTrait).switchTo(_arg1);
        }
        public function switchAlternativeAudioIndex(_arg1:int):void{
            (this.getTraitOrThrow(MediaTraitType.ALTERNATIVE_AUDIO) as AlternativeAudioTrait).switchTo(_arg1);
        }
        public function get displayObject():DisplayObject{
            return (((this._hasDisplayObject) ? (this.getTraitOrThrow(MediaTraitType.DISPLAY_OBJECT) as DisplayObjectTrait).displayObject : null));
        }
        public function get duration():Number{
            return (((this.temporal) ? (this.getTraitOrThrow(MediaTraitType.TIME) as TimeTrait).duration : 0));
        }
        public function get currentTime():Number{
            return (((this.temporal) ? (this.getTraitOrThrow(MediaTraitType.TIME) as TimeTrait).currentTime : 0));
        }
        public function get buffering():Boolean{
            return (((this.canBuffer) ? (this.getTraitOrThrow(MediaTraitType.BUFFER) as BufferTrait).buffering : false));
        }
        public function get bufferLength():Number{
            return (((this.canBuffer) ? (this.getTraitOrThrow(MediaTraitType.BUFFER) as BufferTrait).bufferLength : 0));
        }
        public function get bufferTime():Number{
            return (((this.canBuffer) ? (this.getTraitOrThrow(MediaTraitType.BUFFER) as BufferTrait).bufferTime : this.mediaPlayerBufferTime));
        }
        public function set bufferTime(_arg1:Number):void{
            var _local2:Boolean;
            if (this.canBuffer){
                (this.getTraitOrThrow(MediaTraitType.BUFFER) as BufferTrait).bufferTime = _arg1;
            } else {
                if (_arg1 != this.mediaPlayerBufferTime){
                    _local2 = true;
                };
            };
            this.mediaPlayerBufferTime = _arg1;
            this.mediaPlayerBufferTimeSet = true;
            if (_local2){
                dispatchEvent(new BufferEvent(BufferEvent.BUFFER_TIME_CHANGE, false, false, this.buffering, this.mediaPlayerBufferTime));
            };
        }
        public function get bytesLoaded():Number{
            var _local1:Number = 0;
            if (this.canLoad){
                _local1 = (this.getTraitOrThrow(MediaTraitType.LOAD) as LoadTrait).bytesLoaded;
                if (isNaN(_local1)){
                    _local1 = 0;
                };
            };
            return (_local1);
        }
        public function get bytesTotal():Number{
            var _local1:Number = 0;
            if (this.canLoad){
                _local1 = (this.getTraitOrThrow(MediaTraitType.LOAD) as LoadTrait).bytesTotal;
                if (isNaN(_local1)){
                    _local1 = 0;
                };
            };
            return (_local1);
        }
        public function authenticate(_arg1:String=null, _arg2:String=null):void{
            (this.getTraitOrThrow(MediaTraitType.DRM) as DRMTrait).authenticate(_arg1, _arg2);
        }
        public function authenticateWithToken(_arg1:Object):void{
            (this.getTraitOrThrow(MediaTraitType.DRM) as DRMTrait).authenticateWithToken(_arg1);
        }
        public function get drmState():String{
            return (((this.hasDRM) ? DRMTrait(media.getTrait(MediaTraitType.DRM)).drmState : DRMState.UNINITIALIZED));
        }
        public function get drmStartDate():Date{
            return (((this.hasDRM) ? DRMTrait(media.getTrait(MediaTraitType.DRM)).startDate : null));
        }
        public function get drmEndDate():Date{
            return (((this.hasDRM) ? DRMTrait(media.getTrait(MediaTraitType.DRM)).endDate : null));
        }
        public function get drmPeriod():Number{
            return (((this.hasDRM) ? DRMTrait(media.getTrait(MediaTraitType.DRM)).period : NaN));
        }
        public function get isDVRRecording():Boolean{
            var _local1:DVRTrait = ((media)!=null) ? (media.getTrait(MediaTraitType.DVR) as DVRTrait) : null;
            return (((_local1)!=null) ? _local1.isRecording : false);
        }
        private function getTraitOrThrow(_arg1:String):MediaTraitBase{
            var _local2:String;
            var _local3:String;
            if (((!(media)) || (!(media.hasTrait(_arg1))))){
                _local2 = OSMFStrings.getString(OSMFStrings.CAPABILITY_NOT_SUPPORTED);
                _local3 = _arg1.replace("[class ", "");
                _local3 = _local3.replace("]", "").toLowerCase();
                _local2 = _local2.replace("*trait*", _local3);
                throw (new IllegalOperationError(_local2));
            };
            return (media.getTrait(_arg1));
        }
        private function onMediaError(_arg1:MediaErrorEvent):void{
            this.setState(MediaPlayerState.PLAYBACK_ERROR);
            dispatchEvent(_arg1.clone());
        }
        private function onTraitAdd(_arg1:MediaElementEvent):void{
            this.updateTraitListeners(_arg1.traitType, true);
        }
        private function onTraitRemove(_arg1:MediaElementEvent):void{
            this.updateTraitListeners(_arg1.traitType, false);
        }
        private function updateTraitListeners(_arg1:String, _arg2:Boolean, _arg3:Boolean=true):void{
            var _local4:TimeTrait;
            var _local5:PlayTrait;
            var _local6:AudioTrait;
            var _local7:DynamicStreamTrait;
            var _local8:AlternativeAudioTrait;
            var _local9:DisplayObjectTrait;
            var _local10:LoadTrait;
            var _local11:BufferTrait;
            var _local12:String;
            if ((((((this.state == MediaPlayerState.PLAYBACK_ERROR)) && (_arg3))) && (!((_arg1 == MediaTraitType.LOAD))))){
                return;
            };
            if (_arg2){
                this.updateCapabilityForTrait(_arg1, _arg2);
            };
            switch (_arg1){
                case MediaTraitType.TIME:
                    this.changeListeners(_arg2, _arg1, TimeEvent.COMPLETE, this.onComplete);
                    this._temporal = _arg2;
                    if (((((_arg2) && ((this._currentTimeUpdateInterval > 0)))) && (!(isNaN(this._currentTimeUpdateInterval))))){
                        this._currentTimeTimer.start();
                    } else {
                        this._currentTimeTimer.stop();
                    };
                    _local4 = TimeTrait(media.getTrait(MediaTraitType.TIME));
                    if (((((!((_local4.currentTime == 0))) && ((this._currentTimeUpdateInterval > 0)))) && (!(isNaN(this._currentTimeUpdateInterval))))){
                        dispatchEvent(new TimeEvent(TimeEvent.CURRENT_TIME_CHANGE, false, false, this.currentTime));
                    };
                    if (_local4.duration != 0){
                        dispatchEvent(new TimeEvent(TimeEvent.DURATION_CHANGE, false, false, this.duration));
                    };
                    break;
                case MediaTraitType.PLAY:
                    this.changeListeners(_arg2, _arg1, PlayEvent.PLAY_STATE_CHANGE, this.onPlayStateChange);
                    this.changeListeners(_arg2, _arg1, PlayEvent.LIVE_STALL, this.onLiveStall);
                    this.changeListeners(_arg2, _arg1, PlayEvent.LIVE_RESUME, this.onLiveResume);
                    this._canPlay = _arg2;
                    _local5 = PlayTrait(media.getTrait(MediaTraitType.PLAY));
                    if (((((((this.autoPlay) && (this.canPlay))) && (!(this.playing)))) && (!(this.inSeek)))){
                        this.play();
                    } else {
                        if (_local5.playState != PlayState.STOPPED){
                            dispatchEvent(new PlayEvent(PlayEvent.PLAY_STATE_CHANGE, false, false, ((_arg2) ? _local5.playState : PlayState.STOPPED)));
                        };
                    };
                    if (_local5.canPause){
                        dispatchEvent(new PlayEvent(PlayEvent.CAN_PAUSE_CHANGE, false, false, null, _arg2));
                    };
                    break;
                case MediaTraitType.AUDIO:
                    this._hasAudio = _arg2;
                    _local6 = AudioTrait(media.getTrait(MediaTraitType.AUDIO));
                    if (this.mediaPlayerVolumeSet){
                        this.volume = this.mediaPlayerVolume;
                    } else {
                        if (this.mediaPlayerVolume != _local6.volume){
                            dispatchEvent(new AudioEvent(AudioEvent.VOLUME_CHANGE, false, false, this.muted, this.volume, this.audioPan));
                        };
                    };
                    if (this.mediaPlayerMutedSet){
                        this.muted = this.mediaPlayerMuted;
                    } else {
                        if (this.mediaPlayerMuted != _local6.muted){
                            dispatchEvent(new AudioEvent(AudioEvent.MUTED_CHANGE, false, false, this.muted, this.volume, this.audioPan));
                        };
                    };
                    if (this.mediaPlayerAudioPanSet){
                        this.audioPan = this.mediaPlayerAudioPan;
                    } else {
                        if (this.mediaPlayerAudioPan != _local6.pan){
                            dispatchEvent(new AudioEvent(AudioEvent.PAN_CHANGE, false, false, this.muted, this.volume, this.audioPan));
                        };
                    };
                    break;
                case MediaTraitType.SEEK:
                    this.changeListeners(_arg2, _arg1, SeekEvent.SEEKING_CHANGE, this.onSeeking);
                    this._canSeek = _arg2;
                    if (((SeekTrait(media.getTrait(MediaTraitType.SEEK)).seeking) && (!(this.inExecuteAutoRewind)))){
                        dispatchEvent(new SeekEvent(SeekEvent.SEEKING_CHANGE, false, false, _arg2));
                    };
                    break;
                case MediaTraitType.DYNAMIC_STREAM:
                    this._isDynamicStream = _arg2;
                    _local7 = DynamicStreamTrait(media.getTrait(MediaTraitType.DYNAMIC_STREAM));
                    if (this.mediaPlayerMaxAllowedDynamicStreamIndexSet){
                        this.maxAllowedDynamicStreamIndex = this.mediaPlayerMaxAllowedDynamicStreamIndex;
                    };
                    if (this.mediaPlayerAutoDynamicStreamSwitchSet){
                        this.autoDynamicStreamSwitch = this.mediaPlayerAutoDynamicStreamSwitch;
                    } else {
                        if (this.mediaPlayerAutoDynamicStreamSwitch != _local7.autoSwitch){
                            dispatchEvent(new DynamicStreamEvent(DynamicStreamEvent.AUTO_SWITCH_CHANGE, false, false, this.dynamicStreamSwitching, this.autoDynamicStreamSwitch));
                        };
                    };
                    if (_local7.switching){
                        dispatchEvent(new DynamicStreamEvent(DynamicStreamEvent.SWITCHING_CHANGE, false, false, this.dynamicStreamSwitching, this.autoDynamicStreamSwitch));
                    };
                    dispatchEvent(new DynamicStreamEvent(DynamicStreamEvent.NUM_DYNAMIC_STREAMS_CHANGE, false, false, this.dynamicStreamSwitching, this.autoDynamicStreamSwitch));
                    break;
                case MediaTraitType.ALTERNATIVE_AUDIO:
                    this._hasAlternativeAudio = _arg2;
                    _local8 = AlternativeAudioTrait(media.getTrait(MediaTraitType.ALTERNATIVE_AUDIO));
                    if (((_local8.switching) && (_arg2))){
                        dispatchEvent(new AlternativeAudioEvent(AlternativeAudioEvent.AUDIO_SWITCHING_CHANGE, false, false, _local8.switching));
                    };
                    dispatchEvent(new AlternativeAudioEvent(AlternativeAudioEvent.NUM_ALTERNATIVE_AUDIO_STREAMS_CHANGE, false, false, ((_local8.switching) && (_arg2))));
                    break;
                case MediaTraitType.DISPLAY_OBJECT:
                    this._hasDisplayObject = _arg2;
                    _local9 = DisplayObjectTrait(media.getTrait(MediaTraitType.DISPLAY_OBJECT));
                    if (_local9.displayObject != null){
                        dispatchEvent(new DisplayObjectEvent(DisplayObjectEvent.DISPLAY_OBJECT_CHANGE, false, false, null, this.displayObject, NaN, NaN, this.mediaWidth, this.mediaHeight));
                    };
                    if (((!(isNaN(_local9.mediaHeight))) || (!(isNaN(_local9.mediaWidth))))){
                        dispatchEvent(new DisplayObjectEvent(DisplayObjectEvent.MEDIA_SIZE_CHANGE, false, false, null, this.displayObject, NaN, NaN, this.mediaWidth, this.mediaHeight));
                    };
                    break;
                case MediaTraitType.LOAD:
                    this.changeListeners(_arg2, _arg1, LoadEvent.LOAD_STATE_CHANGE, this.onLoadState);
                    this._canLoad = _arg2;
                    _local10 = LoadTrait(media.getTrait(MediaTraitType.LOAD));
                    if (_local10.bytesLoaded > 0){
                        dispatchEvent(new LoadEvent(LoadEvent.BYTES_LOADED_CHANGE, false, false, null, this.bytesLoaded));
                    };
                    if (_local10.bytesTotal > 0){
                        dispatchEvent(new LoadEvent(LoadEvent.BYTES_TOTAL_CHANGE, false, false, null, this.bytesTotal));
                    };
                    if (_arg2){
                        _local12 = (media.getTrait(_arg1) as LoadTrait).loadState;
                        if (((!((_local12 == LoadState.READY))) && (!((_local12 == LoadState.LOADING))))){
                            this.load();
                        } else {
                            if (((((this.autoPlay) && (this.canPlay))) && (!(this.playing)))){
                                this.play();
                            };
                        };
                        if ((((this._bytesLoadedUpdateInterval > 0)) && (!(isNaN(this._bytesLoadedUpdateInterval))))){
                            this._bytesLoadedTimer.start();
                        } else {
                            this._bytesLoadedTimer.stop();
                        };
                    };
                    break;
                case MediaTraitType.BUFFER:
                    this.changeListeners(_arg2, _arg1, BufferEvent.BUFFERING_CHANGE, this.onBuffering);
                    this._canBuffer = _arg2;
                    _local11 = BufferTrait(media.getTrait(MediaTraitType.BUFFER));
                    if (this.mediaPlayerBufferTimeSet){
                        this.bufferTime = this.mediaPlayerBufferTime;
                    } else {
                        if (this.mediaPlayerBufferTime != _local11.bufferTime){
                            dispatchEvent(new BufferEvent(BufferEvent.BUFFER_TIME_CHANGE, false, false, false, this.bufferTime));
                        };
                    };
                    if (_local11.buffering){
                        dispatchEvent(new BufferEvent(BufferEvent.BUFFERING_CHANGE, false, false, this.buffering));
                    };
                    break;
                case MediaTraitType.DRM:
                    this._hasDRM = _arg2;
                    dispatchEvent(new DRMEvent(DRMEvent.DRM_STATE_CHANGE, this.drmState, false, false, this.drmStartDate, this.drmEndDate, this.drmPeriod));
                    break;
            };
            if (_arg2 == false){
                this.updateCapabilityForTrait(_arg1, false);
            };
        }
        private function updateCapabilityForTrait(_arg1:String, _arg2:Boolean):void{
            var _local3:String;
            switch (_arg1){
                case MediaTraitType.AUDIO:
                    _local3 = MediaPlayerCapabilityChangeEvent.HAS_AUDIO_CHANGE;
                    this._hasAudio = _arg2;
                    break;
                case MediaTraitType.BUFFER:
                    _local3 = MediaPlayerCapabilityChangeEvent.CAN_BUFFER_CHANGE;
                    this._canBuffer = _arg2;
                    break;
                case MediaTraitType.DISPLAY_OBJECT:
                    _local3 = MediaPlayerCapabilityChangeEvent.HAS_DISPLAY_OBJECT_CHANGE;
                    break;
                case MediaTraitType.DRM:
                    _local3 = MediaPlayerCapabilityChangeEvent.HAS_DRM_CHANGE;
                    this._hasDRM = _arg2;
                    break;
                case MediaTraitType.DYNAMIC_STREAM:
                    _local3 = MediaPlayerCapabilityChangeEvent.IS_DYNAMIC_STREAM_CHANGE;
                    this._isDynamicStream = _arg2;
                    break;
                case MediaTraitType.ALTERNATIVE_AUDIO:
                    _local3 = MediaPlayerCapabilityChangeEvent.HAS_ALTERNATIVE_AUDIO_CHANGE;
                    this._hasAlternativeAudio = _arg2;
                    break;
                case MediaTraitType.LOAD:
                    _local3 = MediaPlayerCapabilityChangeEvent.CAN_LOAD_CHANGE;
                    this._canLoad = _arg2;
                    break;
                case MediaTraitType.PLAY:
                    _local3 = MediaPlayerCapabilityChangeEvent.CAN_PLAY_CHANGE;
                    this._canPlay = _arg2;
                    break;
                case MediaTraitType.SEEK:
                    _local3 = MediaPlayerCapabilityChangeEvent.CAN_SEEK_CHANGE;
                    this._canSeek = _arg2;
                    break;
                case MediaTraitType.TIME:
                    _local3 = MediaPlayerCapabilityChangeEvent.TEMPORAL_CHANGE;
                    this._temporal = _arg2;
                    break;
            };
            if (_local3 != null){
                dispatchEvent(new MediaPlayerCapabilityChangeEvent(_local3, false, false, _arg2));
            };
        }
        private function changeListeners(_arg1:Boolean, _arg2:String, _arg3:String, _arg4:Function):void{
            var _local5:int;
            if (_arg1){
                _local5 = 1;
                media.getTrait(_arg2).addEventListener(_arg3, _arg4, false, _local5);
            } else {
                if (media.hasTrait(_arg2)){
                    media.getTrait(_arg2).removeEventListener(_arg3, _arg4);
                };
            };
        }
        private function onSeeking(_arg1:SeekEvent):void{
            this.mediaAtEnd = false;
            if ((((_arg1.type == SeekEvent.SEEKING_CHANGE)) && (_arg1.seeking))){
                this.setState(MediaPlayerState.BUFFERING);
            } else {
                if (((this.canPlay) && (this.paused))){
                    this.setState(MediaPlayerState.PAUSED);
                } else {
                    if (((this.canBuffer) && (this.buffering))){
                        this.setState(MediaPlayerState.BUFFERING);
                    } else {
                        if (((this.canPlay) && (this.playing))){
                            this.setState(MediaPlayerState.PLAYING);
                        } else {
                            if (!this.inExecuteAutoRewind){
                                this.setState(MediaPlayerState.READY);
                            };
                        };
                    };
                };
            };
        }
        private function onPlayStateChange(_arg1:PlayEvent):void{
            if (_arg1.playState == PlayState.PLAYING){
                if ((((((this.canBuffer == false)) || ((this.bufferLength > 0)))) || ((this.bufferTime < 0.001)))){
                    this.setState(MediaPlayerState.PLAYING);
                };
            } else {
                if (_arg1.playState == PlayState.PAUSED){
                    this.setState(MediaPlayerState.PAUSED);
                };
            };
        }
        private function onLiveStall(_arg1:PlayEvent):void{
            dispatchEvent(_arg1.clone());
        }
        private function onLiveResume(_arg1:PlayEvent):void{
            dispatchEvent(_arg1.clone());
        }
        private function onLoadState(_arg1:LoadEvent):void{
            if ((((_arg1.loadState == LoadState.READY)) && ((this.state == MediaPlayerState.LOADING)))){
                this.processReadyState();
            } else {
                if (_arg1.loadState == LoadState.UNINITIALIZED){
                    this.setState(MediaPlayerState.UNINITIALIZED);
                } else {
                    if (_arg1.loadState == LoadState.LOAD_ERROR){
                        this.setState(MediaPlayerState.PLAYBACK_ERROR);
                    } else {
                        if (_arg1.loadState == LoadState.LOADING){
                            this.setState(MediaPlayerState.LOADING);
                        };
                    };
                };
            };
        }
        private function processReadyState():void{
            this.setState(MediaPlayerState.READY);
            if (((((this.autoPlay) && (this.canPlay))) && (!(this.playing)))){
                this.play();
            };
        }
        private function onComplete(_arg1:TimeEvent):void{
            this.mediaAtEnd = true;
            if (((((this.loop) && (this.canSeek))) && (this.canPlay))){
                this.executeAutoRewind(true);
            } else {
                if (((!(this.loop)) && (this.canPlay))){
                    (this.getTraitOrThrow(MediaTraitType.PLAY) as PlayTrait).stop();
                    if (((this.autoRewind) && (this.canSeek))){
                        this.executeAutoRewind(false);
                    } else {
                        this.setState(MediaPlayerState.READY);
                    };
                } else {
                    this.setState(MediaPlayerState.READY);
                };
            };
        }
        private function executeAutoRewind(_arg1:Boolean):void{
            var onSeekingChange:* = null;
            var playAfterAutoRewind:* = _arg1;
            if (this.inExecuteAutoRewind == false){
                onSeekingChange = function (_arg1:SeekEvent):void{
                    if (_arg1.seeking == false){
                        removeEventListener(SeekEvent.SEEKING_CHANGE, onSeekingChange);
                        if (playAfterAutoRewind){
                            play();
                        } else {
                            setState(MediaPlayerState.READY);
                        };
                        inExecuteAutoRewind = false;
                    };
                };
                this.inExecuteAutoRewind = true;
                this.mediaAtEnd = false;
                addEventListener(SeekEvent.SEEKING_CHANGE, onSeekingChange);
                this.seek(0);
            };
        }
        private function onCurrentTimeTimer(_arg1:TimerEvent):void{
            if (((((this.temporal) && (!((this.currentTime == this.lastCurrentTime))))) && (((!(this.canSeek)) || (!(this.seeking)))))){
                this.lastCurrentTime = this.currentTime;
                dispatchEvent(new TimeEvent(TimeEvent.CURRENT_TIME_CHANGE, false, false, this.currentTime));
            };
        }
        private function onBytesLoadedTimer(_arg1:TimerEvent):void{
            var _local2:LoadEvent;
            if (((this.canLoad) && (!((this.bytesLoaded == this.lastBytesLoaded))))){
                _local2 = new LoadEvent(LoadEvent.BYTES_LOADED_CHANGE, false, false, null, this.bytesLoaded);
                this.lastBytesLoaded = this.bytesLoaded;
                dispatchEvent(_local2);
            };
        }
        private function onBuffering(_arg1:BufferEvent):void{
            if (_arg1.buffering){
                this.setState(MediaPlayerState.BUFFERING);
            } else {
                if (((this.canPlay) && (this.playing))){
                    this.setState(MediaPlayerState.PLAYING);
                } else {
                    if (((this.canPlay) && (this.paused))){
                        this.setState(MediaPlayerState.PAUSED);
                    } else {
                        this.setState(MediaPlayerState.READY);
                    };
                };
            };
        }
        private function setState(_arg1:String):void{
            var _local2:Boolean;
            var _local3:String;
            if (this._state != _arg1){
                this._state = _arg1;
                dispatchEvent(new MediaPlayerStateChangeEvent(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, false, false, this._state));
                if (_arg1 == MediaPlayerState.PLAYBACK_ERROR){
                    _local2 = this.playing;
                    for each (_local3 in media.traitTypes) {
                        if (_local3 != MediaTraitType.LOAD){
                            this.updateTraitListeners(_local3, false, false);
                        };
                    };
                    if (_local2){
                        (this.getTraitOrThrow(MediaTraitType.PLAY) as PlayTrait).stop();
                    };
                };
            };
        }
        private function load():void{
            var loadTrait:* = null;
            try {
                loadTrait = (media.getTrait(MediaTraitType.LOAD) as LoadTrait);
                if (((!((loadTrait.loadState == LoadState.LOADING))) && (!((loadTrait.loadState == LoadState.READY))))){
                    loadTrait.load();
                };
            } catch(error:IllegalOperationError) {
                setState(MediaPlayerState.PLAYBACK_ERROR);
                dispatchEvent(new MediaErrorEvent(MediaErrorEvent.MEDIA_ERROR, false, false, new MediaError(MediaErrorCodes.MEDIA_LOAD_FAILED, error.message)));
            };
        }

    }
}//package org.osmf.media 
