﻿package org.osmf.media {
    import __AS3__.vec.*;
    import org.osmf.utils.*;
    import org.osmf.media.pluginClasses.*;

    public class PluginInfo {

        public static const PLUGIN_MEDIAFACTORY_NAMESPACE:String = "http://www.osmf.org/plugin/mediaFactory/1.0";

        private var _mediaFactoryItems:Vector.<MediaFactoryItem>;
        private var _mediaElementCreationNotificationFunction:Function;

        public function PluginInfo(_arg1:Vector.<MediaFactoryItem>=null, _arg2:Function=null){
            this._mediaFactoryItems = ((_arg1)!=null) ? _arg1 : new Vector.<MediaFactoryItem>();
            this._mediaElementCreationNotificationFunction = _arg2;
        }
        public function get numMediaFactoryItems():int{
            return (this._mediaFactoryItems.length);
        }
        public function get frameworkVersion():String{
            return (Version.version);
        }
        public function getMediaFactoryItemAt(_arg1:int):MediaFactoryItem{
            if ((((_arg1 < 0)) || ((_arg1 >= this._mediaFactoryItems.length)))){
                throw (new RangeError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            return ((this._mediaFactoryItems[_arg1] as MediaFactoryItem));
        }
        public function isFrameworkVersionSupported(_arg1:String):Boolean{
            if ((((_arg1 == null)) || ((_arg1.length == 0)))){
                return (false);
            };
            var _local2:Object = VersionUtils.parseVersionString(_arg1);
            var _local3:Object = VersionUtils.parseVersionString(this.frameworkVersion);
            return ((((_local2.major > _local3.major)) || ((((_local2.major == _local3.major)) && ((_local2.minor >= _local3.minor))))));
        }
        public function initializePlugin(_arg1:MediaResourceBase):void{
        }
        public function get mediaElementCreationNotificationFunction():Function{
            return (this._mediaElementCreationNotificationFunction);
        }
        final protected function get mediaFactoryItems():Vector.<MediaFactoryItem>{
            return (this._mediaFactoryItems);
        }
        final protected function set mediaFactoryItems(_arg1:Vector.<MediaFactoryItem>):void{
            this._mediaFactoryItems = _arg1;
        }

    }
}//package org.osmf.media 
