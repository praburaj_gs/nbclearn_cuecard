﻿package org.osmf.media {

    public class PluginInfoResource extends MediaResourceBase {

        private var _pluginInfo:PluginInfo;

        public function PluginInfoResource(_arg1:PluginInfo){
            this._pluginInfo = _arg1;
        }
        public function get pluginInfo():PluginInfo{
            return (this._pluginInfo);
        }

    }
}//package org.osmf.media 
