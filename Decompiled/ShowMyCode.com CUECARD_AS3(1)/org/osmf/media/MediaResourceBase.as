﻿package org.osmf.media {
    import __AS3__.vec.*;
    import flash.utils.*;

    public class MediaResourceBase {

        private var _metadata:Dictionary;
        private var _mediaType:String;
        private var _mimeType:String;

        public function get mediaType():String{
            return (this._mediaType);
        }
        public function set mediaType(_arg1:String):void{
            this._mediaType = _arg1;
        }
        public function get mimeType():String{
            return (this._mimeType);
        }
        public function set mimeType(_arg1:String):void{
            this._mimeType = _arg1;
        }
        public function get metadataNamespaceURLs():Vector.<String>{
            var _local2:String;
            var _local1:Vector.<String> = new Vector.<String>();
            if (this._metadata != null){
                for (_local2 in this._metadata) {
                    _local1.push(_local2);
                };
            };
            return (_local1);
        }
        public function addMetadataValue(_arg1:String, _arg2:Object):void{
            if (this._metadata == null){
                this._metadata = new Dictionary();
            };
            this._metadata[_arg1] = _arg2;
        }
        public function getMetadataValue(_arg1:String):Object{
            if (this._metadata != null){
                return (this._metadata[_arg1]);
            };
            return (null);
        }
        public function removeMetadataValue(_arg1:String):Object{
            var _local2:Object;
            if (this._metadata != null){
                _local2 = this._metadata[_arg1];
                delete this._metadata[_arg1];
                return (_local2);
            };
            return (null);
        }

    }
}//package org.osmf.media 
