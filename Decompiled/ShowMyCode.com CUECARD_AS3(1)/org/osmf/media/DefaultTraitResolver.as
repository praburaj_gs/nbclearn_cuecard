﻿package org.osmf.media {
    import org.osmf.traits.*;
    import org.osmf.utils.*;

    public class DefaultTraitResolver extends MediaTraitResolver {

        private var defaultTrait:MediaTraitBase;
        private var trait:MediaTraitBase;

        public function DefaultTraitResolver(_arg1:String, _arg2:MediaTraitBase){
            super(_arg1);
            if (_arg2 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            if (_arg2.traitType != _arg1){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            this.defaultTrait = _arg2;
            setResolvedTrait(_arg2);
        }
        override protected function processAddTrait(_arg1:MediaTraitBase):void{
            if (this.trait == null){
                setResolvedTrait((this.trait = _arg1));
            };
        }
        override protected function processRemoveTrait(_arg1:MediaTraitBase):MediaTraitBase{
            var _local2:MediaTraitBase;
            if (((_arg1) && ((_arg1 == this.trait)))){
                _local2 = this.trait;
                this.trait = null;
                setResolvedTrait(this.defaultTrait);
            };
            return (_local2);
        }

    }
}//package org.osmf.media 
