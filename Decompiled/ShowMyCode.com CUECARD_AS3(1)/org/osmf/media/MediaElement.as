﻿package org.osmf.media {
    import flash.events.*;
    import __AS3__.vec.*;
    import org.osmf.metadata.*;
    import org.osmf.events.*;
    import org.osmf.traits.*;
    import flash.utils.*;
    import org.osmf.containers.*;
    import org.osmf.utils.*;

    public class MediaElement extends EventDispatcher {

        private var traits:Dictionary;
        private var traitResolvers:Dictionary;
        private var unresolvedTraits:Dictionary;
        private var _traitTypes:Vector.<String>;
        private var _resource:MediaResourceBase;
        private var _metadata:Metadata;
        private var _container:IMediaContainer;

        public function MediaElement(){
            this.traits = new Dictionary();
            this.traitResolvers = new Dictionary();
            this.unresolvedTraits = new Dictionary();
            this._traitTypes = new Vector.<String>();
            super();
            this._metadata = this.createMetadata();
            this._metadata.addEventListener(MetadataEvent.VALUE_ADD, this.onMetadataValueAdd);
            this._metadata.addEventListener(MetadataEvent.VALUE_REMOVE, this.onMetadataValueRemove);
            this._metadata.addEventListener(MetadataEvent.VALUE_CHANGE, this.onMetadataValueChange);
            this.setupTraitResolvers();
            this.setupTraits();
            addEventListener(ContainerChangeEvent.CONTAINER_CHANGE, this.onContainerChange, false, Number.MAX_VALUE);
        }
        public function get traitTypes():Vector.<String>{
            return (this._traitTypes.concat());
        }
        public function hasTrait(_arg1:String):Boolean{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            return (!((this.traits[_arg1] == null)));
        }
        public function getTrait(_arg1:String):MediaTraitBase{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            return (this.traits[_arg1]);
        }
        public function get resource():MediaResourceBase{
            return (this._resource);
        }
        public function set resource(_arg1:MediaResourceBase):void{
            this._resource = _arg1;
        }
        public function get container():IMediaContainer{
            return (this._container);
        }
        public function addMetadata(_arg1:String, _arg2:Metadata):void{
            if ((((_arg1 == null)) || ((_arg2 == null)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            this.metadata.addValue(_arg1, _arg2);
        }
        public function removeMetadata(_arg1:String):Metadata{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            return ((this.metadata.removeValue(_arg1) as Metadata));
        }
        public function getMetadata(_arg1:String):Metadata{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            return ((this.metadata.getValue(_arg1) as Metadata));
        }
        public function get metadataNamespaceURLs():Vector.<String>{
            return (this.metadata.keys);
        }
        protected function createMetadata():Metadata{
            return (new Metadata());
        }
        public function get metadata():Metadata{
            return (this._metadata);
        }
        protected function addTrait(_arg1:String, _arg2:MediaTraitBase):void{
            if ((((((_arg1 == null)) || ((_arg2 == null)))) || (!((_arg1 == _arg2.traitType))))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            var _local3:MediaTraitResolver = this.traitResolvers[_arg1];
            if (_local3 != null){
                _local3.addTrait(_arg2);
            } else {
                this.setLocalTrait(_arg1, _arg2);
            };
        }
        protected function removeTrait(_arg1:String):MediaTraitBase{
            if (_arg1 == null){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            var _local2:MediaTraitBase = this.traits[_arg1];
            var _local3:MediaTraitResolver = this.traitResolvers[_arg1];
            if (_local3 != null){
                return (_local3.removeTrait(_local2));
            };
            return (this.setLocalTrait(_arg1, null));
        }
        final protected function addTraitResolver(_arg1:String, _arg2:MediaTraitResolver):void{
            var _local3:MediaTraitBase;
            if ((((_arg2 == null)) || (!((_arg2.type == _arg1))))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            if (this.traitResolvers[_arg1] == null){
                this.unresolvedTraits[_arg1] = this.traits[_arg1];
                this.traitResolvers[_arg1] = _arg2;
                _local3 = this.traits[_arg1];
                if (_local3){
                    _arg2.addTrait(_local3);
                };
                this.processResolvedTraitChange(_arg1, _arg2.resolvedTrait);
                _arg2.addEventListener(Event.CHANGE, this.onTraitResolverChange);
            } else {
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.TRAIT_RESOLVER_ALREADY_ADDED)));
            };
        }
        final protected function removeTraitResolver(_arg1:String):MediaTraitResolver{
            if ((((_arg1 == null)) || ((this.traitResolvers[_arg1] == null)))){
                throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            var _local2:MediaTraitResolver = this.traitResolvers[_arg1];
            _local2.removeEventListener(Event.CHANGE, this.onTraitResolverChange);
            delete this.traitResolvers[_arg1];
            var _local3:MediaTraitBase = this.unresolvedTraits[_arg1];
            if (_local3 != this.traits[_arg1]){
                this.setLocalTrait(_arg1, _local3);
            };
            delete this.unresolvedTraits[_arg1];
            return (_local2);
        }
        final protected function getTraitResolver(_arg1:String):MediaTraitResolver{
            return (this.traitResolvers[_arg1]);
        }
        protected function setupTraitResolvers():void{
        }
        protected function setupTraits():void{
        }
        private function onMediaError(_arg1:MediaErrorEvent):void{
            dispatchEvent(_arg1.clone());
        }
        private function setLocalTrait(_arg1:String, _arg2:MediaTraitBase):MediaTraitBase{
            var _local3:MediaTraitBase = this.traits[_arg1];
            if (_arg2 == null){
                if (_local3 != null){
                    _local3.removeEventListener(MediaErrorEvent.MEDIA_ERROR, this.onMediaError);
                    _local3.dispose();
                    dispatchEvent(new MediaElementEvent(MediaElementEvent.TRAIT_REMOVE, false, false, _arg1));
                    this._traitTypes.splice(this._traitTypes.indexOf(_arg1), 1);
                    delete this.traits[_arg1];
                };
            } else {
                if (_local3 == null){
                    _local3 = _arg2;
                    this.traits[_arg1] = _local3;
                    this._traitTypes.push(_arg1);
                    _local3.addEventListener(MediaErrorEvent.MEDIA_ERROR, this.onMediaError);
                    dispatchEvent(new MediaElementEvent(MediaElementEvent.TRAIT_ADD, false, false, _arg1));
                } else {
                    if (_local3 != _arg2){
                        throw (new ArgumentError(OSMFStrings.getString(OSMFStrings.TRAIT_INSTANCE_ALREADY_ADDED)));
                    };
                };
            };
            return (_local3);
        }
        private function onTraitResolverChange(_arg1:Event):void{
            var _local2:MediaTraitResolver = (_arg1.target as MediaTraitResolver);
            this.processResolvedTraitChange(_local2.type, _local2.resolvedTrait);
        }
        private function processResolvedTraitChange(_arg1:String, _arg2:MediaTraitBase):void{
            if (_arg2 != this.traits[_arg1]){
                this.setLocalTrait(_arg1, _arg2);
            };
        }
        private function onContainerChange(_arg1:ContainerChangeEvent):void{
            if ((((this._container == _arg1.oldContainer)) && (!((this._container == _arg1.newContainer))))){
                this._container = _arg1.newContainer;
            };
        }
        private function onMetadataValueAdd(_arg1:MetadataEvent):void{
            dispatchEvent(new MediaElementEvent(MediaElementEvent.METADATA_ADD, false, false, null, _arg1.key, (_arg1.value as Metadata)));
        }
        private function onMetadataValueChange(_arg1:MetadataEvent):void{
            dispatchEvent(new MediaElementEvent(MediaElementEvent.METADATA_REMOVE, false, false, null, _arg1.key, (_arg1.oldValue as Metadata)));
            dispatchEvent(new MediaElementEvent(MediaElementEvent.METADATA_ADD, false, false, null, _arg1.key, (_arg1.value as Metadata)));
        }
        private function onMetadataValueRemove(_arg1:MetadataEvent):void{
            dispatchEvent(new MediaElementEvent(MediaElementEvent.METADATA_REMOVE, false, false, null, _arg1.key, (_arg1.value as Metadata)));
        }

    }
}//package org.osmf.media 
