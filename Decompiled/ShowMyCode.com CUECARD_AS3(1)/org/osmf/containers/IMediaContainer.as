﻿package org.osmf.containers {
    import org.osmf.media.*;

    public interface IMediaContainer {

        function addMediaElement(_arg1:MediaElement):MediaElement;
        function removeMediaElement(_arg1:MediaElement):MediaElement;
        function containsMediaElement(_arg1:MediaElement):Boolean;

    }
}//package org.osmf.containers 
