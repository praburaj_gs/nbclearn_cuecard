﻿package org.osmf.containers {
    import flash.display.*;
    import org.osmf.media.*;
    import org.osmf.events.*;
    import org.osmf.layout.*;
    import flash.utils.*;
    import flash.geom.*;
    import org.osmf.utils.*;
    import flash.errors.*;

    public class MediaContainer extends LayoutTargetSprite implements IMediaContainer {

        private var layoutTargets:Dictionary;
        private var _layoutRenderer:LayoutRendererBase;
        private var _backgroundColor:Number;
        private var _backgroundAlpha:Number;
        private var lastAvailableWidth:Number;
        private var lastAvailableHeight:Number;
        private var backgroundVisible:Boolean;

        public function MediaContainer(_arg1:LayoutRendererBase=null, _arg2:LayoutMetadata=null){
            this.layoutTargets = new Dictionary();
            super(_arg2);
            this._layoutRenderer = ((_arg1) || (new LayoutRenderer()));
            this._layoutRenderer.container = this;
            addEventListener(VideoSurfaceEvent.RENDER_CHANGE, this.onVideoSurfaceEvent);
        }
        public function addMediaElement(_arg1:MediaElement):MediaElement{
            var _local2:MediaElementLayoutTarget;
            if (_arg1 == null){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            if (this.layoutTargets[_arg1] == undefined){
                _arg1.dispatchEvent(new ContainerChangeEvent(ContainerChangeEvent.CONTAINER_CHANGE, false, false, _arg1.container, this));
                _local2 = MediaElementLayoutTarget.getInstance(_arg1);
                this.layoutTargets[_arg1] = _local2;
                this._layoutRenderer.addTarget(_local2);
                _arg1.addEventListener(ContainerChangeEvent.CONTAINER_CHANGE, this.onElementContainerChange);
            } else {
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            return (_arg1);
        }
        public function removeMediaElement(_arg1:MediaElement):MediaElement{
            var _local2:MediaElement;
            if (_arg1 == null){
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.NULL_PARAM)));
            };
            var _local3:MediaElementLayoutTarget = this.layoutTargets[_arg1];
            if (_local3){
                _arg1.removeEventListener(ContainerChangeEvent.CONTAINER_CHANGE, this.onElementContainerChange);
                this._layoutRenderer.removeTarget(_local3);
                delete this.layoutTargets[_arg1];
                _local2 = _arg1;
                if (_arg1.container == this){
                    _arg1.dispatchEvent(new ContainerChangeEvent(ContainerChangeEvent.CONTAINER_CHANGE, false, false, _arg1.container, null));
                };
            } else {
                throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.INVALID_PARAM)));
            };
            return (_local2);
        }
        public function containsMediaElement(_arg1:MediaElement):Boolean{
            return (!((this.layoutTargets[_arg1] == undefined)));
        }
        public function get layoutRenderer():LayoutRendererBase{
            return (this._layoutRenderer);
        }
        public function get clipChildren():Boolean{
            return (!((scrollRect == null)));
        }
        public function set clipChildren(_arg1:Boolean):void{
            if (((_arg1) && ((scrollRect == null)))){
                scrollRect = new Rectangle(0, 0, this._layoutRenderer.measuredWidth, this._layoutRenderer.measuredHeight);
            } else {
                if ((((_arg1 == false)) && (scrollRect))){
                    scrollRect = null;
                };
            };
        }
        public function get backgroundColor():Number{
            return (this._backgroundColor);
        }
        public function set backgroundColor(_arg1:Number):void{
            if (_arg1 != this._backgroundColor){
                this._backgroundColor = _arg1;
                this.drawBackground();
            };
        }
        public function get backgroundAlpha():Number{
            return (this._backgroundAlpha);
        }
        public function set backgroundAlpha(_arg1:Number):void{
            if (_arg1 != this._backgroundAlpha){
                this._backgroundAlpha = _arg1;
                this.drawBackground();
            };
        }
        override public function layout(_arg1:Number, _arg2:Number, _arg3:Boolean=true):void{
            super.layout(_arg1, _arg2, _arg3);
            this.lastAvailableWidth = _arg1;
            this.lastAvailableHeight = _arg2;
            if (!isNaN(this.backgroundColor)){
                this.drawBackground();
            };
            if (scrollRect){
                scrollRect = new Rectangle(0, 0, _arg1, _arg2);
            };
        }
        override public function validateNow():void{
            this._layoutRenderer.validateNow();
        }
        override public function addChild(_arg1:DisplayObject):DisplayObject{
            throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.DIRECT_DISPLAY_LIST_MOD_ERROR)));
        }
        override public function addChildAt(_arg1:DisplayObject, _arg2:int):DisplayObject{
            throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.DIRECT_DISPLAY_LIST_MOD_ERROR)));
        }
        override public function removeChild(_arg1:DisplayObject):DisplayObject{
            throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.DIRECT_DISPLAY_LIST_MOD_ERROR)));
        }
        override public function setChildIndex(_arg1:DisplayObject, _arg2:int):void{
            throw (new IllegalOperationError(OSMFStrings.getString(OSMFStrings.DIRECT_DISPLAY_LIST_MOD_ERROR)));
        }
        override protected function onAddChildAt(_arg1:LayoutTargetEvent):void{
            super.addChildAt(_arg1.displayObject, _arg1.index);
        }
        override protected function onRemoveChild(_arg1:LayoutTargetEvent):void{
            super.removeChild(_arg1.displayObject);
        }
        override protected function onSetChildIndex(_arg1:LayoutTargetEvent):void{
            super.setChildIndex(_arg1.displayObject, _arg1.index);
        }
        private function drawBackground():void{
            graphics.clear();
            if (((((((((!(isNaN(this._backgroundColor))) && (!((this._backgroundAlpha == 0))))) && (this.backgroundVisible))) && (this.lastAvailableWidth))) && (this.lastAvailableHeight))){
                graphics.beginFill(this._backgroundColor, this._backgroundAlpha);
                graphics.drawRect(0, 0, this.lastAvailableWidth, this.lastAvailableHeight);
                graphics.endFill();
            };
        }
        private function onElementContainerChange(_arg1:ContainerChangeEvent):void{
            if (_arg1.oldContainer == this){
                this.removeMediaElement((_arg1.target as MediaElement));
            };
        }
        private function onVideoSurfaceEvent(_arg1:VideoSurfaceEvent):void{
            this.backgroundVisible = !(_arg1.usesStageVideo);
            this.drawBackground();
        }

    }
}//package org.osmf.containers 
