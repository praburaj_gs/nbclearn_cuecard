﻿package {
    import flash.events.*;
    import flash.display.*;
    import fl.transitions.*;
    import flash.net.*;
    import flash.utils.*;
    import flash.text.*;
    import flash.ui.*;

    public class ImageViewer extends MovieClip {

        public var controls_mc:MovieClip;
        public var bg:GenericButton;
        public var image_mask:GenericButton;
        private var imageLoader:Loader;
        private var image_mc:Sprite;
        private var imageLoadWidth:Number;
        private var imageLoadHeight:Number;
        private var viewerWidth:Number;
        private var viewerHeight:Number;
        private var imageBaseX:Number;
        private var imageBaseY:Number;
        private var zoomTimer:Timer;
        private var zoomAmount:Number;
        private var zoomFactor:Number = 1;
        private var numberOfImages:int;
        private var imageIndex:int;
        private var imageList:XMLList;
        private var pageList_array:Array;
        private var handCursor:MovieClip;
        public var fullScreen:Boolean = false;
        private var controlsSize_obj:Object;
        private var blankScreen_mc:Sprite;
        private var fullScreenFadeTimer:Timer;
        private var fadeOutTimer:Timer;
        private var controlsVisibleInFullScreen:Boolean = true;
        private var fadeOutTween:Tween;
        private var cueCard:AS3CueCard;
        public var loadingFirstImage:Boolean = true;

        public function ImageViewer(){
            super();
            this.imageLoader = new Loader();
            this.imageLoader.contentLoaderInfo.addEventListener(Event.INIT, this.imageLoaded);
            this.image_mc = new Sprite();
            this.image_mc.mask = this.image_mask;
            this.addChild(this.image_mc);
            this.image_mc.addChild(this.imageLoader);
            this.imageBaseX = this.image_mc.x;
            this.imageBaseY = this.image_mc.y;
            var targetDepth:* = (this.numChildren - 1);
            this.setChildIndex(this.controls_mc, targetDepth);
            this.eventSetUp();
            this.fullScreenFadeTimer = new Timer(2000, 1);
            this.fullScreenFadeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function (_arg1:TimerEvent){
                Mouse.hide();
                fadeOut(_arg1);
                controlsVisibleInFullScreen = false;
            });
            this.fadeOutTimer = new Timer(1200, 1);
            this.fadeOutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, this.fadeOut);
        }
        private function fullScreenFadeIn(_arg1:MouseEvent){
            if (!this.controlsVisibleInFullScreen){
                this.controlsVisibleInFullScreen = true;
                Mouse.show();
                this.fadeIn();
            };
            this.fullScreenFadeTimer.stop();
            this.fullScreenFadeTimer.reset();
            this.fullScreenFadeTimer.start();
        }
        public function fadeInImageControls(_arg1:Object=null):void{
            if (this.fadeOutTween){
                this.fadeOutTween.stop();
            };
            this.fadeOutTimer.reset();
            this.fadeOutTween = this.cueCard.animateMovieClip(this.controls_mc, "alpha", 1, 0.5);
        }
        public function fadeOutImageControls(_arg1:Object=null):void{
            this.fadeOutTimer.reset();
            this.fadeOutTimer.start();
        }
        public function fadeIn(_arg1:Object=null):void{
            if (this.fadeOutTween){
                this.fadeOutTween.stop();
            };
            this.fadeOutTween = this.cueCard.animateMovieClip(this.controls_mc, "alpha", 1, 0.5);
        }
        private function fadeOut(_arg1:TimerEvent){
            if (this.fadeOutTween){
                this.fadeOutTween.stop();
            };
            this.fadeOutTween = this.cueCard.animateMovieClip(this.controls_mc, "alpha", 0, 2);
        }
        private function startZoomTween(){
            this.zoomTimer.start();
            this.cueCard.stage.addEventListener(MouseEvent.MOUSE_UP, function (){
                zoomTimer.stop();
                cueCard.stage.removeEventListener(MouseEvent.MOUSE_UP, zoom);
            });
        }
        private function eventSetUp(){
            this.zoomTimer = new Timer(100);
            this.zoomTimer.addEventListener(TimerEvent.TIMER, this.zoom);
            this.controls_mc.zoomIn_btn.addEventListener(KeyboardEvent.KEY_DOWN, function (_arg1:KeyboardEvent){
                if ((((_arg1.keyCode == 13)) || ((_arg1.keyCode == 32)))){
                    zoomAmount = 0.05;
                    zoom();
                };
            });
            this.controls_mc.zoomIn_btn.addEventListener(MouseEvent.MOUSE_DOWN, function (){
                zoomAmount = 0.05;
                zoom();
                startZoomTween();
            });
            this.controls_mc.zoomOut_btn.addEventListener(MouseEvent.MOUSE_DOWN, function (){
                zoomAmount = -0.06;
                zoom();
                startZoomTween();
            });
            this.controls_mc.zoomOut_btn.addEventListener(KeyboardEvent.KEY_DOWN, function (_arg1:KeyboardEvent){
                if ((((_arg1.keyCode == 13)) || ((_arg1.keyCode == 32)))){
                    zoomAmount = -0.06;
                    zoom();
                };
            });
            this.image_mc.addEventListener(MouseEvent.MOUSE_DOWN, function (){
                image_mc.startDrag();
                cueCard.stage.addEventListener(MouseEvent.MOUSE_UP, stopDragging);
                cueCard.stage.addEventListener(MouseEvent.MOUSE_MOVE, arrangeContent);
            });
            this.image_mc.addEventListener(MouseEvent.ROLL_OVER, function (_arg1:MouseEvent){
                Mouse.cursor = MouseCursor.HAND;
            });
            this.image_mc.addEventListener(MouseEvent.ROLL_OUT, function (_arg1:MouseEvent){
                Mouse.cursor = MouseCursor.AUTO;
            });
            this.controls_mc.pagination.next_btn.addEventListener(MouseEvent.CLICK, this.nextImage);
            this.controls_mc.pagination.prev_btn.addEventListener(MouseEvent.CLICK, this.prevImage);
            this.controls_mc.related_btn.tipText = "Related CueCards";
            this.controls_mc.fullScreen_btn.tipText = "Show in Full Screen";
            this.controls_mc.fitWindow_btn.tipText = "Fit Image in CueCard";
            this.controls_mc.zoomIn_btn.tipText = "Zoom In";
            this.controls_mc.zoomOut_btn.tipText = "Zoom Out";
            this.controls_mc.zoomIn_btn.addEventListener(MouseEvent.ROLL_OVER, this.hiLightControl);
            this.controls_mc.zoomIn_btn.addEventListener(MouseEvent.ROLL_OUT, this.unHiLightControl);
            this.controls_mc.zoomOut_btn.addEventListener(MouseEvent.ROLL_OVER, this.hiLightControl);
            this.controls_mc.zoomOut_btn.addEventListener(MouseEvent.ROLL_OUT, this.unHiLightControl);
            this.controls_mc.fullScreen_btn.addEventListener(MouseEvent.ROLL_OVER, this.hiLightControl);
            this.controls_mc.fullScreen_btn.addEventListener(MouseEvent.ROLL_OUT, this.unHiLightControl);
            this.controls_mc.fitWindow_btn.addEventListener(MouseEvent.ROLL_OVER, this.hiLightControl);
            this.controls_mc.fitWindow_btn.addEventListener(MouseEvent.ROLL_OUT, this.unHiLightControl);
            this.controls_mc.related_btn.addEventListener(MouseEvent.ROLL_OVER, this.hiLightControl);
            this.controls_mc.related_btn.addEventListener(MouseEvent.ROLL_OUT, this.unHiLightControl);
            this.controls_mc.fullScreen_btn.addEventListener(MouseEvent.CLICK, this.toggleFullScreen);
            this.controls_mc.fitWindow_btn.addEventListener(MouseEvent.CLICK, this.matchToSize);
            this.controls_mc.related_btn.visible = false;
            this.controls_mc.related_btn.addEventListener(MouseEvent.CLICK, function (_arg1:MouseEvent){
                if (fullScreen){
                    toggleFullScreen();
                };
                if (!cueCard.videoPlayer.related_vids){
                    cueCard.videoPlayer.loadRelatedVideos(true);
                } else {
                    cueCard.videoPlayer.displayRelatedVideos();
                };
            });
            this.controls_mc.fullScreen_btn.useHandCursor = true;
            this.controls_mc.fullScreen_btn.buttonMode = true;
            this.controls_mc.fitWindow_btn.useHandCursor = true;
            this.controls_mc.fitWindow_btn.buttonMode = true;
            this.controls_mc.related_btn.useHandCursor = true;
            this.controls_mc.related_btn.buttonMode = true;
            this.controls_mc.zoomIn_btn.useHandCursor = true;
            this.controls_mc.zoomIn_btn.buttonMode = true;
            this.controls_mc.zoomOut_btn.useHandCursor = true;
            this.controls_mc.zoomOut_btn.buttonMode = true;
            this.controls_mc.addEventListener(FocusEvent.FOCUS_IN, this.fadeInImageControls);
            this.controls_mc.addEventListener(FocusEvent.KEY_FOCUS_CHANGE, this.fadeInImageControls);
            this.controls_mc.addEventListener(FocusEvent.FOCUS_OUT, this.fadeOutImageControls);
            this.controls_mc.addEventListener(MouseEvent.ROLL_OVER, this.fadeInImageControls);
            this.controls_mc.addEventListener(MouseEvent.ROLL_OUT, function (_arg1:MouseEvent){
                if (!fullScreen){
                    fadeOutImageControls();
                };
            });
            this.image_mc.addEventListener(MouseEvent.ROLL_OVER, function (){
                if (!fullScreen){
                    cueCard.imageViewer.fadeInImageControls();
                };
            });
            this.image_mc.addEventListener(MouseEvent.ROLL_OUT, function (){
                if (!fullScreen){
                    cueCard.imageViewer.fadeOutImageControls();
                };
            });
            this.controlsSize_obj = new Object();
        }
        private function toggleFullScreen(_arg1:MouseEvent=null):void{
            switch (stage.displayState){
                case "normal":
                    this.cueCard.stage.displayState = "fullScreen";
                    break;
                case "fullScreen":
                default:
                    this.cueCard.stage.displayState = "normal";
            };
        }
        private function _fullScreenEvent(_arg1:FullScreenEvent){
            var fscreencloseTimer:* = null;
            var event:* = _arg1;
            if (!this.cueCard.imageMode){
                return;
            };
            if (event.fullScreen){
                this.fullScreen = true;
                this.image_mc.mask = null;
                this.image_mask.visible = false;
                this.controlsSize_obj.fullScreen_btn = this.controls_mc.fullScreen_btn.x;
                this.controlsSize_obj.fitWindow_btn = this.controls_mc.fitWindow_btn.x;
                this.controlsSize_obj.related_btn = this.controls_mc.related_btn.x;
                this.controlsSize_obj.base_mc = this.controls_mc.base_mc.width;
                this.controlsSize_obj.x = this.controls_mc.x;
                this.controlsSize_obj.y = this.controls_mc.y;
                this.controlsSize_obj.panel_width = this.cueCard.mask2.width;
                this.controlsSize_obj.panel_height = this.cueCard.mask2.height;
                this.controlsSize_obj.cueCardX = this.cueCard.x;
                this.controlsSize_obj.cueCardY = this.cueCard.y;
                this.controlsSize_obj.Xscale = this.controls_mc.scaleX;
                this.controlsSize_obj.Yscale = this.controls_mc.scaleY;
                this.controlsSize_obj.Zscale = this.controls_mc.scaleZ;
                this.controlsSize_obj.paginationX = this.controls_mc.pagination.x;
                this.controlsSize_obj.loading_mcX = this.cueCard.loading_mc.x;
                this.controlsSize_obj.loading_mcY = this.cueCard.loading_mc.y;
                this.cueCard.x = 223;
                this.cueCard.y = -41.5;
                this.cueCard.always_mc.visible = false;
                this.cueCard.mask2.height = this.cueCard.stage.stageHeight;
                this.cueCard.mask2.width = this.cueCard.stage.stageWidth;
                this.controls_mc.scaleX = 1.4;
                this.controls_mc.scaleY = 1.4;
                this.controls_mc.zoomIn_btn.scaleX = 0.9;
                this.controls_mc.zoomIn_btn.scaleY = 0.9;
                this.controls_mc.zoomOut_btn.scaleX = 0.9;
                this.controls_mc.zoomOut_btn.scaleY = 0.9;
                this.controls_mc.base_mc.width = (this.cueCard.stage.stageWidth / 2);
                this.controls_mc.related_btn.x = (this.controls_mc.base_mc.width - 35);
                this.controls_mc.fullScreen_btn.x = (this.controls_mc.related_btn.x - 28);
                this.controls_mc.fitWindow_btn.x = (this.controls_mc.fullScreen_btn.x - 19);
                this.controls_mc.x = ((this.cueCard.stage.stageWidth / 2) - (this.controls_mc.width / 2));
                this.centerControls();
                this.controls_mc.y = (this.cueCard.stage.stageHeight - 55);
                this.cueCard.loading_mc.x = ((this.controls_mc.x + (this.controls_mc.base_mc.width / 2)) - this.cueCard.loading_mc.width);
                this.cueCard.loading_mc.y = (this.cueCard.stage.stageHeight / 2);
                (-(this.cueCard.loading_mc.height) / 2);
                this.cueCard.background_mc.visible = false;
                this.cueCard.shadowBackground.visible = false;
                this.arrangeContent();
                this.cueCard.stage.addEventListener(MouseEvent.MOUSE_MOVE, this.fullScreenFadeIn);
                this.cueCard.omniture.trackEvent("event35", "Full Screen Image");
            } else {
                this.image_mask.visible = true;
                this.image_mc.mask = this.image_mask;
                this.controls_mc.fullScreen_btn.x = this.controlsSize_obj.fullScreen_btn;
                this.controls_mc.fitWindow_btn.x = this.controlsSize_obj.fitWindow_btn;
                this.controls_mc.related_btn.x = this.controlsSize_obj.related_btn;
                this.controls_mc.base_mc.width = this.controlsSize_obj.base_mc;
                this.controls_mc.x = this.controlsSize_obj.x;
                this.controls_mc.y = this.controlsSize_obj.y;
                this.cueCard.mask2.width = this.controlsSize_obj.panel_width;
                this.cueCard.mask2.height = this.controlsSize_obj.panel_height;
                this.cueCard.x = this.controlsSize_obj.cueCardX;
                this.cueCard.y = this.controlsSize_obj.cueCardY;
                this.controls_mc.pagination.x = this.controlsSize_obj.paginationX;
                this.controls_mc.scaleX = this.controlsSize_obj.Xscale;
                this.controls_mc.scaleY = this.controlsSize_obj.Yscale;
                this.cueCard.loading_mc.x = this.controlsSize_obj.loading_mcX;
                this.cueCard.loading_mc.y = this.controlsSize_obj.loading_mcY;
                this.cueCard.always_mc.visible = true;
                this.cueCard.background_mc.visible = true;
                this.cueCard.shadowBackground.visible = true;
                this.controls_mc.zoomIn_btn.scaleX = 1;
                this.controls_mc.zoomIn_btn.scaleY = 1;
                this.controls_mc.zoomOut_btn.scaleX = 1;
                this.controls_mc.zoomOut_btn.scaleY = 1;
                this.arrangeContent();
                this.matchToSize();
                this.cueCard.stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.fullScreenFadeIn);
                this.fullScreenFadeTimer.stop();
                this.fullScreenFadeTimer.reset();
                Mouse.show();
                this.fadeIn();
                this.controlsVisibleInFullScreen = false;
                fscreencloseTimer = new Timer(300, 1);
                fscreencloseTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function (_arg1:TimerEvent){
                    fullScreen = false;
                });
                fscreencloseTimer.start();
            };
        }
        private function stopDragging(_arg1:MouseEvent=null){
            this.image_mc.stopDrag();
            this.cueCard.stage.removeEventListener(MouseEvent.MOUSE_UP, stopDrag);
            this.cueCard.stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.arrangeContent);
        }
        public function setCueCard(_arg1:AS3CueCard){
            this.cueCard = _arg1;
            this.cueCard.stage.addEventListener(FullScreenEvent.FULL_SCREEN, this._fullScreenEvent);
        }
        private function clearButtons(){
            var index:* = 0;
            if (this.pageList_array){
                index = (this.pageList_array.length - 1);
                while (index > -1) {
                    try {
                        this.controls_mc.pagination.removeChild(this.pageList_array[index]);
                        this.pageList_array.pop();
                    } catch(e:Error) {
                        trace(("Error: " + e.toString()));
                    };
                    index = (index - 1);
                };
            };
            this.pageList_array = new Array();
        }
        private function hiLightControl(_arg1:MouseEvent, _arg2:Object=null){
            if (_arg2){
                Colors.changeTint(_arg2, 0xFF6600, 1);
            } else {
                Colors.changeTint(_arg1.currentTarget, 0xFF6600, 1);
                this.cueCard.tip.showTip(_arg1.currentTarget.tipText);
            };
        }
        private function unHiLightControl(_arg1:MouseEvent, _arg2:Object=null){
            if (_arg2){
                Colors.changeTint(_arg2, 0xFFFFFF, 0);
            } else {
                Colors.changeTint(_arg1.currentTarget, 0xFFFFFF, 0);
            };
            this.cueCard.tip.hideTip();
        }
        public function loadImageXML(_arg1:XMLList){
            var targetDepth:* = undefined;
            var index:* = 0;
            var imageId:* = null;
            var prevImageID:* = null;
            var imageIdSolo:* = null;
            var imageId_btn:* = null;
            var _images:* = _arg1;
            this.loadingFirstImage = true;
            this.imageList = _images;
            this.controls_mc.related_btn.visible = false;
            this.clearButtons();
            if (this.image_mc.parent != this){
                this.addChild(this.image_mc);
                targetDepth = (this.numChildren - 1);
                this.setChildIndex(this.controls_mc, targetDepth);
            };
            if (this.imageList.length() > 1){
                index = 0;
                while (index < this.imageList.length()) {
                    imageId = new ImageIDButton();
                    imageId.label_txt.text = String((index + 1));
                    imageId._index = index;
                    imageId.underline_txt.visible = false;
                    prevImageID = this.pageList_array[(index - 1)];
                    if (prevImageID){
                        imageId.x = ((prevImageID.x + prevImageID.width) + 1);
                        imageId.y = prevImageID.y;
                    } else {
                        imageId.x = (this.controls_mc.pagination.prev_btn.x + 7);
                        imageId.y = 1;
                    };
                    imageId.hit_btn.addEventListener(MouseEvent.CLICK, function (_arg1:MouseEvent){
                        loadImage(_arg1.currentTarget.parent._index);
                    });
                    imageId.hit_btn.addEventListener(MouseEvent.MOUSE_OVER, function (_arg1:MouseEvent){
                        var _local2:TextFormat;
                        if (imageIndex != _arg1.currentTarget.parent._index){
                            _local2 = new TextFormat();
                            _local2.color = 0xFF6600;
                            _local2.bold = true;
                            _arg1.currentTarget.parent.label_txt.setTextFormat(_local2);
                        };
                    });
                    imageId.hit_btn.addEventListener(MouseEvent.MOUSE_OUT, function (_arg1:MouseEvent){
                        var _local2:TextFormat;
                        if (imageIndex != _arg1.currentTarget.parent._index){
                            _local2 = new TextFormat();
                            _local2.color = 0xCCCCCC;
                            _local2.bold = false;
                            _arg1.currentTarget.parent.label_txt.setTextFormat(_local2);
                        };
                    });
                    imageId.hit_btn.useHandCursor = true;
                    imageId.hit_btn.buttonMode = true;
                    this.pageList_array.push(imageId);
                    this.controls_mc.pagination.addChild(imageId);
                    this.controls_mc.pagination.next_btn.visible = true;
                    this.controls_mc.pagination.prev_btn.visible = true;
                    index = (index + 1);
                };
            } else {
                imageIdSolo = new ImageIDButton();
                imageIdSolo.label_txt.text = "1";
                this.pageList_array.push(imageIdSolo);
                this.controls_mc.addChild(imageIdSolo);
                imageIdSolo.x = (this.controls_mc.pagination.prev_btn.x + 7);
                imageIdSolo.y = 7;
                imageIdSolo.visible = false;
                this.controls_mc.pagination.next_btn.visible = false;
                this.controls_mc.pagination.prev_btn.visible = false;
            };
            this.controls_mc.pagination.next_btn.x = (this.pageList_array[(this.pageList_array.length - 1)].x + 23);
            this.centerControls();
            this.loadImage(0);
            this.arrangeContent();
            this.cueCard.addItemToTabOrder(this.controls_mc.zoomIn_btn, "Zoom In");
            this.cueCard.addItemToTabOrder(this.controls_mc.zoomOut_btn, "Zoom Out");
            this.cueCard.addItemToTabOrder(this.controls_mc.pagination.prev_btn, "Previous Image");
            var tabIndex:* = 0;
            while (tabIndex < this.pageList_array.length) {
                imageId_btn = this.pageList_array[tabIndex];
                this.cueCard.addItemToTabOrder(imageId_btn.hit_btn, ("Image Number " + String((tabIndex + 1))));
                tabIndex = (tabIndex + 1);
            };
            this.cueCard.addItemToTabOrder(this.controls_mc.pagination.next_btn, "Next Image");
            this.cueCard.addItemToTabOrder(this.controls_mc.fitWindow_btn, "Fit Image to Screen");
            this.cueCard.addItemToTabOrder(this.controls_mc.fullScreen_btn, "Display Image in full screen");
            this.cueCard.addItemToTabOrder(this.controls_mc.related_btn, "Related Videos");
        }
        private function loadImage(_arg1:int){
            var _local2:String;
            this.resetPagination();
            this.imageIndex = _arg1;
            this.cueCard.setLoadingMessage(true);
            if (this.imageList.length() > 1){
                _local2 = (this.imageList[_arg1].UrlPathPrefix.toString() + this.imageList[_arg1].CueCardImage.toString());
            } else {
                _local2 = (this.imageList.UrlPathPrefix.toString() + this.imageList.CueCardImage.toString());
            };
            var _local3:URLRequest = new URLRequest(_local2);
            this.imageLoader.load(_local3);
        }
        public function removeImage(){
            if (this.image_mc.parent == this){
                this.removeChild(this.image_mc);
            };
        }
        public function nextImage(_arg1:MouseEvent=null){
            this.imageIndex = (this.imageIndex + 1);
            if (this.imageIndex >= this.imageList.length()){
                this.imageIndex = 0;
            };
            this.loadImage(this.imageIndex);
        }
        public function prevImage(_arg1:MouseEvent=null){
            this.imageIndex = (this.imageIndex - 1);
            if (this.imageIndex < 0){
                this.imageIndex = (this.imageList.length() - 1);
            };
            this.loadImage(this.imageIndex);
        }
        private function imageLoaded(_arg1:Event){
            var _local4:TextField;
            this.imageLoadWidth = this.imageLoader.contentLoaderInfo.width;
            this.imageLoadHeight = this.imageLoader.contentLoaderInfo.height;
            this.matchToSize();
            var _local2:TextFormat = new TextFormat();
            _local2.color = 0xFFFFFF;
            var _local3:int;
            while (_local3 < this.pageList_array.length) {
                _local4 = this.pageList_array[_local3].label_txt;
                if (this.imageIndex == _local3){
                    _local2.color = 0xFFFFFF;
                    _local2.bold = true;
                    _local4.setTextFormat(_local2);
                    this.pageList_array[_local3].underline_txt.visible = true;
                } else {
                    _local2.color = 0xCCCCCC;
                    _local2.bold = false;
                    _local4.setTextFormat(_local2);
                    this.pageList_array[_local3].underline_txt.visible = false;
                };
                _local3++;
            };
            this.cueCard.setLoadingMessage(false);
            if (this.loadingFirstImage){
                this.loadingFirstImage = false;
                this.cueCard.videoPlayer.loadRelatedVideos();
            };
        }
        private function resetPagination(){
            var _local3:TextField;
            var _local1:TextFormat = new TextFormat();
            _local1.color = 0xCCCCCC;
            _local1.bold = false;
            if (!this.pageList_array){
                return;
            };
            if (this.pageList_array.length < 1){
                return;
            };
            var _local2:int;
            while (_local2 < this.pageList_array.length) {
                _local3 = this.pageList_array[_local2].label_txt;
                _local3.setTextFormat(_local1);
                this.pageList_array[_local2].underline_txt.visible = false;
                _local2++;
            };
        }
        public function centerControls(){
            this.controls_mc.pagination.x = ((this.controls_mc.base_mc.width / 2) - (this.controls_mc.pagination.width / 2));
        }
        public function panImage(_arg1:Number, _arg2:Number){
            this.image_mc.x = (this.image_mc.x + _arg1);
            this.image_mc.y = (this.image_mc.y + _arg2);
            this.arrangeContent();
        }
        public function zoom(_arg1:TimerEvent=null, _arg2:Number=0){
            var _local3:Number = this.image_mc.width;
            var _local4:Number = this.image_mc.height;
            if (_arg2 != 0){
                this.image_mc.scaleX = (this.image_mc.scaleX + _arg2);
                this.image_mc.scaleY = (this.image_mc.scaleY + _arg2);
            } else {
                this.image_mc.scaleX = (this.image_mc.scaleX + this.zoomAmount);
                this.image_mc.scaleY = (this.image_mc.scaleY + this.zoomAmount);
            };
            var _local5:Number = this.image_mc.width;
            var _local6:Number = this.image_mc.height;
            var _local7:Number = (_local5 - _local3);
            var _local8:Number = (_local6 - _local4);
            this.image_mc.x = (this.image_mc.x - (_local7 / 2));
            this.image_mc.y = (this.image_mc.y - (_local8 / 2));
            this.arrangeContent();
        }
        public function matchToSize(_arg1:Object=null){
            this.viewerWidth = this.cueCard.mask2.width;
            this.viewerHeight = this.cueCard.mask2.height;
            var _local2:Number = (this.viewerWidth / this.viewerHeight);
            var _local3:Number = (this.imageLoadWidth / this.imageLoadHeight);
            if (_local3 > _local2){
                this.image_mc.width = this.viewerWidth;
                this.image_mc.height = (this.image_mc.width / _local3);
                this.image_mc.y = ((this.imageBaseY + (this.viewerHeight / 2)) - (this.image_mc.height / 2));
                this.image_mc.x = 0;
            } else {
                this.image_mc.height = this.viewerHeight;
                this.image_mc.width = (this.image_mc.height * _local3);
                this.image_mc.x = ((this.imageBaseX + (this.viewerWidth / 2)) - (this.image_mc.width / 2));
                this.image_mc.y = 0;
            };
        }
        public function arrangeContent(_arg1:Object=null){
            if (this.image_mc.parent != this){
                return;
            };
            this.viewerWidth = this.cueCard.mask2.width;
            this.viewerHeight = this.cueCard.mask2.height;
            this.image_mask.width = this.viewerWidth;
            this.image_mask.height = this.viewerHeight;
            this.bg.width = this.viewerWidth;
            this.bg.height = this.viewerHeight;
            var _local2:Number = (this.viewerWidth / this.viewerHeight);
            var _local3:Number = (this.imageLoadWidth / this.imageLoadHeight);
            if (this.image_mc.y > this.imageBaseY){
                this.image_mc.y = this.imageBaseY;
            };
            if (this.image_mc.x > this.imageBaseX){
                this.image_mc.x = this.imageBaseX;
            };
            if (this.image_mc.y < (this.viewerHeight - this.image_mc.height)){
                this.image_mc.y = (this.viewerHeight - this.image_mc.height);
            };
            if (this.image_mc.x < (this.viewerWidth - this.image_mc.width)){
                this.image_mc.x = (this.viewerWidth - this.image_mc.width);
            };
            if (_local3 >= _local2){
                if (this.image_mc.width <= this.viewerWidth){
                    this.image_mc.width = this.viewerWidth;
                    this.image_mc.height = (this.image_mc.width / _local3);
                    this.image_mc.x = this.imageBaseX;
                };
                if (this.image_mc.height <= this.viewerHeight){
                    this.image_mc.y = ((this.imageBaseY + (this.viewerHeight / 2)) - (this.image_mc.height / 2));
                };
            };
            if (_local3 <= _local2){
                if (this.image_mc.height <= this.viewerHeight){
                    this.image_mc.height = this.viewerHeight;
                    this.image_mc.width = (this.image_mc.height * _local3);
                    this.image_mc.y = this.imageBaseY;
                };
                if (this.image_mc.width <= this.viewerWidth){
                    this.image_mc.x = ((this.imageBaseX + (this.viewerWidth / 2)) - (this.image_mc.width / 2));
                };
            };
        }

    }
}//package 
