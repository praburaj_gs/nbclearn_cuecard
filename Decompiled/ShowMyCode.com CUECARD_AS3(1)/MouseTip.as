﻿package {
    import flash.events.*;
    import flash.display.*;
    import fl.transitions.*;
    import flash.utils.*;
    import flash.text.*;
    import flash.filters.*;
    import fl.transitions.easing.*;

    class MouseTip {

        private var location:MovieClip;
        public var followTipListener:Object;
        private var fadingOut:Boolean = false;
        private var pen_mc:Sprite;
        public var tip_mc:Sprite;
        private var root_mc:MovieClip;
        private var tip_txt:TextField;
        private var _align:String = "right";
        private var targetX:Number;
        private var targetY:Number;
        private var skin;
        private var shadow:DropShadowFilter;
        private var left:Number;
        private var right:Number;
        private var top:Number;
        private var bottom:Number;
        private var pauseID:Number;
        private var mouseFadeHandler:Tween;
        private var _delay:Number = 1000;
        private var text_fmt:TextFormat;
        private var delayTimer:Timer;

        public function MouseTip(_arg1:MovieClip){
            this.location = _arg1;
            this.tip_mc = new Sprite();
            this.location.addChild(this.tip_mc);
            this.pen_mc = new Sprite();
            this.tip_mc.addChild(this.pen_mc);
            this.tip_txt = new TextField();
            this.tip_mc.addChild(this.tip_txt);
            this.tip_txt.width = 5;
            this.tip_txt.height = 5;
            this.text_fmt = new TextFormat();
            this.text_fmt.color = "0x000000";
            this.tip_mc.alpha = 0;
            this.delayTimer = new Timer(this._delay, 1);
            this.delayTimer.addEventListener(TimerEvent.TIMER_COMPLETE, this.displayTip);
        }
        private function setTipText(_arg1:String){
            this.tip_txt.autoSize = TextFieldAutoSize.LEFT;
            this.tip_txt.text = _arg1;
            this.tip_txt.setTextFormat(this.text_fmt);
            var _local2:Number = (this.tip_txt.width + 5);
            var _local3:Number = (this.tip_txt.height + 5);
            var _local4:Number = -((15 + (_local3 / 2)));
            var _local5:Number = 20;
            if (this._align == "center"){
                this.tip_txt.x = (-((this.tip_txt.width / 2)) + _local5);
                this.left = (-((_local2 / 2)) + _local5);
                this.right = ((_local2 / 2) + _local5);
            };
            if (this._align == "left"){
                this.tip_txt.x = (-(this.tip_txt.width) + _local5);
                this.left = (-(_local2) + _local5);
                this.right = _local5;
            };
            if (this._align == "right"){
                this.tip_txt.x = (-(_local5) + 3);
                this.left = -(_local5);
                this.right = (_local2 - _local5);
            };
            this.tip_txt.y = (-((this.tip_txt.height / 2)) + _local4);
            this.top = (-((_local3 / 2)) + _local4);
            this.bottom = ((_local3 / 2) + _local4);
        }
        private function drawRect(){
            var _local1:Number = 0;
            this.pen_mc.graphics.clear();
            this.pen_mc.graphics.lineStyle(0, 0, 80);
            this.pen_mc.graphics.beginFill(0xFFFFFF, 80);
            this.pen_mc.graphics.moveTo((this.left + _local1), this.top);
            this.pen_mc.graphics.lineTo((this.right - _local1), this.top);
            this.pen_mc.graphics.curveTo(this.right, this.top, this.right, (this.top + _local1));
            this.pen_mc.graphics.lineTo(this.right, (this.bottom - _local1));
            this.pen_mc.graphics.curveTo(this.right, this.bottom, (this.right - _local1), this.bottom);
            if (this._align != "left"){
                this.pen_mc.graphics.lineTo((this.left + 15), this.bottom);
                this.pen_mc.graphics.lineTo(0, 0);
                this.pen_mc.graphics.lineTo((this.left + 10), this.bottom);
            } else {
                this.pen_mc.graphics.lineTo((this.left - (this.left / 1.5)), this.bottom);
                this.pen_mc.graphics.lineTo(0, 0);
                this.pen_mc.graphics.lineTo((this.left - (this.left / 2)), this.bottom);
            };
            this.pen_mc.graphics.lineTo((this.left + _local1), this.bottom);
            this.pen_mc.graphics.curveTo(this.left, this.bottom, this.left, (this.bottom - _local1));
            this.pen_mc.graphics.lineTo(this.left, (this.top + _local1));
            this.pen_mc.graphics.curveTo(this.left, this.top, (this.left + _local1), this.top);
        }
        private function _showTip(_arg1:String){
            this.fadingOut = false;
            this.setTipText(_arg1);
            this.drawRect();
            this.location.stage.addEventListener(MouseEvent.MOUSE_MOVE, this.followMouse);
            if (this.tip_mc.alpha > 0){
                this.displayTip();
            } else {
                this.delayTimer.start();
            };
        }
        private function displayTip(_arg1:TimerEvent=null){
            this.tip_mc.visible = true;
            this.mouseFadeHandler = this.animateMovieClip(this.tip_mc, "alpha", 1, 1);
        }
        private function followMouse(_arg1:MouseEvent){
            this.tip_mc.x = (this.location.mouseX + 1);
            this.tip_mc.y = (this.location.mouseY - 1);
        }
        public function showTip(_arg1:String){
            this._showTip(_arg1);
        }
        public function hideTip(){
            this.fadingOut = true;
            this.location.stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.followMouse);
            if (this.mouseFadeHandler){
                this.mouseFadeHandler.stop();
                this.mouseFadeHandler = this.animateMovieClip(this.tip_mc, "alpha", 0, 0.25);
                this.mouseFadeHandler.addEventListener(TweenEvent.MOTION_FINISH, this.fadeFinished);
            };
            this.delayTimer.stop();
            this.delayTimer.reset();
        }
        private function fadeFinished(_arg1:TweenEvent){
            if (this.fadingOut){
                this.tip_mc.alpha = 0;
                this.tip_mc.visible = false;
            };
        }
        public function set dropShadow(_arg1:Boolean){
            if (_arg1){
                this.shadow = new DropShadowFilter(10, 45, 0, 0.8, 10, 10, 1, 3, false, false, false);
                this.tip_mc.filters = [this.shadow];
            } else {
                this.tip_mc.filters = [];
            };
        }
        public function set delay(_arg1:Number){
            this._delay = _arg1;
        }
        public function set align(_arg1:String){
            this._align = _arg1;
        }
        private function animateMovieClip(_arg1:Sprite, _arg2:String, _arg3:Number, _arg4:Number):Tween{
            var _local5:Tween;
            _local5 = new Tween(_arg1, _arg2, Regular.easeInOut, _arg1[_arg2], _arg3, _arg4, true);
            return (_local5);
        }

    }
}//package 
