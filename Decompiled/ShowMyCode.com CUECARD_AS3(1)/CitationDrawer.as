﻿package {
    import flash.events.*;
    import flash.display.*;
    import fl.controls.*;
    import flash.utils.*;
    import flash.text.*;
    import flash.filters.*;
    import flash.accessibility.*;
    import flash.desktop.*;

    public class CitationDrawer extends MovieClip {

        public var CHI_rb:RadioButton;
        public var MLA_rb:RadioButton;
        public var APA_rb:RadioButton;
        public var citation_txt:TextField;
        public var copy_btn:GENERIC_BUTTON;
        public var url_bg:MovieClip;
        private var bevel:BevelFilter;
        private var is_copying:Boolean = false;
        private var group:RadioButtonGroup;
        private var metaData:MetaDataHandler;
        public var copy_button:MovieClip;
        private var month_array:Array;
        private var month_mla_array:Array;
        private var todaysDate:Date;

        public function CitationDrawer(_arg1:MetaDataHandler){
            this.metaData = _arg1;
            this.copy_btn.label_txt.text = "COPY";
            Colors.changeTint(this.copy_btn.bg, 2171688, 1);
            this.bevel = new BevelFilter(-1.3, 45, 0x999999, 0.52, 0, 0.74, 2, 2, 1, 3);
            this.eventSetUp();
            this.setUpRadioButtons();
            this.copy_button = this.copy_btn;
            this.month_array = new Array();
            this.month_array[0] = "January";
            this.month_array[1] = "February";
            this.month_array[2] = "March";
            this.month_array[3] = "April";
            this.month_array[4] = "May";
            this.month_array[5] = "June";
            this.month_array[6] = "July";
            this.month_array[7] = "August";
            this.month_array[8] = "September";
            this.month_array[9] = "October";
            this.month_array[10] = "November";
            this.month_array[11] = "December";
            this.month_mla_array = new Array();
            this.month_mla_array[0] = "Jan.";
            this.month_mla_array[1] = "Feb.";
            this.month_mla_array[2] = "Mar.";
            this.month_mla_array[3] = "Apr.";
            this.month_mla_array[4] = "May";
            this.month_mla_array[5] = "June";
            this.month_mla_array[6] = "July";
            this.month_mla_array[7] = "Aug.";
            this.month_mla_array[8] = "Sep.";
            this.month_mla_array[9] = "Oct.";
            this.month_mla_array[10] = "Nov.";
            this.month_mla_array[11] = "Dec.";
            this.todaysDate = new Date();
            this.__setTab_url_bg_CitationDrawer_bg_0();
            this.__setTab_copy_btn_CitationDrawer_button_0();
            this.__setTab_MLA_rb_CitationDrawer_radioButtons_0();
            this.__setTab_APA_rb_CitationDrawer_radioButtons_0();
            this.__setTab_CHI_rb_CitationDrawer_radioButtons_0();
            this.__setProp_MLA_rb_CitationDrawer_radioButtons_0();
            this.__setProp_APA_rb_CitationDrawer_radioButtons_0();
            this.__setProp_CHI_rb_CitationDrawer_radioButtons_0();
            this.__setAcc_MLA_rb_CitationDrawer_radioButtons_0();
            this.__setAcc_APA_rb_CitationDrawer_radioButtons_0();
            this.__setAcc_CHI_rb_CitationDrawer_radioButtons_0();
        }
        private function eventSetUp(){
            var this_mc:* = null;
            this.copy_btn.buttonMode = true;
            this.copy_btn.useHandCursor = true;
            this.citation_txt.useRichTextClipboard = true;
            this.copy_btn.addEventListener(MouseEvent.CLICK, function (_arg1:MouseEvent){
                Clipboard.generalClipboard.setData(ClipboardFormats.TEXT_FORMAT, citation_txt.text);
                copy_btn.label_txt.text = "COPIED";
                Colors.changeTint(copy_btn.bg, 0xFF00, 1);
                is_copying = true;
                var _local2:Timer = new Timer(3500, 1);
                _local2.addEventListener(TimerEvent.TIMER_COMPLETE, changeTextBackToCopy);
                _local2.start();
                this_mc.stage.focus = this_mc.citation_txt;
                this_mc.citation_txt.setSelection(0, this_mc.citation_txt.length);
            });
            this.copy_btn.addEventListener(MouseEvent.ROLL_OVER, function (_arg1:MouseEvent){
                if (!is_copying){
                    Colors.changeTint(copy_btn.bg, 15953697, 1);
                    copy_btn.filters = [bevel];
                };
            });
            this.copy_btn.addEventListener(MouseEvent.ROLL_OUT, function (_arg1:MouseEvent){
                if (!is_copying){
                    Colors.changeTint(copy_btn.bg, 2171688, 1);
                    copy_btn.filters = [];
                };
            });
            var copy_props:* = new AccessibilityProperties();
            copy_props.name = "Copy Citation Button";
            this_mc = this;
        }
        private function changeTextBackToCopy(_arg1:TimerEvent){
            this.copy_btn.label_txt.text = "COPY";
            Colors.changeTint(this.copy_btn.bg, 2171688, 1);
            this.is_copying = false;
        }
        private function setUpRadioButtons(){
            this.group = new RadioButtonGroup("citation_group");
            this.group.addRadioButton(this.MLA_rb);
            this.group.addRadioButton(this.APA_rb);
            this.group.addRadioButton(this.CHI_rb);
            this.group.addEventListener(MouseEvent.CLICK, this.selectCitationStyle);
        }
        private function selectCitationStyle(_arg1:MouseEvent){
            this.refreshCitation();
        }
        public function refreshCitation(){
            var _local3:String;
            var _local4:String;
            var _local5:Array;
            var _local6:String;
            var _local11:String;
            var _local12:String;
            var _local13:String;
            var _local14:int;
            var _local15:String;
            var _local16:String;
            var _local17:String;
            var _local18:String;
            this.citation_txt.htmlText = " ";
            var _local1:String = this.metaData.airDate;
            var _local2 = "";
            var _local7 = "";
            if (this.metaData.Creator){
                _local6 = this.metaData.Creator;
                _local5 = _local6.split("/");
                if (_local5.length < 2){
                    _local5 = _local6.split(", ");
                };
                if (_local5.length > 1){
                    _local3 = this.findReplace("Dr. ", "", _local5[0]);
                    _local12 = _local3.split(" ")[0];
                    _local13 = _local3.split(" ")[1];
                    _local12 = _local12.slice(0, 1);
                    _local4 = _local5[1];
                    _local14 = 1;
                    while (_local14 < _local5.length) {
                        _local5[_local14] = this.findReplace("Dr. ", "", _local5[_local14]);
                        _local15 = _local5[_local14].split(" ")[0];
                        _local16 = _local5[_local14].split(" ")[1];
                        _local7 = (_local7 + (((_local15 + " ") + _local16) + ", "));
                        _local15 = _local15.slice(0, 1);
                        _local2 = (_local2 + (((_local16 + ", ") + _local15) + ". (Reporter), & "));
                        _local14++;
                    };
                    _local7 = (_local7 + "correspondent. ");
                    _local2 = (_local2 + (((_local13 + ", ") + _local12) + ". (Anchor). "));
                } else {
                    _local17 = _local6.split(" ")[0];
                    _local18 = _local6.split(" ")[1];
                    _local7 = (((_local17 + " ") + _local18) + ", correspondent. ");
                    _local17 = _local17.slice(0, 1);
                    _local2 = (((_local18 + ", ") + _local17) + ". (Reporter). ");
                };
            };
            var _local8 = "";
            var _local9 = "";
            var _local10:Date = new Date(_local1);
            if (_local1){
                if (_local10.fullYear){
                    _local8 = (((((("(" + _local10.fullYear) + ", ") + this.isNotAvaliable(this.month_array[_local10.month])) + " ") + this.isNotAvaliable(_local10.getDate())) + ")");
                    _local9 = (((((this.isNotAvaliable(_local10.getDate()) + " ") + this.isNotAvaliable(this.month_mla_array[_local10.month])) + " ") + this.isNotAvaliable(_local10.fullYear)) + ". ");
                } else {
                    _local8 = (("(" + this.isNotAvaliable(_local1)) + ")");
                    _local9 = (this.isNotAvaliable(_local1) + " ");
                };
            };
            if ((((((((this.metaData.Product == "HE")) || ((this.metaData.Product == "K12")))) || ((this.metaData.Product == "he")))) || ((this.metaData.Product == "k12")))){
                _local11 = (("<u>" + this.metaData.bookMarkLink) + "</u>");
            } else {
                _local11 = "To locate this video, access NBC Learn through your LMS and search for the video by title.";
            };
            if (this.group.selectedData == "chi"){
                this.citation_txt.htmlText = ((((((((("\"" + this.ifQuestionMark(this.metaData.title)) + "\"  <i>") + this.isNotAvaliable(this.metaData.Source)) + "</i>, New York, NY: NBC Universal, ") + this.isNotAvaliable(_local1)) + ".  Accessed ") + this.todaysDate.toDateString()) + " from NBC Learn: ") + _local11);
            };
            if (this.group.selectedData == "apa"){
                this.citation_txt.htmlText = ((((((((((_local2 + _local8) + ". ") + this.ifQuestionMark(this.metaData.title)) + " ") + this.addSeriesDescription()) + " <i>") + this.isNotAvaliable(this.metaData.Source)) + ".</i> Retrieved from ") + _local11) + "");
            };
            if (this.group.selectedData == "mla"){
                this.citation_txt.htmlText = (((((((((((((((("\"" + this.ifQuestionMarkMLA(this.metaData.title)) + "\" ") + _local7) + "<i>") + this.isNotAvaliable(this.metaData.Source)) + ".</i> ") + this.findReplace(", LLC", "", this.isNotAvaliable(this.metaData.copyRight))) + " ") + _local9) + "<i>NBC Learn.</i> Web. ") + this.todaysDate.getDate()) + " ") + this.month_mla_array[this.todaysDate.month]) + " ") + this.todaysDate.fullYear) + ".");
            };
        }
        private function addSeriesDescription():String{
            var _local1:String;
            if ((((((((this.metaData.Source == "National Archive")) || ((this.metaData.Source == "National Archives")))) || ((this.metaData.Source == "Universal Newsreel")))) || ((this.metaData.Source == "Archival Film")))){
                _local1 = "[Newsreel].";
            } else {
                _local1 = "[Television series episode].";
            };
            return (_local1);
        }
        private function ifQuestionMark(_arg1:String):String{
            if ((((_arg1.charAt((_arg1.length - 1)) == "?")) || ((_arg1.charAt((_arg1.length - 1)) == ".")))){
                return (_arg1);
            };
            return ((_arg1 + "."));
        }
        private function ifQuestionMarkMLA(_arg1:String):String{
            _arg1 = this.findReplace("\"", "'", _arg1);
            if ((((_arg1.charAt((_arg1.length - 1)) == "?")) || ((_arg1.charAt((_arg1.length - 1)) == ".")))){
                return (_arg1);
            };
            if (_arg1.charAt((_arg1.length - 1)) == "'"){
                return ((_arg1.slice(0, (_arg1.length - 1)) + ".'"));
            };
            return ((_arg1 + "."));
        }
        private function isNotAvaliable(_arg1):String{
            if (!_arg1){
                return ("");
            };
            if (((((((!((_arg1 == null))) && (!((_arg1 == ""))))) && (!((_arg1 == " "))))) && (!((_arg1 == "NaN"))))){
                return ((String(_arg1) as String));
            };
            return ("");
        }
        private function findReplace(_arg1:String, _arg2:String, _arg3:String):String{
            if (_arg2 == "_blank"){
                _arg2 = "";
            };
            if (_arg3.indexOf(_arg1) > -1){
                return (_arg3.split(_arg1).join(_arg2));
            };
            return (_arg3);
        }
        function __setTab_url_bg_CitationDrawer_bg_0(){
            this.url_bg.tabIndex = 0;
        }
        function __setTab_copy_btn_CitationDrawer_button_0(){
            this.copy_btn.tabIndex = 0;
        }
        function __setTab_MLA_rb_CitationDrawer_radioButtons_0(){
            this.MLA_rb.tabIndex = 0;
        }
        function __setTab_APA_rb_CitationDrawer_radioButtons_0(){
            this.APA_rb.tabIndex = 0;
        }
        function __setTab_CHI_rb_CitationDrawer_radioButtons_0(){
            this.CHI_rb.tabIndex = 0;
        }
        function __setProp_MLA_rb_CitationDrawer_radioButtons_0(){
            try {
                this.MLA_rb["componentInspectorSetting"] = true;
            } catch(e:Error) {
            };
            this.MLA_rb.enabled = true;
            this.MLA_rb.groupName = "citation_group";
            this.MLA_rb.label = "";
            this.MLA_rb.labelPlacement = "right";
            this.MLA_rb.selected = true;
            this.MLA_rb.value = "mla";
            this.MLA_rb.visible = true;
            try {
                this.MLA_rb["componentInspectorSetting"] = false;
            } catch(e:Error) {
            };
        }
        function __setProp_APA_rb_CitationDrawer_radioButtons_0(){
            try {
                this.APA_rb["componentInspectorSetting"] = true;
            } catch(e:Error) {
            };
            this.APA_rb.enabled = true;
            this.APA_rb.groupName = "citation_group";
            this.APA_rb.label = "";
            this.APA_rb.labelPlacement = "right";
            this.APA_rb.selected = false;
            this.APA_rb.value = "apa";
            this.APA_rb.visible = true;
            try {
                this.APA_rb["componentInspectorSetting"] = false;
            } catch(e:Error) {
            };
        }
        function __setProp_CHI_rb_CitationDrawer_radioButtons_0(){
            try {
                this.CHI_rb["componentInspectorSetting"] = true;
            } catch(e:Error) {
            };
            this.CHI_rb.enabled = true;
            this.CHI_rb.groupName = "citation_group";
            this.CHI_rb.label = "";
            this.CHI_rb.labelPlacement = "right";
            this.CHI_rb.selected = false;
            this.CHI_rb.value = "chi";
            this.CHI_rb.visible = true;
            try {
                this.CHI_rb["componentInspectorSetting"] = false;
            } catch(e:Error) {
            };
        }
        function __setAcc_MLA_rb_CitationDrawer_radioButtons_0(){
            this.MLA_rb.accessibilityProperties = new AccessibilityProperties();
            this.MLA_rb.accessibilityProperties.name = "MLA Citation";
        }
        function __setAcc_APA_rb_CitationDrawer_radioButtons_0(){
            this.APA_rb.accessibilityProperties = new AccessibilityProperties();
            this.APA_rb.accessibilityProperties.name = "APA Citation";
        }
        function __setAcc_CHI_rb_CitationDrawer_radioButtons_0(){
            this.CHI_rb.accessibilityProperties = new AccessibilityProperties();
            this.CHI_rb.accessibilityProperties.name = "Chicago Manual Of Style Citation";
        }

    }
}//package 
