﻿package singleCard_fla {
    import flash.display.*;
    import flash.accessibility.*;

    public dynamic class LeftControls_60 extends MovieClip {

        public var academic_logo:MovieClip;
        public var peacock:MovieClip;

        public function LeftControls_60(){
            this.__setAcc_academic_logo_LeftControls_Layer2_0();
            this.__setTab_academic_logo_LeftControls_Layer2_0();
        }
        function __setAcc_academic_logo_LeftControls_Layer2_0(){
            this.academic_logo.accessibilityProperties = new AccessibilityProperties();
            this.academic_logo.accessibilityProperties.silent = true;
        }
        function __setTab_academic_logo_LeftControls_Layer2_0(){
            this.academic_logo.tabIndex = 0;
        }

    }
}//package singleCard_fla 
