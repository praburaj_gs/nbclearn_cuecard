﻿package singleCard_fla {
    import flash.display.*;

    public dynamic class FRONT_5 extends MovieClip {

        public var shadowBackground:DROP_SHADOW;
        public var background_mc:MovieClip;
        public var panels_mask:GenericButton;
        public var always_mc:MovieClip;
        public var content_mc:MovieClip;
        public var imageVeiwer_mc:ImageViewer;
        public var captions_mc:MovieClip;

        public function FRONT_5(){
            this.__setTab_background_mc_FRONT_InternalContent_0();
            this.__setTab_content_mc_FRONT_VIDEOCONTENT_0();
        }
        function __setTab_background_mc_FRONT_InternalContent_0(){
            this.background_mc.tabIndex = 1;
        }
        function __setTab_content_mc_FRONT_VIDEOCONTENT_0(){
            this.content_mc.tabIndex = 0;
        }

    }
}//package singleCard_fla 
