﻿package singleCard_fla {
    import flash.display.*;
    import flash.text.*;

    public dynamic class ALWAYSVISIBLE_36 extends MovieClip {

        public var icons_right:MovieClip;
        public var mainDragBox:GenericButton;
        public var loading_mc:MovieClip;
        public var gripper:MovieClip;
        public var controls_mc:MovieClip;
        public var title_txt:TextField;
        public var extensions_bar:ExtensionsManager;
        public var icons_left:MovieClip;
        public var dragCorner:MovieClip;

        public function ALWAYSVISIBLE_36(){
            this.__setTab_title_txt_ALWAYSVISIBLE_TITLE_0();
            this.__setTab_loading_mc_ALWAYSVISIBLE_Loading_0();
        }
        function __setTab_title_txt_ALWAYSVISIBLE_TITLE_0(){
            this.title_txt.tabIndex = 0;
        }
        function __setTab_loading_mc_ALWAYSVISIBLE_Loading_0(){
            this.loading_mc.tabIndex = 0;
        }

    }
}//package singleCard_fla 
