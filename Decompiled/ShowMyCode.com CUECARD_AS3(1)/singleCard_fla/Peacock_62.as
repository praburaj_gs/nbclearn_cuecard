﻿package singleCard_fla {
    import flash.events.*;
    import flash.display.*;
    import fl.transitions.*;
    import flash.net.*;
    import flash.utils.*;
    import flash.geom.*;
    import flash.media.*;
    import flash.system.*;
    import flash.text.*;
    import flash.filters.*;
    import fl.transitions.easing.*;
    import flash.net.drm.*;
    import flash.external.*;
    import flash.accessibility.*;
    import flash.errors.*;
    import flash.ui.*;
    import flash.desktop.*;
    import adobe.utils.*;
    import flash.globalization.*;
    import flash.printing.*;
    import flash.profiler.*;
    import flash.sampler.*;
    import flash.sensors.*;
    import flash.text.ime.*;
    import flash.text.engine.*;
    import flash.xml.*;

    public dynamic class Peacock_62 extends MovieClip {

        public var f:MovieClip;
        public var a:MovieClip;
        public var b:MovieClip;
        public var gray_bg:MovieClip;
        public var c:MovieClip;
        public var d:MovieClip;
        public var e:MovieClip;
        public var delay:int;
        public var numberOfCurrentCalls:int;
        public var calls_array:Array;
        public var grayTween:Tween;
        public var colors_array:Array;
        public var currentIndex:int;
        public var timer:Timer;

        public function Peacock_62(){
            addFrameScript(0, this.frame1);
        }
        public function enableAllColors(){
            var _local1:int;
            while (_local1 < this.colors_array.length) {
                this.colors_array[_local1].tween = new Tween(this.colors_array[_local1], "alpha", Regular.easeOut, this.colors_array[_local1].alpha, 1, 0.5, true);
                _local1++;
            };
            if (this.grayTween){
                this.grayTween.stop();
            };
            this.grayTween = new Tween(this.gray_bg, "alpha", Regular.easeOut, this.gray_bg.alpha, 0, 0.5, true);
        }
        public function clearAllColors(){
            var index:* = 0;
            while (index < this.colors_array.length) {
                try {
                    if (this.colors_array[index].tween){
                        this.colors_array[index].tween.stop();
                    };
                } catch(e:Error) {
                };
                this.colors_array[index].alpha = 0;
                index = (index + 1);
            };
        }
        public function changeColor(_arg1:TimerEvent=null){
            var _local2 = 100;
            var _local3 = 100;
            var _local4 = 100;
            if (this.currentIndex < (this.colors_array.length + this.delay)){
                this.currentIndex++;
                _local2 = (this.currentIndex - 1);
                _local3 = (_local2 - 1);
                _local4 = (_local3 - 1);
            } else {
                this.currentIndex = 0;
            };
            if ((((_local4 < this.colors_array.length)) && ((_local4 >= 0)))){
                this.colors_array[_local4].alpha = 0;
            };
            if ((((_local3 < this.colors_array.length)) && ((_local3 >= 0)))){
                this.colors_array[_local3].alpha = 0.5;
            };
            if ((((_local2 < this.colors_array.length)) && ((_local2 >= 0)))){
                this.colors_array[_local2].alpha = 0.75;
            };
            if ((((this.currentIndex < this.colors_array.length)) && ((this.currentIndex >= 0)))){
                this.colors_array[this.currentIndex].alpha = 1;
            };
        }
        public function stopAllLoading(){
            this.timer.stop();
            var _local1:Number = 0;
            while (_local1 < this.calls_array.length) {
                this.calls_array.pop();
                _local1++;
            };
            this.calls_array = new Array();
            this.currentIndex = 0;
            this.numberOfCurrentCalls = 0;
            this.enableAllColors();
        }
        public function startedLoading(_arg1:String=null){
            if (this.numberOfCurrentCalls < 1){
                this.timer.start();
                this.calls_array.push(_arg1);
                if (this.grayTween){
                    this.grayTween.stop();
                };
                this.gray_bg.alpha = 1;
                this.clearAllColors();
                this.changeColor();
            };
            this.numberOfCurrentCalls++;
        }
        public function finishedLoading(_arg1:String=null){
            this.numberOfCurrentCalls--;
            if (this.numberOfCurrentCalls < 1){
                this.timer.stop();
                this.timer.reset();
                this.currentIndex = 0;
                this.enableAllColors();
            };
            var _local2:Number = 0;
            while (_local2 < this.calls_array.length) {
                if (_arg1 == this.calls_array[_local2]){
                    this.calls_array.splice(_local2, 1);
                    break;
                };
                _local2++;
            };
        }
        function frame1(){
            this.delay = 2;
            this.numberOfCurrentCalls = 0;
            this.calls_array = new Array();
            this.colors_array = new Array(this.a, this.b, this.c, this.d, this.e, this.f);
            this.enableAllColors();
            this.currentIndex = (this.colors_array.length + this.delay);
            this.timer = new Timer(165);
            this.timer.addEventListener(TimerEvent.TIMER, this.changeColor);
        }

    }
}//package singleCard_fla 
