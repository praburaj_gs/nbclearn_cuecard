﻿package singleCard_fla {
    import flash.display.*;
    import flash.accessibility.*;

    public dynamic class MainTimeline extends MovieClip {

        public var cueCard:AS3CueCard;

        public function MainTimeline(){
            addFrameScript(0, this.frame1);
            this.__setAcc_cueCard_Scene1_CUECARD_0();
            this.root.accessibilityProperties = new AccessibilityProperties();
            this.root.accessibilityProperties.noAutoLabeling = true;
            this.__setTab_cueCard_Scene1_CUECARD_0();
        }
        function __setAcc_cueCard_Scene1_CUECARD_0(){
            this.cueCard.accessibilityProperties = new AccessibilityProperties();
            this.cueCard.accessibilityProperties.name = "CUE CARD";
        }
        function __setTab_cueCard_Scene1_CUECARD_0(){
            this.cueCard.tabIndex = 1;
        }
        function frame1(){
        }

    }
}//package singleCard_fla 
