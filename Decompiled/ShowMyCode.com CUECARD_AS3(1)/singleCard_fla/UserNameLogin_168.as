﻿package singleCard_fla {
    import flash.display.*;
    import flash.text.*;
    import flash.accessibility.*;

    public dynamic class UserNameLogin_168 extends MovieClip {

        public var email_txt:TextField;
        public var pass_txt:TextField;
        public var subscribe_mc:MovieClip;
        public var genericReg_mc:MovieClip;
        public var header_txt:TextField;
        public var login_btn:GENERIC_BUTTON;
        public var Text283:TextField;
        public var Text284:TextField;

        public function UserNameLogin_168(){
            this.__setAcc_Text283_UserNameLogin_statictext_0();
            this.__setAcc_Text284_UserNameLogin_statictext_0();
            this.__setTab_Text283_UserNameLogin_statictext_0();
            this.__setTab_Text284_UserNameLogin_statictext_0();
        }
        function __setAcc_Text283_UserNameLogin_statictext_0(){
            this.Text283.accessibilityProperties = new AccessibilityProperties();
            this.Text283.accessibilityProperties.silent = true;
        }
        function __setAcc_Text284_UserNameLogin_statictext_0(){
            this.Text284.accessibilityProperties = new AccessibilityProperties();
            this.Text284.accessibilityProperties.silent = true;
        }
        function __setTab_Text283_UserNameLogin_statictext_0(){
            this.Text283.tabIndex = 0;
        }
        function __setTab_Text284_UserNameLogin_statictext_0(){
            this.Text284.tabIndex = 0;
        }

    }
}//package singleCard_fla 
