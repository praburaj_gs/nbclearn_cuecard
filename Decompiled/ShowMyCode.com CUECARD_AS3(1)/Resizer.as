﻿package {
    import flash.events.*;
    import flash.display.*;
    import fl.transitions.*;
    import flash.utils.*;
    import fl.transitions.easing.*;

    public class Resizer {

        public static const TOP_LEFT:String = "top_left";
        public static const TOP_RIGHT:String = "top_right";
        public static const BOTTOM_LEFT:String = "top_left";
        public static const BOTTOM_RIGHT:String = "top_right";
        public static const WIDTH:String = "width";
        public static const HEIGHT:String = "height";
        public static const WIDTH_AND_HEIGHT:String = "width_and_height";
        public static const X_AND_Y:String = "Verticle and horizontal";
        public static const VERTICLE:String = "verticle";
        public static const HORIZONTAL:String = "horizontal";
        public static const RESIZING:String = "resizing";
        public static const RESIZE_COMPLETE:String = "resiz_compleate";
        public static const RESET_COMPLETE:String = "reset_compleate";
        public static const RESET_FADE:String = "resizing_fade";

        private var timer:Timer;
        private var resetTimer:Timer;
        private var resize_array:Array;
        private var move_array:Array;
        private var resetTween:Tween;
        public var changed:Boolean = false;
        private var constrain:Boolean = false;
        private var dragStartX:Number;
        private var dragStartY:Number;
        private var offsetX:Number;
        private var offsetY:Number;
        private var oldX:Number;
        private var oldY:Number;
        private var currentChangeX:Number;
        private var currentChangeY:Number;
        private var totalChangeX:Number = 0;
        private var totalChangeY:Number = 0;
        private var left:Number;
        private var right:Number;
        private var top:Number;
        private var bottom:Number;
        private var _drag_mc:Sprite;
        private var resizeCompleteCallBack:Function;
        private var resizeEvent_array:Array;
        private var resetFadeEvent_array:Array;
        private var resizeCompleteEvent_array:Array;
        private var resetCompleteEvent_array:Array;
        private var _left:Number;
        private var _right:Number;
        private var _top:Number;
        private var _bottom:Number;
        private var _delay:int = 20;
        public var isBaseSize:Boolean = true;

        public function Resizer(_arg1:Sprite){
            this._drag_mc = _arg1;
            this.timer = new Timer(this._delay);
            this.timer.addEventListener(TimerEvent.TIMER, this.resize);
            this.resetTimer = new Timer(this._delay);
            this.resetTimer.addEventListener(TimerEvent.TIMER, this.resetting);
            this.resize_array = new Array();
            this.dragStartX = this._drag_mc.x;
            this.dragStartY = this._drag_mc.y;
            this.resizeEvent_array = new Array();
            this.resetFadeEvent_array = new Array();
            this.resizeCompleteEvent_array = new Array();
            this.resetCompleteEvent_array = new Array();
        }
        public function constrainTo(_arg1:Number, _arg2:Number, _arg3:Number, _arg4:Number){
            this.left = _arg1;
            this.right = _arg2;
            this.top = _arg3;
            this.bottom = _arg4;
            this.constrain = true;
        }
        public function set dragObject(_arg1:Sprite){
            this._drag_mc = _arg1;
        }
        public function set delay(_arg1:int){
            this._delay = this._delay;
            this.timer.delay = this._delay;
        }
        public function addResizableObject(_arg1:Sprite, _arg2:String="width_and_height"):void{
            this.resize_array.push({
                object:_arg1,
                dimension:_arg2,
                _x:_arg1.x,
                _y:_arg1.y,
                _width:_arg1.width,
                _height:_arg1.height
            });
        }
        public function updateBaseDimension(_arg1:Sprite, _arg2:String, _arg3:Number){
            var _local4:Object;
            var _local5:int;
            while (_local5 < this.resize_array.length) {
                if (_arg1 == this.resize_array[_local5].object){
                    _local4 = this.resize_array[_local5];
                    break;
                };
                _local5++;
            };
            if (_arg2 == Resizer.HEIGHT){
                _local4._height = (_local4._height + _arg3);
            };
            if (_arg2 == Resizer.WIDTH){
                _local4._width = (_local4._width + _arg3);
            };
            if (_arg2 == Resizer.VERTICLE){
                _local4._y = (_local4._y + _arg3);
            };
            if (_arg2 == Resizer.HORIZONTAL){
                _local4._x = (_local4._x + _arg3);
            };
        }
        public function startResize():void{
            this.changed = true;
            this.offsetX = this._drag_mc.mouseX;
            this.offsetY = this._drag_mc.mouseY;
            this.oldX = this._drag_mc.parent.mouseX;
            this.oldY = this._drag_mc.parent.mouseY;
            this.currentChangeX = 0;
            this.currentChangeY = 0;
            this.timer.start();
            this.isBaseSize = false;
        }
        public function stopResize():void{
            this.timer.stop();
            this.totalChangeX = (this.totalChangeX + this.currentChangeX);
            this.totalChangeY = (this.totalChangeY + this.currentChangeY);
            this.dispatchEvent(Resizer.RESIZE_COMPLETE, {
                totalChangeX:this.currentChangeX,
                totalChangeY:this.currentChangeY
            });
        }
        public function getTotalChange():Object{
            return ({
                x:this.totalChangeX,
                y:this.totalChangeY
            });
        }
        public function resize(_arg1:TimerEvent=null, _arg2:Number=0, _arg3:Number=0):void{
            var _local4:Number;
            var _local5:Number;
            var _local7:Sprite;
            var _local8:String;
            if ((((_arg2 == 0)) && ((_arg3 == 0)))){
                _local4 = (this._drag_mc.parent.mouseX - this.oldX);
                _local5 = (this._drag_mc.parent.mouseY - this.oldY);
            } else {
                _local4 = ((_arg2 - this._drag_mc.x) - 192);
                _local5 = ((_arg3 - this._drag_mc.y) + 40);
            };
            this.oldX = this._drag_mc.parent.mouseX;
            this.oldY = this._drag_mc.parent.mouseY;
            if (!this.constrain){
                this._drag_mc.x = (this._drag_mc.x + _local4);
                this._drag_mc.y = (this._drag_mc.y + _local5);
            } else {
                if ((this._drag_mc.x + _local4) > this.right){
                    _local4 = (this.right - this._drag_mc.x);
                };
                if ((this._drag_mc.x + _local4) < this.left){
                    _local4 = (this.left - this._drag_mc.x);
                };
                if ((this._drag_mc.y + _local5) > this.bottom){
                    _local5 = (this.bottom - this._drag_mc.y);
                };
                if ((this._drag_mc.y + _local5) < this.top){
                    _local5 = (this.top - this._drag_mc.y);
                };
                this._drag_mc.x = (this._drag_mc.x + _local4);
                this._drag_mc.y = (this._drag_mc.y + _local5);
            };
            this.currentChangeX = (this.currentChangeX + _local4);
            this.currentChangeY = (this.currentChangeY + _local5);
            var _local6:int;
            while (_local6 < this.resize_array.length) {
                _local7 = this.resize_array[_local6].object;
                _local8 = this.resize_array[_local6].dimension;
                if (_local8 == Resizer.WIDTH_AND_HEIGHT){
                    _local7.width = (_local7.width + _local4);
                    _local7.height = (_local7.height + _local5);
                };
                if (_local8 == Resizer.HEIGHT){
                    _local7.height = (_local7.height + _local5);
                };
                if (_local8 == Resizer.WIDTH){
                    _local7.width = (_local7.width + _local4);
                };
                if (_local8 == Resizer.VERTICLE){
                    _local7.y = (_local7.y + _local5);
                };
                if (_local8 == Resizer.HORIZONTAL){
                    _local7.x = (_local7.x + _local4);
                };
                if (_local8 == Resizer.X_AND_Y){
                    _local7.x = (_local7.x + _local4);
                    _local7.y = (_local7.y + _local5);
                };
                _local6++;
            };
            this.dispatchEvent(Resizer.RESIZING, {
                changeX:_local4,
                changeY:_local5
            });
        }
        public function resetSize(){
            if (this.changed){
            };
        }
        public function resetSizeFade(_arg1:Number=0.65){
            var _local3:Object;
            var _local4:Sprite;
            var _local5:String;
            var _local6:Tween;
            var _local2:int;
            while (_local2 < this.resize_array.length) {
                _local3 = this.resize_array[_local2];
                _local4 = _local3.object;
                _local5 = _local3.dimension;
                if (_local5 == Resizer.WIDTH_AND_HEIGHT){
                    if (_local4.height != _local3._height){
                        _local3.tween = this.animateMovieClip(_local4, "height", _local3._height, _arg1);
                    };
                    if (_local4.width != _local3._width){
                        _local3.tween = this.animateMovieClip(_local4, "width", _local3._width, _arg1);
                    };
                };
                if (_local5 == Resizer.WIDTH){
                    if (_local4.width != _local3._width){
                        _local3.tween = this.animateMovieClip(_local4, "width", _local3._width, _arg1);
                    };
                };
                if (_local5 == Resizer.HEIGHT){
                    if (_local4.height != _local3._height){
                        _local3.tween = this.animateMovieClip(_local4, "height", _local3._height, _arg1);
                    };
                };
                if (_local5 == Resizer.VERTICLE){
                    if (_local4.y != _local3._y){
                        _local3.tween = this.animateMovieClip(_local4, "y", _local3._y, _arg1);
                    };
                };
                if (_local5 == Resizer.HORIZONTAL){
                    if (_local4.x != _local3._x){
                        _local3.tween = this.animateMovieClip(_local4, "x", _local3._x, _arg1);
                    };
                };
                if (_local5 == Resizer.X_AND_Y){
                    if (_local4.x != _local3._x){
                        _local3.tween = this.animateMovieClip(_local4, "x", _local3._x, _arg1);
                        _local3.tween = this.animateMovieClip(_local4, "y", _local3._y, _arg1);
                    };
                };
                if (_local3.tween){
                    _local3.tween.addEventListener(TweenEvent.MOTION_FINISH, this.resetComplete);
                };
                _local2++;
            };
            if (this._drag_mc.x != this.dragStartX){
                this.animateMovieClip(this._drag_mc, "x", this.dragStartX, _arg1);
            };
            if (this._drag_mc.y != this.dragStartY){
                _local6 = this.animateMovieClip(this._drag_mc, "y", this.dragStartY, _arg1);
                _local6.addEventListener(TweenEvent.MOTION_FINISH, this.resetComplete);
            };
            this.changed = false;
            this.resetTimer.start();
        }
        private function resetting(_arg1:TimerEvent){
            this.dispatchEvent(Resizer.RESET_FADE);
        }
        private function animateMovieClip(_arg1:Sprite, _arg2:String, _arg3:Number, _arg4:Number):Tween{
            var _local5:Tween;
            _local5 = new Tween(_arg1, _arg2, Regular.easeInOut, _arg1[_arg2], _arg3, _arg4, true);
            return (_local5);
        }
        private function resetComplete(_arg1:TweenEvent){
            var _local3:Object;
            var _local4:Sprite;
            var _local5:String;
            var _local2:int;
            while (_local2 < this.resize_array.length) {
                _local3 = this.resize_array[_local2];
                _local4 = _local3.object;
                _local5 = _local3.dimension;
                if (_local5 == Resizer.WIDTH_AND_HEIGHT){
                    if (_local4.height != _local3._height){
                        _local4.height = _local3._height;
                    };
                    if (_local4.width != _local3._width){
                        _local4.width = _local3._width;
                    };
                };
                if (_local5 == Resizer.WIDTH){
                    if (_local4.width != _local3._width){
                        !((_local4.width == _local3._width));
                    };
                };
                if (_local5 == Resizer.HEIGHT){
                    if (_local4.height != _local3._height){
                        _local4.height = _local3._height;
                    };
                };
                if (_local5 == Resizer.VERTICLE){
                    if (_local4.y != _local3._y){
                        !((_local4.y == _local3._y));
                    };
                };
                if (_local5 == Resizer.HORIZONTAL){
                    if (_local4.x != _local3._x){
                        !((_local4.x == _local3._x));
                    };
                };
                if (_local5 == Resizer.X_AND_Y){
                    if (_local4.x != _local3._x){
                        !((_local4.x == _local3._x));
                    };
                    if (_local4.y != _local3._y){
                        !((_local4.y == _local3._y));
                    };
                };
                _local2++;
            };
            this._drag_mc["x"] = this.dragStartX;
            this._drag_mc["y"] = this.dragStartY;
            this.totalChangeX = 0;
            this.totalChangeY = 0;
            this.resetTimer.stop();
            this.dispatchEvent(Resizer.RESET_COMPLETE);
            this.isBaseSize = true;
        }
        public function addEventListener(_arg1:String, _arg2:Function){
            switch (_arg1){
                case Resizer.RESET_FADE:
                    this.resetFadeEvent_array.push(_arg2);
                    break;
                case Resizer.RESIZING:
                    this.resizeEvent_array.push(_arg2);
                    break;
                case Resizer.RESIZE_COMPLETE:
                    this.resizeCompleteEvent_array.push(_arg2);
                    break;
                case Resizer.RESET_COMPLETE:
                    this.resetCompleteEvent_array.push(_arg2);
                    break;
            };
        }
        private function dispatchEvent(_arg1:String, _arg2:Object=null){
            var _local3:int;
            var _local4:int;
            var _local5:int;
            var _local6:int;
            switch (_arg1){
                case Resizer.RESET_FADE:
                    _local3 = 0;
                    while (_local3 < this.resetFadeEvent_array.length) {
                        var _local7 = this.resetFadeEvent_array;
                        _local7[_local3](_arg2);
                        _local3++;
                    };
                    break;
                case Resizer.RESIZING:
                    _local4 = 0;
                    while (_local4 < this.resizeEvent_array.length) {
                        _local7 = this.resizeEvent_array;
                        _local7[_local4](_arg2);
                        _local4++;
                    };
                    break;
                case Resizer.RESIZE_COMPLETE:
                    _local5 = 0;
                    while (_local5 < this.resizeCompleteEvent_array.length) {
                        _local7 = this.resizeCompleteEvent_array;
                        _local7[_local5](_arg2);
                        _local5++;
                    };
                    break;
                case Resizer.RESET_COMPLETE:
                    _local6 = 0;
                    while (_local6 < this.resetCompleteEvent_array.length) {
                        _local7 = this.resetCompleteEvent_array;
                        _local7[_local6](_arg2);
                        _local6++;
                    };
                    break;
            };
        }

    }
}//package 
