﻿package {
    import flash.events.*;
    import flash.display.*;
    import flash.net.*;
    import flash.external.*;

    public class ExtensionsManager extends MovieClip {

        public var bg:MovieClip;
        public var cueCard_icon:MovieClip;
        public var extensions_mc:ExtensionsBar;
        public var extensionsName_array:Array;
        public var extensionsButton_array:Array;
        private var extensionsURL_array:Array;
        private var colorSetArray:Array;
        public var extensions_array:Array;
        private var _cueCard:AS3CueCard;
        private var extensionsBar:ExtensionsBar;
        public var currentExtensionName:String;
        private var action_array:Array;
        private var popUpText_array:Array;
        private var popUpType:String = "";

        public function setUpExtensions(_arg1, _arg2:String){
            var targetDepth:* = 0;
            var extension:* = null;
            var _name:* = null;
            var _swf:* = null;
            var _action:* = null;
            var _popUpText:* = null;
            var _disable:* = null;
            var button:* = null;
            var currentState_xml:* = _arg1;
            var _popUpType:* = _arg2;
            this.popUpType = _popUpType;
            this.extensionsButton_array = new Array();
            this.extensionsName_array = new Array();
            this.extensionsURL_array = new Array();
            this.action_array = new Array();
            this.popUpText_array = new Array();
            this.colorSetArray = new Array();
            var extensionIndex:* = 0;
            while (extensionIndex < currentState_xml.Extension.length()) {
                extension = currentState_xml.Extension[extensionIndex];
                _name = extension.attribute("name");
                _swf = extension.attribute("swf");
                _action = extension.attribute("action");
                _popUpText = null;
                try {
                    _popUpText = extension.attribute("popUpText");
                } catch(e:Error) {
                };
                this.extensionsName_array.push(_name);
                this.extensionsURL_array.push(_swf);
                this.action_array.push(_action);
                this.popUpText_array.push(_popUpText);
                _disable = null;
                if (extension.attribute("color")){
                    _disable = extension.attribute("color");
                };
                this.colorSetArray.push(_disable);
                extensionIndex = (extensionIndex + 1);
            };
            this.extensions_array = new Array();
            var index:* = 0;
            while (index < this.extensionsName_array.length) {
                button = new ExtensionsButton(this.extensionsName_array[index]);
                button.index = index;
                if (this.colorSetArray[index] != null){
                    button.setColor(this.colorSetArray[index]);
                };
                button.addListener(ExtensionsButton.CLICK, this.buttonClicked);
                this.addChild(button);
                this.extensionsButton_array.push(button);
                index = (index + 1);
            };
            targetDepth = (this.numChildren - 1);
            var index2:* = (this.extensionsButton_array.length - 1);
            while (index2 > -1) {
                this.setChildIndex(this.extensionsButton_array[index2], targetDepth);
                index2 = (index2 - 1);
            };
        }
        public function doNotDisplayButton(_arg1:String){
            var button:* = null;
            var _name:* = _arg1;
            try {
                button = this.getExtensionButton(_name);
                if (button){
                    button.visible = false;
                    this.arrangeButtons();
                };
            } catch(e:Error) {
            };
        }
        public function displayButton(_arg1:String){
            var button:* = null;
            var _name:* = _arg1;
            try {
                button = this.getExtensionButton(_name);
                if (button){
                    button.visible = true;
                    this.arrangeButtons();
                };
            } catch(e:Error) {
            };
        }
        public function getExtensionButton(_arg1:String):ExtensionsButton{
            var _local2:ExtensionsButton;
            var _local3:int;
            while (_local3 < this.extensionsName_array.length) {
                if (_arg1 == this.extensionsName_array[_local3]){
                    _local2 = this.extensionsButton_array[_local3];
                    break;
                };
                _local3++;
            };
            return (_local2);
        }
        public function isExtensionOpenedInternally():Boolean{
            if (this.extensionsBar.openInternally){
                return (true);
            };
            return (false);
        }
        private function arrangeButtons(){
            var _local2:ExtensionsButton;
            var _local3:ExtensionsButton;
            var _local1:int;
            while (_local1 < this.extensionsButton_array.length) {
                _local2 = this.extensionsButton_array[_local1];
                _local3 = this.extensionsButton_array[(_local1 - 1)];
                if (_local3){
                    if (!_local3.visible){
                        if (this.extensionsButton_array[(_local1 - 2)]){
                            _local2.x = ((this.extensionsButton_array[(_local1 - 2)].x + this.extensionsButton_array[(_local1 - 2)].width) - 1);
                        } else {
                            _local2.x = 5;
                            _local2.seporator.visible = false;
                        };
                    } else {
                        _local2.x = ((_local3.x + _local3.width) - 1);
                    };
                } else {
                    _local2.x = 5;
                    _local2.seporator.visible = false;
                };
                _local1++;
            };
        }
        public function closeExtensions(_arg1:Boolean=false){
            this.unclickButtons();
            this.currentExtensionName = null;
            this.extensionsBar.closeExtensionBar(_arg1, true);
            if (this._cueCard.videoPlayer.mezzThumbnail){
                if (this._cueCard.videoPlayer.mezzThumbnail.visible){
                    this._cueCard.videoPlayer.mezzThumbnail.showPlayButton();
                };
            };
        }
        public function openExtensionByName(_arg1){
            var _local2:ExtensionsButton = this.getExtensionButton(_arg1);
            var _local3:Object = new Object();
            _local3.button = _local2;
            this.unclickButtons(_local3);
            if (this.action_array[_local3.button.index] == "open"){
                this.getExtension(_local3.button.index, _local3.button.labelText);
            };
            if (this.action_array[_local3.button.index] == "signIn"){
                this._cueCard.signIn.init(this.popUpText_array[_local3.button.index], this.popUpType);
                this.unclickButtons();
            };
        }
        public function set cueCard(_arg1:AS3CueCard){
            this._cueCard = _arg1;
            this.extensionsBar = this._cueCard.always_mc.extensions_bar.extensions_mc;
        }
        private function buttonClicked(_arg1:Object){
            var printRequest:* = null;
            var e:* = _arg1;
            if (this.currentExtensionName == e.button.labelText){
                this.closeExtensions();
                return;
            };
            this.currentExtensionName = e.button.labelText;
            this.unclickButtons(e);
            if (this.action_array[e.button.index] == "open"){
                this.getExtension(e.button.index, e.button.labelText);
            };
            if (this.action_array[e.button.index] == "signIn"){
                this._cueCard.signIn.init(this.popUpText_array[e.button.index], this.popUpType);
                this.unclickButtons();
            };
            if (this.action_array[e.button.index] == "url"){
                if (e.button.labelText == "PRINT"){
                    try {
                        printRequest = new URLRequest(this._cueCard.metaData.flatviewURL);
                        navigateToURL(printRequest, "_blank");
                        this.currentExtensionName = null;
                        this.unclickButtons();
                    } catch(e:Error) {
                    };
                };
                if (e.button.labelText == "STANDARDS"){
                    this.currentExtensionName = null;
                    this.unclickButtons();
                    ExternalInterface.call("CueCardManager.openStateStandardsForCueCard", this._cueCard.metaData.Asset_ID);
                };
                if (e.button.labelText == "SAVE"){
                    this.currentExtensionName = null;
                    this.unclickButtons();
                    this.saveCueCardToBBPlaylist();
                };
                if (e.button.labelText == "EMBED"){
                    this.currentExtensionName = null;
                    this.unclickButtons();
                    try {
                        trace(("_cueCard.metaData.blackboardSystem: " + this._cueCard.metaData.blackboardSystem));
                        if (this._cueCard.metaData.blackboardSystem.toUpperCase() == "CANVAS"){
                            this.embedThisCueCardInCANVAS();
                        } else {
                            this.embedThisCueCardInBlackboard();
                        };
                    } catch(e:Error) {
                        trace("No blackboard systemID");
                        embedThisCueCardInBlackboard();
                    };
                };
            };
            if (this.action_array[e.button.index] == "none"){
                this.unclickButtons();
            };
        }
        private function setVideoParameters():URLVariables{
            var _local1:URLVariables = new URLVariables();
            _local1.type = "save";
            _local1.return_url = this._cueCard.stage.loaderInfo.parameters.return_url;
            if (this._cueCard.metaData.blackboardSystem.toUpperCase() == "OLE"){
                _local1.video_id = this._cueCard.metaData.cardID;
            } else {
                _local1.video_id = this._cueCard.metaData.vcmID;
            };
            _local1.name = this.cleanPunctuation(this._cueCard.metaData.title);
            _local1.description = this.cleanPunctuation(this._cueCard.metaData.description);
            _local1.air_date = this._cueCard.metaData.xml.Media_Date;
            if (this._cueCard.metaData.blackboardSystem){
                _local1.lmsSystem = this._cueCard.metaData.blackboardSystem;
            };
            if (this._cueCard.metaData.ClipLength){
                _local1.duration = this._cueCard.metaData.ClipLength;
            } else {
                _local1.duration = "";
            };
            _local1.content_type = this._cueCard.metaData.AssetType;
            if (this._cueCard.metaData.SmallThumbnailUrl){
                _local1.thumbnail_url = this._cueCard.metaData.SmallThumbnailUrl;
            } else {
                if (this._cueCard.metaData.SearchThumbnail130x100){
                    _local1.thumbnail_url = this._cueCard.metaData.SearchThumbnail130x100;
                } else {
                    _local1.thumbnail_url = this._cueCard.metaData.MainThumbnail;
                };
            };
            _local1.keywords = this.cleanPunctuation(this._cueCard.metaData.KeyWords);
            return (_local1);
        }
        private function couldNotSave(_arg1:Object=null){
            this.handleReturnedJSON("nothing");
        }
        private function saveCueCardToBBPlaylist(){
            var saveXMLLoader:* = null;
            var request:* = new URLRequest(this._cueCard.stage.loaderInfo.parameters.save_url);
            request.method = URLRequestMethod.GET;
            request.data = this.setVideoParameters();
            saveXMLLoader = new URLLoader();
            saveXMLLoader.dataFormat = URLLoaderDataFormat.TEXT;
            saveXMLLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.couldNotSave);
            saveXMLLoader.addEventListener(IOErrorEvent.NETWORK_ERROR, this.couldNotSave);
            saveXMLLoader.addEventListener(IOErrorEvent.IO_ERROR, this.couldNotSave);
            saveXMLLoader.addEventListener(Event.COMPLETE, function (_arg1:Event){
                _cueCard.setLoadingMessage(false, "Saving...");
                handleReturnedJSON(saveXMLLoader.data);
            });
            this._cueCard.setLoadingMessage(true, "Saving...");
            this._cueCard.signalNetworkTraffic("SaveToBB");
            saveXMLLoader.load(request);
        }
        private function handleReturnedJSON(_arg1){
            var playLibrary:* = null;
            var messaging:* = null;
            var _data:* = _arg1;
            var returned_message:* = (String(_data) as String);
            var returned_array:* = returned_message.split("success");
            if (this._cueCard.metaData.blackboardSystem.toUpperCase() == "OLE"){
                playLibrary = "OLE library";
            } else {
                playLibrary = "playlist";
            };
            if (returned_array.length > 1){
                messaging = (("You have successfully saved this video to your " + playLibrary) + ".");
            } else {
                messaging = (("There was a problem saving this video to your " + playLibrary) + ".  Please try again.");
            };
            this._cueCard.confirmation.confirm(messaging, "OK", function (){
                _cueCard.setMainFocus(_cueCard.confirmation.button_1, getExtensionButton("SAVE").hit_box);
            }, true);
            this._cueCard.signalNetworkTrafficStop("SaveToBB");
        }
        private function embedThisCueCardInLTI(){
            return ("<iframe%26width%3D500%26height%3D390%26url%3Dhttp%3A%2F%2Farchivesbb.nbclearn.com%2Fportal%2Fsite%2Froot%2Fwidget%2F-8aDmcvw0ub3lhVtfTSx7A/>");
        }
        private function embedThisCueCardInCANVAS(){
            var _local1:String = ((((this._cueCard.stage.loaderInfo.parameters.return_url + "/") + this._cueCard.metaData.cardID) + "&title=") + escape(this.cleanPunctuation(this._cueCard.metaData.title)));
            var _local2:URLRequest = new URLRequest(_local1);
            _local2.method = URLRequestMethod.GET;
            navigateToURL(_local2, "_self");
        }
        private function embedThisCueCardInBlackboard(){
            trace("embed in Blackboard");
            this._cueCard.videoPlayer.stop();
            var _local1:URLRequest = new URLRequest(this._cueCard.stage.loaderInfo.parameters.return_url);
            _local1.method = URLRequestMethod.POST;
            var _local2:URLVariables = new URLVariables();
            _local2.type = "embed";
            _local2.video_id = this._cueCard.metaData.vcmID;
            _local2.name = this.cleanPunctuation(this._cueCard.metaData.title);
            _local2.description = this.cleanPunctuation(this._cueCard.metaData.description);
            _local2.air_date = this._cueCard.metaData.xml.Media_Date;
            if (this._cueCard.metaData.ClipLength){
                _local2.duration = this._cueCard.metaData.ClipLength;
            } else {
                _local2.duration = "";
            };
            _local2.content_type = this._cueCard.metaData.AssetType;
            if (this._cueCard.metaData.SmallThumbnailUrl){
                _local2.thumbnail_url = this._cueCard.metaData.SmallThumbnailUrl;
            } else {
                if (this._cueCard.metaData.SearchThumbnail130x100){
                    _local2.thumbnail_url = this._cueCard.metaData.SearchThumbnail130x100;
                } else {
                    _local2.thumbnail_url = this._cueCard.metaData.MainThumbnail;
                };
            };
            _local1.data = _local2;
            navigateToURL(_local1, "_self");
        }
        private function closeFromExtensions(_arg1:Object=null){
            this.closeExtensions();
        }
        private function getExtension(_arg1:int, _arg2:String){
            var extension:* = undefined;
            var request:* = null;
            var loader:* = null;
            var _index:* = _arg1;
            var _extensionName:* = _arg2;
            if (this.extensions_array[_extensionName]){
                extension = this.extensions_array[_extensionName];
                this.extensionsBar.loadExtension(extension);
            } else {
                try {
                    request = new URLRequest((this._cueCard.folderDirectory + String(this.extensionsURL_array[_index])));
                    loader = new Loader();
                    loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function (_arg1:Event){
                        var _local2:Object = loader.content;
                        _local2.addEventListener("CLOSE", closeFromExtensions);
                        extensions_array[_extensionName] = _local2;
                        extensionsBar.loadExtension(extensions_array[_extensionName]);
                    });
                    loader.load(request);
                } catch(error:Error) {
                    trace(("~FAILED TO LOAD EXTENSION: " + _extensionName));
                    extensionsBar.loadExtensionLabel(_extensionName);
                };
            };
        }
        private function cleanPunctuation(_arg1:String):String{
            _arg1 = this.findReplace("‘", "'", _arg1);
            _arg1 = this.findReplace("’", "'", _arg1);
            _arg1 = this.findReplace("“", "\"", _arg1);
            _arg1 = this.findReplace("”", "\"", _arg1);
            return (_arg1);
        }
        public function findReplace(_arg1:String, _arg2:String, _arg3:String){
            if (_arg2 == "_blank"){
                _arg2 = "";
            };
            if (_arg3.indexOf(_arg1) > -1){
                return (_arg3.split(_arg1).join(_arg2));
            };
            return (_arg3);
        }
        private function unclickButtons(_arg1:Object=null){
            var index:* = 0;
            var button:* = null;
            var e = _arg1;
            try {
                index = 0;
                while (index < this.extensionsButton_array.length) {
                    button = this.extensionsButton_array[index];
                    if (e == null){
                        button.buttonDeselected();
                    } else {
                        if (button != e.button){
                            button.buttonDeselected();
                        };
                    };
                    index = (index + 1);
                };
            } catch(e:Error) {
            };
        }

    }
}//package 
