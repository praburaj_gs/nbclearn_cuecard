﻿package {
    import flash.events.*;
    import flash.display.*;
    import flash.net.*;
    import flash.text.*;

    public class PlayListThumbnail extends MovieClip {

        public var title_txt:TextField;
        public var close_btn:CLOSE_BTN;
        public var data:Object;
        public var metaData;
        public var imageLoader:Loader;
        private var title:TextField;
        private var image:Sprite;
        private var playListWindow:PlayListWindow;

        public function PlayListThumbnail(_arg1:Object, _arg2){
            this.playListWindow = _arg2;
            this.image = new Sprite();
            this.data = _arg1;
            this.metaData = this.data.Metadata;
            this.initListeners();
        }
        private function initListeners(){
            this.close_btn.addEventListener(MouseEvent.CLICK, this.deleteListener);
            this.image.addEventListener(MouseEvent.CLICK, this.thumbnailClicked);
            this.title_txt.addEventListener(MouseEvent.CLICK, this.thumbnailClicked);
            this.image.buttonMode = true;
            this.image.useHandCursor = true;
        }
        private function deleteListener(_arg1:MouseEvent){
            this.playListWindow.cueCard.confirmation.confirm("Are you sure you want to delete this Cue Card?", "Delete", this.deleteConfirmed);
        }
        private function deleteConfirmed(_arg1:Event=null){
            this.playListWindow.deleteThumb(this);
        }
        public function remove(){
            this.data = null;
            this.metaData = null;
            this.imageLoader = null;
            this.image = null;
            this.playListWindow = null;
            this.close_btn.removeEventListener(MouseEvent.CLICK, this.deleteListener);
        }
        private function thumbnailClicked(_arg1:MouseEvent){
            this.playListWindow.playVideo(this.data.Asset_Id);
        }
        public function loadImage(_arg1:String){
            var _local2:URLRequest = new URLRequest(_arg1);
            this.imageLoader = new Loader();
            this.imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, this.buildThumbnail);
            this.imageLoader.load(_local2);
            addChild(this.image);
            this.image.addChild(this.imageLoader);
        }
        public function getHeight():Number{
            return ((this.image.height as Number));
        }
        public function getWidth():Number{
            return ((this.close_btn.x + 15));
        }
        private function buildThumbnail(_arg1:Event){
            var _local2:* = _arg1.currentTarget;
            this.image.width = 65;
            this.image.height = 51;
            this.title_txt.width = 132;
            this.title_txt.height = 52;
            this.title_txt.wordWrap = true;
            this.title_txt.text = this.data.Title;
            this.title_txt.x = (this.image.width + 5);
            this.close_btn.x = ((this.title_txt.x + this.title_txt.width) + 10);
            this.playListWindow.arrangeContent();
        }

    }
}//package 
