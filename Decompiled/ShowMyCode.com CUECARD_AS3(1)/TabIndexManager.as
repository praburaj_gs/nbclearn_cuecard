﻿package {
    import flash.display.*;
    import flash.system.*;
    import flash.accessibility.*;

    public class TabIndexManager {

        private static const _instance:TabIndexManager = new TabIndexManager(SingletonLock);
;

        private var itemList:Array;
        private var associativeArray:Object;

        public function TabIndexManager(_arg1:Class):void{
            if (_arg1 != SingletonLock){
                throw (new Error("Invalid access of singleton class TabIndexManager.  Use TabIndexManager.instance to instantiate class."));
            };
            this.itemList = new Array();
            this.associativeArray = new Object();
        }
        public static function get instance():TabIndexManager{
            return (_instance);
        }

        public function get numTabs():int{
            return (this.itemList.length);
        }
        public function setTab(_arg1:InteractiveObject, _arg2:int=1, _arg3:Boolean=false):void{
            var _local4:DisplayObjectContainer;
            var _local9:int;
            var _local5:Boolean = _arg3;
            if ((_arg1 is DisplayObjectContainer)){
                _local4 = (_arg1 as DisplayObjectContainer);
            } else {
                _local5 = false;
            };
            if (_arg2 > (this.itemList.length + 1)){
                _arg2 = (this.itemList.length + 1);
            };
            var _local6:Array = new Array();
            if (_local5 == true){
                if (_local4.numChildren <= 1){
                    _local4.tabEnabled = true;
                };
                this.collectTabableChildren(_local4, _local6);
                _local6.sortOn("tabIndex");
                this.makeSequential(_local6);
            } else {
                _arg1.tabEnabled = true;
                _local6.push(_arg1);
            };
            var _local7:Array = this.itemList.splice(0, (_arg2 - 1));
            var _local8:int;
            while (_local8 < _local6.length) {
                _local9 = 0;
                while (_local9 < this.itemList.length) {
                    if (this.itemList[_local9] == _local6[_local8]){
                        this.itemList.splice(_local9, 1);
                    };
                    _local9++;
                };
                _local9 = 0;
                while (_local9 < _local7.length) {
                    if (_local7[_local9] == _local6[_local8]){
                        _local7.splice(_local9, 1);
                    };
                    _local9++;
                };
                _local7.push(_local6[_local8]);
                this.associativeArray[_local6[_local8].name] = _local6[_local8];
                _local8++;
            };
            _local7 = _local7.concat(this.itemList);
            this.itemList = _local7;
            this.makeSequential(this.itemList);
            if (Capabilities.hasAccessibility){
                if (Accessibility.active){
                    Accessibility.updateProperties();
                };
            };
        }
        public function removeItemAtIndex(_arg1:int, _arg2:Boolean=false):Array{
            var _local4:Array;
            var _local5:Array;
            var _local6:int;
            var _local7:int;
            var _local8:int;
            _arg1--;
            var _local3:InteractiveObject = this.itemList[_arg1];
            if ((_local3 is DisplayObjectContainer) == false){
                _arg2 = false;
            };
            if (_arg1 > this.itemList.length){
                throw (new Error((("error in TabIndexManager.removeItemAtIndex- targetIndex is out of range.  TabIndexManager contains only " + this.itemList.length) + " items")));
            };
            if (_arg1 < 0){
                throw (new Error("error in TabIndexManager.removeItemAtIndex - targetIndex is less than 1.  tabIndex starts at 1"));
            };
            _local4 = new Array();
            _local5 = new Array();
            if (_arg2 == true){
                this.collectTabableChildren(this.itemList[_arg1], _local5);
                _local5.sortOn("tabIndex");
            } else {
                _local5.push(this.itemList[_arg1]);
            };
            _local6 = _local5.length;
            _local7 = 0;
            while (_local7 < _local6) {
                _local8 = (_local5[_local7].tabIndex - (_local7 + 1));
                _local4.push(this.deleteItem(_local8));
                _local7++;
            };
            this.itemList = this.makeSequential(this.itemList);
            if (Capabilities.hasAccessibility){
                if (Accessibility.active){
                    Accessibility.updateProperties();
                };
            };
            return (_local4);
        }
        public function removeItemByName(_arg1:String, _arg2:Boolean=false):Array{
            var _local3:int = this.associativeArray[_arg1].tabIndex;
            return (this.removeItemAtIndex(_local3, _arg2));
        }
        public function removeAll():void{
            while (this.numTabs > 0) {
                this.removeItemAtIndex(1);
            };
        }
        public function getItemAtIndex(_arg1:int):InteractiveObject{
            _arg1--;
            return (InteractiveObject(this.itemList[_arg1]));
        }
        public function getByName(_arg1:String):InteractiveObject{
            return (InteractiveObject(this.associativeArray[_arg1]));
        }
        public function getNext(_arg1:int):InteractiveObject{
            if (_arg1 >= this.itemList.length){
                _arg1 = 0;
            };
            return (InteractiveObject(this.itemList[_arg1]));
        }
        public function getPrev(_arg1:int):InteractiveObject{
            _arg1 = (_arg1 - 2);
            if (_arg1 < 0){
                _arg1 = (this.itemList.length - 1);
            };
            return (InteractiveObject(this.itemList[_arg1]));
        }
        public function traceList():void{
            trace("-----------begin trace list---------------");
            var _local1:int = this.itemList.length;
            trace((_local1 + " items"));
            var _local2:int;
            while (_local2 < _local1) {
                trace(((this.itemList[_local2].name + " - tab index = ") + this.itemList[_local2].tabIndex));
                _local2++;
            };
            trace("-----------end trace list---------------");
        }
        private function deleteItem(_arg1:int):InteractiveObject{
            var _local2:Array = this.itemList.splice(_arg1, 1);
            var _local3:int;
            while (_local3 < _local2.length) {
                _local2[_local3].tabEnabled = false;
                _local2[_local3].tabIndex = -1;
                _local3++;
            };
            delete this.associativeArray[_local2[0].name];
            return (_local2[0]);
        }
        private function collectTabableChildren(_arg1:InteractiveObject, _arg2:Array):void{
            var _local3:*;
            var _local4:int;
            var _local5:int;
            if ((_arg1 is DisplayObjectContainer)){
                _local3 = (_arg1 as DisplayObjectContainer);
                if ((((_local3.tabChildren == true)) && ((_local3.numChildren >= 1)))){
                    _local4 = _local3.numChildren;
                    _local5 = 0;
                    while (_local5 < _local4) {
                        this.collectTabableChildren((_local3.getChildAt(_local5) as DisplayObjectContainer), _arg2);
                        _local5++;
                    };
                };
                if (((_local3.tabEnabled) || ((_local3.tabIndex >= 0)))){
                    _arg2.push(_local3);
                };
            };
        }
        private function makeSequential(_arg1:Array, _arg2:int=0):Array{
            var _local3:int = _arg1.length;
            var _local4:int = _arg2;
            while (_local4 < _local3) {
                _arg1[_local4].tabIndex = (_local4 + 1);
                _local4++;
            };
            return (_arg1);
        }
        private function moveNegativeToEnd(_arg1:InteractiveObject, _arg2:int, _arg3:Array):Boolean{
            var _local4:*;
            if (_arg1.tabIndex >= 0){
                return (false);
            };
            _local4 = _arg3[_arg2];
            _arg3.splice(_arg2, 1);
            _arg3.push(_local4);
            return (true);
        }

    }
}//package 

class SingletonLock {

    public function SingletonLock(){
    }
}
