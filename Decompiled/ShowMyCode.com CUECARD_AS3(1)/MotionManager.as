﻿package {
    import flash.events.*;
    import flash.display.*;
    import fl.transitions.*;
    import fl.transitions.easing.*;

    public class MotionManager {

        private var prevX:Number;
        private var prevY:Number;
        private var directionX:int;
        private var directionY:int;
        private var canDragX:Boolean = false;
        private var canDragY:Boolean = false;
        private var canDrag:Boolean = false;
        public var preventDragging:Boolean = false;
        private var baseClip:DisplayObject;
        private var cueCard:AS3CueCard;
        private var vidX:Number;
        private var vidY:Number;
        private var vidWidth:Number;
        private var vidHeight:Number;

        public function MotionManager(_arg1:DisplayObject, _arg2:AS3CueCard){
            this.baseClip = _arg1;
            this.cueCard = _arg2;
            this.cueCard.always_mc.mainDragBox.addEventListener(MouseEvent.MOUSE_DOWN, this.startDragging);
            this.cueCard.always_mc.mainDragBox.addEventListener(MouseEvent.ROLL_OVER, this.rollOver);
            this.cueCard.always_mc.mainDragBox.addEventListener(MouseEvent.ROLL_OUT, this.rollOut);
            this.cueCard.always_mc.gripper.addEventListener(MouseEvent.MOUSE_DOWN, this.startDragging);
            this.cueCard.always_mc.gripper.addEventListener(MouseEvent.ROLL_OVER, this.rollOver);
            this.cueCard.always_mc.gripper.addEventListener(MouseEvent.ROLL_OUT, this.rollOut);
        }
        private function rollOver(_arg1:MouseEvent){
            if (!this.cueCard.imageMode){
                if (this.cueCard.videoPlayer){
                    this.cueCard.videoPlayer.fadeInVideoControls();
                };
            } else {
                if (this.cueCard.imageViewer){
                    this.cueCard.imageViewer.fadeInImageControls();
                };
            };
        }
        private function rollOut(_arg1:MouseEvent){
            if (!this.cueCard.imageMode){
                if (this.cueCard.videoPlayer){
                    this.cueCard.videoPlayer.fadeOutVideoControls();
                };
            } else {
                if (this.cueCard.imageViewer){
                    this.cueCard.imageViewer.fadeOutImageControls();
                };
            };
        }
        private function startDragging(_arg1:MouseEvent){
            if (this.preventDragging){
                return;
            };
            this.cueCard.stage.addEventListener(MouseEvent.MOUSE_UP, this.stopDragging);
            this.vidX = (this.cueCard.front_mc.x + this.cueCard.front_mc.panels_mask.x);
            this.vidY = (this.cueCard.front_mc.y + this.cueCard.front_mc.panels_mask.y);
            this.vidWidth = this.cueCard.front_mc.content_mc.blackBackround.width;
            this.vidHeight = this.cueCard.front_mc.content_mc.blackBackround.height;
            this.cueCard.stage.addEventListener(MouseEvent.MOUSE_MOVE, this.videoFollowsMouse);
            this.cueCard.startDrag();
        }
        private function videoFollowsMouse(_arg1:MouseEvent){
            if (this.cueCard.videoPlayer.video_helper.stageVideoUsed){
                this.cueCard.videoPlayer.video_helper.x = (this.cueCard.x + this.vidX);
                this.cueCard.videoPlayer.video_helper.y = (this.cueCard.y + this.vidY);
            };
        }
        private function stopDragging(_arg1:MouseEvent){
            this.cueCard.stage.removeEventListener(MouseEvent.MOUSE_UP, this.stopDragging);
            this.cueCard.stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.videoFollowsMouse);
            this.cueCard.stopDrag();
            this.dontLoseCueCard();
            this.cueCard.videoPlayer.resize(this.cueCard.front_mc.content_mc.blackBackround.width, this.cueCard.front_mc.content_mc.blackBackround.height);
        }
        public function getEmptyScreenSpace():Object{
            var _local1:Number = (this.cueCard.stage.stageWidth - 10);
            var _local2:Number = (this.cueCard.stage.stageHeight - 10);
            var _local3:Number = this.cueCard.shadowBackground.height;
            var _local4:Number = this.cueCard.shadowBackground.width;
            var _local5:Number = (_local1 - _local4);
            var _local6:Number = (_local2 - _local3);
            return ({
                width:_local5,
                height:_local6
            });
        }
        public function keepOnScreen(_arg1:Number, _arg2:Number, _arg3:Number):void{
            var animateTween:* = null;
            var stageWidth:* = NaN;
            var cueCardRight:* = NaN;
            var differenceX:* = NaN;
            var stageHeight:* = NaN;
            var cueCardBottom:* = NaN;
            var differenceY:* = NaN;
            var changeX:* = _arg1;
            var changeY:* = _arg2;
            var time:* = _arg3;
            if (changeX != 0){
                stageWidth = (this.cueCard.stage.stageWidth - 10);
                cueCardRight = ((this.cueCard.x + this.cueCard.LEFT) + this.cueCard.shadowBackground.width);
                if ((cueCardRight + changeX) > stageWidth){
                    differenceX = ((cueCardRight + changeX) - stageWidth);
                    animateTween = this.animateMovieClip(this.cueCard, "x", (this.cueCard.x - differenceX), time);
                };
            };
            if (changeY != 0){
                stageHeight = (this.cueCard.stage.stageHeight - 10);
                cueCardBottom = (this.cueCard.y + this.cueCard.shadowBackground.height);
                if ((cueCardBottom + changeY) > stageHeight){
                    differenceY = ((cueCardBottom + changeY) - stageHeight);
                    animateTween = this.animateMovieClip(this.cueCard, "y", (this.cueCard.y - differenceY), time);
                };
            };
            if (this.cueCard.videoPlayer.video_helper.stageVideoUsed){
                if (animateTween){
                    animateTween.addEventListener(TweenEvent.MOTION_CHANGE, function (_arg1:TweenEvent){
                        cueCard.videoPlayer.resize(cueCard.front_mc.content_mc.blackBackround.width, cueCard.front_mc.content_mc.blackBackround.height);
                    });
                };
            };
        }
        private function animateMovieClip(_arg1:Sprite, _arg2:String, _arg3:Number, _arg4:Number):Tween{
            var _local5:Tween;
            _local5 = new Tween(_arg1, _arg2, Regular.easeInOut, _arg1[_arg2], _arg3, _arg4, true);
            return (_local5);
        }
        public function dontLoseCueCard(){
            var animateTween:* = null;
            var cueCardHeight:* = this.cueCard.shadowBackground.height;
            var cueCardWidth:* = this.cueCard.shadowBackground.width;
            var cueCardBottom:* = (this.cueCard.y + cueCardHeight);
            var cueCardRight:* = ((this.cueCard.x + this.cueCard.LEFT) + cueCardWidth);
            var stageHeight:* = this.cueCard.stage.stageHeight;
            var stageWidth:* = this.cueCard.stage.stageWidth;
            var targetX:* = ((stageWidth - this.cueCard.LEFT) - cueCardWidth);
            var targetY:* = (stageHeight - (cueCardHeight + 10));
            if ((cueCardBottom + 10) > stageHeight){
                animateTween = this.animateMovieClip(this.cueCard, "y", targetY, 1);
            };
            if (cueCardRight > stageWidth){
                animateTween = this.animateMovieClip(this.cueCard, "x", targetX, 1);
            };
            if ((this.cueCard.x + this.cueCard.LEFT) < -40){
                animateTween = this.animateMovieClip(this.cueCard, "x", -(this.cueCard.LEFT), 1);
            };
            if (this.cueCard.y < -40){
                animateTween = this.animateMovieClip(this.cueCard, "y", 0, 1);
            };
            if (this.cueCard.videoPlayer.video_helper.stageVideoUsed){
                if (animateTween){
                    animateTween.addEventListener(TweenEvent.MOTION_CHANGE, function (_arg1:TweenEvent){
                        cueCard.videoPlayer.resize(cueCard.front_mc.content_mc.blackBackround.width, cueCard.front_mc.content_mc.blackBackround.height);
                    });
                };
            };
        }

    }
}//package 
