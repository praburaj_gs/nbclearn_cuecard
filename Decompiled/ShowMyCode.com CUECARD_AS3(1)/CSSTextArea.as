﻿package {
    import fl.controls.*;

    public class CSSTextArea extends TextArea {

        override protected function drawTextFormat():void{
            if (!this.textField.styleSheet){
                super.drawTextFormat();
            } else {
                setEmbedFont();
                if (_html){
                    textField.htmlText = _savedHTML;
                };
            };
        }

    }
}//package 
