﻿package {
    import flash.events.*;
    import flash.display.*;
    import flash.text.*;

    public class ExtensionsButton extends Sprite {

        public static const CLICK:String = "clicked";

        public var seporator:Devider;
        public var hit_box:BOX;
        public var bgSelected:MovieClip;
        public var bg:MovieClip;
        public var button_txt:TextField;
        public var labelText:String;
        public var index:int;
        private var clickFunction:Function;
        private var defaultTextFmt:TextFormat;
        private var selectedTextFmt:TextFormat;
        var dissabledColor:uint = 9278620;
        var enabledColor:uint = 0x494949;
        private var state:String;
        public var selected:Boolean = false;
        private var clickEvent_array:Array;

        public function ExtensionsButton(_arg1:String, _arg2:Function=null){
            this.clickEvent_array = new Array();
            this.labelText = _arg1;
            this.clickFunction = _arg2;
            this.bg.visible = false;
            this.bgSelected.visible = false;
            this.initEvents();
            this.setUpText();
        }
        private function setUpText(){
            this.button_txt.autoSize = TextFieldAutoSize.LEFT;
            this.button_txt.text = this.labelText;
            this.bg.width = ((6 + this.button_txt.width) + 3);
            this.bgSelected.width = this.bg.width;
            this.hit_box.width = this.bg.width;
            this.defaultTextFmt = this.button_txt.getTextFormat();
            this.selectedTextFmt = this.button_txt.getTextFormat();
            this.selectedTextFmt.color = 0xFFFFFF;
        }
        public function setColor(_arg1:String){
            this.state = _arg1;
            if (_arg1 == "disabled"){
                this.defaultTextFmt.color = this.dissabledColor;
                this.selectedTextFmt.color = 0xDDDDDD;
                this.button_txt.setTextFormat(this.defaultTextFmt);
            };
        }
        private function initEvents(){
            this.hit_box.buttonMode = true;
            this.hit_box.useHandCursor = true;
            this.hit_box.addEventListener(MouseEvent.ROLL_OVER, this.rollOverState);
            this.hit_box.addEventListener(MouseEvent.ROLL_OUT, this.rollOutState);
            this.hit_box.addEventListener(MouseEvent.CLICK, this.clickEvent);
        }
        private function rollOverState(_arg1:MouseEvent){
            if (!this.selected){
                if (this.state != "disabled"){
                    this.bg.visible = true;
                    this.button_txt.setTextFormat(this.selectedTextFmt);
                };
            };
        }
        private function rollOutState(_arg1:MouseEvent){
            if (!this.selected){
                if (this.state != "disabled"){
                    this.bg.visible = false;
                    this.button_txt.setTextFormat(this.defaultTextFmt);
                };
            };
        }
        private function clickEvent(_arg1:MouseEvent=null){
            _arg1.preventDefault();
            if (!this.selected){
                this.buttonSelected();
            } else {
                this.buttonDeselected(true);
            };
            this.dispatch(ExtensionsButton.CLICK, {button:this});
        }
        private function buttonSelected(){
            this.bg.visible = true;
            this.button_txt.setTextFormat(this.defaultTextFmt);
            this.selected = true;
            this.bg.visible = false;
            this.bgSelected.visible = true;
        }
        public function buttonDeselected(_arg1:Boolean=false){
            if (!this.selected){
                return;
            };
            this.selected = false;
            this.bgSelected.visible = false;
            if (_arg1){
                this.bg.visible = true;
                this.button_txt.setTextFormat(this.selectedTextFmt);
            } else {
                this.bg.visible = false;
                this.button_txt.setTextFormat(this.defaultTextFmt);
            };
        }
        public function addListener(_arg1:String, _arg2:Function){
            switch (_arg1){
                case ExtensionsButton.CLICK:
                    this.clickEvent_array.push(_arg2);
                    break;
            };
        }
        private function dispatch(_arg1:String, _arg2:Object=null){
            var _local3:int;
            switch (_arg1){
                case ExtensionsButton.CLICK:
                    trace("DISPATCH: CLICK");
                    _local3 = 0;
                    while (_local3 < this.clickEvent_array.length) {
                        var _local4 = this.clickEvent_array;
                        _local4[_local3](_arg2);
                        _local3++;
                    };
                    break;
            };
        }

    }
}//package 
