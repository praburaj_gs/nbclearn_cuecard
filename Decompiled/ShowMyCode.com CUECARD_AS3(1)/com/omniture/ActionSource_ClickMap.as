﻿package com.omniture {
    import flash.events.*;
    import flash.display.*;
    import flash.geom.*;
    import flash.external.*;

    public dynamic class ActionSource_ClickMap {

        private static var isExternalSet:Boolean = false;

        private var actionSource:Object;

        public function ActionSource_ClickMap(_arg1:Object){
            this.actionSource = _arg1;
            this.actionSource.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
            if (((ExternalInterface.available) && (!(isExternalSet)))){
                isExternalSet = true;
                ExternalInterface.addCallback("s_getDOMIndex", getDOMIndex);
                ExternalInterface.addCallback("s_getTrackClickMap", getTrackClickMap);
                ExternalInterface.addCallback("s_getAccount", getAccount);
                ExternalInterface.addCallback("s_getPageName", getPageName);
                ExternalInterface.addCallback("s_getPageURL", getPageURL);
                ExternalInterface.addCallback("s_getMovieID", getMovieID);
                ExternalInterface.addCallback("s_getVersion", getVersion);
                ExternalInterface.addCallback("s_getCharSet", getCharSet);
                ExternalInterface.addCallback("s_getSWFURL", getSWFURL);
            };
        }
        public function getSWFURL():String{
            return (actionSource.root.loaderInfo.loaderURL);
        }
        public function getVersion():String{
            return (actionSource.version);
        }
        public function getDOMIndex():String{
            return (indexChildren(DisplayObjectContainer(actionSource.root)));
        }
        private function onAddedToStage(_arg1:Event):void{
            actionSource.root.addEventListener(MouseEvent.CLICK, onMouseClick, true, 0, true);
        }
        private function getFullPath(_arg1:DisplayObject):String{
            var _local2:Array;
            _local2 = new Array();
            do  {
                _local2.splice(0, 0, _arg1.name);
                _arg1 = _arg1.parent;
            } while (_arg1.parent != null);
            return (_local2.join("."));
        }
        public function getTrackClickMap():String{
            return (actionSource.trackClickMap.toString());
        }
        private function indexChildren(_arg1:DisplayObjectContainer):String{
            var _local2:String;
            var _local3:Number;
            var _local4:DisplayObject;
            var _local5:Point;
            _local2 = new String();
            _local3 = 0;
            while (_local3 < _arg1.numChildren) {
                _local4 = _arg1.getChildAt(_local3);
                _local5 = _local4.localToGlobal(new Point(_local4.x, _local4.y));
                _local2 = (_local2 + (((((((((getFullPath(_local4) + ",") + _local5.x) + ",") + _local5.y) + ",") + _local4.width) + ",") + _local4.height) + "|"));
                if ((_local4 is DisplayObjectContainer)){
                    _local2 = (_local2 + indexChildren(DisplayObjectContainer(_local4)));
                };
                _local3++;
            };
            return (_local2);
        }
        public function getMovieID():String{
            return (actionSource.movieID);
        }
        public function getCharSet():String{
            return (actionSource.charSet);
        }
        public function getAccount():String{
            return (actionSource.account);
        }
        public function getPageURL():String{
            return (actionSource.pageURL);
        }
        private function onMouseClick(_arg1:MouseEvent):void{
            var _local2:InteractiveObject;
            if (actionSource.trackClickMap){
                _local2 = InteractiveObject(_arg1.target);
                actionSource.objectID = getFullPath(_local2);
            };
            if (actionSource.autoTrack){
                actionSource.track();
            };
        }
        public function getPageName():String{
            return (actionSource.pageName);
        }

    }
}//package com.omniture 
