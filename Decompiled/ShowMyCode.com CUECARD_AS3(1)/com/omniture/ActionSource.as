﻿package com.omniture {
    import flash.events.*;
    import flash.display.*;
    import flash.net.*;
    import flash.utils.*;
    import flash.system.*;
    import flash.external.*;

    public dynamic class ActionSource extends Sprite {

        private var onLoadTracked:Boolean = false;
        public var account:String;
        public var _movie:Object;
        private var delayTrackingInterval:Number;
        public var movieID:String;
        public var debugTracking:Boolean = false;
        public var flashASVersion:Number = 3;
        private var _moduleMediaVariables:Object;
        public var otherVariables:Object;
        public var pageURL:String;
        private var requestNum:Number;
        public var dc:Number;
        public var visitorNamespace:String;
        private var trackCalled:Boolean = false;
        private var flashVersion:Number;
        private var _trackOnLoad:Boolean = false;
        public var ClickMap:ActionSource_ClickMap;
        private var version:String;
        public var trackingServer:String;
        public var autoTrack:Boolean;
        public var trackLocal:Boolean = true;
        public var trackingServerSecure:String;
        private var flashRoot:Object;
        private var accountConfigList:Array;
        public var lastRequest:String;
        public var trackClickMap:Boolean;
        private var flashLivePreview:Boolean = false;
        public var charSet:String;
        public var delayTracking:Number;
        private var trackOnLoadInterval:Number;
        private var accountVarList:Array;
        public var pageName:String;
        private var _root:Object;
        private var requiredVarList:Array;
        private var delayTrackingStage:Number;
        public var requestList:Array;
        public var Media:ActionSource_Module_Media;

        public function ActionSource(){
            var _local1:Object;
            var _local2:Number;
            var _local3:String;
            var _local4:Array;
            flashASVersion = 3;
            flashLivePreview = false;
            trackLocal = true;
            debugTracking = false;
            _trackOnLoad = false;
            onLoadTracked = false;
            trackCalled = false;
            super();
            _local1 = this;
            _local1.initPre();
            _local1.version = "FAS-2.0.2";
            if (_local1.isSet(_root)){
                _local1.movie = _root;
            } else {
                if (_local1.isSet(root)){
                    _local1.movie = root;
                };
            };
            _local3 = getVersion();
            _local4 = _local3.split(" ");
            _local1.flashVersion = parseInt(_local4[1].substr(0, 1));
            _local1.requestNum = 0;
            _local1.requestList = new Array();
            _local1.lastRequest = "";
            _local1.requiredVarList = ["visitorID", "vmk", "charSet", "visitorNamespace", "cookieDomainPeriods", "cookieLifetime", "pageName", "pageURL", "referrer", "currencyCode"];
            _local1.accountVarList = ["purchaseID", "variableProvider", "channel", "server", "pageType", "transactionID", "campaign", "state", "zip", "events", "products"];
            _local2 = (_local1.requiredVarList.length - 1);
            while (_local2 >= 0) {
                _local1.accountVarList.unshift(_local1.requiredVarList[_local2]);
                _local2--;
            };
            _local2 = 1;
            while (_local2 <= 50) {
                _local1.accountVarList.push(("prop" + _local2));
                _local1.accountVarList.push(("eVar" + _local2));
                _local1.accountVarList.push(("hier" + _local2));
                _local2++;
            };
            _local1.accountVarList.push("pe");
            _local1.accountVarList.push("pev1");
            _local1.accountVarList.push("pev2");
            _local1.accountVarList.push("pev3");
            _local1.requiredVarList.push("pe");
            _local1.requiredVarList.push("pev1");
            _local1.requiredVarList.push("pev2");
            _local1.requiredVarList.push("pev3");
            _local1.accountConfigList = ["trackDownloadLinks", "trackExternalLinks", "trackClickMap", "linkLeaveQueryString", "linkTrackVars", "linkTrackEvents", "trackingServer", "trackingServerSecure", "dc", "movieID", "autoTrack", "delayTracking", "trackLocal", "debugTracking"];
            _local1.modulesInit();
            _local1.initPost();
        }
        public function clearVars(){
            var _local1:Object;
            var _local2:Number;
            var _local3:String;
            _local1 = this;
            _local2 = 0;
            while (_local2 < accountVarList.length) {
                _local3 = _local1.accountVarList[_local2];
                if ((((((((((((((((((((((_local3.substr(0, 4) == "prop")) || ((_local3.substr(0, 4) == "eVar")))) || ((_local3.substr(0, 4) == "hier")))) || ((_local3 == "channel")))) || ((_local3 == "events")))) || ((_local3 == "purchaseID")))) || ((_local3 == "transactionID")))) || ((_local3 == "products")))) || ((_local3 == "state")))) || ((_local3 == "zip")))) || ((_local3 == "campaign")))){
                    _local1[_local3] = undefined;
                };
                _local2++;
            };
        }
        private function variableOverridesBuild(_arg1:Object){
            var _local2:Object;
            var _local3:Number;
            var _local4:String;
            _local2 = this;
            _local3 = 0;
            while (_local3 < _local2.accountVarList.length) {
                _local4 = _local2.accountVarList[_local3];
                if (!_local2.isSet(_arg1[_local4])){
                    _arg1[_local4] = _local2[_local4];
                    if (!_local2.isSet(_arg1[_local4])){
                        _arg1[("!" + _local4)] = 1;
                    };
                };
                _local3++;
            };
            _local3 = 0;
            while (_local3 < _local2.accountConfigList.length) {
                _local4 = _local2.accountConfigList[_local3];
                if (!_local2.isSet(_arg1[_local4])){
                    _arg1[_local4] = _local2[_local4];
                    if (!_local2.isSet(_arg1[_local4])){
                        _arg1[("!" + _local4)] = 1;
                    };
                };
                _local3++;
            };
        }
        private function flushRequestList(){
            var _local1:Object;
            var _local2:String;
            var _local3:Array;
            var _local4:Number;
            _local1 = this;
            while (_local1.requestNum < _local1.requestList.length) {
                if (_local1.isSet(_local1.debugTracking)){
                    _local2 = ("ActionSource Debug: " + _local1.requestList[_local1.requestNum]);
                    _local3 = _local1.requestList[_local1.requestNum].split("&");
                    _local4 = 0;
                    while (_local4 < _local3.length) {
                        _local2 = (_local2 + ("\n\t" + unescape(_local3[_local4])));
                        _local4++;
                    };
                    trace(_local2);
                };
                _local1.requestURL(_local1.requestList[_local1.requestNum]);
                _local1.lastRequest = _local1.requestList[_local1.requestNum];
                _local1.requestNum++;
            };
        }
        private function bufferRequest(_arg1:String, _arg2:String, _arg3:String){
            var _local4:Object;
            var _local5:Object;
            var _local6:Object;
            var _local7:Number;
            var _local8:Number;
            _local4 = this;
            _local5 = _local4.getBufferedRequests();
            if (_local4.isSet(_local5)){
                _local8 = -1;
                _local7 = 0;
                while (_local7 < _local5.data.list.length) {
                    if (_local5.data.list[_local7].id == _arg2){
                        _local5.data.list[_local7].request = _arg3;
                        _arg3 = "";
                    } else {
                        if (!_local4.isSet(_local5.data.list[_local7].id)){
                            _local8 = _local7;
                        };
                    };
                    _local7++;
                };
                if (_local4.isSet(_arg3)){
                    _local6 = new Object();
                    _local6.account = _arg1;
                    _local6.id = _arg2;
                    _local6.request = _arg3;
                    if (_local8 >= 0){
                        _local5.data.list[_local8] = _local6;
                    } else {
                        _local5.data.list.push(_local6);
                    };
                };
                _local5.flush();
            };
        }
        private function requestURL(_arg1){
            var _local2:URLRequest;
            _local2 = new URLRequest(_arg1);
            sendToURL(_local2);
        }
        private function _trackLink(_arg1:String, _arg2:String, _arg3:String, _arg4:Object){
            var _local5:Object;
            _local5 = this;
            _local5.linkURL = _arg1;
            _local5.linkType = _arg2;
            _local5.linkName = _arg3;
            _local5.track(_arg4);
        }
        public function set movie(_arg1:Object){
            this._movie = _arg1;
            if (this.isSet(this._movie)){
                if (!this.flashLivePreview){
                    if (this.flashASVersion < 3){
                        this._movie.s_s = this;
                    };
                    this.modulesUpdate();
                };
            };
        }
        private function queryStringLinkTracking():String{
            var _local1:Object;
            var _local2:String;
            var _local3:Number;
            _local1 = this;
            _local2 = "";
            if (((_local1.isSet(_local1.linkType)) && (((_local1.isSet(_local1.linkURL)) || (_local1.isSet(_local1.linkName)))))){
                _local1.linkType = _local1.linkType.toLowerCase();
                if (((!((_local1.linkType == "d"))) && (!((_local1.linkType == "e"))))){
                    _local1.linkType = "o";
                };
                if (((_local1.isSet(_local1.linkURL)) && (!(_local1.isSet(_local1.linkLeaveQueryString))))){
                    _local3 = _local1.linkURL.indexOf("?");
                    if (_local3 >= 0){
                        _local1.linkURL = _local1.linkURL.substr(0, _local3);
                    };
                };
                _local2 = (_local2 + ("&pe=lnk_" + escape(_local1.linkType.toLowerCase())));
                _local2 = (_local2 + ((_local1.isSet(_local1.linkURL)) ? ("&pev1=" + escape(_local1.linkURL)) : ""));
                _local2 = (_local2 + ((_local1.isSet(_local1.linkName)) ? ("&pev2=" + escape(_local1.linkName)) : ""));
            };
            return (_local2);
        }
        private function _track(_arg1:Object, _arg2:String){
            var _local3:Object;
            var _local4:Object;
            var _local5:Date;
            var _local6:Number;
            var _local7:String;
            var _local8:String;
            var _local9:String;
            var _local10:Number;
            var _local11:String;
            _local3 = this;
            _local5 = new Date();
            _local6 = Math.floor((Math.random() * 10000000000000));
            _local7 = (("s" + (Math.floor((_local5.getTime() / 10800000)) % 10)) + _local6);
            _local8 = ((((((((((((((("" + _local5.getDate()) + "/") + _local5.getMonth()) + "/") + _local5.getFullYear()) + " ") + _local5.getHours()) + ":") + _local5.getMinutes()) + ":") + _local5.getSeconds()) + " ") + _local5.getDay()) + " ") + _local5.getTimezoneOffset());
            _local9 = ("t=" + escape(_local8));
            if (_local3.isSet(_local3.flashLivePreview)){
                return;
            };
            if (_local3.isSet(_local3.otherVariables)){
                _local10 = 0;
                while (_local10 < _local3.accountVarList.length) {
                    _local11 = _local3.accountVarList[_local10];
                    if (_local3.isSet(_local3.otherVariables[_local11])){
                        _local3[_local11] = _local3.otherVariables[_local11];
                    };
                    _local10++;
                };
                _local10 = 0;
                while (_local10 < _local3.accountConfigList.length) {
                    _local11 = _local3.accountConfigList[_local10];
                    if (_local3.isSet(_local3.otherVariables[_local11])){
                        _local3[_local11] = _local3.otherVariables[_local11];
                    };
                    _local10++;
                };
            };
            if (_local3.isSet(_arg1)){
                _local4 = new Object();
                _local3.variableOverridesBuild(_local4);
                _local3.variableOverridesApply(_arg1);
            };
            if (_local3.isSet(_local3.account)){
                if (!_local3.isSet(_local3.pageURL)){
                    _local3.pageURL = _local3.getMovieURL();
                };
                _local9 = (_local9 + _local3.queryStringAccountVariables());
                _local9 = (_local9 + _local3.queryStringLinkTracking());
                _local9 = (_local9 + _local3.queryStringClickMap());
                _local9 = (_local9 + _local3.queryStringTechnology());
                _local3.makeRequest(_local7, _local9, "", _arg2);
            };
            if (_local3.isSet(_arg1)){
                _local3.variableOverridesApply(_local4);
            };
            _local3.pe = undefined;
            _local3.pev1 = undefined;
            _local3.pev2 = undefined;
            _local3.pev3 = undefined;
            _local3.linkURL = undefined;
            _local3.linkName = undefined;
            _local3.linkType = undefined;
            _local3.objectID = undefined;
            if (_local3.isSet(_local3.account)){
                if (((!(_local3.isSet(_arg2))) && (!(_local3.isSet(_local3.trackCalled))))){
                    _local3.trackCalled = true;
                    _local3.flushBufferedRequests();
                };
            };
        }
        public function get trackOnLoad():Boolean{
            return (this._trackOnLoad);
        }
        private function getVersion():String{
            return (Capabilities.version);
        }
        private function makeRequest(_arg1:String, _arg2:String, _arg3:String, _arg4:String){
            var _local5:Object;
            var _local6:*;
            var _local7:String;
            var _local8:String;
            var _local9:Number;
            _local5 = this;
            _local6 = _local5.getMovieURL();
            if (!_local5.isSet(_arg3)){
                if (_local5.isSet(_local5.trackingServer)){
                    _local7 = _local5.trackingServer;
                    if (((_local5.isSet(_local5.trackingServerSecure)) && ((_local6.toLowerCase().substr(0, 6) == "https:")))){
                        _local7 = _local5.trackingServerSecure;
                    };
                } else {
                    _local8 = _local5.visitorNamespace;
                    if (!_local5.isSet(_local8)){
                        _local8 = _local5.account;
                        _local9 = _local8.indexOf(",");
                        if (_local9 >= 0){
                            _local8 = _local8.substr(0, _local9);
                        };
                        _local8 = _local8.split("_").join("-");
                    };
                    _local7 = (((_local8 + ".") + ((_local5.isSet(_local5.dc)) ? _local5.dc : 112)) + ".2o7.net");
                };
                if (_local6.toLowerCase().substr(0, 6) == "https:"){
                    _arg3 = "https://";
                } else {
                    _arg3 = "http://";
                };
                _arg3 = (_arg3 + (((((((((((_local7 + "/b/ss/") + _local5.account) + "/0/") + _local5.version) + "-AS") + _local5.flashASVersion) + "/") + _arg1) + "?[AQB]&ndh=1&") + _arg2) + "&[AQE]"));
                if (_local5.isSet(_arg4)){
                    _local5.bufferRequest(_local5.account, _arg4, _arg3);
                    return;
                };
            };
            if ((((_local6.toLowerCase().substr(0, 6) == "https:")) && ((_arg3.toLowerCase().substr(0, 5) == "http:")))){
                _arg3 = ("https:" + _arg3.substr(5));
            };
            if (((((((_local5.isSet(_local5.trackLocal)) || ((_local5.flashVersion < 8)))) || (!(_local5.isSet(_local6))))) || ((_local6.toLowerCase().substr(0, 4) == "http")))){
                _local5.requestList.push(_arg3);
                if (((!(_local5.isSet(_local5.delayTracking))) || (((_local5.isSet(_local5.delayTrackingStage)) && ((_local5.delayTrackingStage == 2)))))){
                    _local5.flushRequestList();
                } else {
                    if (((_local5.isSet(_local5.delayTracking)) && (!(_local5.isSet(_local5.delayTrackingStage))))){
                        _local5.delayTrackingStage = 1;
                        _local5.startDelayTrackingInterval();
                    };
                };
            };
        }
        public function replace(_arg1:String, _arg2:String, _arg3:String):String{
            if (this.isSet(_arg1)){
                if (_arg1.indexOf(_arg2) >= 0){
                    return (_arg1.split(_arg2).join(_arg3));
                };
            };
            return (_arg1);
        }
        private function queryStringClickMap():String{
            var _local1:Object;
            var _local2:String;
            var _local3:String;
            var _local4:Number;
            var _local5:String;
            var _local6:Number;
            var _local7:String;
            _local1 = this;
            _local2 = "";
            _local3 = _local1.pageName;
            _local4 = 1;
            _local5 = _local1.objectID;
            _local6 = 1;
            _local7 = "FLASH";
            if (!_local1.isSet(_local3)){
                _local3 = _local1.pageURL;
                _local4 = 0;
            };
            if (((((((_local1.isSet(_local1.trackClickMap)) && (_local1.isSet(_local3)))) && (_local1.isSet(_local5)))) && (_local1.isSet(_local7)))){
                _local2 = (_local2 + ("&pid=" + escape(_local3)));
                _local2 = (_local2 + ((_local1.isSet(_local4)) ? ("&pidt=" + escape(("" + _local4))) : ""));
                _local2 = (_local2 + ("&oid=" + escape(_local5.substr(0, 100))));
                _local2 = (_local2 + ((_local1.isSet(_local6)) ? ("&oidt=" + escape(("" + _local6))) : ""));
                _local2 = (_local2 + ("&ot=" + escape(_local7)));
            };
            return (_local2);
        }
        private function queryStringAccountVariables():String{
            var _local1:Object;
            var _local2:String;
            var _local3:Number;
            var _local4:Number;
            var _local5:String;
            var _local6:String;
            var _local7:*;
            var _local8:String;
            var _local9:String;
            var _local10:*;
            var _local11:*;
            var _local12:*;
            _local1 = this;
            _local2 = "";
            _local10 = "";
            _local11 = "";
            _local12 = "";
            if (_local1.isSet(_local1.linkType)){
                _local10 = _local1.linkTrackVars;
                _local11 = _local1.linkTrackEvents;
            } else {
                if (_local1.isSet(_local1.pe)){
                    _local12 = (_local1.pe.substr(0, 1).toUpperCase() + _local1.pe.substr(1));
                    if (_local1.isSet(_local1[_local12])){
                        _local10 = _local1[_local12].trackVars;
                        _local11 = _local1[_local12].trackEvents;
                    };
                };
            };
            if (_local1.isSet(_local10)){
                _local10 = (((("," + _local10) + ",") + _local1.requiredVarList.join(",")) + ",");
            };
            if (_local1.isSet(_local11)){
                _local11 = (("," + _local11) + ",");
            };
            _local3 = 0;
            while (_local3 < _local1.accountVarList.length) {
                _local5 = _local1.accountVarList[_local3];
                _local6 = _local1[_local5];
                _local8 = _local5.substr(0, 4);
                _local9 = _local5.substr(4);
                if (((_local1.isSet(_local6)) && (((!(_local1.isSet(_local10))) || ((_local10.indexOf((("," + _local5) + ",")) >= 0)))))){
                    switch (_local5){
                        case "visitorID":
                            _local5 = "vid";
                            break;
                        case "pageURL":
                            _local5 = "g";
                            break;
                        case "referrer":
                            _local5 = "r";
                            break;
                        case "vmk":
                            _local5 = "vmt";
                            break;
                        case "charSet":
                            _local5 = "ce";
                            break;
                        case "visitorNamespace":
                            _local5 = "ns";
                            break;
                        case "cookieDomainPeriods":
                            _local5 = "cdp";
                            break;
                        case "cookieLifetime":
                            _local5 = "cl";
                            break;
                        case "currencyCode":
                            _local5 = "cc";
                            break;
                        case "channel":
                            _local5 = "ch";
                            break;
                        case "transactionID":
                            _local5 = "xact";
                            break;
                        case "campaign":
                            _local5 = "v0";
                            break;
                        case "events":
                            if (_local1.isSet(_local11)){
                                _local7 = _local6.split(",");
                                _local6 = "";
                                _local4 = 0;
                                while (_local4 < _local7.length) {
                                    if (_local11.indexOf((("," + _local7[_local4]) + ",")) >= 0){
                                        _local6 = (_local6 + (((_local1.isSet(_local6)) ? "," : "") + _local7[_local4]));
                                    };
                                    _local4++;
                                };
                            };
                            break;
                        default:
                            if (_local1.isNumber(_local9)){
                                if (_local8 == "prop"){
                                    _local5 = ("c" + _local9);
                                } else {
                                    if (_local8 == "eVar"){
                                        _local5 = ("v" + _local9);
                                    } else {
                                        if (_local8 == "hier"){
                                            _local5 = ("h" + _local9);
                                            _local6 = _local6.substr(0, 0xFF);
                                        };
                                    };
                                };
                            };
                    };
                    if (_local1.isSet(_local6)){
                        _local2 = (_local2 + ((("&" + escape(_local5)) + "=") + ((_local5.substr(0, 3))!="pev") ? escape(_local6) : _local6));
                    };
                };
                _local3++;
            };
            return (_local2);
        }
        private function queryStringTechnology():String{
            var _local1:Object;
            var _local2:String;
            var _local3:Object;
            _local1 = this;
            _local2 = "";
            _local3 = Capabilities;
            if (((((_local1.isSet(_local3)) && (_local1.isSet(_local3.screenResolutionX)))) && (_local1.isSet(_local3.screenResolutionY)))){
                _local2 = (_local2 + ((("&s=" + _local3.screenResolutionX) + "x") + _local3.screenResolutionY));
            };
            return (_local2);
        }
        public function isSet(_arg1, _arg2:String=null):Boolean{
            var e:* = null;
            var val:* = _arg1;
            var mbr = _arg2;
            try {
                if (mbr != null){
                    val = val[mbr];
                };
                return (((((((((((!((val == null))) && (!((val == undefined))))) && (!((("" + val) == "NaN"))))) && (!((val == false))))) && (!((val == ""))))) && (!((val == 0)))));
            } catch(e) {
            };
            return (false);
        }
        public function flushBufferedRequests(){
            var _local1:*;
            _local1 = this;
            if (_local1.isSet(_local1.account)){
                _local1._flushBufferedRequests(_local1.account);
            };
        }
        private function startDelayTrackingInterval(){
            var _local1:Object;
            _local1 = this;
            _local1.delayTrackingInterval = setInterval(delayTrackingDone, _local1.delayTracking);
        }
        private function doTrackOnLoad(){
            var _local1:Object;
            _local1 = this;
            if (((!(_local1.isSet(_local1.account))) || (!(_local1.isSet(_local1.movie))))){
                return;
            };
            clearInterval(_local1.trackOnLoadInterval);
            if (((_local1._trackOnLoad) && (!(_local1.onLoadTracked)))){
                _local1.onLoadTracked = true;
                _local1.track();
            };
        }
        public function isNumber(_arg1):Boolean{
            return (!(isNaN(parseInt(_arg1))));
        }
        public function flushBufferedRequest(_arg1:String, _arg2:String){
            var _local3:Object;
            var _local4:Object;
            var _local5:Object;
            var _local6:Number;
            var _local7:String;
            _local3 = this;
            _local4 = _local3.getBufferedRequests();
            if (_local3.isSet(_local4)){
                _local6 = 0;
                while (_local6 < _local4.data.list.length) {
                    _local5 = _local4.data.list[_local6];
                    if ((((_local5.account == _arg1)) && ((_local5.id == _arg2)))){
                        _local7 = _local4.data.list[_local6].request;
                        _local4.data.list[_local6].account = "";
                        _local4.data.list[_local6].id = "";
                        _local4.data.list[_local6].request = "";
                        _local4.flush();
                        _local3.makeRequest("", "", _local7, "");
                    };
                    _local6++;
                };
            };
        }
        public function set trackOnLoad(_arg1:Boolean){
            this._trackOnLoad = _arg1;
            if (this._trackOnLoad){
                this.setTrackOnLoadInterval();
            };
        }
        private function variableOverridesApply(_arg1:Object){
            var _local2:Object;
            var _local3:Number;
            var _local4:String;
            _local2 = this;
            _local3 = 0;
            while (_local3 < _local2.accountVarList.length) {
                _local4 = _local2.accountVarList[_local3];
                if (((_local2.isSet(_arg1[_local4])) || (_local2.isSet(_arg1[("!" + _local4)])))){
                    _local2[_local4] = _arg1[_local4];
                };
                _local3++;
            };
            _local3 = 0;
            while (_local3 < _local2.accountConfigList.length) {
                _local4 = _local2.accountConfigList[_local3];
                if (((_local2.isSet(_arg1[_local4])) || (_local2.isSet(_arg1[("!" + _local4)])))){
                    _local2[_local4] = _arg1[_local4];
                };
                _local3++;
            };
        }
        public function get movie():Object{
            return (this._movie);
        }
        private function getBufferedRequests(){
            var s:* = null;
            var bufferedRequests:* = null;
            s = this;
            if (!s.isSet(s.disableBufferedRequests)){
                bufferedRequests = SharedObject.getLocal("s_br", "/");
            };
            if (!s.isSet(bufferedRequests)){
                bufferedRequests = s.bufferedRequests;
                if (!s.isSet(bufferedRequests)){
                    s.bufferedRequests = new Object();
                    s.bufferedRequests.flush = function (){
                    };
                    bufferedRequests = s.bufferedRequests;
                };
            };
            if (!s.isSet(bufferedRequests.data)){
                bufferedRequests.data = new Object();
            };
            if (!s.isSet(bufferedRequests.data.list)){
                bufferedRequests.data.list = new Array();
            };
            return (bufferedRequests);
        }
        private function modulesInit(){
            var _local1:Object;
            _local1 = this;
            _local1.ClickMap = new ActionSource_ClickMap(_local1);
            _local1.Media = new ActionSource_Module_Media(_local1);
            _local1.modulesUpdate();
        }
        private function modulesUpdate(){
            var _local1:Object;
            var _local2:String;
            _local1 = this;
            if (_local1.isSet(_local1.Media)){
                if (_local1.isSet(_local1._moduleMediaVariables)){
                    for (_local2 in _local1._moduleMediaVariables) {
                        if (_local1.isSet(_local1._moduleMediaVariables[_local2])){
                            if (_local2 == "autoTrack"){
                                if (("" + _local1._moduleMediaVariables[_local2]).toLowerCase() == "true"){
                                    _local1._moduleMediaVariables[_local2] = true;
                                } else {
                                    _local1._moduleMediaVariables[_local2] = false;
                                };
                            };
                            _local1.Media[_local2] = _local1._moduleMediaVariables[_local2];
                        };
                    };
                };
                _local1.Media.autoTrack = _local1.Media.autoTrack;
            };
        }
        public function set moduleMediaVariables(_arg1:Object){
            this._moduleMediaVariables = _arg1;
            this.modulesUpdate();
        }
        public function track(_arg1:Object=null, _arg2:String=""){
            this._track(_arg1, _arg2);
        }
        private function initPre(){
            if ((((parent == null)) || (!((getQualifiedClassName(parent) == "fl.livepreview::LivePreviewParent"))))){
                this.visible = false;
            } else {
                this.flashLivePreview = true;
            };
            this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
        }
        public function get moduleMediaVariables():Object{
            return (this._moduleMediaVariables);
        }
        private function getMovieURL():String{
            var _local1:Object;
            var _local2:String;
            _local1 = this;
            _local2 = null;
            if (((((_local1.isSet(ExternalInterface)) && (_local1.isSet(ExternalInterface.available)))) && (_local1.isSet(ExternalInterface.call)))){
                _local2 = ExternalInterface.call("function s_gl(){return window.location.href;}");
            };
            if (_local1.isSet(_local2)){
                return (_local2);
            };
            if (_local1.isSet(_local1.movie)){
                if ((((((_local1.flashASVersion > 2)) && (_local1.isSet(_local1.movie.loaderInfo)))) && (_local1.isSet(_local1.movie.loaderInfo.loaderURL)))){
                    return (_local1.movie.loaderInfo.loaderURL);
                };
                if (_local1.isSet(_local1.movie._url)){
                    return (_local1.movie._url);
                };
            };
            return ("");
        }
        private function onAddedToStage(_arg1:Event){
            this.movie = root;
        }
        private function setTrackOnLoadInterval():void{
            this.trackOnLoadInterval = setInterval(this.doTrackOnLoad, 50);
        }
        private function initPost(){
        }
        public function trackLink(_arg1:String, _arg2:String, _arg3:String, _arg4:Object=null){
            this._trackLink(_arg1, _arg2, _arg3, _arg4);
        }
        private function _flushBufferedRequests(_arg1:String){
            var _local2:Object;
            var _local3:Object;
            var _local4:Object;
            var _local5:Number;
            _local2 = this;
            _local3 = _local2.getBufferedRequests();
            if (_local2.isSet(_local3)){
                _local5 = 0;
                while (_local5 < _local3.data.list.length) {
                    _local4 = _local3.data.list[_local5];
                    if (_local4.account == _arg1){
                        _local2.flushBufferedRequest(_arg1, _local4.id);
                    };
                    _local5++;
                };
            };
        }
        private function delayTrackingDone(){
            var _local1:Object;
            _local1 = this;
            clearInterval(_local1.delayTrackingInterval);
            _local1.delayTrackingStage = 2;
            _local1.flushRequestList();
        }

    }
}//package com.omniture 
