﻿package com.omniture {
    import flash.utils.*;

    public dynamic class ActionSource_Module_Media {

        public var trackEvents:String;
        public var playerName:String;
        public var trackVars:String;
        private var _autoTrack:Boolean;
        private var list:Object;
        private var s:Object;
        private var autoTrackDone:Boolean;

        public function ActionSource_Module_Media(_arg1:Object){
            var _local2:Object;
            super();
            _local2 = this;
            _local2.s = _arg1;
        }
        public function listenerMedia_scrubbing(_arg1){
            this.listenerMedia.scrubbing(_arg1);
        }
        public function stop(_arg1:String, _arg2:Number){
            this.event(_arg1, 2, _arg2);
        }
        private function attach(_arg1:Object){
            var m:* = null;
            var member:* = null;
            var childNum:* = NaN;
            var monitor:* = null;
            var node:* = _arg1;
            m = this;
            if (m.s.isSet(node)){
                if (((m.s.isSet(node, "addEventListener")) && (m.s.isSet(node, "isFLVCuePointEnabled")))){
                    if (!m.s.isSet(m.listenerFLVPlayback)){
                        m.listenerFLVPlayback = new Object();
                        m.listenerFLVPlayback.m = m;
                        m.listenerFLVPlayback.playerName = "Flash FLVPlayback";
                        m.listenerFLVPlayback.handleEvent = function (_arg1:Object, _arg2:Number){
                            var _local3:Object;
                            var _local4:String;
                            var _local5:Number;
                            var _local6:Number;
                            _local3 = this.m;
                            if (((_local3.s.isSet(_local3.autoTrack)) && (_local3.s.isSet(_arg1)))){
                                if (_local3.s.flashASVersion > 2){
                                    _local4 = _arg1.source;
                                } else {
                                    _local4 = _arg1.contentPath;
                                };
                                _local5 = _arg1.totalTime;
                                _local6 = _arg1.playheadTime;
                                _local3.autoEvent(_local4, _local5, this.playerName, _arg2, _local6, _arg1);
                            };
                        };
                        m.listenerFLVPlayback.stateChange = function (_arg1){
                            var _local2:Object;
                            var _local3:Number;
                            var _local4:Object;
                            _local2 = this.m;
                            _local3 = -1;
                            if (((_local2.s.isSet(_arg1)) && (_local2.s.isSet(_arg1.target)))){
                                _local4 = _arg1.target;
                                if (_local2.s.isSet(_local4, "state")){
                                    if (_local4.state == "playing"){
                                        _local3 = 1;
                                    } else {
                                        if ((((((((((_local4.state == "stopped")) || ((_local4.state == "paused")))) || ((_local4.state == "buffering")))) || ((_local4.state == "rewinding")))) || ((_local4.state == "seeking")))){
                                            _local3 = 2;
                                        };
                                    };
                                    if (_local3 >= 0){
                                        this.handleEvent(_arg1.target, _local3);
                                    };
                                };
                            };
                        };
                        m.listenerFLVPlayback.complete = function (_arg1){
                            if (this.m.s.isSet(_arg1)){
                                this.handleEvent(_arg1.target, 0);
                            };
                        };
                    };
                    if (m.s.flashASVersion > 2){
                        node.addEventListener("complete", m.listenerFLVPlayback_complete);
                        node.addEventListener("stateChange", m.listenerFLVPlayback_stateChange);
                    } else {
                        node.addEventListener("complete", m.listenerFLVPlayback);
                        node.addEventListener("stateChange", m.listenerFLVPlayback);
                    };
                    monitor = new Object();
                    monitor.m = m;
                    monitor.node = node;
                    monitor.watch = function (){
                        var _local1:Object;
                        var _local2:Object;
                        _local1 = this.m;
                        _local2 = this.node;
                        if (((_local1.s.isSet(_local2.state)) && ((_local2.state == "playing")))){
                            this.m.listenerFLVPlayback.handleEvent(_local2, 3);
                        };
                    };
                    m.startMonitor(monitor);
                } else {
                    if (((m.s.isSet(node, "addEventListener")) && (m.s.isSet(node, "addCuePoint")))){
                        if (!m.s.isSet(m.listenerMedia)){
                            m.listenerMedia = new Object();
                            m.listenerMedia.m = m;
                            m.listenerMedia.playerName = "Flash Media";
                            m.listenerMedia.handleEvent = function (_arg1:Object, _arg2:Number){
                                var _local3:Object;
                                var _local4:String;
                                var _local5:Number;
                                var _local6:Number;
                                _local3 = this.m;
                                if (((_local3.s.isSet(_local3.autoTrack)) && (_local3.s.isSet(_arg1)))){
                                    _local4 = _arg1.contentPath;
                                    _local5 = _arg1.totalTime;
                                    _local6 = _arg1.playheadTime;
                                    _local3.autoEvent(_local4, _local5, this.playerName, _arg2, _local6, _arg1);
                                };
                            };
                            m.listenerMedia.complete = function (_arg1){
                                if (this.m.s.isSet(_arg1)){
                                    this.handleEvent(_arg1.target, 0);
                                };
                            };
                            m.listenerMedia.click = function (_arg1){
                                if (((this.m.s.isSet(_arg1)) && (this.m.s.isSet(_arg1.target)))){
                                    this.handleEvent(_arg1.target, ((this.m.s.isSet(_arg1.target.playing)) ? 1 : 2));
                                };
                            };
                            m.listenerMedia.change = function (_arg1){
                                if (((this.m.s.isSet(_arg1)) && (this.m.s.isSet(_arg1.target)))){
                                    this.handleEvent(_arg1.target, ((this.m.s.isSet(_arg1.target.playing)) ? 1 : 2));
                                };
                            };
                            m.listenerMedia.scrubbing = function (_arg1){
                                if (this.m.s.isSet(_arg1)){
                                    this.handleEvent(_arg1.target, 2);
                                };
                            };
                        };
                        if (m.s.flashASVersion > 2){
                            node.addEventListener("complete", m.listenerMedia_complete);
                            node.addEventListener("click", m.listenerMedia_click);
                            node.addEventListener("change", m.listenerMedia_change);
                            node.addEventListener("scrubbing", m.listenerMedia_scrubbing);
                        } else {
                            node.addEventListener("complete", m.listenerMedia);
                            node.addEventListener("click", m.listenerMedia);
                            node.addEventListener("change", m.listenerMedia);
                            node.addEventListener("scrubbing", m.listenerMedia);
                        };
                        monitor = new Object();
                        monitor.m = m;
                        monitor.node = node;
                        monitor.watch = function (){
                            var _local1:Object;
                            var _local2:Object;
                            _local1 = this.m;
                            _local2 = this.node;
                            if (_local1.s.isSet(_local2.playing)){
                                this.m.listenerMedia.handleEvent(_local2, 3);
                            };
                        };
                        m.startMonitor(monitor);
                    } else {
                        if (m.s.flashASVersion > 2){
                            if (((m.s.isSet(node, "numChildren")) && (m.s.isSet(node, "getChildAt")))){
                                childNum = 0;
                                while (childNum < node.numChildren) {
                                    m.attach(node.getChildAt(childNum));
                                    childNum = (childNum + 1);
                                };
                            };
                        } else {
                            for (member in node) {
                                if (((((((m.s.isSet(node[member])) && (m.s.isSet(node[member]._name)))) && ((node[member]._name == member)))) && ((((("" + node) + ".") + member) == ("" + node[member]))))){
                                    m.attach(node[member]);
                                };
                            };
                        };
                    };
                };
            };
        }
        public function listenerMedia_change(_arg1){
            this.listenerMedia.change(_arg1);
        }
        public function open(_arg1:String, _arg2:Number, _arg3:String, _arg4:Object=null){
            this._open(_arg1, _arg2, _arg3, _arg4);
        }
        public function listenerMedia_click(_arg1){
            this.listenerMedia.click(_arg1);
        }
        private function event(_arg1:String, _arg2:Number, _arg3:Number){
            var _local4:Object;
            var _local5:Object;
            var _local6:Date;
            var _local7:Number;
            var _local8:String;
            _local4 = this;
            _local6 = new Date();
            _local7 = Math.floor((_local6.getTime() / 1000));
            _local8 = "--**--";
            _arg1 = _local4.cleanName(_arg1);
            _local5 = ((((((_local4.s.isSet(_arg1)) && (_local4.s.isSet(_local4.list)))) && (_local4.s.isSet(_local4.list[_arg1])))) ? _local4.list[_arg1] : null);
            if (_local4.s.isSet(_local5)){
                if ((((_arg2 == 3)) || (((!((_arg2 == _local5.lastEventType))) && (((!((_arg2 == 2))) || ((_local5.lastEventType == 1)))))))){
                    if (_local4.s.isSet(_arg2)){
                        if ((((_arg3 < 0)) && ((_local5.lastEventTimestamp > 0)))){
                            _arg3 = ((_local7 - _local5.lastEventTimestamp) + _local5.lastEventOffset);
                            _arg3 = (((_arg3 < _local5.length)) ? _arg3 : (_local5.length - 1));
                        };
                        _arg3 = Math.floor(_arg3);
                        if ((((((_arg2 == 2)) || ((_arg2 == 3)))) && ((_local5.lastEventOffset < _arg3)))){
                            _local5.timePlayed = (_local5.timePlayed + (_arg3 - _local5.lastEventOffset));
                        };
                        if (_arg2 != 3){
                            _local5.session = (_local5.session + ((((_arg2 == 1)) ? "S" : "E") + _arg3));
                            _local5.lastEventType = _arg2;
                        } else {
                            if (_local5.lastEventType != 1){
                                _local4.event(_arg1, 1, _arg3);
                            };
                        };
                        _local5.lastEventTimestamp = _local7;
                        _local5.lastEventOffset = _arg3;
                        _local4.s.pe = "media";
                        _local4.s.pev3 = (((((((((((("" + escape(_local5.name)) + _local8) + _local5.length) + _local8) + escape(_local5.playerName)) + _local8) + _local5.timePlayed) + _local8) + _local5.timestamp) + _local8) + _local5.session) + (((_arg2 == 3)) ? ("E" + _arg3) : ""));
                        _local4.s.track(null, ("Media." + _arg1));
                    } else {
                        _local4.event(_arg1, 2, -1);
                        _local4.list[_arg1] = 0;
                        _local4.s.flushBufferedRequest(_local4.s.account, ("Media." + _arg1));
                    };
                };
            };
        }
        public function get autoTrack():Boolean{
            return (this._autoTrack);
        }
        private function startMonitor(_arg1:Object){
            var monitor:* = _arg1;
            monitor.update = function (_arg1){
                if ((((((((((((_arg1.m == null)) || ((_arg1.m == undefined)))) || ((_arg1.m.s == null)))) || ((_arg1.m.s == undefined)))) || ((_arg1.node == null)))) || ((_arg1.node == undefined)))){
                    clearInterval(_arg1.interval);
                } else {
                    _arg1.watch();
                };
            };
            monitor.interval = setInterval(monitor.update, 5000, monitor);
        }
        public function listenerMedia_complete(_arg1){
            this.listenerMedia.complete(_arg1);
        }
        public function listenerFLVPlayback_complete(_arg1){
            this.listenerFLVPlayback.complete(_arg1);
        }
        private function _open(_arg1:String, _arg2:Number, _arg3:String, _arg4:Object){
            var _local5:Object;
            var _local6:Object;
            var _local7:Date;
            var _local8:String;
            var _local9:String;
            _local5 = this;
            _local6 = new Object();
            _local7 = new Date();
            _local8 = "";
            _arg1 = _local5.cleanName(_arg1);
            _arg2 = Math.floor(_arg2);
            if (!_local5.s.isSet(_arg2)){
                _arg2 = 1;
            };
            if (((((_local5.s.isSet(_arg1)) && (_local5.s.isSet(_arg2)))) && (_local5.s.isSet(_arg3)))){
                if (!_local5.s.isSet(_local5.list)){
                    _local5.list = new Object();
                };
                if (_local5.s.isSet(_local5.list[_arg1])){
                    _local5.close(_arg1);
                };
                if (_local5.s.isSet(_arg4)){
                    _local8 = ("" + _arg4);
                };
                for (_local9 in _local5.list) {
                    if (((_local5.s.isSet(_local5.list[_local9])) && ((_local5.list[_local9].playerID == _local8)))){
                        _local5.close(_local5.list[_local9].name);
                    };
                };
                _local6.name = _arg1;
                _local6.length = _arg2;
                _local6.playerName = _local5.cleanName(((_local5.s.isSet(_local5.playerName)) ? _local5.playerName : _arg3));
                _local6.playerID = _local8;
                _local6.timePlayed = 0;
                _local6.timestamp = Math.floor((_local7.getTime() / 1000));
                _local6.lastEventType = 0;
                _local6.lastEventTimestamp = _local6.timestamp;
                _local6.lastEventOffset = 0;
                _local6.session = "";
                _local6.eventList = new Object();
                _local5.list[_arg1] = _local6;
            };
        }
        private function autoEvent(_arg1:String, _arg2:Number, _arg3:String, _arg4:Number, _arg5:Number, _arg6:Object){
            var _local7:Object;
            _local7 = this;
            _arg1 = _local7.cleanName(_arg1);
            if (((((_local7.s.isSet(_arg1)) && (_local7.s.isSet(_arg2)))) && (_local7.s.isSet(_arg3)))){
                if (((!(_local7.s.isSet(_local7.list))) || (!(_local7.s.isSet(_local7.list[_arg1]))))){
                    _local7.open(_arg1, _arg2, _arg3, _arg6);
                };
                _local7.event(_arg1, _arg4, _arg5);
            };
        }
        private function cleanName(_arg1:String):String{
            var _local2:Object;
            _local2 = this;
            return (_local2.s.replace(_local2.s.replace(_local2.s.replace(_arg1, "\n", ""), "\r", ""), "--**--", ""));
        }
        public function set autoTrack(_arg1:Boolean){
            this._autoTrack = _arg1;
            if (((((this._autoTrack) && (!(this.autoTrackDone)))) && (this.s.isSet(this.s.movie)))){
                this.autoTrackDone = true;
                this.attach(this.s.movie);
            };
        }
        public function listenerFLVPlayback_stateChange(_arg1){
            this.listenerFLVPlayback.stateChange(_arg1);
        }
        public function play(_arg1:String, _arg2:Number){
            var m:* = null;
            var media:* = null;
            var monitor:* = null;
            var name:* = _arg1;
            var offset:* = _arg2;
            m = this;
            m.event(name, 1, offset);
            monitor = new Object();
            monitor.m = m;
            monitor.node = m.cleanName(name);
            monitor.watch = function (){
                var _local1:Object;
                var _local2:Object;
                var _local3:Object;
                _local1 = this.m;
                _local2 = this.node;
                _local3 = ((((((_local1.s.isSet(_local2)) && (_local1.s.isSet(_local1.list)))) && (_local1.s.isSet(_local1.list[_local2])))) ? _local1.list[_local2] : null);
                if (_local1.s.isSet(_local3)){
                    if (_local3.lastEventType == 1){
                        _local1.event(_local3.name, 3, -1);
                    };
                } else {
                    this.node = null;
                };
            };
            m.startMonitor(monitor);
        }
        public function close(_arg1:String){
            this.event(_arg1, 0, -1);
        }

    }
}//package com.omniture 
