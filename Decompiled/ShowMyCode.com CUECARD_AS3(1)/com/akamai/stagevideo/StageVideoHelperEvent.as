﻿package com.akamai.stagevideo {
    import flash.events.*;

    public class StageVideoHelperEvent extends Event {

        public static const SIZE_CHANGE:String = "size";

        private var _videoWidth:uint = 0;
        private var _videoHeight:uint = 0;
        private var _status:String = "";

        public function StageVideoHelperEvent(_arg1:uint, _arg2:uint, _arg3:String){
            super(SIZE_CHANGE);
            this._videoWidth = _arg1;
            this._videoHeight = _arg2;
            this._status = _arg3;
        }
        public function get videoWidth():uint{
            return (this._videoWidth);
        }
        public function get videoHeight():uint{
            return (this._videoHeight);
        }
        public function get status():String{
            return (this._status);
        }

    }
}//package com.akamai.stagevideo 
