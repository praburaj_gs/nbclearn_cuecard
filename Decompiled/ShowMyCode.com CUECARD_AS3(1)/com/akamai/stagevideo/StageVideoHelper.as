﻿package com.akamai.stagevideo {
    import flash.events.*;
    import flash.display.*;
    import org.osmf.logging.*;
    import __AS3__.vec.*;
    import flash.net.*;
    import flash.utils.*;
    import flash.geom.*;
    import flash.media.*;
    import flash.system.*;

    public class StageVideoHelper extends EventDispatcher {

        public static const RENDER_STATE:String = "renderState";
        public static const NETSTREAM_RENDER:String = "render";
        public static const STAGE_VIDEO_AVAILABILITY:String = "stageVideoAvailability";
        public static const STATUS_STAGEVIDEO_ACCELERATED:String = "StageVideo accelerated";
        public static const STATUS_STAGEVIDEO_SOFTWARE:String = "StageVideo software";
        public static const STATUS_VIDEO_ACCELERATED:String = "Video accelerated";
        public static const STATUS_VIDEO_SOFTWARE:String = "Video software";
        private static const RELEASE_ERROR_MSG:String = "Calling function on released StageVideoHelper";

        private static var _focus:StageVideoHelper = null;
        private static var _instances:Vector.<StageVideoHelper> = new Vector.<StageVideoHelper>();
;

        private var logger:Logger;
        private var _video:Video = null;
        private var _stageVideo:Object = null;
        private var _x:Number = 0;
        private var _y:Number = 0;
        private var _width:Number = 320;
        private var _height:Number = 240;
        private var _visible:Boolean = true;
        private var _stage:Stage = null;
        private var _netStream:NetStream = null;
        private var _stageVideoUsed:Boolean = false;
        private var _stageVideoRect:Rectangle = null;
        private var _status:String = "Video software";
        private var _emulateSizeEvent:Boolean = false;
        private var _hasEmulateSizeEvent:Boolean = false;
        private var _emulateSizeWidth:int = -1;
        private var _emulateSizeHeight:int = -1;
        private var _fullScreenSmooth:Boolean = false;
        private var _displayObjectContainer:DisplayObjectContainer;
        private var _videoDecodingStatus:String = "Unknown";
        private var _videoCompositingStatus:String = "Unknown";
        private var _player:Object;
        private var _videoContainer:Object;
        private var _usingFlex:Boolean = false;
        private var _rectBeforeFullScreen:Rectangle;

        public function StageVideoHelper(_arg1:Stage, _arg2:Rectangle, _arg3:DisplayObjectContainer=null){
            this.logger = Log.getLogger(describeType(this).@name);
            this._stage = _arg1;
            this._stageVideoUsed = false;
            this._x = _arg2.x;
            this._y = _arg2.y;
            this._width = _arg2.width;
            this._height = _arg2.height;
            this._video = new Video(this._width, this._height);
            this._video.addEventListener(RENDER_STATE, this.videoRenderState);
            this._displayObjectContainer = _arg3;
            if ((this._displayObjectContainer == null)){
                this._stage.addChildAt(this._video, 0);
            } else {
                _arg3.addChildAt(this._video, 0);
            };
            this._stageVideoRect = new Rectangle(this._x, this._y, this._width, this._height);
            this._stage.addEventListener(STAGE_VIDEO_AVAILABILITY, this.stageVideoAvailability);
            this._stage.addEventListener(FullScreenEvent.FULL_SCREEN, this.displayStateEvent);
            _instances.push(this);
            this.initializeStageVideo();
            this.setViewPort();
        }
        public function attachNetStream(_arg1:NetStream):void{
            this.throwErrorIfReleased();
            this._netStream = _arg1;
            if (this._emulateSizeEvent){
                this._netStream.addEventListener(NETSTREAM_RENDER, this.netStreamRender);
                this._hasEmulateSizeEvent = true;
                this._emulateSizeWidth = -1;
                this._emulateSizeHeight = -1;
            };
            this.reattachNetStream();
        }
        public function gotoToFullScreen(_arg1:Object, _arg2:Object, _arg3:Boolean=false, _arg4:Boolean=true):void{
            var _local5:Number;
            var _local6:Number;
            this._player = _arg1;
            this._videoContainer = _arg2;
            this._usingFlex = _arg3;
            this._rectBeforeFullScreen = new Rectangle(this._x, this._y, this._width, this._height);
            if ((Capabilities.screenResolutionX / Capabilities.screenResolutionY) > (this.videoWidth / this.videoHeight)){
                _local6 = Capabilities.screenResolutionY;
                _local5 = ((this.videoWidth * _local6) / this.videoHeight);
            } else {
                _local5 = Capabilities.screenResolutionX;
                _local6 = ((this.videoHeight * _local5) / this.videoWidth);
            };
            this.width = Math.max(_local5, this.videoWidth);
            this.height = Math.max(_local6, this.videoHeight);
            this.x = 0;
            this.y = 0;
            this._stage.fullScreenSourceRect = new Rectangle(this.x, this.y, this.width, this.height);
            if (!this._stageVideoUsed){
                if (((!((this._displayObjectContainer == null))) && (!((this._displayObjectContainer == this._videoContainer))))){
                    if (_arg3){
                        _arg1.addElement(this._displayObjectContainer);
                    } else {
                        _arg1.addChild(this._displayObjectContainer);
                    };
                };
            } else {
                _arg1.visible = !(_arg4);
            };
            _arg1.stage.displayState = StageDisplayState.FULL_SCREEN;
        }
        public function set width(_arg1:Number):void{
            this.throwErrorIfReleased();
            this._width = _arg1;
            this.setViewPort();
        }
        public function get width():Number{
            this.throwErrorIfReleased();
            return (this._width);
        }
        public function set height(_arg1:Number):void{
            this.throwErrorIfReleased();
            this._height = _arg1;
            this.setViewPort();
        }
        public function get height():Number{
            this.throwErrorIfReleased();
            return (this._height);
        }
        public function set x(_arg1:Number):void{
            this.throwErrorIfReleased();
            this._x = _arg1;
            this.setViewPort();
        }
        public function get x():Number{
            this.throwErrorIfReleased();
            return (this._x);
        }
        public function set y(_arg1:Number):void{
            this.throwErrorIfReleased();
            this._y = _arg1;
            this.setViewPort();
        }
        public function get y():Number{
            this.throwErrorIfReleased();
            return (this._y);
        }
        public function set visible(_arg1:Boolean):void{
            this.throwErrorIfReleased();
            this._visible = _arg1;
            this.setViewPort();
        }
        public function get visible():Boolean{
            this.throwErrorIfReleased();
            return (this._visible);
        }
        public function get video():Video{
            return (this._video);
        }
        public function get stageVideo():Object{
            return (this._stageVideo);
        }
        public function get stageVideoUsed():Boolean{
            this.throwErrorIfReleased();
            return (this._stageVideoUsed);
        }
        public function get videoDecodingStatus():String{
            return (this._videoDecodingStatus);
        }
        public function get videoCompositingStatus():String{
            return (this._videoCompositingStatus);
        }
        public function get status():String{
            this.throwErrorIfReleased();
            return (this._status);
        }
        public function get videoWidth():uint{
            var _local1:uint;
            this.throwErrorIfReleased();
            if (((this._stageVideoUsed) && (this._stageVideo))){
                _local1 = (this._stageVideo as StageVideo).videoWidth;
            } else {
                _local1 = (((this._video.videoWidth == 0)) ? this._video.width : this._video.videoWidth);
            };
            return (_local1);
        }
        public function get videoHeight():uint{
            var _local1:uint;
            this.throwErrorIfReleased();
            if (((this._stageVideoUsed) && (this._stageVideo))){
                _local1 = (this._stageVideo as StageVideo).videoHeight;
            } else {
                _local1 = (((this._video.videoHeight == 0)) ? this._video.height : this._video.videoHeight);
            };
            return (_local1);
        }
        public function focus():void{
            this.throwErrorIfReleased();
            _focus = this;
            var _local1:int;
            while (_local1 < _instances.length) {
                _instances[_local1].setViewPort();
                _local1++;
            };
        }
        public function release():void{
            var _local3:int;
            this.throwErrorIfReleased();
            var _local1:Vector.<StageVideoHelper> = new Vector.<StageVideoHelper>();
            var _local2:int;
            while (_local2 < _instances.length) {
                if (_instances[_local2] != this){
                    _local1.push(_instances[_local2]);
                };
                _local2++;
            };
            _instances = _local1;
            this.visible = false;
            if (this._stageVideoUsed){
                if (this._netStream){
                    (this._stageVideo as StageVideo).attachNetStream(null);
                };
            } else {
                if (this._netStream){
                    this._video.attachNetStream(null);
                };
            };
            this._stage.removeChild(this._video);
            this._stageVideo = null;
            this._stage = null;
            if (_instances.length > 0){
                _focus = _instances[0];
                _local3 = 0;
                while (_local3 < _instances.length) {
                    _instances[_local3].setViewPort();
                    _local3++;
                };
            } else {
                _focus = null;
            };
        }
        private function displayStateEvent(_arg1:FullScreenEvent):void{
            if (((!((this._stage.scaleMode == StageScaleMode.NO_SCALE))) && (_arg1.fullScreen))){
                this._fullScreenSmooth = true;
            } else {
                this._fullScreenSmooth = false;
            };
            if ((((this._stage.displayState == StageDisplayState.NORMAL)) && (this._player))){
                this._player.visible = true;
                if (!this._stageVideoUsed){
                    if (((!((this._displayObjectContainer == null))) && (!((this._displayObjectContainer == this._videoContainer))))){
                        if (this._usingFlex){
                            this._videoContainer.addElement(this._displayObjectContainer);
                        } else {
                            this._videoContainer.addChild(this._displayObjectContainer);
                        };
                    };
                };
                this.x = this._rectBeforeFullScreen.x;
                this.y = this._rectBeforeFullScreen.y;
                this.width = this._rectBeforeFullScreen.width;
                this.height = this._rectBeforeFullScreen.height;
            };
            this.setViewPort();
        }
        private function setViewPort():void{
            var _local1:Point;
            var _local2:int;
            this._stageVideoRect.x = this._x;
            this._stageVideoRect.y = this._y;
            this._stageVideoRect.width = this._width;
            this._stageVideoRect.height = this._height;
            if (((this._stageVideoUsed) && (this._stageVideo))){
                if (((this._stageVideoUsed) && ((this._stageVideo == this)))){
                    (this._stageVideo as StageVideo).viewPort = this._stageVideoRect;
                } else {
                    if (((!((this._displayObjectContainer == null))) && ((this._displayObjectContainer.stage.displayState == StageDisplayState.NORMAL)))){
                        _local1 = this._displayObjectContainer.localToGlobal(new Point(this._displayObjectContainer.x, this._displayObjectContainer.y));
                        this._stageVideoRect.x = (this._stageVideoRect.x + _local1.x);
                        this._stageVideoRect.y = (this._stageVideoRect.y + _local1.y);
                    };
                    (this._stageVideo as StageVideo).viewPort = this._stageVideoRect;
                };
            };
            this._video.x = this._x;
            this._video.y = this._y;
            this._video.width = this._width;
            this._video.height = this._height;
            if (((this._stageVideoUsed) && (this._stageVideo))){
                this._video.visible = false;
            } else {
                if (((this._visible) && ((_focus == this)))){
                    if (((((((((!(this._fullScreenSmooth)) && ((this._video.width == this._video.videoWidth)))) && ((this._video.height == this._video.videoHeight)))) && (((this._video.x - Math.floor(this._video.x)) == 0)))) && (((this._video.y - Math.floor(this._video.y)) == 0)))){
                        this._video.smoothing = false;
                    } else {
                        this._video.smoothing = true;
                    };
                    _local2 = 0;
                    while (_local2 < _instances.length) {
                        if ((this._displayObjectContainer == null)){
                            this._stage.setChildIndex(_instances[_local2]._video, _local2);
                        } else {
                            this._displayObjectContainer.setChildIndex(_instances[_local2]._video, _local2);
                        };
                        _local2++;
                    };
                    this._video.visible = true;
                } else {
                    this._video.visible = false;
                };
            };
        }
        private function removeEmulateSizeEvent():void{
            if (((((this._emulateSizeEvent) && (this._hasEmulateSizeEvent))) && (this._netStream))){
                this._netStream.removeEventListener(NETSTREAM_RENDER, this.netStreamRender);
                this._hasEmulateSizeEvent = false;
                this._emulateSizeEvent = false;
            };
        }
        private function stageVideoAvailability(_arg1:Object):void{
            this.removeEmulateSizeEvent();
            this.debug((STAGE_VIDEO_AVAILABILITY + _arg1["availability"]));
            if (this._netStream){
                if (_arg1["availability"].toString() == "available"){
                    this.initializeStageVideo();
                } else {
                    this.initializeVideo();
                };
            };
        }
        private function videoRenderState(_arg1:Object):void{
            this.removeEmulateSizeEvent();
            if ((((_arg1.status == null)) || ((_arg1.status == "unavailable")))){
            } else {
                if (_arg1.status == "accelerated"){
                    this._status = STATUS_VIDEO_ACCELERATED;
                    this._videoDecodingStatus = "GPU";
                    this._videoCompositingStatus = "GPU";
                    this.setViewPort();
                } else {
                    if (_arg1.status == "software"){
                        this._status = STATUS_VIDEO_SOFTWARE;
                        this._videoDecodingStatus = "SOFTWARE";
                        this._videoCompositingStatus = "SOFTWARE";
                        this.setViewPort();
                    };
                };
            };
            dispatchEvent(new StageVideoHelperEvent(this.videoWidth, this.videoHeight, this.status));
        }
        private function stageVideoRenderState(_arg1:Object):void{
            this.removeEmulateSizeEvent();
            if ((_arg1 as StageVideoEvent).status == "unavailable"){
                this.initializeVideo();
            } else {
                if ((_arg1 as StageVideoEvent).status == "accelerated"){
                    this._status = STATUS_STAGEVIDEO_ACCELERATED;
                    this._videoDecodingStatus = "GPU";
                    this._videoCompositingStatus = "GPU";
                    this.setViewPort();
                } else {
                    if ((_arg1 as StageVideoEvent).status == "software"){
                        this._status = STATUS_STAGEVIDEO_SOFTWARE;
                        this._videoDecodingStatus = "SOFTWARE";
                        this._videoCompositingStatus = "GPU";
                        this.setViewPort();
                    };
                };
            };
            dispatchEvent(new StageVideoHelperEvent(this.videoWidth, this.videoHeight, this.status));
        }
        private function netStreamRender(_arg1:Object):void{
            if (this._emulateSizeEvent){
                if (((!((this._emulateSizeWidth == this._video.videoWidth))) || (!((this._emulateSizeHeight == this._video.videoHeight))))){
                    this._emulateSizeWidth = this._video.videoWidth;
                    this._emulateSizeHeight = this._video.videoHeight;
                    this._status = STATUS_STAGEVIDEO_SOFTWARE;
                    this.setViewPort();
                    dispatchEvent(new StageVideoHelperEvent(this.videoWidth, this.videoHeight, this.status));
                };
            };
        }
        private function initializeVideo():void{
            this._stageVideo = null;
            this._stageVideoUsed = false;
            this.reattachNetStream();
        }
        private function initializeStageVideo():void{
            var _local1:int;
            var _local2:Boolean;
            var _local3:int;
            if (this._stage.hasOwnProperty("stageVideos")){
                if (this._stage.stageVideos.length > 0){
                    this.removeEmulateSizeEvent();
                    _local1 = 0;
                    while (_local1 < this._stage.stageVideos.length) {
                        _local2 = false;
                        _local3 = 0;
                        while (_local3 < _instances.length) {
                            if ((_instances[_local3]._stageVideo as StageVideo) == this._stage.stageVideos[_local1]){
                                _local2 = true;
                            };
                            _local3++;
                        };
                        if (!_local2){
                            this._stageVideo = this._stage.stageVideos[_local1];
                            this._stageVideoUsed = true;
                            (this._stageVideo as StageVideo).addEventListener(RENDER_STATE, this.stageVideoRenderState);
                            this.reattachNetStream();
                            return;
                        };
                        _local1++;
                    };
                    this.initializeVideo();
                } else {
                    this._emulateSizeEvent = true;
                    this.initializeVideo();
                };
            } else {
                this._emulateSizeEvent = true;
                this.initializeVideo();
            };
        }
        private function reattachNetStream():void{
            if (((this._stageVideoUsed) && (this._stageVideo))){
                if (this._netStream){
                    (this._stageVideo as StageVideo).attachNetStream(this._netStream);
                };
            } else {
                if (this._netStream){
                    this._video.attachNetStream(this._netStream);
                };
            };
            this.setViewPort();
        }
        private function throwErrorIfReleased():void{
            if (this._stage == null){
                throw (new Error(RELEASE_ERROR_MSG));
            };
        }
        private function debug(_arg1:String):void{
            if (this.logger != null){
                this.logger.debug(("sv: " + _arg1));
            };
        }

    }
}//package com.akamai.stagevideo 
