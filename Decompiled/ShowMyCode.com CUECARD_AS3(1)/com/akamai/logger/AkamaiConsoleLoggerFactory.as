﻿package com.akamai.logger {
    import org.osmf.logging.*;
    import flash.utils.*;

    public class AkamaiConsoleLoggerFactory extends LoggerFactory {

        protected static var loggers:Dictionary = new Dictionary(true);

        override public function getLogger(_arg1:String):Logger{
            var _local2:AkamaiConsoleLogger = loggers[_arg1];
            if (_local2 == null){
                _local2 = new AkamaiConsoleLogger(_arg1);
                loggers[_arg1] = _local2;
            };
            return (_local2);
        }

    }
}//package com.akamai.logger 
