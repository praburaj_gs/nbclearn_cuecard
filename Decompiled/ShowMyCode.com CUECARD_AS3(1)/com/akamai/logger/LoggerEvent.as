﻿package com.akamai.logger {
    import flash.events.*;

    public class LoggerEvent extends Event {

        public static const ERROR_EVENT:String = "loggerMessage";
        public static const LEVEL_DEBUG:String = "DEBUG";
        public static const LEVEL_INFO:String = "INFO";
        public static const LEVEL_WARN:String = "WARN";
        public static const LEVEL_ERROR:String = "ERROR";
        public static const LEVEL_FATAL:String = "FATAL";

        protected var _level:String;
        protected var _msg:Object;

        public function LoggerEvent(_arg1:String="loggerMessage", _arg2:Boolean=false, _arg3:Boolean=false, _arg4:Object=null, _arg5:String="DEBUG"){
            this._msg = _arg4;
            this._level = _arg5;
            super(ERROR_EVENT, _arg2, _arg3);
        }
        override public function clone():Event{
            return (new LoggerEvent(type, bubbles, cancelable, this._msg, this._level));
        }
        public function get level():String{
            return (this._level);
        }
        public function get msg():String{
            if (this._msg){
                return (this._msg.toString());
            };
            return ("");
        }
        override public function toString():String{
            return (this.msg);
        }

    }
}//package com.akamai.logger 
