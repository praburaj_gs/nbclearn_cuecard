﻿package com.akamai.logger {
    import flash.events.*;
    import org.osmf.logging.*;
    import com.akamai.logger.bridge.*;

    public class AkamaiConsoleLogger extends Logger {

        public static const ALL:uint = 2;
        public static const FILTER:uint = 1;
        public static const NONE:uint = 0;
        public static const DEFAULT_CATEGORY:String = "none";

        protected static var _filterLevel:uint = 0;
        protected static var _filterRegEx:RegExp;
        protected static var disp:EventDispatcher;
        protected static var _callBack:Function;
        protected static var _emitTraces:Boolean = true;
        protected static var consoleBridgeEnabled:Boolean = false;

        public function AkamaiConsoleLogger(_arg1:String){
            super(_arg1);
        }
        public static function set filterRegExp(_arg1:RegExp):void{
            _filterRegEx = _arg1;
            filterLevel = AkamaiConsoleLogger.FILTER;
        }
        public static function enable(_arg1:Boolean=true):void{
            if (_arg1){
                filterLevel = ALL;
            } else {
                filterLevel = NONE;
            };
        }
        public static function set filterLevel(_arg1:uint):void{
            _filterLevel = _arg1;
        }
        public static function get filterLevel():uint{
            return (_filterLevel);
        }
        public static function stopConsoleListening():void{
            consoleBridgeEnabled = false;
            TraceConsoleBridge.inst.stopListening();
        }
        public static function startConsoleListening():void{
            consoleBridgeEnabled = true;
            TraceConsoleBridge.inst.restartListening();
        }
        public static function set emitTraces(_arg1:Boolean):void{
            _emitTraces = _arg1;
        }
        public static function get emitTraces():Boolean{
            return (_emitTraces);
        }

        override public function debug(_arg1:String, ... _args):void{
            this.routeMsg(_arg1, LoggerEvent.LEVEL_DEBUG, _args);
        }
        override public function info(_arg1:String, ... _args):void{
            this.routeMsg(_arg1, LoggerEvent.LEVEL_INFO, _args);
        }
        override public function warn(_arg1:String, ... _args):void{
            this.routeMsg(_arg1, LoggerEvent.LEVEL_WARN, _args);
        }
        override public function error(_arg1:String, ... _args):void{
            this.routeMsg(_arg1, LoggerEvent.LEVEL_ERROR, _args);
        }
        override public function fatal(_arg1:String, ... _args):void{
            this.routeMsg(_arg1, LoggerEvent.LEVEL_FATAL, _args);
        }
        protected function returnMessage(_arg1:String, _arg2:String, _arg3:Array):String{
            var _local4 = "";
            _local4 = (_local4 + ((((((("[" + category) + ",") + new Date().toLocaleString()) + ",") + _arg1) + "] ") + this.applyParams(_arg2, _arg3)));
            return (_local4);
        }
        private function applyParams(_arg1:String, _arg2:Array):String{
            var _local3:String = _arg1;
            var _local4:int = _arg2.length;
            var _local5:int;
            while (_local5 < _local4) {
                _local3 = _local3.replace(new RegExp((("\\{" + _local5) + "\\}"), "g"), _arg2[_local5]);
                _local5++;
            };
            return (_local3);
        }
        public function setCallback(_arg1:Function):void{
            _callBack = _arg1;
        }
        public function clearCallback():void{
            _callBack = null;
        }
        public function addEventListener(... _args):void{
            if (disp == null){
                disp = new EventDispatcher();
            };
            disp.addEventListener.apply(null, _args);
        }
        public function removeEventListener(... _args):void{
            if (disp == null){
                return;
            };
            disp.removeEventListener.apply(null, _args);
        }
        protected function dispatchEvent(... _args):void{
            if (disp == null){
                return;
            };
            disp.dispatchEvent.apply(null, _args);
        }
        protected function routeMsg(_arg1:String, _arg2:String="DEBUG", ... _args):void{
            var _local4:String = this.returnMessage(_arg2, _arg1, _args);
            if ((((((LoggerEvent.LEVEL_ERROR == _arg2)) || ((LoggerEvent.LEVEL_FATAL == _arg2)))) || ((_filterLevel == ALL)))){
                this.sendMsg(_local4, _arg2);
                return;
            };
            if ((((_filterRegEx == null)) || ((_filterLevel == NONE)))){
                return;
            };
            var _local5:Boolean = _filterRegEx.test(_local4);
            if (!_local5){
                return;
            };
            this.sendMsg(_local4, _arg2);
        }
        protected function sendMsg(_arg1:String, _arg2:String="DEBUG"):void{
            var _local3:LoggerEvent = new LoggerEvent(LoggerEvent.ERROR_EVENT, false, false, _arg1, _arg2);
            if (_callBack != null){
                _callBack(_local3);
            };
            if (emitTraces){
                trace(_arg1);
            };
            this.dispatchEvent(_local3);
            if (consoleBridgeEnabled){
                TraceConsoleBridge.inst.send(_arg1);
            };
        }

    }
}//package com.akamai.logger 
