﻿package com.akamai.logger.bridge {
    import flash.events.*;
    import flash.net.*;
    import flash.utils.*;
    import com.akamai.logger.*;

    public class TraceConsoleBridge {

        protected static const NAME_ToApp:String = "_Bcon2APP";
        protected static const NAME_ToCon:String = "_Bapp2CON";
        private static const ONDATA_FUNCTION:String = "onAppDataReceived";
        private static const CLASS_NAME:String = "TraceConsoleBridge";

        private static var connected:Boolean = false;
        private static var conToApp:LocalConnection;
        private static var appToCon:LocalConnection;
        private static var SLEEP_TIME:uint = 2;
        private static var _inst:TraceConsoleBridge;

        private var startListeningTimer:Timer;
        private var decayTimer:Timer;

        public function TraceConsoleBridge(_arg1:SingletonBlocker):void{
            if (_arg1 == null){
                throw (new Error("It is a singleton"));
            };
            this.startListeningTimer = new Timer(((SLEEP_TIME * 3) * 1000));
            this.decayTimer = new Timer((SLEEP_TIME * 1000));
            this.startListeningTimer.addEventListener(TimerEvent.TIMER, this.onStartListeningTimer);
            this.startListeningTimer.start();
            this.decayTimer.addEventListener(TimerEvent.TIMER, this.onDecayTimer);
            this.setupListener();
        }
        public static function get inst():TraceConsoleBridge{
            if (!_inst){
                _inst = new TraceConsoleBridge(new SingletonBlocker());
            };
            return (_inst);
        }

        public function stopListening():void{
            this.disconnect();
            this.startListeningTimer.stop();
        }
        public function restartListening():void{
            this.startListeningTimer.start();
        }
        private function onStartListeningTimer(_arg1:TimerEvent):void{
            if (connected){
                return;
            };
            this.setupListener();
        }
        private function onDecayTimer(_arg1:TimerEvent):void{
            if (connected){
                return;
            };
            this.decayTimer.stop();
            this.disconnect();
        }
        private function disconnect():void{
            if (conToApp){
                conToApp.removeEventListener(AsyncErrorEvent.ASYNC_ERROR, this.onComAErr);
                conToApp.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onComSErr);
                conToApp.removeEventListener(StatusEvent.STATUS, this.onComListenStatus);
                try {
                    conToApp.close();
                    conToApp = null;
                } catch(err:Error) {
                };
                connected = false;
            };
            if (appToCon){
                appToCon.removeEventListener(AsyncErrorEvent.ASYNC_ERROR, this.onComAErr);
                appToCon.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onComSErr);
                appToCon.removeEventListener(StatusEvent.STATUS, this.onConSentStatus);
                try {
                    appToCon.close();
                    appToCon = null;
                } catch(err:Error) {
                };
                connected = false;
            };
        }
        private function setupListener():void{
            if (connected){
                return;
            };
            conToApp = new LocalConnection();
            conToApp.allowInsecureDomain("*");
            conToApp.allowDomain("*");
            conToApp.addEventListener(AsyncErrorEvent.ASYNC_ERROR, this.onComAErr);
            conToApp.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onComSErr);
            conToApp.addEventListener(StatusEvent.STATUS, this.onComListenStatus);
            conToApp.client = this;
            conToApp.isPerUser = false;
            try {
                conToApp.connect(NAME_ToApp);
            } catch(err:Error) {
                if (err.errorID == 2082){
                    firstContact();
                } else {
                    lostContact();
                };
            };
            this.decayTimer.start();
        }
        private function connectToConsole():void{
            if (appToCon){
                appToCon.removeEventListener(AsyncErrorEvent.ASYNC_ERROR, this.onComAErr);
                appToCon.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onComSErr);
                appToCon.removeEventListener(StatusEvent.STATUS, this.onConSentStatus);
                try {
                    appToCon.close();
                    appToCon = null;
                } catch(err:Error) {
                };
            };
            appToCon = new LocalConnection();
            appToCon.allowInsecureDomain("*");
            conToApp.allowDomain("*");
            appToCon.addEventListener(AsyncErrorEvent.ASYNC_ERROR, this.onComAErr);
            appToCon.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onComSErr);
            appToCon.addEventListener(StatusEvent.STATUS, this.onConSentStatus);
            appToCon.isPerUser = false;
            this.send("ACK");
        }
        public function onConDataReceived(_arg1:String, _arg2:String=null):void{
            if (!connected){
                this.firstContact();
            };
            this.send(("ACK " + _arg1));
            if ("REGEX" == _arg1){
                if ("ALL" == _arg2){
                    AkamaiConsoleLogger.filterLevel = AkamaiConsoleLogger.ALL;
                    return;
                };
                if ("NONE" == _arg2){
                    AkamaiConsoleLogger.filterLevel = AkamaiConsoleLogger.NONE;
                    return;
                };
                AkamaiConsoleLogger.filterLevel = AkamaiConsoleLogger.FILTER;
                AkamaiConsoleLogger.filterRegExp = new RegExp(_arg2);
            };
            if ("RESET" == _arg1){
                AkamaiConsoleLogger.filterLevel = AkamaiConsoleLogger.NONE;
                AkamaiConsoleLogger.filterRegExp = null;
                this.disconnect();
            };
        }
        private function firstContact():void{
            AkamaiConsoleLogger.filterLevel = AkamaiConsoleLogger.ALL;
            connected = true;
            this.connectToConsole();
        }
        private function lostContact():void{
            this.disconnect();
        }
        public function send(_arg1:String):void{
            if (!connected){
                return;
            };
            appToCon.send(NAME_ToCon, ONDATA_FUNCTION, _arg1);
        }
        private function onComListenStatus(_arg1:StatusEvent):void{
            if (_arg1.level != "status"){
                this.lostContact();
            };
        }
        private function onConSentStatus(_arg1:StatusEvent):void{
            if (_arg1.level != "status"){
                this.lostContact();
            };
        }
        private function onComAErr(_arg1:AsyncErrorEvent):void{
            this.lostContact();
        }
        private function onComSErr(_arg1:SecurityErrorEvent):void{
            this.lostContact();
        }

    }
}//package com.akamai.logger.bridge 

class SingletonBlocker {

    public function SingletonBlocker(){
    }
}
