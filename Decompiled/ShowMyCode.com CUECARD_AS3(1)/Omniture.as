﻿package {
    import com.omniture.*;

    public class Omniture {

        private var omni_mc:ActionSource;
        private var cueCard:AS3CueCard;
        private var trackOmnitureEvents:Boolean;
        private var omnitureXML:XMLList;
        public var omnitureExtension:Object;
        public var blackBoardInternalExternal:String;

        public function Omniture(_arg1:AS3CueCard){
            this.cueCard = _arg1;
            this.trackOmnitureEvents = true;
            this.omni_mc = new ActionSource();
        }
        public function trackNewCard(_arg1:String, _arg2:String, _arg3:String, _arg4:Boolean=false){
            if (this.trackOmnitureEvents == true){
                trace("!Track New Card");
                this.omni_mc.prop14 = _arg3;
                this.omni_mc.eVar25 = this.omni_mc.prop14;
                if (this.cueCard.metaData.Source){
                    this.omni_mc.eVar6 = this.cueCard.metaData.Source;
                };
                if (this.cueCard.metaData.Creator){
                    this.omni_mc.eVar7 = this.cueCard.metaData.Creator;
                };
                this.omni_mc.eVar8 = _arg2;
                this.omni_mc.eVar9 = _arg1;
                this.omni_mc.prop10 = _arg1;
                this.omni_mc.eVar18 = "Cue Card";
                this.omni_mc.prop11 = ("Cue Card|" + this.omni_mc.pageName);
                if (this.cueCard.metaData.collectionName){
                    if (this.cueCard.metaData.collectionName != "Unknown"){
                        this.omni_mc.eVar35 = this.cueCard.metaData.collectionName;
                    } else {
                        if (this.isNotUndefined(this.cueCard.metaData.freeResourceCollection)){
                            this.omni_mc.eVar35 = this.cueCard.metaData.freeResourceCollection;
                        };
                    };
                };
                this.omni_mc.track();
                this.omni_mc.prop10 = "";
                this.omni_mc.prop11 = "";
                this.omni_mc.prop14 = "";
                this.omni_mc.prop22 = "";
                this.omni_mc.prop9 = "";
                this.trackEvent("event34");
                if (_arg4){
                    this.trackEvent("event11");
                };
            } else {
                trace("!Track Local: New Card");
            };
        }
        public function trackRelatedClick(){
            this.trackEvent("event12");
        }
        public function trackEvent(_arg1:String, _arg2:String=null){
            if (this.trackOmnitureEvents == true){
                this.omni_mc.events = _arg1;
                this.omni_mc.prop1 = _arg1;
                this.omni_mc.trackLink("", "o", "customEvent");
                this.omni_mc.events = "";
                this.omni_mc.prop1 = "";
            };
        }
        public function setUpOmniture(_arg1:XMLList){
            this.omnitureXML = _arg1;
            this.omni_mc.account = this.omnitureXML.account;
            this.omni_mc.site = this.omnitureXML.site;
            this.omni_mc.charSet = this.omnitureXML.charSet;
            this.omni_mc.currencyCode = this.omnitureXML.currencyCode;
            this.omni_mc.visitorNamespace = this.omnitureXML.visitorNamespace;
            this.omni_mc.dc = 122;
            if (this.cueCard.isHTTPS == true){
                this.omni_mc.trackingServer = "osimg.nbcuni.com";
            } else {
                this.omni_mc.trackingServer = "oimg.nbcuni.com";
            };
            this.omni_mc.prop8 = "Corporate";
            this.omni_mc.prop9 = this.omnitureXML.businessUnit;
            this.omni_mc.eVar4 = this.omnitureXML.productType;
            this.omni_mc.pageName = "";
            this.omni_mc.pageURL = "";
            this.omni_mc.trackClickMap = false;
            this.omni_mc.movieID = "";
            this.omni_mc.debugTracking = false;
            this.omni_mc.trackLocal = true;
            if (this.isNotUndefined(this.cueCard.metaData.communityID)){
                this.omni_mc.eVar23 = this.cueCard.metaData.communityID;
            };
            if (this.isNotUndefined(this.cueCard.metaData.communityName)){
                this.omni_mc.eVar24 = this.cueCard.metaData.communityName;
            };
            if (this.isNotUndefined(this.cueCard.metaData.State)){
                this.omni_mc.eVar27 = this.cueCard.metaData.State;
            };
            if (this.isNotUndefined(this.cueCard.metaData.role)){
                this.omni_mc.eVar30 = this.cueCard.metaData.role;
            };
            if (this.isNotUndefined(this.cueCard.metaData.email)){
                this.omni_mc.eVar2 = this.cueCard.metaData.email;
            } else {
                this.omni_mc.eVar2 = this.cueCard.metaData.userName;
            };
            if (this.isNotUndefined(this.cueCard.metaData.userName)){
                this.omni_mc.eVar3 = this.cueCard.metaData.userName;
            };
            if (this.isNotUndefined(this.cueCard.metaData.freeResourceCollection)){
                this.omni_mc.eVar34 = this.cueCard.metaData.freeResourceCollection;
            };
            if (this.isNotUndefined(this.cueCard.metaData.schoolAssociation)){
                this.omni_mc.eVar26 = this.cueCard.metaData.schoolAssociation;
            };
            if (this.isNotUndefined(this.cueCard.metaData.BBCourseName)){
                this.omni_mc.eVar37 = this.cueCard.metaData.BBCourseName;
            };
            this.omni_mc.eVar48 = "Flash";
            if ((((this.cueCard.Product == "BBHE")) || ((this.cueCard.Product == "BBK12")))){
                if (this.isNotUndefined(this.blackBoardInternalExternal)){
                    this.omni_mc.eVar5 = this.blackBoardInternalExternal;
                    this.omni_mc.prop22 = this.blackBoardInternalExternal;
                };
                if (this.isNotUndefined(this.cueCard.metaData.communityName)){
                    this.omni_mc.eVar24 = this.cueCard.metaData.communityName;
                };
            };
            this.cueCard.addChild(this.omni_mc);
        }
        public function trackVideoCompleate(){
            this.trackEvent("event78");
        }
        public function trackVideoStart(){
            this.trackEvent("event74");
        }
        public function trackSeconds(_arg1:Number):void{
            if (_arg1 < 0){
                _arg1 = 0;
            };
            this.omni_mc.products = (";;;;event73=" + _arg1);
            this.omni_mc.events = "event73";
            this.omni_mc.prop1 = "event73";
            this.omni_mc.trackLink("", "o", "customEvent");
            this.omni_mc.events = "";
            this.omni_mc.prop1 = "";
            this.omni_mc.products = "";
        }
        public function overrideVideoPlayer(_arg1:Object, _arg2:Object, _arg3:Object){
            var _omnitureExtension:* = _arg1;
            var embeddedPlayer:* = _arg2;
            var videoPlayer:* = _arg3;
            this.omnitureExtension = _omnitureExtension;
            var omnitureOBJ:* = new Object();
            omnitureOBJ.omniture_prop8 = "Archives On Demand";
            omnitureOBJ.omniture_prop9 = this.omnitureXML.businessUnit;
            omnitureOBJ.omniture_prop10 = "";
            omnitureOBJ.omniture_prop14 = "NBC Learn";
            omnitureOBJ.omniture_prop11 = "";
            omnitureOBJ.omniture_prop20 = this.omnitureXML.account;
            omnitureOBJ.omniture_eVar45 = "Archives On Demand";
            omnitureOBJ.omniture_eVar24 = this.cueCard.metaData.communityName;
            omnitureOBJ.omniture_eVar2 = this.cueCard.metaData.userName;
            omnitureOBJ.omniture_pageName = "";
            if (this.cueCard.isHTTPS == true){
                omnitureOBJ.omniture_trackingServer = "osimg.nbcuni.com";
            } else {
                omnitureOBJ.omniture_trackingServer = "oimg.nbcuni.com";
            };
            try {
                omnitureOBJ.omniture_eVar34 = this.cueCard.metaData.freeResourceCollection;
            } catch(e:Error) {
            };
            try {
                this.omnitureExtension.updateOverrides(omnitureOBJ);
            } catch(e:Error) {
                e.message;
                trace("--OMNITURE FAIL ON omnitureExtension--");
                trace(e.message);
            };
        }
        private function isNotUndefined(_arg1){
            if ((((((((((((_arg1 == undefined)) || ((_arg1 == "undefined")))) || ((_arg1 == null)))) || ((_arg1 == "null")))) || ((String(_arg1) == "")))) || ((String(_arg1) == " ")))){
                return (false);
            };
            return (true);
        }

    }
}//package 
