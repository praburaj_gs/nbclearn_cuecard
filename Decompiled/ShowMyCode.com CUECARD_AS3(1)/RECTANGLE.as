﻿package {
    import flash.display.*;
    import flash.geom.*;

    public class RECTANGLE extends Sprite {

        public function RECTANGLE(_arg1:Number, _arg2:Number, _arg3:Number, _arg4=null, _arg5=null){
            var _local6:Matrix;
            var _local7:Number;
            super();
            if (_arg4){
                _local6 = new Matrix();
                _local7 = 0;
                if (_arg5){
                    _local7 = ((Math.PI * _arg5) / 180);
                };
                _local6.createGradientBox(_arg1, _arg2, _local7);
                this.graphics.beginGradientFill(GradientType.LINEAR, [_arg3, _arg4], [1, 1], [0, 0xFF], _local6, SpreadMethod.PAD, "rgb", 1);
            } else {
                this.graphics.beginFill(_arg3, 1);
            };
            this.graphics.drawRect(0, 0, _arg1, _arg2);
        }
    }
}//package 
