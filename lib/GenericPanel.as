﻿package 
{
	import fl.motion.Motion;
	import fl.transitions.easing.Strong;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.FileFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.net.URLRequest
	import flash.display.Loader
	
	/**
	 * ...
	 * @author Ari
	 */
	
	
	public class GenericPanel extends MovieClip 
	{
		private var title_txt:TextField;
		private var file_txt:TextField;
		private var fileDirBackground:MovieClip
		
		private var button_1:MovieClip;
		private var button_2:MovieClip;
		private var buttonTxtFmt:TextFormat;
		private var mainTextFmt:TextFormat;
		private var fileTextFmt:TextFormat;
		
		private var _butonCallBack:Function;
		private var buttonColor:uint;
		private var _cancelFunction:Function;
		
		private var imageLoader:Loader;
		private var image:Sprite;
		private var logoNotLoaded:Boolean = true;
		private var dislayImage:Boolean = false;
		private var onlyOneButton:Boolean = false;
		
		
		
	//	private var butonCallBack:Function;
		
		
		
		public function GenericPanel() {
			
			
		// TITLE TEXT
			title_txt = new TextField();
			title_txt.multiline = true;
			title_txt.wordWrap = true;
			title_txt.width =  322;
			title_txt.autoSize = "center";
			title_txt.textColor = 0xFFFFFF;
			title_txt.antiAliasType = "normal";
			
			
			mainTextFmt = new TextFormat();
			mainTextFmt.color = 0xFFFFFF;
			mainTextFmt.bold = false;
			mainTextFmt.size = 14;
			mainTextFmt.font = "Arial";
			mainTextFmt.align = "center";
		
		// FILE DIRECTORY TEXT
		
			file_txt = new TextField();
			file_txt.multiline = true;
			file_txt.wordWrap = true;
			file_txt.width =  270;
			file_txt.autoSize = "center";
			file_txt.textColor = 0x000000;
			file_txt.antiAliasType = "normal";
			
			
			fileTextFmt = new TextFormat();
			fileTextFmt.color = 0x000000;
			fileTextFmt.bold = false;
			fileTextFmt.size = 14;
			fileTextFmt.font = "Arial";
			fileTextFmt.align = "center";
			
			
			
			button_1 = new MainMenuButton(); // Create a new button on the
			button_2 = new MainMenuButton(); // Create a new button on the
			
			buttonTxtFmt = new TextFormat();
			buttonTxtFmt.bold = true;
			buttonTxtFmt.align = "center";
			
			button_1.button_txt.setTextFormat(buttonTxtFmt);
			button_2.button_txt.setTextFormat(buttonTxtFmt);
			
			button_1.bg.width = 120;
			button_2.bg.width = 120;
			
			button_1.button_txt.x = -120;
			button_2.button_txt.x = -120;
			
			button_1.button_txt.width = 120;
			button_2.button_txt.width = 120;
			
			addChild(button_1)
			addChild(button_2);
			
		
			// First Button
			button_1.addEventListener(MouseEvent.CLICK, buttonPressed);
			button_1.addEventListener(MouseEvent.ROLL_OVER, btnRollOver);
			button_1.addEventListener(MouseEvent.ROLL_OUT, btnRollOut);
			
			// Cancel Button
			button_2.addEventListener(MouseEvent.CLICK, cancelPressed);
			button_2.addEventListener(MouseEvent.ROLL_OVER, btnRollOver);
			button_2.addEventListener(MouseEvent.ROLL_OUT, btnRollOut);
				// make the mouse hand visible on roll over
			button_2.buttonMode = true;
			button_2.useHandCursor = true;
			button_1.buttonMode = true;
			button_1.useHandCursor = true;
			
			fileDirBackground = new MainMenuButton();
			fileDirBackground.button_txt.text = '';
			Colors.changeColor(fileDirBackground.bg.grfx, 0xFFFFFF) // set the color of the bg
			addChild(fileDirBackground);
			addChild(title_txt);	
			addChild(file_txt);	
			
		}
		
		public function displayNBCLogo() {
			if (logoNotLoaded) {
				logoNotLoaded = false;
				image = new Sprite();
				imageLoader = new Loader();
				var urlRequest:URLRequest = new URLRequest("introScreen.png");
				imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, logoLoaded);
				
				imageLoader.load(urlRequest);
				addChild(image);
				image.addChild(imageLoader)
			}else {
				dislayImage = true;
				if (!image) {
					addChild(image);
				}else {
					image.visible = true
				}
				arrangeContent();
			}
			
		}
		private function logoLoaded(e:Event = null) {
			dislayImage = true;
			arrangeContent();
		}

		
		public function setText(_text:String) {
			if (_text != "" || _text != " ") {
				dislayImage = false;
				if (image) {
					image.visible = false
				}
				
			}
			this.title_txt.text = _text;
			if (this.title_txt.width > 322) {
				this.title_txt.width = 270;
			}
			this.title_txt.setTextFormat(mainTextFmt);
			
		}
		
		public function setButtonAtributes(label_1:String, label_2:String, _buttonColor:uint,_butonCallBack:Function = null) {
			
		// Text
			button_1.button_txt.text = label_1;
			button_1.button_txt.setTextFormat(buttonTxtFmt);
			if (label_2 == '' || label_2 == " "){
				onlyOneButton = true;
				button_2.visible = false;
			}else {
				button_2.visible = true;
				button_2.button_txt.text = label_2;
				
				button_2.button_txt.setTextFormat(buttonTxtFmt);
				onlyOneButton = false;
			}
			
			
		// Colors
			buttonColor = _buttonColor;
			Colors.changeColor(button_1.bg.grfx, _buttonColor) // set the color of the bg
			Colors.changeColor(button_2.bg.grfx, _buttonColor) // set the color of the bg
			if(_butonCallBack != null){
				butonCallBack = _butonCallBack;
			}

		}
		
		public function removeFileDirectory() {
			fileDirBackground.visible = false;
			file_txt.visible = false;
			arrangeContent();
			
		}
		public function setFileDirectory(_text:String) {
			fileDirBackground.visible = true;
			file_txt.visible = true;
			this.file_txt.text = _text;
			this.file_txt.setTextFormat(fileTextFmt);
			
			fileDirBackground.width = file_txt.width + 10;
			fileDirBackground.height = file_txt.height + 15;
			
			arrangeContent();
			
		}
		
		public function getFileDirectory():String {
			
			return String(this.file_txt.text);
		}
		
		public function  set butonCallBack(butonCallBack:Function) {
			
			_butonCallBack = butonCallBack;
		}
		
		public function  get butonCallBack():Function {
			return _butonCallBack;
		}
		
		public function set cancelFunction(cancelFunction:Function) {
			_cancelFunction = cancelFunction;
		}
		
	//	public function get cancelFunction() {
			//return _cancelFunction;
	//	}
		
		private function btnRollOver(event:MouseEvent) {
			
			var btn:MovieClip = event.currentTarget as MovieClip;
			Colors.changeColor(btn.bg.grfx, 0x333333) // set the color of the bg
		}
		private function btnRollOut(event:MouseEvent) {
			
			var btn:MovieClip = event.currentTarget as MovieClip;
			Colors.changeColor(btn.bg.grfx, buttonColor) // set the color of the bg
		}
		
		
		
		private function buttonPressed(e:MouseEvent) {
			if(_butonCallBack != null){
				_butonCallBack(); // tell the listener that a button has been pressed
			}
		}
		private function cancelPressed(e:MouseEvent) {
			trace("CANCEL");
			if (_cancelFunction != null) {
				_cancelFunction();
			}
		}
	
	
		public function arrangeContent():void {

				// Place the NBC LOGO ON THE BOTOM RIGHT
				this.parent["nbc_logo"].x = this.parent["bg"].width;
				this.parent["nbc_logo"].y = this.parent["bg"].height;
				
				title_txt.x = this.parent["bg"].width / 2 - title_txt.width / 2
				title_txt.y = this.parent["bg"].height / 2 - title_txt.height / 2
				
				if (file_txt.visible) {
					title_txt.y -= 40;
					file_txt.x = this.parent["bg"].width / 2 - file_txt.width / 2;
					file_txt.y = title_txt.y + title_txt.height + 15;
					fileDirBackground.x = file_txt.x + file_txt.width + 5;
					fileDirBackground.y = (file_txt.y +  file_txt.height / 2) - 2.5;
					
					
					button_1.y = fileDirBackground.y + fileDirBackground.height/2 + 22; // possition the buttons under the white background
					
				}else {
					button_1.y = title_txt.y + title_txt.height + 15; // possition the buttons under the title text
				}
				
				
				
				button_2.y  = button_1.y 
				
				if(onlyOneButton == false){ // we are showing both buttons
					button_1.x = title_txt.x + (title_txt.width / 2)  - 5;
					button_2.x = button_1.x + button_2.width + 10;
				}else { // only one button
					button_1.x = title_txt.x + (title_txt.width / 2)  + (button_1.width / 2);
				}
				
				
				
				
				if (dislayImage){
					button_1.visible = false;
					button_2.visible = false;
					image.x = this.parent["bg"].width / 2 - image.width / 2
					image.y = this.parent["bg"].height / 2 - image.height / 2
				}else {
					button_1.visible = true;
					button_2.visible = true;
				}
				
		}
	}
}