﻿package  {
	
	
	import com.akamai.netSession.BaseInterface;
	
	import fl.motion.easing.Elastic;
	import fl.motion.Tweenables;
	import fl.transitions.Tween;
	import flash.accessibility.AccessibilityProperties;
    import flash.display.*;
    import flash.events.*;
	import flash.geom.Rectangle;
    import flash.net.*;
    import flash.system.*;
	import flash.external.ExternalInterface;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import org.osmf.logging.LoggerFactory
	//import org.osmf.logging.TraceLoggerFactory;
	
	//import com.nbcuni.outlet.extensions.embedded_player.api.EmbeddedPlayerConstants
	//import com.nbcuni.outlet.extensions.video_player.constants.VideoPlayerConstants;
	import flash.events.FullScreenEvent;
	import flash.geom.Point;
	import flash.display.StageDisplayState;
	import flash.text.TextFieldAutoSize;
	import flash.ui.Mouse;
	import flash.system.Security;
	import flash.accessibility.Accessibility;
	
	// OSMF Imports
	import org.osmf.media.MediaPlayer;
	import org.osmf.elements.VideoElement;
	import org.osmf.media.DefaultMediaFactory;
	import org.osmf.media.MediaElement;
	import org.osmf.media.MediaPlayerSprite;
	import org.osmf.events.MediaFactoryEvent;
	import org.osmf.events.MediaErrorEvent; 
	import org.osmf.events.MediaElementEvent; 
	import flash.display.StageScaleMode;

	import org.osmf.media.URLResource;
	import org.osmf.media.MediaResourceBase;
	import flash.display.StageAlign;
	import org.osmf.display.ScaleMode;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import org.osmf.media.MediaElement;
	import org.osmf.layout.LayoutMetadata;
	import org.osmf.layout.LayoutMode;
	import org.osmf.layout.HorizontalAlign;
	import org.osmf.layout.VerticalAlign;
	import org.osmf.events.MediaPlayerStateChangeEvent;
	import org.osmf.media.MediaPlayerState;
	import org.osmf.metadata.Metadata;
	import org.osmf.events.TimeEvent;
	import org.osmf.logging.Log; 
	import org.osmf.utils.OSMFSettings;

	
	// Akamai Imports
	import com.akamai.netSession.NetSessionInterface;
	import com.akamai.netSession.BaseInterface; 
	
	
	import com.akamai.logger.AkamaiConsoleLoggerFactory;
	import com.akamai.logger.AkamaiConsoleLogger;
	import com.akamai.net.f4f.ZStream;
	import com.akamai.osmf.utils.AkamaiStrings;
	
	import flash.media.StageVideoAvailability;
	import flash.events.StageVideoAvailabilityEvent
	import flash.media.StageVideo;
	import flash.events.StageVideoEvent;
	import flash.events.VideoEvent;
	import flash.media.VideoStatus;
	import flash.media.Video;
	import com.akamai.stagevideo.StageVideoHelper;
	import com.akamai.stagevideo.StageVideoHelperEvent;
	
	
	
	//import com.akamai.player.HDVideoRenderer;
	
	//import com.akamai.hdcore.samples.hdn2.NetSessionSample;
	
	//import com.akamai.logger.AkamaiConsoleLogger;
	//import com.akamai.logger.AkamaiConsoleLoggerFactory;
	
	
	//import com.akamai.netSession
	
	
	
	

	
	
	import flash.utils.Timer


	public class VideoPlayer extends Sprite
	{
	
		private var cueCard:AS3CueCard;
	
		private var controls_mc:MovieClip;
		
		private var progressTimer:Timer;
		private var overridesSet:Boolean = false;
	

		private var blankScreen_mc:Sprite;
		private var isPlaying:Boolean = true;
		public var videoHasEnded:Boolean = false;
	// Outlet Variables
		//private var loader:Loader;
		private var _playerComponent; // The outlet framework
		private var embeddedPlayer; // The outlet embedded Player
		//private var videoPlayer; // the video Player itselfe  (OLD OUTLET CODE)
		private var contentMetadata; //contentMetadataDAOID
		private var vidX:Number = -177;
		private var vidY:Number = 71;
		private var controlsSize_obj:Object;

		public var fullScreen:Boolean = false;
		public var initilized:Boolean = false;
		private var fadeOutTimer:Timer;
		private var fadeOutTween:Tween;
		private var fullScreenFadeTimer:Timer;
		private var controlsVisibleInFullScreen:Boolean = true;
	
		private var scrubbing:Boolean = false;
		public var snipe_mc:DynamicSprite;
		private var snipeHitBox_mc:Sprite;
		private var seekDelayTimer:Timer;
		
		public var videoMode:String;
		private var omnitureExtension:Object;
		public var captions:Captions;
		public var popUpType:String = "";
		public var autoPlay:Boolean = true;
		
		private var mezzThumbnail:MezzThumbnail;
		
		private var canDisplayVideoEndPopUp:Boolean = false;
		
		// -----OSMF Variables--------
		
		private var mediaPlayerSprite:MediaPlayerSprite;
		private var mediaFactory:DefaultMediaFactory;
		private var akamaiLoader:URLLoader ;
		private var videoPlayerOSMF:MediaPlayer;
		private var mediaElement:MediaElement;
		private var loadingNewClip:Boolean = false;
		private var akamaiMediaResource:URLResource;
		
		// Omniture tracking variables
		private var lastRecordedTime:Number = 0;
		private var canRemoveLoadingMessage:Boolean = true;
		private var video_helper:StageVideoHelper;
		
		
		
		
		// Content
		private var HDN_MULTI_BITRATE_VOD:String;
		// Plugins
		private static const AKAMAI_PLUGIN:String =	"AkamaiAdvancedStreamingPlugin2.7_osmf1.6.swf"; // "AkamaiAdvancedStreamingPlugin2.7.6_osmf1.5.swf"; //  "AkamaiAdvancedStreamingPlugin.swf"; 
		
		
		public function VideoPlayer(_cueCard:AS3CueCard) {
			//var slfkjsd:NetSessionInterface = new NetSessionInterface();
			
			cueCard = _cueCard;
			
			controls_mc = cueCard.front_mc.always_mc.controls_mc;
			controlsSize_obj = new Object();
			
			
			controls_mc.scrubBar.visible = false;
			
		
		
			
			
			fadeOutTimer = new Timer(1200, 1);
			fullScreenFadeTimer = new Timer(2000, 1);
			fullScreenFadeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent) {
				Mouse.hide();
				fadeOut(e);
				controlsVisibleInFullScreen = false;
			});
			fadeOutTimer.addEventListener(TimerEvent.TIMER_COMPLETE , fadeOut);
			
			setUpOSMF();
		}
		
		
	// -----------  OSMF AKAMAI VIDEO LOADER ---------------------
	
	private function setUpOSMF() {
		
		
			
			mediaPlayerSprite = new MediaPlayerSprite();
			cueCard.front_mc.content_mc.vid.addChild(mediaPlayerSprite);
			mediaPlayerSprite.x = 0; 
			mediaPlayerSprite.y = 0; 
			
			
			mediaPlayerSprite.scaleMode = ScaleMode.LETTERBOX;
		
			mediaPlayerSprite.width =  cueCard.front_mc.content_mc.vid.width;
			mediaPlayerSprite.height = cueCard.front_mc.content_mc.vid.height;
			
		
			mediaPlayerSprite.mediaPlayer.addEventListener(	MediaErrorEvent.MEDIA_ERROR, onMediaError);
			mediaPlayerSprite.mediaPlayer.addEventListener(TimeEvent.COMPLETE, _endPlaying);
			mediaPlayerSprite.mediaPlayer.addEventListener(TimeEvent.CURRENT_TIME_CHANGE, timeUpdate);
			mediaPlayerSprite.mediaPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, State_Change_OSMF) 
		
			videoPlayerOSMF = mediaPlayerSprite.mediaPlayer;
			
			
			
			//stage.addEventListener(Event.RESIZE, onStageResize);
			mediaFactory = new DefaultMediaFactory();
			
			
	
			//loadOmniturePlugin();
			loadPlugin(cueCard.folderDirectory + AKAMAI_PLUGIN);
			//loadOmniturePlugin();
			
			
			//getAkamaiToken("http://preview-archives.nbclearn.com/portal/testjsp/smil.jsp?assetID=b739125d032c31681b609c13aa1a5c42");
		
	}
	
	
	public function loadPlugin(source:String):void{
		//	var akamaiPluginResource = :MediaResourceBase;
			//if (source.substr(0, 4) == "http" ||source.substr(0, 4) == "file"){
				// This is a URL, create a URLResource
				var resource:URLResource = new URLResource(source);
				
			//}else{
			// This is a class
				//var pluginInfoRef:Class = flash.utils.getDefinitionByName(source) as Class;
				//pluginResource = new PluginInfoResource(new pluginInfoRef);
			//}
		
			
				pluginSetupListeners();
				mediaFactory.loadPlugin(resource);
				
			
	}
			
		private function pluginSetupListeners(add:Boolean=true):void{
			
			if (add){
				mediaFactory.addEventListener(MediaFactoryEvent.PLUGIN_LOAD, onPluginLoad);
				
				mediaFactory.addEventListener(MediaFactoryEvent.PLUGIN_LOAD_ERROR, onPluginLoadError);
				
			}else{
				mediaFactory.removeEventListener(MediaFactoryEvent.PLUGIN_LOAD,	onPluginLoad);
				mediaFactory.removeEventListener(MediaFactoryEvent.PLUGIN_LOAD_ERROR,onPluginLoadError);
			}
		}
	
		
		
		function onPluginLoad(event:MediaFactoryEvent):void
		{
			initilized = true;
			trace("~Akamai plugin loaded successfully.");
			initControlEvents();
			pluginSetupListeners(false);
		
			ExternalInterface.call("CueCardManager.singleCardReady");
			captions = new Captions(cueCard.front_mc.content_mc.captions_mc, videoPlayerOSMF, controls_mc.captions_btn, cueCard.always_mc.mainDragBox);
			cueCard.stage.addEventListener(FullScreenEvent.FULL_SCREEN, _fullScreenEvent);  // full screen event tracking
			
			
			
			Log.loggerFactory = new AkamaiConsoleLoggerFactory(); // Dissable Logging and trace statments from the akamai plugin
			//AkamaiConsoleLogger.stopConsoleListening();  // additional methods.
			
			
			
			
			
			
			
			//ZStream.NETSESSION_USAGE_NEVER
//			netse
//			netSessionMode = ZStream.NETSESSION_USAGE_NEVER
			
			
		//	var my_log:AkamaiConsoleLoggerFactory = Log.loggerFactory.getLogger(
			//AkamaiConsoleLogger.stopConsoleListening();
		
		
			//Log.loggerFactory = new LoggerFactory();
			
			//trace("!loggerFactory: " + Log.loggerFactory);
		//	var myLog:LoggerFactory = Log.loggerFactory;
		
		
		//
		
			var black_bg:MovieClip = cueCard.front_mc.content_mc.blackBackround;
			
			video_helper = new StageVideoHelper(cueCard.stage, new Rectangle(black_bg.x, black_bg.y, black_bg.width, black_bg.height));
			//video_helper.addEventListener(StageVideoHelperEvent.status, new function(event:StageVideoHelperEvent) {
			//	checkGPU();
			//}
			
			cueCard.stage.addEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY , checkGPU);
			
			
		}
		
		private function checkGPU(event:StageVideoAvailabilityEvent = null) {
			trace("~GPU CHECK: " + event.availability);
			if (event.availability == "available") {
				//cueCard.shadowBackground.visible = false;
				//cueCard.front_mc.background_mc.mainBackround.visible = false;
				//cueCard.front_mc.content_mc.vid.visible = false;
				//cueCard.front_mc.content_mc.blackBackround.visible = false;
				//cueCard.front_mc.panels_mask.visible = false;
				
				
			}
			
			//trace("~VideoStatus: " + VideoStatus.SOFTWARE);
			
			
		}
		
		function onPluginLoadError(event:MediaFactoryEvent):void
		{
			trace("!plugin failed to load.");
			//trace("~MediaFactoryEvent ERROR: " + event.currentTarget.toString());
		//	trace("~event.type: " + event.type.toString());
			pluginSetupListeners(false);
			
		}
		
		
		
		
		
		
	
	private function State_Change_OSMF(event:MediaPlayerStateChangeEvent):void{ 
		//trace("!~videoPlayerOSMF.state: " + videoPlayerOSMF.state); 
		
		//videoPlayerOSMF.currentTime > 0.1
		if (videoPlayerOSMF.state ==  MediaPlayerState.PLAYING  && loadingNewClip == true) {
			loadingNewClip = false;
			_startPlaying()
		}
		
		updatePlayState();
	} 
	
	
	
	
		//sprite.resource = new URLResource("http://qa-www.nbclearn.com/offline/64_60DB99E3_22E6D_C11A3_DD000_3BA864B05_0500.flv");
		
		private function onMediaError(e:MediaErrorEvent) {
			trace("~MEDIA ERROR: " + e.toString);
			cueCard.confirmation.confirm("The video could not be loaded, please try again.", "OK", new Function(), true);
		}
		private function onStageResize(e:Event) {
			//trace("Stage Resize: " + e.toString);
			//mediaPlayerSprite.width = stage.stageWidth;
			//mediaPlayerSprite.height = stage.stageHeight;
		}
		
		
	
		// ------ END OSMF SETUP -------- <<<
		
		private function fadeOut(e:TimerEvent) {
		
			if(fadeOutTween){
				fadeOutTween.stop();
			}
			fadeOutTween = cueCard.animateMovieClip(controls_mc, "alpha",0, 2);
		}
		
		public function fadeOutVideoControls(object:Object = null):void {

				fadeOutTimer.reset();
				fadeOutTimer.start();
		}
		
		public function fadeInVideoControls(object:Object = null):void {
			if (cueCard.flipped) {
				return;
			}
			if(fadeOutTween){
				fadeOutTween.stop();
			}
			fadeOutTimer.reset();
			fadeOutTween = cueCard.animateMovieClip(controls_mc, "alpha",1, .5);
		}
		
		
		private function contentError(event:Object) {
			
			trace("~ContentError Load Error");
			cueCard.loading_mc.visible = false;
			cueCard.signalNetworkTrafficStop("videoLoad");
		}
		private function geoPathError(event:Object) {
			//trace("~------------------------")
			trace("~GeoPathError");
			trace(event);
			cueCard.loading_mc.visible = false;
			cueCard.signalNetworkTrafficStop("videoLoad");
			//trace("~------------------------")
		}
		
		private function onPlayerStateChange(e:Object):void {
		/*
			//trace("e.data.state: " + e.data.state);
			try{
				if (e.data.state == "EmbeddedPlayer.STATE_READY" && overridesSet == false) {
					overridesSet = true;
					cueCard.omniture.overrideVideoPlayer(omnitureExtension, embeddedPlayer, videoPlayer);
				}
			}catch(e:Error) {
				trace("~ERROR: Could not set omniture overides");
				trace("~"+e);
			}	
			*/
		}	
		
		private function onOutletInitComplete(e:Event = null):void {
			/*
			embeddedPlayer = _playerComponent.getOutletExtension("appStarter"); 
			videoPlayer = _playerComponent.getOutletExtension("player"); 
			omnitureExtension = _playerComponent.getOutletExtension("metricManager");
			
			contentMetadata = _playerComponent.getOutletExtension("contentMetadataDAOID");
			contentMetadata.addEventListener("ContentError.GEO_PATH_ERROR", geoPathError);
			contentMetadata.addEventListener("ContentError.EVENT_TYPE", contentError);
			
			embeddedPlayer.addEventListener("StateUpdateEvent.UPDATE", onPlayerStateChange);
			//embeddedPlayer.showMessage("LOADING....");
			cueCard.loading_mc.visible = true;
			embeddedPlayer.changeControls(" ");
			
			playerEvents();
			initControlEvents();
			
			//setOmnitureOverides();
			
			
		
			
			initilized = true;
			trace("~onOutletInitComplete");
			cueCard.front_mc.content_mc.vid.mask = cueCard.front_mc.panels_mask;
			
			try {
				var screanReaderActive:Boolean = Accessibility.active;
				var cap:Boolean = Capabilities.hasAccessibility;
				trace("~Access Support: " + cap);
				trace("~Scr Reader Active: " + screanReaderActive);
				ExternalInterface.call("CueCardManager.singleCardReady",screanReaderActive);
			}catch (e:Error) {
				trace("~ ERROR No accessibility on system");
				ExternalInterface.call("CueCardManager.singleCardReady");
				captions = new Captions(cueCard.front_mc.content_mc.captions_mc, embeddedPlayer, controls_mc.captions_btn, cueCard.always_mc.mainDragBox);
				
			//}
			
			*/
		}
		
		public function configureVideoMode(videoState_xml:XMLList) {
			videoMode = videoState_xml.attribute("mode");
			//trace("~videoMode: " + videoMode);
			
			if (videoMode == "15_Second") {
				//trace("~Create Snipe");
				snipe_mc = new DynamicSprite();
				var snipe_bg:DynamicSprite = new DynamicSprite();
				snipe_bg.graphics.beginFill(0, .5);
				snipe_bg.graphics.lineTo(445, 0);
				snipe_bg.graphics.lineTo(445, 25);
				snipe_bg.graphics.lineTo(0, 25);
				snipe_bg.graphics.lineTo(0, 0);
				
				snipeHitBox_mc = new Sprite();
				snipeHitBox_mc.graphics.beginFill(0xFF0000, 0);
				snipeHitBox_mc.graphics.lineTo(45, 0);
				snipeHitBox_mc.graphics.lineTo(45, 15);
				snipeHitBox_mc.graphics.lineTo(0, 15);
				snipeHitBox_mc.graphics.lineTo(0, 0);
				snipeHitBox_mc.useHandCursor = true;
				snipeHitBox_mc.buttonMode = true;
				snipeHitBox_mc.y = 42
				
				snipeHitBox_mc.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
					cueCard.signIn.init("Sign In to see full videos",popUpType);
				});
				
				snipe_mc.bg = snipe_mc.addChild(snipe_bg);
				var snipe_txt:TextField = new TextField();
				
				var fmt:TextFormat = new TextFormat();
				fmt.color = 0xFFFFFF;
				
				snipe_txt.autoSize = TextFieldAutoSize.LEFT;
				snipe_txt.htmlText = "15 second preview, <u><b>sign in</b></u> for full videos.";
				snipe_txt.y = 5;
				snipe_txt.setTextFormat(fmt);
				snipe_mc.snipe_txt = snipe_mc.addChild(snipe_txt);
				snipe_mc.y = 0
				snipe_mc.x = 0;
				snipe_txt.x = snipe_bg.width/2 - snipe_txt.width/2
				cueCard.front_mc.content_mc.vid.addChild(snipe_mc);//cueCard.front_mc.content_mc
				snipeHitBox_mc.x = snipe_mc.snipe_txt.x - 88;
				cueCard.always_mc.snipeHitBox_mc = cueCard.always_mc.addChild(snipeHitBox_mc);
			}
		}
		
		// LOAD A NEW CLIP into the video Player
		
		public function playClip(videoID:String, seekTo:Number = 0):void {
			//var domain:String = String(cueCard.folderDirectory).split("nbclearn.com")[0] +"nbclearn.com"  //http://dev-archives.nbclearn.com
			var domain:String
			var domain_array:Array;
			 domain_array = String(cueCard.folderDirectory).split("nbclearn.com");
			if (domain_array.length > 1) {
				domain = domain_array[0] + "nbclearn.com"
			}else {
				domain_array = String(cueCard.stage.loaderInfo.parameters.videoUrl).split("/nbcarchive/nbcarchive_getVideo.jsp?assetID=");
				if (domain_array.length > 1) {
					domain = domain_array[0];
				}
			}
			trace("! Application DOMAIN = " + domain);
			//https://preview-nbclearn-com.ezproxy1.lib.asu.edu/files/higheredsa/site/componentsAOD2/singleCard.swf
			getAkamaiToken(domain + "/portal/testjsp/smil.jsp?assetID=" + videoID);  // locate the physical location of the file from akamai
			
			cueCard.loading_mc.visible = true;
			canRemoveLoadingMessage = true;
		}
		
		private function getAkamaiToken(token_url:String) {
			if(!akamaiLoader){
				akamaiLoader = new URLLoader();
				akamaiLoader.addEventListener(Event.COMPLETE, setflvURL);
			}
				
				akamaiLoader.load(new URLRequest(token_url));
		}
		private function setflvURL(event:Event) {
			
			var loader:URLLoader = URLLoader(event.target);
			
			var akamaiSmil:String = loader.data; // the loader returns the phyisical location of the smil file.
			HDN_MULTI_BITRATE_VOD = akamaiSmil; //
			akamaiMediaResource = new URLResource(HDN_MULTI_BITRATE_VOD)

			var akamaiMetaData:Metadata = new Metadata(); // Create a meta data Key
			//var akamaiMetaData:Metadata = akamaiMediaResource.getMetadataValue(AkamaiStrings.AKAMAI_ADVANCED_STREAMING_PLUGIN_METADATA_NAMESPACE) as Metadata;
			akamaiMetaData.addValue(AkamaiStrings.AKAMAI_METADATA_KEY_USE_NETSESSION, false); // Add false to the Net session key on the meta data
			try{
				akamaiMediaResource.addMetadataValue(AkamaiStrings.AKAMAI_ADVANCED_STREAMING_PLUGIN_METADATA_NAMESPACE, akamaiMetaData); // attache the meta data to the URL Stream Resource
			}catch (e:Error) {
				trace("~ Could not add MetaData Tag to akamaiMediaResource");
			}
		
			OSMFSettings.enableStageVideo = false;
			
			
			mediaElement = mediaFactory.createMediaElement(akamaiMediaResource);
			mediaPlayerSprite.media =  mediaElement; // set the media element as the new media to load,  it should automaticly trigger a play command.
			
			//trace("!AKAMAI_METADATA_KEY_USE_NETSESSION: " + AkamaiStrings.AKAMAI_METADATA_KEY_USE_NETSESSION);
		
		}
		
		
		public function loadClip(_clip) {
		//	trace("~_clip: " + _clip);
			if (initilized) {
				loadingNewClip = true;
				//resize(cueCard.front_mc.content_mc.blackBackround.width, cueCard.front_mc.content_mc.blackBackround.height);
				//trace("~LOAD CLIP: " + _clip);
				
				//loader.alpha = 0;
				cueCard.signalNetworkTraffic("videoLoad");
				
				controls_mc.progressBar_mc.elapsed_mc.scaleX = .01; // reset the bar to the left
				controls_mc.timeDisplay.currentTime.text = "00:00";
				controls_mc.timeDisplay.totalTime.text = "00:00";
				controls_mc.scrubBar.x = controls_mc.progressBar_mc.x; 
				
				
				if (snipe_mc) { // only run this if we are in a guest mode type of situation
					if (cueCard.overrideGuestMode == true) {
					
							snipe_mc.visible = false;
					
					}else {
						snipe_mc.visible = true;
					}
				}
				
				//embeddedPlayer.showMessage("LOADING....");
				if(autoPlay == true){ // Always be true for now.
					playClip(String(_clip));
					
				}else {
					
					//cueCard.always_mc.mainDragBox.visible = false;
					//loadMezzThumbnail(_clip, cueCard.metaData.HomeThoughtStarter289x160);
				}
				
				if(!cueCard.flipped){
					fadeInVideoControls();
				}

				captions.setCaptionInfo(cueCard.metaData.captionsInfo);
				captions.setAdditionalCaptions(cueCard.metaData.otherCaptions);
				controls_mc.scrubBar.visible = false;
				
				if (captions.hasCaptions) {
					controls_mc.captions_btn.visible = true;
					
				}else {
					controls_mc.captions_btn.visible = false;
				}
			}
		
		}
		
		private function loadMezzThumbnail(clipID, image_url:String) {
			if (!mezzThumbnail) {
				mezzThumbnail = new MezzThumbnail(cueCard.front_mc.panels_mask,cueCard.always_mc.mainDragBox);
				cueCard.front_mc.content_mc.vid.addChild(mezzThumbnail);
			}
			mezzThumbnail.loadImage(clipID,image_url, playClip);
		}
		
		private function playerEvents() {
			//trace("~playerEvents");
			//EMBEDDED PLAYER
			//embeddedPlayer.addEventListener("Player.start", _startPlaying);
			//embeddedPlayer.addEventListener("Player.end", _endPlaying);
			
			//cueCard.stage.addEventListener(FullScreenEvent.FULL_SCREEN, _fullScreenEvent);
			// VIDEO PLAYER
			//videoPlayer.addEventListener("Player.paused", _pause);	
			//videoPlayer.addEventListener("Player.playing", _play);	
			//videoPlayer.addEventListener("Player.volumeMute", _mute);
			//videoPlayer.addEventListener("Player.error", loadError);
			
		}
		private function loadError(e:Event) {
			trace("---LOAD ERROR---");
			cueCard.loading_mc.visible = false;
			cueCard.signalNetworkTrafficStop("videoLoad");
		}
		private function _startPlaying() {
			
			trace("!~Start Playing");
			
			if (cueCard.visible == false || cueCard.imageMode == true) {
				stop();
				//trace("prevent player from playing");
				return;
			}
			
			videoHasEnded = false;
			isPlaying = true;
			//loader.alpha = 1;
			cueCard.signalNetworkTrafficStop("videoLoad");
			//if(!cueCard.flipped && cueCard.extensionsManager.isExtensionOpenedInternally() != true){
				//cueCard.always_mc.mainDragBox.visible = true;
			//}
			cueCard.loading_mc.alpha = .7;
			if (!cueCard.flipped) {
				controls_mc.visible = true;
			}
			
			cueCard.omniture.trackVideoStart();
			lastRecordedTime = 0;
			
			controls_mc.scrubBar.visible = true;
			
			fadeOutVideoControls();
			
			canDisplayVideoEndPopUp = true;
			
			progressTimer.start();
			
			
		}
		private function selectBitRate(bitRate:int) {
			
			if (videoPlayerOSMF.isDynamicStream) {
				if (bitRate <= videoPlayerOSMF.maxAllowedDynamicStreamIndex) {
					try {
						videoPlayerOSMF.switchDynamicStreamIndex(bitRate);
						
					}catch (e:Error) {
						trace("!Could Not Switch to Stream: " + bitRate);
					}
					
				}
				
			}
		}
		
		private function _endPlaying(e:Event){
			
			controls_mc.scrubBar.visible = false;
			
			isPlaying = false;
			canRemoveLoadingMessage = true;
			videoHasEnded = true;
			loadingNewClip = true;
		//	cueCard.always_mc.mainDragBox.visible = false;
			updatePlayState();
			controls_mc.progressBar_mc.elapsed_mc.width = 0;
			if (fullScreen) {
				goFullScreen();
			}
			
			cueCard.omniture.trackVideoCompleate();
			cueCard.omniture.trackSeconds(videoPlayerOSMF.duration - lastRecordedTime);
			lastRecordedTime = 0;
			//cueCard.omniture.trackEvent("event29", "Video End");
		}
			
		private function _play(e:Event){
		//	trace("~ PLAYER PLAY");
			isPlaying = true;
			videoHasEnded = false;
			updatePlayState();
		}
		private function _pause(e:Event){
	
			//trace("~ PLAYER IS PAUSED");
			isPlaying = false;
			updatePlayState();
		}
		
		private function _mute(e:Event) {
			//trace("~_mute");
		}
			
		private function _fullScreenEvent(event:FullScreenEvent) {
			//trace("~fullScreen: " + event.fullScreen);
			if (cueCard.imageMode) { 
				return; // these functions are only for video
			}
			
			//trace("~event.fullScreen: " + event.fullScreen);
			if (event.fullScreen) {
				fullScreen = true; 
				
				var point1:Point = new Point(0, 0);
					cueCard.front_mc.content_mc.vid.x = cueCard.front_mc.content_mc.globalToLocal(point1).x;
					cueCard.front_mc.content_mc.vid.y = cueCard.front_mc.content_mc.globalToLocal(point1).y ;
			
				cueCard.front_mc.content_mc.vid.mask = null;
				cueCard.front_mc.panels_mask.visible = false;
				
				//resize(cueCard.stage.width, cueCard.stage.height);
				//cueCard.always_mc.visible = false;
				
				if (snipe_mc) {
					
					snipe_mc.visible = false;// Turn snipe off for full screen
				}
				
				// REMOVE FROM SCREEN doesn't appear in full screen
				cueCard.shadowBackground.visible = false;
				cueCard.front_mc.background_mc.visible = false;
				
				// Always shown items to remove:
				cueCard.always_mc.icons_right.visible = false;
				cueCard.always_mc.icons_left.visible = false;
				cueCard.always_mc.title_txt.visible = false;
				cueCard.always_mc.extensions_bar.visible = false;
				cueCard.always_mc.mainDragBox.visible = false;
				cueCard.always_mc.dragCorner.visible = false;
		
				
			// CAPTURE THE CURRENT LOCATIONS of the Video Controls
				
				controlsSize_obj.progressBar_mc = controls_mc.progressBar_mc.width;
				controlsSize_obj.mute_btn = controls_mc.mute_btn.x;
				controlsSize_obj.volume_mc = controls_mc.volume_mc.x;
				controlsSize_obj.fullScreen_btn = controls_mc.fullScreen_btn.x;
				controlsSize_obj.captions_btn = controls_mc.captions_btn.x;
				controlsSize_obj.captions_mc = controls_mc.captions_btn.y;
				controlsSize_obj.captions_mcX = captions.captions_mc.x;
				controlsSize_obj.captions_mcY = captions.captions_mc.y;
				controlsSize_obj.timeDisplay = controls_mc.timeDisplay.x;
				controlsSize_obj.base_mc = controls_mc.base_mc.width;
				controlsSize_obj.x = controls_mc.x;
				controlsSize_obj.y = controls_mc.y;
				
				//controls_mc.scaleX = .74;
				//controls_mc.scaleY = .74;
				
			// SET THE NEW LOCATION CENTERED ON THE SCREEN
				var mediaWidth:Number = cueCard.stage.stageWidth;
				var mediaHeight:Number = cueCard.stage.stageHeight;
				
				resize(mediaWidth, mediaHeight);// -26); // resize the player even more
				
			//	trace("~mediaWidth: " + mediaWidth);
			//	trace("~mediaHeight: " + mediaHeight);
			point1.y = mediaHeight - 70;
				controls_mc.y = cueCard.always_mc.globalToLocal(point1).y ; //cueCard.front_mc.content_mc.vid.y + cueCard.front_mc.content_mc.vid.height - 110;
				
				
				controls_mc.progressBar_mc.width = mediaWidth / 2;
				controls_mc.timeDisplay.x = controls_mc.progressBar_mc.x + controls_mc.progressBar_mc.width + 10;
				controls_mc.mute_btn.x = controls_mc.timeDisplay.x + controls_mc.timeDisplay.width;
				controls_mc.volume_mc.x = controls_mc.mute_btn.x + 20;
				controls_mc.fullScreen_btn.x = controls_mc.volume_mc.x + 36;
				controls_mc.captions_btn.x = controls_mc.fullScreen_btn.x + 28;
				controls_mc.base_mc.width = controls_mc.captions_btn.x + 15;
				
				controls_mc.x = cueCard.front_mc.content_mc.vid.x + (mediaWidth / 2) - (controls_mc.width / 2);
				cueCard.curtain_mc.alpha = 1;
				cueCard.curtain_mc.visible = false;
				// captions_mc
				captions.captions_mc.scaleX = 2;
				captions.captions_mc.scaleY = 2;
				captions.captions_mc.y = controls_mc.y - 10 - captions.captions_mc.height ;
				captions.captions_mc.x = (controls_mc.x + (controls_mc.base_mc.width / 2) - captions.captions_mc.width / 2);
				//OSMFSettings.enableStageVideo = true;
				// If Stage Video is running
				
				
				if (video_helper.videoCompositingStatus == "GPU"){
					cueCard.front_mc.content_mc.vid.visible = false;
					
				}
				cueCard.front_mc.content_mc.blackBackround.visible = false;
				
				updateProgressBar();
				controlsVisibleInFullScreen = true;
				cueCard.stage.addEventListener(MouseEvent.MOUSE_MOVE, fullScreenFadeIn);
				
				cueCard.omniture.trackEvent("event35", "Full Screen Video");
	// ---------------------- RETURN TO REGULAR SCREEN -----------------------
			}else {
				if (cueCard.fmanager.getFocus() == controls_mc.play_btn) {
					cueCard.fmanager.setFocus(controls_mc.fullScreen_btn);
				}
				//trace("~_endFullScreen");
				cueCard.front_mc.content_mc.vid.visible = true;
				cueCard.front_mc.content_mc.blackBackround.visible = true;
				cueCard.front_mc.content_mc.vid.mask = cueCard.front_mc.panels_mask;
				
				resize(cueCard.front_mc.content_mc.blackBackround.width, cueCard.front_mc.content_mc.blackBackround.height);
				cueCard.front_mc.content_mc.vid.x = vidX;
				cueCard.front_mc.content_mc.vid.y = vidY;
				cueCard.always_mc.visible = true;
				
				// reset controls
				controls_mc.progressBar_mc.width = controlsSize_obj.progressBar_mc;
				controls_mc.base_mc.width = controlsSize_obj.base_mc;
				controls_mc.mute_btn.x = controlsSize_obj.mute_btn;
				controls_mc.volume_mc.x = controlsSize_obj.volume_mc;
				controls_mc.fullScreen_btn.x = controlsSize_obj.fullScreen_btn;
				controls_mc.captions_btn.x = controlsSize_obj.captions_btn;
				
				captions.captions_mc.scaleX = 1;
				captions.captions_mc.scaleY = 1;
				captions.captions_mc.x = controlsSize_obj.captions_mcX;
				captions.captions_mc.y = controlsSize_obj.captions_mcY;
				
				controls_mc.timeDisplay.x = controlsSize_obj.timeDisplay;
				controls_mc.x =  controlsSize_obj.x;
				controls_mc.y = controlsSize_obj.y;
				//controls_mc.scaleX = 1;
				//controls_mc.scaleY = 1;
				cueCard.curtain_mc.alpha = .4;
				cueCard.curtain_mc.visible = true;
				if (snipe_mc && cueCard.overrideGuestMode != true) {
					snipe_mc.visible = true;
				}
				// Place back the cue card boarder
				//if(!video_helper.stageVideoUsed){
					cueCard.shadowBackground.visible = true;
					cueCard.front_mc.background_mc.visible = true;
				//}
				
			// Always shown items to add back:
				cueCard.always_mc.icons_right.visible = true;
				cueCard.always_mc.icons_left.visible = true;
				cueCard.always_mc.title_txt.visible = true;
				cueCard.always_mc.extensions_bar.visible = true;
				cueCard.always_mc.mainDragBox.visible = true;
				cueCard.always_mc.dragCorner.visible = true;
				
				
				cueCard.stage.removeEventListener(MouseEvent.MOUSE_MOVE, fullScreenFadeIn);
				fullScreenFadeTimer.stop();
				fullScreenFadeTimer.reset();
				updateProgressBar();
				Mouse.show();
				controlsVisibleInFullScreen = false;
				
				var fscreencloseTimer:Timer = new Timer(300, 1);
				fscreencloseTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent) {
					fullScreen = false; 
					//trace("~!vidX AFTER Delay: " + vidX);
					
				});
				fscreencloseTimer.start();
				//trace("~!vidX AFTER: " + vidX);
			}
		}
		
		private function fullScreenFadeIn(e:MouseEvent){
			if (!controlsVisibleInFullScreen) {
				controlsVisibleInFullScreen = true;
				Mouse.show();
				fadeInVideoControls();
			}
			fullScreenFadeTimer.stop();
			fullScreenFadeTimer.reset();
			fullScreenFadeTimer.start();
		}
	
		
		
	
		
		private function initControlEvents(event:Event = null) {
			//trace("~INIT VIDEO EVENTS 2.0");
		// Click Events
		
		// PLAY PAUSE
			controls_mc.play_btn.addEventListener(MouseEvent.CLICK, playHandler);
			/*controls_mc.play_btn.addEventListener(KeyboardEvent.KEY_UP, function(e:KeyboardEvent) {
				if (e.keyCode == 13 || e.keyCode == 32) {   // Enter Key and Space Bar
					
					playHandler();	
					
				}
			});*/
		// MUTE UNMUTE
			controls_mc.mute_btn.addEventListener(MouseEvent.CLICK, toggleMute);
				
				
		
			/*controls_mc.mute_btn.addEventListener(KeyboardEvent.KEY_UP, function(e:KeyboardEvent) {
				if (e.keyCode == 32) { // Enter Key and Space Bar
					embeddedPlayer.mute();
					muteUnMute(e);
				}
			}); */
			
			
			
			
			
			
			// FULL SCREEN
			controls_mc.fullScreen_btn.addEventListener(MouseEvent.CLICK, goFullScreen);
			controls_mc.fullScreen_btn.addEventListener(KeyboardEvent.KEY_UP, function(e:KeyboardEvent) {
				
				if (e.keyCode == 13 || e.keyCode == 32) {   // Enter Key
						e.stopImmediatePropagation()
						e.stopPropagation()
						goFullScreen(e);	
						cueCard.fmanager.setFocus(controls_mc.play_btn);
				}
			});
			
			
			
			
			
			
			
	// PROGRESS BAR 
			
			controls_mc.progressBar_mc.addEventListener(MouseEvent.CLICK, progressBarClickHandler);
			controls_mc.volume_mc.hotspot.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				var mousePossition:Number = controls_mc.volume_mc.hotspot.mouseX; 
				var nextPossition:Number = (Math.floor(mousePossition / 3) * 3) + 3;
			
				
				controls_mc.volume_mc.fullness_mc.fill_mc.width = nextPossition;
				nextPossition *= 3;
				if (nextPossition > 100) {
					nextPossition = 100;
				}
				
				setVolume(nextPossition);
				
			});
			
	// CAPTIONS
			controls_mc.captions_btn.cc_gfx.gotoAndStop("Inactive");
			controls_mc.captions_btn.hit_btn.addEventListener(MouseEvent.CLICK, function(event:MouseEvent) {
				
				captions.toggleCaptionsButton();
			
			});
	/*		controls_mc.captions_btn.hit_btn.addEventListener(KeyboardEvent.KEY_UP, function(event:KeyboardEvent) {
				if (event.keyCode == 13 || event.keyCode == 32) {
					captions.toggleCaptionsButton();
					
				}
			});
			
		*/
			// FADE IN OUT EVENTS
			controls_mc.addEventListener(MouseEvent.ROLL_OVER, fadeInVideoControls);
			controls_mc.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				if(!fullScreen){
					fadeOutVideoControls();
				}
			});
			
			// SET UP EVENTS TO SHOW AND HIDE THE Video Controls based on keyboard focus
			controls_mc.addEventListener(FocusEvent.FOCUS_IN, function(e:FocusEvent) {
				//cueCard.disableKeyboardControl = true;
				fadeInVideoControls(e);
				// Add code here to set a variable showing that the keyboard has focus.
			});
			controls_mc.addEventListener(FocusEvent.KEY_FOCUS_CHANGE, fadeInVideoControls);
			controls_mc.addEventListener(FocusEvent.FOCUS_OUT, function(e:FocusEvent) {
				//cueCard.disableKeyboardControl = false;
				fadeOutVideoControls(e);
			});
			
			// Roll Over Events
		// Controls Buttons Roll Over Events
			// Roll Over
			controls_mc.play_btn.addEventListener(MouseEvent.ROLL_OVER, hiLightControl);
			controls_mc.mute_btn.addEventListener(MouseEvent.ROLL_OVER,  hiLightControl);
			controls_mc.fullScreen_btn.addEventListener(MouseEvent.ROLL_OVER, hiLightControl);
			controls_mc.scrubBar.addEventListener(MouseEvent.ROLL_OVER, hiLightControl);
			controls_mc.captions_btn.hit_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				captions.showLangSelection();
				//cueCard.always_mc.mainDragBox.visible = false;
				hiLightControl(e, controls_mc.captions_btn.cc_gfx);
			});
			controls_mc.volume_mc.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				Colors.changeTint(controls_mc.volume_mc,0xff6600,.5)
			});
			// Roll Out
			controls_mc.mute_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			controls_mc.fullScreen_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			controls_mc.scrubBar.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			controls_mc.play_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			controls_mc.volume_mc.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			controls_mc.captions_btn.addEventListener(MouseEvent.ROLL_OUT,  function(e:MouseEvent) {
				captions.hideLangSelection();
				
				unHiLightControl(e, controls_mc.captions_btn.cc_gfx);
			});
					
			progressTimer = new Timer(500);
			progressTimer.addEventListener(TimerEvent.TIMER, updateProgressBar);
			
		// Button mode controls
			controls_mc.scrubBar.buttonMode = true;
			controls_mc.scrubBar.useHandCursor = true;
			controls_mc.play_btn.buttonMode = true;
			controls_mc.play_btn.useHandCursor = true;
			controls_mc.mute_btn.buttonMode = true;
			controls_mc.mute_btn.useHandCursor = true;
			controls_mc.fullScreen_btn.buttonMode = true;
			controls_mc.fullScreen_btn.useHandCursor = true;
			controls_mc.captions_btn.hit_btn.buttonMode = true;
			controls_mc.captions_btn.hit_btn.useHandCursor = true;
			controls_mc.volume_mc.hotspot.buttonMode = true;
			controls_mc.volume_mc.hotspot.useHandCursor = true;
			
			
		// Scrubbing Event
			
			controls_mc.scrubBar.addEventListener(MouseEvent.MOUSE_DOWN, startScrubbing);
			
			seekDelayTimer = new Timer(600, 3);
			seekDelayTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function() {
				try{
					updateProgressBar();
				}catch (e:Error) {
					trace("SEEK ERROR");
				}
			});
		
		
			cueCard.setTabOrder(controls_mc);
			//controls_mc.visible = false;
		}
		
		
// --------------- VOLUME CONTROLS ----------------

		public function setVolume(_volume:Number) {
			
				var volume:Number = videoPlayerOSMF.volume;
				if (volume == _volume) {
					return; // don't do anything because the requested volume is already the same
				}
			
				
				
				videoPlayerOSMF.volume = _volume;
				
				updateMuteButton();
		}
		public function changeVolume(_volume:int) {
			var volume:Number = videoPlayerOSMF.volume;
			var _setToVolume:Number = volume + (_volume / 100);
		
			
			if (_setToVolume > 1) {
				_setToVolume = 1
			}
			if (_setToVolume < 0) {
				_setToVolume = 0
			}
			
			
			setVolume(_setToVolume);
			_setToVolume =  (_setToVolume/3);
			controls_mc.volume_mc.fullness_mc.fill_mc.width = (_setToVolume*100); // move the volume indecator bar to the correct volume
			
		}
		private function toggleMute(e:Event) {
			
			
			if (videoPlayerOSMF.volume == 0) {
				
				
				videoPlayerOSMF.volume = 1;
				
				
			}else {
				
				videoPlayerOSMF.volume = 0;
				
				
			}
			updateMuteButton(e);
		}
		
		private function updateMuteButton(event:Event = null) {
			
			
			var accessProps:AccessibilityProperties = controls_mc.mute_btn.accessibilityProperties;
			
			var volume:Number = videoPlayerOSMF.volume;
			
			
			
			if (volume == 0) {
					controls_mc.mute_btn.gotoAndStop("mute");
				//videoPlayerOSMF.volume = 0;
				accessProps.name = "Hear Audio";
			}else {
			
				controls_mc.mute_btn.gotoAndStop("unmute");
				//videoPlayerOSMF.volume = 1;
				accessProps.name = "Mute Audio";
			}
			
			controls_mc.mute_btn.accessibilityProperties = accessProps;
			cueCard.updateAccessibilityProperties();
			
			//trace("~volume: " + volume);
		
		}
		
		private function hiLightControl(e:MouseEvent, alternate:Sprite = null) {
			//Colors.changeTint(e.currentTarget,0x4D40BF,.6)
			if(alternate == null){
				Colors.changeTint(e.currentTarget,0xff6600,1)
			}else {
				Colors.changeTint(alternate,0xff6600,1)
			}
		}
		private function unHiLightControl(e:MouseEvent, alternate:Sprite = null) {
			if(alternate == null){
				Colors.changeTint(e.currentTarget, 0xFFFFFF, .0)
			}else {
				Colors.changeTint(alternate,0xFFFFFF,1)
			}
			
		}
		
		private function startScrubbing(e:MouseEvent) {
			cueCard.stage.addEventListener(MouseEvent.MOUSE_UP, stopScrubbing);
			cueCard.stage.addEventListener(MouseEvent.MOUSE_MOVE, scrubBarIsMoving);
			scrubbing = true;
			var rect:Rectangle = new Rectangle(controls_mc.progressBar_mc.x, controls_mc.scrubBar.y, controls_mc.progressBar_mc.width, 0);
			controls_mc.scrubBar.startDrag(false, rect);
			
			
		}
		private function stopScrubbing(e:MouseEvent) {
			
			cueCard.stage.removeEventListener(MouseEvent.MOUSE_UP, stopScrubbing);
			cueCard.stage.removeEventListener(MouseEvent.MOUSE_MOVE, scrubBarIsMoving);
			controls_mc.scrubBar.stopDrag();
			var totalTime:Number = videoPlayerOSMF.duration;
			
			var percent:Number = controls_mc.progressBar_mc.elapsed_mc.width / 100
			seekVideoPlayer(totalTime * percent);	
			
			scrubbing = false;
		}
		
		
		private function scrubBarIsMoving(e:MouseEvent = null) {
			var distance:Number = (controls_mc.scrubBar.x - controls_mc.progressBar_mc.x);
			var percent:Number = distance / controls_mc.progressBar_mc.width;
			
			controls_mc.progressBar_mc.elapsed_mc.width = percent*100;
			var totalTime:Number = videoPlayerOSMF.duration;
			updateDisplayTime(totalTime*percent, totalTime);
			
		}
		
		public function goFullScreen(event:Event = null) {
			// GO FULL SCREEN
			
			
			try {
				if (!fullScreen) {
					
					//vidX = cueCard.front_mc.content_mc.vid.x;
					//vidY = cueCard.front_mc.content_mc.vid.y;
					//trace("~!vidX before: " + vidX);
					//trace("~!vidY before: " + vidY);
					
					cueCard.stage.displayState = StageDisplayState.FULL_SCREEN;
				}else {

					cueCard.stage.displayState = StageDisplayState.NORMAL;
				}
				
			}catch (e:Error) {
				trace("~THERE WAS AN ERROR WITH FULL SCREEN");
			}
		
			
			
		}
		
		
		public function togglePlayPause(event:Event = null) {
			try {
				playHandler(event);
			}catch (e:Error) {
				
			}
		}
		
		private function playHandler(event:Event = null):void {
			
			if (videoPlayerOSMF.playing) { 
				videoPlayerOSMF.pause();
				
			
			}else{
				videoPlayerOSMF.play();
				
			}
			
			
		}
		
		private function  updatePlayState(event = null) {
			
			//trace("Video Stage: " + videoPlayer.getState());
			var accessProps:AccessibilityProperties = controls_mc.play_btn.accessibilityProperties;
			if (videoPlayerOSMF.state == MediaPlayerState.PLAYING){//outletPlayer._myPlayer.getState() == "playing") {
				controls_mc.play_btn.gotoAndStop("pause");
				accessProps.name = "Pause video";
				progressTimer.start();
			}else{ // show the play button in all other instances  (in the future add in other states)
			
			
				controls_mc.play_btn.gotoAndStop("play");
				progressTimer.stop();
				accessProps.name = "Play video";
			}
			controls_mc.play_btn.accessibilityProperties = accessProps;
			cueCard.updateAccessibilityProperties();
		}
		private function stopHandler(event:MouseEvent):void {
			
			embeddedPlayer.pause();
			//trace("getState(): " + videoPlayer.getState());
		}
		
		
		
	// JUMP TO A PORTION OF THE VIDEO
		private function progressBarClickHandler(e:MouseEvent) {
			var prog_mc:MovieClip = e.currentTarget as MovieClip;
			var totalTime:Number = videoPlayerOSMF.duration;
			var seekTarget:Number = Number(prog_mc.mouseX/100 )*totalTime ;
			//trace("seekTarget: " + seekTarget);
			//trace("videoPlayer.getTotalTime(): " + videoPlayer.getTotalTime());
			seekVideoPlayer(seekTarget);
			
			seekDelayTimer.stop();
			seekDelayTimer.reset();
			if (isPlaying == false) {
				controls_mc.scrubBar.x = controls_mc.mouseX;
				scrubBarIsMoving();
				seekDelayTimer.start();
			}else {
				updateProgressBar();
			}
			
		}
		
		private function updateDisplayTime(currentTime:Number,totalTime:Number) {
			// Set Time
			var minuets:Number;
			var seconds:Number;
			// Current Time
			minuets = Math.floor(currentTime/60);
			seconds = Math.round(currentTime%60);
			if(seconds == 60){
				seconds = 0;
				minuets += 1;
			}
			controls_mc.timeDisplay.currentTime.text = checkIfValid(minuets) + ":"+checkIfValid(seconds);
			// Total Time
			minuets =  Math.floor(totalTime/60);
			seconds =  Math.round(totalTime%60);
			if(seconds == 60){
				seconds = 0;
				minuets += 1;
			}
			controls_mc.timeDisplay.totalTime.text = checkIfValid(minuets) + ":" + checkIfValid(seconds);
			
			
			 //-- Trace out Info
			 /*
			cueCard.trace_txt.htmlText = "";
			if (videoPlayerOSMF.isDynamicStream) {
				cueCard.trace_txt.htmlText += "<b>DynamicStream Active:</b> " + videoPlayerOSMF.isDynamicStream +"\n";
				cueCard.trace_txt.htmlText += "<b>Stream #:</b> " + (videoPlayerOSMF.currentDynamicStreamIndex + 1) + " of " + (videoPlayerOSMF.maxAllowedDynamicStreamIndex + 1) + "\n";
				cueCard.trace_txt.htmlText += "<b>Stream Res:</b> " + videoPlayerOSMF.mediaWidth + "/" + videoPlayerOSMF.mediaHeight + "px";
				cueCard.trace_txt.htmlText += "<b>BIT RATE: </b>" + videoPlayerOSMF.getBitrateForDynamicStreamIndex(videoPlayerOSMF.currentDynamicStreamIndex) + "kb\n";
				cueCard.trace_txt.htmlText += "<b>Video Status: </b>" + video_helper.status+ "\n";
				cueCard.trace_txt.htmlText += "<b>video Compositing Status: </b>" + video_helper.videoCompositingStatus+ "\n";
				cueCard.trace_txt.htmlText += "<b>Video Decoding Status: </b>" + video_helper.videoDecodingStatus+ "\n";
				
			}else {
				cueCard.trace_txt.htmlText += "DynamicStream Active: " + videoPlayerOSMF.isDynamicStream;
				cueCard.trace_txt.htmlText += "<b>Stream Res:</b> " + videoPlayerOSMF.mediaWidth + "/" + videoPlayerOSMF.mediaHeight + "px";
				try{
				cueCard.trace_txt.htmlText += "<b>BIT RATE: </b>" + videoPlayerOSMF.getBitrateForDynamicStreamIndex(videoPlayerOSMF.currentDynamicStreamIndex) + "kb\n";
				}catch (e:Error) {
					
				}
			}
			
			*/
			
		
			
				
		}
		
		private function checkIfValid(testNumber):String{
			if(isNaN(testNumber) || testNumber == undefined || testNumber < 0){ // if the test number is not a number or is undefined
				return "00";
			}else{
				if(testNumber < 10){
					return "0"+testNumber;
				}else{
					return testNumber;
				}
			}
		}
	
		private function timeUpdate(event:TimeEvent) {
			
			if (event.time > (lastRecordedTime+30)) {
				lastRecordedTime = event.time;
				cueCard.omniture.trackSeconds(30);
			}
		}
		
	// MOVE THE PROGRESS BAR AT SET INTERVALS ACCORDING TO THE ELAPSED TIME
		public function updateProgressBar(event:TimerEvent = null) {
			
			//trace("!videoMode = " + videoMode);
			//trace("!cueCard.overrideGuestMode = " + cueCard.overrideGuestMode);
		
			if (!scrubbing && cueCard.isResizing == false) {
				var currentTime:Number = videoPlayerOSMF.currentTime;
				
				var totalTime:Number = videoPlayerOSMF.duration;
				var percent:Number = (currentTime / totalTime) * 100;
			
				controls_mc.progressBar_mc.elapsed_mc.width = percent;
				// Scrub Bar
				if(canRemoveLoadingMessage){  // Until  the clip has fully loaded
					if (percent > 0) { // if the time has moved
						canRemoveLoadingMessage = false; // Now it is fully playing
						cueCard.loading_mc.visible = false; // remove the loading screen
						cueCard.loading_mc.alpha = 1;
						//trace("!----------------------------Remove Loading Screen--------------------------------------------------------");
					}else {
						controls_mc.scrubBar.x = controls_mc.progressBar_mc.x; // otherwise if we are still loading keep the progress bar at 0
					}
					
					
				}else {
					controls_mc.scrubBar.x = Math.round(controls_mc.progressBar_mc.x + controls_mc.progressBar_mc.width * (percent / 100));
				}
				
				updateDisplayTime(currentTime, totalTime);
				if (videoMode == "15_Second" && cueCard.overrideGuestMode != true ) {
					if (currentTime > 15) {
						//trace("~TIME IS OVER 15");
						stop();
						if(canDisplayVideoEndPopUp){
							canDisplayVideoEndPopUp = false;
							if (fullScreen) {
								goFullScreen();
							}
							cueCard.signIn.init("For full videos please sign in or register.", popUpType);
						}
					}
				}
			}
		}
		
		
		
	// DISPLAY THE TIME WHEN MOUSING OVER THE PRGRESS BAR
		private function setUpTimeDisplay() {
			controls_mc.progressBar_mc.addEventListener(MouseEvent.ROLL_OVER, function() {
				
				controls_mc.progressBar_mc.addEventListener(MouseEvent.MOUSE_MOVE, updateTimeDisplay);
				controls_mc.timeBar_mc.visible = true;
				//Resizer.maxFramerate();
			});
			controls_mc.progressBar_mc.addEventListener(MouseEvent.ROLL_OUT, function() {
				controls_mc.timeBar_mc.visible = false;
				controls_mc.progressBar_mc.removeEventListener(MouseEvent.MOUSE_MOVE, updateTimeDisplay);
				//Resizer.idleFramerate();
				
			});
			controls_mc.timeBar_mc.visible = false;
			
		}
		private function updateTimeDisplay(e:MouseEvent) {
			var totalTime:Number = videoPlayerOSMF.duration;
		// Set Possition
			controls_mc.timeBar_mc.x = controls_mc.mouseX;
			
			// Set Time
			var percent:Number = controls_mc.progressBar_mc.mouseX;
			var currentTime:Number = (percent * totalTime) / 100;
			var time_str:String;
			
			var minuets:Number = 0;
			var seconds:Number;
			var seconds_str:String;
			// Current Time
			minuets = Math.floor(currentTime/60);
			seconds = Math.round(currentTime%60);
			if(seconds == 60){
				seconds = 0;
				minuets += 1;
			}
			if (seconds < 10) {
				seconds_str = "0"+seconds
			}else {
				seconds_str = seconds.toString();
			}
			controls_mc.timeBar_mc.time_txt.text = minuets + ":" + seconds_str;
			
			
		}
		
		
		
		public function stop() {
			if(videoPlayerOSMF.state == MediaPlayerState.PLAYING || videoPlayerOSMF.state == MediaPlayerState.BUFFERING){
				videoPlayerOSMF.stop();
			}
			captions.stopTracking();
			//embeddedPlayer.showMessage("LOADING....");
		}
		public function resize(width:Number, height:Number):void {
			
           mediaPlayerSprite.mediaContainer.width  = width;
           mediaPlayerSprite.mediaContainer.height = height;
		
            var layout:LayoutMetadata = new LayoutMetadata();
            layout.width  = width;
            layout.height = height;
            layout.layoutMode = LayoutMode.NONE;
            layout.horizontalAlign = HorizontalAlign.CENTER;
            layout.verticalAlign = VerticalAlign.MIDDLE;
            layout.scaleMode = ScaleMode.LETTERBOX;
		
            mediaElement.removeMetadata(LayoutMetadata.LAYOUT_NAMESPACE);
            mediaElement.addMetadata(LayoutMetadata.LAYOUT_NAMESPACE, layout);
	
			//if (!cueCard.isUserLoggedIn() && cueCard.whiteListedIP != true && videoMode == "15_Second") {
				
			if (videoMode == "15_Second") {
				snipe_mc.bg.width = width;
				snipe_mc.snipe_txt.x = (width / 2) - (snipe_mc.snipe_txt.width / 2);
				if(snipeHitBox_mc){
					snipeHitBox_mc.x = snipe_mc.snipe_txt.x - 88;
				}
			}
			
		}
		private function seekVideoPlayer(_target:Number) {
			cueCard.omniture.trackSeconds(videoPlayerOSMF.currentTime - lastRecordedTime);
			lastRecordedTime = _target;
			videoPlayerOSMF.seek(_target);
		}
		
		public function seekToPercent(_percent:Number) {
			var totalTime:Number = videoPlayerOSMF.duration;
			var percent:Number = controls_mc.progressBar_mc.elapsed_mc.width / 100
				
			seekVideoPlayer(totalTime / (100 / _percent) );
			seekDelayTimer.start();
			
		}
		
		
		
		
	}
}