﻿package {
    import flash.events.*;
    import flash.display.*;
    import flash.net.*;
    import flash.text.*;
    import flash.filters.*;
    import flash.external.*;

    public class SignIn extends MovieClip {

        public var blackboard_image:Blackboardsplash;
        public var signInMain_mc:MovieClip;
        public var registerationCode_mc:MovieClip;
        public var backToVideo:MovieClip;
        public var peacock_mc:MovieClip;
        private var cueCard:AS3CueCard;
        private var bg:RECTANGLE;
        private var originalWidth:Number;
        private var originalHeight:Number;
        private var bevel:BevelFilter;
        private var blackboardMode:Boolean = false;
        private var blackboard_mc:MovieClip;
        private var learn_mc:MovieClip;
        private var learnFirstTime:Boolean = true;
        private var highered_Loader:Loader;
        public var freeTrialURL:String;

        public function SignIn(_arg1:AS3CueCard){
            this.cueCard = _arg1;
            this.freeTrialURL = this.cueCard.loaderInfo.parameters.freeTrailURL;
            this.blackboard_mc = this.blackboard_image;
            this.signInMain_mc.email_txt.restrict = "^%$#!~&*()-+|<>[]{}?\"//^";
            this.signInMain_mc.pass_txt.restrict = "^%$#!~&*()-+|<>[]{}\"?//^";
            this.registerationCode_mc.regCode_txt.restrict = "^%$\"()?//^";
            this.signInMain_mc.pass_txt.displayAsPassword = true;
            this.blackboard_mc.visible = false;
            this.visible = false;
            this.originalWidth = this.cueCard.front_mc.content_mc.blackBackround.width;
            this.originalHeight = this.cueCard.front_mc.content_mc.blackBackround.height;
            this.bevel = new BevelFilter(-0.6, 45, 0xFFFFFF, 0.52, 0, 0.74, 2, 2, 1, 3);
            this.bg = new RECTANGLE(this.originalWidth, this.originalHeight, 0xFFFFFF, 0xE5E5E5, 45);
            this.addChildAt(this.bg, 0);
            var _local2:int = (this.signInMain_mc.numChildren - 1);
            this.signInMain_mc.setChildIndex(this.signInMain_mc.header_txt, _local2);
            this.eventSetUp();
        }
        public function init(_arg1:String=null, _arg2:String=null){
            if (this.cueCard.videoPlayer.snipe_mc){
                this.cueCard.videoPlayer.snipe_mc.visible = false;
            };
            if (_arg1){
                this.signInMain_mc.header_txt.text = _arg1;
            };
            if (_arg2 == "2-button popup"){
                this.signInMain_mc.genericReg_mc.visible = true;
                this.signInMain_mc.subscribe_mc.visible = false;
                this.cueCard.disableKeyboardControl = true;
                this.enterLogin();
            } else {
                if (_arg2 == "blackboard"){
                    this.blackboardMode = true;
                    this.signInMain_mc.visible = false;
                    this.registerationCode_mc.visible = false;
                    this.blackboard_mc.visible = true;
					this.blackboard_mc.contact_txt.text = "To see full videos, please contact your school's Learning Management System administrator to request a subscription to NBC Learn";
                } else {
                    if (_arg2 == "learn"){
                        this.signInMain_mc.visible = false;
                        this.registerationCode_mc.visible = false;
                        this.blackboard_mc.visible = false;
                        if (this.learnFirstTime){
                            this.setUpLearnState();
                            this.arrangeContent();
                        } else {
                            this.arrangeContent();
                        };
                    } else {
                        this.signInMain_mc.genericReg_mc.visible = false;
                        this.signInMain_mc.subscribe_mc.visible = true;
                        this.cueCard.disableKeyboardControl = true;
                        this.enterLogin();
                    };
                };
            };
            this.cueCard.addItemToTabOrder(this.signInMain_mc.header_txt, "");
            this.cueCard.addItemToTabOrder(this.signInMain_mc.email_txt, "Enter Your Email Address");
            this.cueCard.addItemToTabOrder(this.signInMain_mc.pass_txt, "Enter Your Password");
            this.cueCard.addItemToTabOrder(this.signInMain_mc.login_btn.hit_btn, "Login In");
            this.cueCard.addItemToTabOrder(this.signInMain_mc.subscribe_mc.sub_title_txt, "");
            this.cueCard.addItemToTabOrder(this.signInMain_mc.subscribe_mc.sub_title_txt, "");
            this.cueCard.addItemToTabOrder(this.signInMain_mc.subscribe_mc.sign_up_txt, "");
            this.cueCard.addItemToTabOrder(this.signInMain_mc.subscribe_mc.signUp_btn.hit_btn, "Sign Up");
            this.cueCard.addItemToTabOrder(this.signInMain_mc.subscribe_mc.schoolCode_txt, "");
            this.cueCard.addItemToTabOrder(this.signInMain_mc.subscribe_mc.register_btn.hit_btn, "Register");
            this.cueCard.addItemToTabOrder(this.registerationCode_mc.RegTitle_txt, "");
            this.cueCard.addItemToTabOrder(this.registerationCode_mc.regCode_txt, "Registration Code");
            this.cueCard.addItemToTabOrder(this.registerationCode_mc.login_btn, "Registration Code Login");
            this.cueCard.addItemToTabOrder(this.registerationCode_mc.regCodeDscr_txt, "");
            this.cueCard.addItemToTabOrder(this.backToVideo.close_btn, "Go back to the video");
            this.x = -179;
            this.y = 37;
            if (this.cueCard.imageMode == false){
                this.cueCard.videoControls_mc.visible = false;
            };
            this.visible = true;
            if (_arg2 != "learn"){
                this.arrangeContent();
            };
        }
        private function setUpLearnState(){
            this.learnFirstTime = false;
            this.learn_mc = new MovieClip();
            this.addChild(this.learn_mc);
            this.learn_mc.y = 10;
            var learnText:* = new TextField();
            this.learn_mc.title_txt = this.learn_mc.addChild(learnText);
            learnText.width = 360;
            learnText.height = 50;
            learnText.multiline = true;
            learnText.wordWrap = true;
            var txt_fmt:* = new TextFormat();
            txt_fmt.size = 15;
            learnText.htmlText = ((("You have selected a feature that is for subscribers only.<br/>To learn more choose a product or sign up for a <a href=\"" + this.freeTrialURL) + this.cueCard.metaData_mc.Asset_ID) + "\"><Font color=\"#0000FF\"><u>free trial</u></Font></a>.");
            learnText.setTextFormat(txt_fmt);
            var highered_mc:* = new Sprite();
            var archives_mc:* = new Sprite();
            var archives_loader:* = new Loader();
            this.learn_mc.archives_mc = this.learn_mc.addChild(archives_mc);
            archives_mc.addChild(archives_loader);
            archives_mc.x = learnText.x;
            archives_mc.y = (learnText.y + learnText.height);
            this.learn_mc.highered_mc = this.learn_mc.addChild(highered_mc);
            archives_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, this.loadHigheredButton);
            archives_loader.load(new URLRequest("http://icue.nbcunifiles.com/files/nbcarchives/LearnAssets/cueCardAssets/371x84_PopUpBanners_Final_K12.jpg"));
            highered_mc.buttonMode = true;
            highered_mc.useHandCursor = true;
            highered_mc.addEventListener(MouseEvent.CLICK, function (){
                navigateToURL(new URLRequest("http://highered.nbclearn.com"));
            });
            archives_mc.buttonMode = true;
            archives_mc.useHandCursor = true;
            archives_mc.addEventListener(MouseEvent.CLICK, function (){
                navigateToURL(new URLRequest("http://archives.nbclearn.com"));
            });
        }
        private function loadHigheredButton(_arg1:Object=null){
            var event = _arg1;
            this.highered_Loader = new Loader();
            this.learn_mc.highered_mc.addChild(this.highered_Loader);
            this.learn_mc.highered_mc.x = this.learn_mc.archives_mc.x;
            this.learn_mc.highered_mc.y = ((this.learn_mc.archives_mc.y + 85) + 10);
            this.highered_Loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function (_arg1:Event){
                arrangeContent();
            });
            this.highered_Loader.load(new URLRequest("http://icue.nbcunifiles.com/files/nbcarchives/LearnAssets/cueCardAssets/371x84_PopUpBanners_Final_HIGHED.jpg"));
            this.arrangeContent();
            this.learn_mc.y = 11;
        }
        public function arrangeContent(){
            if (this.visible == true){
                this.bg.width = this.cueCard.front_mc.content_mc.blackBackround.width;
                this.bg.height = this.cueCard.front_mc.content_mc.blackBackround.height;
                this.peacock_mc.x = (this.bg.width - this.peacock_mc.width);
                this.peacock_mc.y = (this.bg.height - this.peacock_mc.height);
                this.backToVideo.x = (this.bg.width - 65);
                if (this.signInMain_mc.visible == true){
                    this.signInMain_mc.x = ((this.bg.width / 2) - 185);
                };
                if (this.registerationCode_mc.visible == true){
                    this.registerationCode_mc.x = ((this.bg.width / 2) - 160);
                };
                if (this.blackboard_mc.visible == true){
                    this.blackboard_mc.x = ((this.bg.width / 2) - (this.blackboard_mc.width / 2));
                    this.blackboard_mc.y = ((this.bg.height / 2) - (this.blackboard_mc.height / 2));
                };
                if (this.learn_mc){
                    if (this.learn_mc.visible == true){
                        this.learn_mc.x = ((this.bg.width / 2) - (this.learn_mc.width / 2));
                        this.learn_mc.y = ((this.bg.height / 2) - (this.learn_mc.height / 2));
                    };
                };
            };
        }
        public function reset(){
            this.originalHeight;
            this.bg.width = this.originalWidth;
            this.bg.height = this.originalHeight;
            this.peacock_mc.x = (this.bg.width - this.peacock_mc.width);
            this.peacock_mc.y = (this.bg.height - this.peacock_mc.height);
            this.backToVideo.x = (this.bg.width - 65);
        }
        private function eventSetUp(){
            this.backToVideo.close_btn.addEventListener(MouseEvent.CLICK, this.closeThisPopUp);
            this.backToVideo.close_btn.addEventListener(MouseEvent.ROLL_OVER, function (_arg1:MouseEvent){
                Colors.changeTint(backToVideo.close_btn.bg, 13116234, 1);
                Colors.changeTint(backToVideo.close_btn.gfx, 0xFFFFFF, 1);
                backToVideo.close_btn.filters = [bevel];
            });
            this.backToVideo.close_btn.addEventListener(MouseEvent.ROLL_OUT, function (_arg1:MouseEvent){
                Colors.changeTint(backToVideo.close_btn.bg, 12583747, 1);
                Colors.changeTint(backToVideo.close_btn.gfx, 0xFFFFFF, 1);
                backToVideo.close_btn.filters = [];
            });
            Colors.changeTint(this.backToVideo.close_btn.bg, 12583747, 1);
            Colors.changeTint(this.backToVideo.close_btn.gfx, 0xFFFFFF, 1);
            this.backToVideo.close_btn.buttonMode = true;
            this.backToVideo.close_btn.useHandCursor = true;
            this.setUpButton("SIGN IN", this.signInMain_mc.login_btn, 11054776, 11054776, this.login);
            this.setUpButton("SIGN UP", this.signInMain_mc.subscribe_mc.signUp_btn, 11054776, 11054776, this.freeTrial);
            this.setUpButton("REGISTER", this.signInMain_mc.subscribe_mc.register_btn, 11054776, 11054776, this.enterRegCode);
            this.setUpButton("REGISTER", this.registerationCode_mc.login_btn, 11054776, 11054776, this.registerWithInitCode);
            this.setUpButton("REGISTER", this.signInMain_mc.genericReg_mc.signUp_btn, 11054776, 11054776, this.gotoRegisterPage);
            this.signInMain_mc.pass_txt.addEventListener(KeyboardEvent.KEY_UP, function (_arg1:KeyboardEvent){
                trace(("~event.keyCode: " + _arg1.keyCode));
                if (_arg1.keyCode == 13){
                    trace("~ try to login");
                    login();
                };
            });
            this.registerationCode_mc.regCode_txt.addEventListener(KeyboardEvent.KEY_UP, function (_arg1:KeyboardEvent){
                if (_arg1.keyCode == 13){
                    registerWithInitCode();
                };
            });
            this.blackboard_mc.hitBox_mc.useHandCursor = true;
            this.blackboard_mc.hitBox_mc.buttonMode = true;
            this.blackboard_mc.hitBox_mc.addEventListener(MouseEvent.CLICK, function (_arg1:MouseEvent){
                navigateToURL(new URLRequest("http://nbclearn.com/lmsmoreinfo"), "_blank");
            });
        }
        private function setUpButton(_arg1:String, _arg2:MovieClip, _arg3:Number, _arg4:Number, _arg5:Function=null){
            var _label:* = _arg1;
            var button:* = _arg2;
            var initialColor:* = _arg3;
            var overColor:* = _arg4;
            var clickFunction = _arg5;
            button.label_txt.text = _label;
            Colors.changeTint(button.bg, initialColor, 1);
            if (clickFunction != null){
                button.hit_btn.buttonMode = true;
                button.hit_btn.useHandCursor = true;
                button.hit_btn.addEventListener(MouseEvent.CLICK, clickFunction);
            };
            button.hit_btn.addEventListener(MouseEvent.ROLL_OVER, function (_arg1:MouseEvent){
                Colors.changeTint(_arg1.currentTarget.parent.bg, overColor, 1);
                button.filters = [bevel];
            });
            button.hit_btn.addEventListener(MouseEvent.ROLL_OUT, function (_arg1:MouseEvent){
                Colors.changeTint(_arg1.currentTarget.parent.bg, initialColor, 1);
                button.filters = [];
            });
        }
        private function enterRegCode(_arg1:MouseEvent=null){
            this.signInMain_mc.header_txt.text = "Register";
            this.signInMain_mc.visible = false;
            this.registerationCode_mc.visible = true;
            this.cueCard.stage.focus = this.registerationCode_mc.regCode_txt;
            this.cueCard.addItemToTabOrder(this.registerationCode_mc.RegTitle_txt, "");
            this.cueCard.addItemToTabOrder(this.registerationCode_mc.regCode_txt, "Registration Code");
            this.cueCard.addItemToTabOrder(this.registerationCode_mc.login_btn, "Registration Code Login");
            this.cueCard.addItemToTabOrder(this.registerationCode_mc.regCodeDscr_txt, "");
            this.cueCard.addItemToTabOrder(this.backToVideo.close_btn, "Go back to the video");
            this.cueCard.updateAccessibilityProperties();
            this.arrangeContent();
        }
        private function enterLogin(_arg1:MouseEvent=null){
            this.signInMain_mc.visible = true;
            this.registerationCode_mc.visible = false;
            this.cueCard.stage.focus = this.signInMain_mc.email_txt;
            this.arrangeContent();
        }
        private function login(_arg1:MouseEvent=null){
            if ((((this.signInMain_mc.email_txt.length < 3)) && ((this.signInMain_mc.pass_txt.length < 2)))){
                return;
            };
            ExternalInterface.call("doModalLogin", this.signInMain_mc.email_txt.text, this.signInMain_mc.pass_txt.text, undefined, this.cueCard.metaData_mc.vcmID);
            this.closeThisCueCard();
        }
        private function registerWithInitCode(_arg1:MouseEvent=null){
            ExternalInterface.call("submittoRegisterWithcode", this.registerationCode_mc.regCode_txt.text, this.cueCard.metaData_mc.vcmID);
            this.closeThisCueCard();
        }
        private function loginFaceBook(_arg1:MouseEvent=null){
            ExternalInterface.call("doFacebookLogin", this.cueCard.metaData_mc.vcmID);
            this.closeThisCueCard();
        }
        private function freeTrial(_arg1:Object=null){
            var request:* = null;
            var event = _arg1;
            try {
                request = new URLRequest((this.freeTrialURL + this.cueCard.metaData_mc.Asset_ID));
                navigateToURL(request, "_blank");
            } catch(e:Error) {
            };
        }
        private function gotoRegisterPage(_arg1:MouseEvent=null){
            if (this.cueCard.whiteListedIP == true){
                ExternalInterface.call("submittoRegister");
            };
            this.closeThisCueCard();
        }
        private function forgotPassword(_arg1:MouseEvent=null){
            var request:* = null;
            var e = _arg1;
            var baseURL:* = this.cueCard.loaderInfo.parameters.snasapiURL;
            var regURL:* = (baseURL.split("snas")[0] + "forgotpassword/");
            try {
                request = new URLRequest(regURL);
                navigateToURL(request, "_blank");
            } catch(e:Error) {
            };
        }
        private function closeThisCueCard(_arg1:MouseEvent=null){
            this.visible = false;
            this.cueCard.removeCard();
        }
        public function close(){
            if (this.visible){
                this.closeThisPopUp();
            };
        }
        private function closeThisPopUp(_arg1:MouseEvent=null){
            var e = _arg1;
            this.cueCard.disableKeyboardControl = false;
            try {
                if (this.cueCard.always_mc.snipeHitBox_mc){
                    this.cueCard.setMainFocus(this.backToVideo.close_btn, this.cueCard.always_mc.snipeHitBox_mc);
                    this.cueCard.videoPlayer.snipe_mc.visible = true;
                };
            } catch(e:Error) {
            };
            this.visible = false;
            if (this.cueCard.imageMode == false){
                this.cueCard.videoControls_mc.visible = true;
            };
        }

    }
}//package 
