﻿package {
    import flash.events.*;
    import flash.display.*;
    import fl.transitions.*;
    import flash.utils.*;
    import flash.text.*;
    import flash.filters.*;
    import fl.transitions.easing.*;

    public class Transcript extends MovieClip {

        private const TEXT_RIGHT:Number = -8;
        private const TEXT_TOP:Number = -91;
        private const BUTTON_RIGHT:Number = 320;

        public var activity_btn:GenericButton;
        public var content_mc:MovieClip;
        public var transcript_btn:GenericButton;
        public var lessons_btn:GenericButton;
        public var open:Boolean = false;
        private var resizer:Resizer;
        private var contentStartX:Number;
        private var buttonStartX:Number;
        private var openX:Number;
        private var targetX:Number;
        private var _cueCard:AS3CueCard;
        private var ds:DropShadowFilter;
        private var text_mc:CSSTextArea;
        private var transHeight:Number = 298;
        private var mask_mc:Sprite;
        private var transcriptContentAvailable:Boolean = false;
        private var activityContentAvailable:Boolean = false;
        private var lessonsContentAvailable:Boolean = false;
        private var hasMoved:Boolean = false;
        private var failSafeTimer:Timer;
        private var transcriptTweenCompleted:Boolean = false;
        private var transcriptTextFormat:TextFormat;
        private var activityTextFormat:TextFormat;
        private var lessonsTextFormat:TextFormat;
        private var transcriptText:String;
        private var activityText:String;
        private var lessonsText:String;
        private var activityCSS:StyleSheet;

        public function Transcript(){
            this.contentStartX = this.content_mc.x;
            this.buttonStartX = this.transcript_btn.x;
            this.openX = (this.transcript_btn.x + this.BUTTON_RIGHT);
            this.failSafeTimer = new Timer(700, 1);
            this.failSafeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, this.openCloseFailSafe);
            this.activityCSS = new StyleSheet();
            this.activityCSS.parseCSS(" p { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; } a { color:#0000FF; text-decoration:underline; }  a:hover { text-decoration:underline; color:#FF0000;}");
            this.text_mc = new CSSTextArea();
            this.text_mc.wordWrap = true;
            this.text_mc.tabEnabled = false;
            this.content_mc.addChild(this.text_mc);
            this.text_mc.y = this.TEXT_TOP;
            this.text_mc.setSize(200, this.transHeight);
            this.text_mc.x = (this.TEXT_RIGHT - this.text_mc.width);
            this.text_mc.editable = false;
            this.text_mc.textField.tabEnabled = false;
            this.text_mc.textField.wordWrap = true;
            var _local1:TextFormat = new TextFormat();
            _local1.font = "Arial";
            _local1.color = 0;
            this.text_mc.textField.styleSheet = this.activityCSS;
            this.text_mc.setStyle("textFormat", _local1);
            this.mask_mc = new Sprite();
            this.mask_mc.graphics.beginFill(0xFF0000, 1);
            this.mask_mc.graphics.lineTo(300, 0);
            this.mask_mc.graphics.lineTo(300, this.transHeight);
            this.mask_mc.graphics.lineTo(0, this.transHeight);
            this.mask_mc.graphics.lineTo(0, 0);
            this.mask_mc = (this.addChild(this.mask_mc) as Sprite);
            this.mask_mc.y = this.TEXT_TOP;
            this.text_mc.mask = this.mask_mc;
            this.eventSetUp();
        }
        private function eventSetUp(){
            this.transcriptTextFormat = this.content_mc.transcript_tab.label_txt.getTextFormat();
            this.activityTextFormat = this.content_mc.activity_tab.label_txt.getTextFormat();
            this.lessonsTextFormat = this.content_mc.activity_tab.label_txt.getTextFormat();
            this.content_mc.activity_tab.wedge.visible = false;
            this.transcript_btn.addEventListener(MouseEvent.MOUSE_DOWN, function (_arg1:MouseEvent){
                if (transcriptContentAvailable){
                    if (!open){
                        setText(transcriptText);
                        if (activityContentAvailable){
                            content_mc.activity_tab.visible = false;
                            activity_btn.visible = false;
                            _cueCard.shadowBackground.trans_tab.activity_img.visible = false;
                        };
                        if (lessonsContentAvailable){
                            content_mc.lessons_tab.visible = false;
                            lessons_btn.visible = false;
                            _cueCard.shadowBackground.trans_tab.lessons_img.visible = false;
                        };
                    };
                    startTranscriptDrag();
                };
            });
            this.transcript_btn.addEventListener(MouseEvent.CLICK, this.toggleOpenClose);
            this.transcript_btn.addEventListener(MouseEvent.ROLL_OVER, function (_arg1:MouseEvent){
                if (!open){
                    if (transcriptContentAvailable){
                        content_mc.setChildIndex(content_mc.transcript_tab, (content_mc.numChildren - 1));
                        content_mc.activity_tab.wedge.visible = false;
                        content_mc.transcript_tab.wedge.visible = true;
                        transcriptTextFormat.color = 0xFFFFFF;
                        content_mc.transcript_tab.label_txt.setTextFormat(transcriptTextFormat);
                    };
                };
            });
            this.transcript_btn.addEventListener(MouseEvent.ROLL_OUT, function (_arg1:MouseEvent){
                transcriptTextFormat.color = 0;
                content_mc.transcript_tab.label_txt.setTextFormat(transcriptTextFormat);
            });
            this.activity_btn.addEventListener(MouseEvent.MOUSE_DOWN, function (_arg1:MouseEvent){
                if (activityContentAvailable){
                    content_mc.transcript_tab.visible = false;
                    if (!open){
                        setText(activityText);
                        transcript_btn.visible = false;
                        _cueCard.shadowBackground.trans_tab.transcript_img.visible = false;
                        if (lessonsContentAvailable){
                            lessons_btn.visible = false;
                            _cueCard.shadowBackground.trans_tab.lessons_img.visible = false;
                            content_mc.lessons_tab.visible = false;
                        };
                    };
                    startTranscriptDrag();
                };
            });
            this.activity_btn.addEventListener(MouseEvent.CLICK, this.toggleOpenClose);
            this.activity_btn.addEventListener(MouseEvent.ROLL_OVER, function (_arg1:MouseEvent){
                if (!open){
                    if (lessonsContentAvailable){
                        content_mc.transcript_tab.wedge.visible = false;
                        content_mc.activity_tab.wedge.visible = true;
                        content_mc.lessons_tab.wedge.visible = false;
                        content_mc.setChildIndex(content_mc.activity_tab, (content_mc.numChildren - 1));
                        activityTextFormat.color = 0xFFFFFF;
                        content_mc.activity_tab.label_txt.setTextFormat(activityTextFormat);
                    };
                };
            });
            this.activity_btn.addEventListener(MouseEvent.ROLL_OUT, function (_arg1:MouseEvent){
                activityTextFormat.color = 0;
                content_mc.activity_tab.label_txt.setTextFormat(activityTextFormat);
            });
            this.transcript_btn.buttonMode = true;
            this.transcript_btn.useHandCursor = true;
            this.activity_btn.buttonMode = true;
            this.activity_btn.useHandCursor = true;
            this.lessons_btn.addEventListener(MouseEvent.MOUSE_DOWN, function (_arg1:MouseEvent){
                if (lessonsContentAvailable){
                    if (!open){
                        setText(lessonsText);
                        if (transcriptContentAvailable){
                            transcript_btn.visible = false;
                            content_mc.transcript_tab.visible = false;
                            _cueCard.shadowBackground.trans_tab.transcript_img.visible = false;
                        };
                        if (activityContentAvailable){
                            activity_btn.visible = false;
                            _cueCard.shadowBackground.trans_tab.activity_img.visible = false;
                            content_mc.activity_tab.visible = false;
                        };
                    };
                    startTranscriptDrag();
                };
            });
            this.lessons_btn.addEventListener(MouseEvent.CLICK, this.toggleOpenClose);
            this.lessons_btn.addEventListener(MouseEvent.ROLL_OVER, function (_arg1:MouseEvent){
                if (!open){
                    content_mc.transcript_tab.wedge.visible = false;
                    content_mc.activity_tab.wedge.visible = false;
                    content_mc.lessons_tab.wedge.visible = true;
                    content_mc.setChildIndex(content_mc.lessons_tab, (content_mc.numChildren - 1));
                    lessonsTextFormat.color = 0xFFFFFF;
                    content_mc.lessons_tab.label_txt.setTextFormat(lessonsTextFormat);
                };
            });
            this.lessons_btn.addEventListener(MouseEvent.ROLL_OUT, function (_arg1:MouseEvent){
                lessonsTextFormat.color = 0;
                content_mc.lessons_tab.label_txt.setTextFormat(lessonsTextFormat);
            });
            this.transcript_btn.buttonMode = true;
            this.transcript_btn.useHandCursor = true;
            this.activity_btn.buttonMode = true;
            this.activity_btn.useHandCursor = true;
            this.lessons_btn.buttonMode = true;
            this.lessons_btn.useHandCursor = true;
        }
        public function setTranscript(_arg1:String){
            this.transcriptText = _arg1;
            var _local2:int = _arg1.length;
            if (_local2 <= 45){
                this.disableTranscript();
                return;
            };
            if ((((((((_arg1 == "")) || ((_arg1 == " ")))) || ((_arg1 == null)))) || ((_arg1 == "null")))){
                this.disableTranscript();
            } else {
                this.enableTranscript();
            };
        }
        public function triggerTranscriptRefresh(){
            if (this.open){
                this.setText(this.transcriptText);
            };
        }
        public function setText(_arg1){
            var transcript_str:* = _arg1;
            try {
                transcript_str = this.findReplace("\n", "", transcript_str);
                transcript_str = this.findReplace("\r", "", transcript_str);
                transcript_str = this.findReplace("</p>", "</p><br/>", transcript_str);
                transcript_str = this.findReplace("&rsquo;", "'", transcript_str);
                transcript_str = this.findReplace("&hellip;", "...", transcript_str);
                transcript_str = this.findReplace("&ldquo;", "\"", transcript_str);
                transcript_str = this.findReplace("&rdquo;", "\"", transcript_str);
                this.text_mc.htmlText = transcript_str;
                return (true);
            } catch(e:Error) {
                _cueCard.confirmation.confirm("The text was not able to display", "OK", new Function(), true);
                trace(("~!ERROR: " + e.toString()));
                return (false);
            };
        }
        public function setActivity(_arg1){
            this.activityText = _arg1;
            if (this.isNotUndefined(_arg1)){
                this.enableActivity();
            } else {
                this.disableActivity();
            };
        }
        public function setLessons(_arg1){
            this.lessonsText = _arg1;
            if (this.isNotUndefined(_arg1)){
                this.enableLessons();
            } else {
                this.disableLessons();
            };
        }
        public function disableTranscript(){
            this.transcriptContentAvailable = false;
            if (this.open){
                this.closeTranscript(true);
            };
            this.content_mc.transcript_tab.visible = false;
            this.transcript_btn.visible = false;
            this._cueCard.shadowBackground.trans_tab.transcript_img.visible = false;
        }
        private function enableTranscript(){
            this.transcriptContentAvailable = true;
            this.content_mc.transcript_tab.visible = true;
            this.transcript_btn.visible = true;
            this._cueCard.shadowBackground.trans_tab.transcript_img.visible = true;
        }
        public function disableActivity(){
            this.activityContentAvailable = false;
            this.content_mc.activity_tab.visible = false;
            this.activity_btn.visible = false;
            this._cueCard.shadowBackground.trans_tab.activity_img.visible = false;
        }
        private function enableActivity(){
            this.activityContentAvailable = true;
            this.content_mc.activity_tab.visible = true;
            this.activity_btn.visible = true;
            this._cueCard.shadowBackground.trans_tab.activity_img.visible = true;
        }
        public function disableLessons(){
            trace("~disableLessons");
            this.lessonsContentAvailable = false;
            this.content_mc.lessons_tab.visible = false;
            this.lessons_btn.visible = false;
            this._cueCard.shadowBackground.trans_tab.lessons_img.visible = false;
        }
        private function enableLessons(){
            trace("~enableLessons");
            this.lessonsContentAvailable = true;
            this.content_mc.lessons_tab.visible = true;
            this.lessons_btn.visible = true;
            this._cueCard.shadowBackground.trans_tab.lessons_img.visible = true;
            if (this.activityContentAvailable){
                this.content_mc.lessons_tab.y = 82;
                this.lessons_btn.y = 110;
                this._cueCard.shadowBackground.trans_tab.lessons_img.y = 180;
            } else {
                this.content_mc.lessons_tab.y = -6;
                this.lessons_btn.y = 20;
                this._cueCard.shadowBackground.trans_tab.lessons_img.y = 95;
            };
        }
        public function set cueCard(_arg1){
            this._cueCard = _arg1;
            this.initResizer();
        }
        private function initResizer(){
            this.resizer = new Resizer(this.transcript_btn);
            this.resizer.addResizableObject(this.content_mc, Resizer.HORIZONTAL);
            this.resizer.addResizableObject(this._cueCard.shadowBackground.transcript_bg, Resizer.WIDTH);
            this.resizer.addResizableObject(this._cueCard.shadowBackground.trans_tab, Resizer.HORIZONTAL);
            this.resizer.constrainTo(this.buttonStartX, 700, this.transcript_btn.y, this.transcript_btn.y);
            this.resizer.addEventListener(Resizer.RESIZING, this.isResizing);
            this.resizer.addEventListener(Resizer.RESIZING, this._cueCard.updateStageDimensions);
            this.resizer.addEventListener(Resizer.RESET_COMPLETE, this._resetComplete);
        }
        private function _resetComplete(_arg1:Object=null){
            this.text_mc.visible = false;
        }
        private function startTranscriptDrag(_arg1:Event=null):void{
            this.hasMoved = false;
            this.resizer.startResize();
            stage.addEventListener(MouseEvent.MOUSE_UP, this.stopTranscriptDrag);
            this.open = true;
        }
        private function stopTranscriptDrag(_arg1:Event){
            this.resizer.stopResize();
            if (this.transcript_btn.x == this.buttonStartX){
                this.open = false;
                this.resetTabs();
            };
            stage.removeEventListener(MouseEvent.MOUSE_UP, stopDrag);
        }
        private function isResizing(_arg1:Object){
            if (((!((_arg1.changeX == 0))) || (!((_arg1.changeY == 0))))){
                this.hasMoved = true;
            };
            this.lessons_btn.x = (this.activity_btn.x = this.transcript_btn.x);
            this.mask_mc.width = (this.content_mc.x - this.mask_mc.x);
            this.arrangeContent();
        }
        public function arrangeContent():void{
            var _local1:Number = ((this.content_mc.x - this.mask_mc.x) - 8);
            if (_local1 < 10){
                this.text_mc.visible = false;
            } else {
                this.text_mc.visible = true;
            };
            this.text_mc.x = (this.TEXT_RIGHT - _local1);
            this.text_mc.setSize(_local1, this.transHeight);
            this.mask_mc.height = this.transHeight;
            this.text_mc.invalidate();
        }
        public function masterDimensionResize(_arg1:Number, _arg2:Number){
            this.transHeight = (this.transHeight + _arg2);
            this.arrangeContent();
        }
        private function toggleOpenClose(_arg1:MouseEvent){
            if (this.hasMoved){
                return;
            };
            if (this.open){
                this.closeTranscript(true);
            } else {
                if (_arg1.currentTarget == this.transcript_btn){
                    if (this.transcriptContentAvailable){
                        this.content_mc.activity_tab.visible = false;
                        this.activity_btn.visible = false;
                        this._cueCard.shadowBackground.trans_tab.activity_img.visible = false;
                        this.content_mc.lessons_tab.visible = false;
                        this.lessons_btn.visible = false;
                        this._cueCard.shadowBackground.trans_tab.lessons_img.visible = false;
                        this.setText(this.transcriptText);
                        this.openTranscript(true);
                    };
                };
                if (_arg1.currentTarget == this.activity_btn){
                    if (this.activityContentAvailable){
                        this.setText(this.activityText);
                        this.content_mc.transcript_tab.visible = false;
                        this.transcript_btn.visible = false;
                        this._cueCard.shadowBackground.trans_tab.transcript_img.visible = false;
                        this.content_mc.lessons_tab.visible = false;
                        this.lessons_btn.visible = false;
                        this._cueCard.shadowBackground.trans_tab.lessons_img.visible = false;
                        this.openTranscript(true);
                    };
                };
                if (_arg1.currentTarget == this.lessons_btn){
                    if (this.lessonsContentAvailable){
                        this.setText(this.lessonsText);
                        this.content_mc.transcript_tab.visible = false;
                        this.transcript_btn.visible = false;
                        this._cueCard.shadowBackground.trans_tab.transcript_img.visible = false;
                        this.content_mc.activity_tab.visible = false;
                        this.activity_btn.visible = false;
                        this._cueCard.shadowBackground.trans_tab.activity_img.visible = false;
                        this.openTranscript(true);
                    };
                };
            };
        }
        public function resetContent(){
            this.transHeight = 298;
            this.arrangeContent();
        }
        public function openTranscript(_arg1:Boolean=false){
            var _local2:Number;
            var _local3:Number;
            var _local4:Number;
            var _local5:Tween;
            if ((((this.transcriptContentAvailable == false)) && ((this.activityContentAvailable == false)))){
                return;
            };
            this.text_mc.visible = true;
            if (_arg1 == true){
                this.resizer.changed = true;
                this.transcript_btn.__x = this.transcript_btn.x;
                _local3 = (this.openX - this.transcript_btn.x);
                _local4 = this._cueCard.motionManager.getEmptyScreenSpace().width;
                if (_local3 < _local4){
                    this.targetX = this.openX;
                    _local2 = (this.BUTTON_RIGHT - 8);
                } else {
                    _local2 = (_local4 - 8);
                    this.targetX = (this.transcript_btn.x + _local4);
                };
                this._cueCard.motionManager.keepOnScreen((this.targetX - this.transcript_btn.x), 0, 0.64);
                _local5 = this.animateMovieClip(this.transcript_btn, "x", this.targetX, 0.64);
                _local5.addEventListener(TweenEvent.MOTION_FINISH, this.barHasOpend);
                _local5.addEventListener(TweenEvent.MOTION_CHANGE, this.whileBarIsTweening);
            };
            this.text_mc.setSize(_local2, this.text_mc.height);
            this.text_mc.x = (this.TEXT_RIGHT - this.text_mc.width);
            this.text_mc.visible = true;
            this.failSafeTimer.start();
            this.transcriptTweenCompleted = false;
            this._cueCard.omniture.trackEvent("event36", "Open Transcript");
        }
        private function barHasOpend(_arg1:TweenEvent=null){
            this.open = true;
            this.lessons_btn.x = (this.activity_btn.x = this.transcript_btn.x);
            this.transcriptTweenCompleted = true;
            this.failSafeTimer.stop();
            this.failSafeTimer.reset();
            this._cueCard.fmanager.setFocus(this.text_mc);
            this.text_mc.verticalScrollPosition = 0;
        }
        public function closeTranscript(_arg1:Boolean=false){
            var _local2:Tween;
            this.transcript_btn.__x = this.transcript_btn.x;
            if (_arg1 == false){
                if (((this.resizer) && (this.resizer.changed))){
                    this.resizer.resetSizeFade();
                    this.open = false;
                };
                _local2 = this.animateMovieClip(this.transcript_btn, "x", this.buttonStartX, 0.64);
            } else {
                _local2 = this.animateMovieClip(this.transcript_btn, "x", this.buttonStartX, 0.64);
                this.failSafeTimer.start();
            };
            _local2.addEventListener(TweenEvent.MOTION_FINISH, this.barHasClosed);
            _local2.addEventListener(TweenEvent.MOTION_CHANGE, this.whileBarIsTweening);
            this.transcriptTweenCompleted = false;
        }
        private function openCloseFailSafe(_arg1:TimerEvent){
            if (!this.transcriptTweenCompleted){
                if (this.open){
                    this.transcript_btn.x = this.buttonStartX;
                    this.whileBarIsTweening();
                    this.barHasClosed();
                } else {
                    this.transcript_btn.x = this.targetX;
                    this.whileBarIsTweening();
                    this.barHasOpend();
                };
            };
            this.failSafeTimer.stop();
            this.failSafeTimer.reset();
        }
        private function whileBarIsTweening(_arg1:TweenEvent=null){
            var _local2:Number = (this.transcript_btn.x - this.transcript_btn.__x);
            this.transcript_btn.__x = this.transcript_btn.x;
            this.content_mc.x = (this.content_mc.x + _local2);
            this._cueCard.shadowBackground.transcript_bg.width = (this._cueCard.shadowBackground.transcript_bg.width + _local2);
            this._cueCard.shadowBackground.trans_tab.x = (this._cueCard.shadowBackground.trans_tab.x + _local2);
            this._cueCard.updateStageDimensions({
                changeX:_local2,
                changeY:0
            });
            this.mask_mc.width = (this.content_mc.x - this.mask_mc.x);
        }
        private function resetTabs(){
            if (this.transcriptContentAvailable){
                this.content_mc.transcript_tab.visible = true;
                this.transcript_btn.visible = true;
                this._cueCard.shadowBackground.trans_tab.transcript_img.visible = true;
            };
            if (this.activityContentAvailable){
                this.content_mc.activity_tab.visible = true;
                this.activity_btn.visible = true;
                this._cueCard.shadowBackground.trans_tab.activity_img.visible = true;
            };
            if (this.lessonsContentAvailable){
                this.content_mc.lessons_tab.visible = true;
                this.lessons_btn.visible = true;
                this._cueCard.shadowBackground.trans_tab.lessons_img.visible = true;
            };
        }
        private function barHasClosed(_arg1:TweenEvent=null){
            this.content_mc.x = this.contentStartX;
            this.transcript_btn.x = this.buttonStartX;
            this.lessons_btn.x = (this.activity_btn.x = this.transcript_btn.x);
            this.open = false;
            this.text_mc.visible = false;
            this.resetTabs();
            this.transcriptTweenCompleted = true;
            this.failSafeTimer.stop();
            this.failSafeTimer.reset();
        }
        private function animateMovieClip(_arg1:Sprite, _arg2:String, _arg3:Number, _arg4:Number):Tween{
            var _local5:Tween;
            _local5 = new Tween(_arg1, _arg2, Regular.easeInOut, _arg1[_arg2], _arg3, _arg4, true);
            return (_local5);
        }
        private function isNotUndefined(_arg1){
            if ((((((((((((_arg1 == undefined)) || ((_arg1 == "undefined")))) || ((_arg1 == null)))) || ((_arg1 == "null")))) || ((String(_arg1) == "")))) || ((String(_arg1) == " ")))){
                return (false);
            };
            return (true);
        }
        private function findReplace(_arg1:String, _arg2:String, _arg3:String){
            if (_arg3.indexOf(_arg1) > -1){
                return (_arg3.split(_arg1).join(_arg2));
            };
            return (_arg3);
        }

    }
}//package 
