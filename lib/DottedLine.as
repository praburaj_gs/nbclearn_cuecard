﻿package {
    import flash.display.*;
    import flash.geom.*;

    public class DottedLine extends Shape {

        private var _w:Number;
        private var _h:Number;
        private var _color:uint;
        private var _dotAlpha:Number;
        private var _dotWidth:Number;
        private var _spacing:Number;

        public function DottedLine(_arg1:Number=100, _arg2:Number=1, _arg3:uint=0x777777, _arg4=1, _arg5:Number=1, _arg6:Number=1){
            this._w = _arg1;
            this._h = _arg2;
            this._color = _arg3;
            this.alpha = _arg4;
            this._dotWidth = _arg5;
            this._spacing = _arg6;
            this.drawDottedLine();
        }
        private function drawDottedLine():void{
            graphics.clear();
            var _local1:BitmapData = new BitmapData((this._dotWidth + this._spacing), (this._h + 1), true);
            var _local2:Rectangle = new Rectangle(0, 0, this._dotWidth, this._h);
            var _local3:uint = this.returnARGB(this._color, 0xFF);
            _local1.fillRect(_local2, _local3);
            var _local4:Rectangle = new Rectangle(this._dotWidth, 0, (this._dotWidth + this._spacing), this._h);
            _local1.fillRect(_local4, 0);
            graphics.beginBitmapFill(_local1, null, true);
            graphics.drawRect(0, 0, this._w, this._h);
            graphics.endFill();
        }
        private function returnARGB(_arg1:uint, _arg2:uint):uint{
            var _local3:uint;
            _local3 = (_local3 + (_arg2 << 24));
            _local3 = (_local3 + _arg1);
            return (_local3);
        }

    }
}//package 
