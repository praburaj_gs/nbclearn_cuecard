﻿package {
    import flash.events.*;
    import flash.display.*;
    import fl.transitions.*;
    import flash.utils.*;
    import flash.text.*;
    import fl.transitions.easing.*;

    public class ExtensionsBar extends MovieClip {

        public var extensionContent:MovieClip;
        private var _cueCard:AS3CueCard;
        private var resizer:Resizer;
        public var tween:Tween;
        public var open:Boolean = false;
        public var barIsFullyOpen:Boolean = false;
        public var openInternally:Boolean = false;
        private var contentStartY:Number;
        private var contentStartX:Number;
        private var thisStartX:Number;
        private var thisStartY:Number;
        private var autoCancelCardID:String;
        public var content_mc:MovieClip;
        private var openY:Number;
        private var targetY:Number;
        private var timer:Timer;
        private var hasMoved:Boolean = false;
        private var pane:Sprite;
        private var paneBottom:Number = -6;
        private var paneInitialWidth:Number;
        private var format:TextFormat;
        private var clear_btn:Sprite;
        private var clear_txt:TextField;
        private var cancelTimer:Timer;
        private var motionTween:Tween;
        private var nextExtensionLabel:String;
        private var currentExtension;
        private var nextExtension;
        private var nextResetExtensionType:String;
        private var extensionsManager:ExtensionsManager;
        private var mask_mc:Sprite;
        private var doNotMoveCard:Boolean = false;

        public function ExtensionsBar(){
            this.content_mc = this.extensionContent;
            this.contentStartY = this.content_mc.y;
            this.contentStartX = this.content_mc.x;
            this.thisStartX = this.x;
            this.thisStartY = this.y;
            this.openY = (this.contentStartY + 200);
            this.timer = new Timer(650, 1);
            this.timer.addEventListener(TimerEvent.TIMER, this.motionFailSafe);
            this.pane = this.content_mc.background_mc;
            this.paneInitialWidth = this.pane.width;
            this.pane.height = 200;
            this.pane.y = 0;
            this.content_mc.visible = false;
            this.content_mc.alpha = 0;
            this.setUpMask();
        }
        private function setUpMask(){
            this.mask_mc = new Sprite();
            this.mask_mc.graphics.beginFill(0xFF0000, 1);
            this.mask_mc.graphics.lineTo(this.paneInitialWidth, 0);
            this.mask_mc.graphics.lineTo(this.paneInitialWidth, this.pane.height);
            this.mask_mc.graphics.lineTo(0, this.pane.height);
            this.mask_mc.graphics.lineTo(0, 0);
            this.mask_mc = (this.addChild(this.mask_mc) as Sprite);
            this.mask_mc.x = -134;
            this.mask_mc.y = 0;
            this.content_mc.mask = this.mask_mc;
        }
        public function set cueCard(_arg1:AS3CueCard){
            this._cueCard = _arg1;
            this.extensionsManager = _arg1.extensionsManager;
        }
        private function onCardReset(_arg1:Object=null){
            this.content_mc.visible = false;
            this.content_mc.alpha = 0;
        }
        private function initilizeExtension():Number{
            var extensionHeight:* = NaN;
            try {
                this.currentExtension.removeEventListener("READY", this.extensionIsReady);
            } catch(e:Error) {
            };
            try {
                this.currentExtension.parent.removeChild(this.currentExtension);
            } catch(e:Error) {
            };
            if (this.nextExtensionLabel){
                this.content_mc.label_txt.autoSize = TextFieldAutoSize.LEFT;
                this.content_mc.label_txt.text = this.nextExtensionLabel;
                extensionHeight = 200;
            } else {
                if (this.nextExtension){
                    this.content_mc.label_txt.text = "";
                    this.currentExtension = this.nextExtension;
                    this.currentExtension.y = this.pane.y;
                    this.currentExtension.x = this.pane.x;
                    this.content_mc.addChild(this.currentExtension);
                    try {
                        extensionHeight = this.currentExtension.getHeight();
                    } catch(e:Error) {
                        trace("!~COULD NOT GET EXTENSION Height");
                    };
                    this.currentExtension.addEventListener("READY", this.extensionIsReady);
                };
            };
            return (extensionHeight);
        }
        public function arrangeContent(_arg1:Object=null):void{
            var object = _arg1;
            try {
                if (this.currentExtension){
                    this.currentExtension.arrangeContent(this.pane.width, this.pane.height);
                };
            } catch(e:Error) {
            };
            this.content_mc.label_txt.x = ((this.pane.x + (this.pane.width / 2)) - (this.content_mc.label_txt.width / 2));
            this.content_mc.label_txt.y = ((this.pane.y + (this.pane.height / 2)) - (this.content_mc.label_txt.height / 2));
        }
        public function loadExtensionLabel(_arg1:String){
            this.nextExtensionLabel = _arg1;
            this.nextExtension = null;
            if (this.open){
                this.closeExtensionBar();
            } else {
                this.openExtensionBar();
            };
        }
        public function loadExtension(_arg1){
            this.nextExtensionLabel = null;
            this.nextExtension = _arg1;
            if (this.open){
                this.closeExtensionBar();
            } else {
                this.openExtensionBar();
            };
        }
        private function clearTween(){
            if (this.motionTween){
                this.motionTween.stop();
                this.motionTween.removeEventListener(TweenEvent.MOTION_CHANGE, this.whileBarIsMoving);
                this.motionTween.removeEventListener(TweenEvent.MOTION_FINISH, this.barHasOpend);
                this.motionTween.removeEventListener(TweenEvent.MOTION_FINISH, this.barHasClosed);
                this.motionTween.removeEventListener(TweenEvent.MOTION_FINISH, this.barHasClosedInternally);
            };
            this.timer.reset();
        }
        private function cardIsFinishedReseting(_arg1:Event){
            this._cueCard.removeEventListener(AS3CueCard.CARD_RESET_EVENT, this.cardIsFinishedReseting);
            var _local2:Number = this._cueCard.motionManager.getEmptyScreenSpace().height;
            if (_local2 > 200){
                this.openExtensionBar();
            } else {
                this.openExtensionInCardBody();
            };
        }
        private function extensionIsReady(_arg1:Object=null){
            this.motionTween = this.animateMovieClip(this.content_mc, "y", this.targetY, 0.64);
            this.motionTween.addEventListener(TweenEvent.MOTION_FINISH, this.barHasOpend);
            this.motionTween.addEventListener(TweenEvent.MOTION_CHANGE, this.whileBarIsMoving);
            this.content_mc.visible = true;
            this.visible = true;
            this.content_mc.alpha = 1;
            var _local2:int = this.currentExtension.getHeight();
            if (!this.openInternally){
                if (this.doNotMoveCard == true){
                    this.doNotMoveCard = false;
                } else {
                    this._cueCard.motionManager.keepOnScreen(0, _local2, 0.64);
                };
            };
            this.arrangeContent();
        }
        private function openExtensionInCardBody(){
            this.openInternally = true;
            this._cueCard.always_mc.mainDragBox.visible = false;
            if (!this._cueCard.imageMode){
                this._cueCard.always_mc.controls_mc.visible = false;
            };
            this.open = true;
            this.y = (this.thisStartY - 30);
            this.clearTween();
            var extensionHeight:* = this.initilizeExtension();
            this.pane.height = extensionHeight;
            this.mask_mc.height = (extensionHeight + 10);
            this.content_mc.y = (this.contentStartY + extensionHeight);
            this.mask_mc.y = (17 - this.mask_mc.height);
            this.targetY = (((this.contentStartY + this.paneBottom) - extensionHeight) + 17);
            this.content_mc.__y = this.content_mc.y;
            try {
                this.currentExtension.init(this._cueCard);
                this.arrangeContent();
            } catch(e:Error) {
            };
            if (this._cueCard.videoPlayer.mezzThumbnail){
                if (this._cueCard.videoPlayer.mezzThumbnail.visible){
                    this._cueCard.videoPlayer.mezzThumbnail.hidePlayButton();
                };
            };
            if (this._cueCard.videoPlayer.related_vids){
                if (this._cueCard.videoPlayer.related_vids.visible){
                    this._cueCard.front_mc.setChildIndex(this._cueCard.front_mc.always_mc, (this._cueCard.front_mc.numChildren - 1));
                };
            };
        }
        public function openExtensionBar(){
            this.clearTween();
            var avalibleHeight:* = this._cueCard.motionManager.getEmptyScreenSpace().height;
            var extensionHeight:* = this.initilizeExtension();
            if (avalibleHeight < extensionHeight){
                if (this._cueCard.resizerHasChanged() == true){
                    this._cueCard.addEventListener(AS3CueCard.CARD_RESET_EVENT, this.cardIsFinishedReseting);
                    this._cueCard.resetSize();
                    return;
                };
                this.openExtensionInCardBody();
                return;
            };
            this.y = this.thisStartY;
            this.open = true;
            if (this.openInternally){
                this.openInternally = false;
            };
            this.pane.height = extensionHeight;
            this.content_mc.y = (this.paneBottom - extensionHeight);
            this.arrangeContent();
            this.mask_mc.height = extensionHeight;
            this.content_mc.__y = this.content_mc.y;
            this.targetY = this.contentStartY;
            this.mask_mc.y = 0;
            try {
                this.currentExtension.init(this._cueCard);
                this.arrangeContent();
            } catch(e:Error) {
            };
        }
        private function motionFailSafe(_arg1:TimerEvent){
            if (!this.openInternally){
            };
        }
        private function barHasOpend(_arg1:TweenEvent){
            var _local2:Number;
            var _local3:Number;
            var _local4:Number;
            var _local5:int;
            if (!this.openInternally){
                _local2 = this._cueCard.always_mc.extensions_bar.y;
                _local3 = this._cueCard.always_mc.extensions_bar.extensions_mc.extensionContent.background_mc.height;
                _local4 = (Number(_local2) + Number(_local3));
                _local5 = (Math.floor(this._cueCard.shadowBackground.main_bg.height) - 28);
                if (_local5 < _local4){
                    this._cueCard.shadowBackground.main_bg.height = (Number(_local4) + Number(28));
                };
            };
            this.barIsFullyOpen = true;
        }
        public function closeExtensionBar(_arg1:Boolean=false, _arg2:Boolean=false){
            var resetBar:Boolean = _arg1;
            var clearExtensions:Boolean = _arg2;
            if (clearExtensions){
                this.nextExtensionLabel = null;
                this.nextExtension = null;
            };
            this._cueCard.always_mc.mainDragBox.visible = true;
            this._cueCard.always_mc.controls_mc.visible = true;
            this.content_mc.__y = this.content_mc.y;
            this.clearTween();
            if (!this.openInternally){
                this.motionTween = this.animateMovieClip(this.content_mc, "y", ((this.contentStartY - this.content_mc.height) + this.paneBottom), 0.64);
                this.motionTween.addEventListener(TweenEvent.MOTION_FINISH, this.barHasClosed);
                if (this._cueCard.flipped){
                    this._cueCard.always_mc.mainDragBox.visible = false;
                };
            } else {
                this.motionTween = this.animateMovieClip(this.content_mc, "y", (this.contentStartY - this.paneBottom), 0.64);
                this.motionTween.addEventListener(TweenEvent.MOTION_FINISH, this.barHasClosedInternally);
                if ((((this._cueCard.flipped == false)) && ((this._cueCard.imageMode == false)))){
                    if (this._cueCard.motionManager.preventDragging == true){
                        this._cueCard.always_mc.mainDragBox.visible = false;
                    };
                    try {
                        if (this._cueCard.videoPlayer.related_vids.visible){
                            this._cueCard.always_mc.mainDragBox.visible = false;
                            this._cueCard.always_mc.controls_mc.visible = false;
                        };
                    } catch(e:Error) {
                    };
                    try {
                        if (this._cueCard.videoPlayer.mezzThumbnail.visible){
                            this._cueCard.always_mc.mainDragBox.visible = false;
                            this._cueCard.always_mc.controls_mc.visible = false;
                        };
                    } catch(e:Error) {
                    };
                } else {
                    this._cueCard.always_mc.mainDragBox.visible = false;
                    this._cueCard.always_mc.controls_mc.visible = false;
                };
            };
            this.motionTween.addEventListener(TweenEvent.MOTION_CHANGE, this.whileBarIsMoving);
            this.timer.stop();
            this.timer.reset();
            this.barIsFullyOpen = false;
        }
        private function whileBarIsMoving(_arg1:TweenEvent=null){
            var _local2:Number = (this.content_mc.y - this.content_mc.__y);
            this.content_mc.__y = this.content_mc.y;
            if (!this.openInternally){
                this._cueCard.shadowBackground.main_bg.height = (this._cueCard.shadowBackground.main_bg.height + _local2);
            };
            this._cueCard.updateStageDimensions({
                changeX:0,
                changeY:_local2
            });
        }
        private function barHasClosedInternally(_arg1:TweenEvent){
            this.open = false;
            this.visible = false;
            this.content_mc.visible = false;
            this.content_mc.alpha = 0;
            if (((!((this.nextExtensionLabel == null))) || (!((this.nextExtension == null))))){
                this.openExtensionBar();
            };
        }
        private function barHasClosed(_arg1:TweenEvent){
            this.open = false;
            this.content_mc.visible = false;
            this.content_mc.alpha = 0;
            this.visible = false;
            if (((!((this.nextExtensionLabel == null))) || (!((this.nextExtension == null))))){
                this.openExtensionBar();
            };
        }
        public function resetContent(){
            this.pane.width = this.paneInitialWidth;
            this.mask_mc.width = this.paneInitialWidth;
            this.arrangeContent();
        }
        public function refreshOpenExtensions(){
            if (this.open){
                try {
                    this.doNotMoveCard = true;
                    this.currentExtension.init(this._cueCard);
                    this.arrangeContent();
                } catch(e:Error) {
                };
            };
        }
        public function masterDimensionResize(_arg1:Number, _arg2:Number){
            this.pane.width = (this.pane.width + _arg1);
            this.mask_mc.width = (this.mask_mc.width + _arg1);
            this.arrangeContent();
        }
        private function animateMovieClip(_arg1:Sprite, _arg2:String, _arg3:Number, _arg4:Number):Tween{
            var _local5:Tween;
            _local5 = new Tween(_arg1, _arg2, Regular.easeInOut, _arg1[_arg2], _arg3, _arg4, true);
            return (_local5);
        }

    }
}//package 
