﻿package {
	
	import adobe.utils.CustomActions;
	import fl.containers.ScrollPane;
	import fl.motion.Color;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.*
	import flash.geom.ColorTransform;
	import flash.geom.Transform;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;

	
	
	public class PlayListWindow extends MovieClip{
		private var player;
		private var playList_mc:MovieClip;
		private var AODlibrary:Object;
		public var cueCard:AS3CueCard;
		
		public var active:Boolean = false;
		public var open:Boolean = false;
		private var thumbnail_array:Array;
		
		public var nbc_logo:Sprite;
		
		private var pen_mc:Shape;
		private var lines_array:Array;
		private var pane:ScrollPane;
		
		
		public function PlayListWindow(_player,_cueCard:AS3CueCard) {
			cueCard = _cueCard;
			playList_mc = this;
			player = _player;
			//player.source = "video_assets/caption_video.flv";
			//player.play();
			//player.fullScreenTakeOver =  true;
			//player.volume = 1;
			//player.autoRewind = true;
	
			thumbnail_array = new Array();
			lines_array = new Array();
			pane = pane_mc;
			
			
		}
		
		
		
		public function updatePlayList():void{
			// CALL API TO THE AIR SHELL TO UPDATE THE PLAY LIST
			getPlayList();
		}
		
		private function arangePlayList():void{
			/* When the play list has been rendered on screen or the window size has been adjusted
			*  shift the play list icons around to fit on the screen
			*/
		}
		
		public function setAODLib(_AODlibrary:Object) {
			AODlibrary = _AODlibrary;
			
			getPlayList();
		}
		
		public function getPlayList() {

			buildPlayList(AODlibrary.getAllCueCards());

		}
		
		 function deleteThumb(deleteThumb:PlayListThumbnail) {
			
			var deleteSuccess:String = deleteCueCard(deleteThumb.data.Asset_Id);
			if(deleteSuccess == "SUCCESS") {
				for (var index:int = 0 ; index < thumbnail_array.length; index++) { // go through all the cards in the thumbs array
					var thumb:PlayListThumbnail = thumbnail_array[index];
					if (thumb == deleteThumb) { // if this is the correct thumb
						deleteThumb.remove();
						thumbnail_array.splice(index, 1);
						var contents:MovieClip = pane.content as MovieClip;
						contents.removeChild(deleteThumb);
						break;
					}
					
				}
				arrangeContent();
			}
			
		}
		
		private function deleteCueCard(assetID:String){
			var result:String = AODlibrary.deleteCueCardFromCatalogue (assetID);
			return result;
		}
		
		public function playVideo(AssetId) {
			cueCard.loadCueCard(AssetId);
		}
	
		
		private function buildPlayList(cards_array) {
			for (var index:int = 0 ; index < cards_array.length; index++) {
				var card_obj:Object = cards_array[index];
				addThumbnail(card_obj);
			}
		
		}
		//_cueCard.playList_mc.addThumbnail(_cueCard.AODPlayerLibrary.aodPlayerLib.getCueCard(event.id));

		public function newCardAdded(event:Object) {
			trace("-------NEW CARD ADDED-------");
		trace("event.getCueCard().Asset_Id = " + event.getCueCard().Asset_Id);
			//var card_obj:Object =  cueCard.AODPlayerLibrary.aodPlayerLib.getCueCard(event.getCueCard().Asset_Id);
			//addThumbnail(card_obj);
		}
		
		public function addThumbnail(card_obj:Object) {
			var thumb:PlayListThumbnail = new PlayListThumbnail(card_obj,this);
				var contents:MovieClip = pane.content as MovieClip;
				contents.addChild(thumb) as MovieClip;
				//thumb.data = ;

				thumb.loadImage(card_obj.ThumbnailUrl);
				thumbnail_array.push(thumb);
			
		}
		
	
		
		private function createSeporator(length:Number,_y:Number):Shape {
			var line:Shape = new DottedLine(length);
			var contents:MovieClip = pane.content as MovieClip;
			contents.addChild(line) as MovieClip;
			line.y = _y;
			line.x = 15;
			lines_array.push(line);
			return line;
		}
		private function clearSeporators() {
			for (var index:int = 0 ; index < lines_array.length ; index++) {
				
				var contents:MovieClip = pane.content as MovieClip;
				contents.removeChild(lines_array[index]);
				delete lines_array[index];
			}
			
			lines_array = new Array();
		}
		
		
		
		public function arrangeContent():void {
			if (!active) {
				return;
			}
			if(lines_array.length > 0){
				clearSeporators();
			}
			pane.setSize(this.parent["bg"].width-5,this.parent["bg"].height-32)
			//pane.width = this.parent["bg"].width-5
			//pane.height = this.parent["bg"].height - 20;
			for (var index:int = 0 ; index < thumbnail_array.length ; index++) {
				
				var thumb:PlayListThumbnail = thumbnail_array[index];
				var prevThumb:PlayListThumbnail = thumbnail_array[index - 1];
				if (prevThumb) {
					var targetX:Number = prevThumb.x + prevThumb.getWidth();
					var targetRight:Number = Number(targetX) + Number(thumb.getWidth());
					
					
					
					if (targetRight> this.parent["bg"].width ) { // if the thumbnail will be greater than the size of the background
						
						thumb.y = Number(prevThumb.y) + Number(prevThumb.getHeight()) + 15; // drop it to the next line
						createSeporator(this.parent["bg"].width-50,thumb.y -7); // draw a doted line in between the icons
						thumb.x = 15; // and put it in the first row
					}else { // otherwise this card goes to the left of the last one
						thumb.y = prevThumb.y;
						thumb.x = targetX;
						
					}
				}else {
					thumb.y = 10;
					thumb.x = 15;
					createSeporator(this.parent["bg"].width-50,thumb.y -7); // draw a doted line in between the icons
				}
			
				
			}
			
			// Place the NBC LOGO ON THE BOTOM RIGHT
			this.parent["nbc_logo"].x = this.parent["bg"].width;
			this.parent["nbc_logo"].y = this.parent["bg"].height;
			
			pane.update();
			
		}		
		
		
		
		
		public function setText(_text:String) {
			//this.title_txt.text = _text;
		}
		public function removeFileDirectory() {
			//fileDirBackground.visible = false;
			//file_txt.visible = false;
		}
		public function setFileDirectory() {
			//fileDirBackground.visible = true;
			//file_txt.visible = true;
			
		}
		
		
	}
	
	
	
}
// ------CONTROLE FULL SCREEN FUNCTIONALITY---------

/*fullScreen_btn.addEventListener(MouseEvent.CLICK, toggleFullScreen);
var isFullScreen:Boolean = false;
function toggleFullScreen(e:MouseEvent){
	if(!isFullScreen){
		isFullScreen = true;
		fullScreen_btn.stage.displayState = StageDisplayState.FULL_SCREEN; // go Full Screen
	}else{
		isFullScreen = false;
		fullScreen_btn.stage.displayState = StageDisplayState.NORMAL; // go Full Screen
	}
}
stage.addEventListener(FullScreenEvent.FULL_SCREEN,fullScreenEvent);
function fullScreenEvent(e:FullScreenEvent){
	trace("FULLL SCREEN:" + stage.displayState);
	for(var i:String in e){
		trace(i+ " = " + e[i]);
	}
}
*/

// ----------ADD IN THE VIDEO PLAYER------------


//player.fullScreenBackgroundColor = 0x000000;











