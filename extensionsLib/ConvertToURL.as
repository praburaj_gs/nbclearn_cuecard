package  
{
	import flash.text.TextField;
	/**
	 * ...
	 * @author Ari Oshinsky
	 */
	
	public class ConvertToURL{
		
		private var myOriginalString:String;
		private var myTextFeild:TextField;

		//first we need to split out by each word so we can identify a url link
		var myArray:Array;

		//we use this variable a little later
		var myV:String ="";
		//we are searching for words that contain 'www.'
		var searchValue = "www.";
				
		
		public function ConvertToURL() {
			//this is the function that searches through our array
			//by each word for our searchValue, when it finds the value
			//it returns it as a link in html formated text.
			
		}
		
		public  function convertTextFeild(_textFeild:TextField) {
			
			myOriginalString =	_textFeild.text;
			myTextFeild = _textFeild
			
			
			//first we need to split out by each word so we can identify a url link
			
			trace("---- START ----");
			
			myArray = myOriginalString.split(" ");

			//we use this variable a little later
			var myV ="";
			//we are searching for words that contain 'www.'
			var searchValue = "www.";
			testSearch(searchValue);		
						
		}
		private function testSearch (searchValue) {
			for (var i=0; i<myArray.length; i++) {
						
				var str:String = myArray[i].toString()
				var startIndex:Number = 0;
				var oldIndex:Number = 0;
				var newString:String = "";
				myTextFeild.htmlText = "";
				
			
				
				while ((startIndex = str.indexOf(searchValue, startIndex)) != -1) {
					trace("str: " + str);
					newString += str.substring(oldIndex, startIndex)+"<font color='#0000FF' target='_blank'><a href='http://"+str+"'><u>"+str+"</u></a></font>";
					oldIndex = startIndex += searchValue.length;
					//set the value of our text field with what we find
					myTextFeild.htmlText += replaceString(myOriginalString, str, newString);
				}
				
			}
		}
		
		
		
		
		private function searchForURL_inTextFeild (searchValue) {
			for (var i=0; i<myArray.length; i++) {
						
				var str = myArray[i].toString()
				var startIndex:Number = 0;
				var oldIndex:Number = 0;
				var newString:String = "";
						
				while ((startIndex = str.indexOf(searchValue, startIndex)) != -1) {
					trace("str: " + str);
					newString += str.substring(oldIndex, startIndex)+"<font color='#0000FF' target='_blank'><a href='http://"+str+"'><u>"+str+"</u></a></font>";
					oldIndex = startIndex += searchValue.length;
					//set the value of our text field with what we find
					myTextFeild.htmlText += replaceString(myOriginalString,str,newString)
				}
			}
		}
		
		private function replaceString(str:String, find:String, replace:String):String {
			var startIndex:Number = 0;
			var oldIndex:Number = 0;
			var newString:String = "";
			while ((startIndex=str.indexOf(find, startIndex)) != -1) {
				newString += str.substring(oldIndex, startIndex)+replace;
				oldIndex = startIndex += find.length;		
				for(var i=oldIndex;i<str.length;i++){				
					myTextFeild.htmlText += (str.charAt(i));
					myV += (str.charAt(i));
					
				}
			}

			return ((newString == "") ? str : newString + myV);
		}
		
		
	}

}