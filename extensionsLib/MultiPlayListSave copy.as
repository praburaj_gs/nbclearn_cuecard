﻿package 
{
		
	import fl.containers.ScrollPane;
	import fl.controls.RadioButton;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import DottedLine;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.display.StageAlign
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign
	import flash.events.MouseEvent;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.system.Security
	import flash.system.SecurityDomain;
	import flash.ui.Mouse;
	import flash.text.TextFieldAutoSize;
	import flash.filters.BevelFilter;
	import IExtensions;
	import flash.events.FocusEvent;
	import TabIndexManager;
	

	/**
	 * ...
	 * @author 
	 */
	public class MultiPlayListSave extends MovieClip implements IExtensions
	{
		private var _width:int = 445;
		private var _height:int = 153;
		public var ID:String = "MultiPlayListSave";
		private var cueCard;// :AS3CueCard;
		
		private var ClientCode:String = "icue";
		private var bookmarkCode:String = "iCue";
		private var Title:String;
		private var Asset_ID:String;
		private var BookMarkURL:String;
		
		var line1:DottedLine;
		var line2:DottedLine;
		
		var line1Y:int = 28;
		var line2Y:int = 123;
		
		private var xml:XML;
	
		private var addThis_array:Array;
		private var pane:ScrollPane;
		private var paneSource;
		private var tabs_array:Array;
		
		// |--- SOCIAL NET VARIABLES -->
		private var _contentID:String;// = "204355786";
		public var _personUUID:String;//  = 'e210728b40d2c34176cb01d42258d12b';
		public var _sessionKey:String;
		public var _profileUUID:String;
		private var _siteName:String = "aod.icue.nbcuni.com";
		private var _siteDomainName:String = "aod.icue.nbcuni.com";
		private var _baseURL:String;// = "http://archives.nbclearn.com/nbcarchive/snas/api/" //"http://snasqa.nbcuni.com/snas/api/";  // <--- Direct to snas QA // "http://snasdev1.nbcuni.com/snas/api/";  // <--- Direct to snas (dev) //   "http://64.210.195.182:9191/icue/snas/api/" // <-- DEV Proxy from vignette   
		private var cardURL:String;// = '09c2d888fcef4210VgnVCM10000075c1d240RCRD';
		private var listener_array:Array;
		private var numberOfCurrentCalls:Number = 0;
		public var calls_array:Array;
		private var playListArray:Array;
		private var checkedPlayLists:XMLList;
		private var bevel:BevelFilter;
		
		private var createMode:Boolean = false;
		private var saving:Boolean = false;
		private var tabManager:TabIndexManager;

		// <---- SOC NET VAR ------|
		
		public function MultiPlayListSave() 
		{
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			
			//this.stage.align = StageAlign.TOP_LEFT;
			pane = this.playListPane_mc;
		
		
			
			
			 create_mc.create_txt.restrict = '^"'; // don't allow the double quoats "
			 
			save_btn.label_txt.text = "SAVE";
			close_btn.label_txt.text = "CANCEL";
			line1 = new DottedLine(418);
			line1.y = line1Y;
			line1.x = 5
			line2 = new DottedLine(418);
			line2.y = line2Y;
			line2.x = 5;
			addChild(line1);
			addChild(line2);
			//this.stage.addEventListener(Event.RESIZE, stageResize);
			addThis_array = new Array();
			Colors.changeTint(save_btn.bg, 0x212328, 1);
			Colors.changeTint(close_btn.bg, 0x212328, 1);
			bevel = new BevelFilter(-1.3,45,0x999999,.52,0x000000,.74,2,2,1,3);
			
			eventSetUp();
			//init();
			
			
		}
		private function handleCheckedPlayLists(e:Event) {
			var xml:XML = new XML(e.target.data);
		
			xml.ignoreWhitespace = true; 
			
			checkedPlayLists = xml.content;
			
			listAllPlaylist(handleXML);
		}

		private function eventSetUp() {
			
			// CLOSE BUTTON
			close_btn.buttonMode = true;
			close_btn.useHandCursor = true;
			close_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				
				dispatchEvent(new Event("CLOSE"));
			});
			close_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				Colors.changeTint(close_btn.bg, 0xc8234a, 1);
				close_btn.filters = [bevel];
			});
			close_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				Colors.changeTint(close_btn.bg, 0x212328, 1);
				close_btn.filters = [];
			});
			
			// SAVE BUTTON
			save_btn.buttonMode = true;
			save_btn.useHandCursor = true;
			save_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				
				
				if (!createMode) {
					if(getSelectedCategories().length > 0){ // you must have at least one selected playlist in order to save
						saving = true;
						saveMultiPlayList(); 
					}else {
						//trace("must select a category");
						return
					}
				}else {
					if(create_mc.create_txt.text.length > 0 && create_mc.create_txt.text != " "){
						saving = true;
						createANewPlayList(create_mc.create_txt.text);
					}else {
						//trace("~no create text");
						return;
					}
				}
				Colors.changeTint(save_btn.bg, 0xFF0000, 1);
				save_btn.label_txt.text = "SAVING..."
				cueCard.omniture.trackEvent("event37", "Saved To Playlist");
				
			});
			save_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				if(!saving){
					Colors.changeTint(save_btn.bg, 0xF36F21, 1);
					save_btn.filters = [bevel];
				}
			});
			save_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				if(!saving){
					Colors.changeTint(save_btn.bg, 0x212328, 1);
					save_btn.filters = [];
				}
			});
			
			
			create_mc.create_txt.addEventListener(FocusEvent.FOCUS_IN, function(e:FocusEvent) {
				cueCard.disableKeyboardControl = true;
				//trace("~FOCUS_IN");
			});
			create_mc.create_txt.addEventListener(FocusEvent.FOCUS_OUT, function(e:FocusEvent) {
				
				cueCard.disableKeyboardControl = false;
				//trace("~FOCUS_OUT");
			});
			
			// RADIO BUTTONS
			existing_rb.buttonMode = true;
			existing_rb.useHandCursor = true;
			new_rb.buttonMode = true;
			new_rb.useHandCursor = true;
			
			existing_rb.addEventListener(MouseEvent.CLICK,enterPlayListMode);
			new_rb.addEventListener(MouseEvent.CLICK, enterCreateMode);
			
			
		}
		
		private function setUpTabOrder() {
			
			
			
			
				
				
			tabManager.setTab(existing_rb, tabManager.numTabs,true);
			tabManager.setTab(new_rb, tabManager.numTabs, true);
		
			//tabManager
			for (var index:int = 0 ; index < tabs_array.length ; index++) {
				var tab:DynamicSprite = tabs_array[index];
				tab.tabEnabled = true;
				tabManager.setTab(tab.label.hit_btn, tabManager.numTabs);
			}
			tabManager.setTab(save_btn, tabManager.numTabs);
			tabManager.setTab(close_btn, tabManager.numTabs);
			
			cueCard.fmanager.setFocus(existing_rb);
		}
		
		private function enterCreateMode(e:Object = null) {
			create_mc.visible = true;
			
			this.stage.focus = create_mc.create_txt;
			pane.visible = false;
			hidePlayLists();
			createMode = true;
		}
		private function enterPlayListMode(e:Object = null) {
			create_mc.visible = false;
			pane.visible = true;
			showPlaylists();
			createMode = false;
			existing_rb.selected = true;
		}
		
		
		private function createANewPlayList(_catName:String) {
			if (checkForUndefined(_contentID)) { // if this cue card has NOT already been stored in snas and does NOT have contentID
						trace("~NO CONTENT ID")
				saveCueCard(cueCard.metaData.description, cueCard.metaData.title, _catName, function() {
					createContentCategory(_catName, onPlayListCreated);
				});
			}else{
				createContentCategory(_catName, onPlayListCreated); //This creates a single new category // withought saving the cue cue card first
			}
		}
		private function onPlayListCreated(playListName:String) {
				//trace("~onPlayListCreated: " + playListName);
				addContentToContentGroups(null, new Array(playListName));// Add this new playList to it (in array form)
		} 

		private function changeTextBackToSave(e:TimerEvent) {
			save_btn.label_txt.text = "SAVE";
			saving = false
			Colors.changeTint(save_btn.bg, 0x212328, 1);
			
		}
		private function stageResize(e:Event) {
			
			arrangeContent(this.stage.stageWidth, this.stage.stageHeight);
		}
		
		public function getWidth():int {
			return _width;
		}
		public function getHeight():int {
			return _height;
		}
		
		private function showPlaylists() {
			for(var index:Number = 0 ; index < tabs_array.length ; index++){
			
				var tab:DynamicSprite = tabs_array[index];
				tab.visible = true;
			}
		}
		
		private function hidePlayLists() {
			for(var index:Number = 0 ; index < tabs_array.length ; index++){
			
				var tab:DynamicSprite = tabs_array[index];
				tab.visible = false;
			}
		}
	
		
		public function arrangeContent(bg_width:Number, bg_height:Number) {
			
			if (line1) {
				this.removeChild(line1);
			}
			if (line2) {
				this.removeChild(line2);
			}
			line1 = new DottedLine(bg_width-30);
			line1.x = 5;
			line1.y = line1Y;
			line2 = new DottedLine(bg_width-30);
			line2.x = 5;
			line2.y = line2Y;
			addChild(line1);
			addChild(line2);
			close_btn.x = bg_width - 77;
			save_btn.x = close_btn.x - 70;
			pane.setSize(bg_width - 5, pane.height);
			
			for(var index:Number = 0 ; index < tabs_array.length ; index++){
			
				var tab:DynamicSprite = tabs_array[index];
				var prevTab:DynamicSprite = tabs_array[index-1];
				if(prevTab){
					tab.y = prevTab.y;
					tab.x = prevTab.x + 130; 
					
					if (tab.x > bg_width - 135){ //pane.width - 130){ // test to see if we need to move this down a line
						tab.x = 15;
						tab.y = prevTab.y + 21;
					}
				}else{
					tab.y = 40;
					tab.x = 15;
				}
			}
			pane.update();
			pane.invalidate();
		}
		private function createButton(_info):DynamicSprite {
			var tab:DynamicSprite = new DynamicSprite();
			
			tab._info = _info;
			if(_info.selected == true){
				tab.checked = true;
			}else {
				tab.checked = false;
			}
	
			
			
			var label:GENERIC_BUTTON = new GENERIC_BUTTON();
			label.x = 16;
			
			
			tab.label = tab.addChild(label);
			var checkMark:CheckMark = new CheckMark();
			checkMark.y = 3;
			checkMark.check_img.visible = false;
			tab.checkMark = tab.addChild(checkMark) as Sprite;
			
			if (tab.checked) {
				tab.checkMark.check_img.visible = true;
				tab.markedSelected = true;
			}else {
				tab.checkMark.check_img.visible = false;
				tab.markedSelected = false;
			}
			
			tab.swapChildren(checkMark, label);
			tab.fmt = new TextFormat();
			tab.fmt.align = TextFormatAlign.LEFT;
			tab.fmt.color = 0x000000
			
			label.label_txt.autoSize = TextFieldAutoSize.LEFT;
			label.label_txt.text = constrainText(_info.name, 20);
			label.hit_btn.width = label.label_txt.width;
			label.bg.width = label.label_txt.width;
			Colors.changeTint(label.bg, 0x212328, 1);
			label.hit_btn.width += 16;
			label.hit_btn.x -= 16;
			label.hit_btn.useHandCursor = true;
			label.hit_btn.buttonMode = true;
			tab.catName = _info.name;
			tab.id = _info.attribute("ID");
			tab.order = _info.desc; 
			label.label_txt.setTextFormat(tab.fmt);
			label.bg.alpha = 0;
			
			//trace("!catName = " + tab.catName);
			//trace("!id = " + tab.id);
			//trace("!order = " + tab.order);

			label.hit_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				
				e.currentTarget.parent.parent.fmt.color = 0xFFFFFF
				e.currentTarget.parent.label_txt.setTextFormat(e.currentTarget.parent.parent.fmt);
				e.currentTarget.parent.bg.alpha = 1;
			});
			label.hit_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				//if(!e.currentTarget.parent.parent.checked){
					e.currentTarget.parent.parent.fmt.color = 0x000000
					e.currentTarget.parent.label_txt.setTextFormat(e.currentTarget.parent.parent.fmt);
					e.currentTarget.parent.bg.alpha = 0;
				//}
			});
			label.hit_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				
				var myTab:DynamicSprite = e.currentTarget.parent.parent;
				if (!myTab.checked) {
					myTab.checked = true
					myTab.checkMark.check_img.visible = true;
				}else {
					myTab.checked = false;
					myTab.checkMark.check_img.visible = false;
				}
				
			});
			
			return tab;
		}
		
		public function init(_cueCard):void {
			saving = false;
			//trace("~INIT SAVE DRAW");
			Title = _cueCard.metaData.title;
			Asset_ID = _cueCard.metaData.Asset_ID;
			BookMarkURL = _cueCard.metaData.bookMarkLink;
			cardURL = _cueCard.metaData.vcmID;
			cueCard = _cueCard;
			tabManager = cueCard.tabManager;
			save_btn.label_txt.text = "SAVE";
			
			
			
			_baseURL = _cueCard.stage.loaderInfo.parameters.snasapiURL;
			_personUUID = _cueCard.stage.loaderInfo.parameters.personUUID;
			
			if (_cueCard.Product == "BBHE" || _cueCard.Product == "HE") { 
				_siteName = 'higheredsa.icue.nbcuni.com';
				_siteDomainName = 'higheredsa.icue.nbcuni.com';
			}
			
			enterPlayListMode();
			if (tabs_array) {
				for(var index:Number = 0 ; index < tabs_array.length ; index++){
			
					var tab:DynamicSprite = tabs_array[index];
					tab.parent.removeChild(tab);
					
				}
			}
			getContent(); // check to see if this card has already been saved to snass
			
		}

// ------ SOCIAL NETWORK FUNCTIONS -----------
		
		
		private function noCash(){
			Math.round(Math.random()*1000)
			var returnString:String = "&randomNumber="+String(Math.round(Math.random()*1000));
			return returnString;
		}
		private function overide(){
			var return_str:String = "&mgcprm=1";
			
			return return_str;
		}
		
		private function authenticate(){
			var return_str:String = "&loggedpersonuuid=" + _personUUID + "&sessionid=" + _sessionKey;
			return return_str;
			
		}

		public function saveCueCard(postText:String,contentTitle:String,contentGroupName,handler:Function =  null){

			postText = cleanText(String(postText));
			//var load_str:String = _baseURL+"addBlogPost?personUUID="+_personUUID+"&siteName="+_siteName+"&siteDomainName="+_siteDomainName+"&groupName="+contentGroupName+"&categoryName=My Playlists&postText="+postText+"&URL="+cardURL+","+_personUUID+"&contentSSUniqueKey="+cardURL+","+_personUUID+"&contentTitle="+contentTitle+"&privacyLevelCode=PUBLIC"+overide()+noCash();
			var   load_str:String = _baseURL+"addBlogPost?personUUID="+_personUUID+"&siteName="+_siteName+"&siteDomainName="+_siteDomainName+"&groupName="+escape(contentGroupName)+"&categoryName=My Playlists&postText="+escape(postText)+"&URL="+cardURL+","+_personUUID+"&contentSSUniqueKey="+cardURL+","+_personUUID+"&contentTitle="+escape(contentTitle)+"&privacyLevelCode=PUBLIC"+authenticate()+overide()+noCash();
			var request:URLRequest = new URLRequest(load_str);
			var xmlLoader:URLLoader = new URLLoader();
			
			cueCard.signalNetworkTraffic("saveCueCard");
			xmlLoader.addEventListener(Event.COMPLETE, function(e:Event) {
				var returnXML:XML = new XML(e.target.data);
				_contentID = returnXML.entityIdentifier;
				cueCard.metaData.contentID = _contentID;
				//trace("~ returned in save function _contentID: " + _contentID);
				//cueCard.metaData.contentID = String(returnXML.status.entityIdentifier);
				handler();
				cueCard.signalNetworkTrafficStop("saveCueCard");
			});
			xmlLoader.load(request);
			
		}
		public function getContent(handler:Object = null){
		
			
			var load_str:String = _baseURL+"getContent?personUUID="+_personUUID+"&siteName="+_siteName+"&siteDomainName="+_siteDomainName+"&contentGroupTypeCode=BLOG&contentType=BLOG_POST&sourceSystemUniqueKey="+cardURL+","+_personUUID+overide()+noCash();
			var request:URLRequest = new URLRequest(load_str);
			var xmlLoader:URLLoader = new URLLoader();
			xmlLoader.addEventListener(Event.COMPLETE, function(e:Event) {
				try {
					_contentID = new XML(e.target.data).contentID;
					cueCard.metaData.contentID = _contentID;
				}catch (e:Error) {
					_contentID = null;
				}
				cueCard.signalNetworkTrafficStop("getContent");
				listContentPlaylists(handleCheckedPlayLists);
				
				
			});
			cueCard.signalNetworkTraffic("getContent");
			xmlLoader.load(request);
		}

	
	  // LIST ALL PLAYLIST
		public function listAllPlaylist(handler:Function = null){
	
			var load_str:String = _baseURL+"listAlbumsByType?personUUID="+_personUUID+"&viewerPersonUUID="+_personUUID+"&siteName="+_siteName+"&contentGroupType=BLOG&contentGroupTypeCode=BLOG&siteDomainName="+_siteDomainName+"&fromRow=1&toRow=1000"+authenticate()+overide()+noCash();
			var request:URLRequest = new URLRequest(load_str);
			var xmlLoader:URLLoader = new URLLoader();
			xmlLoader.addEventListener(Event.COMPLETE, function(e:Event) {
				cueCard.signalNetworkTrafficStop("listAllPlaylist");
				handler(e);
				dispatchEvent(new Event("READY"));
			});
			
			cueCard.signalNetworkTraffic("listAllPlaylist");
			xmlLoader.load(request);
		}
		public function listContentPlaylists(handler:Function = null){
		
			//trace("~listContentPlaylists");
			var load_str:String = _baseURL+"getContentGroup?contentID="+_contentID+"&siteName="+_siteName+"&siteDomainName="+_siteDomainName+overide()+noCash();
			var request:URLRequest = new URLRequest(load_str);
			var xmlLoader:URLLoader = new URLLoader();
			xmlLoader.addEventListener(IOErrorEvent.IO_ERROR , function(e:Event) {
				dispatchEvent(new Event("READY"));
				cueCard.signalNetworkTrafficStop("listContentPlaylists");
			});
			xmlLoader.addEventListener(Event.COMPLETE, function(e:Event) {
				cueCard.signalNetworkTrafficStop("listContentPlaylists");
				handler(e);
				
			});
			cueCard.signalNetworkTraffic("listContentPlaylists");
			xmlLoader.load(request);
						
		}
// ADD A PLAYLIST TO THE CUE CARD
		public function addContentToContentGroups(handler:Object, groupsName_array:Array) {
			var contentGroups_str:String = "";
			for (var index:Number = 0 ; index < groupsName_array.length ; index++) {
				
				contentGroups_str += "&contentGroupName=" + escape(groupsName_array[index]);
				
			}
			cueCard.metaData.firstPlayListName = groupsName_array[0];
			//trace("~contentGroups_str = " + contentGroups_str);
		
			//_root.arisOutPut.text += "contentGroups_str:\n" + contentGroups_str;
			var this_class = this;
			//&contentGroupName=AODPlaylist1&contentGroupName=AODPlaylist3&contentGroupName=AODPlaylist4
			//trace("~ in contentGroups - _contentID: " +  _contentID);
			var load_str:String = _baseURL+"addContentToContentGroups?personUUID="+_personUUID+"&siteName="+_siteName+"&siteDomainName="+_siteDomainName+contentGroups_str+"&contentSSUniqueKey="+cardURL+","+_personUUID+"&contentURL="+cardURL+","+_personUUID+"&contentID="+_contentID+"&contentGroupTypeCode=BLOG&contentTypeCode=BLOG_POST&fromRow=1&toRow=1000"+authenticate()+overide()+noCash();
			var request:URLRequest = new URLRequest(load_str);
			var xmlLoader:URLLoader = new URLLoader();
			xmlLoader.addEventListener(Event.COMPLETE, function(e:Event) {
				Colors.changeTint(save_btn.bg, 0x00FF00, 1);
				save_btn.label_txt.text = "SAVED";
				var closeTimer:Timer = new Timer(500, 1);
				closeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent) {
					dispatchEvent(new Event("CLOSE"));
				});
				closeTimer.start();
				
				var changeBackTimer:Timer = new Timer(1700, 1);
				changeBackTimer.addEventListener(TimerEvent.TIMER_COMPLETE, changeTextBackToSave);
				changeBackTimer.start();
				cueCard.signalNetworkTrafficStop("addContentToContentGroups");
				cueCard.reloadPage = true;  // this will cause the page if it is in the playlist area.
			});
			cueCard.signalNetworkTraffic("addContentToContentGroups");
			xmlLoader.load(request);
			
		}
	
// REMOVE A PLAYLIST FROM THE CUE CARD
		public function removeContentFromContentGroups(handler:Function, groupsName_array:Array) {
			var contentGroups_str:String = "";
			for (var index:Number = 0 ; index < groupsName_array.length ; index++) {
				
				contentGroups_str += "&contentGroupName=" + groupsName_array[index];
				
			}
												   
			var load_str:String = _baseURL+"removeContentFromContentGroups?personUUID="+_personUUID+"&siteName="+_siteName+"&siteDomainName="+_siteDomainName+contentGroups_str+"&contentSSUniqueKey="+cardURL+","+_personUUID+"&contentURL="+cardURL+","+_personUUID+"&contentID="+_contentID+"&contentGroupTypeCode=BLOG&contentTypeCode=BLOG_POST&fromRow=1&toRow=1000"+overide()+noCash();
			var request:URLRequest = new URLRequest(load_str);
			var xmlLoader:URLLoader = new URLLoader();
			xmlLoader.load(request);
		}
		
// CREATE A NEW PLAY LIST
		public function createContentCategory(catname:String,handler:Function = null){
			
			var load_str:String = _baseURL+"createBlog?personUUID="+_personUUID+"&siteName="+_siteName+"&siteDomainName="+_siteDomainName+"&blogName="+escape(catname)+overide()+noCash();
			var request:URLRequest = new URLRequest(load_str);
			var xmlLoader:URLLoader = new URLLoader();
		
			xmlLoader.addEventListener(Event.COMPLETE, function(e:Event) {
				cueCard.signalNetworkTrafficStop("createContentCategory");
				handler(catname);
			});
			cueCard.signalNetworkTraffic("createContentCategory");
			xmlLoader.load(request);
	
		}
		
		
// <------------- END SOCIAL NETWORK FUNCTIONS ---------|
		
		// Go through the selected categories and add them to a string to save as playLists
		private function getSelectedCategories():Array {
			var return_array:Array = new Array();
		
			for (var index:int = 0;  index < tabs_array.length ; index++) {
				var tab:DynamicSprite = tabs_array[index];
				//tab.catName;
				//tab.id;
				//tab.order;
				//tab.markedSelected;
				if(tab.checked == true){
					return_array.push(tab.catName);
				}
			}
			return return_array;
		}
		
		private function getDeselectedPlayLists():Array {

			var return_array:Array = new Array();
			for (var index:int = 0;  index < tabs_array.length ; index++) {
				var tab:DynamicSprite = tabs_array[index];
				//tab.catName;
				//tab.id;
				//tab.order;
				//tab.markedSelected;
				if(tab.checked == false && tab.markedSelected == true){ // if this tab was orriginally selected on load but is now unselected
					return_array.push(tab.catName); //add this tab to the deselected list so that we can remove it from the card
				}
			}
		
			return return_array;
		}

	// --- SNAG CUE CARD? ---
		private function saveMultiPlayList() {

			var selectedPlayGroups:Array = getSelectedCategories();// find out what has been deselected
			var deselectedPlayGroups:Array = getDeselectedPlayLists(); // find out what has been selected
		
			if(!checkForUndefined(_contentID)){
				
				// ADD CONTENT
				if(selectedPlayGroups.length > 0){
					addContentToContentGroups(null, selectedPlayGroups);
				}
				
				// REMOVE CONTENT
				if(deselectedPlayGroups.length > 0){
					removeContentFromContentGroups(null, deselectedPlayGroups);
				}
			}else {
				//!id =("~enter else statment");
				//trace("~cueCard.metaData.description: " + cueCard.metaData.description);
			//	trace("~cueCard.metaData.title: " + cueCard.metaData.title);
				//trace("~selectedPlayGroups[0]: " + selectedPlayGroups[0]);
	
				saveCueCard(cueCard.metaData.description, cueCard.metaData.title, selectedPlayGroups[0], function() {
					addContentToContentGroups(null, selectedPlayGroups);
				});
			}
	
		}

		private function handleXML(e:Event):void {
				
			
			var xml:XML = new XML(e.target.data);
		
			xml.ignoreWhitespace = true; 
			
			var playList:XMLList = xml.album;
			
			
		
			var recordCount:int = int(xml.attribute("recordCount"));
			
			
	
			if(recordCount > 0 ){
				//save_rb.selected = true;
				existing_rb.enabled = true;
			}else{
				//createListener.click();
				//recordCount = 0;
				//create_rb.selected = true;
				//save_rb.selected = false;
				//save_rb.enabled = false // make sure that you cant select this
				//dropDown.enabled = false
				existing_rb.enabled = false;
				new_rb.selected = true;
				enterCreateMode();
				return;
			}
			
			if(playList.length()){
		
		    // SORT THE ARRAY SO THAT IT IS ORDERED ACCORDING TO DISCRIPTION AND THEN ID
				
	
			
				var discr_array:Array = new Array();
				var ID_array:Array = new Array();
				// SPlit up the arrays
				for(var index1:Number = 0 ; index1 < playList.length() ; index1++){
				
					//trace("playList[index].desc: " + playList[index].desc);
					var disc = playList[index1].desc;
					if(checkForUndefined(disc) || isNaN(disc) ){ // if descr is unavalible
						ID_array.push(playList[index1]);  // add to ID_array because its disc  is invalid
					}else{ // otherwise we can sort by discription array (which is numerical)
						 discr_array.push(playList[index1]); // add it to the discription array (which will go first)
					}
				}

				discr_array.sort(sortDiscriptionFunction);
				ID_array.sort(sortIDFunction);
				//trace("");
				//trace("Sorted Desct_array");
				for(var index2:Number = 0 ; index2 < discr_array.length ; index2++){
					//trace(" " + discr_array[index2].attribute("name") + ": " + discr_array[index2].desc);
				}
				//trace("");
				//trace("Sorted ID_array");
				for(var index3:Number = 0 ; index3 < ID_array.length ; index3++){
					//trace(" " + ID_array[index3].attribute("name") + ": " + ID_array[index3].attribute("ID"));
				}
				
				//trace("");
				//trace("Combined array");
				
				
				playListArray = new Array();
				
				for(var index4:Number = 0 ; index4 < discr_array.length ; index4++){
					playListArray.push(discr_array[index4]);
				}
				for(var index5:Number = 0 ; index5 < ID_array.length ; index5++){
					playListArray.push(ID_array[index5]);
				}
				for(var index6:Number = 0 ; index6 < playListArray.length ; index6++){
					//trace(" " + playListArray[index6].attribute("name") + ": " + playListArray[index6].desc);
					
				}
	
			
	// ----------- END SORT -----------
			}else{
				playListArray = new Array();
				playListArray.push(playList);
			}
	
		
		
		// Apply Check Mark to saved playList
			//trace("~selected playlist record count: " + Number(sellectedPlayList.getContentGroup.recordCount));
		
			for(var index7:Number = 0 ; index7 < playListArray.length ; index7++){ // ALL PLAYLISTS
				for(var index8:Number = 0 ; index8 < checkedPlayLists.length() ; index8++){ // SELECTED PLAYLISTS
					if(checkedPlayLists[index8].groupID == playListArray[index7].attribute("ID")){ // if the two content ID's Match, than this should get a check mark
						
						playListArray[index7].selected = true;
					}
				}
				
			}

			// BUILD THE PLAYLIST
			tabs_array = new Array();
			paneSource = pane.content;
			
			
			for(var index:Number = 0 ; index < playListArray.length ; index++){
				
				//var tab:MovieClip = dropDown.content.attachMovie("MultiTab_MC","tab_" + index,dropDown.content.getNextHighestDepth(),{markedSelected:playListArray[index].selected});
				var tab:DynamicSprite = createButton(playListArray[index]);
				var prevTab:DynamicSprite = tabs_array[index-1];
				if(prevTab){
					tab.y = prevTab.y;
					tab.x = prevTab.x +130; 
					
					if(tab.x > 300){ //pane.width - 130){ // test to see if we need to move this down a line
						tab.x = 15;
						tab.y = prevTab.y + 21;
					}
				}else{
					tab.y = 40;
					tab.x = 15;
				}
				
				// MAKE THE RESET FUNCTION ALSO GO TO 50
				this.addChild(tab);
				//paneSource.addChild(tab);
				tabs_array.push(tab);
				
				
				
			}
			pane.visible = false;
			
		
			pane.update();
			pane.invalidate();
		
			setUpTabOrder();
		}
		
// UTILITY FUNCTIONS
		private function sortDiscriptionFunction(a, b):Number {
			var name1:Number = Number(a.desc);
			var name2:Number = Number(b.desc);
			
			if (name1<name2) {
			return -1;
			} else if (name1>name2) {
			return 1;
			} else {
			return 0;
			}
		}
		private function sortIDFunction(a, b):Number {
			var name1:String = a.attribute("ID")
			var name2:String = b.attribute("ID")
			if (name1<name2) {
			return -1;
			} else if (name1>name2) {
			return 1;
			} else {
			return 0;
			}
		}
		
		private function constrainText(_text:String,_length:Number):String{
			var returnText:String;
			if( _text.length > _length){
				returnText = _text.substr( 0, (_length-2)) + "...";
			}else{
				returnText = _text
			}
			return returnText;
		}
		/**
		* return true if variable is undefined
		*/
		private function checkForUndefined(check_obj){
			if(check_obj == undefined || check_obj == "undefined" || check_obj == null || check_obj == "null" || String(check_obj) == "" || String(check_obj) == " "){
				return true;
			}else{
				return false;
			}
		}
		/**
		* returns a sanatized version for url's and HTTPS's
		*/
		private function cleanText(text_str:String){
		
			text_str = findReplace( '"', '%22', text_str);
			text_str = findReplace( "'", "%27", text_str);
			text_str = findReplace( '&quot;', '%22', text_str);
			text_str = findReplace( '&apos;', '%27', text_str);
			text_str = findReplace( '&amp;', ' and ', text_str)

			return text_str;
			
		}
		private function findReplace( lookUp:String, replace:String, orig:String ){
	
			if( orig.indexOf( lookUp ) > -1 ){
			
				return orig.split( lookUp ).join( replace );
			}
			return orig;
		}
	
	}
}