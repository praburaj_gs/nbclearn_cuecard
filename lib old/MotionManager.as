﻿package 
{
	
	/**
	 * ...
	 * @author 
	 */
	public class  MotionManager
	{
		import flash.display.MovieClip;
		import flash.display.Sprite;
		import flash.utils.Timer;
		import flash.external.ExternalInterface
		import flash.events.MouseEvent;
		import flash.display.DisplayObject;
		import fl.transitions.Tween;
		import fl.transitions.easing.*;
		import fl.transitions.TweenEvent;
		

		private var prevX:Number;
		private var prevY:Number;
		private var directionX:int;
		private var directionY:int;
		private var canDragX:Boolean = false;
		private var canDragY:Boolean = false;
		private var canDrag:Boolean = false;
		public var preventDragging:Boolean = false;
		private var baseClip:DisplayObject;
		private var cueCard:AS3CueCard;
		
		private var vidX:Number;
		private var vidY:Number;
		private var vidWidth:Number;
		private var vidHeight:Number;
		
		
		public function MotionManager(_baseClip:DisplayObject,_cueCard:AS3CueCard) {
			
			baseClip = _baseClip;
			cueCard = _cueCard;
			
			//ExternalInterface.addCallback("updatePossitionX",updatePossitionX);
			//ExternalInterface.addCallback("updatePossitionY",updatePossitionY);
			
			//button.addEventListener(MouseEvent.MOUSE_DOWN,startDragging);
			//button.addEventListener(MouseEvent.MOUSE_UP, stopDragging);
			cueCard.always_mc.mainDragBox.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);
			
			cueCard.always_mc.mainDragBox.addEventListener(MouseEvent.ROLL_OVER, rollOver);
			cueCard.always_mc.mainDragBox.addEventListener(MouseEvent.ROLL_OUT, rollOut);
			cueCard.always_mc.gripper.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);
			
			cueCard.always_mc.gripper.addEventListener(MouseEvent.ROLL_OVER, rollOver);
			cueCard.always_mc.gripper.addEventListener(MouseEvent.ROLL_OUT, rollOut);
			
		}
		
		private function rollOver(e:MouseEvent) {
			if(!cueCard.imageMode){
				if(cueCard.videoPlayer){
					cueCard.videoPlayer.fadeInVideoControls();
				}
			}else {
				if (cueCard.imageViewer) {
					cueCard.imageViewer.fadeInImageControls();
				}
			}
		}
		private function rollOut(e:MouseEvent) {
			if(!cueCard.imageMode){
				if(cueCard.videoPlayer){
					cueCard.videoPlayer.fadeOutVideoControls();
				}
			}else {
				if (cueCard.imageViewer) {
					cueCard.imageViewer.fadeOutImageControls();
				}
			}
		}
		
	

		private function startDragging(e:MouseEvent) {
			if (preventDragging) {
				return;
			}
			cueCard.stage.addEventListener(MouseEvent.MOUSE_UP, stopDragging);
			
			vidX = cueCard.front_mc.x + cueCard.front_mc.panels_mask.x;
			vidY = cueCard.front_mc.y + cueCard.front_mc.panels_mask.y;
			vidWidth = cueCard.front_mc.content_mc.blackBackround.width;
			vidHeight = cueCard.front_mc.content_mc.blackBackround.height;
			
			
			cueCard.stage.addEventListener(MouseEvent.MOUSE_MOVE, videoFollowsMouse);
				
			
			cueCard.startDrag();
	
		}
		
		private function videoFollowsMouse(e:MouseEvent) {
			
			
			if(cueCard.videoPlayer.video_helper.stageVideoUsed){
				//cueCard.videoPlayer.resize(vidWidth, vidHeight);
				cueCard.videoPlayer.video_helper.x = cueCard.x + vidX;
				cueCard.videoPlayer.video_helper.y = cueCard.y + vidY;
				
				
			}
		}
		private function stopDragging(e:MouseEvent){
			cueCard.stage.removeEventListener(MouseEvent.MOUSE_UP, stopDragging);
			cueCard.stage.removeEventListener(MouseEvent.MOUSE_MOVE, videoFollowsMouse);
			
			cueCard.stopDrag();
			dontLoseCueCard();
			cueCard.videoPlayer.resize(cueCard.front_mc.content_mc.blackBackround.width, cueCard.front_mc.content_mc.blackBackround.height);
		}
		
		public function getEmptyScreenSpace():Object {
			var stageWidth:Number = cueCard.stage.stageWidth - 10;
			var stageHeight:Number = cueCard.stage.stageHeight -10;
			
			var cueCardHeight:Number = cueCard.shadowBackground.height;
			var cueCardWidth:Number = cueCard.shadowBackground.width;
			
			var emptyStageWidth:Number = stageWidth - cueCardWidth;
			var emptyStageHeight:Number = stageHeight - cueCardHeight;
			return { width:emptyStageWidth, height:emptyStageHeight };
		}
		
		public function keepOnScreen(changeX:Number, changeY:Number,time:Number):void {
			
			var animateTween:Tween;
			if (changeX != 0) {
				var stageWidth:Number = cueCard.stage.stageWidth - 10;
				var cueCardRight:Number = cueCard.x + cueCard.LEFT + cueCard.shadowBackground.width;
				if (cueCardRight + changeX > stageWidth) {
					var differenceX:Number = cueCardRight + changeX - stageWidth
					animateTween = animateMovieClip(cueCard, "x", cueCard.x - differenceX, time);
					
				}
			}
			if (changeY != 0) {
				var stageHeight:Number = cueCard.stage.stageHeight -10;
				var cueCardBottom:Number = cueCard.y  + cueCard.shadowBackground.height;
				if (cueCardBottom + changeY > stageHeight) {
					var differenceY:Number = cueCardBottom + changeY - stageHeight
					
					animateTween = animateMovieClip(cueCard, "y", cueCard.y - differenceY, time);
					
				}
			}
		
			if (cueCard.videoPlayer.video_helper.stageVideoUsed) {
				if(animateTween){
					animateTween.addEventListener(TweenEvent.MOTION_CHANGE, function(event:TweenEvent) {
				
						cueCard.videoPlayer.resize(cueCard.front_mc.content_mc.blackBackround.width, cueCard.front_mc.content_mc.blackBackround.height);
					});
				}
			}
			
		}
		
		
			// ANIMATE A SPECIFIC PROPETY OF A MOVIECLIP
		private function animateMovieClip(target_mc:Sprite, property:String ,target:Number,time:Number):Tween {
			var animateTween:Tween;
			animateTween = new Tween(target_mc, property, Regular.easeInOut, target_mc[property], target, time, true);
			return animateTween;
		}
		
	
		
		public function dontLoseCueCard(){
		
			var cueCardHeight:Number = cueCard.shadowBackground.height;
			var cueCardWidth:Number = cueCard.shadowBackground.width;
			var cueCardBottom:Number = cueCard.y  + cueCardHeight;
			
			var animateTween:Tween;
			var cueCardRight:Number = cueCard.x + cueCard.LEFT + cueCardWidth;
			
			
			var stageHeight:Number = cueCard.stage.stageHeight;
			var stageWidth:Number = cueCard.stage.stageWidth;
			var targetX:Number = (stageWidth - cueCard.LEFT) - cueCardWidth;
			var targetY:Number = stageHeight - (cueCardHeight + 10);
			
			//trace("! DON'T LOSE CARD");
			if (cueCardBottom+10 > stageHeight) {
			animateTween = animateMovieClip(cueCard, "y", targetY, 1);
				
			}
			
			if (cueCardRight > stageWidth) {
				animateTween = animateMovieClip(cueCard, "x", targetX, 1);
				
			}
			
			if (cueCard.x + cueCard.LEFT < -40) {
				animateTween = animateMovieClip(cueCard, "x", -cueCard.LEFT, 1);
				
				
			}
			if (cueCard.y < -40) {
				animateTween = animateMovieClip(cueCard, "y", 0, 1);
				
			}
			
			if (cueCard.videoPlayer.video_helper.stageVideoUsed) {
				if(animateTween){
					animateTween.addEventListener(TweenEvent.MOTION_CHANGE, function(event:TweenEvent) {
				
						cueCard.videoPlayer.resize(cueCard.front_mc.content_mc.blackBackround.width, cueCard.front_mc.content_mc.blackBackround.height);
					});
				}
			}
			

		}
		
	}
}