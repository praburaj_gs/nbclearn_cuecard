﻿package 
{
	import fl.containers.ScrollPane;
	import fl.transitions.Tween;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.ui.ContextMenuItem;
	import flash.utils.Timer;
	
	
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;


	
	
	/**
	 * ...
	 * @author Ari
	 */
	public class DownloadBar extends MovieClip 
	{
		
		private var _cueCard:AS3CueCard;
		
		private var resizer:Resizer;
		public var tween:Tween;
		public var open:Boolean = false;
		
		
		private var contentStartY:Number;
		private var buttonStartY:Number;
		private var autoCancelCardID:String;
	//	private var content_mc:MovieClip;
	
		private var openY:Number; 
		private var downloads_array:Array;
		private var timer:Timer;
		private var hasMoved:Boolean = false;
		private var pane:ScrollPane;
		private var paneBottom:Number = -37;
		private var paneInitialWidth:Number;
		private var format:TextFormat;
		
		private var clear_btn:Sprite;
		private var clear_txt:TextField;
		private var cancelTimer:Timer;
		
		public function DownloadBar() {
			contentStartY = this.content_mc.y;
			buttonStartY = this.downloadDrag_btn.y;
			content_mc.spinner_mc.visible = false;
			openY = downloadDrag_btn.y + 200;
			timer = new Timer(650,1);
			timer.addEventListener(TimerEvent.TIMER,motionFailSafe);
			this.downloadDrag_btn.addEventListener(MouseEvent.MOUSE_DOWN, startDownloadExpand);
			this.downloadDrag_btn.addEventListener(MouseEvent.CLICK, toggleOpenClose);
			
		
			// make the mouse hand visible on roll over
			
			
			downloadDrag_btn.buttonMode = true;
			downloadDrag_btn.useHandCursor = true;
			downloads_array = new Array();
			
			pane = content_mc.pane_mc;
			paneInitialWidth = pane.width;
			clear_btn = new Sprite();
			clear_txt = new TextField();
			
			
			clear_txt.autoSize = TextFieldAutoSize.LEFT;
			clear_txt.textColor = 0xf36f21;
			clear_txt.text = "Clear All";
			clear_txt.selectable = false;
			format = new TextFormat();
			format.size = 11;
			
			clear_txt.setTextFormat(format);
			
			
			clear_btn.addChild(clear_txt);
			clear_btn.y = paneBottom - 2;
			clear_btn.x = pane.x + paneInitialWidth - clear_txt.width - 2;
			clear_btn.buttonMode = true;
			clear_btn.useHandCursor = true;
			clear_btn.addEventListener(MouseEvent.CLICK, clearAllInactiveDownloads);
			clear_btn.addEventListener(MouseEvent.ROLL_OVER, underLineClearBtn);
			clear_btn.addEventListener(MouseEvent.ROLL_OUT, removeUnderLineClearBtn);
			content_mc.addChild(clear_btn);
			
		}
		
		private function underLineClearBtn(e:MouseEvent) {
			format.underline = true;
			clear_txt.setTextFormat(format);
		}
		private function removeUnderLineClearBtn(e:MouseEvent) {
			format.underline = false;
			clear_txt.setTextFormat(format);
		}

		// DOWNLOAD LISTENERS
		public function startDownload(event:Object):void {
			//trace("Downloading " + event.cueCard.ThumbnailUrl + " : "  + event.cueCard);
			
			//_cueCard.notifyUser();
			
			var hasCueCard = _cueCard.AODPlayerLibrary.aodPlayerLib.getCueCard(event.id);
			
			if(hasCueCard == null || hasCueCard == undefined){
			
				content_mc.spinner_mc.visible = true;
				createDownloader(event);
				openDownloadBar();
				
			}else {
				//_cueCard.AODPlayerLibrary.aodPlayerLib.cancelDownload(event.id);
				
				_cueCard.conformation.confirm("This video has already been downloaded to your library.", "Open Library", openLibrary, false);
				autoCancelCardID = event.id
			//	trace("CALL THE CANCEL FUNTION ON THIS CARD: " + autoCancelCardID);
				cancelTimer = new Timer(50, 1);
				cancelTimer.addEventListener(TimerEvent.TIMER_COMPLETE, autoCancel);
				cancelTimer.start();
					
				
				checkDownloads();
			}
			
			
		}
		private function autoCancel(obj = null):void {
			//trace("--------Cancel This Download-------");
			_cueCard.AODPlayerLibrary.aodPlayerLib.cancelDownload(autoCancelCardID);
			
		}
		private function openLibrary():void {
			
			_cueCard.menu.selectCategory("Library");
		}
		
		public function downloadProgress(event:Object):void {
			//trace("Push Progress");
			var download_thumb:DownloadThumb = getDownloader(event.id);
			if(download_thumb){
				download_thumb.pushProgress(event);
			}
			
		}
		
		public function downloadComplete(event:Object):void {
			getDownloader(event.id).downloadComplete(event);
			//trace("-------- DOWNLOAD COMPLETE-------");
			//trace("event.id = " + event.id);
			//_cueCard.playList_mc.addThumbnail(_cueCard.AODPlayerLibrary.aodPlayerLib.getCueCard(event.id));
			
		}
		
		
		
		
		// Go through all the on going downloads to return the requested one
		private function getDownloader(asset_id:String):DownloadThumb {
			
			var thumb:DownloadThumb ;
			
			for ( var index:Number = 0 ; index < downloads_array.length; index++) {
				
				if(asset_id == downloads_array[index].id){
						thumb = downloads_array[index];
						break;
					
				}
			}
			
			return thumb;
		}
	// REMOVE ALL INACTIVE AND CANCELED DOWNLOADS
		private function clearAllInactiveDownloads(e:MouseEvent = null) {
			
			
			for ( var index:Number = (downloads_array.length-1) ; index > -1; index--) {
				var thumb:DownloadThumb = downloads_array[index];
				
				if (thumb.getState() == false || thumb.canceled == true) {
					thumb.removeThumb();
				}
			}
		}
	
		
		private function createDownloader(event:Object):DownloadThumb {
			
			var thumb:DownloadThumb = new DownloadThumb(event, event.cueCard);
			
			downloads_array.push(thumb);
			thumb.addListener(DownloadThumb.INIT, thumbCreated);
			thumb.addListener(DownloadThumb.LOADED, finishedLoading);
			thumb.addListener(DownloadThumb.CLICK, playVideo);
			thumb.addListener(DownloadThumb.REMOVED, thumbRemoved);
			thumb.addListener(DownloadThumb.CANCELED, downloadCanceled);
			
			
			return thumb;
			
		}
	
		private function thumbCreated(event:Object) {
			//event.thumb.setState(true);
				
			var pane_contents:MovieClip = pane.content as MovieClip;
		
			pane_contents.addChild(event.thumb) as MovieClip;
			arrangeContent();
			checkDownloads();
			//openDownloadBar(true);
		}
		
		private function thumbRemoved(event:Object) {
			var pane_contents:MovieClip = pane.content as MovieClip;
			for (var index:uint = 0 ; index < downloads_array.length ; index++) {
				if (downloads_array[index] == event.thumb) {
					downloads_array.splice(index, 1);
				}
			}
			pane_contents.removeChild(event.thumb);
			arrangeContent();
			
		}
		private function playVideo(event:Object) {
			event.id;
			event.thumb;
			_cueCard.loadCueCard(event.id);
			
		}
		
		private function downloadCanceled(event:Object) {
			checkDownloads();
			_cueCard.AODPlayerLibrary.aodPlayerLib.cancelDownload(event.id);
			
		}
		
		
		private function finishedLoading(event:Object) {
			checkDownloads();
		}
		private function checkDownloads():void {
			
			var activeDownloads:Boolean = false;
			for ( var index:Number = 0 ; index < downloads_array.length; index++) {
				var thumb:DownloadThumb = downloads_array[index];
				
				if (thumb.getState() == true && thumb.canceled != true) {
					activeDownloads = true;
				}
			}
			if (activeDownloads) {
				content_mc.spinner_mc.visible = true;
			}else {
				content_mc.spinner_mc.visible = false;
			}
			
		}
		
		public function arrangeContent(object = null):void {
		
			
			for ( var index:Number = downloads_array.length-1 ; index > -1; index--) {
				var thumb:DownloadThumb = downloads_array[index];
				var prevThumb:DownloadThumb = downloads_array[index + 1]
				if (prevThumb) {
					thumb.x = prevThumb.x;
					thumb.y = prevThumb.y + prevThumb.getHeight() + 12;
				}else {
					thumb.x = 5; //thumb.x = -146;
					thumb.y = 25;//pane.height-thumb.getHeight(); //thumb.y = -82;
				}
			}
			
			pane.update();
			pane.invalidate();
			
			
			
		}
		
		public function set cueCard(_cueCard) {
			this._cueCard = _cueCard;
			initResizer();
		}
		
		private function toggleOpenClose(e:MouseEvent) {
			if (hasMoved) {
				return;
			}
			if (open) {
				closeDownloadBar(true);
			}else {
				openDownloadBar(true);
			}
		}
		
		private function initResizer() {
			if (!resizer) {
				resizer = new Resizer(this.downloadDrag_btn);
				
				resizer.delay = 50;
				resizer.addResizableObject(this.content_mc, Resizer.VERTICLE);
			    resizer.addResizableObject(this._cueCard.shadowBackground.main_bg, Resizer.HEIGHT);
				resizer.addResizableObject(this._cueCard.shadowBackground.down_tab, Resizer.VERTICLE);// for download draw
				resizer.addResizableObject(this._cueCard.background_mc.whiteBackround, Resizer.HEIGHT);
				
				
				resizer.constrainTo(downloadDrag_btn.x, downloadDrag_btn.x, buttonStartY, openY);
				resizer.addEventListener(Resizer.RESIZING, onDownloadBarResize);
				resizer.addEventListener(Resizer.RESIZING, _cueCard.updateStageDimensions);
				
				//resizer.addEventListener(Resizer.RESIZE_COMPLETE, onCardResizeComplete);
				//resizer.addEventListener(Resizer.RESET_COMPLETE, onCardReset);
				resizer.startResize();
				resizer.stopResize();
				
				
								
			}
		}
		
		private function onDownloadBarResize(e:Object){
			if (e.changeX != 0 || e.changeY != 0){
				hasMoved = true
			}
			var paneHeight:Number = this.content_mc.y - contentStartY;
			
			if(paneHeight > 27){
				pane.setSize(pane.width, paneHeight)
				pane.y = paneBottom - paneHeight;
			}
			
			arrangeContent();
		}
		
		private function startDownloadExpand(e:Event):void { // SETUP RESIZE
			hasMoved = false;
			content_mc.openClose_arrow.rotation = 180;
			
			
			resizer.startResize();		
			stage.addEventListener(MouseEvent.MOUSE_UP, stopDownloadExpand);
			
			open = true;
		
		}
		
		private function stopDownloadExpand(e:Event) { // STOP RESIZE
			
			resizer.stopResize();
			stage.removeEventListener(MouseEvent.MOUSE_UP, stopDownloadExpand);
			
			if (downloadDrag_btn.y == buttonStartY) {
				open = false;
				content_mc.openClose_arrow.rotation = 0;
			
			}
			
			
		}
		
	// Slide the DownloadBar Open
		public function openDownloadBar(internalCall:Boolean = false){
			//if (internalCall == true) {
			// set the size of the pane to be the full dimensions
				var paneHeight:Number = openY - buttonStartY;
				pane.setSize(pane.width, paneHeight);
				pane.y = paneBottom - paneHeight;
				arrangeContent();
			
			
				resizer.changed = true;
				downloadDrag_btn.__y = downloadDrag_btn.y; // save the possition before the tween
				var closeTween:Tween = animateMovieClip(this.downloadDrag_btn, "y", openY, .64);
				closeTween.addEventListener(TweenEvent.MOTION_FINISH, barHasOpend);
				closeTween.addEventListener(TweenEvent.MOTION_CHANGE, whileBarIsMoving);
			//timer.stop();
				timer.start();
					content_mc.openClose_arrow.rotation = 180;
				
			//}
		}
		
		private function motionFailSafe(e:TimerEvent) {
			downloadDrag_btn.__y = downloadDrag_btn.y; // reset for the next change
			downloadDrag_btn.y = openY;
			whileBarIsMoving();
			timer.stop();
		}
		
		private function barHasOpend(e:TweenEvent) {
			open = true;
		
			
			
		}
		
	// Slide the DownloadBar Closed
		public function closeDownloadBar(internalCall:Boolean = false) {
			
			if(internalCall == false) {
				if (resizer && resizer.changed) { // only perform this if the resizer has been initilized
					resizer.resetSizeFade(); // if the call is comming from outside the download bar
					content_mc.openClose_arrow.rotation = 0;
					open = false;
				}
			}else{ // this is comming from the close button
				//animateMovieClip(this.content_mc, "y", contentStartY, .64);
				downloadDrag_btn.__y = downloadDrag_btn.y; // save the possition before the tween
				var closeTween:Tween = animateMovieClip(this.downloadDrag_btn, "y", buttonStartY, .64);
				closeTween.addEventListener(TweenEvent.MOTION_FINISH, barHasClosed);
				closeTween.addEventListener(TweenEvent.MOTION_CHANGE, whileBarIsMoving);
			}
			timer.stop();
			//timer.start();
		}
		
		private function whileBarIsMoving(e:TweenEvent = null) {
			var changeY:Number = downloadDrag_btn.y - downloadDrag_btn.__y
			downloadDrag_btn.__y = downloadDrag_btn.y; // reset for the next change
			
			this.content_mc.y += changeY;
			_cueCard.shadowBackground.main_bg.height += changeY;
			_cueCard.shadowBackground.down_tab.y += changeY;
			_cueCard.background_mc.whiteBackround.height += changeY;
			_cueCard.updateStageDimensions({changeX:0,changeY:changeY})
		
		}
		
		private function barHasClosed(e:TweenEvent) {
			content_mc.y = contentStartY;
			downloadDrag_btn.y = buttonStartY;
			open = false;
			content_mc.openClose_arrow.rotation = 0;
		}
		
		public function resetContent() {
			for ( var index:Number = 0 ; index < downloads_array.length; index++) {
				var thumb:DownloadThumb = downloads_array[index];
				thumb.resetContent();
			}
			pane.width = paneInitialWidth;
			
			
		}
		
		public function masterDimensionResize(totalChangeX:Number, totalChangeY:Number) {
			
			// set the pane size
			
			pane.setSize(pane.width + totalChangeX, pane.height);
			
			// Set the width of the thumbnails
			for ( var index:Number = 0 ; index < downloads_array.length; index++) {
				var thumb:DownloadThumb = downloads_array[index];
				thumb.resize(totalChangeX);
				
			}
			//resizer.updateBaseDimension(_cueCard.shadowBackground, Resizer.HEIGHT, totalChangeY); // tell the resizer that the base dimension has changed
			//resizer.updateBaseDimension(_cueCard.background_mc.whiteBackround, Resizer.HEIGHT, totalChangeY);// tell the resizer that the base dimension has changed
		}
		
		
		
		// ANIMATE A SPECIFIC PROPETY OF A MOVIECLIP
		private function animateMovieClip(target_mc:Sprite, property:String ,target:Number,time:Number):Tween {
			var animateTween:Tween;
			animateTween = new Tween(target_mc, property, Regular.easeInOut, target_mc[property], target, time, true);
			return animateTween;
		}

	}

}