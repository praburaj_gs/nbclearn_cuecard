﻿package 
{
	
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	
	/**
	 * ...
	 * @author Ari
	 */
	public class Resizer 
	{
		private var timer:Timer;
		private var resetTimer:Timer;
		private var resize_array:Array;
		private var move_array:Array;
		private var resetTween:Tween;
		public var changed:Boolean = false;
		private var constrain:Boolean = false;
		
		private var dragStartX:Number;
		private var dragStartY:Number;
		private var offsetX:Number;
		private var offsetY:Number;
		private var oldX:Number;
		private var oldY:Number;
		private var currentChangeX:Number;
		private var currentChangeY:Number;
		private var totalChangeX:Number = 0;
		private var totalChangeY:Number = 0;
		
	// CONSTRAIN POSSITIONS
		private var left:Number;
		private var right:Number;
		private var top:Number;
		private var bottom:Number;
		
		private var _drag_mc:Sprite;
	// CONSTANTS POSSITIONS
		public static const TOP_LEFT:String = "top_left";
		public static const TOP_RIGHT:String = "top_right";
		public static const BOTTOM_LEFT:String = "top_left";
		public static const BOTTOM_RIGHT:String = "top_right";
		
	// CONSTANTS - DIMENSIONS
		public static const WIDTH:String = "width";
		public static const HEIGHT:String = "height";
		public static const WIDTH_AND_HEIGHT:String = "width_and_height";
		public static const X_AND_Y:String = "Verticle and horizontal";
		public static const VERTICLE:String = "verticle";
		public static const HORIZONTAL:String = "horizontal";
	// CONSTENTS - EVENTS
		public static const RESIZING:String = "resizing";
	
		public static const RESIZE_COMPLETE:String = "resiz_compleate";
		public static const RESET_COMPLETE:String = "reset_compleate";
		public static const RESET_FADE:String = "resizing_fade";
	
		
		// event arrays
		private var resizeCompleteCallBack:Function;
		private var resizeEvent_array:Array;
		private var resetFadeEvent_array:Array;
		private var resizeCompleteEvent_array:Array;
		private var resetCompleteEvent_array:Array;
		
		
		
		
		private var _left:Number;
		private var _right:Number;
		private var _top:Number;
		private var _bottom:Number;
		
		private var _delay:int = 20;
		public var isBaseSize:Boolean = true;
		
	// ---== CONSTRUCTOR FUNCTION ==---
		public function Resizer(drag_object:Sprite) {
			_drag_mc = drag_object;
			
			
			timer = new Timer(_delay);
			timer.addEventListener(TimerEvent.TIMER, resize);
			resetTimer = new Timer(_delay);
			resetTimer.addEventListener(TimerEvent.TIMER, resetting);
			
			resize_array = new Array;
			dragStartX = _drag_mc.x;
			dragStartY = _drag_mc.y;
			
			resizeEvent_array = new Array();
			resetFadeEvent_array = new Array();
			resizeCompleteEvent_array = new Array;
			resetCompleteEvent_array = new Array();
			
			
		}
		
		public function constrainTo(left:Number, right:Number, top:Number, bottom:Number) {
			this.left = left;
			this.right = right;
			this.top = top;
			this.bottom = bottom;
			constrain = true;
		}
		
		
		
		
		public function set dragObject(_drag_mc:Sprite) {
			this._drag_mc = _drag_mc;
		}
		public function set delay(_dealy:int) {
			this._delay = _delay;
			timer.delay = _delay;
		}
		
		
		public function addResizableObject(object:Sprite,resizableDimension:String = Resizer.WIDTH_AND_HEIGHT):void {
			
			
			resize_array.push( { object:object, dimension:resizableDimension ,_x:object.x, _y:object.y, _width:object.width, _height:object.height } );
			
		}
		
		
		public function updateBaseDimension(object:Sprite, dimensionType:String, changeBaseDimension:Number){
			var resizerObject:Object;
			for (var index:int = 0 ; index < resize_array.length ; index++) {
				if(object == resize_array[index].object){
					resizerObject = resize_array[index];
					break
				}
			}
			
			// Determin which dymension we are using and reset the base value
			if(dimensionType == Resizer.HEIGHT){
					resizerObject._height += changeBaseDimension;
				}
			if(dimensionType == Resizer.WIDTH){
				resizerObject._width += changeBaseDimension;
					
			}
				
				
			if(dimensionType == Resizer.VERTICLE){
				resizerObject._y += changeBaseDimension;
			}
			if(dimensionType == Resizer.HORIZONTAL){
				resizerObject._x += changeBaseDimension;
			}
		
		}
		
		public function startResize():void {
			changed = true;
			offsetX = _drag_mc.mouseX
			offsetY = _drag_mc.mouseY;
			oldX = _drag_mc.parent.mouseX;
			oldY = _drag_mc.parent.mouseY;
			currentChangeX = 0;
			currentChangeY = 0;
			timer.start();
			isBaseSize = false;
		}
		public function stopResize():void {
			timer.stop();
			totalChangeX += currentChangeX;
			totalChangeY += currentChangeY;
			dispatchEvent(Resizer.RESIZE_COMPLETE, { totalChangeX:currentChangeX, totalChangeY:currentChangeY } );
			//trace("DRAG MC X:" + _drag_mc.x + " Y:" +_drag_mc.y);
			
			
		}
		
		public function getTotalChange():Object {
			return {x:totalChangeX,y:totalChangeY}
		}
		
		
		
	// EVENT IS FIRED RAPPIDLY DURRING THE USER RESIZE
		public function resize(e:TimerEvent = null, _width:Number = 0, _height:Number = 0):void {
			
			var changeX:Number;
			var changeY:Number;
			
			
			
			if(_width == 0 && _height == 0){
			
				changeX = _drag_mc.parent.mouseX - oldX;
				changeY = _drag_mc.parent.mouseY - oldY;
			}else {
				
				//DRAG MC X:448 = 640
				//Y:520 = 480 

				
			//	trace("CHANGE TO: " + _width + "/" + _height);
			//	trace("_drag_mc x:" + Math.round(_drag_mc.x) + " y:" + Math.round(_drag_mc.y));
				
				
				changeX = (_width - _drag_mc.x - 192);
				changeY = (_height - _drag_mc.y + 40);
				
			//	trace("change: X:" + Math.round(changeX) + " Y:" + Math.round(changeY));
			}
			
			
			oldX = _drag_mc.parent.mouseX
			oldY = _drag_mc.parent.mouseY
			if(!constrain){
				_drag_mc.x += changeX;
				_drag_mc.y += changeY;
			}else {
				
				// Constrain Width
				if (_drag_mc.x + changeX > right) {
					changeX = right - _drag_mc.x;
				}
				if (_drag_mc.x + changeX < left) {
					changeX = left - _drag_mc.x;
				}
				
				// Constrain Height
				if (_drag_mc.y + changeY > bottom) {
					changeY = bottom - _drag_mc.y;
				}
				if (_drag_mc.y + changeY < top) {
					changeY = top - _drag_mc.y;
				}
				// update the drag property
				_drag_mc.x += changeX;
				_drag_mc.y += changeY;
			}
			currentChangeX += changeX;
			currentChangeY += changeY;
			
			
			
//			Go through all the objects in the resize array and resize them
			for (var index:int = 0 ; index < resize_array.length ; index++) {
				var item_mc:Sprite = resize_array[index].object;  // isolate the object
				var dimension:String = resize_array[index].dimension; // get the dimension
				if(dimension == Resizer.WIDTH_AND_HEIGHT){
					item_mc.width  += changeX;
					item_mc.height += changeY;
				}
				
				if(dimension == Resizer.HEIGHT){
					item_mc.height += changeY;
				}
				
				if(dimension == Resizer.WIDTH){
					item_mc.width  += changeX;
				}
				
				
				if(dimension == Resizer.VERTICLE){
					item_mc.y  += changeY;
				}
				
				if(dimension == Resizer.HORIZONTAL){
					item_mc.x  += changeX;
				}
				if (dimension == Resizer.X_AND_Y) {
					item_mc.x  += changeX;
					item_mc.y  += changeY;
				}
				
			}

			dispatchEvent(Resizer.RESIZING,{changeX:changeX,changeY:changeY});
			
		}
		
		public function resetSize() {
			if (changed) { // only call reset if somthing has changed
			
			}
			
		}
		
	// Slowly reset the sizees of all the objects to their original sizes
		public function resetSizeFade(time:Number = .65) {
			//if(changed){ // only call reset if somthing has changed
			
				for (var index:int = 0 ; index < resize_array.length ; index++) {
					
					var resizerObject:Object = resize_array[index];
					var item_mc:Sprite = resizerObject.object;  // isolate the object
					var dimension:String = resizerObject.dimension; // get the dimension
					
					if(dimension == Resizer.WIDTH_AND_HEIGHT){ // adjust the width and height properties
						if(item_mc.height != resizerObject._height){ // prevent unnessesary tweens
							resizerObject.tween = animateMovieClip(item_mc, "height", resizerObject._height, time); 
						}
						if(item_mc.width != resizerObject._width){// prevent unnessesary tweens
							resizerObject.tween = animateMovieClip(item_mc, "width", resizerObject._width, time); 
						}
						
					}
					
					if(dimension == Resizer.WIDTH){ // adjust width property
						if(item_mc.width != resizerObject._width){// prevent unnessesary tweens
							resizerObject.tween = animateMovieClip(item_mc, "width", resizerObject._width, time); 
						}
					}
					
					if(dimension == Resizer.HEIGHT){ // adjust height property
						if(item_mc.height != resizerObject._height){ // prevent unnessesary tweens
							resizerObject.tween = animateMovieClip(item_mc, "height", resizerObject._height, time); 
						}
					}
					
					if (dimension == Resizer.VERTICLE) {
						if(item_mc.y != resizerObject._y){// prevent unnessesary tweens
							resizerObject.tween = animateMovieClip(item_mc, "y", resizerObject._y, time); 
						}
					}
					if (dimension == Resizer.HORIZONTAL) {
						if(item_mc.x != resizerObject._x){// prevent unnessesary tweens
							resizerObject.tween = animateMovieClip(item_mc, "x", resizerObject._x, time);
						}
					}
					if (dimension == Resizer.X_AND_Y) {
						if(item_mc.x != resizerObject._x){// prevent unnessesary tweens
							resizerObject.tween = animateMovieClip(item_mc, "x", resizerObject._x, time);
							resizerObject.tween = animateMovieClip(item_mc, "y", resizerObject._y, time);
						}
					}
					
					if(resizerObject.tween){
						resizerObject.tween.addEventListener(TweenEvent.MOTION_FINISH, resetComplete);
					}
					
					//	trace(item_mc.name +".width = " + item_mc.width +"; " + item_mc.name +".height = " + item_mc.height +"; " + item_mc.name + ".x = " + item_mc.x + "; " + item_mc.name + ".y = " + item_mc.y + ";" );
				
					
				}
				if(_drag_mc.x != dragStartX){
					animateMovieClip(_drag_mc, "x", dragStartX, time);	
				}
				if(_drag_mc.y != dragStartY){
					var resetTween:Tween = animateMovieClip(_drag_mc, "y", dragStartY, time); 
					resetTween.addEventListener(TweenEvent.MOTION_FINISH, resetComplete);
				}
				
				
				
				changed = false;
				resetTimer.start();
		}
			
			
		private function resetting(e:TimerEvent) {
			dispatchEvent(Resizer.RESET_FADE);
		}

		
		
		// ANIMATE A SPECIFIC PROPETY OF A MOVIECLIP
		private function animateMovieClip(target_mc:Sprite, property:String ,target:Number,time:Number):Tween {
			var animateTween:Tween;
			animateTween = new Tween(target_mc, property, Regular.easeInOut, target_mc[property], target, time, true);
			return animateTween;
		}
			
		
		
		
//------------- EVENT HANDLING -------------


		private function resetComplete(e:TweenEvent) { // when the reset is complete
			
			//trace("Target: " + e.target);
			//trace("currentTarget: " + e.currentTarget);
			//trace("");
		// Hard reset all the properties // to avoid errors
		
			/*for (var index:int = 0 ; index < resize_array.length ; index++) { 
				
				resize_array[index].object["width"] = resize_array[index]._width; 
				resize_array[index].object["height"] = resize_array[index]._height; 
				resize_array[index].object["x"] = resize_array[index]._x; 
				resize_array[index].object["y"] = resize_array[index]._y; 
			}*/
			// simple code was causing errors (replaced by the more complex propram that checks each parameters
			
			// This code checks to determin the dimension property and reset it to its base component
			
			for (var index:int = 0 ; index < resize_array.length ; index++) {
				
					var resizerObject:Object = resize_array[index];
					var item_mc:Sprite = resizerObject.object;  // isolate the object
					var dimension:String = resizerObject.dimension; // get the dimension
					
					if(dimension == Resizer.WIDTH_AND_HEIGHT){ // adjust the width and height properties
						if(item_mc.height != resizerObject._height){ // prevent unnessesary tweens
							item_mc.height = resizerObject._height
						}
						if(item_mc.width != resizerObject._width){// prevent unnessesary tweens
							item_mc.width = resizerObject._width
						}
						
					}
					
					if(dimension == Resizer.WIDTH){ // adjust width property
						if(item_mc.width != resizerObject._width){// prevent unnessesary tweens
							item_mc.width != resizerObject._width
						}
					}
					
					if(dimension == Resizer.HEIGHT){ // adjust height property
						if(item_mc.height != resizerObject._height){ // prevent unnessesary tweens
							item_mc.height = resizerObject._height
						}
					}
					
					if (dimension == Resizer.VERTICLE) {
						if(item_mc.y != resizerObject._y){// prevent unnessesary tweens
							item_mc.y != resizerObject._y
						}
					}
					if (dimension == Resizer.HORIZONTAL) {
						if(item_mc.x != resizerObject._x){// prevent unnessesary tweens
							item_mc.x != resizerObject._x
						}
					}
					if (dimension == Resizer.X_AND_Y) {
						if(item_mc.x != resizerObject._x){// prevent unnessesary tweens
							item_mc.x != resizerObject._x
						}
						if(item_mc.y != resizerObject._y){// prevent unnessesary tweens
							item_mc.y != resizerObject._y;
						}
					}
			}
			

			
			_drag_mc["x"] =  dragStartX;
			_drag_mc["y"] =  dragStartY;
			totalChangeX = 0;
			totalChangeY = 0;
			resetTimer.stop();
			dispatchEvent(Resizer.RESET_COMPLETE); // let any funciton know that there are listeners
			
			isBaseSize = true;
		}
		
		
		

	// Registering for an event with the resizer
		public function addEventListener(eventType:String, callBackFunction:Function) {
			
			switch(eventType) { 
				
				case Resizer.RESET_FADE:
					resetFadeEvent_array.push(callBackFunction);
				break
				case Resizer.RESIZING:
				
				resizeEvent_array.push(callBackFunction);
					
				break
				
				case Resizer.RESIZE_COMPLETE:
					resizeCompleteEvent_array.push(callBackFunction);
				break;
				
				case Resizer.RESET_COMPLETE:
					resetCompleteEvent_array.push(callBackFunction);
				break;
				
			
			}
			
		}
		
	// EVENT HANDLING FOR LISTENING OBJECTS
		
		private function dispatchEvent(eventType:String,returnObject:Object = null) {
			switch(eventType) { 
				
				
				
				case Resizer.RESET_FADE:
				
					for (var index0:int = 0 ; index0 <  resetFadeEvent_array.length ; index0++) {
						resetFadeEvent_array[index0](returnObject);
					}
				break
				
				case Resizer.RESIZING:
				
					for (var index:int = 0 ; index <  resizeEvent_array.length ; index++) {
						resizeEvent_array[index](returnObject);
					}
				break
				
				case Resizer.RESIZE_COMPLETE:
					//trace("DISPATCH: RESIZE COMPLETE");
					for (var index2:int = 0 ; index2 <  resizeCompleteEvent_array.length ; index2++) {
						resizeCompleteEvent_array[index2](returnObject);
					}
				break;
				
				case Resizer.RESET_COMPLETE:
					//trace("DISPATCH: RESIZE COMPLETE");
					for (var index3:int = 0 ; index3 <  resetCompleteEvent_array.length ; index3++) {
						resetCompleteEvent_array[index3](returnObject);
					}
				break;
			}
		}
	}	
}