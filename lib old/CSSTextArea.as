package  
{
	import fl.controls.TextArea;
	
	/**
	 * ...
	 * @author Ari Oshinsky
	 */
	public class CSSTextArea extends TextArea 
	{
		
		public function CSSTextArea() 
		{
			super();
			
		}
		
		override protected function drawTextFormat():void {
            if(!this.textField.styleSheet) super.drawTextFormat();
            else {
                setEmbedFont();
                if (_html) textField.htmlText = _savedHTML;
				
            }
        }
		
	}

}

 
 