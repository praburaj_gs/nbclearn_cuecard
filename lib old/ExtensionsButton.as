﻿package 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import Colors;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author 
	 */
	public class ExtensionsButton extends Sprite 
	{
		
		public var labelText:String;
		public var index:int;
		private var clickFunction:Function;
		
		private var defaultTextFmt:TextFormat;
		private var selectedTextFmt:TextFormat;
		var dissabledColor:uint = 0x8D949C //0x69707A
		var enabledColor:uint = 0x494949
		private var state:String;
		
		public var selected:Boolean = false;
		
		
		public static const CLICK:String = "clicked";
	
		private var clickEvent_array:Array;
		
		
		public function ExtensionsButton(_label:String, _clickFunction:Function = null) {
			
			clickEvent_array = new Array();
			labelText = _label;
			clickFunction = _clickFunction;
			bg.visible = false;
			bgSelected.visible = false;
			initEvents();
			setUpText();
		}
		private function setUpText() {
			button_txt.autoSize = TextFieldAutoSize.LEFT;
			button_txt.text = labelText;
			bg.width = 6 + button_txt.width + 3;
			bgSelected.width = bg.width;
			hit_box.width = bg.width;
			defaultTextFmt = button_txt.getTextFormat();
			selectedTextFmt = button_txt.getTextFormat();
			selectedTextFmt.color = 0xFFFFFF;
			//trace("~labelText: " + labelText);
		}
		
		public function setColor(_state:String) {
			state = _state;
			if (_state == "disabled") {
				defaultTextFmt.color = dissabledColor;
				//Colors.changeTint(bg, 0xffffff, .2);
				selectedTextFmt.color = 0xDDDDDD
				button_txt.setTextFormat(defaultTextFmt);
				
			}
		}
	
		private function initEvents() {
			hit_box.buttonMode = true;
			hit_box.useHandCursor = true;
			hit_box.addEventListener(MouseEvent.ROLL_OVER, rollOverState);
			hit_box.addEventListener(MouseEvent.ROLL_OUT, rollOutState);
			hit_box.addEventListener(MouseEvent.CLICK, clickEvent);
		}
		
		private function rollOverState(e:MouseEvent) {
			if (!selected) {
				if (state != "disabled") {
					bg.visible = true;
					button_txt.setTextFormat(selectedTextFmt);
				}
			}
		}
		private function rollOutState(e:MouseEvent) {
			if (!selected) {
				if (state != "disabled") {
					bg.visible = false;
					button_txt.setTextFormat(defaultTextFmt);
				}
			}
		}
		
		
		
		private function clickEvent(e:MouseEvent = null) {
			e.preventDefault();
			if (!selected) {
				buttonSelected();
				
			}else {
				buttonDeselected(true);
			}
			dispatch(ExtensionsButton.CLICK, { button:this } );
		}
		private function buttonSelected() {
			bg.visible = true;
			button_txt.setTextFormat(defaultTextFmt);
			selected = true;
			bg.visible = false;
			bgSelected.visible = true;
		}
		
	// ---PUBLIC API---
		public function buttonDeselected(overState:Boolean = false) {
			if (!selected) {
				return;
			}
			selected = false;
			bgSelected.visible = false;
			if(overState){
				bg.visible = true;
				button_txt.setTextFormat(selectedTextFmt);
			}else {
				bg.visible = false;
				button_txt.setTextFormat(defaultTextFmt);
			}
		}
	
// ----EVENTS----
		// Registering for an event with the resizer
		public function addListener(eventType:String, callBackFunction:Function) {
			
			switch(eventType) { 
				
			
				
				case ExtensionsButton.CLICK:
					clickEvent_array.push(callBackFunction);
				break;
			
			}
			
		}
		
		private function dispatch(eventType:String,returnObject:Object = null) {
			switch(eventType) { 
				case ExtensionsButton.CLICK:
					trace("DISPATCH: CLICK");
					for (var index4:int = 0 ; index4 <  clickEvent_array.length ; index4++) {
						clickEvent_array[index4](returnObject);
					}
				break;
				
			}
		}
	}
}