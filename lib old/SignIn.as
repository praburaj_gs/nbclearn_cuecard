﻿package 
{
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.external.*;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import flash.filters.BevelFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	
	import flash.text.TextFieldAutoSize;
	/**
	 * ...
	 * @author 
	 */
	public class  SignIn extends MovieClip {
		
		private var cueCard:AS3CueCard;
		private var bg:RECTANGLE;
		private var originalWidth:Number;
		private var originalHeight:Number;
		private var bevel:BevelFilter;
		private var blackboardMode:Boolean = false;
		private var blackboard_mc:MovieClip;
		
		private var learn_mc:MovieClip;
		private var learnFirstTime:Boolean = true ;
		private var highered_Loader:Loader;
		public var freeTrialURL:String;
		//private var preventLoginTimer:Timer
		//private var canLogin:Boolean = false;
		
		public function SignIn(_cueCard:AS3CueCard ) {
			this.cueCard = _cueCard;
			
			freeTrialURL = cueCard.loaderInfo.parameters.freeTrailURL;
			
			blackboard_mc = blackboard_image;
			signInMain_mc.email_txt.restrict = '^%$#!~&*()-+|<>[]{}?"//^';
			signInMain_mc.pass_txt.restrict = '^%$#!~&*()-+|<>[]{}"?//^';
			registerationCode_mc.regCode_txt.restrict = '^%$"()?//^';
			signInMain_mc.pass_txt.displayAsPassword = true;
			
			blackboard_mc.visible = false;
			this.visible = false;
		
			originalWidth = cueCard.front_mc.content_mc.blackBackround.width;
			originalHeight = cueCard.front_mc.content_mc.blackBackround.height;
			bevel = new BevelFilter( -.6, 45, 0xFFFFFF, .52, 0x000000, .74, 2, 2, 1, 3);	
			bg = new RECTANGLE(originalWidth, originalHeight, 0xFFFFFF,0xE5E5E5,45);
			
			this.addChildAt(bg, 0);
			
			
			var targetDepth:int = signInMain_mc.numChildren - 1;
			signInMain_mc.setChildIndex(signInMain_mc.header_txt, targetDepth)
			
			
			//preventLoginTimer = new Timer(200, 1);
			//preventLoginTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent) {
				//canLogin = true;
				
			//});
			
			eventSetUp();
		}
	


		public function init(_label:String = null , popUpType:String = null) {
			//canLogin = false; // prevent an emediate login based on keyboard control
			
			//preventLoginTimer.reset();
			//preventLoginTimer.start();
			
			cueCard.videoPlayer.snipe_mc.visible = false;
			if(_label){
				signInMain_mc.header_txt.text = _label;
			}
			
		
			
			
			
			
			
			
			//trace("~popUpType: " + popUpType);
			if (popUpType == "2-button popup") {
				signInMain_mc.genericReg_mc.visible = true;
				signInMain_mc.subscribe_mc.visible = false;
				cueCard.disableKeyboardControl = true;
				enterLogin();
			}else if (popUpType == "blackboard") {
				blackboardMode = true;
				signInMain_mc.visible = false;
				registerationCode_mc.visible = false;
				blackboard_mc.visible = true;
			}
			else if (popUpType == "learn") {
				
				
				signInMain_mc.visible = false;
				registerationCode_mc.visible = false;
				blackboard_mc.visible = false;
				if (learnFirstTime) { // load these images the first time the user tries to enter the sign in mode.
					
					setUpLearnState();
							arrangeContent();
					
					
				}else{
				
					//trace("~Learn Second Time");
					arrangeContent();
				}
			}else{
				signInMain_mc.genericReg_mc.visible = false;
				signInMain_mc.subscribe_mc.visible = true;
				cueCard.disableKeyboardControl = true;
				
			
				
				
				//cueCard.tabManager.setTab(backToVideo, cueCard.tabManager.numTabs, true);
				
				
				
				enterLogin();
			}
			
			// LOGIN
				
				cueCard.addItemToTabOrder(signInMain_mc.header_txt, "");
				cueCard.addItemToTabOrder(signInMain_mc.email_txt, "Enter Your Email Address");
				cueCard.addItemToTabOrder(signInMain_mc.pass_txt, "Enter Your Password");				
				cueCard.addItemToTabOrder(signInMain_mc.login_btn.hit_btn, "Login In");
				
				cueCard.addItemToTabOrder(signInMain_mc.subscribe_mc.sub_title_txt, "");
				cueCard.addItemToTabOrder(signInMain_mc.subscribe_mc.sub_title_txt, "");
				cueCard.addItemToTabOrder(signInMain_mc.subscribe_mc.sign_up_txt, "");
				cueCard.addItemToTabOrder(signInMain_mc.subscribe_mc.signUp_btn.hit_btn, "Sign Up");
				cueCard.addItemToTabOrder(signInMain_mc.subscribe_mc.schoolCode_txt, "");
				cueCard.addItemToTabOrder(signInMain_mc.subscribe_mc.register_btn.hit_btn, "Register");
				
				
				// Registration 
				cueCard.addItemToTabOrder(registerationCode_mc.RegTitle_txt, "");
				cueCard.addItemToTabOrder(registerationCode_mc.regCode_txt, "Registration Code");
				cueCard.addItemToTabOrder(registerationCode_mc.login_btn, "Registration Code Login");
				cueCard.addItemToTabOrder(registerationCode_mc.regCodeDscr_txt, "");
				
				
				
				cueCard.addItemToTabOrder(backToVideo.close_btn, "Go back to the video");
		
			this.x = -179
			this.y = 37
			if (cueCard.imageMode == false) {
				//cueCard.videoPlayer.stop();
				cueCard.videoControls_mc.visible = false;
			}
			
			this.visible = true;
			if (popUpType != "learn"){
				arrangeContent();
			}
		}
		
		
		
		
		private function setUpLearnState() {
			learnFirstTime = false;
			learn_mc = new MovieClip();
			this.addChild(learn_mc);
			learn_mc.y = 10;
					
			var learnText:TextField = new TextField();
			learn_mc.title_txt = learn_mc.addChild(learnText);
			learnText.width = 360;
			learnText.height = 50;
			learnText.multiline = true;
			learnText.wordWrap = true;
			var txt_fmt:TextFormat = new TextFormat();
			txt_fmt.size = 15;
					
			//learnText.autoSize = TextFieldAutoSize.LEFT;
			
			learnText.htmlText = 'You have selected a feature that is for subscribers only.<br/>To learn more choose a product or sign up for a <a href="' + freeTrialURL + cueCard.metaData.Asset_ID + '"><Font color="#0000FF"><u>free trial</u></Font></a>.';
			learnText.setTextFormat(txt_fmt);
			
			var highered_mc:Sprite = new Sprite();
			
			
			var archives_mc:Sprite = new Sprite();
			var archives_loader:Loader = new Loader();
			
			learn_mc.archives_mc = learn_mc.addChild(archives_mc);
			archives_mc.addChild(archives_loader);
			archives_mc.x = learnText.x;
			archives_mc.y = learnText.y + learnText.height;
			
			learn_mc.highered_mc = learn_mc.addChild(highered_mc);
					
			
			archives_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadHigheredButton);
			archives_loader.load(new URLRequest("http://icue.nbcunifiles.com/files/nbcarchives/LearnAssets/cueCardAssets/371x84_PopUpBanners_Final_K12.jpg"));
			
			
					
			highered_mc.buttonMode = true;
			highered_mc.useHandCursor = true;
			highered_mc.addEventListener(MouseEvent.CLICK, function() {
				navigateToURL(new URLRequest('http://highered.nbclearn.com'));
			});
				
			archives_mc.buttonMode = true;
			archives_mc.useHandCursor = true;
			archives_mc.addEventListener(MouseEvent.CLICK, function() {
				navigateToURL(new URLRequest('http://archives.nbclearn.com'));
			});
					
			
		}
		
		private function loadHigheredButton(event:Object = null) {
			
				
				//trace("~load Highered button");
				highered_Loader = new Loader();
				
				learn_mc.highered_mc.addChild(highered_Loader);
				
				learn_mc.highered_mc.x = learn_mc.archives_mc.x;
				
				learn_mc.highered_mc.y = learn_mc.archives_mc.y + 85 + 10;
				
				highered_Loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e:Event) {
					//trace("~Highered Loaded");
					arrangeContent();
				});
				
				
				highered_Loader.load(new URLRequest("http://icue.nbcunifiles.com/files/nbcarchives/LearnAssets/cueCardAssets/371x84_PopUpBanners_Final_HIGHED.jpg"));
				arrangeContent();
				learn_mc.y = 11;
				
		}
		
		public function arrangeContent() {
			if(this.visible == true){
				this.bg.width = cueCard.front_mc.content_mc.blackBackround.width;
				this.bg.height = cueCard.front_mc.content_mc.blackBackround.height;
				this.peacock_mc.x = bg.width - peacock_mc.width;
				this.peacock_mc.y = bg.height - peacock_mc.height;
				backToVideo.x = bg.width - 65;
				
				if (signInMain_mc.visible == true){
					signInMain_mc.x = bg.width / 2 - 185;
				}
				if (registerationCode_mc.visible == true) {
					registerationCode_mc.x = bg.width / 2 - 160;
				}
				if (blackboard_mc.visible == true) {
					blackboard_mc.x = bg.width / 2 - blackboard_mc.width / 2;
					blackboard_mc.y = bg.height / 2 - blackboard_mc.height/2;
				}
				if (learn_mc) {
					if (learn_mc.visible == true) {
						learn_mc.x = bg.width / 2 - learn_mc.width / 2;
						learn_mc.y = bg.height / 2 - learn_mc.height / 2;
						
					}
				}
				
			}
			
		}
		public function reset() {
			originalHeight
			this.bg.width = originalWidth;
			this.bg.height = originalHeight;
			this.peacock_mc.x = bg.width - peacock_mc.width;
			this.peacock_mc.y = bg.height - peacock_mc.height;
			backToVideo.x = bg.width - 65;
		}
		private function eventSetUp() {
		
			backToVideo.close_btn.addEventListener(MouseEvent.CLICK, closeThisPopUp);
			backToVideo.close_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				Colors.changeTint(backToVideo.close_btn.bg, 0xc8234a, 1)
				Colors.changeTint(backToVideo.close_btn.gfx, 0xFFFFFF, 1)
				backToVideo.close_btn.filters = [bevel];
			});
			backToVideo.close_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				Colors.changeTint(backToVideo.close_btn.bg, 0xc00343, 1)
				Colors.changeTint(backToVideo.close_btn.gfx, 0xFFFFFF, 1)
				backToVideo.close_btn.filters = [];
			});
			Colors.changeTint(backToVideo.close_btn.bg, 0xc00343, 1)
			Colors.changeTint(backToVideo.close_btn.gfx, 0xFFFFFF, 1)
			
			backToVideo.close_btn.buttonMode = true;
			backToVideo.close_btn.useHandCursor = true;
			
			
	// EMAIL BUTTON
			setUpButton("SIGN IN",signInMain_mc.login_btn, 0xa8aeb8, 0xa8aeb8,login);
	// SIGN UP BUTTON
			setUpButton("SIGN UP",signInMain_mc.subscribe_mc.signUp_btn, 0xa8aeb8, 0xa8aeb8,freeTrial);
	// REGISTER BUTTON
			setUpButton("REGISTER",signInMain_mc.subscribe_mc.register_btn, 0xa8aeb8, 0xa8aeb8,enterRegCode);
	// REG CODE BUTTON
			setUpButton("REGISTER", registerationCode_mc.login_btn, 0xa8aeb8, 0xa8aeb8, registerWithInitCode);
	// GOTO REGISTRATION PAGE	
			setUpButton("REGISTER", signInMain_mc.genericReg_mc.signUp_btn, 0xa8aeb8, 0xa8aeb8, gotoRegisterPage);
			
			
			
			signInMain_mc.pass_txt.addEventListener(KeyboardEvent.KEY_UP, function(event:KeyboardEvent) {
				trace("~event.keyCode: " + event.keyCode);
				if (event.keyCode == 13) { // ENTER KEY
					//trace("~cueCard.fmanager.getFocus(): " + cueCard.fmanager.getFocus());
					//trace("~cueCard.fmanager.getFocus().name: " + cueCard.fmanager.getFocus().name + " | parent: " + cueCard.fmanager.getFocus().parent.name);
					trace("~ try to login");	
					//if( signInMain_mc.visible == true && signInMain_mc.pass_txt.length > 2 && signInMain_mc.email_txt.length > 3){
						
						
							login();
						//}
					
				}
			});
			
			
			registerationCode_mc.regCode_txt.addEventListener(KeyboardEvent.KEY_UP,function(event:KeyboardEvent) {
				if (event.keyCode == 13) {
						
						registerWithInitCode();
					
				}
			});
			blackboard_mc.hitBox_mc.useHandCursor = true;
			blackboard_mc.hitBox_mc.buttonMode = true;
			blackboard_mc.hitBox_mc.addEventListener(MouseEvent.CLICK , function(e:MouseEvent) {
				navigateToURL(new URLRequest("http://nbclearn.com/bbmoreinfo"), "_blank");
			});
			
			
		
		}
		
		private function setUpButton(_label:String, button:MovieClip, initialColor:Number, overColor:Number, clickFunction:Function = null) {
			button.label_txt.text = _label;
			Colors.changeTint(button.bg, initialColor, 1);
			if (clickFunction != null) {
				button.hit_btn.buttonMode = true;
				button.hit_btn.useHandCursor = true;
				button.hit_btn.addEventListener(MouseEvent.CLICK, clickFunction);
			}
			button.hit_btn.addEventListener(MouseEvent.ROLL_OVER, function(event:MouseEvent) {
				Colors.changeTint(event.currentTarget.parent.bg, overColor, 1)
				button.filters = [bevel];
			});
			button.hit_btn.addEventListener(MouseEvent.ROLL_OUT, function(event:MouseEvent) {
				Colors.changeTint(event.currentTarget.parent.bg, initialColor, 1)
				button.filters = [];
			});
			
		}
		
		


		private function enterRegCode(e:MouseEvent = null){
			signInMain_mc.header_txt.text = "Register";
			signInMain_mc.visible = false;
			
			registerationCode_mc.visible = true;
			//cueCard.fmanager.setFocus(registerationCode_mc.regCode_txt);
			cueCard.stage.focus = registerationCode_mc.regCode_txt;
			
			// Registration 
				cueCard.addItemToTabOrder(registerationCode_mc.RegTitle_txt, "");
				cueCard.addItemToTabOrder(registerationCode_mc.regCode_txt, "Registration Code");
				cueCard.addItemToTabOrder(registerationCode_mc.login_btn, "Registration Code Login");
				cueCard.addItemToTabOrder(registerationCode_mc.regCodeDscr_txt, "");
				
				
				
				cueCard.addItemToTabOrder(backToVideo.close_btn, "Go back to the video");
				cueCard.updateAccessibilityProperties();
			
			arrangeContent();
			

		}
		private function enterLogin(e:MouseEvent = null){
			//signInMain_mc.header_txt.text = "Sign In";
			signInMain_mc.visible = true;
			registerationCode_mc.visible = false;

			cueCard.stage.focus = signInMain_mc.email_txt;
			arrangeContent();
		}



		// LOG INTO ICUE
		private function login(e:MouseEvent = null){
			//trace("~doModalLogin");
			//text,signInMain_mc.remember_mc.state instead of undefined
			if(signInMain_mc.email_txt.length < 3 && signInMain_mc.pass_txt.length < 2){
				return;	
			}
			ExternalInterface.call("doModalLogin",signInMain_mc.email_txt.text,signInMain_mc.pass_txt.text,undefined,cueCard.metaData.vcmID);
			closeThisCueCard();
		}
		private function registerWithInitCode(e:MouseEvent = null){
			//trace("~doModalRegister");
			
			ExternalInterface.call("submittoRegisterWithcode",registerationCode_mc.regCode_txt.text,cueCard.metaData.vcmID);//"doModalRegister"
			closeThisCueCard()
		}
		private function loginFaceBook(e:MouseEvent = null){
			//trace("~faceBookLogin");
			ExternalInterface.call("doFacebookLogin",cueCard.metaData.vcmID);
			closeThisCueCard()
		}
		private function freeTrial(event:Object = null) {
			try {
				var request:URLRequest = new URLRequest(freeTrialURL + cueCard.metaData.Asset_ID);
				navigateToURL(request,"_blank"); 
			}catch (e:Error) {
					
			}
		}
		// GOTO REGISTRATION
		private function gotoRegisterPage(e:MouseEvent = null){
			//ExternalInterface.call("CueCardManager.register",email_txt.text,pass_txt.text,remember_mc.state);
			//trace("~submittoRegister");
			if(cueCard.whiteListedIP == true){
				ExternalInterface.call("submittoRegister");
			}
			
			
			closeThisCueCard()
		}
		private function forgotPassword(e:MouseEvent = null){
			var baseURL:String = cueCard.loaderInfo.parameters.snasapiURL; //https://dev-preview.icue.com/icue/snas/api/");
			var regURL = baseURL.split("snas")[0] + "forgotpassword/"
			
			try {
					var request:URLRequest = new URLRequest(regURL);
					navigateToURL(request,"_blank"); 
				}catch (e:Error) {
					
				}
		}
		
		
		
		private function closeThisCueCard(e:MouseEvent = null){
			
			this.visible = false;
			cueCard.removeCard();
			
		}
		public function close() {
			if(this.visible){
				closeThisPopUp();
			}
		}
		private function closeThisPopUp(e:MouseEvent = null){
			cueCard.disableKeyboardControl = false;
			
			cueCard.setMainFocus(backToVideo.close_btn, cueCard.always_mc.snipeHitBox_mc);
			cueCard.videoPlayer.snipe_mc.visible = true;
			this.visible = false;
			if (cueCard.imageMode == false) {
				
				cueCard.videoControls_mc.visible = true;
			}
			
				
			
			
		}
	}
}