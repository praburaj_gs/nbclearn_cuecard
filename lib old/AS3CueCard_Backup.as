﻿package {
	
	/**
	 * ...
	 * @author ...
	 */

	//import com.nbcuni.outlet.extensions.video_player.constants.VideoPlayerConstants;
	import fl.managers.FocusManager;
	import fl.motion.AdjustColor;
	import fl.motion.Color;
	import flash.display.*
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	import flash.geom.Point;
	import flash.xml.XMLNode;
	
	import flash.filters.GlowFilter;
	import flash.events.*
	import flash.filters.DropShadowFilter;
	import flash.utils.setInterval;
	import flash.utils.clearInterval;
	import flash.utils.Timer;
	import flash.display.MovieClip;
	import flash.external.ExternalInterface
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.filters.BevelFilter
	import flash.system.Capabilities;
	import flash.net.navigateToURL;
	import flash.ui.Mouse;
	import com.omniture.ActionSource;
	
	
	
	import flash.system.Security;

	
	
	 
	 
	public class  AS3CueCard extends MovieClip
	{
		
		
	// Movie Clips
		public var cueCard:MovieClip;
		public var background_mc:MovieClip;
		public var front_mc:MovieClip;
		public var back_mc:MovieClip;
		public var shadowBackground:MovieClip;
		public var controls_mc:MovieClip;
		
		// USER INFO
		public var whiteListedIP:Boolean = false;
		public var signedIn:Boolean = false;
		public var userName:String;
		public var personUUID:String;
		public var communityID:String
		public var communityName:String;
		public var role:String;
		public var reloadPage:Boolean = false;

		
		private var minimize_obj:Object;
		
		public var always_mc:MovieClip;
		private var ds:DropShadowFilter;
		private var glow:GlowFilter;
		
		private var controlsTimer:Timer;
		private var controls_obj;
		
		public var flipped:Boolean = false;
		public var imageMode:Boolean = false;
		
		private var bevel:BevelFilter;
		public var signIn:SignIn;
		public var states_xml:XMLList;
		public var currentState_xml:XMLList 
		public var platform:String;
		
		public var Product:String;
		public var Access:String;
		public var Mode:String;
		
		
		public var omniture:Omniture;
		
		
		public var folderDirectory:String;
		public var randomKey:String;
		
		// Targets
		var targetRotation:int = 180;
		private var midwayReached:Boolean = false;
		private var finalFlipReached:Boolean = false;
		private var flipFailSafeTimer:Timer;
		
	// Consts
		public const TOP:int = 46;
		public const BOTTOM:int = 372
		public const LEFT:int = -228;
		public const RIGHT:int = 5;
		public const WIDTH:int = 456;
		public const HEIGHT:int = 312;
		private const SIDEBAR_LEFT:int = -286;
		private const SIDEBAR_RIGHT:int = -176;
		
		public var overrideGuestMode:Boolean = false;
		public var disableKeyboardControl:Boolean = false;
		
		public static const CARD_RESET_EVENT:String = "onCardReset";
		
		private var blackBoardInternalExternal:String;
		public var isHTTPS:Boolean = false;
		
	// Tweens
		var removeTween:Tween;
		var flipTween:Tween;
	
	
		var shadowBackgroundTween:Tween;
		private var sideBarTween:Tween;

		
		private var resizer:Resizer;
		private var flipDelay:int;

		public var playList_mc:PlayListWindow;
		public var confirmation:ConfirmationManager;
		
		
		public var videoPlayer:VideoPlayer;
		public var imageViewer:ImageViewer;
		public var customCurser:MovieClip;
		//private var _data:XMLList;
		
		
		
	// Complex Objects
		private var matrix_obj:Object;
		private var leftSideBarInt:int;
		
		//public var menu:CardMainMenu;
		public var metaData:MetaDataHandler;
		private var currentCueCard:Object;
		
		public var extensionsManager:ExtensionsManager;
		public var motionManager:MotionManager;
		
		public var cueCardHeight:Number;
		
		public var curtain_mc:DynamicSprite;
		public var isResizing:Boolean = false;
		public var loading_mc:MovieClip;
		
		private var stopCueCardDrag:Boolean = false;
		
		public var tip:MouseTip;
		
		public var fmanager:FocusManager;
		public var videoControls_mc:MovieClip;
		public var tabOrder_array:Array 
		
		
		public function AS3CueCard() {
			Security.allowInsecureDomain("*");
			Security.allowDomain("*");
			trace("~AS3 Cue Card 3.015 -  Updated Nov 15th 2011");
			cueCard = this;
			
			var stageInitTimer:Timer = new Timer(50);
			stageInitTimer.addEventListener(TimerEvent.TIMER, function(e:TimerEvent) {
				
				if (cueCard.stage != null) {
					stageInitTimer.stop();
					init();
				}
			});
			
			
			
			stageInitTimer.start();
		
			
			
			//init();
			
		}
		private function init() {
			trace("~applicationDomain: " + this.stage.loaderInfo.applicationDomain);
			var folder:String = this.stage.loaderInfo.loaderURL as String;
			
			folderDirectory = folder.split("singleCard.swf")[0];
			// Detect if HTTPS is enabled.
			var video_string_array:Array = String(this.stage.loaderInfo.parameters.videoUrl).split("ttps://");
			if (video_string_array.length > 1) { // check to see if we are running in HTTPS
				isHTTPS = true;
				
			}else {
				isHTTPS = false;
			}
			trace("~isHTTPS: " + isHTTPS);
			
			//isHTTPS = _cueCard.stage.loaderInfo.parameters.videoUrl
			//trace("~folderDirectory: " + folderDirectory)
	
			randomKey = "?random=" + Math.round(Math.random() * 10000);
		
			customCurser = new CustomCurser();
			
			
			customCurser.visible = false;
			//cueCard.stage.addChild(customCurser);
			
			
			//this._data = data;
			cueCard.stage.align = StageAlign.TOP_LEFT;
			//declare the different objects in the cue card
			
			cueCard.x = 300;
			cueCard.y = 50;
			front_mc = cueCard.front_mc;
			background_mc = cueCard.front_mc.background_mc;
			back_mc = cueCard.back_mc;
			always_mc = cueCard.front_mc.always_mc;
			imageViewer = front_mc.imageVeiwer_mc;
			loading_mc = front_mc.loading_mc;
			imageViewer.setCueCard(this);
			shadowBackground = cueCard.front_mc.shadowBackground;
			
			
			background_mc.transcript_mc.cueCard = this;
			always_mc.extensions_bar.cueCard = this;
			always_mc.extensions_bar.extensions_mc.cueCard = this;
			extensionsManager = always_mc.extensions_bar;
			
			
			this.stage.scaleMode = StageScaleMode.NO_SCALE;
		
			back_mc.visible = false;
			
			tip = new MouseTip(cueCard);
			
			minimize_obj = new Object();
			ds = new DropShadowFilter(30, 45, 0x000000, 0.55, 16, 16, .8, 2, false, false, false);
			
			glow = new GlowFilter(0x000000, .8,2,2,1.9,2);
			
			bevel = new BevelFilter( -.6, 45, 0xFFFFFF, .52, 0x000000, .74, 2, 2, 1, 3);
			
			shadowBackground.filters = [glow,ds];
			
				
			
			flipped = false;
				// save matrix
			matrix_obj = new Object();
			matrix_obj.front_matrix = front_mc.transform.matrix;
			matrix_obj.back_matrix = back_mc.transform.matrix;
			
			
			initEvents();
			initResizer();
			//stage.addEventListener(MouseEvent.MOUSE_DOWN, stageMouseDownListener);
			
		//	menu = front_mc.background_mc.leftMenu_mc;
		//	menu.setBaseCard(this);
			
			
			metaData = new MetaDataHandler(this.back_mc.content_mc,this);
			confirmation = new ConfirmationManager(front_mc.panels_mask);
			confirmation.setCancelFunction("Cancel", null);
			addChild(confirmation);
			
			
			
			motionManager = new MotionManager(this.parent, this);
			cueCardHeight = HEIGHT;
			flipFailSafeTimer = new Timer(1100, 1); // set the timer for just after the flipMotion Fails
			flipFailSafeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, flipFailSafe);
			//motionManager.keepOnScreen();
			
			setUpVideoPlayer();
			ExternalInterface.addCallback("loadClip", loadXML);
			ExternalInterface.addCallback("newCard", loadXML);
			this.visible = false;
			
			omniture = new Omniture(this);
			signIn = new SignIn(this);
			always_mc.addChild(signIn);
			
			
			validateLogin();
			
			
			curtain_mc = new DynamicSprite();
		
			curtain_mc.alpha = .4;
			this.stage.addChild(curtain_mc);
			
			this.stage.setChildIndex(curtain_mc, 0)
			curtain_mc.visible = false;
			this.stage.addEventListener(Event.RESIZE, function(e:Event) {
				drawCurtain();
			});
			versionDetection()
			
			
			
		}
		private function versionDetection() {
			var versionNumber:String = Capabilities.version;
			

			// The version number is a list of items divided by ","
			var versionArray:Array = versionNumber.split(",");
			var length:Number = versionArray.length;
			
			var platformAndVersion:Array = versionArray[0].split(" ");
			platform = platformAndVersion[0];
			var majorVersion:Number = parseInt(platformAndVersion[1]);
			var minorVersion:Number = parseInt(versionArray[1]);
			var buildNumber:Number = parseInt(versionArray[2]);

			//trace("~Platform: "+platform);
			//trace("~Major version: " + majorVersion);
			if (majorVersion < 10) {
				var request:URLRequest = new URLRequest("http://get.adobe.com/flashplayer/");
				
				confirmation.confirm("You must update to the latest version of Adobe Flash!", "UPDATE", function(){navigateToURL(request,"_blank") },true );
			}
		}
		
		private function drawCurtain() {
			if (!this.visible) {
				return;
			}
			curtain_mc.graphics.clear();
			curtain_mc.graphics.beginFill(0, 1);
			curtain_mc.graphics.moveTo(0, 0);
			curtain_mc.graphics.lineTo(cueCard.stage.stageWidth, 0);
			curtain_mc.graphics.lineTo(cueCard.stage.stageWidth, cueCard.stage.stageHeight);
			curtain_mc.graphics.lineTo(0, cueCard.stage.stageHeight);
			curtain_mc.graphics.lineTo(0, 0);
			
		}
		
		public function loadXML(_url:String, overrideGuest = false) {
			//trace("~overrideGuest: " + overrideGuest);
			reloadPage = false;
			this.disableKeyboardControl = false;
			if(overrideGuest  == "noGuestMode"){
				overrideGuestMode = true;
			}else{
				overrideGuestMode = false;
			}
			
			var request:URLRequest = new URLRequest(_url);
			var xmlLoader:URLLoader = new URLLoader();
			xmlLoader.addEventListener(Event.COMPLETE, handleMetaData);
			xmlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, crossDomainError);
			xmlLoader.addEventListener(IOErrorEvent.NETWORK_ERROR, XML_didNot_Loaded);
			xmlLoader.addEventListener(IOErrorEvent.IO_ERROR, IO_didNot_Loaded);
			
			
			xmlLoader.load(request);
			metaData.setVcmID(_url.split("id=")[1],_url);

		}
		
		
		private function IO_didNot_Loaded(e:IOErrorEvent) {
			cardDidNotLoad("IO_ERROR", "The Cue Card was not able to properly load the XML feed." , e.text);
		
		}
		
		private function XML_didNot_Loaded(e:IOErrorEvent){
			cardDidNotLoad("NETWORK_ERROR", "The Cue Card is experiencing network load issues.", e.text);
		}
		
		private function crossDomainError(e:SecurityErrorEvent){
			cardDidNotLoad("SECURITY ERROR" ,"The Cue Card experienced a cross domain security error.", e.text);
		}
		
		private function cardDidNotLoad(_errorType:String , _message:String, _traceString){
		
			trace(_errorType + ":  " + _traceString);
			var _helpURL:String;
			if (Product == "K12" ) {
				_helpURL = 'http://archives.nbclearn.com/portal/site/k-12/help'; // K12 Help
			}
			
			if(Product == 'HE'){
				_helpURL = 'http://highered.nbclearn.com/portal/site/HigherEd/help'; // Highered HELP
			}
			if(Product == "BBHE") {
				_helpURL = 'http://archivesbb.nbclearn.com/portal/site/BbHigherEd/help';
			}
			if(Product == "BBK12" || Product == "BBHE") {
				_helpURL = 'http://archivesbb.nbclearn.com/portal/site/BbHigherEd/help';
			}
			
						
			always_mc.title_txt.htmlText = " ";	
			//_message
			//this.visible = true; 
			// curtain_mc.visible = true;
			  drawCurtain();
			  closeCardThatDidNotLoad();
			 //confirmation.setCancelFunction("OK", closeCardThatDidNotLoad ); // have the cancel function close the cue card
			//confirmation.confirm("Whoops something went wrong with the CueCard.  Please Try again." , "OK", closeCardThatDidNotLoad ,true);// function() { navigateToURL(new URLRequest(_helpURL)) }, false );
			
		}
		private function closeCardThatDidNotLoad() {
			//confirmation.setCancelFunction("Cancel", null); // retun the cancel function back to normal.
			closeCueCard();
		}
		
		private function validateLogin() {
			
			var canLoadStatesXML:Boolean = true;
			// userName
			if (isNotUndefined(this.stage.loaderInfo.parameters.userName) == true) {
				userName = this.stage.loaderInfo.parameters.userName;
				metaData.userName = userName;
			
			}
			if (isNotUndefined(this.stage.loaderInfo.parameters.email) == true) {
				metaData.email = this.stage.loaderInfo.parameters.email;
			}
			
			// personUUID
			if (isNotUndefined(this.stage.loaderInfo.parameters.personUUID) == true) {
				personUUID = this.stage.loaderInfo.parameters.personUUID;
				metaData.personUUID = personUUID;
			}
			// communityName
			if (isNotUndefined(this.stage.loaderInfo.parameters.communityName) == true) {
				communityName = this.stage.loaderInfo.parameters.communityName;
				metaData.communityName = communityName;
			}
			// School State
			if (isNotUndefined(this.stage.loaderInfo.parameters.State) == true) {
				metaData.State = this.stage.loaderInfo.parameters.State;
				
			}
			
			// communityID
			if (isNotUndefined(this.stage.loaderInfo.parameters.communityID) == true) {
				communityID = this.stage.loaderInfo.parameters.communityID;
				metaData.communityID = communityID;
			}
			
			// freeResourceCollection
			if (isNotUndefined(this.stage.loaderInfo.parameters.freeResource) == true) {
				
				metaData.freeResourceCollection = this.stage.loaderInfo.parameters.freeResource;
			}
			// School Association
			if (isNotUndefined(this.stage.loaderInfo.parameters.schoolAssociation) == true) {
				
				metaData.schoolAssociation = this.stage.loaderInfo.parameters.schoolAssociation;
			}
			
			// Role
			if (isNotUndefined(this.stage.loaderInfo.parameters.role) == true) {
				role = this.stage.loaderInfo.parameters.role;
				metaData.role = role;
			}
			
			// Product
			if (isNotUndefined(this.stage.loaderInfo.parameters.Product) == true) {
				Product = this.stage.loaderInfo.parameters.Product;
				metaData.Product = Product;
			}
			// Access
			if (isNotUndefined(this.stage.loaderInfo.parameters.Access) == true) {
				Access = this.stage.loaderInfo.parameters.Access;
				metaData.Access = Access;
			}
			// Mode
			
			if (isNotUndefined(this.stage.loaderInfo.parameters.cueCardMode) ) {
				if (Product == "NBC Learn" || Product == "LEARN2" ||  this.stage.loaderInfo.parameters.cueCardMode == "learn") {
					Product = "NBC Learn";
					Access = "GUEST";
					Mode = "SHARE";
					metaData.Product = Product;
					loadStatesXML();
					return;
				}
			// WE ARE IN BLACKBOARD
				Mode = String(this.stage.loaderInfo.parameters.cueCardMode).toLocaleUpperCase();
				trace("~cueCardMode Is Set To: " + String(this.stage.loaderInfo.parameters.cueCardMode).toLocaleUpperCase());
				trace("~MODE Is Set To: " + this.stage.loaderInfo.parameters.Mode);
				try {
					if (this.stage.loaderInfo.parameters.switchTrial == "y" || this.stage.loaderInfo.parameters.switchTrial == "Y") {
						if(this.stage.loaderInfo.parameters.Access == "FULL"){
							Access = "TRIAL";
						}
						if(this.stage.loaderInfo.parameters.Access == "GUEST") {
							Access = "GUEST";	
						}
						
						
					}
				}catch(e:Error) {
					
				}
				if (!isNotUndefined(Access)) {
					canLoadStatesXML = false;
					authenticateFromURL();
				}else { // We are loading from the nbc environement
					blackBoardInternalExternal = "NBC Environment";
					if (isNotUndefined(userName)){
						communityName = userName;
						metaData.communityName = userName;
					}
				}
			}else if (isNotUndefined(this.stage.loaderInfo.parameters.Mode) == true) {
				Mode = this.stage.loaderInfo.parameters.Mode;
				trace("~Attempt to set as trial");
				try {
					if (this.stage.loaderInfo.parameters.switchTrial == "y" || this.stage.loaderInfo.parameters.switchTrial == "Y") {
						if(this.stage.loaderInfo.parameters.Access == "FULL"){
							Access = "TRIAL";
						}
						if(this.stage.loaderInfo.parameters.Access == "GUEST") {
							Access = "GUEST";	
						}
						
						
					}
				}catch(e:Error) {
					
				}
				
			}
			
			try {
				var expDate:Date = new Date(this.stage.loaderInfo.parameters.expirationTime);
				trace("!exp : " + expDate.getTime());
				var currentDate:Date = new Date();
				trace("!date: " + currentDate.getTime());
				if (currentDate.getTime() > expDate.getTime()) {
					Access = "GUEST";
					trace("!Community Expired");
				}
			}catch (e:Error) {
				trace("! No Date");
			}
		
			
			

		// check for white listed IP
			if (isNotUndefined(personUUID) == false && isNotUndefined(userName) == false) {
				
			
				if (isNotUndefined(communityName) && isNotUndefined(communityID)){ // see if there is still a community ID which would 
					//userName = "White Listed IP";								  // which indicates that this user is comming from an white listed IP
				//	whiteListedIP = true;
				}else { // otherwise the user name is a guest
					//userName = "Guest";
				}
			}
			
			if (canLoadStatesXML) { // if we are not in `````````` and the access is set from the url parameters than go ahead and load the states xml
				loadStatesXML();
			}
		
		}
		
		private function authenticateFromURL() {
			
			//trace("~authenticateFromURL");
			// Authenticate from blackboard before loading the statesxml.
			var request:URLRequest = new URLRequest(this.stage.loaderInfo.parameters.auth_url);// "http://icue.nbcunifiles.com/icue/files/nbcarchives/site/componentsAOD2/masterStatesList.xml");// 
			var authenticationLoader:URLLoader = new URLLoader();
			
			authenticationLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, xmlSecurityError);
			authenticationLoader.load(request);
			authenticationLoader.addEventListener(Event.COMPLETE, parseAuthenticationURL);
		}
		private function parseAuthenticationURL(e:Event) {
			//trace("~ AUTHENTICATE: ");
			var authXML:XML = XML(e.target.data) as XML ;
			
	
			var schoolName:String = String(authXML);  // School Name
			var experationDate:String = String(authXML.attribute("exiprationdate")) as String; // Expiration Date
			var isTrial:String = String(authXML.attribute("switchTrial")); // Free Trial
			var siteMode:String = String(authXML.attribute("siteMode")); // Product
			var state_str:String = String(authXML.attribute("state")); // State
			var community_str:String = String(authXML.attribute("communityId")); // State
					
					// this_class.popUp("authenticate","schoolName: " + schoolName,null,"OK");
					/*
					trace("~ SCHOOL NAME: " + schoolName);
					trace("~ experationDate: " + experationDate);
					trace("~ isTrial: " + isTrial);
					trace("~ siteMode: " + siteMode);
					trace("~ state_str: " + siteMode);
					trace("~ community_str: " + siteMode);

					*/

			
		// Set the PRODCUT		
			if (isNotUndefined(siteMode)) {
				Product = siteMode;
				metaData.Product = Product;
			}else {
				var product_array:Array = String(cueCard.stage.loaderInfo.parameters.cuecardURL).split("BbHigherEd");
				if(product_array.length > 1){
					Product = "BBHE";
				}else {
					Product = "BBK12";
					
				}
				metaData.Product = Product;
			}
		// Set the ACCESS
			if (isNotUndefined(schoolName) && schoolName != "No Results Found") {
				if(siteMode){
					 Access = "FULL";
				 }
				if (isTrial == "Y" || isTrial == "y") {
					//trace("~Set Access to trial");
					Access = "TRIAL";
				}
				 
				metaData.communityName = schoolName;
				metaData.State = state_str;
				metaData.expirationDate = experationDate;
				metaData.communityID = community_str;
			
			}else{
						
				Access = "GUEST";
				userName = "Guest";
				metaData.communityName = userName;
				metaData.communityID = userName;
				
			}
			metaData.State = state_str;
			metaData.Access = Access;
			
			
			// this_class.startTracking("BB Hosted");
			blackBoardInternalExternal = "BB Hosted";
			loadStatesXML();
		}
		
		private function loadStatesXML(states_url:String = null) {
			
			
			var request:URLRequest = new URLRequest(folderDirectory +"masterStatesList.xml");// "http://icue.nbcunifiles.com/icue/files/nbcarchives/site/componentsAOD2/masterStatesList.xml");// 
			var xmlLoader:URLLoader = new URLLoader();
			xmlLoader.addEventListener(Event.COMPLETE, parseStatesXML);
			xmlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, xmlSecurityError);
			xmlLoader.load(request);
			//setTabOrder();
		}
		
		
		private function xmlSecurityError(e:SecurityErrorEvent) {
			trace("~SECURITY ERROR: " + e.toString());
		}
		private function parseStatesXML(e:Event):void {
		
			
			//Product = "K12";
			//Access = "FULL";
			//Mode = "SHARE";
			states_xml = new XMLList(e.target.data);
			
			
		
			states_xml.ignoreWhitespace = true; 
			
			
			var product_xml:XMLList = states_xml.Product.(@name == Product);  // Product
			var access_xml:XMLList  = product_xml.Access.(@name == Access); // Access
			
			
			
			currentState_xml  = access_xml.Mode.(@name == Mode); // MODE
			
			
		//	trace("attribute(mode) = " + currentState_xml.video.attribute("mode"));
		
		// check for white listed IP
			
			//Product = "K12";
			//Access = "FULL";
			//Mode = "SHARE";
			trace("~Product: " + Product);
			trace("~Access: " + Access);
			trace("~ Mode: " + Mode);
			
			if (Access == "GUEST") { 
				
				userName = "Guest";
			}
			if (Access == "GENERIC") { 
				userName = "White Listed IP";	
				whiteListedIP = true;
			}
			metaData.userName = userName;
			var popUpType:String;
			
			try{
				popUpType = access_xml.attribute("popUpType");
				videoPlayer.popUpType = popUpType;
			}catch (e:Error) {
				
			}
			
			extensionsManager.setUpExtensions(currentState_xml, popUpType);
			
			
			if (Product == "BBK12" || Product == "BBHE") {
				omniture.blackBoardInternalExternal = blackBoardInternalExternal;
			}
			omniture.setUpOmniture(product_xml.Omniture);
			
			
			if (Mode == "BARENC") {
				//trace("~Entered BARENC");
				this.x = 227;
				this.y = 0;
				
				
				always_mc.icons_right.close_mc.visible = false;
				always_mc.icons_right.flip_mc.x = 21;
				
				motionManager.preventDragging = true;
				always_mc.dragCorner.visible = false;
				videoPlayer.autoPlay = false;
				
			}
			if(currentState_xml.keywords.attribute("available") == "no"){
				metaData.disableKeywords = true;
			}
			
			videoPlayer.configureVideoMode(currentState_xml.video);
			
		}
		
		
		public function setTabOrder(controls:MovieClip) {
			
			fmanager = new FocusManager(controls);
		//	fmanager.setFocus(controls.play_btn);
			var all_items_array:Array = new Array();
			this.videoControls_mc = controls;
			
			//collectTabableChildren(cueCard, all_items_array);
			//for (var index1:int = 0 ; index1 < all_items_array.length; index1++) {
				//all_items_array[index1].tabEnabled = true;
			//}
			
			//cueCard.tabEnabled = false;
		//	trace("~Start adding items to TAB");
			var tabOrder_array:Array = new Array();
			
			tabOrder_array.push(controls.play_btn);   /// PLAY BUTTON
			tabOrder_array.push(controls.mute_btn);
			
			tabOrder_array.push(controls.fullScreen_btn);  // FULLL SCREEN
			tabOrder_array.push(controls.captions_btn.hit_btn);   // CAPTIONS BUTTON
			tabOrder_array.push(always_mc.icons_right.flip_mc); // FLIP
			tabOrder_array.push(always_mc.icons_right.close_mc);  // CLOSE
			
		
			tabOrder_array.push(background_mc.transcript_mc.transcript_btn);  // TRANSCRIPT
			tabOrder_array.push(background_mc.transcript_mc.activity_btn);  // TRANSCRIPT
			
			
			try{
				for (var ext_index:int = 0; ext_index < extensionsManager.extensionsButton_array.length ; ext_index++) {
					tabOrder_array.push(extensionsManager.extensionsButton_array[ext_index].hit_box);
				}
			}catch (e:Error) {
				
			}
		
		
			
			for (var index:int = 0; index < tabOrder_array.length ; index++) {
				var tabItem:Object = tabOrder_array[index];
				tabItem.tabIndex = (index + 1);
				tabItem.tabEnabled = true;
			}
		
			//trace("~ " + tabOrder_array.length + " items set for Tab order");
			
			
		
	
			
		}
		
			private function collectTabableChildren(item:InteractiveObject, storageArray:Array):void
		/* recursively visits each child of newItem and if it has tabEnabled, adds it to storageArray 
		 * Will not check the children of newItem if newItem.tabChildren is false                     
		 * after completion, storageArray will contain newItem (if newItem is tabable) and all tabable children */
		{
			if(item is DisplayObjectContainer)
			{
				var doc = item as DisplayObjectContainer;
				//trace(doc.name + " has " + doc.numChildren + " children");
				if(doc.tabChildren == true && doc.numChildren >= 1)
				{
					var children:int = doc.numChildren;
					for(var i:int=0; i < children; i++)
					{
						//trace("processing " + doc.name);
						collectTabableChildren(doc.getChildAt(i) as DisplayObjectContainer, storageArray);
					}//END for(var i=0; i<newItem.numChildren; i++)
				}//END if(newItem.tabChildren)
				
				if ( (doc.tabEnabled) || (doc.tabIndex >= 0) )
				{
					//trace(doc.name + " is eligible ");
					storageArray.push(doc);
				}
				else
				{
					//trace(doc.name + " is NOT eligible.  TabIndex = " + doc.tabIndex + ", tabEnabled = " + doc.tabEnabled);
				}
			}//END if(item is DisplayObjectContainer)
			
		}//END collectTabableChildren
		
		public function isUserLoggedIn():Boolean {
			if(isNotUndefined(userName)){
				if (userName != "Guest" && userName != "White Listed IP") {
					return true; // only if the userName isn't from a white listed IP or is a guest return true
				}
				
			}
			return false;
		}
		
		public function isUserWhiteListed():Boolean {
			if (userName == "White Listed IP") {
					return true;
			}
			return false;
		}
		
		private function handleMetaData(e:Event):void {
			
			var _metadata:XML = new XML(e.target.data);

			
			_metadata.ignoreWhitespace = true; 
			metaData.loadNewMetaData(_metadata);
			
			//ExternalInterface.call("showCard");
			 // Load the clip into the video player
			 if (_metadata.AssetType.name == "Video") {
				 
				front_mc.content_mc.visible = true;
				imageViewer.visible = false;
				imageViewer.removeImage();
				
				videoPlayer.loadClip(String(_metadata.Videos.Video.Video_Id));
				always_mc.mainDragBox.visible = true;
				imageMode = false;
				extensionsManager.displayButton("DOWNLOAD");

			}else{
				 
				front_mc.content_mc.visible = false;
				imageViewer.visible = true;
				imageViewer.loadImageXML(_metadata.Images.Image);
				always_mc.mainDragBox.visible = false;
				imageMode = true;
				extensionsManager.doNotDisplayButton("DOWNLOAD");
				
				
			 }
			
			 this.visible = true; 
			 curtain_mc.visible = true;
			 omniture.trackNewCard(metaData.title, metaData.vcmID, _metadata.AssetType.name);
			  drawCurtain();
		}
		
		

		public function unloadCard(e:TweenEvent):void{
			
			if (videoPlayer) {
				videoPlayer.stop();	
			}
			
		
		}	
		public function closeCueCard() {
			removeCard(); // call the remove function
		}
		
		public function removeCard(e:MouseEvent = null) {
			videoPlayer.stop();
			cueCard.signIn.close();
			cueCard.visible = false;
			curtain_mc.visible = false;
			if (flipped) {
				flipCueCard();
			}else {
				extensionsManager.closeExtensions();
				if (background_mc.transcript_mc.open) {
					background_mc.transcript_mc.closeTranscript(true);
				}
				this.disableKeyboardControl = true;
			}
			cueCard.always_mc.icons_left.peacock.stopAllLoading();
			
			trace("Close Card, reload: " + reloadPage); 
			if (reloadPage) { // if the page is supposed to be reloded
				ExternalInterface.call("CueCardManager.closeFlashArea", reloadPage, metaData.vcmID, metaData.contentID, metaData.firstPlayListName);
			}else{
				ExternalInterface.call("CueCardManager.closeFlashArea", reloadPage);
			}
			//ExternalInterface.call("CueCardManager.closeFlashArea", undefined, metaData.Asset_ID, metaData.contentID);
			//removeTween = new Tween(cueCard, "alpha", Strong.easeInOut, cueCard.alpha, 0, 1, true);
			//removeTween.addEventListener(TweenEvent.MOTION_FINISH,unloadCard);
		}
	
		private function minimizeCard(e:MouseEvent) {
			//window.minimize();
			return; // prevent original logic to move card to the right part of screen (use default windows/mac actions)
		
			var time:Number = 1.5;
			// Original Values
		
			
			minimize_obj.originalXscale =  cueCard.scaleX;
			minimize_obj.originalYscale =  cueCard.scaleY;
			
			var target_mc:MovieClip;
			if(flipped){
				target_mc = back_mc;
			}else{
				target_mc = front_mc;
			}
			
			var screens_array:Array = Screen.screens;
			var targetX:int = 0;
			for(var i:int = 0 ; i < screens_array.length ; i++){
				if((screens_array[i].visibleBounds.width + screens_array[i].visibleBounds.x ) > targetX){
					targetX = screens_array[i].visibleBounds.x + screens_array[i].visibleBounds.width;
				}
			
			}
			
			//Rotation
			minimize_obj.rotation = new Tween(target_mc, "rotationY", Strong.easeInOut, target_mc.rotationY, 60, time, true);
			// Position
			//minimize_obj.xTween = new Tween(window, "x", Strong.easeInOut, window.x, targetX - (this.RIGHT-100), time, true);
			//minimize_obj.xTween = new Tween(window, "y", Strong.easeInOut, window.y, 280, time, true);
			//Scale
			minimize_obj.scaleXTween = new Tween(target_mc, "scaleX", Strong.easeInOut, target_mc.scaleX, .3, time, true);
			minimize_obj.scaleTween = new Tween(target_mc, "scaleY", Strong.easeInOut, target_mc.scaleY, .4, time, true);
			minimize_obj.xTween.addEventListener(TweenEvent.MOTION_FINISH,minizeListener);
		}
		
		
		private function minizeListener(e:TweenEvent){
			minimize_obj.xTween.removeEventListener(TweenEvent.MOTION_FINISH,minizeListener);
			cueCard.addEventListener(MouseEvent.CLICK,restoreCard);
		}
// Bring the card back to its last possition
	private function restoreCard(e:MouseEvent) {
		var time:Number = 1.5;
		
		var target_mc:MovieClip;
		if(flipped){
			target_mc = back_mc;
		}else{
			target_mc = front_mc;
		}
		
		
		// Rotation
		minimize_obj.rotationTween = new Tween(target_mc, "rotationY", Strong.easeInOut, target_mc.rotationY, 0, time-.3, true);
		minimize_obj.rotationTween.addEventListener(TweenEvent.MOTION_FINISH, finalFlip);
		// Possition
	//	minimize_obj.xTween = new Tween(window, "x", Strong.easeInOut, window.x, minimize_obj.originalX, time, true);
	//	minimize_obj.xTween = new Tween(window, "y", Strong.easeInOut, window.y, minimize_obj.originalY, time, true);
		// Scale
		minimize_obj.scaleXTween = new Tween(target_mc, "scaleX", Strong.easeInOut, target_mc.scaleX, 1, time, true);
		minimize_obj.scaleTween = new Tween(target_mc, "scaleY", Strong.easeInOut, target_mc.scaleY, 1, time, true);
		// Restore Listener
		cueCard.removeEventListener(MouseEvent.CLICK,restoreCard);
		
	}
	public function resetSize(object:Object = null) {
		if(resizer){
			resizer.resetSizeFade();
			always_mc.extensions_bar.extensions_mc.resetContent();
			background_mc.transcript_mc.resetContent();
			if (!flipped){
				videoPlayer.resize(445, 250);
				videoPlayer.captions.captions_mc.alpha = 0;
			}else {
				back_mc.content_mc.line_mc.width = 415;
			}
			//extensionsManager.closeExtensions();
			background_mc.transcript_mc.closeTranscript();
			
		}
	}
	public function resetSizeAndCloseExtensions(e:Object = null) {
		extensionsManager.closeExtensions();
		resetSize();
	}
	
	private function flipFailSafe(e:TimerEvent) {
		flipFailSafeTimer.reset();
		if (!finalFlipReached) {
			
			if (!midwayReached) { // if it never reached the midway
				
			}else { // otherwise just finish the final flip
				var target_mc;
				if (flipped) { 
					target_mc = back_mc // finish turning over the back
				}else {
					target_mc = front_mc;// finish turning over the front
				}
				target_mc.rotationY = 0;
				finalFlip();
			}
		}
	}


// -----------FLIP THE CUE CARD OVER---------------
	public function flipCueCard(e:MouseEvent = null) {
		clearInterval(flipDelay);
		if(resizer.changed){
			resetSize();
		}
		midwayReached = false; // so that it can be set if the player stops flipping
		finalFlipReached = false;
		flipFailSafeTimer.start();
		extensionsManager.closeExtensions();
		background_mc.transcript_mc.closeTranscript();
		
		
		//background_mc.leftMenu_mc.shiftBackLeftChrome(.5);
		var secondRotation:int;
		var secondTarget_mc:MovieClip;
		root.transform.perspectiveProjection.projectionCenter = new Point(this.x , this.y );
		var targetX:Number;
		if (!flipped) {

			omniture.trackEvent("event40", "Flip Cue Card");
		   targetRotation = 180;
		   secondRotation = 90;
		   secondTarget_mc = front_mc
		   targetX = WIDTH / 2;
		   
		  
		}else{
			
			targetRotation = 0;
			secondRotation = -90;
			secondTarget_mc = back_mc;
			targetX = -WIDTH/2;
		}
		//trace("secondTarget_mc: " + secondTarget_mc.name);
		
		
		flipTween = new Tween(secondTarget_mc, "rotationY", Regular.easeIn, secondTarget_mc.rotationY, secondRotation, .7, true);
		//var moveTween:Tween = new Tween(secondTarget_mc, "x", Regular.easeIn, secondTarget_mc.x, targetX, .7, true);
		
		flipTween.addEventListener(TweenEvent.MOTION_STOP,midwayPoint);
	}

// SWITCH THE FRONT AND BACK GRAPHICS
	private function midwayPoint(e:TweenEvent){
		
		var secondRotation:int = 0;  // used to set up the final states
		var secondTarget_mc:MovieClip;
		var targetDepth:int 
		var targetX:Number; 
		if(!flipped){ // if we are flipping over
			flipped = true;
			targetX = -WIDTH / 2
			//cueCard.always_mc.icons_left.minimize_btn.visible = false;
			
			back_mc.rotationY = -90;
			secondTarget_mc = back_mc; // set back_mc to be the current side shown
			
			shadowBackground = back_mc.addChild(shadowBackground)as MovieClip;
			background_mc = back_mc.addChild(background_mc)as MovieClip;
			always_mc = back_mc.addChild(always_mc) as MovieClip; // move the top controls (always_mc) to the back of the card
			// Now reset the depths
			targetDepth = back_mc.numChildren - 1;
			back_mc.setChildIndex(background_mc,targetDepth)
			back_mc.setChildIndex(back_mc.content_mc,targetDepth); // Move the main content above the background
			targetDepth = back_mc.numChildren - 1;
			back_mc.setChildIndex(always_mc,targetDepth); // Place the always visible tools on top
			
			
			always_mc.mainDragBox.visible = false;
			
			
			front_mc.visible = false;
			
			back_mc.visible = true;
		}else{ // if we are flipping back
			flipped = false;
			
			//cueCard.always_mc.icons_left.minimize_btn.visible = true;
			back_mc.rotationY = 90;
			
						
			shadowBackground = front_mc.addChild(shadowBackground)as MovieClip;
			background_mc = front_mc.addChild(background_mc)as MovieClip;
			always_mc = front_mc.addChild(always_mc) as MovieClip; // move the top controls (always_mc) to the back of the card
			if(imageMode == true || videoPlayer.videoHasEnded == true || motionManager.preventDragging == true){
				always_mc.mainDragBox.visible = false;
			}else {
				always_mc.mainDragBox.visible = true;
			}
			targetDepth = front_mc.numChildren - 1;
			
			front_mc.setChildIndex(background_mc,targetDepth)
			front_mc.setChildIndex(front_mc.content_mc, targetDepth);
			front_mc.setChildIndex(imageViewer,targetDepth);
			front_mc.setChildIndex(always_mc, targetDepth);
			front_mc.setChildIndex(loading_mc,targetDepth);
			
			
			
			targetX = WIDTH / 2
			secondTarget_mc = front_mc; // set front_mc to be the current side shown
			front_mc.visible = true;
			back_mc.visible = false; 
		
		}
		
		midwayReached = true;
		
		var finalFlipTween:Tween = new Tween(secondTarget_mc, "rotationY", Regular.easeOut, secondTarget_mc.rotationY, secondRotation, .3, true);
		//var moveTween:Tween = new Tween(secondTarget_mc, "x", Regular.easeOut, secondTarget_mc.x, targetX, .3, true);
		finalFlipTween.addEventListener(TweenEvent.MOTION_STOP,finalFlip);
	}
	// When the last part of the flip is over
		private function finalFlip(e:TweenEvent = null) {
			
			// force object to its final rotation
			
			if (!flipped) {
				front_mc.rotationY = targetRotation;
				
			}else {
				back_mc.rotationY = targetRotation;
				
				if(metaData.expandGeneral){
					metaData.openExtension("General Information");
					metaData.expandGeneral = false;
				}
			}
			// Make sure to return the objects matrix to the original state after flipping
			restoreMatrixInfo();
			finalFlipReached = true;
			flipFailSafeTimer.stop();
			flipFailSafeTimer.reset();
			
		}

		private function restoreMatrixInfo(){
			// Make sure to return the objects matrix to the original state after flipping
			if(flipped){
				back_mc.transform.matrix  = matrix_obj.back_matrix;
			}else{
				front_mc.transform.matrix  = matrix_obj.front_matrix;
			}
			//ExternalInterface.call("resetDimensions");
			
		}
		
		private function setUpVideoPlayer():void {
			//AODPlayerLibrary = new AODPlayerLibraryInitilizer(this);
			//AODPlayerLibrary.addEventListener(AODPlayerLibraryInitilizer.READY , AODIsReady);
			
			videoPlayer = new VideoPlayer(this);
		}

		
		
		private function updatePrompt(event:Object,title:String):void{
				
		}
		public function loadCueCard(cardID) {
			
			videoPlayer.playClip("123363");	
		}

			
		public function setVideoPlayer(_videoPlayer) {
			videoPlayer = _videoPlayer;
		}
		
		public function signalNetworkTraffic(_name:String) {
			cueCard.always_mc.icons_left.peacock.startedLoading(_name)
		}
		public function signalNetworkTrafficStop(_name:String) {
			cueCard.always_mc.icons_left.peacock.finishedLoading(_name)
		}

		
// ANIMATE A SPECIFIC PROPETY OF A MOVIECLIP
		public function animateMovieClip(target_mc:Sprite, property:String ,target:Number,time:Number):Tween {
			var tween:Tween;
			tween = new Tween(target_mc, property, Regular.easeInOut, target_mc[property], target, time, true);
			return tween;
		}

	// --------- EVENTS ------------- 
	// TODO: Events
	
		private function initEvents():void {
			
			
			//always_mc.gripper.addEventListener(MouseEvent.MOUSE_DOWN, startMyDrag);
			//Colors.changeTint(always_mc.dragCorner, 0x000000, 1);
			always_mc.icons_right.flip_mc.addEventListener(MouseEvent.CLICK, function() {
				addBuffer(true);
				clearInterval(flipDelay);
				flipDelay = setInterval(flipCueCard, 150);
				
				
			});
			always_mc.icons_right.flip_mc.addEventListener(MouseEvent.MOUSE_OVER, hiLightFlip);
			always_mc.icons_right.flip_mc.addEventListener(MouseEvent.MOUSE_OUT, unHiLightFlip);
			always_mc.icons_right.flip_mc.buttonMode = true;
			always_mc.icons_right.flip_mc.useHandCursor = true;
			
			always_mc.icons_right.close_mc.addEventListener(MouseEvent.CLICK, removeCard);
			always_mc.icons_right.close_mc.addEventListener(MouseEvent.MOUSE_OVER, hiLightClose);
			always_mc.icons_right.close_mc.addEventListener(MouseEvent.MOUSE_OUT, unHiLightClose);
			always_mc.icons_right.close_mc.buttonMode = true;
			always_mc.icons_right.close_mc.useHandCursor = true;
			//background_mc.leftMenu_mc.close_btn.addEventListener(MouseEvent.CLICK, removeCard);
			//background_mc.leftMenu_mc.minimize_btn.addEventListener(MouseEvent.CLICK, minimizeCard);
		
			always_mc.dragCorner.addEventListener(MouseEvent.MOUSE_DOWN, startMovieClipExpand);
			
			
			always_mc.dragCorner.doubleClickEnabled = true;
			always_mc.dragCorner.addEventListener(MouseEvent.DOUBLE_CLICK, resetSizeAndCloseExtensions);
			//always_mc.dragCorner.buttonMode = true;
			//always_mc.dragCorner.useHandCursor = true;
			always_mc.dragCorner.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent) {
				if(!isResizing){
					showCustomCurser();
					Colors.changeTint(always_mc.dragCorner, 0x164E9B, 1);
				}
				
			});
			always_mc.dragCorner.addEventListener(MouseEvent.MOUSE_OUT, function(e:MouseEvent) {
				if(!isResizing){
					hideCustomCurser();
					Colors.changeTint(always_mc.dragCorner, 0x000000, 0);//ODL: 0xf36f21
				}
				
			});

			cueCard.stage.addEventListener(KeyboardEvent.KEY_UP, upKeyDetection);
			cueCard.stage.addEventListener(KeyboardEvent.KEY_DOWN, downKeyDetection);

		}
		
		public function downKeyDetection(event:KeyboardEvent) {
			if (!this.visible || disableKeyboardControl) {
				return;
			}
			if (imageMode) {
				//trace("#" + event.keyCode + " - " + event.ctrlKey);
				
				if(event.keyCode == 107) {
					imageViewer.zoom(null, .05);
				}
				if(event.keyCode == 187) {
					imageViewer.zoom(null, .05);
				}
				if(event.keyCode == 189) {
					imageViewer.zoom(null, -.06);
				}
				
				if(event.keyCode == 38) {
					imageViewer.zoom(null, .05);
				}
				if(event.keyCode == 40) {
					imageViewer.zoom(null, -.06);
				}

			}
		}
		
		private function upKeyDetection(event:KeyboardEvent) {
		
			if (!this.visible || disableKeyboardControl) {
				return;
			}
			if (!imageMode && videoPlayer.initilized == true && event.keyCode == 32) {  // SPACE BAR 
				try {
					videoPlayer.togglePlayPause();
				}catch (e:Error) {
					
				}
			}
			/*
				if (event.keyCode == 9) {
					//trace("#" + event.keyCode + " - " + event.ctrlKey);
				}
				try {
					//fmanager.setFocus(tabOrder_array[Math.ceil(Math.random() * 14)]);
				}catch (e:Error) {
					
				}*/
			}
			
		
		
		private function showCustomCurser() {
			Mouse.hide();
			stage.addEventListener(MouseEvent.MOUSE_MOVE, followWithCustomSymbol);
			customCurser.x = stage.mouseX;
			customCurser.y = stage.mouseY;
			customCurser.visible = true;
		}
		private function hideCustomCurser() {
			if(customCurser.visible == true){
				Mouse.show();
				customCurser.visible = false;
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, followWithCustomSymbol);
			}
		}
		private function followWithCustomSymbol(e:MouseEvent) {
			
			customCurser.x = stage.mouseX;
			customCurser.y = stage.mouseY;
		}
		
		private function initResizer() {
			if (!resizer) {
				resizer = new Resizer(always_mc.dragCorner);
				resizer.constrainTo(always_mc.dragCorner.x, 800, always_mc.dragCorner.y, 800);
				
				resizer.delay = 50;
				resizer.addResizableObject(background_mc.mainBackround);
				//resizer.addResizableObject(always_mc.extensions_bar.extensions_mc, Resizer.VERTICLE); // for download draw
				//resizer.addResizableObject(shadowBackground.down_tab, Resizer.VERTICLE);// for download draw
				resizer.addResizableObject(shadowBackground.main_bg); // for download and all others shadows
			//	 Transcript drawer
				resizer.addResizableObject(background_mc.transcript_mc, Resizer.HORIZONTAL); // for transcript draw
				resizer.addResizableObject(shadowBackground.trans_tab, Resizer.HORIZONTAL);
				resizer.addResizableObject(shadowBackground.transcript_bg); // for transcript draw
				
				
				resizer.addResizableObject(front_mc.content_mc.blackBackround);
				resizer.addResizableObject(front_mc.panels_mask);
				resizer.addResizableObject(always_mc.icons_right, Resizer.HORIZONTAL);
				resizer.addResizableObject(always_mc.extensions_bar, Resizer.VERTICLE);
				resizer.addResizableObject(always_mc.extensions_bar.bg, Resizer.WIDTH);
				resizer.addResizableObject(always_mc.extensions_bar.cueCard_icon, Resizer.HORIZONTAL);
				resizer.addResizableObject(always_mc.gripper, Resizer.WIDTH);	
				resizer.addResizableObject(always_mc.mainDragBox);
				
				
			// Image Viewer Object
				resizer.addResizableObject(imageViewer.controls_mc, Resizer.VERTICLE); // for the Image controls
				resizer.addResizableObject(imageViewer.controls_mc.base_mc, Resizer.WIDTH);
				resizer.addResizableObject(imageViewer.controls_mc.fitWindow_btn, Resizer.HORIZONTAL); 
				resizer.addResizableObject(imageViewer.controls_mc.fullScreen_btn, Resizer.HORIZONTAL); 
			
			// Video objects	
				resizer.addResizableObject(front_mc.content_mc.controls_mc, Resizer.VERTICLE); // for the video controls
				resizer.addResizableObject(front_mc.content_mc.controls_mc.progressBar_mc, Resizer.WIDTH);
				resizer.addResizableObject(front_mc.content_mc.controls_mc.base_mc, Resizer.WIDTH);
				resizer.addResizableObject(front_mc.content_mc.controls_mc.mute_btn, Resizer.HORIZONTAL); // for the video controls
				resizer.addResizableObject(front_mc.content_mc.controls_mc.volume_mc, Resizer.HORIZONTAL); // for the video controls
				resizer.addResizableObject(front_mc.content_mc.controls_mc.fullScreen_btn, Resizer.HORIZONTAL); // for the video controls
				resizer.addResizableObject(front_mc.content_mc.controls_mc.timeDisplay, Resizer.HORIZONTAL); // for the time display 
				resizer.addResizableObject(front_mc.content_mc.controls_mc.captions_btn, Resizer.HORIZONTAL); // for the captions

				// RESIZING EVENTS
				resizer.addEventListener(Resizer.RESIZING, onCardResize);
				resizer.addEventListener(Resizer.RESIZE_COMPLETE, onCardResizeComplete);
				resizer.addEventListener(Resizer.RESET_COMPLETE, onCardReset);
				resizer.addEventListener(Resizer.RESET_FADE, resetting);
				
			}
			
		}
	
		
		public function getTotalChange():Object {
			return resizer.getTotalChange();
		}
		
		public function resizerHasChanged():Boolean {
			if (resizer.changed) {
				return true;
			}else {
				return false;
			}
		}
		
		private function startMovieClipExpand(e:Event):void { // SETUP RESIZE
			if (motionManager.preventDragging == true) {
				return;
			}
			
			isResizing = true;
			resizer.startResize();
			videoPlayer.captions.captions_mc.alpha = 0;
			stage.addEventListener(MouseEvent.MOUSE_UP, stopMovieClipExpand);
			//front_mc.content_mc.vid.alpha = .3;
			addBuffer();
			hideCustomCurser();
		
			
		}

		public function onCardResize(e:Object) {
			//this.stage.stageHeight += e.changeY;
			//this.stage.stageWidth += e.changeX;
			
		
			updateStageDimensions(e);
			
			always_mc.extensions_bar.extensions_mc.masterDimensionResize(e.changeX, e.changeY);
			background_mc.transcript_mc.masterDimensionResize(e.changeX, e.changeY);
			if(imageMode == true){
				imageViewer.arrangeContent(); 
				imageViewer.centerControls();
			}
			if (flipped) { 
				back_mc.content_mc.line_mc.width += e.changeX;
			}
			signIn.arrangeContent();
		}
		private function onCardResizeComplete(e:Object) {
			if(!flipped){ // Only adjust the video size if we are facing front
				videoPlayer.resize(front_mc.content_mc.blackBackround.width, front_mc.content_mc.blackBackround.height)
				
				
				
				videoPlayer.captions.captions_mc.y = front_mc.content_mc.controls_mc.y - 15; // - 10 - videoPlayer.captions.captions_mc.height ;
				videoPlayer.captions.captions_mc.x = front_mc.content_mc.controls_mc.x + (front_mc.content_mc.controls_mc.base_mc.width / 2) - videoPlayer.captions.captions_mc.width / 2;
				videoPlayer.captions.captions_mc.alpha = 1;
				
				videoPlayer.updateProgressBar();
			}
			//front_mc.content_mc.vid.alpha = 1; 
			removeBuffer(); // remove the buffer
			// update the base demension for the download bar
			//always_mc.extensions_bar.extensions_mc.masterDimensionResize(e.totalChangeX, e.totalChangeY);
		}
		
		private function resetting(e:Object) {
			if (imageMode) {
				imageViewer.arrangeContent();
				imageViewer.centerControls();
			}
			signIn.arrangeContent();
		}
		
		private function onCardReset(e:Object) {
			
			
			//window.height = 510;
			//window.width = 550;
			//ExternalInterface.call("resetDimensions");
			cueCardHeight = HEIGHT;
			loading_mc.y = front_mc.content_mc.y +  front_mc.content_mc.blackBackround.y + ((front_mc.content_mc.blackBackround.height / 2) - 10);
			loading_mc.x = front_mc.content_mc.x + front_mc.content_mc.blackBackround.x + ((front_mc.content_mc.blackBackround.width / 2) - 45);
			if(flipped == false && imageMode == false){
				videoPlayer.resize(front_mc.content_mc.blackBackround.width, front_mc.content_mc.blackBackround.height)
				videoPlayer.updateProgressBar();
				front_mc.content_mc.captions_mc.x = -128;
				front_mc.content_mc.captions_mc.y =  270;
			}
			if (flipped) {
				back_mc.content_mc.line_mc.width = 415;
			}
			signIn.reset();
			videoPlayer.captions.captions_mc.alpha = 1;
			dispatchEvent(new Event(CARD_RESET_EVENT));
			
			
		}
		
		
		public function addBuffer(addBufferFromBothSides:Boolean = false) {
			
			if(addBufferFromBothSides){
				//this.x += 100;
				//this.y += 100;
			}
			//ExternalInterface.call("addBuffer",addBufferFromBothSides);
		}
		public function removeBuffer(removeBufferFromBothSides:Boolean = false) {
			
			//ExternalInterface.call("removeBuffer",removeBufferFromBothSides);
			if(removeBufferFromBothSides){
				//this.x -= 100;
			//	this.y -= 100;
			}
			
		}
		
		public function updateStageDimensions(e:Object) {
			//window.height += e.changeY;
			//	window.width += e.changeX;
			cueCardHeight += e.changeY
			
			//front_mc.content_mc.loading_mc.y = front_mc.content_mc.blackBackround.y + ((front_mc.content_mc.blackBackround.height / 2) - 10);
			//front_mc.content_mc.loading_mc.x = front_mc.content_mc.blackBackround.x + ((front_mc.content_mc.blackBackround.width / 2) - 45);
			loading_mc.y = front_mc.content_mc.y +  front_mc.content_mc.blackBackround.y + ((front_mc.content_mc.blackBackround.height / 2) - 10);
			loading_mc.x = front_mc.content_mc.x + front_mc.content_mc.blackBackround.x + ((front_mc.content_mc.blackBackround.width / 2) - 45);
			//ExternalInterface.call("scaleDimensions",e.changeX,e.changeY);
		}
		
		private function stopMovieClipExpand(e:Event) { // STOP RESIZE
			isResizing = false;
			resizer.stopResize();
			stage.removeEventListener(MouseEvent.MOUSE_UP, stopMovieClipExpand);
			
			if(String(e.target.name) != "dragCorner"){ // if the mouse is note over the dragg bar after expanding 
				Colors.changeTint(always_mc.dragCorner, 0x000000, 0);//OLD: 0xf36f21
				hideCustomCurser();
			}else{
				showCustomCurser();
				Colors.changeTint(always_mc.dragCorner, 0x164E9B, 1);
				// this is so that if it is still over, we dont revert the mouse to pointer.
			
			}
			
		}

		//always_mc.icons_right.flip_mc.filters = [glow];

		// Flip
		private function hiLightFlip(e:MouseEvent){
			Colors.changeTint(always_mc.icons_right.flip_mc.hit_btn, 0xf36f21, 1)
			Colors.changeTint(always_mc.icons_right.flip_mc.gfx, 0xFFFFFF, 1)
			always_mc.icons_right.flip_mc.filters = [bevel];
		}
		private function unHiLightFlip(e:MouseEvent){
			Colors.changeTint(always_mc.icons_right.flip_mc.hit_btn, 0x69707a, 1);
			Colors.changeTint(always_mc.icons_right.flip_mc.gfx, 0xDFE0E5, 1)
			always_mc.icons_right.flip_mc.filters = [];
		}
		// Close
		private function hiLightClose(e:MouseEvent){
			Colors.changeTint(always_mc.icons_right.close_mc.hit_btn, 0xc8234a, 1)
			Colors.changeTint(always_mc.icons_right.close_mc.gfx, 0xFFFFFF, 1)
			always_mc.icons_right.close_mc.filters = [bevel];
		
		}
		private function unHiLightClose(e:MouseEvent){
			Colors.changeTint(always_mc.icons_right.close_mc.hit_btn, 0x69707a, 1);
			Colors.changeTint(always_mc.icons_right.close_mc.gfx, 0xDFE0E5, 1)
			always_mc.icons_right.close_mc.filters = [];
			
		}

		// Utility for finding and replacing text
		private function findReplace( lookUp:String, replace:String, orig:String ){
			if (replace == "_blank") {
				replace = "";
			}
			if( orig.indexOf( lookUp ) > -1 ){
			
				return orig.split( lookUp ).join( replace );
			}
			return orig;
		}
		// CHECK FOR UNDEFINED
		private function isNotUndefined(check_obj){
			if(check_obj == undefined || check_obj == "undefined" || check_obj == null || check_obj == "null" || String(check_obj) == "" || String(check_obj) == " "){
				return false;
			}else{
				return true;
			}
		}

	}
}