﻿package 
{
	
	import fl.controls.TextArea;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.StyleSheet;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author Ari
	 */
	public class Transcript extends MovieClip 
	{
		
		public var open:Boolean = false;
		
		private var resizer:Resizer;
		private var contentStartX:Number;
		private var buttonStartX:Number;
		private var openX:Number; 
		private var targetX:Number;
		private var _cueCard:AS3CueCard;
		
		private var ds:DropShadowFilter;
		private var text_mc:CSSTextArea;
		private var transHeight:Number = 298;
		private var mask_mc:Sprite;
		
		private var transcriptContentAvailable:Boolean = false;
		private var activityContentAvailable:Boolean = false;
		private var lessonsContentAvailable:Boolean = false;
		private var hasMoved:Boolean = false;
		
		// Constants
		private const TEXT_RIGHT:Number = -8;
		private const TEXT_TOP:Number = -91;
		private const BUTTON_RIGHT:Number = 320;
		private var failSafeTimer:Timer;
		private var transcriptTweenCompleted:Boolean = false;
		
		private var transcriptTextFormat:TextFormat;
		private var activityTextFormat:TextFormat;
		private var lessonsTextFormat:TextFormat;
		
		private var transcriptText:String;
		private var activityText:String;
		private var lessonsText:String;
		
		private var activityCSS:StyleSheet;

		public function Transcript() {
			
			contentStartX = this.content_mc.x;
			buttonStartX = this.transcript_btn.x;
			openX = transcript_btn.x + BUTTON_RIGHT;
			
			
			failSafeTimer = new Timer(700, 1);
			failSafeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, openCloseFailSafe);
			
			activityCSS = new StyleSheet();
			
			activityCSS.parseCSS(' p { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; } a { color:#0000FF; text-decoration:underline; }  a:hover { text-decoration:underline; color:#FF0000;}');
			



			
			
			// Set Up Text Box
			text_mc = new CSSTextArea();
			text_mc.wordWrap = true;
			text_mc.tabEnabled = false;
			content_mc.addChild(text_mc);
			//text_mc.text = " kl jewlk ewkljrlqkejkqwel foief ;iqwe foqwer opqiwjr;qlkr l;qwjkf oiqe ofqopi fopqi opqi eopqwier pweoqr-0qwer p dfkljsldkflksadklfsaksjdflklsdlkfjnsdlfklklsdlkfjuiufgdl;rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrl; re;lrl;r;kl ewl;tlkrej dsfkj ewkl jdskl jdsklajhfkasjhf ilafi weuh fk seaifui esiu fhiawu fh askf kasuh fiouha fiuesh fnsdlfklklsdlkfj\nsdlfklklsdlkf   dlskj ds ksdklf klsdjklsdjkl f lslfklklsdksjdflk sdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkflksjdflklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklklsdlkfj\nsdlfklksjafkl sdjkfl sfklsdlkfj\nsdlfklskjf ljkdslkf\nlkdsjkl fslkfljksdf";
			text_mc.y = TEXT_TOP;
			text_mc.setSize(200, transHeight);
			text_mc.x = TEXT_RIGHT - text_mc.width
			text_mc.editable = false;
			text_mc.textField.tabEnabled = false;
			text_mc.textField.wordWrap = true;
			
			
				//font settings
			var font:TextFormat = new TextFormat();
            
				font.font = 'Arial';
				font.color = 0x000000;
					
			
				text_mc.textField.styleSheet = activityCSS;
				text_mc.setStyle("textFormat", 	font);	

			
		

		// SET UP THE TRANSCRIPT MASK
			mask_mc = new Sprite();
			mask_mc.graphics.beginFill(0xFF0000, 1);
			mask_mc.graphics.lineTo(300,0);
			mask_mc.graphics.lineTo(300, transHeight);
			mask_mc.graphics.lineTo(0, transHeight);
			mask_mc.graphics.lineTo(0, 0);
			mask_mc = this.addChild(mask_mc) as Sprite;
			
			
			//mask_mc.x = this.x;
			mask_mc.y = TEXT_TOP;
			
			text_mc.mask = mask_mc;
			
			eventSetUp();
			
		}

		private function eventSetUp() {
			//content_mc.transcript_tab.over_img.visible = false;
			//content_mc.activity_tab.over_img.visible = false;
			
			// TRANSCRIPT BUTTON
			
			transcriptTextFormat = content_mc.transcript_tab.label_txt.getTextFormat();
			activityTextFormat = content_mc.activity_tab.label_txt.getTextFormat();
			lessonsTextFormat = content_mc.activity_tab.label_txt.getTextFormat();
			
			content_mc.activity_tab.wedge.visible = false;
			
			this.transcript_btn.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent) {
				if(transcriptContentAvailable){
				
					
					//content_mc.wedge.visible = false;
					//content_mc.transcript_tab.over_img.visible = false
					
					if (!open) {
						
						setText(transcriptText);
						if(activityContentAvailable){
							content_mc.activity_tab.visible = false;
							activity_btn.visible = false;
							_cueCard.shadowBackground.trans_tab.activity_img.visible = false;
						}
						if(lessonsContentAvailable){
							content_mc.lessons_tab.visible = false;
							lessons_btn.visible = false;
							_cueCard.shadowBackground.trans_tab.lessons_img.visible = false;
						}
						
					}
					startTranscriptDrag();
				
				}
			});
			this.transcript_btn.addEventListener(MouseEvent.CLICK, toggleOpenClose);
			this.transcript_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				if (!open) {
					if (transcriptContentAvailable) {
						
					
						//content_mc.transcript_tab.over_img.visible = true; // display the graphical over image
						
						content_mc.setChildIndex(content_mc.transcript_tab, content_mc.numChildren - 1);
						content_mc.activity_tab.wedge.visible = false; // remove the activities tab wedge
						content_mc.transcript_tab.wedge.visible = true;
						transcriptTextFormat.color = 0xFFFFFF//0x8D949C;
						content_mc.transcript_tab.label_txt.setTextFormat(transcriptTextFormat);
					}
				}
			});
			this.transcript_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				transcriptTextFormat.color = 0x000000;
				content_mc.transcript_tab.label_txt.setTextFormat(transcriptTextFormat);
			});
			
			// ACTIVITY BUTTON
			
			this.activity_btn.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent) {
				if(activityContentAvailable){
					content_mc.transcript_tab.visible = false;

					if (!open) {
						setText(activityText);
						
						transcript_btn.visible = false;
						_cueCard.shadowBackground.trans_tab.transcript_img.visible = false;
						if(lessonsContentAvailable){
							lessons_btn.visible = false;
							_cueCard.shadowBackground.trans_tab.lessons_img.visible = false;
							content_mc.lessons_tab.visible = false;
						}
						
					}
					startTranscriptDrag();
				}
			});
			this.activity_btn.addEventListener(MouseEvent.CLICK, toggleOpenClose);
			this.activity_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				if (!open) {
					if(lessonsContentAvailable){
						//content_mc.activity_tab.over_img.visible = true; // display the graphical over image
						content_mc.transcript_tab.wedge.visible = false; // remove the transcript tab wedge
						content_mc.activity_tab.wedge.visible = true; // remove the activity tab wedge
						content_mc.lessons_tab.wedge.visible = false; // add the lessons tab wedge
						content_mc.setChildIndex(content_mc.activity_tab, content_mc.numChildren - 1);
						activityTextFormat.color = 0xFFFFFF//0x8D949C;
						content_mc.activity_tab.label_txt.setTextFormat(activityTextFormat);
					}
				}
			});
			this.activity_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				//content_mc.activity_tab.over_img.visible = false;
				activityTextFormat.color = 0x000000;
					content_mc.activity_tab.label_txt.setTextFormat(activityTextFormat);
			});
			

			// make the mouse hand visible on roll over
			transcript_btn.buttonMode = true;
			transcript_btn.useHandCursor = true;
			activity_btn.buttonMode = true;
			activity_btn.useHandCursor = true;
			
			
		// LESSONS BUTTON
			
			this.lessons_btn.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent) {
				if(lessonsContentAvailable){
					
					
					if (!open) {
						setText(lessonsText);
						if(transcriptContentAvailable){
							transcript_btn.visible = false;
							content_mc.transcript_tab.visible = false;
							_cueCard.shadowBackground.trans_tab.transcript_img.visible = false;
						}
						if(activityContentAvailable){
							activity_btn.visible = false;
							_cueCard.shadowBackground.trans_tab.activity_img.visible = false;
							content_mc.activity_tab.visible = false;
						}
						
						
					}
					startTranscriptDrag();
				}
			});
			this.lessons_btn.addEventListener(MouseEvent.CLICK, toggleOpenClose);
			this.lessons_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				if (!open) {
					//if(activityContentAvailable){
						//content_mc.activity_tab.over_img.visible = true; // display the graphical over image
						content_mc.transcript_tab.wedge.visible = false; // remove the transcript tab wedge
						content_mc.activity_tab.wedge.visible = false; // remove the activity tab wedge
						content_mc.lessons_tab.wedge.visible = true; // add the lessons tab wedge
						content_mc.setChildIndex(content_mc.lessons_tab, content_mc.numChildren - 1);
						lessonsTextFormat.color = 0xFFFFFF//0x8D949C;
						content_mc.lessons_tab.label_txt.setTextFormat(lessonsTextFormat);
					//}
				}
			});
			this.lessons_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				//content_mc.activity_tab.over_img.visible = false;
				lessonsTextFormat.color = 0x000000;
					content_mc.lessons_tab.label_txt.setTextFormat(lessonsTextFormat);
			});
			

			// make the mouse hand visible on roll over
			transcript_btn.buttonMode = true;
			transcript_btn.useHandCursor = true;
			activity_btn.buttonMode = true;
			activity_btn.useHandCursor = true;	
			lessons_btn.buttonMode = true;
			lessons_btn.useHandCursor = true;	
			
			
			
		}
		
		
		public function setTranscript(_txt:String) {
			transcriptText = _txt;
			var textLength:int =  _txt.length;
			
			if (textLength <= 45) {
				disableTranscript();
				return;
			}
			if (_txt == '' || _txt == " "  || _txt == null || _txt == "null"){
				
				disableTranscript();
			}else {
				enableTranscript();
			}
		}
		
		public function setText(transcript_str) {
		
			
			try {
				//trace("~ no changes");
				
			//	transcript_str = findReplace("<br/>", " ", transcript_str);
		//	transcript_str = findReplace("&nbsp;", " ", transcript_str);
				
			//transcript_str = findReplace("<p>"," ",transcript_str);
			//transcript_str = findReplace("</p>", " ", transcript_str);
			//str_replace(array("\r\n", "\n", "\r"), "", $text); 
			transcript_str = findReplace("\n", "", transcript_str);
			transcript_str = findReplace("\r", "", transcript_str);
			transcript_str = findReplace("</p>", "</p><br/>", transcript_str);
			transcript_str = findReplace("&rsquo;", "'", transcript_str);
			transcript_str = findReplace("&hellip;" , "...", transcript_str);
			transcript_str = findReplace('&ldquo;', '"', transcript_str);
			transcript_str = findReplace('&rdquo;', '"', transcript_str);
			
				//trace("! transcript_str: " + transcript_str);
				text_mc.htmlText = transcript_str;
				return true;
			}catch (e:Error) {
				_cueCard.confirmation.confirm("The text was not able to display", "OK", new Function(),true);
				trace("~!ERROR: " + e.toString());
				return false;
			}
		}
		
		public function setActivity(_activityText) {
			activityText = _activityText;
			if (isNotUndefined(_activityText)) { // if there is valid activity text
				enableActivity();
			}else {
				disableActivity();
			}
		}
		public function setLessons(_lessonText) {
			lessonsText = _lessonText;
			if (isNotUndefined(_lessonText)) { // if there is valid activity text
				enableLessons();
			}else {
				disableLessons();
			}
		}
		
		public function disableTranscript() {
			//content_mc.alpha = .6;
			//content_mc.transcript_tab.alpha = .6;
			transcriptContentAvailable = false;
			if (open) {
				closeTranscript(true);
			}
			// remove the transcript tab
			content_mc.transcript_tab.visible = false;
			transcript_btn.visible = false;
			_cueCard.shadowBackground.trans_tab.transcript_img.visible = false;
		}
		private function enableTranscript() {
			transcriptContentAvailable = true;
			//content_mc.transcript_tab.alpha = 1;
			// Show Transcript drawer
			content_mc.transcript_tab.visible = true;
			transcript_btn.visible = true;
			_cueCard.shadowBackground.trans_tab.transcript_img.visible = true;
			
		}
		public function disableActivity() {
			activityContentAvailable = false;
			//content_mc.activity_tab.alpha = .6;
			// Remove the activity drawere
			content_mc.activity_tab.visible = false;
			activity_btn.visible = false;
			_cueCard.shadowBackground.trans_tab.activity_img.visible = false;
		}
		private function enableActivity() {
			activityContentAvailable = true;
			//content_mc.activity_tab.alpha = 1;
			// Display the activity drawer
			content_mc.activity_tab.visible = true;
			activity_btn.visible = true;
			_cueCard.shadowBackground.trans_tab.activity_img.visible = true;
		}
		
		public function disableLessons() {
			trace("~disableLessons");
			lessonsContentAvailable = false;
			//content_mc.activity_tab.alpha = .6;
			// Remove the activity drawere
			content_mc.lessons_tab.visible = false;
			lessons_btn.visible = false;
			_cueCard.shadowBackground.trans_tab.lessons_img.visible = false;
		}
		private function enableLessons() {
			trace("~enableLessons");
			lessonsContentAvailable = true;
			//content_mc.activity_tab.alpha = 1;
			// Display the activity drawer
			content_mc.lessons_tab.visible = true;
			lessons_btn.visible = true;
			_cueCard.shadowBackground.trans_tab.lessons_img.visible = true;
			
			if (activityContentAvailable) {
				content_mc.lessons_tab.y = 82
				lessons_btn.y = 110;
				_cueCard.shadowBackground.trans_tab.lessons_img.y = 180
			}else {
				content_mc.lessons_tab.y = -6
				lessons_btn.y = 20;
				_cueCard.shadowBackground.trans_tab.lessons_img.y =95
				
			}
			
		}
		
		
		public function set cueCard(_cueCardX) {
			this._cueCard = _cueCardX;
			initResizer();
				//_cueCard.shadowBackground.trans_tab.activity_img.wedge.visible = false;
			//_cueCard.shadowBackground.trans_tab.transcript_img.wedge.visible = false;
		}
		
		private function initResizer() {
			resizer = new Resizer(transcript_btn);
			resizer.addResizableObject(content_mc, Resizer.HORIZONTAL);
			resizer.addResizableObject(_cueCard.shadowBackground.transcript_bg, Resizer.WIDTH);
			resizer.addResizableObject(_cueCard.shadowBackground.trans_tab, Resizer.HORIZONTAL);
			resizer.constrainTo(buttonStartX, 700, transcript_btn.y, transcript_btn.y);
			resizer.addEventListener(Resizer.RESIZING, isResizing);
			resizer.addEventListener(Resizer.RESIZING, _cueCard.updateStageDimensions);
			resizer.addEventListener(Resizer.RESET_COMPLETE, _resetComplete);
			
		}
		private function _resetComplete(event:Object = null) {
			text_mc.visible = false;
		}
		
		
		private function startTranscriptDrag(e:Event = null):void { // SETUP RESIZE
			hasMoved = false;
			//content_mc.openClose_arrow.rotation = 90;
			
			resizer.startResize();		
			stage.addEventListener(MouseEvent.MOUSE_UP, stopTranscriptDrag);
			
			open = true;
			
		
		}
		
		private function stopTranscriptDrag(e:Event) { // STOP RESIZE
			
			resizer.stopResize();
		
			if (transcript_btn.x == buttonStartX) {
				open = false;
				resetTabs();
			
			}
			stage.removeEventListener(MouseEvent.MOUSE_UP, stopDrag);
			
			
		}
		
		private function isResizing(e:Object) {
			if (e.changeX != 0 || e.changeY != 0){
				hasMoved = true
			}
			lessons_btn.x = activity_btn.x = transcript_btn.x;
			mask_mc.width = content_mc.x - mask_mc.x;
		
			arrangeContent();
			
			
		}
		
		public function arrangeContent():void {
			var _width:Number = (content_mc.x - mask_mc.x) - 8;
			if (_width < 10) {
				text_mc.visible = false;
			}else {
				text_mc.visible = true;
			}
			text_mc.x = TEXT_RIGHT - _width;
			text_mc.setSize(_width, transHeight);
			//mask_mc.width = _width;
			
			mask_mc.height = transHeight;
			text_mc.invalidate();
			
		}
		
		public function masterDimensionResize(totalChangeX:Number, totalChangeY:Number) {
			transHeight += totalChangeY;
			arrangeContent();
		}
		
		
		private function toggleOpenClose(e:MouseEvent) {
			if (hasMoved) {
				return;
			}
			if (open) {
				closeTranscript(true);
			}else {
				// Transcript
				if (e.currentTarget == transcript_btn) {
					if(transcriptContentAvailable){ // only open if there is content in the transcript
						content_mc.activity_tab.visible = false;
						activity_btn.visible = false;
						_cueCard.shadowBackground.trans_tab.activity_img.visible = false;
						
						content_mc.lessons_tab.visible = false;
						lessons_btn.visible = false;
						_cueCard.shadowBackground.trans_tab.lessons_img.visible = false;
						
						
						setText(transcriptText);
						openTranscript(true);
					}
				}
				// Activity
				if (e.currentTarget == activity_btn) {
				
					if (activityContentAvailable) { // only open if there is content in the activities section
						setText(activityText);	
						
						

			
				
						
						content_mc.transcript_tab.visible = false;
						transcript_btn.visible = false;
						_cueCard.shadowBackground.trans_tab.transcript_img.visible = false;
						
						content_mc.lessons_tab.visible = false;
						lessons_btn.visible = false;
						_cueCard.shadowBackground.trans_tab.lessons_img.visible = false;
					
					
						openTranscript(true);
						
					}
				}
				// Lessons
				if (e.currentTarget == lessons_btn) {
				
					if (lessonsContentAvailable) { // only open if there is content in the activities section
						setText(lessonsText);
						
						content_mc.transcript_tab.visible = false;
						transcript_btn.visible = false;
						_cueCard.shadowBackground.trans_tab.transcript_img.visible = false;
						
						content_mc.activity_tab.visible = false;
						activity_btn.visible = false;
						_cueCard.shadowBackground.trans_tab.activity_img.visible = false;
						openTranscript(true);
						
					}
				}
				
			}
		}
		
		public function resetContent() {
			transHeight = 298;
			arrangeContent()
		}
		
	
		
	// Slide the Transcript Open
		public function openTranscript(internalCall:Boolean = false) {
			if (transcriptContentAvailable == false && activityContentAvailable == false) {
				return
			}
			text_mc.visible = true;
			var _width:Number;
			if (internalCall == true) {
				resizer.changed = true;
				transcript_btn.__x = transcript_btn.x; // save the possition before the tween
				var difference:Number = openX - this.transcript_btn.x; 
				var avalibleWidth:Number = _cueCard.motionManager.getEmptyScreenSpace().width;
				
				if (difference < avalibleWidth) {
					targetX = openX;
					_width = BUTTON_RIGHT - 8;
				}else {
					_width = avalibleWidth - 8;
					targetX = transcript_btn.x + avalibleWidth
				}
				_cueCard.motionManager.keepOnScreen(targetX - transcript_btn.x, 0, .64);
				var closeTween:Tween = animateMovieClip(this.transcript_btn, "x", targetX, .64);
				closeTween.addEventListener(TweenEvent.MOTION_FINISH, barHasOpend);
				closeTween.addEventListener(TweenEvent.MOTION_CHANGE, whileBarIsTweening);
			}
			
			
			//mask_mc.width = 0; contentStartX;// + openX;
			text_mc.setSize(_width, text_mc.height);
			text_mc.x = TEXT_RIGHT - text_mc.width;
			//content_mc.openClose_arrow.rotation = 90;
			text_mc.visible = true;
			failSafeTimer.start();
			transcriptTweenCompleted = false;
			_cueCard.omniture.trackEvent("event36", "Open Transcript");
		
		}
		
		private function barHasOpend(e:TweenEvent = null) {
			open = true;
			lessons_btn.x = activity_btn.x = transcript_btn.x
			
			transcriptTweenCompleted = true;
			failSafeTimer.stop();
			failSafeTimer.reset();
			
			
			_cueCard.fmanager.setFocus(text_mc);
			text_mc.verticalScrollPosition = 0;
			
		}
	
	
	// Slide the Transcript Close
		public function closeTranscript(internalCall:Boolean = false) {
			var closeTween:Tween
			this.transcript_btn.__x = transcript_btn.x; // save the possition before the tween
			if(internalCall == false) {
				if (resizer && resizer.changed) { // only perform this if the resizer has been initilized
					resizer.resetSizeFade(); // if the call is comming from outside the transcript
					//content_mc.openClose_arrow.rotation = -90;
					open = false;
				}
				closeTween = animateMovieClip(this.transcript_btn, "x", buttonStartX, .64); // close the transcript very rappidly
			}else { // this is comming from the close button */
				closeTween = animateMovieClip(this.transcript_btn, "x", buttonStartX, .64);
				failSafeTimer.start();
			}
			closeTween.addEventListener(TweenEvent.MOTION_FINISH, barHasClosed);
			closeTween.addEventListener(TweenEvent.MOTION_CHANGE, whileBarIsTweening);
			
			transcriptTweenCompleted = false;
		
		}
		
		private function openCloseFailSafe(e:TimerEvent) {
			if (!transcriptTweenCompleted) {
				
				if(open){ // if the bar was in the process of openning but never finished
					this.transcript_btn.x = buttonStartX;
					whileBarIsTweening();
					barHasClosed();  // close the bar
				}else {
					
					this.transcript_btn.x = targetX;
					whileBarIsTweening();
					barHasOpend();  // open the bar
				}
			}
			failSafeTimer.stop();
			failSafeTimer.reset();
		}
		
	
		
	// FIRES OFF WHILE THE TRANSCRIPT IS OPENNING OR CLOSING FROM A TWEEN
		private function whileBarIsTweening(e:TweenEvent = null) {
			var changeX:Number = transcript_btn.x - transcript_btn.__x
			transcript_btn.__x = transcript_btn.x; // reset for the next change
			
			this.content_mc.x += changeX;
			
			_cueCard.shadowBackground.transcript_bg.width += changeX;
			_cueCard.shadowBackground.trans_tab.x += changeX;
			
			_cueCard.updateStageDimensions({changeX:changeX,changeY:0})
		
			// Mask
			
			mask_mc.width = content_mc.x - mask_mc.x;
			
			
		}
		
		private function resetTabs() {
			// now that the bar is in the closed possition, make sure that all the transcript and activity assets are turned on
			if(transcriptContentAvailable){
				content_mc.transcript_tab.visible = true;
				transcript_btn.visible = true;
				_cueCard.shadowBackground.trans_tab.transcript_img.visible = true;
			}
			if(activityContentAvailable){
				content_mc.activity_tab.visible = true;
				activity_btn.visible = true;
				_cueCard.shadowBackground.trans_tab.activity_img.visible = true;
			}
			if(lessonsContentAvailable){
				content_mc.lessons_tab.visible = true;
				lessons_btn.visible = true;
				_cueCard.shadowBackground.trans_tab.lessons_img.visible = true;
			}
			//content_mc.wedge.visible = true;
			//content_mc.activity_tab.extension.visible = false;
		
		}
		
		private function barHasClosed(e:TweenEvent = null) {
			content_mc.x = contentStartX;
			transcript_btn.x = buttonStartX;
			lessons_btn.x = activity_btn.x = transcript_btn.x
			open = false;
			text_mc.visible = false;
			resetTabs();
			
			transcriptTweenCompleted = true;
			failSafeTimer.stop();
			failSafeTimer.reset();
			
		}

		// ANIMATE A SPECIFIC PROPETY OF A MOVIECLIP
		private function animateMovieClip(target_mc:Sprite, property:String ,target:Number,time:Number):Tween {
			var animateTween:Tween;
			animateTween = new Tween(target_mc, property, Regular.easeInOut, target_mc[property], target, time, true);
			return animateTween;
		}
		private function isNotUndefined(check_obj){
			if(check_obj == undefined || check_obj == "undefined" || check_obj == null || check_obj == "null" || String(check_obj) == "" || String(check_obj) == " "){
				return false;
			}else{
				return true;
			}
		}
		private function findReplace( lookUp:String, replace:String, orig:String ){
			
			if ( orig.indexOf( lookUp ) > -1 ) {
			
			
				return orig.split( lookUp ).join( replace );
			}
			return orig;
		}
		
	}
}