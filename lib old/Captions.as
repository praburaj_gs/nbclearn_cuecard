package  
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import GenericButton;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Ari Oshinsky
	*/
	public class Captions {
		public var captions_mc:MovieClip
		private var captionsArray:Array;
		private var captionsTimer:Timer;
		private var player:Object;
		public var hasCaptions:Boolean;
		private var captions_btn:MovieClip;
		
		private var currentLanguage:String = "English"
		public var availableLanguages:Array;
		private var langButton_array:Array;
		
		private var langSelectFadeTimer:Timer;
		private var mainDragBox:MovieClip;
		
		
		public function Captions(_captions_mc:MovieClip,_player:Object,_captions_btn:MovieClip, _mainDragBox:MovieClip) 
		{
			
			mainDragBox = _mainDragBox;
			availableLanguages = new Array();
			captionsArray = new Array();
			langButton_array = new Array();
			captions_btn = _captions_btn;
			player = _player;
			captions_mc = _captions_mc;
			//MovieClip(captions_mc.root).trace_txt.text = "TEST TRACE";
			captionsTimer = new Timer(300);
			captionsTimer.reset();
			captionsTimer.addEventListener(TimerEvent.TIMER, testForCaption);
			captions_btn.capSelect_bg.visible = false;
			langSelectFadeTimer = new Timer(2000, 1);
			langSelectFadeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, fadeOutLangBox);
			
			captions_btn.capSelect_bg.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				showLangSelection();
			});
		}
		
		public function clearLangCaptions() {
			for (var index3:int = 0 ; index3 < langButton_array.length ; index3++) {
				// REMOVE THE LANGUIAGE BUTTONS
				try{
					captions_btn.removeChild(langButton_array.pop());
				}catch (e:Error) {
					trace("! could not remove a lang");
				}
			}
		}
		
		public function showLangSelection() {
			//return;
			if (availableLanguages.length <= 1) {
				return;
			}
			captions_btn.capSelect_bg.visible = true;
			for (var index:int = 0 ; index < langButton_array.length ; index++) {
				langButton_array[index].visible = true;
			}
			langSelectFadeTimer.reset();
			langSelectFadeTimer.stop();
			
		}
		public function hideLangSelection() {
			langSelectFadeTimer.reset();
			langSelectFadeTimer.start();
		}
	
		private function fadeOutLangBox(e:TimerEvent = null) {
			captions_btn.capSelect_bg.visible = false;
			for (var index:int = 0 ; index < langButton_array.length ; index++) {
				langButton_array[index].visible = false;
			}
			//mainDragBox.visible = false;
		}
	
	
		
		public function setCaptionInfo(_xml:XMLList) {
			
			currentLanguage = "English";
			
			// clear clear out all the language arrays
			if (availableLanguages) {
				 while(availableLanguages.length > 0 ) {
					
					 var lang:String = availableLanguages.pop(); // Pop Out the last languige in the array
					 if (captionsArray[lang]) { // if that language is part of the internal array
						while (captionsArray[lang].length > 0) {  // run through all the parts of the lanuage
							captionsArray[lang].pop();  // and pop them off.
						}
					}
					traceOut("POP OFF: " + lang);
				}
				
			}
			
			
			
			
			
			
		
			
			captionsArray = new Array();  // redefine the array
			captionsArray["English"] = new Array();
			
			if (_xml == null) {
				hasCaptions = false;
				return;
			}else {
				hasCaptions = true;
				
				availableLanguages.push("English");
				
			}
		
			
			
			for (var i:Number = 0; i < _xml.p.length() ;  i++) {
				var xml_cap:Object = _xml.p[i];
				var caption:Object = new Object();
				// Begin Time
				var begin_array:Array = String(xml_cap.attribute("begin")).split(":") ;
				var begin_min:Number = parseFloat(begin_array[1])*60;
				var begin_sec:Number = parseFloat(begin_array[2]);
				caption.begin = Number(begin_min) + Number(begin_sec);
			
					
				// End Time
					
				var end_array:Array = String(xml_cap.attribute("end")).split(":") ;
				var end_min:Number = parseFloat(end_array[1])*60;
				var end_sec:Number = parseFloat(end_array[2]);
				caption.end = Number(end_min)+Number(end_sec);
			
				
				caption.text = xml_cap;
					
				captionsArray["English"].push(caption);
			}
			if (captions_btn.cc_gfx.currentFrameLabel == "Active") {
				startTracking();
			}
			
		}
// ---------------- SPANISH SPANISH  -----------

		// ADDITIONAL CAPTIONS FUNCTION: parses the spanish languige captions and provides the option to change Lang to the user.
		
		public function setAdditionalCaptions(_xml:XMLList) {
			
			
			if (_xml == null) {
				
				return; // if there is no additional language don't do anything.
			}
			
			traceOut("!-setAdditionalCaptions-");
		//	MovieClip(captions_mc.root).trace_txt.text += "\n2. " + _xml.attribute("language")toString();// _xml.Additional_Captions.Captions.toString();
			
			//traceOut("_xml.length() = " + _xml.length());
			for (var index1:int = 0 ; index1 < _xml.length() ; index1++) {
			
				var lang:String = _xml[index1].attribute("language");
				availableLanguages.push(lang);
				captionsArray[lang] = new Array();  // initilize the new language array
				
			// Go through each language and parse it into the captions array
							
				for (var i:Number = 0; i < _xml[index1].p.length() ;  i++) {
					var xml_cap:Object = _xml[index1].p[i];
					var caption:Object = new Object();
					// Begin Time
					var begin_array:Array = String(xml_cap.attribute("begin")).split(":") ;
					var begin_min:Number = parseFloat(begin_array[1])*60;
					var begin_sec:Number = parseFloat(begin_array[2]);
					caption.begin = Number(begin_min) + Number(begin_sec);
				
						
					// End Time
						
					var end_array:Array = String(xml_cap.attribute("end")).split(":") ;
					var end_min:Number = parseFloat(end_array[1])*60;
					var end_sec:Number = parseFloat(end_array[2]);
					caption.end = Number(end_min)+Number(end_sec);
				
					
					caption.text = xml_cap;
					
					
					captionsArray[lang].push(caption);
				}		
			}
			//traceOut(".....");
		
				if (availableLanguages.length > 1) {
					for (var index2:int = 0 ; index2 < availableLanguages.length ; index2++) {
					
				
						//traceOut("Setting : " +  availableLanguages[index2]);
						var lang_btn:DynamicSprite = new DynamicSprite();
						lang_btn.x = captions_btn.capSelect_bg;
						if (index2 == 0) {
							
							lang_btn.y = captions_btn.hit_btn.y - 20;
						}else {
						
							lang_btn.y = captions_btn.hit_btn.y - 20 - (index2 * 20);
						}
						
						var lang_txt:TextField;
						if(!lang_btn.lang_txt){ // only create the lang text asset if there isn't one already
							lang_txt = new TextField();
							lang_btn.lang_txt = lang_btn.addChild(lang_txt);
						}else {
							lang_txt = lang_btn.lang_txt;
						}
						
						
						lang_txt.autoSize = TextFieldAutoSize.LEFT;
						lang_txt.height = 10;
						lang_txt.selectable = false;
						
						//lang_txt.textColor = 0xFFFFFF;
						var txt_fmt:TextFormat = new TextFormat();
						txt_fmt.size = 11;
						txt_fmt.bold = false;
						txt_fmt.color = 0xFFFFFF;
						lang_txt.setTextFormat(txt_fmt);
						lang_txt.text = availableLanguages[index2];
						lang_btn.lang = availableLanguages[index2];
						lang_txt.setTextFormat(txt_fmt);
						
						
						var langHitBox:GenericButton = new GenericButton();
						
						langHitBox.width = lang_txt.textWidth;
						langHitBox.height = lang_txt.textHeight;
						langHitBox.alpha = 0;
						langHitBox.buttonMode = true;
						langHitBox.useHandCursor = true;
						
						langHitBox.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
							selectNewLang(e.currentTarget.parent);
							
						});
						
						lang_btn.addChild(langHitBox);
						captions_btn.addChild(lang_btn);
						lang_btn.visible = false;
						langButton_array.push(lang_btn);
						
				
					}
					captions_btn.capSelect_bg.y = langButton_array[langButton_array.length -1].y - 5;
					captions_btn.capSelect_bg.height = captions_btn.capSelect_bg.y + captions_btn.hit_btn.y + 35;
					captions_btn.capSelect_bg.width = langButton_array[langButton_array.length -1].width + 12;
						
						
				}else {
					for (var index3:int = 0 ; index3 < langButton_array.length ; index3++) {
						// REMOVE THE LANGUIAGE BUTTONS
						captions_btn.removeChild(langButton_array.pop());
					}
					langButton_array = new Array();
				}
				if (!captionsTimer.running) { // if the captions are not on when the video starts
					
					selectNewLang(langButton_array[0]);
					stopTracking();
					toggleButtonState();  // turn the captions back off
				}else {
					selectNewLang(langButton_array[0]); // otherwise don't reset the timer button.
				}
				
			
		}
		
		private function selectNewLang(lang_btn:DynamicSprite) {
			// Remove bold from non selected languiages
			for (var index:int = 0 ; index < langButton_array.length ; index++) {
				var temp_btn:DynamicSprite = langButton_array[index];
				var new_fmt1:TextFormat = temp_btn.lang_txt.getTextFormat();
				new_fmt1.underline = false;
				temp_btn.lang_txt.setTextFormat(new_fmt1);
			}
			
			var new_fmt:TextFormat = lang_btn.lang_txt.getTextFormat();
			new_fmt.underline = true;
			lang_btn.lang_txt.setTextFormat(new_fmt);
			
			currentLanguage = lang_btn.lang;
			trace("!currentLanguage: " + currentLanguage);
			startTracking();
			toggleButtonState();
			
		}
		
		public function startTracking() {
			if (!hasCaptions) {
				return;
			}
			
			//testForCaption();
			captionsTimer.start();
			captions_mc.visible = true;
			
			
		}
		
		public function toggleCaptions():Boolean {
			if (captionsTimer.running) {
				stopTracking();
				return false;
			}else {
				startTracking();
				return true;
			}
		}
		
		public function stopTracking() {
			captionsTimer.stop();
			captions_mc.visible = false;
		}
		
		
		private function testForCaption(e:TimerEvent):void {
			if (!hasCaptions) {
				return;
			}
			//trace("!timer Count: " + captionsTimer.currentCount);
			var currentTime:Number = player.currentTime; //player.getPlayer().getCurrentTimeStart();
			
			var cap_txt:TextField = captions_mc.captions_txt;
			var foundCaption:Boolean = false;
			for (var i:Number = 0 ; i < captionsArray[currentLanguage].length ; i++) {
				//_root.arisOutPut.text += "currentTime: " + currentTime;
				//_root.arisOutPut.text += "  |   cap.begin: " + cap.begin + "  |  cap.end: " + cap.end;
				//_root.arisOutPut.text += "\n";
				var cap:Object = captionsArray[currentLanguage][i];
				
				if (cap.begin < currentTime && cap.end > currentTime) {
					
					cap_txt.htmlText = cap.text;
					foundCaption = true;
					break;
				}
				// remove caption if there is nothing to display
				if (!foundCaption) {
					cap_txt.htmlText = "";
				}
			}
			
			
		}
	public function toggleCaptionsButton() {
		var _running:Boolean = toggleCaptions();
		toggleButtonState();
	}
	
	private function toggleButtonState() {
		if(captionsTimer.running){
			captions_btn.cc_gfx.gotoAndStop("Active");
				
		}else {
			captions_btn.cc_gfx.gotoAndStop("Inactive");
					
		}
	}
		
		private function traceOut(str:String) {
			try {
				MovieClip(captions_mc.root).trace_txt.text += "\n" + str;
			}catch (e:Error) {
				trace(str);
			}
		}
		
		
	}

}