package 
{
	import flash.events.SecurityErrorEvent;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.errors.IOError;
	import flash.events.IOErrorEvent;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent
	import flash.events.MouseEvent;
	import flash.display.Loader;
	import DynamicSprite;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize
	import flash.text.TextFormat
	import flash.text.TextFormatAlign;
	import flash.display.StageAlign;
	import flash.filters.BevelFilter;
	import flash.system.Security;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	/**
	 * ...
	 * @author Ari Oshinsky
	 */
	
	
	public class  RelatedVideos extends MovieClip {
		
		private var feedRSS:XML;
		private var videos_array:Array;
		private var rss_array:Array;
		
		private var saved_width:Number;
		private var saved_height:Number;
		private var numberOfRSSItems:int;
		private var cueCards_rss:XMLList;
		private var cueCard:AS3CueCard;
		private var refferenceClip:DynamicSprite;
		private var title_txt:TextField;
		private var pen_mc:Sprite;
		
		public var feedLoaded:Boolean = false;
		public var feedLoading:Boolean = false;
		private var displayOnLoad:Boolean = false;
		private var backToVideo:MovieClip;
		private var bevel:BevelFilter;
		var back_txt:TextField;
		private var attempt:int = 1;
		private var canLoadNextVideo:Boolean = false;
		public var doNotOpen:Boolean = false;
		
	
		public function RelatedVideos(_cueCard:AS3CueCard, _refferenceClip:DynamicSprite) {
			Security.allowInsecureDomain("*");
			Security.allowDomain("*");
			
			//this.stage.align = StageAlign.TOP_LEFT;
			//this.stage.addEventListener(Event.RESIZE, stageResize);
			cueCard = _cueCard;
			refferenceClip = _refferenceClip;
			
			//createBackground();
			//loadRSSFeed();
			title_txt = new TextField();
			
			this.visible = false;
			
			title_txt.autoSize = TextFieldAutoSize.LEFT;
			title_txt.x = 14;
			title_txt.y = 3;
			title_txt.text = "Related CueCards";
			
			var fmt:TextFormat = new TextFormat;
			fmt.color = 0xFFFFFF;
			fmt.bold = true;
			fmt.size = 13;
			fmt.font = "Arial";
			title_txt.setTextFormat(fmt);
			
			pen_mc = new Sprite();
			
			this.addChild(pen_mc);
			this.addChild(title_txt);
			
			
		// BACK TO VIDEO BUTTON
			bevel = new BevelFilter( -.6, 45, 0xFFFFFF, .52, 0x000000, .74, 2, 2, 1, 3);	
			backToVideo = new ReturnButton();
			this.addChild(backToVideo);
			backToVideo.y = 3;
			backToVideo.hit_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				closeRelatedVideos();
			});
			/*
			backToVideo.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				Colors.changeTint(backToVideo.hit_btn, 0xc8234a, 1)
				Colors.changeTint(backToVideo.gfx, 0xFFFFFF, 1)
				backToVideo.filters = [bevel];
			});
			backToVideo.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				Colors.changeTint(backToVideo.hit_btn, 0xc00343, 1)
				Colors.changeTint(backToVideo.gfx, 0xFFFFFF, 1)
				backToVideo.filters = [];
			});
			*/
			Colors.changeTint(backToVideo.bg, 0xc00343, 1)
			Colors.changeTint(backToVideo.gfx, 0xFFFFFF, 1)
			
			backToVideo.hit_btn.buttonMode = true;
			backToVideo.useHandCursor = true;
			
			back_txt = new TextField();
			fmt.color = 0xA8AEB8;
			fmt.size = 12;
			back_txt.autoSize = TextFieldAutoSize.LEFT;
			
			back_txt.text = "Back";
			back_txt.y = title_txt.y+1;
			back_txt.setTextFormat(fmt);
			this.addChild(back_txt);
			
			
		}
		
		private function closeRelatedVideos() {
			
			//if (cueCard.imageMode) {
				//return;
			//}
			doNotOpen = true;
			var openTimer:Timer = new Timer(300,1);
			openTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent) {
				doNotOpen = false;
			});
			openTimer.start();
			
			
			if(!cueCard.imageMode){
				
				if (cueCard.videoPlayer.mezzThumbnail) {
					if (cueCard.videoPlayer.mezzThumbnail.visible) {
						cueCard.controls_mc.visible = false;
						  if(cueCard.fmanager.getFocus() == backToVideo.hit_btn){
								cueCard.fmanager.setFocus(cueCard.videoPlayer.mezzThumbnail.playButton);
						  }
					}else{
						cueCard.controls_mc.visible = true;
						
						 if(cueCard.fmanager.getFocus() == backToVideo.hit_btn){
							 cueCard.fmanager.setFocus(cueCard.controls_mc.related_btn);
						 }
					}
					
			
					
					
					
					
					
			   }else{     // NO MEZZ THUMB
			  	cueCard.controls_mc.visible = true;
			
				if(cueCard.fmanager.getFocus() == backToVideo.hit_btn){
							 cueCard.fmanager.setFocus(cueCard.controls_mc.related_btn);
				}
				
				
			
					
					//cueCard.videoPlayer.snipe_mc && cueCard.videoPlayer.videoMode == "15_Second" && cueCard.overrideGuestMode != true  && cueCard.imageMode != true
					
					if (cueCard.videoPlayer.snipe_mc && cueCard.videoPlayer.videoMode == "15_Second" && cueCard.overrideGuestMode != true  && cueCard.imageMode != true) {
						cueCard.always_mc.snipeHitBox_mc.visible = true;
						cueCard.videoPlayer.snipe_mc.visible = true;
					}
			
			
			   }
			     
						
				
			}else{  // NO MEZZ THUMB
				cueCard.imageViewer.controls_mc.visible = true;
			   if(cueCard.fmanager.getFocus() == backToVideo.hit_btn){
				  
					cueCard.fmanager.setFocus(cueCard.imageViewer.controls_mc.related_btn);
			   }
			   
			   		
					
			   
			   
			}
			
			this.visible = false;
		}
			
		
		
		private function crossdomainError(e:SecurityErrorEvent) {
			//cueCard.confirmation.confirm("RELATED VIDEOS Cross Domain ERROR", "OK", new Function(), true);
			feedLoading = false;
			trace("!RELATED VIDEOS Cross Domain ERROR");
		}
	
		public function loadNewRSSFeed(_url:String = null, _displayOnLoad:Boolean = false) {
			
			if (cueCard.Product == "NBC Learn" || cueCard.Product == "LEARN2" || cueCard.Product == "learn" || cueCard.Product == "LEARN") {
			return; // don't load related videos in nbclearn.com	
			}
		
		displayOnLoad = _displayOnLoad
		feedLoading = true;
		
			
			var request:URLRequest = new URLRequest();
			
			var rssLoader:URLLoader = new URLLoader();
			
			rssLoader.addEventListener(Event.COMPLETE, rssLoaded);
			rssLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, crossdomainError);
			rssLoader.addEventListener(IOErrorEvent.IO_ERROR, function(e:IOErrorEvent) {
				this.visible = false;
				//cueCard.confirmation.confirm("RELATED VIDEOS IO ERROR", "OK", new Function(), true);
				trace("!RELATED VIDEOS IO ERROR");
				feedLoading = false;
			});
			
			
			// PROD http://searchservices.nbcuni.com/
			// DEV http://snasdev9.nbcuni.com
			var product_search:String;
			if (cueCard.Product == "K12" || cueCard.Product == "BBK12") {
				product_search = "nbcarchives";
			}
			if (cueCard.Product == "HE" || cueCard.Product == "BBHE") {
				product_search = "highered";
			}
		                            //DEV PASSTHROUGH	http://dev-archives.nbclearn.com/portal/security/search? SOLR DIRECT//http://searchservices.nbcuni.com/solrsearch/api/enhancedSearch?
			request.url = encodeURI("http://archives.nbclearn.com/portal/security/search?page=1&collectionList="+product_search+"&site=icue&perPage=9&searchType=BOOLEAN&searchString="+cueCard.metaData.keyWordsArray[0]+"+AND+"+cueCard.metaData.keyWordsArray[1]);
		
			
				
			rssLoader.load(request);
			//trace("RSS: " + request.url);	
		}
		
		public function SetFocusOnFirstThumbnail() {
			
			if (cueCard.fmanager.getFocus() == cueCard.videoControls_mc.related_btn || cueCard.fmanager.getFocus() == cueCard.imageViewer.controls_mc.related_btn) {
				canLoadNextVideo = false;
					try {
						var focusDelayTimer:Timer = new Timer(300, 1);
						focusDelayTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent) {
							
							canLoadNextVideo = true;
							
						});
						focusDelayTimer.start();
						cueCard.fmanager.setFocus(videos_array[0].hit_btn);
					
					

				}catch (e:Error) {
					
				}
			
			}
		}
		
		private function rssLoaded(e:Event) {
		
			
			
			var rss:XML = new XML(e.target.data)
			rss.ignoreWhitespace = true; 
			
			
				if (rss_array) {
					for (var rss_index:int = 0 ; rss_index < rss_array.length ; rss_index++) {
						delete rss_array[rss_index];
					}
					for (var rss_index2:int = 0 ; rss_index2 < rss_array.length ; rss_index2++) {
						rss_array.pop();
					}
			}	
			rss_array = new Array();
			
			//trace("LOoaded rss");
			var results_list:XMLList = rss.searchResult.searchResultDocument;
			for(var index:int = 0 ; index < results_list.length() ; index++){
				
			
				var key_list:XMLList = results_list[index].searchResultFieldList.entry;
				var card_id:String;
				var thumb_url:String;
				var title_str:String;
				var asset_type:String;
				for(var i:int = 0 ; i < key_list.length() ; i++){
					//trace("Key: " + key_list[i].value.fieldValue);
					if(key_list[i].key == "sourcecontentid"){
						card_id = key_list[i].value.fieldValue;
					}
					if(key_list[i].key == "usercontenttype"){
						asset_type = key_list[i].value.fieldValue;
					}
					if(key_list[i].key == "title"){
						title_str = key_list[i].value.fieldValue;
					}
					if(key_list[i].key == "thumbnail"){
						thumb_url = key_list[i].value.fieldValue;
					}
				}
				
				//trace("");
				//trace("------- " + title_str + "-----");
				//trace("card_id = " + card_id);
				//trace("thumb_url = " + thumb_url);
				//trace("asset_type = " + asset_type);
				rss_array.push( { id:card_id, thumb:thumb_url , title:title_str , type:asset_type } );
			}
			buildVideosList();
		}
				
				
			
			
		
	
		private function buildVideosList() {
				
			
			
				if (videos_array) { // REMOVE ANY EXSISTING VIDEOS
						//trace("Video array exsists");
						if(videos_array.length > 0){
							for (var i:int = 0 ; i < videos_array.length ; i++) {
								this.removeChild(videos_array[i]);
								delete videos_array[i];
								
							}
							
							
						}
					}
				
			
				videos_array = new Array();

				numberOfRSSItems = rss_array.length;
				if (numberOfRSSItems > 1) {
					
					//this.visible = true;
					cueCard.addItemToTabOrder(backToVideo.hit_btn, "Go back to the video");
					if(!cueCard.imageMode){
						cueCard.videoControls_mc.related_btn.visible = true;
					
					}else {
						cueCard.imageViewer.controls_mc.related_btn.visible = true;
				
					}
					
					buildVideo(0);
				}else { // There are no videos to load,
					
					this.visible = false;
					if(!cueCard.imageMode){ // De Activate the icon for videos
						cueCard.videoControls_mc.related_btn.visible = false
					}else { // de activate the icon for images
						cueCard.imageViewer.controls_mc.related_btn.visible = false;
						
					}
					feedLoading = false; // set that the feed is no longer loading
					
					//cueCard.videoPlayer.loadMezzThumbnail(cueCard.metaData.cardID, cueCard.metaData.MezzanineVdoCMSImage);
				}
				cueCard.videoPlayer.arrangeControls();
				//cueCard.imageViewer.arrangeControls();
				
					
				
		}
		

		private function buildVideo(i:int) {
			
			

				// Create the assets
				var video:DynamicSprite = new DynamicSprite();
				video.index = i;
				var thumb:Sprite = new Sprite();
				
				//var pubDate:String = cueCards_rss[i].attribute("publish");
				var clipTitle_txt:TextField = new TextField();
				var hit_btn:Sprite = new Sprite();
				hit_btn.graphics.beginFill(0xFF0000,0);
				hit_btn.graphics.drawRect(0, 0, 130, 100);
				hit_btn.graphics.endFill();
				
				
				//var publish_txt:TextField = new TextField();
				var fmt:TextFormat = new TextFormat("Arial");
				fmt.size = 12;
				fmt.color = 0xFFFFFF;
				fmt.bold = true;
				fmt.align = TextFormatAlign.CENTER;
				
				video.format = fmt;
				
				//pubDate = pubDate.slice(0, pubDate.length - 2);
				
				video.imageURL = rss_array[i].thumb;
				video.title = rss_array[i].title;
				//video.description = cueCards_rss[i].description;
				
				// Set up the Title and Publish Date
				clipTitle_txt.width = 130; 
				//clipTitle_txt.height = 100;
				clipTitle_txt.autoSize = TextFieldAutoSize.LEFT;
				clipTitle_txt.wordWrap = true;
				//publish_txt.autoSize = TextFieldAutoSize.LEFT;
				clipTitle_txt.selectable = false;
				
				
				clipTitle_txt.htmlText = "<b>"+video.title+"<B>";
				//publish_txt.htmlText = "Published: " + pubDate;
				
				clipTitle_txt.setTextFormat(video.format);
				//publish_txt.setTextFormat(video.format);
				
				video.thumb = video.addChild(thumb);
				clipTitle_txt.visible = false;
				
				
				
				video.clipTitle_txt = video.addChild(clipTitle_txt);
				
				//video.publish_txt = video.addChild(publish_txt);
				video.clipID = rss_array[i].id;
				
				var loader:Loader = new Loader();
				var request:URLRequest = new URLRequest(video.imageURL);
				if (cueCard.Product == "BB" || cueCard.Product == "BBK12" || cueCard.Product == "BBHE"){
				
				request.url = "http://archivesbb.nbclearn.com" +video.imageURL;
				}else {
					request.url = video.imageURL;
				}
				thumb.addChild(loader);
				video.hit_btn = video.addChild(hit_btn);
				video.visible = false;
				
				
				
				
					clipTitle_txt.x = 0;
					
					
				// Add Interactions
				hit_btn.mouseEnabled = true;
				hit_btn.useHandCursor = true;
				//thumb.addEventListener(MouseEvent.CLICK, function(e:Event) {
					//playVideo(e.currentTarget.parent.clipID);
					
				//});
				hit_btn.addEventListener(KeyboardEvent.KEY_UP, function(e:KeyboardEvent) {
					if (e.keyCode == 13 || e.keyCode == 32) {  // ENTER or SPACE
						
						//if(extensionOpened == true){
					
							
							playVideo(e.currentTarget.parent.clipID);
							
						//}
					}
				});
				
				
				
				
				
				hit_btn.addEventListener(MouseEvent.ROLL_OVER, applyRollOver);
				hit_btn.addEventListener(MouseEvent.ROLL_OUT, applyRollOut);
				hit_btn.buttonMode = true;
				hit_btn.useHandCursor = true;
				hit_btn.addEventListener(MouseEvent.CLICK, function(e:Event) {
					playVideo(e.currentTarget.parent.clipID);
					
				});
				
				
				
				
				
				hit_btn.addEventListener(FocusEvent.FOCUS_IN, function(e:Event) {
					applyRollOver(e);
					//cueCard.disableKeyboardControl = true;
				});
				hit_btn.addEventListener(FocusEvent.FOCUS_OUT, function(e:Event) {
					applyRollOut(e);
					//cueCard.disableKeyboardControl = false;
				});
				
				
				// Place the thumbnails in the draw
				
				videos_array.push(video);
				
				this.addChild(video);
				
				
				try{
					
					
					hit_btn.tabEnabled = true;
					hit_btn.focusRect = true;
					
					//publish_txt.tabEnabled = false;
					clipTitle_txt.tabEnabled = false;
					
					
					cueCard.addItemToTabOrder(hit_btn, "Play " + video.title);
				}catch (e:Error) {
					
				}
				
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, thumbnailFailedToLoad);
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e:Event) {
				// Reduce the size of the thumb when it loads.
					var vid:DynamicSprite = e.target.loader.parent.parent;
					e.target.loader.width = 130;
					e.target.loader.height = 100;
										
					//trace("THUMB Loaded");
					thumbnailLoaded(vid);
				});
				
				//trace("video.imageURL: " + video.imageURL);
				attempt = 1;
				loader.load(request);
				
				
				//trace("Image URL: " + video.imageURL);
				
			
			
		
			
		//	cueCard.addItemToTabOrder(close_btn, "Close Videos");
		
		
		}
		private function thumbnailFailedToLoad(e:IOErrorEvent) {
			var test:IOErrorEvent;
			//trace("e.currentTarget.parent: " + e.currentTarget.loader);
			
			if(attempt < 2){
				attempt++
				e.target.loader.load(new URLRequest("http://archives.nbclearn.com/files/nbcarchives/site/images/thumbs/placeholder.jpg"));
			}else {
				var vid:DynamicSprite = e.target.loader.parent.parent
				if (vid.index + 1 < numberOfRSSItems) {
						videos_array.pop();
						buildVideo(vid.index + 1);
						//stageResize();
					}else {
						arrangeContent();
						feedLoaded = true;
						feedLoading = false;
						trace("ALL IMAGES LOADED, displayOnLoad: " + displayOnLoad);
						if (displayOnLoad) {
							cueCard.videoPlayer.related_vids.visible = true;
						if(!cueCard.imageMode){
							cueCard.controls_mc.visible = false;
						}else{
							cueCard.imageViewer.controls_mc.visible = false;
						}
							SetFocusOnFirstThumbnail();
						}
					}
			}
					
		}
		
		private function thumbnailLoaded(vid:DynamicSprite) {
			vid.hit_btn.width = vid.thumb.width;
					vid.hit_btn.height = vid.thumb.height;
					
					vid.clipTitle_txt.y = 100 / 2 - vid.clipTitle_txt.height / 2;
					//trace("Index: " + vid.index);
					//arrangeContent(refferenceClip.width, refferenceClip.height);
					
					
					
					if(vid.index + 1 < numberOfRSSItems){
						buildVideo(vid.index + 1);
						//stageResize();
					}else {
						arrangeContent();
						feedLoaded = true;
						feedLoading = false;
						trace("ALL IMAGES LOADED, displayOnLoad: " + displayOnLoad);
						canLoadNextVideo = true;
						if (displayOnLoad) {
							cueCard.videoPlayer.related_vids.visible = true;
							SetFocusOnFirstThumbnail();
						}
					}
		}
		
		public function arrangeContent() {
			resize(refferenceClip.width, refferenceClip.height);
		}
		public function resetContent(bg_width:Number, bg_height:Number) {
			resize(bg_width, bg_height);
		}
		
		private function resize(bg_width:Number = 0, bg_height:Number = 0) {
	
			if (!videos_array) {
				return;
			}
			
			var hMargin:Number = 0;
			var vMargin:Number = 4;
			
			backToVideo.x = bg_width - backToVideo.width - 5;
			back_txt.x = backToVideo.x - back_txt.width - 5;
		
		
			pen_mc.graphics.clear();
			pen_mc.graphics.beginFill(0x000000, .9);
			pen_mc.graphics.lineTo(bg_width, 0);
			pen_mc.graphics.lineTo(bg_width, bg_height);
			pen_mc.graphics.lineTo(0, bg_height);
			pen_mc.graphics.lineTo(0, 0);
			pen_mc.graphics.endFill();
			
			

			
			var lastX:Number = 0;
			var lastY:Number = 15;
			var thumbWidthIndex:int = 1;
			
			var numVideos:int = videos_array.length;
		
			
		
		// H MARGIN
			while ((lastX + 130) < bg_width) {
				if(thumbWidthIndex < numVideos){
					lastX += 130;
					thumbWidthIndex++
					
					
				}else {
					break;
				}
			}
			
			hMargin = ((bg_width - lastX) / thumbWidthIndex);
			
		
			
			//trace("hMargin: " + hMargin);
			//trace("vMargin: " + vMargin);
			//trace("----");
			var numVMargins:int = 1;
				
			
			for(var index:Number = 0 ; index < numVideos ; index++){
				
				var video:DynamicSprite = videos_array[index];
			
				
				
				var prevVideo:DynamicSprite = videos_array[index - 1];
				
				if (prevVideo) {
					
					
					video.x = prevVideo.x + 130 + hMargin;
					
					if (video.x + video.width + hMargin -8 >= bg_width) { // if we have gone over the avilable space
						video.x = hMargin;
						video.y = prevVideo.y + 100 ;
						
						if (video.y + video.height < bg_height) {
							numVMargins++;
							lastY = video.y;
						}
					}else {
						video.y = prevVideo.y 
					}
		
					
					if (video.y + video.height < bg_height) {
						lastY = video.y + video.height;
						
					}
						
					video.vMargIndex = numVMargins;
					
				
	
				}else {
					video.x = hMargin;
					video.y = 15;
					video.visible = true;
					video.vMargIndex = numVMargins;	
				}
	

			}
			
			
				// V MARGIN
			var thumbHeightIndex = 1;
			var newX:Number = 130;
			
			
			 // NOW Calculate weather to show it or not.
			
			
			numVMargins++
			vMargin = ((bg_height-10) - lastY) / (numVMargins); // devide the remaining space 
			
			for (var vIndex:int = 0 ; vIndex < videos_array.length ; vIndex++) {
				
				var vid:DynamicSprite = videos_array[vIndex];
				
					vid.y += (vid.vMargIndex*vMargin);
				if (vid.y + vid.height + vMargin -8 < bg_height) {
					vid.visible = true;
				}else {
					vid.visible = false;
				}

				
			}
			
		//cueCard.trace_txt.htmlText = "<b>numVMargins:</b> " + numVMargins +"\n<b>vMargin:</b> " + vMargin+ "\n<b>lastY:</b> " + lastY +"\n<b>bg_height:</b> " + bg_height;
			
			
		}
		

		
		private function playVideo(_videoID:String) {
			if (!canLoadNextVideo) {
				return;
			}
			cueCard.omniture.trackRelatedClick(); // calls the track event on the current CueCard so that we can compaire how many times the video was watched compleatly to how many related videos it led to.
			cueCard.videoPlayer.autoPlay = true;
			cueCard.trackRelatedVideo = true;
			if(cueCard.Product =="BBHE" || cueCard.Product =="BB"){
				cueCard.loadXML("http://archivesbb.nbclearn.com/portal/vcm/cuecardxml?id=" + _videoID,true);
			}else {
				cueCard.loadXML("http://archives.nbclearn.com/portal/vcm/cuecardxml?id=" + _videoID, true);
			}
			this.visible = false;
			//closeExtension();
		}
		
		private function applyRollOver(e:Object) {
			e.currentTarget.parent.thumb.alpha = .4;
			e.currentTarget.parent.clipTitle_txt.visible = true;
		}
		private function applyRollOut(e:Object) {
			e.currentTarget.parent.thumb.alpha = 1;
			e.currentTarget.parent.clipTitle_txt.visible = false;
		}
		
		// Utility for finding and replacing text
		private function findReplace( lookUp:String, replace:String, orig:String ){
			if (replace == "_blank") {
				replace = "";
			}
			if( orig.indexOf( lookUp ) > -1 ){
			
				return orig.split( lookUp ).join( replace );
			}
			return orig;
		}
		
		
	}
	
}