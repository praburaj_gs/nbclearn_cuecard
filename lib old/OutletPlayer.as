﻿package  {
	
    import flash.display.*;
    import flash.events.*;
    import flash.net.*;
    import flash.system.*;
	import flash.external.ExternalInterface;

	/**
	 *  <p>This class will be used to implements swf in swf in casses where the loader needs to be embedded in
	 *  a flash professional project.  The class will be attached as a document class for the FLA and allow loading 
	 *  extensions into flash professional.  The way it works is that it's loading the outlet loader into this class
	 *  and using methods to bring in the extensions needed.  There are three variables to set:</p>
	 * 
	 *  <ul>
	 * 		<li>Config url - outlet config url file.</li>
	 * 		<li>Outlet loader url - a swf class that uses the outlet manager.</li>
	 * 		<li>Starter - the extension that will be used as the start extension. </li>
	 *  </ul>
	 * 
	 */		
	public class OutletPlayer extends Sprite
	{
	    //--------------------------------------------------------------------------
	    //
	    //  Settings in case flashvars weren't set
	    //
	    //--------------------------------------------------------------------------
	    		
		private static const CONFIG_URL:String ="outletComponents/config_relitive.xml";
		private static const OUTLET_LOADER_URL:String = "outletComponents/OutletManagerSWFinSWFLoader.swf";
		private static const STARTER:String = "appStarter";
		private static const LOCALHOSTED:String = "false";
		private static const CLIPPLAYLIST:String = "1134013";//"859723,852126,859722";

	    //--------------------------------------------------------------------------
	    //
	    //  Variables
	    //
	    //--------------------------------------------------------------------------
	    		 
		private var component:Sprite = new Sprite();
		//public var controls_MC:MovieClip = new MovieClip();
		public var _myPlayer:*;
		private var _clipID:String;
		private var _seekTo:Number;
		private var _isPlayerLoaded:Boolean;
		private var _w:Number=-1;
		private var _h:Number=-1;
		private var _newClipAssigned:Boolean;
		private var _skinPath:String;
		public var _embeddedPlayer:Object;
		
		/**
		 * Loader instance 
		 */		
		private var loader:Loader;
		
		/**
		 * outlet manager instance 
		 */		
		protected var outletmanager:Object;
		
		/**
		 * Holds the url to the config url 
		 */		
		private var _configUrl:String;
		
		/**
		 * Holds global variables
		 */			
		private var _global:Object;
		
		/**
		 * Holds the url for the outlet loader swf 
		 */			
		private var _outletLoaderURL:String;
		
		/**
		 * Holds the starter extension id
		 */			
		private var _starter:String;
		
		/**
		 * Holds the localhosted
		 */			
		private var _localhosted:String;
		
		/**
		 * Holds the clipplaylist ids
		 */			
		private var _clipPlaylist:String;				
		
		/**
		 * Player instace 
		 */		
		private var player:Object;
		
	    //--------------------------------------------------------------------------
	    //
	    // Setters and getters
	    //
	    //--------------------------------------------------------------------------
	    		
		public function get configUrl():String
		{
			return _configUrl;
		}
		
		public function set configUrl(val:String):void
		{
			_configUrl = val;
		}

		public function get global():Object
		{
			return _global;
		}
		public function set global(val:Object):void
		{
			_global = val;
		}

		public function get outletLoaderURL():String
		{
			return _outletLoaderURL;
		}
		public function set outletLoaderURL(val:String):void
		{
			_outletLoaderURL = val;
		}			
		
		public function get starter():String
		{
			return _starter;
		}
		
		public function set starter(val:String):void
		{
			_starter = val;
		}
		
		public function get localhosted():String
		{
			return _localhosted;
		}
		
		public function set localhosted(val:String):void
		{
			_localhosted = val;
		}
		
		public function get clipPlaylist():String
		{
			return _clipPlaylist;
		}
		
		public function set clipPlaylist(val:String):void
		{
			_clipPlaylist = val;
		}				
		public function playClip(cid:*,seekTo:Number=0):void{
			_newClipAssigned=true;
			trace("fn: playClip");
			_clipID=String(cid);
			_seekTo=seekTo;
			//ExternalInterface.call("alert",_clipID);
			//ExternalInterface.call("alert",_isPlayerLoaded);
			if(_isPlayerLoaded){
				_myPlayer.playVideo (_clipID);
				//_myPlayer.seekTo(_seekTo);
			}else{
				//startLoad();
			}
		}
		public function stop(){
			_myPlayer.pause();
		}
		public function pause(){
			_myPlayer.pause();
		}
		public function play(){
			_myPlayer.play();
		}
		
	    //--------------------------------------------------------------------------
	    //
	    //  Constructor
	    //
	    //--------------------------------------------------------------------------
	    		
		public function OutletPlayer()
		{
			Security.allowDomain("*");
			// Set the stage properties in order to ensure that the video size // is correct when entering fullscreen mode. 
			//try{ stage.align = StageAlign.TOP_LEFT; stage.scaleMode = StageScaleMode.NO_SCALE; } catch(e:Error) { trace("Stage error: " + e.message); }
			
			//component.y=81;
			//component.x=171;
			
			_isPlayerLoaded=false;

			this.addChild(component);
				configUrl = CONFIG_URL;
				outletLoaderURL = OUTLET_LOADER_URL;
				starter = STARTER;
				localhosted = LOCALHOSTED;
				//clipPlaylist = CLIPPLAYLIST;
				
				global = new Object();
				global.configUrl = CONFIG_URL;
				global.outletLoaderURL = OUTLET_LOADER_URL;
				global.starter = STARTER;
				global.localhosted = LOCALHOSTED;
				//global.clipPlaylist=CLIPPLAYLIST;
			
			trace("configURL: "+global.configUrl);
			trace("outletLoaderURL: "+global.outletLoaderURL);
			trace("starter: "+global.starter);	
			trace("localhosted: "+global.localhosted);
		}
		
		/**
		 * Method to load the swf that uses the outlet manager API 
		 * 
		 */		
		public function startLoad():void
		{
			trace("fn: startLoad");
			global.clipPlaylist = _clipID;
			loader = new Loader();
			var request:URLRequest = new URLRequest(global.outletLoaderURL);
			
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, completeHandler);
			loader.load(request);
			
			this.component.addChild(loader);
		}
		public function setVolume(vol:Number){
			_myPlayer.setVolume(vol);
		}
		public function resize(w:Number,h:Number){
			_w=w;
			_h=h;
			if(_myPlayer != null){
				_myPlayer.resize(_w,_h);
			}
		}
		public function changeControls(skinPath:String){
			_skinPath=skinPath;
			if(_myPlayer != null){			
				_myPlayer.changeControls(skinPath);
			}
		}
	    //--------------------------------------------------------------------------
	    //
	    //  Event handlers
	    //
	    //--------------------------------------------------------------------------		
		
		/**
		 * Handles complete handler
		 * 
		 * @param event
		 * 
		 */		
		private function completeHandler(event:Event):void 
		{
			outletmanager = loader.content;
			outletmanager.addEventListener("outletInitComplete", onOutletInitComplete);
			outletmanager.addEventListener("outletManagerErrorEvent", onOutletError);
			outletmanager.init(configUrl, global);
		}
		/**
		 * Handle io error handler
		 *  
		 * @param event
		 * 
		 */		
		private function ioErrorHandler(event:IOErrorEvent):void 
		{
			throw new Error("Couldn't load Outlet manager swf, ioErrorHandler: " + event.toString());
		}
		
		/**
		 * Get an instance of the extension and call the start method.
		 *  
		 * @param event
		 * 
		 */		
		public function onOutletInitComplete(event:Event):void
		{
			outletmanager.start(starter, component);
			// Create a reference to the EmbeddedPlayer extension
			var embeddedPlayer:Object = outletmanager.getOutletExtension("appStarter");
			_embeddedPlayer = embeddedPlayer;
			// Add event listeners to the EmbeddedPlayer extension
			var adData:Object = { numberOfAdCalls:0};
			embeddedPlayer.updateAdData (adData);
			
			addPlayerEventListeners (embeddedPlayer);
			_myPlayer = embeddedPlayer;
			dispatchEvent(new Event("OUTLET_INIT_COMPLETE"));
			
			
		}
		
		/**
		 * Event handler for outlet error
		 *  
		 * @param event
		 * 
		 */		
		private function onOutletError(event:Event):void
		{
			throw new Error("Coudn't init outlet manager");
		}
		public function getPlayer():* {
			try{
				var ep:* = outletmanager.getOutletExtensionsByType("EmbeddedPlayer");
				return ep.getPlayer();
			}catch (err:Error) {
				trace("err: " + err);
				
				trace("CORE LOADER ERROR: Embed Player extension not available");
				return null;
			}
	
		}
		

		/**
         * Add event listeners to the reference to the EmbeddedPlayer extension.
         * 
         * @param embeddedPlayer is the reference to the EmbeddedPlayer extension
         */
		private function addPlayerEventListeners (embeddedPlayer:Object):void
		{

			//ExternalInterface.call("alert",_myPlayer);
			// Add listeners for Embedded Player Events
			embeddedPlayer.addEventListener ("VideoControlsEvents.USER_PLAY", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("VideoControlsEvents.USER_PAUSE", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("VideoControlsEvents.SKIN_READY", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("Player.seekStart", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("Player.start", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("Player.end", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("AD_BREAK_EVENT_ON_LOADING", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("AD_BREAK_EVENT_ON_BEGIN", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("AD_BREAK_EVENT_ON_END", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("AD_BREAK_EVENT_ON_COMPLETE", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("AD_EVENT_ON_BEGIN", onPlayerEventReceived);
			embeddedPlayer.addEventListener ("AD_EVENT_ON_END", onPlayerEventReceived);
			//embeddedPlayer.addEventListener ("CONTENT_METADATA.clip_info_update", logMetadataProps);
			embeddedPlayer.addEventListener ("StateUpdateEvent.UPDATE", onPlayerEventReceived);
		}
		/**
         * Handle all events from the player in a single function, or assign individual functions as listeners
         * 
         * @param event
         * 
         */
		
		private function onPlayerEventReceived (event:*):void
		{
			// use the switch statement to distinguish between eache event's type proprety

			switch (event.type)
			{
					// Video play and controls events
				case "VideoControlsEvents.USER_PLAY" :
					{
						dispatchEvent(new Event("PLAY"));
						break;
					};

				case "VideoControlsEvents.USER_PAUSE" :
					{
						dispatchEvent(new Event("PAUSE"));
						break;
					};

				case "VideoControlsEvents.SKIN_READY" :
					{
						break;
					};

				case "Player.seekStart" :
					{
						break;
					};

				case "Player.start" :
					{
						if(_w!=-1 && _h!=-1){
							_myPlayer.resize(_w,_h);
							_w=-1;
							_h=-1
						}
						if(_newClipAssigned){
							_myPlayer.seekTo(_seekTo);
							dispatchEvent(new Event("PLAYER_START"));
							_newClipAssigned=false;
						}
						break;
					};

				case "Player.end" :
					{
						break;
					};

				case "AD_BREAK_EVENT_ON_LOADING" :
					{
						break;
					};

				case "AD_BREAK_EVENT_ON_BEGIN" :
					{
						break;
					};

				case "AD_BREAK_EVENT_ON_END" :
					{
						break;
					};

				case "AD_BREAK_EVENT_ON_COMPLETE" :
					{
						break;
					};

				case "AD_EVENT_ON_BEGIN" :
					{
						break;
					};

				case "AD_EVENT_ON_END" :
					{
						break;
					};

					// Content metadata events
				case "CONTENT_METADATA.clip_info_update" :
					{
						break;
					};

					// Video Player state events
				case "StateUpdateEvent.UPDATE" :
					{

						// Use a switch statement to distinguish between different state property values
						// attached to the properties property of the state event
						switch (event.state)
						{
								// Player has been initialized and is ready
							case "EmbeddedPlayer.STATE_READY" :
								{
									
									if(_skinPath!=null){
										_myPlayer.changeControls(_skinPath);
										_skinPath=null;
									}
									if(_isPlayerLoaded){
										//_myPlayer.seekTo(_seekTo);
									}
									_isPlayerLoaded=true;
									break;
								};

								// User explicitly sizes the player
							case "EmbeddedPlayer.STATE_RESIZE" :
								{
									break;
								};

								// Related to overlay ads
							case "EmbeddedPlayer.STATE_EXPANDED" :
								{
									break;
								};

							case "EmbeddedPlayer.STATE_HIDDEN" :
								{
									break;
								};

							case "EmbeddedPlayer.STATE_COLLAPSE" :
								{
									break;
								};

								// Related to player size
							case "EmbeddedPlayer.STATE_LARGE" :
								{
									break;
								};

							case "EmbeddedPlayer.STATE_DEFAULT" :
								{
									break;
								};

							case "EmbeddedPlayer.STATE_FULLSCREEN" :
								{
									break;
								};

							default :
								{
									break;
							}
						};
						break;
					};
				default :
					{
						break;
				}
			}
		}
	}
}