﻿package {

import flash.events.TimerEvent;
import flash.filters.DropShadowFilter;
import flash.text.TextField;
import flash.text.TextFormat;
import fl.transitions.Tween;
import flash.utils.Timer;

import flash.display.Sprite;
import fl.transitions.Tween;
import flash.events.MouseEvent;
import flash.display.MovieClip;
import flash.text.TextFieldAutoSize;
import fl.transitions.TweenEvent;
import fl.transitions.easing.*;

//import mx.transitions.easing;



	class MouseTip{
		
		private var location:MovieClip
		public var followTipListener:Object;
		private var fadingOut:Boolean = false;
		private var pen_mc:Sprite;
		public var tip_mc:Sprite;
		private var root_mc:MovieClip;
	// Text Variables
		private var tip_txt:TextField;
		
		private var _align:String = "right";
		
		private var targetX:Number;
		private var targetY:Number;
		private var skin;
		private var shadow:DropShadowFilter;
		
		private var left:Number;
		private var right:Number;
		private var top:Number;
		private var bottom:Number;
	// Intervals
		private var pauseID:Number;
		private var mouseFadeHandler:Tween
		
		private var _delay:Number = 1000;
		private var text_fmt:TextFormat;
		private var delayTimer:Timer;
		
		public function MouseTip(_location:MovieClip){
			
			this.location = _location;

		// create Tip
			tip_mc = new Sprite();
			
			location.addChild(tip_mc);
			
		
		// Create Pen
			pen_mc = new Sprite()
			tip_mc.addChild(pen_mc);
			
		// Create TextBox
			tip_txt = new TextField();
			tip_mc.addChild(tip_txt);
			tip_txt.width = 5;
			tip_txt.height = 5;
			
			text_fmt = new TextFormat();
			
			
			text_fmt.color = "0x000000";
			
			tip_mc.alpha = 0;
			delayTimer = new Timer(_delay, 1);
			delayTimer.addEventListener(TimerEvent.TIMER_COMPLETE, displayTip);
			
		}
		
		private function setTipText(text_str:String){
			
			tip_txt.autoSize = TextFieldAutoSize.LEFT;
			tip_txt.text = text_str;
			tip_txt.setTextFormat(text_fmt);
			
			
			var width:Number = tip_txt.width+5;
			var height:Number = tip_txt.height+5;
			var offsetY:Number = -(15 + (height/2));
			var offsetX:Number = 20
			if(_align == "center"){
				tip_txt.x = -(tip_txt.width/2)  + offsetX;
				left = -(width/2) + offsetX;
				right = width/2 + offsetX;
			}
			if(_align == "left"){
				tip_txt.x = -(tip_txt.width)  + offsetX;
				left = -(width) + offsetX;
				right = offsetX;
			}
			if(_align == "right"){
				tip_txt.x = -(offsetX) + 3;
				left = -offsetX;
				right = width - offsetX;
			}
			tip_txt.y = -(tip_txt.height/2) + offsetY;
		
			
			top = -(height/2) + offsetY;
			bottom = height/2 + offsetY;
			
			
		}
		
		private function drawRect(){
				
			
			var curve:Number = 0;
			
			pen_mc.graphics.clear();
			pen_mc.graphics.lineStyle(0,0x000000,80);
			pen_mc.graphics.beginFill(0xFFFFFF,80);
			
			pen_mc.graphics.moveTo(left+curve,top);
			
			pen_mc.graphics.lineTo(right-curve,top);
			pen_mc.graphics.curveTo(right,top,right,top+curve);
			pen_mc.graphics.lineTo(right,bottom-curve);
			pen_mc.graphics.curveTo(right,bottom,right-curve,bottom);
			// line to target
			if(_align != "left"){
				pen_mc.graphics.lineTo(left + 15,bottom)
				pen_mc.graphics.lineTo(0,0);
				pen_mc.graphics.lineTo(left+10,bottom)
			}else{
				pen_mc.graphics.lineTo(left-(left/1.5),bottom)
				pen_mc.graphics.lineTo(0,0);
				pen_mc.graphics.lineTo(left-(left/2),bottom)
				
			}
			// Resume Regular Line
			
			pen_mc.graphics.lineTo(left+curve,bottom);
			pen_mc.graphics.curveTo(left,bottom,left,bottom-curve);
			pen_mc.graphics.lineTo(left,top+curve);
			pen_mc.graphics.curveTo(left,top,left+curve,top);
			
		}
		
		private function _showTip(text_str:String) {
			fadingOut = false;
			//tip_mc.alpha = 0;
		
			setTipText(text_str);
			drawRect();
			
			location.stage.addEventListener(MouseEvent.MOUSE_MOVE, followMouse);
			if(tip_mc.alpha > 0){
				displayTip();
			}else{
				delayTimer.start();
			}
		
			//clearInterval(this_class.pauseID);
		}
		private function displayTip(e:TimerEvent = null) {
			tip_mc.visible = true;
			mouseFadeHandler = animateMovieClip(tip_mc, "alpha",1, 1);
		}
		
	
	// Folllow Mouse
	
	private function followMouse(event:MouseEvent) {
		tip_mc.x = location.mouseX + 1;
		tip_mc.y = location.mouseY - 1;
		
	}
		
		
	// ------ PUBLIC API ------

	// SHOW TIP
		public function showTip(text_str:String){
					
		//	clearInterval(pauseID);
		//	pauseID = setInterval(this._showTip,_delay,this,x,y,text_str);
			_showTip(text_str);
			
		}
		
	// HIDE TIP
		public function hideTip(){
			//clearInterval(pauseID);
			fadingOut = true;
			
			
			location.stage.removeEventListener(MouseEvent.MOUSE_MOVE, followMouse);
			
			if(mouseFadeHandler){
				mouseFadeHandler.stop();
				mouseFadeHandler = animateMovieClip(tip_mc, "alpha", 0, .25);
				mouseFadeHandler.addEventListener(TweenEvent.MOTION_FINISH, fadeFinished);
			}
			
				delayTimer.stop();
				delayTimer.reset();
			

			
		}
		
		private function fadeFinished(e:TweenEvent) {
			if (fadingOut){
				tip_mc.alpha = 0;
				tip_mc.visible = false;
			}
		}
		
		
	// USER DROP SHADOW
		public function set dropShadow(show:Boolean){
			if(show){
				shadow = new DropShadowFilter(10, 45, 0x000000, .8, 10, 10, 1, 3, false, false, false);
				tip_mc.filters = [shadow];
				
			}else{
				tip_mc.filters = [];
			}
			
		}
	// DELAY TIME
		public function set delay(_delay:Number){
			this._delay = _delay;
		}

	// ALIGN TEXT TO THE CENTER, LEFT, OR RIGHT
		public function set align(_align:String){
			this._align = _align;
			
			
		}
	// ANIMATE A SPECIFIC PROPETY OF A MOVIECLIP
		private function animateMovieClip(target_mc:Sprite, property:String ,target:Number,time:Number):Tween {
			var animateTween:Tween;
			animateTween = new Tween(target_mc, property, Regular.easeInOut, target_mc[property], target, time, true);
			return animateTween;
		}
	}
	
}