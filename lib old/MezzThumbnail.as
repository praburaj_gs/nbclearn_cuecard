package  
{
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.display.LoaderInfo;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Ari Oshinsky
	 */
	public class MezzThumbnail extends Sprite
	{
		private var clipID:String;
		private var imageURL:String;
		private var imageLoader:Loader;
		public var playButton:MezzPlayButton;
		private var clickHandler:Function;
		private var request:URLRequest;
		private var referenceClip:MovieClip;
		private var mainDragBox:MovieClip;
		
		
		public function MezzThumbnail(_referenceClip:MovieClip,_mainDragBox:MovieClip) 
		{
			
			/*
			var path:String = 'http://video-origin.nbcuni.com/mezzthumb/icue/6be19a67032c7e850182bcde11961149_mezzn.jpg';
			clearTextString = path+"|"+imageWidth+"|"+imageHeight+"|couldyoupassthesalt";
			hashedText = MD5C.calcMD5(clearTextString);
			trace("path = " + path);
			trace("clearTextString = " + clearTextString);
			trace("hashedText = " + hashedText);
			trace("imageWidth = " + imageWidth);
			trace("imageHeight = " + imageHeight);
			trace("phpCodePage = " + phpCodePage);
			// LOAD IMAGE
			tempW = imageWidth;
			tempH = imageHeight;
			//var loadListener:Object = new Object();
			movieLoader.addListener(this);
			//trace(phpCodePage+"?w="+String(imageWidth)+"&h="+String(imageHeight)+"&path="+path+"&hash="+hashedText+"&default="+Default);
			movieLoader.loadClip((phpCodePage + "?w=" + String(imageWidth) + "&h=" + String(imageHeight) + "&path=" + path + "&hash=" + hashedText + "&default=" + Default)
			*/
			mainDragBox = _mainDragBox;
			referenceClip = _referenceClip;
			request = new URLRequest();
			imageLoader = new Loader();
			this.addChild(imageLoader);
			playButton = new MezzPlayButton();
			playButton.buttonMode = true;
			playButton.useHandCursor = true;
			
			this.addChild(playButton);
			playButton.addEventListener(MouseEvent.MOUSE_OVER, function() {
				playButton.gotoAndStop(2);
			});
			playButton.addEventListener(MouseEvent.MOUSE_OUT, function() {
				playButton.gotoAndStop(1);
			});
			playButton.addEventListener(MouseEvent.CLICK, buttonClicked);
			
			
		}
		public function loadImage(_clipID:String, _imageURL:String, _clickHandler:Function) {
			this.visible = false;
				clickHandler = _clickHandler;
			
			clipID = _clipID;
			imageURL = _imageURL;
			request.url = _imageURL;
			imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, failedToLoadImage);
			imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoaded);
			imageLoader.load(request);
		}
		
		public function arrangeContent() {
			if(this.visible){
				mezzArrange();
			}
		}
		
		public function hidePlayButton() {
			trace("Hide Play Button");
			playButton.visible = false;
		}
		public function showPlayButton() {
			trace("Show Play Button");
			playButton.visible = true;
		}
		
		private function imageLoaded(e:Event = null) {
			trace("~Mezz Loaded");
			mezzArrange();
			this.visible = true;
			mainDragBox.visible = false;
		}
		private function mezzArrange(e:Event = null) {
			
			imageLoader.width = referenceClip.width;
			imageLoader.height = referenceClip.height;
			//trace("~this.parent.name: " + referenceClip.name);
			//trace("~this.parent.width: " + referenceClip.width);
			
			playButton.x = imageLoader.width / 2 - playButton.width / 2;
			playButton.y = imageLoader.height / 2 - playButton.height / 2;
			//trace("~playButton.x: " + playButton.x);
			//trace("~playButton.y: " + playButton.y);
			
		}
		private function failedToLoadImage(e:Event) {
			trace("! IMAGE LOAD FAILED");
			trace("!request.url: " + request.url.toString());
		}
		private function buttonClicked(e:MouseEvent) {
			this.visible = false;
			clickHandler(clipID);
		}
		
		
	}

}