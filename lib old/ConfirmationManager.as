﻿package 
{
	import flash.display.MovieClip;
	import fl.motion.Motion;
	import fl.transitions.easing.Strong;
	import flash.display.SpreadMethod;
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	import flash.events.MouseEvent;
	import flash.net.FileFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author 
	 */
	public class ConfirmationManager extends MovieClip {
		
		private var callBack:Function;
		private var label:String;
		private var discription:String;
		private var title_txt:TextField;
		
		public var button_1:MovieClip;
		private var button_2:MovieClip;
		private var buttonTxtFmt:TextFormat;
		private var mainTextFmt:TextFormat;
		private var fileTextFmt:TextFormat;
		
		private var _butonCallBack:Function;
		private var buttonColor:uint;
		private var _cancelFunction:Function;
		private var _cancelLabel:String;
		private var referenceObject:Object;
		private var pen_mc:Sprite;
		private var cueCard:AS3CueCard;
		private var allowPress:Boolean = false;
		
		
		public function ConfirmationManager(_cueCard, _referenceObject:Object = null) {
			
			referenceObject = _referenceObject;
			cueCard = _cueCard;
			pen_mc = new Sprite();
			addChild(pen_mc);
			//pen_mc.tabEnabled = false;
			
			main_mc.setChildIndex(main_mc.bg, main_mc.numChildren - 1);
			this.setChildIndex(this.main_mc, this.numChildren - 1);
			this.visible = false;
			this.x = -75;
			this.y = 100;
			
			
			// TITLE TEXT
			title_txt = new TextField();
			title_txt.multiline = true;
			title_txt.wordWrap = true;
			title_txt.width =  322;
			title_txt.autoSize = "center";
			title_txt.textColor = 0xFFFFFF;
			title_txt.antiAliasType = "normal";
			
			
			mainTextFmt = new TextFormat();
			mainTextFmt.color = 0xFFFFFF;
			mainTextFmt.bold = false;
			mainTextFmt.size = 14;
			mainTextFmt.font = "Arial";
			mainTextFmt.align = "center";
			
			
			
			button_1 = new MainMenuButton(); // Create a new button on the
			button_2 = new MainMenuButton(); // Create a new button on the
			
			buttonTxtFmt = new TextFormat();
			buttonTxtFmt.bold = true;
			buttonTxtFmt.align = "center";
			
			button_1.button_txt.setTextFormat(buttonTxtFmt);
			button_2.button_txt.setTextFormat(buttonTxtFmt);
			
			button_1.bg.width = 80;
			button_2.bg.width = 80;
			
			button_1.button_txt.x = -80;
			button_2.button_txt.x = -80;
			
			button_1.button_txt.width = 80;
			button_2.button_txt.width = 80;
			
			main_mc.addChild(title_txt);
			main_mc.addChild(button_1)
			main_mc.addChild(button_2);

			initEvents();
			
		}
		
		public function confirm(_discription:String, _label:String, _callBack:Function, showOnlyOneButton:Boolean = false) {
			allowPress = false;
			discription = _discription;
			label = _label;
		//	_butonCallBack = _callBack;
		
		
			title_txt.text = discription;
			if (this.title_txt.width > 300) {
				this.title_txt.width = 300;
			}
			this.title_txt.setTextFormat(mainTextFmt);
			
			title_txt.x = 300 / 2 - title_txt.width / 2
			title_txt.y = 90 / 4 - title_txt.height / 2
			
			button_1.y = title_txt.y + title_txt.height + 15; // possition the buttons under the title text
				
				
				
			button_2.y  = button_1.y 
				
			
			if (showOnlyOneButton == true) {
				button_2.visible = false;
				button_1.x = title_txt.x + (title_txt.width / 2)  + button_1.width/2;
			}else {
				button_2.visible = true;
				button_1.x = title_txt.x + (title_txt.width / 2)  - 5;
				button_2.x = button_1.x + button_2.width + 10;
			}
			button_2.x = button_1.x + button_2.width + 10;
			
			setButtonAtributes(_label, _cancelLabel, 0x81002c, _callBack);
			main_mc.bg.height = button_1.y + 20;
			
			this.visible = true;
			if (referenceObject) {
				this.x = - 229;
				this.y = -1;
				
				drawCoverRect(referenceObject.width + 20, referenceObject.height + 32); // set the heigt
				main_mc.x = (referenceObject.width / 2) - main_mc.width / 2;
				main_mc.y = (referenceObject.height / 2) - main_mc.height / 2;
			}
			cueCard.addItemToTabOrder(title_txt, "");
			cueCard.addItemToTabOrder(button_1, _label);
			cueCard.addItemToTabOrder(button_2, _cancelLabel);
			
			if(cueCard.fmanager.getFocus()){
				//cueCard.fmanager.
				cueCard.fmanager.setFocus(title_txt);
			}
			allowPress = true;
			//var waitTimer:Timer = new Timer(200, 1);
			//waitTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent) {
				
			//});
			//waitTimer.start();
		}
		private function drawCoverRect(_width:Number,_height:Number){
			pen_mc.graphics.clear();
			pen_mc.graphics.moveTo(0, 0);
			pen_mc.graphics.beginFill(0, .5);
			pen_mc.graphics.drawRect(0,0, _width+6, _height+30);// Sets the black cover to exactly the same proportions as the mask
			pen_mc.graphics.endFill();
		}
		
		public function setButtonAtributes(label_1:String, label_2:String, _buttonColor:uint,_callBack:Function = null) {
			
		// Text
			button_1.button_txt.text = label_1;
			button_2.button_txt.text = label_2;
			button_1.button_txt.setTextFormat(buttonTxtFmt);
			button_2.button_txt.setTextFormat(buttonTxtFmt);
			
			
		// Colors
			buttonColor = _buttonColor;
			Colors.changeColor(button_1.bg.grfx, _buttonColor) // set the color of the bg
			Colors.changeColor(button_2.bg.grfx, _buttonColor) // set the color of the bg
			if(_callBack != null){
				_butonCallBack = _callBack;
			}
		}
		
		
		
		
		
		public function possition(_x:Number, _y:Number) {
			if (_x) {
				this.x = _x;
			}
			if (_y) {
				this.y = _y;
			}
		}
		private function initEvents() {
				// First Button
			button_1.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				buttonPressed();
			});
			button_1.addEventListener(KeyboardEvent.KEY_UP, function(e:KeyboardEvent) {
				if (e.keyCode == 13 || e.keyCode == 32) {   // Enter Key and Space Bar
					trace("!~ KEYBOARD CONFIRMATION BUTTON PRESSED");
					buttonPressed();
				}
			});
			
			button_1.addEventListener(MouseEvent.ROLL_OVER, btnRollOver);
			button_1.addEventListener(MouseEvent.ROLL_OUT, btnRollOut);
			
			// Cancel Button
			button_2.addEventListener(MouseEvent.CLICK, cancelPressed);
			button_2.addEventListener(MouseEvent.ROLL_OVER, btnRollOver);
			button_2.addEventListener(MouseEvent.ROLL_OUT, btnRollOut);
				// make the mouse hand visible on roll over
			button_2.buttonMode = true;
			button_2.useHandCursor = true;
			button_1.buttonMode = true;
			button_1.useHandCursor = true;
			
			
		}
		
		public function  get butonCallBack():Function {
			return _butonCallBack;
		}
		
		public function  setCancelFunction(_label:String,cancelFunction:Function):void {
			_cancelFunction = cancelFunction;
			_cancelLabel = _label
		}
		
	//	public function get cancelFunction() {
			//return _cancelFunction;
	//	}
		
		private function btnRollOver(event:MouseEvent) {
			
			var btn:MovieClip = event.currentTarget as MovieClip;
			Colors.changeColor(btn.bg.grfx, 0x333333) // set the color of the bg
		}
		private function btnRollOut(event:MouseEvent) {
			
			var btn:MovieClip = event.currentTarget as MovieClip;
			Colors.changeColor(btn.bg.grfx, buttonColor) // set the color of the bg
		}
		
		
		
		private function buttonPressed() {
			if( allowPress == true){
				if(_butonCallBack != null){
					_butonCallBack(); // tell the listener that a button has been pressed
					
					
				}else {
					
					cueCard.setMainFocus(button_1); // set the focus so that you don't lose keyboard access.
				}
				this.visible = false;
				pen_mc.graphics.clear();
			}
		}
		private function cancelPressed(e:MouseEvent) {
		
			this.visible = false;
			pen_mc.graphics.clear();
			if (_cancelFunction != null) {
				_cancelFunction();
			}else {
				cueCard.setMainFocus(button_2);
			}
		}
	}
	
}