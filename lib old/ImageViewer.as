﻿package 
{
	import fl.controls.TextArea;
	import flash.display.MovieClip;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.display.Loader
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.Timer;
	import flash.text.TextFormat;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	import flash.events.FullScreenEvent;
	import fl.transitions.Tween;
	import flash.events.KeyboardEvent;
	import flash.events.FocusEvent;
	
	/**
	 * ...
	 * @author Ari
	 */
	public class ImageViewer extends MovieClip{
		private var imageLoader:Loader;
		private var image_mc:Sprite;
		private var imageLoadWidth:Number;
		private var imageLoadHeight:Number;
		private var viewerWidth:Number;
		private var viewerHeight:Number;
		private var imageBaseX:Number;
		private var imageBaseY:Number;
		private var zoomTimer:Timer;
		private var zoomAmount:Number;
		private var zoomFactor:Number = 1;
		private var numberOfImages:int;
		private var imageIndex:int;
		private var imageList:XMLList;
		private var pageList_array:Array;
		private var handCursor:MovieClip;
		public var fullScreen:Boolean = false;
		private var controlsSize_obj:Object;
		private var blankScreen_mc:Sprite;
		
		private var fullScreenFadeTimer:Timer;
		private var fadeOutTimer:Timer;
		private var controlsVisibleInFullScreen:Boolean = true;
		private var fadeOutTween:Tween;
		
		private var cueCard:AS3CueCard;
		public var loadingFirstImage:Boolean = true;
		
	
		
		public function ImageViewer() {
			imageLoader = new Loader();
			
			
			
			imageLoader.contentLoaderInfo.addEventListener(Event.INIT, imageLoaded);
			image_mc = new Sprite();
			image_mc.mask = image_mask;
			this.addChild(image_mc);
			image_mc.addChild(imageLoader);
			imageBaseX = image_mc.x;
			imageBaseY = image_mc.y;
			var targetDepth = this.numChildren - 1;
			this.setChildIndex(controls_mc, targetDepth);
			//handCursor = new HandCursor();
			//this.addChild(handCursor);
			eventSetUp();
			
			fullScreenFadeTimer = new Timer(2000, 1);
			fullScreenFadeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent) {
				Mouse.hide();
				fadeOut(e);
				controlsVisibleInFullScreen = false;
			});
			fadeOutTimer = new Timer(1200, 1);
			fadeOutTimer.addEventListener(TimerEvent.TIMER_COMPLETE , fadeOut);
			
		}
		
		private function fullScreenFadeIn(e:MouseEvent){
			if (!controlsVisibleInFullScreen) {
				controlsVisibleInFullScreen = true;
				Mouse.show();
				fadeIn();
			}
			fullScreenFadeTimer.stop();
			fullScreenFadeTimer.reset();
			fullScreenFadeTimer.start();
		}
		
		
		
		
		// Regullar screen Cue Card only
		public function fadeInImageControls(object:Object = null):void {
			if(fadeOutTween){
				fadeOutTween.stop();
			}
			fadeOutTimer.reset();
			fadeOutTween = cueCard.animateMovieClip(controls_mc, "alpha",1, .5);
		}
		
		public function fadeOutImageControls(object:Object = null):void {
			//if(!fullScreen){
				fadeOutTimer.reset();
				fadeOutTimer.start();
			//}

		}
		
		// FADE ITSELF
		
		public function fadeIn(object:Object = null):void {
			if(fadeOutTween){
				fadeOutTween.stop();
			}
			
			fadeOutTween = cueCard.animateMovieClip(controls_mc, "alpha",1, .5);
		}
		
		private function fadeOut(e:TimerEvent) {
		
			if(fadeOutTween){
				fadeOutTween.stop();
			}
			fadeOutTween = cueCard.animateMovieClip(controls_mc, "alpha",0, 2);
		}

		
		private function startZoomTween() {
			zoomTimer.start();
			cueCard.stage.addEventListener(MouseEvent.MOUSE_UP, function() {
				zoomTimer.stop();
				cueCard.stage.removeEventListener(MouseEvent.MOUSE_UP, zoom);
			});
		}
		
		private function eventSetUp() {
			zoomTimer = new Timer(100);
			zoomTimer.addEventListener(TimerEvent.TIMER, zoom);
			
			// CLICK CONTROL
			controls_mc.zoomIn_btn.addEventListener(KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent) {
				if (event.keyCode == 13 || event.keyCode == 32) {
					zoomAmount = .05;
					zoom();
				}
			});
			controls_mc.zoomIn_btn.addEventListener(MouseEvent.MOUSE_DOWN, function() {
				zoomAmount = .05;
				zoom();
				startZoomTween();
				
			});
			controls_mc.zoomOut_btn.addEventListener(MouseEvent.MOUSE_DOWN, function() {
				zoomAmount = -.06;
				zoom();
				startZoomTween();
			});
			
			controls_mc.zoomOut_btn.addEventListener(KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent) {
				if (event.keyCode == 13 || event.keyCode == 32) {
					zoomAmount = -.06;
					zoom();
				}
			});
			
			image_mc.addEventListener(MouseEvent.MOUSE_DOWN, function() {
				image_mc.startDrag();
				//handCursor.gotoAndStop("closed");
				cueCard.stage.addEventListener(MouseEvent.MOUSE_UP, stopDragging);
				cueCard.stage.addEventListener(MouseEvent.MOUSE_MOVE, arrangeContent);
			});
			image_mc.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				Mouse.cursor = MouseCursor.HAND;
			});
			image_mc.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				Mouse.cursor = MouseCursor.AUTO;
			});
			
			controls_mc.pagination.next_btn.addEventListener(MouseEvent.CLICK, nextImage);
			controls_mc.pagination.prev_btn.addEventListener(MouseEvent.CLICK, prevImage);
			
			
			// Tip settings
			
			controls_mc.related_btn.tipText = "Related CueCards";
			controls_mc.fullScreen_btn.tipText = "Show in Full Screen";
			controls_mc.fitWindow_btn.tipText = "Fit Image in CueCard";
			controls_mc.zoomIn_btn.tipText = "Zoom In";
			controls_mc.zoomOut_btn.tipText = "Zoom Out";
			
			
			
			// COLOR CONTROL
			controls_mc.zoomIn_btn.addEventListener(MouseEvent.ROLL_OVER, hiLightControl);
			controls_mc.zoomIn_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			
			controls_mc.zoomOut_btn.addEventListener(MouseEvent.ROLL_OVER, hiLightControl);
			controls_mc.zoomOut_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			
			
			controls_mc.fullScreen_btn.addEventListener(MouseEvent.ROLL_OVER, hiLightControl);
			controls_mc.fullScreen_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			
			controls_mc.fitWindow_btn.addEventListener(MouseEvent.ROLL_OVER, hiLightControl);
			controls_mc.fitWindow_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			
			controls_mc.related_btn.addEventListener(MouseEvent.ROLL_OVER,  hiLightControl);
			
			controls_mc.related_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			

			controls_mc.fullScreen_btn.addEventListener(MouseEvent.CLICK, toggleFullScreen);
			controls_mc.fitWindow_btn.addEventListener(MouseEvent.CLICK, matchToSize);
			controls_mc.related_btn.visible = false;
			controls_mc.related_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				if (fullScreen) {
					toggleFullScreen();
				}
				if(!cueCard.videoPlayer.related_vids){
					cueCard.videoPlayer.loadRelatedVideos(true);
				}else {
					cueCard.videoPlayer.displayRelatedVideos();
				}
				//cueCard.imageViewer.visible = false;
				//cueCard.videoPlayer.related_vids.visible = true;
			});
		
			
			
			controls_mc.fullScreen_btn.useHandCursor = true;
			controls_mc.fullScreen_btn.buttonMode = true;
			controls_mc.fitWindow_btn.useHandCursor = true;
			controls_mc.fitWindow_btn.buttonMode = true;
			
			controls_mc.related_btn.useHandCursor = true;
			controls_mc.related_btn.buttonMode = true;
			
			controls_mc.zoomIn_btn.useHandCursor = true;
			controls_mc.zoomIn_btn.buttonMode = true;
			
			controls_mc.zoomOut_btn.useHandCursor = true;
			controls_mc.zoomOut_btn.buttonMode = true;
			
		// SET UP EVENTS TO SHOW AND HIDE THE Video Controls based on keyboard focus
			controls_mc.addEventListener(FocusEvent.FOCUS_IN, fadeInImageControls);
			controls_mc.addEventListener(FocusEvent.KEY_FOCUS_CHANGE, fadeInImageControls);
			controls_mc.addEventListener(FocusEvent.FOCUS_OUT, fadeOutImageControls);
			
			controls_mc.addEventListener(MouseEvent.ROLL_OVER, fadeInImageControls);
			controls_mc.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				if(!fullScreen){
					fadeOutImageControls();
				}
			});
			
			image_mc.addEventListener(MouseEvent.ROLL_OVER, function() {
				if(!fullScreen){
					cueCard.imageViewer.fadeInImageControls();
				}
			});
			image_mc.addEventListener(MouseEvent.ROLL_OUT, function() {
				if(!fullScreen){
					cueCard.imageViewer.fadeOutImageControls();
				}
			});
			controlsSize_obj = new Object();
			
			
			
		}
		
		
		 private function toggleFullScreen(event:MouseEvent = null):void {
            switch(stage.displayState) {
                case "normal":
                    cueCard.stage.displayState = "fullScreen";    
                    break;
                case "fullScreen":
                default:
                    cueCard.stage.displayState = "normal";    
                    break;
            }
        }    
		
		private function _fullScreenEvent(event:FullScreenEvent) {
			
			if (!cueCard.imageMode) { 
				return; // these functions are only for image viewer
			}
			if (event.fullScreen) {
				
				fullScreen = true; 
				image_mc.mask = null
				image_mask.visible = false;
				
				// SAVE THE PRESENT STATE
				controlsSize_obj.fullScreen_btn = controls_mc.fullScreen_btn.x;
				controlsSize_obj.fitWindow_btn = controls_mc.fitWindow_btn.x;
				controlsSize_obj.related_btn = controls_mc.related_btn.x;
				controlsSize_obj.base_mc = controls_mc.base_mc.width;
				controlsSize_obj.x = controls_mc.x;
				controlsSize_obj.y = controls_mc.y;
				controlsSize_obj.panel_width = cueCard.mask2.width;
				controlsSize_obj.panel_height = cueCard.mask2.height;
				controlsSize_obj.cueCardX = cueCard.x;
				controlsSize_obj.cueCardY = cueCard.y;
				controlsSize_obj.Xscale = controls_mc.scaleX;
				controlsSize_obj.Yscale = controls_mc.scaleY;
				controlsSize_obj.Zscale = controls_mc.scaleZ;
				controlsSize_obj.paginationX = controls_mc.pagination.x;
				controlsSize_obj.loading_mcX = cueCard.loading_mc.x;
				controlsSize_obj.loading_mcY = cueCard.loading_mc.y;
				
				
				// MAKE FULL SCREEN CHANGES
				cueCard.x = 223;
				cueCard.y = -41.50;
				cueCard.always_mc.visible = false;
				cueCard.mask2.height = cueCard.stage.stageHeight; // set mask height to full stage width
				cueCard.mask2.width = cueCard.stage.stageWidth; // set mask height to full stage width
				
				controls_mc.scaleX = 1.4;
				controls_mc.scaleY = 1.4;
				controls_mc.zoomIn_btn.scaleX = .9;
				controls_mc.zoomIn_btn.scaleY = .9;
				controls_mc.zoomOut_btn.scaleX = .9;
				controls_mc.zoomOut_btn.scaleY = .9;
				
				controls_mc.base_mc.width = cueCard.stage.stageWidth / 2;
				controls_mc.related_btn.x = controls_mc.base_mc.width - 35;
				controls_mc.fullScreen_btn.x = controls_mc.related_btn.x - 28;
				controls_mc.fitWindow_btn.x =  controls_mc.fullScreen_btn.x -19
				controls_mc.x =  cueCard.stage.stageWidth / 2 - controls_mc.width / 2	
				centerControls();
				controls_mc.y = cueCard.stage.stageHeight - 55;
				cueCard.loading_mc.x = controls_mc.x + controls_mc.base_mc.width / 2 - cueCard.loading_mc.width;
				cueCard.loading_mc.y = cueCard.stage.stageHeight / 2; - cueCard.loading_mc.height/2;
				
				
				cueCard.background_mc.visible = false;
				cueCard.shadowBackground.visible = false;
			
				arrangeContent();
				
				cueCard.stage.addEventListener(MouseEvent.MOUSE_MOVE, fullScreenFadeIn);
				//image_mc.addEventListener(KeyboardEvent.KEY_DOWN, cueCard.downKeyDetection);
				
				cueCard.omniture.trackEvent("event35", "Full Screen Image");
				
			}else {
				
				image_mask.visible = true;
				image_mc.mask = image_mask
				
				//  --- REVERSE ALL THE CHANGES MADE FOR FULL SCREEN ----
				controls_mc.fullScreen_btn.x = controlsSize_obj.fullScreen_btn;
				controls_mc.fitWindow_btn.x = controlsSize_obj.fitWindow_btn;
				controls_mc.related_btn.x = controlsSize_obj.related_btn;
				controls_mc.base_mc.width = controlsSize_obj.base_mc;
				controls_mc.x = controlsSize_obj.x;
				controls_mc.y = controlsSize_obj.y;
				
				cueCard.mask2.width = controlsSize_obj.panel_width;
				cueCard.mask2.height = controlsSize_obj.panel_height;
				cueCard.x = controlsSize_obj.cueCardX;
				cueCard.y = controlsSize_obj.cueCardY;
				controls_mc.pagination.x = controlsSize_obj.paginationX;
				
				controls_mc.scaleX = controlsSize_obj.Xscale;
				controls_mc.scaleY = controlsSize_obj.Yscale;
				
				cueCard.loading_mc.x = controlsSize_obj.loading_mcX;
				cueCard.loading_mc.y = controlsSize_obj.loading_mcY;
				
				cueCard.always_mc.visible = true;
				cueCard.background_mc.visible = true;
				cueCard.shadowBackground.visible = true;
				
				controls_mc.zoomIn_btn.scaleX = 1;
				controls_mc.zoomIn_btn.scaleY = 1;
				controls_mc.zoomOut_btn.scaleX = 1;
				controls_mc.zoomOut_btn.scaleY = 1;
				
				arrangeContent();
				matchToSize();
				cueCard.stage.removeEventListener(MouseEvent.MOUSE_MOVE, fullScreenFadeIn);
				//image_mc.removeEventListener(KeyboardEvent.KEY_DOWN, cueCard.downKeyDetection);
				fullScreenFadeTimer.stop();
				fullScreenFadeTimer.reset();
				
				Mouse.show();
				fadeIn();
				controlsVisibleInFullScreen = false;
				var fscreencloseTimer:Timer = new Timer(300, 1);
				fscreencloseTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent) {
					fullScreen = false; 
					
				});
				fscreencloseTimer.start();
				// ----------------
			}
		}
		
		private function stopDragging(e:MouseEvent = null) {
			image_mc.stopDrag();
			//handCursor.gotoAndStop("open");
			cueCard.stage.removeEventListener(MouseEvent.MOUSE_UP, stopDrag);
			cueCard.stage.removeEventListener(MouseEvent.MOUSE_MOVE, arrangeContent);
		}
		public function setCueCard(_cueCard:AS3CueCard) {
			cueCard = _cueCard;
			cueCard.stage.addEventListener(FullScreenEvent.FULL_SCREEN, _fullScreenEvent);
		}
	// CLEAN OUT ALL THE PAGE BUTTONS
		private function clearButtons() {
			if(pageList_array){ // if there is an exsisting page list than clear it out
				for (var index:int = pageList_array.length-1 ; index > -1 ; index--) {
					try {
						controls_mc.pagination.removeChild(pageList_array[index]);
						pageList_array.pop();
					}catch (e:Error) {
						trace("Error: " + e.toString());
					}
				}
			}
			pageList_array = new Array();
		}
		
		private function hiLightControl(e:MouseEvent,alterniteTarget:Object = null) {
			if (alterniteTarget) {
				Colors.changeTint(alterniteTarget, 0xff6600, 1) // Orange
			}else{
				//Colors.changeTint(e.currentTarget,0x4D40BF,.6)
				Colors.changeTint(e.currentTarget, 0xff6600, 1) // Orange
				//	Colors.changeTint(e.currentTarget, 0x164E9B, 1); // Blue
				cueCard.tip.showTip(e.currentTarget.tipText);
			}
				
		
			
			
			

			
		}
		private function unHiLightControl(e:MouseEvent,alterniteTarget:Object = null) {
			if (alterniteTarget) {
				Colors.changeTint(alterniteTarget, 0xFFFFFF, .0);
			}else{
				Colors.changeTint(e.currentTarget, 0xFFFFFF, .0);
			}
			cueCard.tip.hideTip();
		}
		
		public function loadImageXML(_images:XMLList) {
			loadingFirstImage = true;
			imageList = _images;
			controls_mc.related_btn.visible = false;
			clearButtons();
			if(image_mc.parent != this){
				this.addChild(image_mc);
				var targetDepth = this.numChildren - 1;
				this.setChildIndex(controls_mc, targetDepth)
			}
			
			
			
			
			
			if (imageList.length() > 1) {
				for (var index:int = 0 ; index < imageList.length() ; index++) {
					var imageId:MovieClip = new ImageIDButton();
					imageId.label_txt.text = String(index + 1);
					imageId._index = index;
					imageId.underline_txt.visible = false;
					var prevImageID:ImageIDButton = pageList_array[index - 1];
					if (prevImageID) {
						imageId.x = prevImageID.x + prevImageID.width + 1;
						imageId.y = prevImageID.y;
					}else {
						imageId.x = controls_mc.pagination.prev_btn.x + 7;
						imageId.y = 1;
					}
					imageId.hit_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
						loadImage(e.currentTarget.parent._index);
					});
					imageId.hit_btn.addEventListener(MouseEvent.MOUSE_OVER, function(event:MouseEvent) {
						
						if(imageIndex != event.currentTarget.parent._index){
							var textFormat:TextFormat = new TextFormat();
							textFormat.color = 0xff6600;
							textFormat.bold = true;
							event.currentTarget.parent.label_txt.setTextFormat(textFormat);
						}
						
					});
					imageId.hit_btn.addEventListener(MouseEvent.MOUSE_OUT, function(event:MouseEvent) {
						if(imageIndex != event.currentTarget.parent._index){
							var textFormat:TextFormat = new TextFormat();
							textFormat.color = 0xCCCCCC;
							textFormat.bold = false;
							event.currentTarget.parent.label_txt.setTextFormat(textFormat);
						}
					});
						
				
					
					
					imageId.hit_btn.useHandCursor = true;
					imageId.hit_btn.buttonMode = true;
					pageList_array.push(imageId);
					controls_mc.pagination.addChild(imageId);
					controls_mc.pagination.next_btn.visible = true;
					controls_mc.pagination.prev_btn.visible = true;
				}
				
				
			
			}else { // if there is only one image dont do anything
					var imageIdSolo:MovieClip = new ImageIDButton();
					imageIdSolo.label_txt.text = "1"
					pageList_array.push(imageIdSolo);
					controls_mc.addChild(imageIdSolo);
					imageIdSolo.x = controls_mc.pagination.prev_btn.x + 7;
					imageIdSolo.y = 7;
					imageIdSolo.visible = false;
					controls_mc.pagination.next_btn.visible = false;
					controls_mc.pagination.prev_btn.visible = false;
				
			}
			
			controls_mc.pagination.next_btn.x = pageList_array[pageList_array.length - 1].x + 23;
			centerControls();
			loadImage(0);
			arrangeContent();
			
			// SET TAB ORDER:
			
			cueCard.addItemToTabOrder(controls_mc.zoomIn_btn, "Zoom In");
			cueCard.addItemToTabOrder(controls_mc.zoomOut_btn, "Zoom Out");
			cueCard.addItemToTabOrder(controls_mc.pagination.prev_btn, "Previous Image");
			for (var tabIndex:int = 0 ; tabIndex < pageList_array.length ; tabIndex++) {
				var imageId_btn:MovieClip = pageList_array[tabIndex];
				cueCard.addItemToTabOrder(imageId_btn.hit_btn, "Image Number " + String(tabIndex + 1));
			}
			cueCard.addItemToTabOrder(controls_mc.pagination.next_btn, "Next Image");
			
			cueCard.addItemToTabOrder(controls_mc.fitWindow_btn, "Fit Image to Screen");
			cueCard.addItemToTabOrder(controls_mc.fullScreen_btn, "Display Image in full screen");
			cueCard.addItemToTabOrder(controls_mc.related_btn, "Related Videos");
		}
		private function loadImage(_index:int) {
			resetPagination();
			imageIndex = _index;
			cueCard.loading_mc.visible = true;
			
			var _imageUrl:String;
			if (imageList.length() > 1) {
				_imageUrl = imageList[_index].UrlPathPrefix.toString() + imageList[_index].CueCardImage.toString();
			
			}else {
				_imageUrl = imageList.UrlPathPrefix.toString() + imageList.CueCardImage.toString();
			}
			var request:URLRequest = new URLRequest(_imageUrl);
			imageLoader.load(request);
		
		}
		public function removeImage() {
			if(image_mc.parent == this){
				this.removeChild(image_mc);
			}
		}
		public function nextImage(e:MouseEvent = null){
			imageIndex += 1;
			if(imageIndex >= imageList.length()){
				imageIndex = 0;
			}
			
			loadImage(imageIndex);
		}
		
		public function prevImage(e:MouseEvent = null){
			imageIndex -= 1;
			if(imageIndex < 0){
				imageIndex = (imageList.length() -1);
			}
			
			loadImage(imageIndex);
		}
		
		private function imageLoaded(e:Event) {
			
			imageLoadWidth = imageLoader.contentLoaderInfo.width;
			imageLoadHeight = imageLoader.contentLoaderInfo.height;
			matchToSize();
			// UPDATE THE  CONTROLS 
				var textFormat:TextFormat = new TextFormat();
				textFormat.color = 0xFFFFFF;
				
				for (var index:int = 0 ; index < pageList_array.length ; index++) {
					var label_txt:TextField = pageList_array[index].label_txt;
					if(imageIndex == index){
						textFormat.color = 0xFFFFFF;
						textFormat.bold = true;
						label_txt.setTextFormat(textFormat);
						pageList_array[index].underline_txt.visible = true;
					}else {
						textFormat.color = 0xCCCCCC;
						textFormat.bold = false;
						label_txt.setTextFormat(textFormat);
						pageList_array[index].underline_txt.visible = false;
						
					}
				}
				
				cueCard.loading_mc.visible = false;
				if (loadingFirstImage) {
					loadingFirstImage = false;
					cueCard.videoPlayer.loadRelatedVideos();
				}

		}
		
		private function resetPagination() {
			var textFormat:TextFormat = new TextFormat();
			textFormat.color = 0xCCCCCC;
			textFormat.bold = false;
			if (!pageList_array) {
				return;
			}
			if (pageList_array.length < 1) {
				return;
			}
			for (var index:int = 0 ; index < pageList_array.length ; index++) {
					var label_txt:TextField = pageList_array[index].label_txt;
						
						label_txt.setTextFormat(textFormat);
						pageList_array[index].underline_txt.visible = false;
						
					
				}
		}
		
		public function centerControls() {
			
			controls_mc.pagination.x = controls_mc.base_mc.width / 2 - controls_mc.pagination.width / 2;
		}
		public function panImage(_horizontal:Number, _verticle:Number) {
			
			image_mc.x += _horizontal;
			image_mc.y += _verticle;
			arrangeContent();
		}
		
		public function zoom(e:TimerEvent = null, _zoomAmountOveride:Number = 0){
						
			// Save the preScale Width
			var preWidth:Number = image_mc.width;
			var preHeight:Number = image_mc.height
			
			if(_zoomAmountOveride != 0){
				image_mc.scaleX += _zoomAmountOveride;
				image_mc.scaleY += _zoomAmountOveride;
			}else {
				image_mc.scaleX += zoomAmount;
				image_mc.scaleY += zoomAmount;
			}
			
			//Zoom the window in a centered manner
			var postWidth:Number = image_mc.width;
			var postHeight:Number = image_mc.height
			var changeW:Number = postWidth - preWidth;
			var changeH:Number = postHeight - preHeight;
			
			// Move the image over to compensate for the scale
			image_mc.x -= changeW/2;
			image_mc.y -= changeH/2;
			
			
			arrangeContent();
			//preventImageClipping();
		
		}
		
		public function matchToSize(event:Object = null) {
			//if (image_mc.parent != this) {
				//return;
			//}
			viewerWidth = cueCard.mask2.width;
			viewerHeight = cueCard.mask2.height;
			
			var targetRatio:Number = viewerWidth / viewerHeight; // Wider has a larger ration number targetRatio = 2 means that the image is twice as wide as high
			var imageRatio:Number = imageLoadWidth / imageLoadHeight;		// a ratio value of .5 means that the image is twice has high as wide
			
			//this.target_ratio_txt.text = "targetRatio: " + targetRatio;
			//this.image_ratio_txt.text = "imageRatio: " + imageRatio;
			
			if (imageRatio > targetRatio) { // if the image is wider than the avalible space
				image_mc.width  = viewerWidth;
				image_mc.height = image_mc.width / imageRatio;
				image_mc.y = imageBaseY + (viewerHeight / 2) - (image_mc.height / 2)
				image_mc.x = 0;
			}else {
				image_mc.height  = viewerHeight;
				image_mc.width = image_mc.height * imageRatio;
				image_mc.x = imageBaseX + (viewerWidth / 2) - (image_mc.width / 2)
				image_mc.y = 0;
			}
			//image_mc.width = cueCard.
		}
		
		public function arrangeContent(e:Object = null) {
			if (image_mc.parent != this) {
				return;
			}
			viewerWidth = cueCard.mask2.width;
			viewerHeight = cueCard.mask2.height;
			image_mask.width = viewerWidth;
			image_mask.height = viewerHeight;
			bg.width = viewerWidth;
			bg.height = viewerHeight;
			
		
			
			var targetRatio:Number = viewerWidth / viewerHeight; // Wider has a larger ratio number targetRatio = 2 means that the image is twice as wide as high (Wide Screen)
			var imageRatio:Number = imageLoadWidth / imageLoadHeight;		// a ratio value of .5 means that the image is twice has high as wide (Upright)
			
			
			// X AND Y ADJUSTMENT
			// Dont allow the picture to go lower than the viewer top frame
			if (image_mc.y > imageBaseY) {
				image_mc.y = imageBaseY
			}
			// Dont allow the picture to go more to the right than the viewer left frame
			if (image_mc.x > imageBaseX) {
				image_mc.x = imageBaseX
			}
			// Dont allow the picture to be dragged off the screen
			if (image_mc.y < viewerHeight - image_mc.height) {
				image_mc.y = viewerHeight - image_mc.height;
			}
			// Dont allow the picture to be dragged off the screen
			if (image_mc.x < viewerWidth - image_mc.width) {
				image_mc.x = viewerWidth - image_mc.width
			}
			
		
	
		
		// WIDTH AND HEIGHT ADJUSTMENT
			if (imageRatio >= targetRatio) { // if the image is wider than the avalible space
				
				if (image_mc.width <= viewerWidth) {
					image_mc.width = viewerWidth
					image_mc.height = image_mc.width / imageRatio;
					image_mc.x = imageBaseX;
				}
				// Center picture verticly
				if(image_mc.height <= viewerHeight){
					image_mc.y = imageBaseY + (viewerHeight / 2) - (image_mc.height / 2)
				}
			}
			if (imageRatio <= targetRatio) { // if the image is taller than the avalible space
				if (image_mc.height <= viewerHeight) {
					image_mc.height = viewerHeight
					image_mc.width = image_mc.height * imageRatio;
					image_mc.y = imageBaseY;
				}
				// Center Picture Horizontally
				if (image_mc.width <= viewerWidth) {
					image_mc.x = imageBaseX + (viewerWidth / 2) - (image_mc.width / 2);
				}
			}
		}
	}	
}