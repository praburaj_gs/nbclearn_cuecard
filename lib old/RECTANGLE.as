﻿package 
{
	import flash.display.Sprite;
	import flash.display.GradientType;
	import flash.display.SpreadMethod;
	import flash.geom.Matrix;
	/**
	 * ...
	 * @author 
	 */
	public class RECTANGLE extends Sprite 
	{
		public function RECTANGLE(_width:Number,_height:Number,_color:Number,_color2 = null,rotation = null){
			if (_color2) {
				var my_matrix:Matrix = new Matrix();
				var radians:Number = 0;
				if (rotation) {
					radians = Math.PI*rotation/180; // convert to radians
				}
				//trace("rotation: " + rotation);
				//trace("radians: " + radians);
				my_matrix.createGradientBox(_width, _height, radians);
				this.graphics.beginGradientFill(GradientType.LINEAR, [_color, _color2], [1,1], [0, 255],my_matrix,SpreadMethod.PAD,"rgb",1);
			}else{
				this.graphics.beginFill(_color, 1);
			}
			this.graphics.drawRect(0,0,_width,_height);
		}
	}
}