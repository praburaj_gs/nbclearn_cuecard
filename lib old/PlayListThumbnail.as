﻿package 
{
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;
	
	/**
	 * ...
	 * @author Ari
	 */
	public class PlayListThumbnail extends MovieClip 
	{
		public var data:Object;
		public var metaData:*;
		public var imageLoader:Loader;
		private var title:TextField;
		private var image:Sprite;
		private var playListWindow:PlayListWindow;
		
		public function PlayListThumbnail(_data:Object, _playListWindow ) {
			
			playListWindow = _playListWindow;
			image = new Sprite();
			data = _data;
			metaData = data.Metadata;
			initListeners();
		}
		
		private function initListeners() {
			close_btn.addEventListener(MouseEvent.CLICK, deleteListener);
			image.addEventListener(MouseEvent.CLICK, thumbnailClicked);
			title_txt.addEventListener(MouseEvent.CLICK, thumbnailClicked);
			image.buttonMode = true;
			image.useHandCursor = true;
			
		}
		private function deleteListener(e:MouseEvent) {
			playListWindow.cueCard.confirmation.confirm("Are you sure you want to delete this Cue Card?","Delete",deleteConfirmed);
			
			
		}
		private function deleteConfirmed(e:Event = null) {
			playListWindow.deleteThumb(this);
		}
		
		public function remove() {
			data = null;
			metaData = null;
			imageLoader = null;
			image = null;
			playListWindow = null;
			close_btn.removeEventListener(MouseEvent.CLICK, deleteListener);
		}
		
		private function thumbnailClicked(e:MouseEvent) {
			
			playListWindow.playVideo(data.Asset_Id);
		}
		
		
		
		public function loadImage(_imageURL:String) {
			
			
			var urlRequest:URLRequest = new URLRequest(_imageURL);
			
			imageLoader = new Loader();
			
			imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, buildThumbnail);
			
			imageLoader.load(urlRequest);
			
			
			addChild(image);
			image.addChild(imageLoader)
		}
		
		
		
		
		public function getHeight():Number {
			return image.height as Number;
		}
		
		public function getWidth():Number {
			//return Number(Number(image.width) + Number(title_txt.width) + 5);
			return (close_btn.x + 15);
		}
		
		
		private function buildThumbnail(e:Event) {
			
			var loadedContent = e.currentTarget;
			
			
			image.width = 65;
			image.height = 51;
			
			title_txt.width = 132;
			title_txt.height = 52;
			title_txt.wordWrap = true;
			this.title_txt.text = data.Title;
			this.title_txt.x = image.width + 5;
			this.close_btn.x = this.title_txt.x + this.title_txt.width + 10;
			
			playListWindow.arrangeContent();
		}
		
	}
	
}