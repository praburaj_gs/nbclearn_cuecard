package 
{
	
	import org.osmf.media.DefaultMediaFactory;
	import org.osmf.media.MediaFactoryItem;
	import org.osmf.media.MediaResourceBase;
	import org.osmf.media.MediaElement;
	import org.osmf.elements.VideoElement;
	import com.akamai.osmf.elements.AkamaiVideoElement;
	
	/**
	 * ...
	 * @author Ari Oshinsky
	 */
	public class CustomDefaultMediaFactory extends DefaultMediaFactory 
	{
		public var _highjackedMediaCreationFunction:Function;
		public function CustomDefaultMediaFactory() {
			super();
		}
		
		override protected function resolveItems(resource:MediaResourceBase, items:Vector.<MediaFactoryItem>):MediaFactoryItem{
			var mfi:MediaFactoryItem = super.resolveItems( resource, items );
			/*If a custom MFI is being used, hijack it and intercept the media element it returns to set smoothing on it*/
			if( mfi.id.indexOf( 'org.osmf' ) < 0 )
			{
			_highjackedMediaCreationFunction = mfi.mediaElementCreationFunction;
			var hijacker:MediaFactoryItem = new MediaFactoryItem( mfi.id, mfi.canHandleResourceFunction, interceptMediaElement );
			return hijacker;
			}
			return mfi;
		}
		protected function interceptMediaElement():MediaElement{
			var element:AkamaiVideoElement = _highjackedMediaCreationFunction() as AkamaiVideoElement;
			trace("~interceptMediaElement");
			trace("element: " + element.toString());
			if ( element is AkamaiVideoElement )
			
			{
				
				element.smoothing = true;
				
			//VideoElement( element ).smoothing = true;
			trace("!APPLY SMOOTHING!!!!!!!!!!!!!!!")
			}
			return element;
		}
	}
		
		
	
	
}