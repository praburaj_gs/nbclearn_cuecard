package 
{
	import flash.display.MovieClip;
	import flash.net.URLLoader;
	import org.osmf.media.MediaElement;
	import org.osmf.media.MediaPlayerSprite;
	import org.osmf.media.MediaPlayer;
	import org.osmf.media.DefaultMediaFactory;
	import org.osmf.display.ScaleMode;
	import org.osmf.media.URLResource;
	import org.osmf.events.MediaFactoryEvent;
	import com.akamai.stagevideo.StageVideoHelper;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.geom.Rectangle;
	import flash.events.Event;
	import flash.net.URLRequest;
	
	/**
	 * ...
	 * @author Ari Oshinsky
	 */
	public class OSMF_Simple_test extends MovieClip 
	{
		
		
		private static const AKAMAI_PLUGIN:String =	"AkamaiAdvancedStreamingPlugin2.7_osmf1.6.swf"; 
		private var videoPlayerOSMF:MediaPlayer;
		var mediaFactory:DefaultMediaFactory;
		var video_helper:StageVideoHelper;
		var mediaPlayerSprite:MediaPlayerSprite
		
		public function OSMF_Simple_test() {
			
			mediaPlayerSprite = new MediaPlayerSprite();
		
			mediaPlayerSprite.x = 5; 
			mediaPlayerSprite.y = 5; 
			mediaPlayerSprite.width = 640;
			mediaPlayerSprite.height = 480;
			
			this.addChild(mediaPlayerSprite);
			
			mediaPlayerSprite.scaleMode = ScaleMode.LETTERBOX;
		
			

		
		//	videoPlayerOSMF = mediaPlayerSprite.media;
			mediaFactory = new DefaultMediaFactory()
			mediaFactory.addEventListener(MediaFactoryEvent.PLUGIN_LOAD, onPluginLoad)
			var resource:URLResource = new URLResource(AKAMAI_PLUGIN);

			mediaFactory.loadPlugin(resource);
		}
		
		private function onPluginLoad(e:MediaFactoryEvent){
			
			trace("~Akamai plugin loaded successfully.");
			video_helper = new StageVideoHelper(this.stage, new Rectangle(mediaPlayerSprite.x, mediaPlayerSprite.y, mediaPlayerSprite.width, mediaPlayerSprite.height));
			this.stage.addEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY , checkStageVideoAvilability);
			
			
			var akamaiLoader:URLLoader = new URLLoader();
			akamaiLoader.addEventListener(Event.COMPLETE, loadVideoStream);
			akamaiLoader.load(new URLRequest("http://archives.nbclearn.com/portal/testjsp/smil.jsp?assetID=7ce847a30303ad9d2d14d18f89b7944e"));
			
		}
		
		private function loadVideoStream(event:Event) {
			var loader:URLLoader = URLLoader(event.target);
			var akamaiMediaResource:URLResource = new URLResource(loader.data)// the loader returns the phyisical location of the smil file.
			
			var mediaElement:MediaElement = mediaFactory.createMediaElement(akamaiMediaResource);
			mediaPlayerSprite.media =  mediaElement; // set the media element as the new media to load,  it should automaticly trigger a play command.
		
		}
		private function checkStageVideoAvilability(event:StageVideoAvailabilityEvent) {
			object_mc.trace_txt.text = "Stage Video Available: " + event.availability;
			//trace("~GPU CHECK: " + event.availability);
		}
	}
	
}